<!-- end #header -->
<?php
$data['visit_id'] = $v_data['visit_id'] = $visit_id;
$data['lab_test'] = 100;

if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['visit_history'] = 1;
//Symptoms

// var_dump($patient_id);die();
//Plan
// $v_data['query'] = $this->nurse_model->get_visit_pan_detail($visit_id);

// $plan = $this->load->view('medical_record/plan', $v_data, TRUE);

?>
<div id="loader" style="display: none;"></div>

<div class="col-md-2">
	<div id="sidebar-detail"></div>
</div>
<div class="col-md-10">
	<br>
		<div class="pull-right">
			<a href="<?php echo site_url();?>queues/general-queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
		</div>
		 
	<br>
	<br>
	<div class="tabs">
		<ul class="nav nav-tabs nav-justified">
		    <li class="active">
		        <a class="text-center" data-toggle="tab" href="#outpatient">Prescription</a>
		    </li>
		    <li>
		        <a class="text-center" data-toggle="tab" href="#visit-prescription" onclick="get_past_prescriptions(<?php echo $visit_id;?>,<?php echo $patient_id;?>)">Previous Prescription</a>
		    </li>                
		    <li>
		        <a class="text-center" data-toggle="tab" href="#plan-visit1" onclick="get_visit_trail(<?php echo $visit_id;?>)">Visit Trail</a>
		    </li>
		</ul>
		<div class="tab-content">
		    <div class="tab-pane active" id="outpatient" style="height: 68vh;overflow-x: none;overflow-y: scroll;">
		    	   <br>
		    	      <br>
		       <div class="col-md-12">
		       	
		       	
	                <div class="col-lg-8 col-md-8 col-sm-8">
		              <div class="form-group">
		              	<label class="col-md-2 control-label">Drugs: </label>

							<div class="col-md-10">
				                <select id='service_id_item' name='service_charge_id' class='form-control custom-select ' onchange="get_service_charge_amount(this.value)">
				                  <option value=''>None - Please Select a Drugs to prescribe</option>
				                  <?php echo $drugs;?>
				                </select>
				         </div>
		              </div>
		            </div>
		            <input type="hidden" name="charge" id="drug-charge" >
		            <div class="col-lg-4 col-md-4 col-sm-4">
					  <div class="form-group">
						  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_drug_charge(<?php echo $visit_id;?>);"> Add Drug</button>
					  </div>
					</div>

		            

		         </div>
		         <br>
		          <div id="prescription_view"></div>
		          <hr>
		           <div id="visit_prescription"></div>
		    </div>
		    <div class="tab-pane" id="visit-prescription" style="height: 68vh;overflow-x: none;overflow-y: scroll;">
		        <h4 class="center-align" style="margin-bottom:10px;">Visit Prescriptions </h4>
		         <div id="patient-previous-prescription"></div>                   
		    </div>
		      <div class="tab-pane" id="plan-visit1" style="height: 68vh;overflow-x: none;overflow-y: scroll;">
		        <h4 class="center-align" style="margin-bottom:10px;">Visit Trail</h4>	
		        <div id="patient-visit-trail"></div>
		       
		       
		    </div>
		</div>
	</div>


	<div class="row" style="margin-top:5px;">
			<div class="center-align">
				<?php

				$items = '';
				if($visit_id > 0)
				{

					$visit_rs = $this->accounts_model->get_visit_details($visit_id);
					$visit_type_id = 0;
					if($visit_rs->num_rows() > 0)
					{
						foreach ($visit_rs->result() as $key => $value) {
							# code...
							$close_card = $value->close_card;
							$visit_type_id = $value->visit_type;
							$invoice_number = $value->invoice_number;
							$visit_time_out = $value->visit_time_out;
							$parent_visit = $value->parent_visit;
							$insurance_number = $value->insurance_number;
							$department_id_to = $value->department_id;
							$copay_percentage = 0;//$value->copay_percentage;
							
							$visit_time_out = date('jS F Y',strtotime($visit_time_out));
						}
					}


					$department_name_to = $this->reception_model->get_department_name($department_id_to);

					
					$all_departments_rs = $this->reception_model->get_patient_departments($visit_id,1);
				
					if($all_departments_rs->num_rows() > 0)
					{
						
						?>
						<div class="col-md-2 pull-left">
								<?php echo form_open("pharmacy/release_patient/".$visit_id."/".$visit_department_id, array("class" => "form-horizontal"));?>
									<input type="hidden" name="visit_department_id" value="<?php echo $visit_department_id;?>">
									<input type="submit" class="btn btn-sm btn-success center-align" value="Release patient " onclick="return confirm('Are you sure you want to release the patient from your queue ?');"/>
								<?php echo form_close();?>
							</div>
						<div class="col-md-4 pull-right" >
							<?php echo '<a href="'.site_url().'pharmacy/print-prescription/'.$visit_id.'" class="btn btn-sm btn-warning print_prescription" target="_blank"><i class="fa fa-print"></i> Print Prescription</a>';?>
						</div>
						<?php
					}
					else
					{

						if($department_id_to == 5)
						{
							?>
							<div class="col-md-2">
								<div class="center-align">
									<?php echo form_open("pharmacy/send_to_accounts/".$visit_id."/7/a", array("class" => "form-horizontal"));?>
									<input type="hidden" name="visit_department_id" value="<?php echo $visit_department_id;?>">
									<input type="submit" class="btn btn-sm btn-warning center-align" value="Send To Accounts" onclick="return confirm('Send to accounts?');"/>
									<?php echo form_close();?>
								</div>
							</div>
							<div class="col-md-2">
								<a href="<?php echo site_url();?>accounts/end_visit/<?php echo $visit_id?>/a" class="btn btn-danger btn-sm  " onclick="return confirm('Do you want to close this visit ?')" ><i class="fa fa-folder"></i> Close this visit </a>
							</div>
							<?php
						}
						else
						{
							?>
							<div class="col-md-2">
								<div class="center-align">
										<?php echo form_open("pharmacy/send_to_doctor/".$visit_id."/".$department_id."", array("class" => "form-horizontal"));?>
											<input type="hidden" name="visit_department_id" value="<?php echo $visit_department_id;?>">
											<input type="submit" class="btn btn-sm btn-success center-align" value="Send To <?php echo $department_name_to?> " onclick="return confirm('Send to <?php echo $department_name_to?> ?');"/>
										<?php echo form_close();?>
									</div>
							</div>
							<div class="col-md-2">
								<div class="center-align">
									<?php echo form_open("pharmacy/send_to_accounts/".$visit_id."/7", array("class" => "form-horizontal"));?>
									<input type="hidden" name="visit_department_id" value="<?php echo $visit_department_id;?>">
									<input type="submit" class="btn btn-sm btn-warning center-align" value="Send To Accounts" onclick="return confirm('Send to accounts?');"/>
									<?php echo form_close();?>
								</div>
							</div>

							<?php
						}
						?>
							
						<div class="col-md-4 pull-right" >
							<?php echo '<a href="'.site_url().'pharmacy/print-prescription/'.$visit_id.'" class="btn btn-sm btn-warning print_prescription" target="_blank"><i class="fa fa-print"></i> Print Prescription</a>';?>
						</div>
					


						<?php
					}
				}
			
					?>
				
			</div>
	
			
		</div>
	    
    


	  


</div>
  
<script type="text/javascript">
	$(document).ready(function(){
	  display_inpatient_prescription(<?php echo $visit_id;?>,1);
	  get_visit_amounts(<?php echo $visit_id;?>);
	  get_sidebar_details(<?php echo $patient_id;?>,<?php echo $visit_id;?>);
	});

	$(function() {
	    $("#service_id_item").customselect();

	  });

	function myPopup2(visit_id,module) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id+"/"+module,"Popup3","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function myPopup2_soap(visit_id) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id,"Popup2","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function send_to_pharmacy2(visit_id)
	{
		var config_url = $('#config_url').val();
		var url = config_url+"pharmacy/display_prescription/"+visit_id;
	
		$.get(url, function( data ) {
			var obj = window.opener.document.getElementById("prescription");
			obj.innerHTML = data;
			window.close(this);
		});
	}
</script>



<script type="text/javascript">

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function get_visit_amounts(visit_id)
	{

		var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var url = "<?php echo site_url();?>pharmacy/get_visit_amount/"+visit_id;
	    // alert(url);
	     if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	              // var prescription_view = document.getElementById("prescription_view");
	             
	              document.getElementById("visit-balance").innerHTML=XMLHttpRequestObject.responseText;
	               // prescription_view.style.display = 'block';
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }

	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}

	function update_prescription_values(visit_id,visit_charge_id,prescription_id,module)
    {
      
       //var product_deductions_id = $(this).attr('href');
       var quantity = $('#quantity'+prescription_id).val();
       var x = $('#x'+prescription_id).val();
       var duration = $('#duration'+prescription_id).val();
       var consumption = $('#consumption'+prescription_id).val();


       var url = "<?php echo base_url();?>pharmacy/update_prescription/"+visit_id+'/'+visit_charge_id+'/'+prescription_id+'/'+module;
  
        //window.alert(data_url);
		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
		  dataType: 'text',
           success:function(data){
            
            // window.alert(data.result);
            if(module == 1){
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"/1'";
			
			}else{
				window.location.href = "<?php echo base_url();?>pharmacy/prescription1/"+visit_id+"";
			}
           },
           error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
           
           }
        });
        return false;
     }
  </script>

<script type="text/javascript">
	function get_drug_to_prescribe(visit_id)
	{
	  var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var drug_id = document.getElementById("drug_id").value;

	    var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/1";

	     if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	              var prescription_view = document.getElementById("prescription_view");
	             
	              document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
	               prescription_view.style.display = 'block';
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }

	}
	function pass_prescription()
    {

	   var quantity = document.getElementById("quantity_value").value;
	   var x = document.getElementById("x_value").value;
	   var dose_value = document.getElementById("dose_value").value;
	   var duration = 1;//document.getElementById("duration_value").value;
	   var consumption = document.getElementById("consumption_value").value;
	   var number_of_days = document.getElementById("number_of_days_value").value;
	   var service_charge_id = document.getElementById("drug_id").value;
	   var visit_id = document.getElementById("visit_id").value;
	   var input_total_units = document.getElementById("input-total-value").value;
	   var module = document.getElementById("module").value;
	   var passed_value = document.getElementById("passed_value").value;
	   var type_of_drug = document.getElementById("type_of_drug").value;
	   var charge_date = document.getElementById("charge_date").value;
	   
	   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
	   
	  
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug,charge_date: charge_date},
	   dataType: 'text',
	   success:function(data){
	   		var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				 var prescription_view = document.getElementById("prescription_view");
			   prescription_view.style.display = 'none';
			   display_inpatient_prescription(visit_id,1);
			}
			else
			{

				alert(data.result);
			}
	  
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	   
	   return false;
    }


	function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }
	function display_inpatient_prescription(visit_id,module){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	    get_visit_amounts(visit_id);
	}

	function display_inpatient_visit_prescription(visit_id,module){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"pharmacy/display_inpatient_visit_prescription/"+visit_id+"/"+module;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("all_visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
	{
	  var quantity = $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	
	  var url = "<?php echo site_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;


	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
	  dataType: 'text',
	  success:function(data){

	  
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}

	function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  var service_charge_amount = $('#service_charge_amount'+prescription_id).val();
	  var product_id = $('#product_id'+prescription_id).val();
	 var product_deleted = $('#product_deleted'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 // alert(url);
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given,service_charge_amount: service_charge_amount, product_id: product_id,product_deleted: product_deleted},
	  dataType: 'text',
	  success:function(data){
	  	var data = jQuery.parseJSON(data);
	  	display_inpatient_prescription(visit_id,1);
	  	if(data.status == 1)
	  	{
	    	window.alert(data.result);
	  	}
	  	else
	  	{
	  		var res = confirm(data.result);  
			if(res)
			{
				var url = "<?php echo site_url();?>pharmacy/borrow_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
				$.ajax({
				type:'POST',
				url: url,
				data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
				dataType: 'json',
				success:function(data){

					display_inpatient_prescription(visit_id,1);					
					// window.alert(data.result);
				},
				error: function(xhr, status, error) {
				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

				}
				});
				return false;
			}

	  	}
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  return false;
	}

	function undispense_prescription(visit_id,visit_charge_id,prescription_id,module)
	{

	  var quantity =  $('#quantity'+prescription_id).val();
	  var x = $('#x'+prescription_id).val();
	  var duration = $('#duration'+prescription_id).val();
	  var consumption = $('#consumption'+prescription_id).val();
	  var charge = $('#charge'+prescription_id).val();
	  var units_given = $('#units_given'+prescription_id).val();
	  
	  var url = "<?php echo site_url();?>pharmacy/undispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module+"/"+quantity;
	 
	  $.ajax({
	  type:'POST',
	  url: url,
	  data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
	  dataType: 'json',
	  success:function(data){
	  	display_inpatient_prescription(visit_id,1);
	    window.alert(data.result);
	  },
	  error: function(xhr, status, error) {
	  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	  }
	  });
	  display_inpatient_prescription(visit_id,1);
	  return false;
	}

function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
{
  var res = confirm('Are you sure you want to delete this prescription ?');
  
  if(res)
  {
    var XMLHttpRequestObject = false;
    
    if (window.XMLHttpRequest) {
      XMLHttpRequestObject = new XMLHttpRequest();
    } 
    
    else if (window.ActiveXObject) {
      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "<?php echo site_url();?>pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
    // alert(url);
    if(XMLHttpRequestObject) {
      
      XMLHttpRequestObject.open("GET", url);
      
      XMLHttpRequestObject.onreadystatechange = function(){
        
        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
          
           display_inpatient_prescription(visit_id,1);
        }
      }
      XMLHttpRequestObject.send(null);
    }
  }
}

$(document).on("click","a.print_prescription",function(e)
{
	e.preventDefault();
	
	//get checkbox values
	var val = [];
	$(':checkbox:checked').each(function(i){
	  	val[i] = $(this).val();
	});
	var request = '<?php echo site_url();?>pharmacy/print_selected_drugs/<?php echo $visit_id;?>';
	$.ajax({
		type:'POST',
		url: request,
		data:{prescription_id: val},
		dataType: 'text',
		success:function(data)
		{
			var win = window.open("<?php echo site_url();?>pharmacy/print-prescription/<?php echo $visit_id;?>", '_blank');
			if(win){
				//Browser has allowed it to be opened
				win.focus();
			}else{
				//Broswer has blocked it
				alert('Please allow popups for this site');
			}
		},
		error: function(xhr, status, error) 
		{
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		}
	});
	
	return false;
});

function get_visit_trail(visit_id)
{
	document.getElementById("loader").style.display = "block";
	var XMLHttpRequestObject = false;       
	if (window.XMLHttpRequest) {

	XMLHttpRequestObject = new XMLHttpRequest();
	} 

	else if (window.ActiveXObject) {
	XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var config_url = $('#config_url').val();
	var url = config_url+"nurse/get_visit_trail/"+visit_id;
	// alert(url);
	if(XMLHttpRequestObject) {
	var obj = document.getElementById("patient-visit-trail");
	XMLHttpRequestObject.open("GET", url);
	   
	XMLHttpRequestObject.onreadystatechange = function(){
	 
	 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	   obj.innerHTML = XMLHttpRequestObject.responseText;
	   //obj2.innerHTML = XMLHttpRequestObject.responseText;
	  
	   document.getElementById("loader").style.display = "none";
	   // get_lab_table(visit_id);

	 }
	}

	XMLHttpRequestObject.send(null);
	}
}

function get_past_prescriptions(visit_id,patient_id)
{
	document.getElementById("loader").style.display = "block";
	var XMLHttpRequestObject = false;       
	if (window.XMLHttpRequest) {

	XMLHttpRequestObject = new XMLHttpRequest();
	} 

	else if (window.ActiveXObject) {
	XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var config_url = $('#config_url').val();
	var url = config_url+"pharmacy/get_patient_previous_prescriptions/"+visit_id+"/"+patient_id;
	// alert(url);
	if(XMLHttpRequestObject) {
	var obj = document.getElementById("patient-previous-prescription");
	XMLHttpRequestObject.open("GET", url);
	   
	XMLHttpRequestObject.onreadystatechange = function(){
	 
	 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	   obj.innerHTML = XMLHttpRequestObject.responseText;
	   //obj2.innerHTML = XMLHttpRequestObject.responseText;
	  
	   document.getElementById("loader").style.display = "none";
	   // get_lab_table(visit_id);

	 }
	}

	XMLHttpRequestObject.send(null);
	}

}


	function get_service_charge_amount(service_charge_id)
	{
  	   var url = "<?php echo base_url();?>pharmacy/get_drug_price/"+service_charge_id;
  	   // alert(url);
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{service_charge_id: service_charge_id},
	   dataType: 'text',
	   success:function(data){
	   	 var data = jQuery.parseJSON(data);
         var amount = data.amount
         // alert(amount);
	   	 document.getElementById("drug-charge").value = amount;
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	   
	   return false;
	}

	 function parse_drug_charge(visit_id)
	{
		var service_charge_id = document.getElementById("service_id_item").value;
		prescibe_drug(service_charge_id, visit_id);	    
	}
	function prescibe_drug(service_charge_id, visit_id){
    
	   var quantity = 1;
	   var x = 1;
	   var duration = 1;
	   var consumption = 1;
	   var charge = $('#drug-charge').val();;
	   var units_given = 1;
	   var input_total_units = 1;
	   
	   var url = "<?php echo base_url();?>pharmacy/add_pharmacy_charge/"+service_charge_id+"/"+visit_id;
	   
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given,service_charge_id: service_charge_id,input_total_units: input_total_units},
	   dataType: 'text',
	   success:function(data){
	    
	     var data = jQuery.parseJSON(data);
		  // alert(data.content);
		   // window.alert(data.result);
		if(data.status == "1")
		{
			 display_inpatient_prescription(visit_id,1);
		}
		else
		{
			alert(data.result);
		}


	     
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   	 display_inpatient_prescription(visit_id,1);
	   }
	   });
	   // display_inpatient_prescription(visit_id,1);
	   return false;
	}


	function get_sidebar_details(patient_id,visit_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"reception/get_sidebar_details/"+patient_id+"/"+visit_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#sidebar-detail").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}



</script>                        