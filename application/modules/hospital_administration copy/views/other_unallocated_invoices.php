<div class="row">
    <div class="col-md-6">
<?php echo $this->load->view('search_invoice', '', TRUE);?>
</div>

 <div class="col-md-6">
<?php echo $this->load->view('search_patients', '', TRUE);?>
</div>
</div>
<div class="row">
    <div class="col-md-6">

      
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
      </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			$this->session->unset_userdata('error_message');
		}
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th><a >Invoice Number</a></th>
						  <th><a >item Name</a></th>
						  <th><a >Amount</a></th>
						  <th >Quantity</th>
						  <th >Total</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			$total_amount =0;
			foreach ($query->result() as $row)
			{
				
				$invoice_number = $row->invoice_number;
				$customer_name = $row->customer_name;
				$item_name = $row->item_name;
				$item_price = $row->item_price;
				$quantity = $row->quantity;

				$amount = $item_price*$quantity;
				$total_amount +=$amount;
				
			
				
				$count++;
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$invoice_number.'</td>
							<td>'.$item_name.'</td>
							<td>'.$item_price.'</td>
							<td>'.$quantity.'</td>
							<td>'.$amount.'</td>
						
							
						</tr> 
					';
			}
			$result .= 
					'
						<tr>
							<td colspan=3> Account Name : '.$customer_name.'</td>
							<td></td>
							<td></td>
							<td>'.$total_amount.'</td>
						
							
						</tr> 
					';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no services";
		}
		?>
            <?php echo $result; ?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
        </div>
        <div class="col-md-6">
        <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Patients</h2>
      </header>             

          <!-- Widget content -->
                <div class="panel-body">
                	<?php
					$result_patient = '';
					
					//if users exist display them
					if(!empty($patients_query))
					{

						if ($patients_query->num_rows() > 0)
						{
							$counting = 0;
							
							$result_patient .= 
								'
									<table class="table table-hover table-bordered ">
									  <thead>
										<tr>
										  <th>#</th>
										  <th><a >Name</a></th>
										  <th><a >Number</a></th>
										</tr>
									  </thead>
									  <tbody>
								';
							
							//get all administrators
						
							foreach ($patients_query->result() as $patients_rows => $values)
							{
									
								$display_name = $values->display_name;
								$patient_number = $values->patient_number;
								$patient_id = $values->patient_id;

								$counting++;
								$result_patient .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$display_name.'</td>
											<td>'.$patient_number.'</td>
											<td><a href="'.site_url().'hospital-administration/allocate-invoice/'.$invoice_number.'/'.$patient_id.'" class="btn btn-xs btn-danger" onclick="return confirm(\' Do you want to allocate this invoice '.$invoice_number.' to '.$patient_number.'?\'); "><i class="fa fa-money"></i> Allocate to patient</a></td>
										</tr> 
									';
							}
							
							
							$result_patient .= 
							'
										  </tbody>
										</table>
							';
						}
						
						else
						{
							$result_patient .= "There are no patients";
						}

					}
					
					?>
            <?php echo $result_patient; ?>
          </div>
        
		</section>
        </div>
  </div>
        