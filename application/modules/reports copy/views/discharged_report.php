<!-- search -->
<?php

  echo $this->load->view('search/search_discharges', '', TRUE);
 ?>
<!-- end search -->

<section class="panel ">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
        <div class="pull-right">
	          <!-- <a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-primary btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-up"></i> Outpatient Queue</a>
	           <a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-success btn-sm pull-right " style="margin-top:-25px;margin-right:5px;"><i class="fa fa-arrow-up"></i> Inpatient Queue</a> -->
	    </div>
    </header>

        <!-- Widget content -->
        <div class="panel-body">
          <div class="padd">
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
				
		$search = $this->session->userdata('discharge_report_search');
		
		if(!empty($search))
		{
			echo '
			<a href="'.site_url().'reports/close_discharge_search" class="btn btn-warning btn-sm ">Close Search</a>
			';
		}
	
		
		$result = '';
		
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			
				$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Patient Type</th>
						  <th>Patient Name</th>
						  <th> Phone</th>
						  <th>Age</th>
						  <th>No. of Visits</th>
						  <th>Visit Type</th>						  
						  <th>Date Of Admission</th>
						  <th>Date Of Discharge</th>
						  <th>Total Days as Inpatient</th>
						  <th>Visit Balance</th>
						  <th colspan="2"></th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

			$personnel_id = $this->session->userdata('personnel_id');

			$is_accounts = $this->reception_model->check_personnel_department_id($personnel_id,7);
			$is_admin = $this->reception_model->check_personnel_department_id($personnel_id,1);
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{

				$patient_id = $row->patient_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_date = $row->visit_date;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$deleted_by = $row->deleted_by;
				$visit_type_id = $row->visit_type_id;
				$created = $row->patient_date;
				$last_modified = $row->last_modified;
				$last_visit = $row->last_visit;
				$patient_phone1 = $row->patient_phone1;
				$patient_number = $row->patient_number;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_number = $row->patient_number;
				$current_patient_number = $row->current_patient_number;
				$patient_date = $row->patient_date;
				$visit_time_out = $row->visit_time_out;
				$visit_type_name = $row->visit_type_name;
				$visit_id = $row->visit_id;
				if($last_visit != NULL)
				{
					$last_visit = date('jS M Y',strtotime($last_visit));
				}
				
				else
				{
					$last_visit = '';
				}
			

				if(!empty($patient_date_of_birth))
				{
					$patient_age = $this->reception_model->calculate_age($patient_date_of_birth);
				}
				else
				{
					$patient_age = '';
				}
				
				$discharged_date = date('Y-m-d',strtotime($visit_time_out));


				$no_of_days = $this->reports_model->dateDiff($visit_date,$discharged_date);
				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);
				$visits = $last_visit_rs->num_rows();

				$bill = $this->accounts_model->get_total_visit_balance($visit_id);
				$count++;
				
				if($authorize_invoice_changes OR $is_admin OR $is_accounts)
				{
					$add = '<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-xs btn-success"> <i class="fa fa-print"></i> Account</a></td>';
				}
				else
				{
					$add = '';
				}
				
				
					$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$patient_number.' </td>
							<td>'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$patient_phone1.'</td>
							<td>'.$patient_age.'</td>
							<td>'.$visits.'</td>
							<td>'.$visit_type_name.'</td>
							<td>'.date('jS M Y',strtotime($visit_date)).'</td>
							<td>'.date('jS M Y',strtotime($visit_time_out)).'</td>
							<td>'.$no_of_days.'</td>
							<td>'.number_format($bill,2).'</td>
							'.$add.'
							
							<td><a href="'.site_url().'print-discharge-summary/'.$visit_id.'" target="_blank" class="btn btn-xs btn-warning"> <i class="fa fa-print"></i> discharge summary</a></td>

						</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->

      </div>
    </section>