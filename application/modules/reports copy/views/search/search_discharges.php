       
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
    	<h2 class="panel-title">Search Patients:</h2>
    
    </header>             

  <!-- Widget content -->
        <div class="panel-body">
	<?php
    echo form_open("reports/search_discharge_reports", array("class" => "form-horizontal"));
    ?>
    <div class="row">

        <div class="col-md-4">

        	 <div class="form-group">
                <label class="col-md-4 control-label"> Patient No: </label>
                
                <div class="col-md-8">
                	<input type="text" class="form-control" name="patient_number" placeholder="Patient no" autocomplete="off">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label"> Patient Name: </label>
                
                <div class="col-md-8">
                	<input type="text" class="form-control" name="surname" placeholder="Patient Name" autocomplete="off">
                </div>
            </div>
    
            
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-lg-4 control-label"> Admission Date: </label>
                
                <div class="col-lg-8">
                	<div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="admission_date_from" placeholder="Admission Date From">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Admission Date: </label>
                
                <div class="col-lg-8">
                	<div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="admission_date_to" placeholder="Admission Date To">
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-md-4">
        	<div class="form-group">
                <label class="col-lg-4 control-label"> Discharge Date: </label>
                
                <div class="col-lg-8">
                	<div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharge_date_from" placeholder="Discharge Date From">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Discharge Date: </label>
                
                <div class="col-lg-8">
                	<div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharge_date_to" placeholder="Discharge Date To">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
    	<div class="col-md-12">
    		<div class="form-group">
                <div class="center-align">
                    <button type="submit" class="btn btn-info">Search Report</button>
                </div>
        </div>
    		
    	</div>
    	
    </div>
    
    
    <?php
    echo form_close();
    ?>
  </div>
</section>