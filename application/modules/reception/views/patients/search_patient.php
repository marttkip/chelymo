 <section class="panel panel-info">
    <header class="panel-heading">
        <h2 class="panel-title">Search Patients</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
        <div class="padd">
            <?php
            echo form_open("reception/search_patients", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                   
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">I.D. number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_national_id" placeholder="National ID">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Patient Phone: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_phone_number" placeholder="Patient number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="surname" placeholder="Name">
                        </div>
                    </div>
                    
                    <div class="form-group" style="display: none">
                        <label class="col-md-4 control-label">Other names: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="othernames" placeholder="Other Names">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="center-align">
                                <button type="submit" class="btn btn-info btn-sm">Search</button>
                            </div>
                        </div>
                </div>
                
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>