<!-- search -->
<?php echo $this->load->view('search/search_inpatients', '', TRUE);?>
<!-- end search -->
 
 <section class="panel panel-success">
    <header class="panel-heading">
          <h2 class="panel-title"><i class="icon-reorder"></i><?php echo $title;?></h2>
          <div class="pull-right">
	          <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-right"></i> Patients List</a>
	    	</div>
        </header>
      <div class="panel-body">
          <div class="padd">
          
<?php
		$search = $this->session->userdata('inpatients_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reception/close_general_inpatient_queue_search" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
				
				if($page_name == 'nurse')
				{
					$actions = 5;
				}
				
				else if($page_name == 'doctor')
				{
					$actions = 4;
				}
				
				else if($page_name == 'laboratory')
				{
					$actions = 3;
				}
				
				else if(($page_name == 'pharmacy') || ($page_name == 'xray') || ($page_name == 'ultrasound'))
				{
					$actions = 2;
				}
				
				else if($page_name == 'accounts')
				{
					$actions = 5;
				}
				else if($page_name == 'administration')
				{
					$actions = 2;
				}
				
				else
				{
					$actions = 7;
				}
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						   <th>Patient Number</th>
						  <th>Admission date</th>
						  <th>Patient</th>
						  <th>Visit Type</th>
						  <th>Ward</th>
						   <th>Room</th>
						  <th>Doctor</th>
						  <th colspan="'.$actions.'">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$admission_date = date('jS M Y',strtotime($row->visit_date));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$patient_number = $row->patient_number;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$coming_from = $this->reception_model->coming_from($visit_id);
				$sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				
				//bed details
				$bed_query = $this->reception_model->get_visit_bed($visit_id);
				$ward = $row->ward_name;
				//$room = $row->room_name;
				$room = $this->reception_model->get_rooms();
				$beds = $this->reception_model->get_beds();
				$bed = '<span class="lable label-danger">Unasigned</span>';
				$room = '<span class="lable label-danger">Unasigned</span>';
				
			
				
				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_fname;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}
				$v_data = array('visit_id'=>$visit_id);
				$count++;
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_nurse = $this->reception_model->check_if_admin($personnel_id,24);

				$personnel_id = $this->session->userdata('personnel_id');
				$is_doctor = $this->reception_model->check_if_admin($personnel_id,12);

				if($is_nurse || $is_doctor)
				{
					$buttons = '
					

					<td><a href="'.site_url().'nurse/inpatient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Patient Card</a></td>
					<td><a href="'.site_url().'xray/'.$visit_id.'" class="btn btn-sm btn-danger"> Xray</a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-warning">Uploads</a></td>
					<td><a href="'.site_url().'patient-bill/'.$patient_id.'" class="btn btn-sm btn-primary" >Bill</a></td>
					';
				}
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_lab = $this->reception_model->check_if_admin($personnel_id,25);

				
				if($is_lab)
				{
					$buttons = '
					

					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-info">Tests</a></td>
					<td><a href="'.site_url().'laboratory/test_history/'.$visit_id.'" class="btn btn-sm btn-danger">History</a></td>
					';
				}
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_phamacist = $this->reception_model->check_if_admin($personnel_id,27);

				if($is_phamacist)
				{
					$buttons = '
					

					<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Prescription</a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					';
				}
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_records = $this->reception_model->check_if_admin($personnel_id,35);



				if($is_records)
				{
					$buttons = '  
					          <td>
									<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$patient_id.'">Discharged</button>
								
									<div class="modal fade" id="create_inpatient'.$patient_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Change Patient Status to Discharged</h4>
												</div>
												<div class="modal-body">
													'.form_open('reception/change_discharged_status/'.$patient_id, array("class" => "form-horizontal")).'
													<div class="form-group">
														<label class="col-lg-4 control-label">Discharged date: </label>
														<input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="'.$this->uri->uri_string().'">
														<div class="col-lg-8">
															<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharged_date" placeholder="RIP Date" value="'.date('Y-m-d').'">
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-12">
															<div class="center-align">
																<button type="submit" class="btn btn-primary">Update Status</button>
															</div>
														</div>
													</div>
													'.form_close().'
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									</td>
									<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Patient Card</a></td>
						
					
					
                    <td><a href="'.site_url().'nurse/inpatient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info"> Card</a></td>
                    <td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#create_inpatient'.$patient_id.'">Discharge</button>
								
									<div class="modal fade" id="create_inpatient'.$patient_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Change Patient Status to Discharged</h4>
												</div>
												<div class="modal-body">
													'.form_open('reception/change_discharged_status/'.$patient_id, array("class" => "form-horizontal")).'
													<div class="form-group">
														<label class="col-lg-4 control-label">Discharged date: </label>
														<input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="'.$this->uri->uri_string().'">
														<div class="col-lg-8">
															<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharged_date" placeholder="Discharged Date" value="'.date('Y-m-d').'">
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-12">
															<div class="center-align">
																<button type="submit" class="btn btn-primary">Update Status</button>
															</div>
														</div>
													</div>
													'.form_close().'
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
							</td>


					';
				}				
				
				
				$is_admin = $this->reception_model->check_if_admin($personnel_id,1);

				if($is_admin OR $personnel_id == 0)
				{
					$buttons = '
					<td><a href="'.site_url().'nurse/inpatient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Card</a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					
					<td><a href="'.site_url().'care-units/'.$visit_id.'" class="btn btn-sm btn-primary">ICU/HDU/Theatre</a></td>
					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-info">Tests</a></td>
					<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-default">Prescription</a></td>
					<td><a href="'.site_url().'xray/'.$visit_id.'" class="btn btn-sm btn-danger"> Xray</a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-warning">Uploads</a></td>
					
					';
					
					// <td><a href="'.site_url().'pharmacy/send_to_accounts/'.$visit_id.'" class="btn btn-sm btn-success" onclick="return confirm(\'Send to accounts?\');">To Accounts</a></td>
					
					// <td><a href="'.site_url().'reception/end_visit/'.$visit_id.'" class="btn btn-sm btn-info" onclick="return confirm(\'Do you really want to end this visit ?\');">End Visit</a></td>
					
				}
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_accountant = $this->reception_model->check_if_admin($personnel_id,28);
				
				if($is_accountant)
				{
					$buttons = '
					<td>
						<a  class="btn btn-sm btn-success" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Visit Trail</a>
						<a  class="btn btn-sm btn-success" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close Trail</a></td>
					</td>

					<td><a href="'.site_url().'accounts/print_receipt_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-info">Receipt</a></td>
					<td><a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-success">Invoice </a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					<td><a href="'.site_url().'reception/end_visit/'.$visit_id.'/1" class="btn btn-sm btn-danger" onclick="return confirm(\'End this visit?\');">Discharge</a></td>
					';
				}
				
				$personnel_id = $this->session->userdata('personnel_id');
				$is_receptionist = $this->reception_model->check_if_admin($personnel_id,10);
				
				if($is_receptionist)
				{

					$buttons = '
					<td>
						<a  class="btn btn-sm btn-success" id="open_visit'.$visit_id.'" onclick="get_visit_trail('.$visit_id.');">Visit Trail</a>
						<a  class="btn btn-sm btn-success" id="close_visit'.$visit_id.'" style="display:none;" onclick="close_visit_trail('.$visit_id.');">Close Trail</a></td>
					</td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td><a href="'.site_url().'reception/print-invoice/'.$visit_id.'/reception" target="_blank" class="btn btn-sm btn-warning fa fa-print"> Print Invoice </a></td>
					<td><a href="'.site_url().'reception/end_visit/'.$visit_id.'" class="btn btn-sm btn-info" onclick="return confirm(\'Do you really want to end this visit ?\');">End Visit</a></td>
					<td><a href="'.site_url().'reception/delete_visit/'.$visit_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this visit?\');">Delete Visit</a></td>';
					$buttons .= '<td></td>';
				}
			
								
				
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$patient_number.'</td>
							<td>'.$admission_date.'</td>
							<td>'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$visit_type_name.'</td>
							<td>'.$ward.'</td>
							<td>'.$room.'</td>
							<td>'.$doctor.'</td>
							'.$buttons.'
						</tr> 
					';
					
					$v_data['patient_type'] = $visit_type_id;
				
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
?>
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		echo $result;
		?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>

  <script type="text/javascript">

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
  </script>