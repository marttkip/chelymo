 <section class="panel panel-success">
    <header class="panel-heading">
        <h2 class="panel-title">Search <?php echo $title?> for <?php echo date('jS M Y',strtotime(date('Y-m-d'))); ?></h2>
    </header>
    <!-- Widget content -->
         <div class="panel-body">
    	<div class="padd">
			<?php
			
			
			echo form_open("reception/search_all_queue", array("class" => "form-horizontal"));
			
            
            ?>
            <div class="row">
                <div class="col-md-4">
                    
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">File No: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_number" placeholder="Patient No.">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">I.D. No.: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_national_id" placeholder="I.D. No.">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="surname" placeholder="Patient">
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label">Phone: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="phone_number" placeholder="Phone">
                        </div>
                    </div>
                    
                    
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <!-- <label class="col-lg-3 control-label">Schedule appointment? </label> -->
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="date_time" value="1" >
                                    Today
                                </label>
                            </div>
                        </div>
                        
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="date_time" value="2" >
                                    Yesterday
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="date_time" value="3" >
                                    This Week
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="date_time" value="4" >
                                    This Month
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- <label class="col-md-4 control-label">Date From: </label> -->
                                
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- <label class="col-md-4 control-label">Date To: </label> -->
                                
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="center-align">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
</section>