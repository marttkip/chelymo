<?php
$all_wards = '';
if($wards->num_rows() > 0)
{
	foreach($wards->result() as $row):
		$ward_name = $row->ward_name;
		$ward_id = $row->ward_id;
		
		if($ward_id == set_value('ward_id'))
		{
			$all_wards .= "<option value='".$ward_id."' selected='selected'>".$ward_name."</option>";
		}
		
		else
		{
			$all_wards .= "<option value='".$ward_id."'>".$ward_name."</option>";
		}
	endforeach;
}
$all_rooms = '';
if($rooms->num_rows() > 0)
{
	foreach($rooms->result() as $row):
		$room_name = $row->room_name;
		$room_id = $row->room_id;
		
		if($room_id == set_value('room_id'))
		{
			$all_rooms .= "<option value='".$room_id."' selected='selected'>".$room_name."</option>";
		}
		
		else
		{
			$all_rooms .= "<option value='".$room_id."'>".$room_name."</option>";
		}
	endforeach;
}
$all_doctors = '';
if($doctors->num_rows() > 0){
	foreach($doctors->result() as $row):
		$fname = $row->personnel_fname;
		$onames = $row->personnel_onames;
		$personnel_id = $row->personnel_id;
		
		if($personnel_id == set_value('personnel_id'))
		{
			$all_doctors .= "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
		}
		
		else
		{
			$all_doctors .= "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
		}
	endforeach;
}
?>
<!-- search -->
<?php echo $this->load->view('search/search_patients', '', TRUE);?>
<!-- end search -->
 
 <section class="panel panel-primary">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> for <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></h2>

        <div class="pull-right">
	          <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-right"></i> Patients List</a>
	    </div>
    </header>
      <div class="panel-body">
          <div class="padd">
          
<?php
		$search = $this->session->userdata('general_queue_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reception/close_general_queue_search/'.$page_name.'" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		$dependant_id =0;

		$personnel_id = $this->session->userdata('personnel_id');

		$is_front_office = $this->reception_model->check_personnel_department_id($personnel_id,9);
		$is_accounts = $this->reception_model->check_personnel_department_id($personnel_id,7);
		$is_dentist = $this->reception_model->check_personnel_department_id($personnel_id,21);
		$is_doctor = $this->reception_model->check_personnel_department_id($personnel_id,2);
		$is_admin = $this->reception_model->check_personnel_department_id($personnel_id,1);
		$is_pharmacy = $this->reception_model->check_personnel_department_id($personnel_id,5);
		$is_labtech = $this->reception_model->check_personnel_department_id($personnel_id,6);
		$is_nurse = $this->reception_model->check_personnel_department_id($personnel_id,8);
		$is_ultrasound = $this->reception_model->check_personnel_department_id($personnel_id,22);

		$is_ultrasound = $this->reception_model->check_personnel_department_id($personnel_id,22);

		$is_obs = $this->reception_model->check_personnel_department_id($personnel_id,24);

		$is_urologist = $this->reception_model->check_personnel_department_id($personnel_id,23);
		$is_peaditrician = $this->reception_model->check_personnel_department_id($personnel_id,23);
		$is_physician = $this->reception_model->check_personnel_department_id($personnel_id,22);
		$is_mch = $this->reception_model->check_personnel_department_id($personnel_id,26);



		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
				
				
			$result .= 
				'
					<table class="table  table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Date</th>
						  <th>Number</th>
						  <th>Name</th>
						  <th>Type</th>
						  <th>Department</th>
						  <th>From</th>
						  <th>To</th>
						   <th>Time</th>
						  <th>Doctor</th>
						  <th colspan="9">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			$personnel_query = $this->personnel_model->get_all_personnel();

		
			
			foreach ($query->result() as $row)
			{
				$date_checked = $row->visit_date;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				$visit_created = date('H:i a',strtotime($row->visit_date));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $doctor_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$patient_number = $row->patient_number;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$time_start = $row->time_start;
				$accounts = "";//$row->accounts;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$coming_from = $this->reception_model->coming_from($visit_id);
				$sent_to = $this->reception_model->going_to($visit_id);

				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
					$department_name = $row->department_name;

				// $visit_department_id = $row->department_id;
				$doctor = $row->personnel_onames;

				$revist = $revisit;

				$patient_date_of_birth = $row->patient_date_of_birth;
				$hold_card = $row->hold_card;
				$ward_id = 1;//$row->ward_id;
				$response = 0;//$this->accounts_model->get_visit_balance($visit_id);
				$invoice_total = 0;//$this->accounts_model->total_invoice($visit_id);
				$waiver = 0;//$this->accounts_model->total_waivers($visit_id);
				
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
					$seconds = strtotime($row->visit_time_out) - strtotime($row->visit_time);//$row->waiting_time;
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
				}
				else
				{

					$visit_time_out = date('Y-m-d H:i:s');
					// var_dump($visit_time_out); die();
					$seconds = strtotime($visit_time_out) - strtotime($row->visit_time);//$row->waiting_time;
					$days    = floor($seconds / 86400);
					$hours   = floor(($seconds - ($days * 86400)) / 3600);
					$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
					$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
					
					//$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
					$total_time = $days.' '.$hours.':'.$minutes.':'.$seconds;
					$visit_time_out = '-';
				}
					
				$user_id = $this->session->userdata('personnel_id');
				

				// var_dump($department_id); die();
				//cash paying patient sent to department but has to pass through the accounts
				if($coming_from == "Laboratory")
				{
					$balanced = 'warning';
				}
				else
				{
					$balanced = 'default';
				}
				
				
				$v_data = array('visit_id'=>$visit_id);
				$count++;			

					// var_dump($is_doctor); die();
				if($is_doctor OR $is_mch OR $is_obs OR $is_urologist)
				{
					if($sent_to == "Traige" || $sent_to == "General Practice")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
				

					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/1" class="btn btn-sm btn-info">Card</a></td>
					<td><a href="'.site_url().'xray/'.$visit_id.'" class="btn btn-sm btn-danger"> Xray</a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td>
						<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$visit_id.'">Inpatient</button>
						
						<div class="modal fade" id="create_inpatient'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Change to inpatient</h4>
									</div>
									<div class="modal-body">
										'.form_open('reception/change_patient_visit/'.$visit_id.'/'.$visit_type_id, array("class" => "form-horizontal")).'
										<div class="form-group">
											<label class="col-md-4 control-label">Ward: </label>
											
											<div class="col-md-8">
												<select name="ward_id" id="ward_id" class="form-control" >
													<option value="">----Select a ward----</option>
													'.$all_wards.'
												</select>
											</div>
										</div>

										

										<div class="form-group">
											<label class="col-md-4 control-label">Doctor: </label>
											
											<div class="col-md-8">
												 <select name="personnel_id" id="personnel_id" class="form-control custom-select">
													<option value="">----Select a Doctor----</option>
													'.$all_doctors.'
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Admission date: </label>
											
											<div class="col-lg-8">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Admission Date" value="'.date('Y-m-d').'">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-8 col-md-offset-4">
												<div class="center-align">
													<button type="submit" class="btn btn-primary">Create inpatient</button>
												</div>
											</div>
										</div>
										'.form_close().'
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				
					
					

					';
				}
				else if($is_nurse)
				{
					if($sent_to == "Traige" || $sent_to == "General Practice")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
				

					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">TRIAGE</a></td>
					<td>
						<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$visit_id.'">Inpatient</button>
						
						<div class="modal fade" id="create_inpatient'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Change to inpatient</h4>
									</div>
									<div class="modal-body">
										'.form_open('reception/change_patient_visit/'.$visit_id.'/'.$visit_type_id, array("class" => "form-horizontal")).'
										<div class="form-group">
											<label class="col-md-4 control-label">Ward: </label>
											
											<div class="col-md-8">
												<select name="ward_id" id="ward_id" class="form-control" >
													<option value="">----Select a ward----</option>
													'.$all_wards.'
												</select>
											</div>
										</div>

										

										<div class="form-group">
											<label class="col-md-4 control-label">Doctor: </label>
											
											<div class="col-md-8">
												 <select name="personnel_id" id="personnel_id" class="form-control custom-select">
													<option value="">----Select a Doctor----</option>
													'.$all_doctors.'
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Admission date: </label>
											
											<div class="col-lg-8">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Admission Date" value="'.date('Y-m-d').'">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-8 col-md-offset-4">
												<div class="center-align">
													<button type="submit" class="btn btn-primary">Create inpatient</button>
												</div>
											</div>
										</div>
										'.form_close().'
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
					<td><a href="'.site_url().'patient-bill/'.$patient_id.'" class="btn btn-sm btn-primary" >Bill</a></td>
					
					

					';
				}
				else if($department_id == 11)
				{
					if($sent_to == "Dialysis")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
							<td><a href="'.site_url().'patient-bil/'.$patient_id.'" class="btn btn-sm btn-primary">Dialysis</a></td>		

					';
				}else if($department_id == 4)
				{
					if($sent_to == "Optical")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
							<td><a href="'.site_url().'optical/'.$visit_id.'" class="btn btn-sm btn-primary">Optical</a></td>
					

					';
				}else if($is_labtech)
				{
					if($sent_to == "Laboratory")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
				

					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-info">Tests</a></td>
					';
				}else if($is_pharmacy)
				{
					if($sent_to == "Pharmacy")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$close_page = 0;
					// $department_id = 5;
					if($hold_card == 0)
					{
						$current_time = strtotime(date('h:i:s a'));
						
						$buttons = '
									<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
									
									<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Prescription</a></td>
									<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
									';
					}
					else
					{
						$buttons ='<td colspan="4">This card is held</td>';
					}
					
				}else if($is_accounts)
				{
					if($sent_to == "Accounts")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$close_page = 0;
					// $department_id = 6;
					$buttons = '
					<td><a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-success">Invoice </a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>
					';
				}else if($is_dentist)
				{
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
							<td><a href="'.site_url().'dental/'.$visit_id.'" class="btn btn-sm btn-primary">Dental</a></td>
				
					
					

					';


				}else if($is_admin or $user_id == 0)
				{
					// var_dump($personnel_id); die();
					// $department_id = 2;

					$close_page = 0;

					$buttons = '

					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-success">Card</a></td>
					<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Pharm</a></td>
					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-danger">Tests</a></td>
					<td><a href="'.site_url().'dental/'.$visit_id.'" class="btn btn-sm btn-warning">Dental</a></td>
					<td><a href="'.site_url().'care-units/'.$visit_id.'" class="btn btn-sm btn-default">Theatre</a></td>
					<td><a href="'.site_url().'xray/'.$visit_id.'" class="btn btn-sm btn-primary"> Xray</a></td>
					<td><a href="'.site_url().'accounts/payments/'.$patient_id.'" class="btn btn-sm btn-primary" >Payments</a></td>				
					<td><a href="'.site_url().'reception/edit_visit/'.$visit_id.'" class="btn btn-sm btn-success fa fa-pencil"> </a></td>
					<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-sm btn-primary">Uploads</a></td>
					<td><a href="'.site_url().'reception/unhold_card/'.$visit_id.'" class="btn btn-sm btn-danger fa fa-refresh" onclick="return confirm(\'Do you really want to unhold this card?\');"></a></td>
					';
					
				}else if($is_front_office)
				{
					// var_dump($personnel_id); die();
					// $department_id = 0;
					if($sent_to == "Reception")
					{
						$balanced = 'warning';
					}
					else
					{
						$balanced = 'default';
					}
					$buttons = '
					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-success">Card</a></td>
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td><a href="'.site_url().'reception/edit_visit/'.$visit_id.'" class="btn btn-sm btn-success fa fa-pencil"> </a></td>
					<td>
						<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#create_inpatient'.$visit_id.'">Inpatient</button>
						
						<div class="modal fade" id="create_inpatient'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Change to inpatient</h4>
									</div>
									<div class="modal-body">
										'.form_open('reception/change_patient_visit/'.$visit_id.'/'.$visit_type_id, array("class" => "form-horizontal")).'
										<div class="form-group">
											<label class="col-md-4 control-label">Ward: </label>
											
											<div class="col-md-8">
												<select name="ward_id" id="ward_id" class="form-control" >
													<option value="">----Select a ward----</option>
													'.$all_wards.'
												</select>
											</div>
										</div>

										

										<div class="form-group">
											<label class="col-md-4 control-label">Doctor: </label>
											
											<div class="col-md-8">
												 <select name="personnel_id" id="personnel_id" class="form-control custom-select">
													<option value="">----Select a Doctor----</option>
													'.$all_doctors.'
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Admission date: </label>
											
											<div class="col-lg-8">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
													<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Admission Date" value="'.date('Y-m-d').'">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-8 col-md-offset-4">
												<div class="center-align">
													<button type="submit" class="btn btn-primary">Create inpatient</button>
												</div>
											</div>
										</div>
										'.form_close().'
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
					<td><a href="'.site_url().'patient-bill/'.$patient_id.'" class="btn btn-sm btn-primary" >Bill</a></td>';
					
				}
			
				if(empty($doctor_id))
				{

					$balanced = 'danger';
				}
				else
				{
					$balanced = $balanced;
				}	


				if($date_checked != date('Y-m-d'))
				{
					$balanced = 'success';
				}	


				if($revisit == 1)
				{
					$revisit_status = 'Revisit';
				}
				else
				{
					$revisit_status = 'New Patient';
				}
					
				$result .= 
					'
						<tr >
							<td class="'.$balanced.'">'.$count.'</td>
							<td class="'.$balanced.'">'.$visit_date.' '.$time_start.'</td>
							<td class="'.$balanced.'">'.$patient_number.'</td>
							<td class="'.$balanced.'">'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$visit_type_name.'</td>
							<td>'.$department_name.'</td>
							<td>'.$coming_from.'</td>
							<td>'.$sent_to.'</td>
							<td>'.$revisit_status.'</td>
						    <td>'.$doctor.'</td>
							'.$buttons.'
						</tr> 
					';
					
						$pink = 15;
					
					$v_data['patient_type'] = $visit_type_id;
					
					
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
?>
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		echo $result;
		?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>

  <audio id="sound1" src="<?php echo base_url();?>sound/beep.mp3"></audio>
  <script type="text/javascript">
  	$(document).ready(function(){
  	   $("#personnel_id").customselect();
       $("#bed_id").customselect();
       $("#room_id").customselect();
       var department_id = document.getElementById("department_id").value;
       // alert(department_id);
		// setInterval(function(){check_new_patients(department_id)},10000);

	 });

  
   	function check_new_patients(module)
		{	
		 var XMLHttpRequestObject = false;
        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		    
		    var config_url = $('#config_url').val();
		    var url = config_url+"nurse/check_queues/"+module;
		    // alert(url);
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	
	         			var one = XMLHttpRequestObject.responseText;
	         			if(one == 1)
	         			{
	         				 var audio1 = document.getElementById("sound1");
						 	 if (audio1.paused !== true){
							    audio1.pause();
							 }
							 else
							 {
								audio1.play();
							 }
	         			}
	         			else
	         			{

	         			}
			         	
	         
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
  </script>

  <script type="text/javascript">
	
	$(document).on("change","select#ward_id",function(e)
	{
		var ward_id = $(this).val();
		
		var url = "<?php echo site_url();?>nurse/get_ward_rooms/"+ward_id;
		// alert(url);
		//get rooms
		$.get( url , function( data ) 
		{
			$( "#room_id" ).html( data );
			
			$.get( "<?php echo site_url();?>nurse/get_room_beds/0", function( data ) 
			{
				$( "#bed_id" ).html( data );
			});
		});
	});
	
	$(document).on("change","select#room_id",function(e)
	{
		var room_id = $(this).val();
		
		//get beds
		$.get( "<?php echo site_url();?>nurse/get_room_beds/"+room_id, function( data ) 
		{
			$( "#bed_id" ).html( data );
		});
	});
</script>

 