		<style type="text/css">
			#insured_company{display:none;}
		</style>
		<section class="panel panel-warning">
                <div class="panel-title">
                    <header class="panel-heading ">
                        <strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Account Bal: </strong> Kes <?php echo number_format($account_balance, 2);?>
                        <a href="<?php echo site_url();?>administration/individual_statement/<?php echo $patient_id;?>/2" class="btn btn-sm btn-success " target="_blank" style="margin-top: 0px;"> Patient Statement</a>
                        <div class="pull-right">
                              <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right fa fa-arrow-left" style="margin-top:0px">  Back to patient list</a>
                        </div>
                    </header>

                </div>
                
		        <!-- Widget content -->
		        <div class="panel-body">
                	
                    
					<?php 
                        $validation_error = validation_errors();
                        
                        if(!empty($validation_error))
                        {
                            echo '<div class="alert alert-danger center-align">'.$validation_error.'</div>';
                        }
						
						$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						
						if(!empty($error))
						{
							echo '<div class="alert alert-danger">'.$error.'</div>';
							$this->session->unset_userdata('error_message');
						}
						
						if(!empty($success))
						{
							echo '<div class="alert alert-success">'.$success.'</div>';
							$this->session->unset_userdata('success_message');
						}
                    ?>
                    
					<div class="tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a class="text-center" data-toggle="tab" href="#outpatient">Outpatient</a>
                            </li>
                            <li>
                                <a class="text-center" data-toggle="tab" href="#inpatient">Inpatient</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="outpatient">
                                <h4 class="center-align" style="margin-bottom:10px;">Initiate outpatient visit</h4>
                                <?php $this->load->view('visit/initiate_outpatient');?>
                            </div>
                            <div class="tab-pane" id="inpatient">
                                <h4 class="center-align" style="margin-bottom:10px;">Initiate inpatient visit</h4>
                                <?php $this->load->view('visit/initiate_inpatient');?>
                            </div>
                        </div>
                    </div>
                    
                </div>
        	</section>