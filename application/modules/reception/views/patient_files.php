<?php echo $this->load->view('patients/search_patients_files', '', TRUE);?>
<section class="panel panel-info">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
        <div class="pull-right">
	         
	    </div>
    </header>

        <!-- Widget content -->
        <div class="panel-body">
          <div class="padd">
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
				
		$search = $this->session->userdata('visit_search');
		
		if(!empty($search))
		{
			echo '
			<a href="'.site_url().'reception/close_patient_files_search" class="btn btn-warning btn-sm ">Close Search</a>
			';
		}
		
		
		
		$result = '';
		
		$personnel_id = $this->session->userdata('personnel_id');
		$doctors = $this->reception_model->get_doctor();

		// var_dump($doctors);die();
		$is_front_office = $this->reception_model->check_personnel_department_id($personnel_id,9);
		$is_doctor = $this->reception_model->check_personnel_department_id($personnel_id,2);
		$is_admin = $this->reception_model->check_personnel_department_id($personnel_id,1);
		$is_physician = $this->reception_model->check_personnel_department_id($personnel_id,22);
		

     

		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			
			$result .= 
			'	
				<table class="table table-condensed table-striped table-bordered ">
				  <thead>
					<tr>
					  <th>#</th>
					  <th>Patient Number</th>
					  <th>First name</th>
					  <th>Other Names</th>
					  <th>Age</th>
					  <th>Contact details</th>
					  <th>Last Visit</th>
					  <th>FORM D1 Status</th>
					  <th colspan="5">Actions</th>
					</tr>
				  </thead>
				  <tbody>
			';
			
			
			$personnel_query = $this->accounts_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{

				$patient_id = $row->patient_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$deleted_by = $row->deleted_by;
				$visit_type_id = $row->visit_type_id;
				$created = $row->patient_date;
				$last_modified = $row->last_modified;
				$last_visit = $row->last_visit;
				$patient_phone1 = $row->patient_phone1;
				$patient_number = $row->patient_number;
				$rip_status = $row->rip_status;
				$rip_date = $row->rip_date;
				$rip_report = $row->rip_report;
				$rip_doctor_id = $row->rip_doctor_id;
				$current_patient_number = $row->current_patient_number;
				// $patient = $this->reception_model->patient_names2($patient_id);
				// $patient_type = $patient['patient_type'];
				// $patient_othernames = $patient['patient_othernames'];
				// $patient_surname = $patient['patient_surname'];
				// $patient_type_id = $patient['visit_type_id'];
				// $account_balance = $patient['account_balance'];
				if($last_visit != NULL)
				{
					$last_visit = date('jS M Y',strtotime($last_visit));
				}
				
				else
				{
					$last_visit = '';
				}

				$patient_type = $row->patient_type;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$gender = $row->gender;
				

				if(!empty($patient_date_of_birth))
				{
					$patient_age = $this->reception_model->calculate_age($patient_date_of_birth);
				}
				else
				{
					$patient_age = '';
				}
				if($rip_doctor_id > 0)
				{
					 $rip_doctor =  '';
				}
				
				$options = '<option >----Select Doctor ---</option>';

				 if(count($doctors) > 0){
		            foreach($doctors as $row):
		                $fname = $row->personnel_fname;
		                $onames = $row->personnel_onames;
		                $personnel_id = $row->personnel_id;

		                if($rip_doctor_id == $personnel_id)
		                {
		                	    $options .='<option value="'.$personnel_id.'" selected="selected">Dr. '.$onames.'</option>';
		                	       $rip_doctor = 'Dr. '.$onames;
		                }
		                else
		                {
		                	    $options .='<option value="'.$personnel_id.'">Dr. '.$onames.'</option>';
		                }

		             
		           
		            endforeach;
		        }


		        if(empty($rip_date))
		        {
		        	$rip_date = date('Y-m-d');
		        }
				
				if(($is_front_office OR $is_physician OR $is_doctor OR $is_admin OR $authorize_invoice_changes))
				{
					$buttons = '
								
									<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#create_inpatient'.$patient_id.'">RIP</button>
								
									<div class="modal fade" id="create_inpatient'.$patient_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Change '.$patient_othernames.' '.$patient_surname.' '.$patient_number.' STATUS TO RIP</h4>
												</div>
												'.form_open('reception/change_patient_status/'.$patient_id, array("class" => "form-horizontal")).'

												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-lg-4 control-label">RIP DATE: </label>
																	<input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="'.$this->uri->uri_string().'">
																	<div class="col-lg-8">
																		<div class="input-group">
																			<span class="input-group-addon">
																				<i class="fa fa-calendar"></i>
																			</span>
																			<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="rip_date" placeholder="RIP DATE" value="'.$rip_date.'" >
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
							                                        <label class="col-lg-4 control-label">Schedule: </label>
							                                        
							                                        <div class="col-lg-8">
						                                                <select name="doctor_id" id="doctor_id"  class="form-control" >
						                                                   '.$options.'
						                                                </select>
							                                         
							                                      
							                                        </div>
							                                    </div>

															</div>
														</div>

													</div>
													
													<br>
													<div class="row">
														<div class="col-md-12">
															<h4>OTHER INFORMATION<h4>
														</div>
														<div class="col-md-12">
															<textarea class="form-control cleditor" autocomplete="off" name="rip_report"> '.$rip_report.'</textarea>
														</div>
													</div>
												
												</div>
												<div class="modal-footer">
													<button  class="btn btn-sm btn-primary" type="submit">MARK PATIENT AS DECEASED</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												</div>

												'.form_close().'
											</div>
										</div>
									</div>
								';

					
				}
				else
				{
					$buttons = '';
				}


				if($rip_status == 1)
				{
					$other_list = '<td>'.date('jS M Y',strtotime($rip_date)).' - '.$rip_doctor.'</td>';
					$color = 'danger';
				}
				else
				{
					$other_list = '<td></td>';
					$color = '';
				}
				
				$count++;
				
			
			
					$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td class="'.$color.'">'.$patient_number.' </td>
							<td class="'.$color.'">'.$patient_surname.' </td>
							<td class="'.$color.'">'.$patient_othernames.'</td>
							<td class="'.$color.'">'.$patient_age.'</td>
							<td class="'.$color.'">'.$patient_phone1.'</td>
							<td>'.$last_visit.'</td>
							'.$other_list.'
							<td><a href="'.site_url().'patient-cards/'.$patient_id.'" class="btn btn-sm btn-primary">Patient Cards</a>'.$buttons.'</td>
							

						</tr> 
					';
				
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients visit";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->

      </div>
    </section>