
<?php 
    echo form_open("reception/register_other_patient", array("class" => "form-horizontal"));
    form_hidden('visit_type_id', 3);
    if(isset($dependant_parent))
    {
        form_hidden('dependant_id', $dependant_parent);
    }
    
    else
    {
        form_hidden('dependant_id', 0);
    }

?>
<?php
/**
 * Simple PHP age Calculator
 * 
 * Calculate and returns age based on the date provided by the user.
 * @param   date of birth('Format:yyyy-mm-dd').
 * @return  age based on date of birth
 */
function ageCalculator($patient_date_of_birth){

    if(!empty($patient_date_of_birth)){
        $birthdate = new DateTime($patient_date_of_birth);
        $today   = new DateTime('today');
        $patient_age = $birthdate->diff($today)->y;
        return $patient_age;
    }else{
        return 0;
    }
}
//$dob = '1992-03-18';
//echo ageCalculator($dob);
?>
 <section class="panel">
    <header class="panel-heading">
          <h5 class="pull-left"><i class="icon-reorder"></i>Add Patient</h5>
          <div class="widget-icons pull-right">
               <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right"><i class="fa fa-arrow-left"></i>  Patients List</a>

          </div>
          <div class="clearfix"></div>
    </header>
      <div class="panel-body">
        <div class="padd">
          <div class="row">
            <div class="col-md-6">
               

            

                
                 
         <div class="form-group" style="display: none">
            <label class="col-lg-4 control-label">Title: </label>
            
            <div class="col-lg-8">
                <select class="form-control" name="title_id">
                    <?php
                        if($titles->num_rows() > 0)
                        {
                            $title = $titles->result();
                            
                            foreach($title as $res)
                            {
                                $db_title_id = $res->title_id;
                                $title_name = $res->title_name;
                                
                                if($db_title_id == $title_id)
                                {
                                    echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
                                }
                            }
                        }
                    ?>
                </select>
            </div>
        </div>

        
                
                <div class="form-group">
                    <label class="col-md-4 control-label">First name: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_surname" placeholder="First name" value="<?php echo set_value('patient_surname');?>">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label">Other Names: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_othernames" placeholder="Other Names">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label">Gender: </label>
                    
                    <div class="col-md-8">
                        <select class="form-control" name="gender_id">
                            <?php
                                if($genders->num_rows() > 0)
                                {
                                    $gender = $genders->result();
                                    
                                    foreach($gender as $res)
                                    {
                                        $gender_id = $res->gender_id;
                                        $gender_name = $res->gender_name;
                                        
                                        if($gender_id == set_value("gender_id"))
                                        {
                                            echo '<option value="'.$gender_id.'" selected>'.$gender_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$gender_id.'">'.$gender_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
              <div class="form-group" style="display: none">
                  <label class="col-lg-4 control-label">Religion: </label>
            
                    <div class="col-lg-8">
                        <select class="form-control" name="religion_id">
                            <?php
                                if($religions->num_rows() > 0)
                                {
                                    $religion = $religions->result();
                                    
                                    foreach($religion as $res)
                                    {
                                        $religion_id = $res->religion_id;
                                        $religion_name = $res->religion_name;
                                        
                                        if($religion_id == $religion_id)
                                        {
                                            echo '<option value="'.$religion_id.'" selected>'.$religion_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$religion_id.'">'.$religion_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
               </div>
                    <div class="form-group" style="display: none">
        
            <label class="col-lg-4 control-label">Civil Status: </label>
            
            <div class="col-lg-8">
                <select class="form-control" name="civil_status_id">
                    <?php
                        if($civil_statuses->num_rows() > 0)
                        {
                            $status = $civil_statuses->result();
                            
                            foreach($status as $res)
                            {
                                $status_id = $res->civil_status_id;
                                $status_name = $res->civil_status_name;
                                
                                if($status_id == $civil_status_id)
                                {
                                    echo '<option value="'.$status_id.'" selected>'.$status_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$status_id.'">'.$status_name.'</option>';
                                }
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label">Year: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_year"  placeholder=" Year" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Date of Birth : </label>
            
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" id="date_of_birth" type="text" data-plugin-datepicker class="form-control" name="patient_date_of_birth" placeholder="Date of Birth"> <a onclick="calculate_age()" class="btn btn-success">Get Age</a> <span id="age_value"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <label class="col-md-4 control-label">Age: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_age" id="age_value_state" placeholder=" Age" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">National ID: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_national_id" placeholder="National Id">
                    </div>
                </div>
                
                <div class="form-group" style="display: none">
                    <label class="col-md-4 control-label">Email Address: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_email" placeholder="Email Address">
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-md-4 control-label">Primary Phone: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_phone1" placeholder="Primary Phone">
                    </div>
                </div>
               
                
               
                 <div class="form-group" style="display: none">
                    <label class="col-md-4 control-label">Refferal Facility: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_refferal" placeholder="Refferal Facility">
                    </div>
                </div>
                 <div class="form-group" style="display: none">
                    <label class="col-md-4 control-label">Nationality: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_nationality" placeholder=" Nationality">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Postal Address: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_postalcode" placeholder="Patient Postal Address">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Town: </label>
                    
                    <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_town" placeholder="Town">
                    </div>
                </div>
            
                <div class="form-group">
                    <label class="col-md-4 control-label">Village: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_village" placeholder="Village">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Location: </label>
                        
                      <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_location" placeholder="Location">
                        </div>
                </div>
                
                
                
            </div>
            
            <div class="col-md-6">
                
                
                
                
                
                <div class="form-group">
                <label class="col-md-4 control-label">Sub-Location: </label>
                    
                  <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_district" placeholder="District">
                    </div>
                </div>
              <div class="form-group">
                    <label class="col-md-4 control-label">County: </label>
                    
                    <div class="col-md-8">
                        <select class="form-control" name="county_id">
                            <option value="0">------ select a country ----</option>
                            <?php
                                if($countys->num_rows() > 0)
                                {
                                    $county = $countys->result();
                                    
                                    foreach($county as $res)
                                    {
                                        $county_id = $res->county_id;
                                        $county_name = $res->county_name;
                                        
                                        if($county_id == set_value("county_id"))
                                        {
                                            echo '<option value="'.$county_id.'" selected>'.$county_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$county_id.'">'.$county_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div> 
                   <div class="form-group" style="display: none;">
                    <label class="col-md-4 control-label">Ethnic Group: </label>
                    
                    <div class="col-md-8">
                        <select class="form-control" name="ethnic_id">
                            <?php
                                if($ethnics->num_rows() > 0)
                                {
                                    $ethnic = $ethnics->result();
                                    
                                    foreach($ethnic as $res)
                                    {
                                        $ethnic_id = $res->ethnic_id;
                                        $ethnic_name = $res->ethnic_name;
                                        
                                        if($ethnic_id == set_value("ethnic_id"))
                                        {
                                            echo '<option value="'.$ethnic_id.'" selected>'.$ethnic_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$ethnic_id.'">'.$ethnic_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>   
                 <div class="form-group">
                    <label class="col-md-4 control-label">Occupation: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_occupation" placeholder="Occupation">
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-md-4 control-label">Employeer: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_employeer" placeholder="Employeer">
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-md-4 control-label">Employer Address: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_employeer_address" placeholder="Employeer Address">
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-md-4 control-label">Employeer Phone no: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_employeer_phone" placeholder="Employeer Phone">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Next of Kin name: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="patient_kin_sname" placeholder="Next of kin name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Next of Kin Contact: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="next_of_kin_contact" placeholder="">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label">Relationship To Kin: </label>
                    
                    <div class="col-md-8">
                        <select class="form-control" name="relationship_id">
                            <option value="0">------ select a country ----</option>
                            <?php
                                if($relationships->num_rows() > 0)
                                {
                                    $relationship = $relationships->result();
                                    
                                    foreach($relationship as $res)
                                    {
                                        $relationship_id = $res->relationship_id;
                                        $relationship_name = $res->relationship_name;
                                        
                                        if($relationship_id == set_value("relationship_id"))
                                        {
                                            echo '<option value="'.$relationship_id.'" selected>'.$relationship_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$relationship_id.'">'.$relationship_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">NHIF Patients ? </label>
                    <div class="col-lg-4">
                        <div class="radio">
                            <label>
                                <input id="optionsRadios2" type="radio" name="nhif_status" value="0" checked="checked" >
                                No
                            </label>
                        </div>
                    </div>
                    
                    <div class="col-lg-4">
                        <div class="radio">
                            <label>
                                <input id="optionsRadios2" type="radio" name="nhif_status" value="1" >
                                Yes
                            </label>
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-md-4 control-label">Dependants Contibutor (NHIF/AON etc): </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contibutor_nhif" placeholder="Contibutor">
                    </div>
                </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Dependants Contibutor Contact: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contibutor_contact" placeholder="Contibutor Contact">
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-md-4 control-label">Insurance : </label>
                    
                    <div class="col-md-8">
                        <select class="form-control" name="insurance_company_id">
                            <option value="0">Select an insurance Company</option>
                            <?php
                                if($insurance->num_rows() > 0)
                                {
                                    $insurance = $insurance->result();
                                    
                                    foreach($insurance as $res)
                                    {
                                        $insurance_company_id1 = $res->insurance_company_id;
                                        $insurance_company_name = $res->insurance_company_name;
                                        
                                        if($insurance_company_id1 ==  set_value("relationship_id"))
                                        {
                                            echo '<option value="'.$insurance_company_id.'" selected>'.$insurance_company_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$insurance_company_id.'">'.$insurance_company_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>-->
                
            </div>
        </div>
       
            <br/>
        <div class="center-align">
            <button class="btn btn-info btn-sm" type="submit">Add Patient</button>
        </div>
        </div>
    </div>
</section>
<?php echo form_close();

?>

<script type="text/javascript">
    function calculate_age()
    {
        var dateString =  $('#date_of_birth').val();

        var dateString = new Date(dateString);
        var month = dateString.getMonth() + 1;
       

       if(month < 10)
       {
            month  = "0"+month;
       }

       var day = dateString.getDate();
       

       if(day < 10)
       {
            day  = "0"+day;
       }

        var dateString = day + '/' + month + '/' +  dateString.getFullYear();
        var now = new Date();
          var today = new Date(now.getYear(),now.getMonth(),now.getDate());

          var yearNow = now.getYear();
          var monthNow = now.getMonth();
          var dateNow = now.getDate();

          var dob = new Date(dateString.substring(6,10),
                             dateString.substring(0,2)-1,                   
                             dateString.substring(3,5)                  
                             );

          var yearDob = dob.getYear();
          var monthDob = dob.getMonth();
          var dateDob = dob.getDate();
          var age = {};
          var ageString = "";
          var yearString = "";
          var monthString = "";
          var dayString = "";


          yearAge = yearNow - yearDob;

          if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
          else {
            yearAge--;
            var monthAge = 12 + monthNow -monthDob;
          }

          if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
          else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
              monthAge = 11;
              yearAge--;
            }
          }

          age = {
              years: yearAge,
              months: monthAge,
              days: dateAge
              };

          if ( age.years > 1 ) yearString = " years";
          else yearString = " year";
          if ( age.months> 1 ) monthString = " months";
          else monthString = " month";
          if ( age.days > 1 ) dayString = " days";
          else dayString = " day";



          if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
            ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
          else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
            ageString = "Only " + age.days + dayString + " old!";
          else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
            ageString = age.years + yearString + " old. Happy Birthday!!";
          else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
            ageString = age.years + yearString + " and " + age.months + monthString + " old.";
          else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
            ageString = age.months + monthString + " and " + age.days + dayString + " old.";
          else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
            ageString = age.years + yearString + " and " + age.days + dayString + " old.";
          else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
            ageString = age.months + monthString + " old.";
          else ageString = "Oops! Could not calculate age!";

        document.getElementById('age_value').value = ageString;
         document.getElementById('age_value_state').value = ageString;
    }
</script>