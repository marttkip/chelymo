<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/auth/controllers/auth.php";
// require_once "./application/libraries/africastalkinggateway.php";
ini_set('MAX_EXECUTION_TIME', -1);
ini_set('max_execution_time',0);
set_time_limit(0);
error_reporting(0);
date_default_timezone_set('Africa/Nairobi');
class Reception  extends MX_Controller
{	
	var $csv_path;
	var $document_upload_path;
	var $document_upload_location;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception_model');
		$this->load->model('strathmore_population');
		$this->load->model('database');
		$this->load->model('administration/reports_model');
		$this->load->model('administration/administration_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('messaging/messaging_model');
		$this->load->model('administration/sync_model');
		$this->load->model('online_diary/rooms_model');
		$this->load->model('admin/file_model');


		$this->load->library('image_lib');
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->document_upload_path = realpath(APPPATH . '../assets/patient_scans');
		$this->document_upload_location = base_url().'assets/patient_scans/';
		
		$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		$this->session->unset_userdata('visit_search');
		$this->session->unset_userdata('patient_search');
		
		$where = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type AND visit.branch_code = \''.$this->session->userdata('branch_code').'\' AND visit.visit_date = \''.date('Y-m-d').'\'';
		
		$table = 'visit, patients, visit_type';
		$query = $this->reception_model->get_all_ongoing_visits2($table, $where, 10, 0);
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		
		$v_data['visit'] = 0;
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('reception_dashboard', $v_data, TRUE);
		
		$data['title'] = 'Dashboard';
		$data['sidebar'] = 'reception_sidebar';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	/*
	* Code for displaying deleted patients removed
	*/
	public function patients()
	{
		$delete = 0;
		$segment = 3;
		
		$patient_search = $this->session->userdata('patient_search');
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND patient_delete = '.$delete;
		$where = 'patient_type = 0 AND patient_delete = '.$delete;


		if(!empty($patient_search))
		{
			$where .= $patient_search;
		}
		
		else
		{
			// $where .= ' AND patients.branch_code = \''.$this->session->userdata('branch_code').'\'';
		}

		// var_dump($where);die();
		
		$table = 'patients';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reception/patients';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_patients($table, $where, $config["per_page"], $page);
		
		if($delete == 1)
		{
			$data['title'] = 'Deleted Patients';
			$v_data['title'] = 'Deleted Patients';
		}
		
		else
		{
			$search_title = $this->session->userdata('patient_search_title');
			
			if(!empty($search_title))
			{
				$data['title'] = $v_data['title'] = 'Patients filtered by :'.$search_title;
			}
			
			else
			{
				$data['title'] = 'Patients';
				$v_data['title'] = 'Patients';
			}
		}
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['delete'] = $delete;
		$v_data['doctors'] = $this->reception_model->get_all_personnel_doctors();

		// var_dump($v_data['doctors']); die();
		$v_data['branches'] = $this->reception_model->get_branches();
		$data['content'] = $this->load->view('all_patients', $v_data, true);
		
		$data['sidebar'] = 'reception_sidebar';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	/*
	*
	*	$visits = 0 :ongoing visits of the current day
	*	$visits = 1 :terminated visits
	*	$visits = 2 :deleted visits
	*	$visits = 3 :all other ongoing visits
	*
	*/
	public function visit_list()
	{
		
		$delete = 0;
		$segment = 3;
		
		$visit_search = $this->session->userdata('visit_search');
		$where = 'patient_delete = 0 ';
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		else
		{

		}
		
		$table = 'patients';
		
		// var_dump($table);die();
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/patients-files';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_patients_files($table, $where, $config["per_page"], $page);
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
			$data['title'] = 'Patient Files';
			$v_data['title'] = 'Patient Files';
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('patient_files', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	
	/*
	*
	*	$visits = 0 :ongoing visits of the current day
	*	$visits = 1 :terminated visits
	*	$visits = 2 :deleted visits
	*	$visits = 3 :all other ongoing visits
	*
	*/
	public function general_queue_old($page_name)
	{
		$segment = 4;
		// AND visit.visit_date = \''.date('Y-m-d').'\'
		$where = 'visit.inpatient = 0 AND visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type';
		
		// if($page_name != 'reception')
		// {
			
		// }
		
		
		
		if($page_name == 'doctor')
		{
			//$where .= ' AND visit.personnel_id = '.$this->session->userdata('personnel_id');
		}
		
		if(($page_name != 'accounts') && ($page_name != 'doctor'))
		{
			// $where .= ' AND visit.branch_code = \''.$this->session->userdata('branch_code').'\'';
		}
		
		if(($page_name == 'laboratory') || ($page_name == 'radiology') || ($page_name == 'pharmacy'))
		{
			$where = 'visit.inpatient = 0 AND visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type';
		}
		$where .= ' AND visit.visit_date = \''.date('Y-m-d').'\'';
		$table = 'visit_department, visit, patients, visit_type';
		
		$visit_search = $this->session->userdata('general_queue_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'reception/general_queue/'.$page_name;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		if($page_name == 'administration')
		{
			$data['title'] = 'All visits';
			$v_data['title'] = 'All visits';
		}
		else
		{
			$data['title'] = 'General Queue';
			$v_data['title'] = 'General Queue';
		}
		
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['doctor'] = $this->reception_model->get_doctor();
		$v_data['page_name'] = $page_name;
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('general_queue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	
	/*
	*	Add a new patient
	*
	*/
	public function add_patient($dependant_staff = NULL)
	{
		$v_data['relationships'] = $this->reception_model->get_relationship();
		$v_data['religions'] = $this->reception_model->get_religion();
		$v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		$v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();
		$v_data['countys'] = $this->reception_model->get_county();
		$v_data['ethnics'] = $this->reception_model->get_ethnic();
		// $v_data['revisits'] = $this->reception_model->get_revisit();
		$v_data['insurance'] = $this->reception_model->get_insurance();
		$v_data['dependant_staff'] = $dependant_staff;
		$data['content'] = $this->load->view('add_patient', $v_data, true);
		
		$data['title'] = 'Add Patients';
		$data['sidebar'] = 'reception_sidebar';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	/*
	*	Add a new patient
	*
	*/
	public function add_other_dependant($dependant_parent)
	{
		$v_data['relationships'] = $this->reception_model->get_relationship();
		$v_data['religions'] = $this->reception_model->get_religion();
		$v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		$v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();
		$v_data['countys'] = $this->reception_model->get_county();
		$v_data['revisits'] = $this->reception_model->get_revisit();
		$v_data['dependant_parent'] = $dependant_parent;
		$patient = $this->reception_model->patient_names2($dependant_parent);
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$data['content'] = $this->load->view('add_other_dependant', $v_data, true);
		
		$data['title'] = 'Add Patients';
		$data['sidebar'] = 'reception_sidebar';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	
	/*
	*	Register other patient
	*
	*/
	public function register_other_patient()
	{
		//form validation rules
		$this->form_validation->set_rules('title_id', 'Title', 'is_numeric|xss_clean');
		$this->form_validation->set_rules('patient_surname', 'Surname', 'xss_clean');
		$this->form_validation->set_rules('patient_othernames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('patient_dob', 'Date of Birth', 'xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'trim|xss_clean');
		$this->form_validation->set_rules('religion_id', 'Religion', 'trim|xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_email', 'Email Address', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_address', 'Postal Address', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_postalcode', 'Postal Code', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_town', 'Town', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_phone1', 'Primary Phone', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_phone2', 'Other Phone', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_kin_sname', 'Next of Kin Surname', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_kin_othernames', 'Next of Kin Other Names', 'trim|xss_clean');
		$this->form_validation->set_rules('relationship_id', 'Relationship With Kin', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_national_id', 'National ID', 'trim|xss_clean');
		$this->form_validation->set_rules('next_of_kin_contact', 'Next of Kin Contact', 'trim|xss_clean');
		
		
		//if form conatins invalid data
		if ($this->form_validation->run() == FALSE)
		{
			$this->add_patient();
		}
		
		else
		{
			$patient_id = $this->reception_model->save_other_patient();
			//echo $patient_id; die();
			if($patient_id != FALSE)
			{
				$this->get_found_patients($patient_id,3);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add patient. Please try again");
				$this->add_patient();	
			}
		}
	}
	
	public function get_found_patients($patient_id,$place_id)
	{
		//  1 for students 2 for staff 3 for others 4 for dependants
		$this->session->set_userdata('patient_search', ' AND patients.patient_id = '.$patient_id);
	
		redirect('patients');
		
		
	}
	
	public function get_department_services($department_id, $selected_service_id = NULL)
	{
		echo '<option value="0">--Select Service--</option>';
		
		$service_charge = $this->reception_model->get_services_per_department($department_id);
		foreach($service_charge AS $key) 
		{
			if($selected_service_id == $key->service_id)
			{
				echo '<option value="'.$key->service_id.'" selected="selected">'.$key->service_name.'</option>';
			}
			
			else
			{
				echo '<option value="'.$key->service_id.'">'.$key->service_name.'</option>';
			}
		}
	}
	
	public function get_services_charges($patient_type_id, $department_id=null, $selected_service_charge_id=null)
	{
		echo '<option value="0">--Select Consultation Charge--</option>';


		$service_charge = $this->reception_model->get_service_charges_per_name($patient_type_id, 'Consultation',$department_id);

		// var_dump($service_charge); die();
		foreach($service_charge AS $key) 
		{ 
			
			echo '<option value="'.$key->service_charge_id.'">'.$key->service_charge_name.' KES. '.$key->service_charge_amount.'</option>';
		}
	}
	
	/*
	*	Add a visit
	*
	*/
	public function set_visit($primary_key)
	{
		$v_data["patient_id"] = $primary_key;
		$v_data['visit_departments'] = $this->reception_model->get_visit_departments();
		$v_data['visit_types'] = $this->reception_model->get_visit_types();
		$v_data['charge'] = $this->reception_model->get_service_charges($primary_key);
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['doctor'] = $this->reception_model->get_doctor();
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['patient_insurance'] = $this->reception_model->get_patient_insurance($primary_key);
		$patient = $this->reception_model->patient_names2($primary_key);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		
		$data['content'] = $this->load->view('visit/initiate_visit', $v_data, true);
		
		$data['title'] = 'Create Visit';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	/*
	*	Add a visit
	*
	*/
	public function edit_visit($visit_id)
	{
		$v_data["visit_id"] = $visit_id;
		$v_data['visit_details'] = $this->reception_model->get_visit($visit_id);
		$v_data['visit_depts'] = $this->reception_model->get_visit_depts($visit_id);
		$v_data['visit_charges'] = $this->reception_model->get_visit_charges($visit_id);
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$visit_date = $this->reception_model->get_visit_date($visit_id);
		$gender = $patient['gender'];
		$visit_date = date('jS M Y',strtotime($visit_date));
		$v_data['age'] = $age;
		$v_data['visit_date'] = $visit_date;
		$v_data['gender'] = $gender;
		
		$v_data['visit_departments'] = $this->reception_model->get_visit_departments();
		$v_data['visit_types'] = $this->reception_model->get_visit_types();
		$v_data['charge'] = $this->reception_model->get_service_charges2($visit_id);
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['doctor'] = $this->reception_model->get_doctor();
		$v_data['type'] = $this->reception_model->get_types();
		
		$data['content'] = $this->load->view('visit/edit_visit', $v_data, true);
		
		$data['title'] = 'Edit Visit';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function save_visit($patient_id)
	{
		$this->form_validation->set_rules('visit_date', 'Visit Date', 'required');
		$this->form_validation->set_rules('department_id', 'Department', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('visit_type_id', 'Visit type', 'required|is_natural_no_zero');
		$visit_type_id = $this->input->post("visit_type_id"); 
		
		if(isset($_POST['department_id'])){
			if(($_POST['department_id'] == 2) || ($_POST['department_id'] == 3) || ($_POST['department_id'] == 4))
			{
				//if nurse visit (7) or theatre (14) service must be selected
				// $this->form_validation->set_rules('personnel_id', 'Doctor', 'is_natural_no_zero');
				$this->form_validation->set_rules('service_charge_name', 'Consultation Type', 'trim|xss_clean');
				$service_charge_id = $this->input->post("service_charge_name");
				$doctor_id = $this->input->post('personnel_id');
			}
			else if($_POST['department_id'] == 12)
			{
				//if nurse visit doctor must be selected
				// $this->form_validation->set_rules('personnel_id2', 'Doctor', 'required|is_natural_no_zero');
				$this->form_validation->set_rules('service_charge_name2', 'Consultation Type', 'trim|xss_clean');
				$service_charge_id = $this->input->post("service_charge_name2");
				$doctor_id = $this->input->post('personnel_id2');
			}
			else 
			{
				$service_charge_id = 0;
				$doctor_id = 0;
			}
		}
		
		if($visit_type_id != 1)
		{
			//$this->form_validation->set_rules('insurance_limit', 'Insurance limit', 'trim|xss_clean');
			//$this->form_validation->set_rules('insurance_number', 'Insurance number', 'trim|xss_clean');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->set_visit($patient_id);
		}
		else
		{
			$insurance_limit = $this->input->post("insurance_limit");
			$insurance_number = $this->input->post("insurance_number");
			
			//$visit_type = $this->get_visit_type($type_name);
			$visit_date = $this->input->post("visit_date");
			$timepicker_start = $this->input->post("timepicker_start");
			$timepicker_end = $this->input->post("timepicker_end");
			
			$appointment_id = $this->input->post("appointment_id");
			if($appointment_id == 1)
			{
				$close_card = 2;
			}
			else
			{		
				$close_card = 0;
			}
			//  check if the student exisit for that day and the close card 0;
			$check_visits = $this->reception_model->check_patient_exist($patient_id, $visit_date);
			$check_count = count($check_visits);
			if($check_count > 0)
			{
				$this->session->set_userdata('error_message', 'Seems like there is another visit that has been initiated');
				redirect('queues/outpatient-queue');
			}
			
			else
			{
				//create visit
				// var_dump($service_charge_id); die();
				$visit_id = $this->reception_model->create_visit($visit_date, $patient_id, $doctor_id, $insurance_limit, $insurance_number, $visit_type_id, $timepicker_start, $timepicker_end, $appointment_id, $close_card);
				// $this->sync_model->sync_patient_bookings($visit_id);
				//save consultation charge for nurse visit, counseling or theatre

				$department_id = $this->input->post('department_id');


				if($_POST['department_id'] == 2 || $_POST['department_id'] == 3 || $_POST['department_id'] == 4)
				{
					if(isset($service_charge_id))
					{
						$this->reception_model->save_visit_consultation_charge($visit_id, $service_charge_id);
					}
					
				}
				if($visit_type_id == 6)
				{
					$this->reception_model->save_copay_consultation($visit_id, 7769);	
				}
				
				//set visit department if not appointment
				if($appointment_id == 0)
				{
					//update patient last visit
					$this->reception_model->set_last_visit_date($patient_id, $visit_date);
					
					

					if($this->reception_model->set_visit_department($visit_id, $department_id, $visit_type_id))
					{
						if($visit_type_id == 1)
						{
								$this->reception_model->set_visit_department($visit_id, 7);
						}
						
						if($appointment_id == 0)
						{
							$this->session->set_userdata('success_message', 'Visit has been initiated');
						}
						else
						{
							$this->session->set_userdata('success_message', 'Appointment has been created');
						}
					}
					else
					{
						$this->session->set_userdata('error_message', 'Internal error. Could not add the visit');
					}
				}
				
				else
				{
					$this->session->set_userdata('success_message', 'Visit has been initiated');
				}
				
				redirect('queues/outpatient-queue');
			}
			
		}
	}
	
	public function save_inpatient_visit($patient_id)
	{
		$this->form_validation->set_rules('visit_date', 'Admission Date', 'required');
		$this->form_validation->set_rules('ward_id', 'Ward', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('visit_type_id', 'Visit type', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('personnel_id', 'Doctor', 'required|is_natural_no_zero');
		// $this->form_validation->set_rules('service_charge_name', 'Consultation charge', 'required|is_natural_no_zero');
		
		$ward_id = $this->input->post("ward_id"); 
		$visit_type_id = $this->input->post("visit_type_id"); 
		$doctor_id = $this->input->post("personnel_id");
		// $service_charge_id = $this->input->post("service_charge_name");
		
		if($visit_type_id != 1)
		{
			//$this->form_validation->set_rules('insurance_limit', 'Insurance limit', 'trim|xss_clean');
			//$this->form_validation->set_rules('insurance_number', 'Insurance number', 'trim|xss_clean');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->set_visit($patient_id);
		}
		else
		{
			$insurance_limit = $this->input->post("insurance_limit");
			$insurance_number = $this->input->post("insurance_number");
			
			$visit_date = $this->input->post("visit_date");
			
			$close_card = 0;
			//  check if the student exisit for that day and the close card 0;
			$check_visits = $this->reception_model->check_inpatient_exist($patient_id, $visit_date);
			$check_count = count($check_visits);
			if($check_count > 0)
			{
				$this->session->set_userdata('error_message', 'Seems like there is another visit that has been initiated');
				redirect('reception/set_visit/'.$patient_id);
			}
			
			else
			{
				//create visit
				$visit_id = $this->reception_model->create_inpatient_visit($visit_date, $patient_id, $doctor_id, $insurance_limit, $insurance_number, $visit_type_id, $close_card, $ward_id);
				
				//save admission fee

				if($visit_id)
				{
					$this->session->set_userdata('success_message', 'Inpatient visit initiated successfully');
				}
				else
				{
						$this->session->set_userdata('error_message', 'Unable to save admission fee');
				}
				// if($this->reception_model->save_admission_fee($visit_type_id, $visit_id))
				// {
					//save consultation fee
					// $this->reception_model->save_visit_consultation_charge($visit_id, $service_charge_id);
					
					
				// }
				
				// else
				// {
				
				// }
				
				redirect('queues/inpatient-queue');
			}
		}
	}
	
	public function update_patient_number()
	{
		$this->db->select('*');
		$this->db->where('patient_id > 0 AND patient_type = 0');
		$query = $this->db->get('patients');

		if($query->num_rows() > 0 )
		{
			$number = 0;
			foreach ($query->result() as $key) {
				# code...
				$patient_number = $key->patient_number;
				$patient_id = $key->patient_id;
				$patient_id = $key->patient_id;
				$year = date('Y');
				$number = '00'.$patient_id;

				$number++;

				$update_array = array('patient_number'=>$number);

				$this->db->where('patient_id',$patient_id);
				$this->db->update('patients',$update_array);

			
			}
		}
	}
	
	public function search_patients()
	{
		$patient_national_id = $this->input->post('patient_national_id');
		$patient_phone_number = $this->input->post('patient_phone_number');
		$branch_code = $this->input->post('branch_code');
		$revisit_id = $this->input->post('revisit_id');
		$search_title = '';
		
		if(!empty($patient_phone_number))
		{
			$search_title .= ' patient number <strong>'.$patient_phone_number.'</strong>';
			$patient_phone_number = ' AND patients.patient_phone1 ="'.$patient_phone_number.'"';
		}
		
		if(!empty($patient_national_id))
		{
			$search_title .= ' I.D. number <strong>'.$patient_national_id.'</strong>';
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($branch_code))
		{
			$search_title .= ' branch code <strong>'.$branch_code.'</strong>';
			$branch_code = ' AND patients.branch_code = \''.$branch_code.'\' ';
		}
		if(!empty($revisit_id))
		{
			$revisit_id = ' AND patients.revisit_id = '.$revisit_id;
		}
		else
		{
			$revisit_id = '';
		}
		
		//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['othernames']))
		{
			$search_title .= ' other names <strong>'.$_POST['othernames'].'</strong>';
			$other_names = explode(" ",$_POST['othernames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.addslashes($other_names[$r]).'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.addslashes($other_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $patient_national_id.$patient_phone_number.$surname.$revisit_id.$branch_code;
		$this->session->set_userdata('patient_search', $search);
		$this->session->set_userdata('patient_search_title', $search_title);
		// var_dump($search); die();
		redirect('patients');
	}
	
	public function search_visits($visits, $page_name = NULL)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$personnel_id = $this->input->post('personnel_id');
		$visit_date = $this->input->post('visit_date');
		$patient_number = $this->input->post('patient_number');
		$patient_national_id = $this->input->post('patient_national_id');
		
		if(!empty($patient_national_id))
		{
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($personnel_id))
		{
			$personnel_id = ' AND visit.personnel_id = '.$personnel_id.' ';
		}
		
		if(!empty($visit_date))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date.'\' ';
		}
		
		//search surname
		$surnames = explode(" ",$_POST['surname']);
		$total = count($surnames);
		
		$count = 1;
		$surname = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
			}
			
			else
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
			}
			$count++;
		}
		$surname .= ') ';
		
		//search other_names
		$other_names = explode(" ",$_POST['othernames']);
		$total = count($other_names);
		
		$count = 1;
		$other_name = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\'';
			}
			
			else
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' AND ';
			}
			$count++;
		}
		$other_name .= ') ';
		
		$search = $visit_type_id.$patient_number.$surname.$other_name.$visit_date.$patient_national_id.$personnel_id;
		$this->session->set_userdata('visit_search', $search);
		
		if($visits == 13)
		{
			$this->appointment_list();
		}
		
		else
		{
			$this->visit_list($visits, $page_name);
		}
	}
	
	function doc_schedule($personnel_id,$date)
	{
		$data = array('personnel_id'=>$personnel_id,'date'=>$date);
		$this->load->view('show_schedule',$data);	
	}
	function load_charges($patient_type){
		
		$v_data['service_charge'] = $this->reception_model->get_service_charges_per_type($patient_type);
		
		$this->load->view('service_charges_pertype',$v_data);	
		
	}
	public function save_initiate_lab($primary_key)
	{
		$this->form_validation->set_rules('patient_type', 'Patient Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('insurance_id', 'Insurance Company', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_insurance_id', 'Patient Insurance Number', 'trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == FALSE)
		{
			$this->initiate_lab($primary_key);
		}
		
		else
		{
			$visit_type_id = $this->input->post("patient_type");
			$patient_insurance_number = $this->input->post("insurance_id");
			$patient_insurance_id = $this->input->post("patient_insurance_id");
			$insert = array(
				"close_card" => 0,
				"patient_id" => $primary_key,
				"visit_type" => $visit_type_id,
				"patient_insurance_id" => $patient_insurance_id,
				"patient_insurance_number" => $patient_insurance_number,
				"visit_date" => date("y-m-d"),
				"nurse_visit"=>1,
				"lab_visit" => 12
			);
			$this->database->insert_entry('visit', $insert);
	
			$this->visit_list(0);
		}
	}
	
	public function save_initiate_pharmacy($patient_id)
	{
		$this->form_validation->set_rules('patient_type', 'Patient Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('insurance_id', 'Insurance Company', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_insurance_id', 'Patient Insurance Number', 'trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == FALSE)
		{
			$this->initiate_pharmacy($primary_key);
		}
		
		else
		{
			$visit_type_id = $this->input->post("patient_type");
			$patient_insurance_number = $this->input->post("insurance_id");
			$patient_insurance_id = $this->input->post("patient_insurance_id");
				$insert = array(
					"close_card" => 0,
					"patient_id" => $patient_id,
					"visit_type" => $visit_type_id,
					"patient_insurance_id" => $patient_insurance_id,
					"patient_insurance_number" => $patient_insurance_number,
					"visit_date" => date("y-m-d"),
					"visit_time" => date("Y-m-d H:i:s"),
					"nurse_visit" => 1,
					"pharmarcy" => 6
				);
			$table = "visit";
			$this->database->insert_entry($table, $insert);
	
			$this->visit_list(0);
		}
	}
	
	public function close_visit_search($visit, $page_name = NULL)
	{
		$this->session->unset_userdata('visit_search');
		
		if($visit == 13)
		{
			redirect('reception/appointments-list');
		}
		
		else
		{
			$this->visit_list($visit, $page_name);
		}
	}
	
	public function close_patient_search($page = NULL)
	{
		
		$this->session->unset_userdata('patient_search');
		$this->session->unset_userdata('patient_staff_search');
		$this->session->unset_userdata('patient_dependants_search');
		$this->session->unset_userdata('patient_student_search');
		redirect('patients');
		
	}
	
	
	public function dependants($patient_id)
	{
		$v_data['dependants_query'] = $this->reception_model->get_all_patient_dependant($patient_id);
		$v_data['patient_query'] = $this->reception_model->get_patient_data($patient_id);
		$v_data['patient_id'] = $patient_id;
		$v_data['relationships'] = $this->reception_model->get_relationship();
		$v_data['religions'] = $this->reception_model->get_religion();
		$v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		$v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();
		$data['content'] = $this->load->view('dependants', $v_data, true);
		
		$data['title'] = 'Dependants';
		$data['sidebar'] = 'reception_sidebar';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function register_dependant($patient_id, $visit_type_id, $staff_no)
	{
		//form validation rules
		$this->form_validation->set_rules('title_id', 'Title', 'is_numeric|xss_clean');
		$this->form_validation->set_rules('patient_surname', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('patient_othernames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('patient_dob', 'Date of Birth', 'trim|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'trim|xss_clean');
		$this->form_validation->set_rules('religion_id', 'Religion', 'trim|xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == FALSE)
		{
			$this->dependants($patient_id);
		}
		
		else
		{
			//add staff dependant
			if($visit_type_id == 2)
			{
				$patient_id = $this->reception_model->save_dependant_patient($staff_no);
			}
			
			else
			{
				$patient_id = $this->reception_model->save_other_dependant_patient($patient_id);
			}
			
			if($patient_id != FALSE)
			{
				//initiate visit for the patient
				$this->session->set_userdata('success_message', 'Patient added successfully');
				$this->get_found_patients($patient_id,3);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not create patient. Please try again');
				$this->dependants($patient_id);
			}
		}
	}
	
	public function end_visit($visit_id, $page = NULL)
	{
		//check if card is held
		if($this->reception_model->is_card_held($visit_id))
		{
			redirect('queues/outpatient-queue');
		}
		
		else
		{
			$data = array(
				"close_card" => 1,
				"visit_time_out" => date('Y-m-d H:i:s')
			);
			$table = "visit";
			$key = $visit_id;
			$this->database->update_entry($table, $data, $key);
			
			//sync data
			redirect('queues/outpatient-queue');
		}
	}

	
	public function appointment_list()
	{
		$where = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND close_card = 2  AND visit.appointment_id = 1 AND visit.visit_date >= "'.date('Y-m-d').'" ';
		
		$table = 'visit, patients';
		$appointment_search = $this->session->userdata('appointment_search');
		// var_dump($appointment_search); die();
		if(!empty($appointment_search))
		{
			$where .= $appointment_search;
		}
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'appointments';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = 2;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$data['title'] = 'Appointment List';
		$v_data['title'] = 'Appointment List';
		$v_data['visit'] = 13;
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits2($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['page_name'] = 'none';
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['rooms'] = $this->rooms_model->all_rooms();
		$v_data['doctors'] = $this->reception_model->get_all_doctors();
		
		$data['content'] = $this->load->view('appointment_list', $v_data, true);
		$data['sidebar'] = 'reception_sidebar';
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	
	public function initiate_visit_appointment_old($visit_id)
	{
		$data = array(
        	"close_card" => 0
    	);
		$table = "visit";
		$key = $visit_id;
		
		$this->database->update_entry($table, $data, $key);
		$this->reception_model->set_visit_department($visit_id, 7);
		
		$this->session->set_userdata('success_message', 'The patient has been added to the queue');
		redirect('queues/outpatient-queue');
	}
	public function initiate_visit_appointment($visit_id,$patient_id)
	{


		$this->form_validation->set_rules('visit_type_id', 'Visit type', 'is_natural_no_zero');
		$visit_type_id = $this->input->post("visit_type_id"); 

		// var_dump($visit_type_id); die();
		
		$data['chcked'] = 0;
		
		if($visit_type_id != 1)
		{
			$this->form_validation->set_rules('insurance_description', 'Insurance limit', 'required');
			$this->form_validation->set_rules('insurance_number', 'Insurance number', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['status'] = 0;
		}
		else
		{
			$insurance_limit = $this->input->post("insurance_limit");
			$insurance_number = $this->input->post("insurance_number");
			$insurance_description = $this->input->post("insurance_description");
			$mcc = $this->input->post("mcc");
			
			$visit_date = $this->reception_model->get_visit_date($visit_id);

			// if($query->num_rows() > 0)
			// {
			// 	foreach ($query->result() as $key => $value) {
			// 		# code...
			// 		$visit_date = $value->visit_date;
			// 	}
			// }
			if($visit_date == date('Y-m-d'))
			{
				//update visit
				$visit_id_new = $this->reception_model->initiate_appointment_visit($insurance_description, $insurance_number,$visit_id,$visit_type_id,$mcc,$patient_id);
				$data['status'] = 1;
				$this->session->set_userdata('success_message', 'You have successfully initiated a visit');

			}
			else
			{

				$data['status'] = 0;
				$this->session->set_userdata('error_message', 'Please update the visit date before you queue the patient');
			}		
			
		}

		
		
		// redirect('queue');
		echo json_encode($data);
	}
	
	function get_appointments()
	{	
		$this->load->model('reports_model');
		//get all appointments
		$appointments_result = $this->reports_model->get_all_appointments();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($appointments_result->num_rows() > 0)
		{
			$result = $appointments_result->result();
			
			foreach($result as $res)
			{
				$visit_date = date('D M d Y',strtotime($res->visit_date)); 
				$time_start = $visit_date.' '.$res->time_start.':00 GMT+0300'; 
				$time_end = $visit_date.' '.$res->time_end.':00 GMT+0300';
				$visit_type_name = $res->visit_type_name.' Appointment';
				$patient_id = $res->patient_id;
				$dependant_id = $res->dependant_id;
				$visit_type = $res->visit_type;
				$patient_othernames = $res->patient_othernames;
				$patient_surname = $res->patient_surname;
				$personnel_fname = $res->personnel_fname;
				$personnel_onames = $res->personnel_onames;
				$visit_id = $res->visit_id;
				$strath_no = $res->strath_no;
				$patient_data = $patient_surname.' '.$patient_othernames.' to see Dr. '.$personnel_onames;
				//$color = $this->reception_model->random_color();
				$color = '#0088CC';
				
				$data['title'][$r] = $patient_data;
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_start;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = FALSE;
				$data['url'][$r] = site_url().'reception/search_appointment/'.$visit_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}
	
	public function search_patient_appointments($visits, $page_name = NULL)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$personnel_id = $this->input->post('personnel_id');
		$visit_date = $this->input->post('visit_date');
		$patient_number = $this->input->post('patient_number');
		$patient_national_id = $this->input->post('patient_national_id');
		
		if(!empty($patient_national_id))
		{
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($personnel_id))
		{
			$personnel_id = ' AND visit.personnel_id = '.$personnel_id.' ';
		}
		
		if(!empty($visit_date))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date.'\' ';
		}
		//search surname
		$surnames = explode(" ",$_POST['surname']);
		$total = count($surnames);
		

		if(!empty($surnames))
		{
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\'';
				}
				
				else
				{
					$surname .= ' patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
			//search other_names
		$other_names = explode(" ",$_POST['othernames']);
		$total = count($other_names);
		
		if(!empty($other_names))
		{

			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.addslashes($other_names[$r]).'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.addslashes($other_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';

		}

		$search = $visit_type_id.$patient_number.$surname.$other_name.$visit_date.$patient_national_id.$personnel_id;
		$this->session->set_userdata('appointment_search', $search);
		
		redirect('appointments');
	}

	function search_appointment($visit_id)
	{
		if($visit_id > 0)
		{
			$search = ' AND visit.visit_id = '.$visit_id;
			$this->session->set_userdata('visit_search', $search);
		}
		
		redirect('reception/appointment_list');
	}
	
	public function delete_patient($patient_id, $page)
	{
		if($this->reception_model->delete_patient($patient_id))
		{
			$this->session->set_userdata('success_message', 'The patient has been deleted successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Could not delete patient. Please <a href="'.site_url().'reception/delete_patient/'.$patient_id.'">try again</a>');
		}
		
		if($page == 1)
		{
			redirect('reception/patients');
		}
		
		if($page == 2)
		{
			redirect('reception/staff');
		}
		
		if($page == 3)
		{
			redirect('reception/staff_dependants');
		}
		
		if($page == 4)
		{
			redirect('reception/students');
		}
	}
	
	public function delete_visit($visit_id, $visit)
	{

		// $data = array(
		// 		"close_card" => 1,
		// 		"visit_time_out" => date('Y-m-d H:i:s')
		// 	);
		// $table = "visit";
		// $key = $visit_id;
		// if($this->database->update_entry($table, $data, $key))
		// {
		// 	$this->session->set_userdata('success_message', 'The visit has been deleted successfully');
		// }
		// else
		// {
		// 	$this->session->set_userdata('error_message', 'Sorry please try again');
		// }
		// if($this->reception_model->delete_visit($visit_id))
		// {
		// 	$this->session->set_userdata('success_message', 'The visit has been deleted successfully');
		// }

		
		
		// else
		// {
		// 	$this->session->set_userdata('error_message', 'Could not delete visit. Please <a href="'.site_url().'reception/delete_patient/'.$patient_id.'">try again</a>');
		// }
		
		// redirect('reception/visit_list/'.$visit);
		redirect('queues/outpatient-queue');
	}
	
	public function change_patient_type($patient_id)
	{
		//form validation rules
		$this->form_validation->set_rules('visit_type_id', 'Visit Type', 'required|is_numeric|xss_clean');
		$this->form_validation->set_rules('strath_no', 'Staff/Student ID No.', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->reception_model->change_patient_type($patient_id))
			{
				$this->session->set_userdata('success_message', 'Patient type updated successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to update patient type. Please try again');
			}
			
			if($this->input->post('visit_type_id') == 1)
			{
				// this is a student
				redirect('reception/students');
			}
			else
			{
				redirect('reception/staff');
			}
			
		}
		
		$v_data['patient'] = $this->reception_model->patient_names2($patient_id);
		$data['content'] = $this->load->view('change_patient_type', $v_data, true);
		$data['sidebar'] = 'reception_sidebar';
		$data['title'] = 'Change Patient Type';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function change_patient_to_others($patient_id,$visit_type_idd)
	{
		
		if($this->reception_model->change_patient_type_to_others($patient_id,$visit_type_idd))
		{
			$this->session->set_userdata('success_message', 'Patient type updated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Unable to update patient type. Please try again');
		}
		
		redirect('patients');
	}
	
	/*
	*	Edit other patient
	*
	*/
	public function edit_patient($patient_id)
	{
		//form validation rules
		$this->form_validation->set_rules('title_id', 'Title', 'is_numeric|xss_clean');
		$this->form_validation->set_rules('patient_surname', 'Surname', 'xss_clean');
		$this->form_validation->set_rules('patient_othernames', 'Other Names', 'xss_clean');
		$this->form_validation->set_rules('patient_dob', 'Date of Birth', 'xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'xss_clean');
		$this->form_validation->set_rules('religion_id', 'Religion', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'xss_clean');
		$this->form_validation->set_rules('patient_email', 'Email Address', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_address', 'Postal Address', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_postalcode', 'Postal Code', 'trim|xss_clean');
		$this->form_validation->set_rules('county_id', 'County', 'is_numeric|xss_clean');
		$this->form_validation->set_rules('patient_town', 'Town', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_phone1', 'Primary Phone', 'xss_clean');
		$this->form_validation->set_rules('patient_phone2', 'Other Phone', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_location', 'Location', 'xss_clean');
		$this->form_validation->set_rules('patient_refferal', 'Reffaral facility', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_occupation', 'Occupation', 'xss_clean');
		$this->form_validation->set_rules('patient_village', 'Village', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_district', 'District', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_kin_sname', 'Next of Kin Surname', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_kin_othernames', 'Next of Kin Other Names', 'trim|xss_clean');
		$this->form_validation->set_rules('relationship_id', 'Relationship With Kin', 'xss_clean');
		$this->form_validation->set_rules('revisit_id', 'Revisit', 'xss_clean');
		$this->form_validation->set_rules('patient_national_id', 'National ID', 'trim|xss_clean');
		$this->form_validation->set_rules('next_of_kin_contact', 'Next of Kin Contact', 'xss_clean');
		$this->form_validation->set_rules('patient_national_id', 'National ID', 'trim|xss_clean');
		$this->form_validation->set_rules('contibutor_contact', 'Contributor Contact', 'trim|xss_clean');
		// $age = $this->reception_model->calculate_age($patient_date_of_birth);

		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->reception_model->edit_other_patient($patient_id))
			{
				$this->session->set_userdata("success_message","Patient edited successfully");
				redirect('reception/patients');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add patient. Please try again");
			}
		}
		
		$v_data['relationships'] = $this->reception_model->get_relationship();
		$v_data['religions'] = $this->reception_model->get_religion();
		$v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		$v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();
		$v_data['countys'] = $this->reception_model->get_county();
		$v_data['ethnics'] = $this->reception_model->get_ethnic();
		$v_data['revisits'] = $this->reception_model->get_revisit();
		$v_data['patient'] = $this->reception_model->get_patient_data($patient_id);
		$v_data['insurance'] = $this->reception_model->get_insurance();
		$data['content'] = $this->load->view('patients/edit_other_patient', $v_data, true);
		
		$data['title'] = 'Edit Patients';
		$data['sidebar'] = 'reception_sidebar';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function search_general_queue($page_name)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$patient_national_id = $this->input->post('patient_national_id');
		$patient_number = $this->input->post('patient_number');
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		if(!empty($patient_national_id))
		{
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($strath_no))
		{
			$strath_no = ' AND patients.strath_no LIKE '.$strath_no.' ';
		}
		
		//search surname
		if(!empty($_POST['surname']))
		{
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
				}
				
				else
				{
					$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['othernames']))
		{
			$other_names = explode(" ",$_POST['othernames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $visit_type_id.$patient_number.$surname.$other_name.$patient_national_id;
		$this->session->set_userdata('general_queue_search', $search);
		
		redirect('queues/outpatient-queue');
	}
	
	public function close_general_queue_search($page_name)
	{
		$this->session->unset_userdata('general_queue_search');
		// $this->general_queue($page_name);
		redirect('queues/outpatient-queue');
	}
	public function close_general_inpatient_queue_search($page_name=NULL)
	{
		$this->session->unset_userdata('inpatients_search');
		// $this->general_queue($page_name);
		redirect('queues/inpatient-queue');
	}
	
	function import_template()
	{
		//export products template in excel 
		 $this->reception_model->import_template();
	}
	
	function import_patients()
	{
		//open the add new product
		$v_data['title'] = 'Import Patients';
		$data['title'] = 'Import Patients';
		$data['content'] = $this->load->view('patients/import_patients', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	function do_patients_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->reception_model->import_csv_products($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Patients';
		$data['title'] = 'Import Patients';
		$data['content'] = $this->load->view('patients/import_patients', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function inpatients()
	{
		$segment = 3;
		
		$where = 'visit.ward_id = ward.ward_id AND visit.inpatient = 1 AND visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type ';
		
		$table = 'visit, patients, visit_type, ward';
		
		$visit_search = $this->session->userdata('inpatients_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'queues/inpatient-queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_inpatient_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['title'] = $v_data['title'] = 'Inpatient Queue';
		
		$v_data['page_name'] = 'reception';
		$v_data['wards'] = $this->reception_model->get_wards();
	    $v_data['rooms'] = $this->reception_model->get_rooms();
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('inpatients', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	
	public function search_inpatients($page_name)
	{
		$ward_id = $this->input->post('ward_id');
		$visit_type_id = $this->input->post('visit_type_id');
		$patient_national_id = $this->input->post('patient_national_id');
		$patient_number = $this->input->post('patient_number');
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		if(!empty($patient_national_id))
		{
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($ward_id))
		{
			$ward_id = ' AND visit.ward_id = '.$ward_id.' ';
		}
		
		//search surname
		if(!empty($_POST['surname']))
		{
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
				}
				
				else
				{
					$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['othernames']))
		{
			$other_names = explode(" ",$_POST['othernames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $visit_type_id.$patient_number.$surname.$other_name.$patient_national_id.$ward_id;
		$this->session->set_userdata('inpatients_search', $search);
		
		// $this->inpatients($page_name);
		redirect('queues/inpatient-queue');
	}
	public function change_items()
	{
		$this->reception_model->changing_to_osh();
	}
	
	public function change_patient_visit($visit_id, $visit_type_id)
	{
		$this->form_validation->set_rules('visit_date', 'Admission Date', 'required');
		$this->form_validation->set_rules('ward_id', 'Ward', 'required|is_natural_no_zero');
		// $this->form_validation->set_rules('room_id', 'Ward', 'required|is_natural_no_zero');
		// $this->form_validation->set_rules('bed_id', 'Ward', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('personnel_id', 'Doctor', 'required|is_natural_no_zero');
		
		$ward_id = $this->input->post("ward_id"); 
		$doctor_id = $this->input->post("personnel_id");
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('error_message', validation_errors());
			redirect('queues/outpatient-queue');
		}
		else
		{
			$visit_date = $this->input->post("visit_date");
			//create visit
			if($this->reception_model->change_patient_visit($visit_date, $doctor_id, $visit_id, $ward_id))
			{
				//save admission fee
				$this->session->set_userdata('success_message', 'Inpatient visit initiated successfully');
				redirect('reception/inpatients');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to change to inpatient. Please try again');
				redirect('queues/outpatient-queue');
			}
		}
	}

	public function close_todays_visit()
	{
		$response	= $this->reception_model->close_todays_visits();

		echo $response."<br>";
	}
	
	public function update_visit_old($visit_id)
	{
		$this->form_validation->set_rules('visit_date', 'Visit Date', 'required');
		$this->form_validation->set_rules('department_id', 'Department', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('visit_type_id', 'Visit type', 'required|is_natural_no_zero');
		$visit_type_id = $this->input->post("visit_type_id"); 
		
		if(isset($_POST['department_id'])){
			if(($_POST['department_id'] == 2) || ($_POST['department_id'] == 4) || ($_POST['department_id'] == 3))
			{
				//if nurse visit (7) or theatre (14) service must be selected
				$this->form_validation->set_rules('personnel_id', 'Doctor', 'is_natural_no_zero');
				$this->form_validation->set_rules('service_charge_name', 'Consultation Type', 'required|is_natural_no_zero');
				$service_charge_id = $this->input->post("service_charge_name");
				$doctor_id = $this->input->post('personnel_id');
			}
			else if($_POST['department_id'] == 12)
			{
				//if nurse visit doctor must be selected
				$this->form_validation->set_rules('personnel_id2', 'Doctor', 'required|is_natural_no_zero');
				$this->form_validation->set_rules('service_charge_name2', 'Consultation Type', 'required|is_natural_no_zero');
				$service_charge_id = $this->input->post("service_charge_name2");
				$doctor_id = $this->input->post('personnel_id2');
			}
			else 
			{
				$service_charge_id = 0;
				$doctor_id = 0;
			}
		}
		
		if($visit_type_id != 1)
		{
			$this->form_validation->set_rules('insurance_limit', 'Insurance limit', '');
			$this->form_validation->set_rules('insurance_number', 'Insurance number', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->edit_visit($visit_id);
		}
		else
		{
			$insurance_limit = $this->input->post("insurance_limit");
			$insurance_number = $this->input->post("insurance_number");
			
			$visit_date = $this->input->post("visit_date");
			$timepicker_start = $this->input->post("timepicker_start");
			$timepicker_end = $this->input->post("timepicker_end");
			
			$appointment_id = $this->input->post("appointment_id");
			if($appointment_id == 1)
			{
				$close_card = 2;
			}
			else
			{		
				$close_card = 0;
			}
			
			//update visit
			$visit_id = $this->reception_model->update_visit($visit_date, $visit_id, $doctor_id, $insurance_limit, $insurance_number, $visit_type_id, $timepicker_start, $timepicker_end, $appointment_id, $close_card);
			
			//save consultation charge for nurse visit, counseling or theatre
			if($_POST['department_id'] == 2 || $_POST['department_id'] == 7 || $_POST['department_id'] == 12 || $_POST['department_id'] == 14)
			{
				//$this->reception_model->save_visit_consultation_charge($visit_id, $service_charge_id);	
			}
			
			//set visit department if not appointment
			if($appointment_id == 0)
			{
				//update patient last visit
				$this->reception_model->set_last_visit_date($patient_id, $visit_date);
				
				$department_id = $this->input->post('department_id');
				if($this->reception_model->set_visit_department($visit_id, $department_id, $visit_type_id))
				{
					if($appointment_id == 0)
					{
						$this->session->set_userdata('success_message', 'Visit has been updated');
					}
					else
					{
						$this->session->set_userdata('success_message', 'Appointment has been created');
					}
				}
				else
				{
					$this->session->set_userdata('error_message', 'Internal error. Could not add the visit');
				}
			}
			
			else
			{
				$this->session->set_userdata('success_message', 'Visit has been updated');
			}
			
			redirect('queues/outpatient-queue');
		}
	}
	public function update_visit($visit_id)
	{
		$this->form_validation->set_rules('visit_date', 'Visit Date', 'required');
		$this->form_validation->set_rules('visit_type_id', 'Visit type', 'required|is_natural_no_zero');
		$visit_type_id = $this->input->post("visit_type_id"); 

		// var_dump($_POST); die();
		
		
		
		if($visit_type_id != 1)
		{
			// $this->form_validation->set_rules('insurance_limit'.$visit_id, 'Insurance limit', 'required');
			// $this->form_validation->set_rules('insurance_number'.$visit_id, 'Insurance number', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->edit_visit($visit_id);
		}
		else
		{
			$insurance_description = $this->input->post("insurance_description");
			$insurance_number = $this->input->post("insurance_number");
			
			//$visit_type = $this->get_visit_type($type_name);
			$visit_date = $this->input->post("visit_date");
			$timepicker_start = $this->input->post("timepicker_start");
			$timepicker_end = $this->input->post("timepicker_end");
			$doctor_id = $this->input->post("personnel_id");			
			$appointment_id = $this->input->post("appointment_id");


			// var_dump($_POST);die();
			if($appointment_id == 1)
			{
				$close_card = 2;
			}
			else
			{		
				$close_card = 0;
			}
			
			//update visit
			$visit_id = $this->reception_model->update_visit($visit_date, $visit_id, $doctor_id, $insurance_description, $insurance_number, $visit_type_id, $timepicker_start, $timepicker_end, $appointment_id, $close_card);
			
			
			
			//set visit department if not appointment
			if($appointment_id == 0)
			{
				//update patient last visit
				$this->reception_model->set_last_visit_date($patient_id, $visit_date);
				$this->session->set_userdata('success_message', 'Visit has been updated');
				
				// $department_id = $this->input->post('department_id');
				// if($this->reception_model->set_visit_department($visit_id, $department_id, $visit_type_id))
				// {
				// 	if($appointment_id == 0)
				// 	{
				// 		$this->session->set_userdata('success_message', 'Visit has been updated');
				// 	}
				// 	else
				// 	{
				// 		$this->session->set_userdata('success_message', 'Appointment has been created');
				// 	}
				// }
				// else
				// {
				// 	$this->session->set_userdata('error_message', 'Internal error. Could not add the visit');
				// }
			}
			
			else
			{
				$this->session->set_userdata('success_message', 'Visit has been updated');
			}
			
			redirect('queue');
		}
	}
	public function patients_queue($department_id = NULL)
	{

		$personnel_id = $this->session->userdata('personnel_id');
		$parent_department = 0;
		// $department_id = $this->reception_model->get_personnel_department($personnel_id);
		// $admin_department = $this->session->userdata('department_id');
		// if($admin_department == 40000)
		// {
		// 	$department_id == 0;
		// }
		// else
		// {
		// 	$department_id = $parent_department = $department_id;
		// }
		// var_dump($department_id); die();
		if($department_id == 24 OR $department_id == 26)
		{
			// $department_id = 0;
			$department_where = ' AND visit.department_id = '.$department_id;
			$department_table = '';
		}
		
		else
		{
			

			$department_where = ' AND visit.department_id < 24';
			$department_table = '';

			// }
			
		}



		$segment = 3;
		
		$visit_date = date('Y-m-d');
		$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
		$where = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id  AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.visit_date >= "'.$visit_date.'" AND visit.inpatient = 0 AND (patients.patient_type = 0 OR patients.patient_type IS NULL)  '.$department_where ;
		  
		
		$table = 'visit, patients, visit_type'.$department_table;
		
		$visit_search = $this->session->userdata('general_queue_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		// var_dump($where);die();
		//pagination
		$this->load->library('pagination');
		if($department_id == 24)
			$config['base_url'] = site_url().'queues/maternity-queue';
		else if($department_id == 26)
			$config['base_url'] = site_url().'queues/mch-queue';
		else
			$config['base_url'] = site_url().'queues/outpatient-queue';	
		
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;

		// var_dump($department_id);die();
		
		if($department_id == 24)
				$v_data['title'] = $data['title'] = 'Maternity Queue';
		else if ($department_id == 26)
				$v_data['title'] = $data['title'] = 'MCH Queue';
		else
				$v_data['title'] = $data['title'] = 'Outpatient Queue';
	
		$page_name = 'reception';
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['rooms'] = $this->reception_model->get_rooms();
		$v_data['beds'] = $this->reception_model->get_rooms();

		$v_data['page_name'] = $page_name;
		$v_data['department_id'] = $parent_department;
		$v_data['type'] = $this->reception_model->get_types();
		// $v_data['rooms'] = $this->rooms_model->all_rooms();

		
		$v_data['doctors'] = $this->reception_model->get_all_doctors();
		
		$data['content'] = $this->load->view('patient_queue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}


	public function unclosed_visits()
	{


		$segment = 3;

		$date = date('Y-m-d',strtotime("-3 days"));
		$where = 'visit.visit_delete = 0  AND visit.patient_id = patients.patient_id  AND visit_type.visit_type_id = visit.visit_type AND visit_date ="'.$date.'" AND visit.inpatient = 0 AND  (patients.patient_type = 0 OR patients.patient_type IS NULL) ';
		
		
		$table = 'visit, patients, visit_type';
		
		$visit_search = $this->session->userdata('general_queue_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		// var_dump($where);die();
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'queues/unclosed-visit';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		

		$v_data['title'] = $data['title'] = 'Outpatient Queue for '.$date;
		$page_name = 'reception';
		// $v_data['wards'] = $this->reception_model->get_wards();
		// $v_data['rooms'] = $this->reception_model->get_rooms();
		// $v_data['beds'] = $this->reception_model->get_rooms();

		$v_data['page_name'] = $page_name;
		$v_data['type'] = $this->reception_model->get_types();
		// $v_data['rooms'] = $this->rooms_model->all_rooms();

		
		// $v_data['doctors'] = $this->reception_model->get_all_doctors();
		
		$data['content'] = $this->load->view('patient_queue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}


	public function walkins()
	{

		$personnel_id = $this->session->userdata('personnel_id');
		$department_id = $this->reception_model->get_personnel_department($personnel_id);
		$admin_department = $this->session->userdata('department_id');
		if($admin_department == 40000)
		{
			$department_id == 0;
		}
		else
		{
			$department_id = $department_id;
		}
		// var_dump($department_id); die();
		if($department_id == 0 OR $department_id == 9 OR $department_id == 1)
		{
			$department_id = $department_id;
			$department_where = '';
			$department_table = '';
		
		}
		else
		{
			// $department_where = ' AND visit.visit_id = visit_department.visit_id AND visit_department.visit_department_status = 1 AND visit.close_card = 0 AND visit_department.department_id = '.$department_id;
			// $department_table = ',visit_department';
			$department_where = '';
			$department_table = '';
		}

		$segment = 3;

		$where = 'visit.visit_delete = 0  AND visit.patient_id = patients.patient_id  AND visit_type.visit_type_id = visit.visit_type AND visit_date ="'.date('Y-m-d').'" AND visit.inpatient = 0  AND  (patients.patient_type = 1 )'.$department_where ;
		  
		
		$table = 'visit, patients, visit_type'.$department_table;


		$segment = 3;
		$visit_search = $this->session->userdata('walkin_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		// var_dump($where);die();
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'queues/walkins';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		

		$v_data['title'] = $data['title'] = 'Walkins Queue ';
		$page_name = 'reception';
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['rooms'] = $this->reception_model->get_rooms();
		$v_data['beds'] = $this->reception_model->get_rooms();

		$v_data['page_name'] = $page_name;
		$v_data['department_id'] = $department_id;
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['rooms'] = $this->rooms_model->all_rooms();


		$v_data['personnel_department_id'] = $department_id;

		
		$v_data['doctors'] = $this->reception_model->get_all_doctors();
		
		$data['content'] = $this->load->view('walkins_queue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}

	public function change_patient_status($patient_id)
	{
		$rip_date = $this->input->post('rip_date');
		$rip_report = $this->input->post('rip_report');
		$doctor_id = $this->input->post('doctor_id');
		$redirect_url = $this->input->post('redirect_url');
		
		if(empty($rip_date))
		{
			$this->session->userdata('error_message','Please select a date when the patient passed on');
			redirect($redirect_url);
		}
		else
		{
			$array = array('rip_status'=>1,'rip_date'=>$rip_date,'rip_report'=>$rip_report,'rip_doctor_id'=>$doctor_id,'rip_marked_by'=>$this->session->userdata('personnel_id'),'rip_marked_on'=>date('Y-m-d'));
			
			$this->db->where('patient_id',$patient_id);
			$this->db->update('patients',$array);

			$this->session->userdata('sucess_message','Please select a date when the patient passed on');
			redirect($redirect_url);
		}
	}
   




	public function search_patient_files()
	{
		$patient_number = $this->input->post('patient_number');
		$patient_national_id = $this->input->post('patient_national_id');
		
		if(!empty($patient_national_id))
		{
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		//search surname
		$surnames = explode(" ",$_POST['surname']);
		$total = count($surnames);
		
		$count = 1;
		$surname = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
			}
			
			else
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
			}
			$count++;
		}
		$surname .= ') ';
		
		//search other_names
		$other_names = explode(" ",$_POST['othernames']);
		$total = count($other_names);
		
		$count = 1;
		$other_name = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\'';
			}
			
			else
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' AND ';
			}
			$count++;
		}
		$other_name .= ') ';
		
		$search = $patient_number.$surname.$other_name.$patient_national_id;
		$this->session->set_userdata('visit_search', $search);
		
		redirect('records/patients-files');
	}
	public function close_patient_files_search()
	{
		$this->session->unset_userdata('visit_search');
		
		redirect('records/patients-files');
	}

	public function patient_uploads($patient_id)
	{
		
		$visit_search = $this->session->userdata('visit_search');
		$where = 'patient_document_type.document_type_id = patient_document_uploads.document_type_id AND document_status = 1 AND patient_document_uploads.patient_id ='.$patient_id;
		
		$table = 'patient_document_type,patient_document_uploads';		
		$segment  = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'patient-uploads/'.$patient_id;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_patients_documents($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['patient_id'] = $patient_id;
		
		$v_data['document_types'] = $this->reception_model->all_patient_document_types();
		$data['title'] = 'Patient Scans';
		$v_data['title'] = 'Patient Scans';
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('patient_scans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
	}

	public function add_patient_scan($patient_id)
	{

		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';
		
		//upload image if it has been selected
		$response = $this->reception_model->upload_any_file($this->document_upload_path, $this->document_upload_location, $document_name, 'document_scan');
		if($response)
		{
			$document_upload_location = $this->document_upload_location.$this->session->userdata($document_name);
		}
		
		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);
		$this->form_validation->set_rules('document_item_name', 'Document Name', 'xss_clean');
		$this->form_validation->set_rules('document_type_id', 'Document Type', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{

			if($this->reception_model->upload_patients_documents($patient_id, $document))
			{
				$this->session->set_userdata('success_message', 'Document uploaded successfully');
				$this->session->unset_userdata($document_name);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
		}

		redirect('patient-uploads/'.$patient_id);

	}
	public function save_appointment_accounts($patient_id,$visit_id = NULL)
	{
		$this->form_validation->set_rules('visit_date', 'Visit Date', 'required');
		$this->form_validation->set_rules('doctor_id', 'Doctor', 'required');
		$this->form_validation->set_rules('timepicker_start', 'Time start', 'required');
		$this->form_validation->set_rules('timepicker_end', 'Time start', 'required');

		$redirect_url = $this->input->post("redirect_url");
		
		if ($this->form_validation->run() == TRUE)
		{

			$visit_date = $this->input->post("visit_date");
			$timepicker_start = $this->input->post("timepicker_start");
			$timepicker_end = $this->input->post("timepicker_end");
			$room_id = $this->input->post("room_id");
			$procedure_done = $this->input->post("procedure_done");

			$doctor_id = $this->input->post("doctor_id");
			$check_visits = $this->reception_model->check_patient_appointment_exist($patient_id,$visit_date);
			$check_time = $this->reception_model->check_another_appointment_exist($patient_id,$timepicker_start,$timepicker_end,$visit_date,$doctor_id);

			$check_count = count($check_visits);
			$time_count = count($check_time);

			if($visit_id == NULL)
			{

				$this->db->where('patient_id',$patient_id);
			}
			else
			{

				$this->db->where('visit_id',$visit_id);
			}
			$query = $this->db->get('visit');

			$patient_insurance_id = 0;
			$patient_insurance_number = '';
			$patient_type = 0;

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key) {
					# code...
					$patient_insurance_id = $key->patient_insurance_id;
					$patient_insurance_number = $key->patient_insurance_number;
					$patient_type = $key->visit_type;
				}
			}



			$appointment_id = 1;

			if($appointment_id == 1)
			{
				$close_card = 2;
			}
			else
			{		
				$close_card = 0;
			}

			if($check_count > 0 )
			{
				$this->session->set_userdata('error_message', 'Seems like there is another appointment scheduled');

			}
			else if($time_count > 0)
			{
				$this->session->set_userdata('error_message', 'Seems like the time picked is already taken');
			}
			else
			{
				//create visit
				$visit_id_new = $this->reception_model->create_visit($visit_date, $patient_id, $doctor_id, $patient_insurance_id, $patient_insurance_number, $patient_type, $timepicker_start, $timepicker_end, $appointment_id, $close_card,'',$procedure_done,$room_id);
				
				if($visit_id_new > 0)
				{
					$this->session->set_userdata('success_message', 'Appointment Has been scheduled');
					$response['status'] = 'success';
					$response['message'] = 'Appointment Has been scheduled';
				}
				else
				{
					$this->session->set_userdata('error_message', 'Appointment Has not been scheduled. Pleae try again');
					$response['status'] = 'fail';
					$response['message'] = 'Appointment Has not been scheduled. Pleae try again';
				}
				
				
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Appointment Has not been scheduled. Please fill in all the details');
			$response['status'] = 'fail';
			$response['message'] = 'Appointment Has not been scheduled. Please fill in all the details';

		}
		
		// echo json_encode($response);
		redirect($redirect_url);
	}

	public function send_appointments_older_version()
	{
		$date_tomorrow = date("Y-m-d",strtotime("yesterday"));
		// $date_tomorrow = date('Y-m-d');
		$this->db->select('*');
		$this->db->where('visit.visit_date = "'.$date_tomorrow.'" AND visit.patient_id = patients.patient_id AND schedule_id = 0');
		$query = $this->db->get('visit,patients');
		// var_dump($query); die();
		$email_message = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_phone = $value->patient_phone1;
				$patient_id = $value->patient_id;
				$patient_othernames = $value->patient_othernames;
				$patient_surname = $value->patient_surname;
				$time_start = $value->time_start;
				$visit_date = date('jS M Y',strtotime($date_tomorrow));
				$message = "Hello ".$patient_surname.", a kind reminder about your appointment on ".$visit_date." at ".$time_start.". Please confirm in good time.\n Chelymo Medical Centre\n 0753998256.";

				$patient_phone = 704808007;
				$patient_phone = str_replace(' ', '', $patient_phone);

				$message_details['message'] = $message;
				$message_details['phone'] = $patient_phone;
				$message_details['sender_id'] = $this->config->item('sender_id');
				$message_details['sender_key'] = $this->config->item('sender_key');
				$message_details['sender_user'] = $this->config->item('sender_user');
				if(!empty($patient_phone))
				{
					$url =  $this->config->item('message_url').'admin/cron/send_message';		

					$data_string = json_encode($message_details);
					

					try{

						$ch = curl_init($url);
						var_dump($data_string); die();
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                  
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);              
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                    
						curl_setopt($ch, CURLOPT_HTTPHEADER, array(                             
							'Content-Type: application/json',                                          
							'Content-Length: ' . strlen($data_string))                       
						);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);                                                                                                                     
						$response = curl_exec($ch);                                                                                      
						// var_dump($response); die();
						curl_close($ch);
						// $response = $this->sync_model->parse_sync_up_response($result);
						$message_data = array(
												"phone_number" => $patient_phone,
												"entryid" => $patient_id,
												"message" => $message,
												"message_batch_id"=>0,
												'message_status' => 0
											);
						$this->db->insert('messages', $message_data);
						$message_id = $this->db->insert_id();



						
						// $response = $this->messaging_model->sms($patient_phone,$message);
						// var_dump($response); die();

						$email_message .= $patient_surname.' '.$patient_othernames.' AT '.$time_start.'<br>';

						$visit_update = array('schedule_id' => 1);
						$this->db->where('visit_id',$visit_id);
						$this->db->update('visit', $visit_update);

						if($response == "Success" OR $response == "success")
						{
							$service_charge_update = array('message_status' => 1,'delivery_message'=>'Success','sms_cost'=>3);
							$this->db->where('message_id',$message_id);
							$this->db->update('messages', $service_charge_update);

						}
						else
						{
							$service_charge_update = array('message_status' => 0,'delivery_message'=>'Success','sms_cost'=>0);
							$this->db->where('message_id',$message_id);
							$this->db->update('messages', $service_charge_update);


						}
					}
					
					catch(Exception $e)
					{
						$response = "something went wrong";
						echo json_encode($response.' '.$e);
					}
				}
				else
				{
					$response = "no data to sync";
					echo json_encode($response);
				}
				//var_dump($response); die();
			

			}

			// if($email_message == '')
			// {

			// }
			// else
			// {
			// 	// $this->send_email_for_appointment($email_message);
			// 	$date_tomorrow = date('Y-m-d');
			// 	$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($date_tomorrow)));
			// 	$visit_date = date('jS M Y',strtotime($date_tomorrow));
			// 	$branch = $this->config->item('branch_name');
			// 	$message_result['subject'] = 'Appointment report';
			// 	$v_data['persons'] = $email_message;
			// 	$text =  $this->load->view('emails_items',$v_data,true);

			// 	// echo $text; die();
			// 	$message_result['text'] = $text;
			// 	$contacts = $this->site_model->get_contacts();
			// 	$sender_email =$this->config->item('sender_email');//$contacts['email'];
			// 	$shopping = "";
			// 	$from = $sender_email; 
				
			// 	$button = '';
			// 	$sender['email']= $sender_email; 
			// 	$sender['name'] = $contacts['company_name'];
			// 	$receiver['name'] = $message_result['subject'];
			// 	// $payslip = $title;

			// 	$sender_email = $sender_email;
			// 	$tenant_email = $this->config->item('appointments_email');
			// 	// var_dump($sender_email); die();
			// 	$email_array = explode('/', $tenant_email);
			// 	$total_rows_email = count($email_array);

			// 	for($x = 0; $x < $total_rows_email; $x++)
			// 	{
			// 		$receiver['email'] = $email_tenant = $email_array[$x];

			// 		$this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);		
					

			// 	}
			// }
		}
		// redirect('appointments');
		echo '<script language="JavaScript">';
		echo 'window.self.close();';
		echo '</script>';
	}

	public function send_appointments()
	{
		$date_tomorrow = date("Y-m-d",strtotime("tomorrow"));
		// $date_tomorrow = date('Y-m-d');
		$this->db->select('*');
		$this->db->where('visit.visit_date = "'.$date_tomorrow.'" AND visit.patient_id = patients.patient_id AND schedule_id = 0');
		$query = $this->db->get('visit,patients');
		// var_dump($query); die();
		$email_message = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_phone = $value->patient_phone1;
				$patient_id = $value->patient_id;
				$patient_othernames = $value->patient_othernames;
				$patient_surname = $value->patient_surname;
				$time_start = $value->time_start;
				$visit_date = date('jS M Y',strtotime($date_tomorrow));
				$message = "Hello ".$patient_surname.", a kind reminder about your appointment on ".$visit_date." at ".$time_start.". Please confirm in good time.\n Chelymo Medical Centre\n 0753998256.";

				// $patient_phone = 704808007;
				$patient_phone = str_replace(' ', '', $patient_phone);

				$message_data = array(
										"phone_number" => $patient_phone,
										"entryid" => $patient_id,
										"message" => $message,
										"message_batch_id"=>0,
										'message_status' => 0
									);
				$this->db->insert('messages', $message_data);
				$message_id = $this->db->insert_id();
				$response = $this->messaging_model->sms($patient_phone,$message);
						// var_dump($response); die();

				$email_message .= $patient_surname.' '.$patient_othernames.' AT '.$time_start.'<br>';

				$visit_update = array('schedule_id' => 1);
				$this->db->where('visit_id',$visit_id);
				$this->db->update('visit', $visit_update);

				if($response == "Success" OR $response == "success")
				{
					$service_charge_update = array('message_status' => 1,'delivery_message'=>'Success','sms_cost'=>3);
					$this->db->where('message_id',$message_id);
					$this->db->update('messages', $service_charge_update);

				}
				else
				{
					$service_charge_update = array('message_status' => 0,'delivery_message'=>'Success','sms_cost'=>0);
					$this->db->where('message_id',$message_id);
					$this->db->update('messages', $service_charge_update);


				}
			// if($email_message == '')
			// {

			// }
			// else
			// {
			// 	// $this->send_email_for_appointment($email_message);
			// 	$date_tomorrow = date('Y-m-d');
			// 	$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($date_tomorrow)));
			// 	$visit_date = date('jS M Y',strtotime($date_tomorrow));
			// 	$branch = $this->config->item('branch_name');
			// 	$message_result['subject'] = 'Appointment report';
			// 	$v_data['persons'] = $email_message;
			// 	$text =  $this->load->view('emails_items',$v_data,true);

			// 	// echo $text; die();
			// 	$message_result['text'] = $text;
			// 	$contacts = $this->site_model->get_contacts();
			// 	$sender_email =$this->config->item('sender_email');//$contacts['email'];
			// 	$shopping = "";
			// 	$from = $sender_email; 
				
			// 	$button = '';
			// 	$sender['email']= $sender_email; 
			// 	$sender['name'] = $contacts['company_name'];
			// 	$receiver['name'] = $message_result['subject'];
			// 	// $payslip = $title;

			// 	$sender_email = $sender_email;
			// 	$tenant_email = $this->config->item('appointments_email');
			// 	// var_dump($sender_email); die();
			// 	$email_array = explode('/', $tenant_email);
			// 	$total_rows_email = count($email_array);

			// 	for($x = 0; $x < $total_rows_email; $x++)
			// 	{
			// 		$receiver['email'] = $email_tenant = $email_array[$x];

			// 		$this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);		
					

			// 	}
			// }
			}
		}
		// redirect('appointments');
		echo '<script language="JavaScript">';
		echo 'window.self.close();';
		echo '</script>';
	
	}
	public function send_checkups()
	{
		$date_tomorrow = date("Y-m-d",strtotime("yesterday"));
		// $date_tomorrow = date('Y-m-d');
		$this->db->select('*');
		$this->db->where('visit.visit_date = "'.$date_tomorrow.'" AND visit.patient_id = patients.patient_id AND inpatient = 0 ');
		$this->db->order_by('visit_id','desc');
		$this->db->group_by('visit.patient_id');
		$query = $this->db->get('visit,patients');
		// var_dump($query); die();
		$email_message = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_phone = $value->patient_phone1;
				$patient_id = $value->patient_id;
				$patient_othernames = $value->patient_othernames;
				$patient_surname = $value->patient_surname;
				$time_start = $value->time_start;
				$visit_date = date('jS M Y',strtotime($date_tomorrow));
				$time_start = date('H:i A',strtotime($time_start));
				$message = "Hello ".$patient_surname.". Chelymo Medical Centre values you. We attended to you yesterday please let us know how you are doing today. \n If you have any issue Call or text 0753998256. Thank you";
				// $patient_phone = '0726126635';			
				// $patient_phone = 704808007;
				$patient_phone = str_replace(' ', '', $patient_phone);
				if(!empty($patient_phone))
				{
					$message_data = array(
										"phone_number" => $patient_phone,
										"entryid" => $patient_id,
										"message" => $message,
										"message_batch_id"=>0,
										'message_status' => 0
									);
					$this->db->insert('messages', $message_data);
					$message_id = $this->db->insert_id();
					
					$response = $this->messaging_model->sms($patient_phone,$message);
					// var_dump($response); die();

					$email_message .= $patient_surname.' '.$patient_othernames.' AT '.$time_start.'<br>';

					$visit_update = array('schedule_id' => 1);
					$this->db->where('visit_id',$visit_id);
					$this->db->update('visit', $visit_update);

					if($response == "Success" OR $response == "success")
					{
						$service_charge_update = array('message_status' => 1,'delivery_message'=>'Success','sms_cost'=>3);
						$this->db->where('message_id',$message_id);
						$this->db->update('messages', $service_charge_update);

					}
					else
					{
						$service_charge_update = array('message_status' => 0,'delivery_message'=>'Success','sms_cost'=>0);
						$this->db->where('message_id',$message_id);
						$this->db->update('messages', $service_charge_update);


					}

				}
				
				
			}

			// if($email_message == '')
			// {

			// }
			// else
			// {
			// 	// $this->send_email_for_appointment($email_message);
			// 	$date_tomorrow = date('Y-m-d');
			// 	$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($date_tomorrow)));
			// 	$visit_date = date('jS M Y',strtotime($date_tomorrow));
			// 	$branch = $this->config->item('branch_name');
			// 	$message_result['subject'] = 'Checkup report';
			// 	$v_data['persons'] = $email_message;
			// 	$text =  $this->load->view('emails_items',$v_data,true);

			// 	// echo $text; die();
			// 	$message_result['text'] = $text;
			// 	$contacts = $this->site_model->get_contacts();
			// 	$sender_email =$this->config->item('sender_email');//$contacts['email'];
			// 	$shopping = "";
			// 	$from = $sender_email; 
				
			// 	$button = '';
			// 	$sender['email']= $sender_email; 
			// 	$sender['name'] = $contacts['company_name'];
			// 	$receiver['name'] = $message_result['subject'];
			// 	// $payslip = $title;

			// 	$sender_email = $sender_email;
			// 	$tenant_email = $this->config->item('appointments_email');
			// 	// var_dump($sender_email); die();
			// 	$email_array = explode('/', $tenant_email);
			// 	$total_rows_email = count($email_array);

			// 	for($x = 0; $x < $total_rows_email; $x++)
			// 	{
			// 		$receiver['email'] = $email_tenant = $email_array[$x];

			// 		$this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);		
					

			// 	}
			// }
		}
		// redirect('appointments');
		// redirect('appointments');
		echo '<script language="JavaScript">';
		echo 'window.self.close();';
		echo '</script>';
	}

	public function send_checkups_older_version()
	{
		$date_tomorrow = date("Y-m-d",strtotime("yesterday"));
		// $date_tomorrow = date('Y-m-d');
		$this->db->select('*');
		$this->db->where('visit.visit_date = "'.$date_tomorrow.'" AND visit.patient_id = patients.patient_id ');
		$this->db->order_by('visit_id','desc');
		$this->db->group_by('visit.patient_id');
		$query = $this->db->get('visit,patients');
		// var_dump($query); die();
		$email_message = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_phone = $value->patient_phone1;
				$patient_id = $value->patient_id;
				$patient_othernames = $value->patient_othernames;
				$patient_surname = $value->patient_surname;
				$time_start = $value->time_start;
				$visit_date = date('jS M Y',strtotime($date_tomorrow));
				$time_start = date('H:i A',strtotime($time_start));
				$message = "Hello ".$patient_surname.". Chelymo Medical Centre values you. We attended to you yesterday please let us know how you are doing today. \n If you have any issue Call or text 0753998256. Thank you";
				// $patient_phone = '0726126635';			
				$patient_phone = 704808007;
				$patient_phone = str_replace(' ', '', $patient_phone);

				$message_details['message'] = $message;
				$message_details['phone'] = $patient_phone;
				$message_details['sender_id'] = $this->config->item('sender_id');
				$message_details['sender_key'] = $this->config->item('sender_key');
				$message_details['sender_user'] = $this->config->item('sender_user');
				if(!empty($patient_phone))
				{
					$url =  $this->config->item('message_url').'admin/cron/send_message';		

					$data_string = json_encode($message_details);
					


					
					try{

						$ch = curl_init($url);

						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                  
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);              
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                    
						curl_setopt($ch, CURLOPT_HTTPHEADER, array(                             
							'Content-Type: application/json',                                          
							'Content-Length: ' . strlen($data_string))                       
						);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);                                                                                                                     
						$response = curl_exec($ch);                                                                                      
						// var_dump($response); die();
						curl_close($ch);
						// $response = $this->sync_model->parse_sync_up_response($result);
						$message_data = array(
												"phone_number" => $patient_phone,
												"entryid" => $patient_id,
												"message" => $message,
												"message_batch_id"=>0,
												'message_status' => 0
											);
						$this->db->insert('messages', $message_data);
						$message_id = $this->db->insert_id();



						
						// $response = $this->messaging_model->sms($patient_phone,$message);
						// var_dump($response); die();

						$email_message .= $patient_surname.' '.$patient_othernames.' AT '.$time_start.'<br>';

						$visit_update = array('schedule_id' => 1);
						$this->db->where('visit_id',$visit_id);
						$this->db->update('visit', $visit_update);

						if($response == "Success" OR $response == "success")
						{
							$service_charge_update = array('message_status' => 1,'delivery_message'=>'Success','sms_cost'=>3);
							$this->db->where('message_id',$message_id);
							$this->db->update('messages', $service_charge_update);

						}
						else
						{
							$service_charge_update = array('message_status' => 0,'delivery_message'=>'Success','sms_cost'=>0);
							$this->db->where('message_id',$message_id);
							$this->db->update('messages', $service_charge_update);


						}
					}
					
					catch(Exception $e)
					{
						$response = "something went wrong";
						echo json_encode($response.' '.$e);
					}
				}
				else
				{
					$response = "no data to sync";
					echo json_encode($response);
				}
			}

			// if($email_message == '')
			// {

			// }
			// else
			// {
			// 	// $this->send_email_for_appointment($email_message);
			// 	$date_tomorrow = date('Y-m-d');
			// 	$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($date_tomorrow)));
			// 	$visit_date = date('jS M Y',strtotime($date_tomorrow));
			// 	$branch = $this->config->item('branch_name');
			// 	$message_result['subject'] = 'Checkup report';
			// 	$v_data['persons'] = $email_message;
			// 	$text =  $this->load->view('emails_items',$v_data,true);

			// 	// echo $text; die();
			// 	$message_result['text'] = $text;
			// 	$contacts = $this->site_model->get_contacts();
			// 	$sender_email =$this->config->item('sender_email');//$contacts['email'];
			// 	$shopping = "";
			// 	$from = $sender_email; 
				
			// 	$button = '';
			// 	$sender['email']= $sender_email; 
			// 	$sender['name'] = $contacts['company_name'];
			// 	$receiver['name'] = $message_result['subject'];
			// 	// $payslip = $title;

			// 	$sender_email = $sender_email;
			// 	$tenant_email = $this->config->item('appointments_email');
			// 	// var_dump($sender_email); die();
			// 	$email_array = explode('/', $tenant_email);
			// 	$total_rows_email = count($email_array);

			// 	for($x = 0; $x < $total_rows_email; $x++)
			// 	{
			// 		$receiver['email'] = $email_tenant = $email_array[$x];

			// 		$this->email_model->send_sendgrid_mail($receiver, $sender, $message_result, NULL);		
					

			// 	}
			// }
		}
		// redirect('appointments');
		// redirect('appointments');
		echo '<script language="JavaScript">';
		echo 'window.self.close();';
		echo '</script>';
	}


    public function delete_document_scan($document_upload_id, $patient_id)
    {

    	// var_dump($patient_id); die();
		if($this->reception_model->delete_scan($document_upload_id))
		{
			$this->session->set_userdata('success_message', 'Document has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Document could not deleted');
		}
		redirect('patient-uploads/'.$patient_id);
	}
   	

   	public function search_walkin()
	{
		
		$patient_name = $this->input->post('patient_name');
		
		
		
		
		//search other_names
		if(!empty($patient_name))
		{
			$other_names = explode(" ",$patient_name);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' (patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' OR patients.patient_surname LIKE \'%'.$other_names[$r].'%\' ) ';
				}
				
				else
				{
					$other_name .= ' (patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' OR patients.patient_surname LIKE \'%'.$other_names[$r].'%\') AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $other_name;
		$this->session->set_userdata('walkin_search', $search);
		
		redirect('queues/walkins');
	}
	public function unhold_card($visit_id, $page = NULL)
	{
		//check if card is held
		
		$data = array(
					"hold_card" => 0,
					);
		$table = "visit";
		$key = $visit_id;
		$this->database->update_entry($table, $data, $key);
		redirect('queues/outpatient-queue');
		
	}

		public function test($visit_id)
	{
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inpatient'] = $patient['inpatient'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$visit_date = $this->reception_model->get_visit_date($visit_id);
		$gender = $patient['gender'];
		$visit_date = date('jS M Y',strtotime($visit_date));
		$v_data['age'] = $age;
		$v_data['visit_date'] = $visit_date;
		$v_data['gender'] = $gender;
		$v_data['visit_id'] = $visit_id;
		$v_data['visit'] = 1;

		$ultra_sound_order = 'service_charge_name';
		    
		$ultra_sound_where = 'service_charge.service_id = service.service_id AND service_charge.service_charge_status = 1 AND (service.service_name = "Dental Procedures" OR service.service_name = "Dental Procedures") AND  service_charge.visit_type_id = 1';


		$ultra_sound_table = '`service_charge`, service';

		$ultra_sound_query = $this->reception_model->get_dental($ultra_sound_table, $ultra_sound_where, $ultra_sound_order);
		$rs13 = $ultra_sound_query->result();
		$ultrasound = '';
		foreach ($rs13 as $ultra_sound_rs) :


		  $dental_id = $ultra_sound_rs->service_charge_id;
		  $dental_name = $ultra_sound_rs->service_charge_name;

		  $ultra_sound_price = $ultra_sound_rs->service_charge_amount;

		  $ultrasound .="<option value='".$dental_id."'>".$dental_name." KES.".$ultra_sound_price."</option>";

		endforeach;


		$v_data['ultrasound'] = $ultrasound;
		
		$data['content'] = $this->load->view('sheet', $v_data, true);
		$data['title'] = 'Dental';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function update_visit_items()
	{
		$this->db->where('visit_id > 0');

		$query= $this->db->get('visit');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_id = $value->visit_id;
				$patient_id = $value->patient_id;

				// chek if there is another visit before this 
				$this->db->where('patient_id = '.$patient_id.' AND visit_id < '.$visit_id);
				$query_less = $this->db->get('visit');

				$less_items = $query_less->num_rows();

				// check if there is another visit of this patient after this day
				$this->db->where('patient_id = '.$patient_id.' AND visit_id > '.$visit_id);
				$query_more = $this->db->get('visit');

				$more_items = $query_more->num_rows();


				if($less_items > 0 AND $more_items > 0)
				{
					// update the visit is like a revisit
					$visit_update['revisit'] = 1;

				}
				else if($less_items == 0 AND $more_items > 0)
				{

					// update the visit new visit
					$visit_update['revisit'] = 0;

				}
				else if($less_items == 0 AND $more_items == 0)
				{

					// update the visit new visit
					$visit_update['revisit'] = 0;

				}

				else if($less_items > 0 AND $more_items == 0)
				{

					// update the visit revisit visit
					$visit_update['revisit'] = 1;

				}

				$this->db->where('visit_id',$visit_id);
				$this->db->update('visit',$visit_update);

				

			}
		}
	}



	// chanegs


	public function general_queue()
	{

		$personnel_id = $this->session->userdata('personnel_id');

		$department_id = $this->reception_model->get_personnel_department($personnel_id);
		$admin_department = $this->session->userdata('department_id');

		$parent_department = 0;
		if($admin_department == 40000)
		{
			$department_id == 0;
		}
		else
		{
			$department_id = $parent_department = $department_id;
		}
		// var_dump($department_id); die();
		if($department_id == 0 OR $department_id == 1)
		{
			$department_id = 0;
			$department_where = '';
			$department_table = '';
		}
		// else if($department_id == 8)
		// {
		// 	$department_where = ' AND visit.visit_id = visit_department.visit_id AND visit_department.visit_department_status = 1 AND visit.close_card = 0 AND visit_department.department_id = 2';
		// 	$department_table = ',visit_department';
		// }
		else
		{
			// $current_time = strtotime(date('h:i:s a'));
			// $approved = $this->reception_model->get_if_authorized($personnel_id);
			// // var_dump($current_time); die();
			// if ($current_time > strtotime('07:00pm') AND $current_time < strtotime('09:00am') AND $approved) 
			// { 
			// 	// $department_id = 0;
			// 	$department_where = '';
			// 	$department_table = '';

			// }
			// else
			// {
				// $department_where = ' AND visit.visit_id = visit_department.visit_id AND visit_department.visit_department_status = 1 AND visit.close_card = 0 AND visit_department.department_id = '.$department_id;
				// $department_table = ',visit_department';

			$department_where = '';
			// $department_where = ' AND visit.department_id = '.$department_id;
			$department_table = '';

			// }
			
		}



		$segment = 3;
		

		$visit_date = date('Y-m-d');
		// $visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
		$where = 'visit.visit_delete = 0 AND patients.patient_delete = 0 AND visit.close_card <> 2 AND visit.patient_id = patients.patient_id  AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.inpatient = 0    '.$department_where;
		  
		
		$table = 'visit, patients, visit_type'.$department_table;
		
		$visit_search = $this->session->userdata('all_queue_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.$visit_date.'"';
		}
		// var_dump($where);die();
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'queues/general-queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		

		$v_data['title'] = $data['title'] = 'General Queue';
		$page_name = 'reception';
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['rooms'] = $this->reception_model->get_rooms();
		$v_data['beds'] = $this->reception_model->get_rooms();

		$v_data['page_name'] = $page_name;
		$v_data['department_id'] = $parent_department;
		$v_data['type'] = $this->reception_model->get_types();
		// $v_data['rooms'] = $this->rooms_model->all_rooms();

		
		$v_data['doctors'] = $this->reception_model->get_all_doctors();
		
		$data['content'] = $this->load->view('general_queue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}





	public function search_all_queue($page_name=NULL)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$patient_national_id = $this->input->post('patient_national_id');
		$patient_number = $this->input->post('patient_number');
		$phone_number = $this->input->post('phone_number');
		$date_time = $this->input->post('date_time');
		$visit_date_from = $this->input->post('date_from');
		$visit_date_to = $this->input->post('date_to');
		

		$search_title = '';
		if(!empty($patient_number))
		{
			$search_title .= ' Number :  '.$patient_number;
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		if(!empty($patient_national_id))
		{
			$search_title .= ' Id :  '.$patient_national_id;
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}


		if(!empty($phone_number))
		{
			$search_title .= ' Phone :  '.$phone_number;
			$phone_number = ' AND patients.patient_phone1 = \''.$phone_number.'\' ';

		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($strath_no))
		{
			$strath_no = ' AND patients.strath_no LIKE '.$strath_no.' ';
		}
		
		//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' Name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}


		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date_checked = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$providers_date_from = $visit_date_from;
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from) && empty($visit_date_to))
		{
			$visit_date_checked = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$providers_date_from = $visit_date_from;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to) && empty($visit_date_from))
		{
			$visit_date_checked = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date_checked = '';
			$providers_date_from = '';
			$providers_date_to = '';
		}
		// var_dump($visit_date_checked);die();
		
		//search other_names
		if(!empty($_POST['othernames']))
		{
			$other_names = explode(" ",$_POST['othernames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		$list_view = '';

		// var_dump($date_time);die();
		if(!empty($date_time))
		{

			if($date_time == 1)
			{
				$list_view = ' AND visit.visit_date = \''.date('Y-m-d').'\' ';
				$search_title .= ' Today ';
			}
			else if($date_time == 2)
			{
				$visit_date = date('Y-m-d');
				$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
				$list_view = ' AND visit.visit_date = \''.$visit_date.'\'';
				$search_title .= ' Yesterday ';

			}
			else if($date_time == 3)
			{		
				$visit_date = date('Y-m-d');
				$firstday = date('Y-m-d', strtotime("this week")); 
				$list_view = ' AND visit.visit_date BETWEEN \''.$firstday.'\' AND \''.$visit_date.'\'  ';
				$search_title .= ' This Week ';
			}
			else if($date_time == 4)
			{
				

				$visit_date = date('Y-m-01');
				$last_day = date('Y-m-t'); 
				$list_view = ' AND visit.visit_date BETWEEN \''.$visit_date.'\' AND \''.$last_day.'\'  ';
				$search_title .= ' This Month ';
			}
			
		}
		
		$search = $visit_type_id.$patient_number.$surname.$other_name.$patient_national_id.$phone_number.$list_view.$visit_date_checked;
		$this->session->set_userdata('all_search_title', $search_title);
		$this->session->set_userdata('all_queue_search', $search);
		
		redirect('queues/general-queue');
	}
	
	public function close_all_queue_search($page_name=null)
	{
		$this->session->unset_userdata('all_queue_search');
		$this->session->unset_userdata('all_search_title');
		// $this->general_queue($page_name);
		redirect('queues/general-queue');
	}
	public function maternity_queue()
	{

		$personnel_id = $this->session->userdata('personnel_id');
		$parent_department = 0;
		$department_id = $this->reception_model->get_personnel_department($personnel_id);
		$admin_department = $this->session->userdata('department_id');
		if($admin_department == 40000)
		{
			$department_id == 0;
		}
		else
		{
			$department_id = $parent_department = $department_id;
		}
		// var_dump($department_id); die();
		if($department_id == 0 OR $department_id == 9 OR $department_id == 7 OR $department_id == 1)
		{
			$department_id = 0;
			$department_where = '';
			$department_table = '';
		}
		// else if($department_id == 8)
		// {
		// 	$department_where = ' AND visit.visit_id = visit_department.visit_id AND visit_department.visit_department_status = 1 AND visit.close_card = 0 AND visit_department.department_id = 2';
		// 	$department_table = ',visit_department';
		// }
		else
		{
			// $current_time = strtotime(date('h:i:s a'));
			// $approved = $this->reception_model->get_if_authorized($personnel_id);
			// // var_dump($current_time); die();
			// if ($current_time > strtotime('07:00pm') AND $current_time < strtotime('09:00am') AND $approved) 
			// { 
			// 	// $department_id = 0;
			// 	$department_where = '';
			// 	$department_table = '';

			// }
			// else
			// {
				// $department_where = ' AND visit.visit_id = visit_department.visit_id AND visit_department.visit_department_status = 1 AND visit.close_card = 0 AND visit_department.department_id = '.$department_id;
				// $department_table = ',visit_department';

			$department_where = '';
			$department_table = '';

			// }
			
		}



		$segment = 3;
		
		$visit_date = date('Y-m-d');
		$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
		$where = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id  AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.visit_date >= "'.$visit_date.'" AND visit.inpatient = 0 AND (patients.patient_type = 0 OR patients.patient_type IS NULL)  ' ;
		  
		
		$table = 'visit, patients, visit_type'.$department_table;
		
		$visit_search = $this->session->userdata('general_queue_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		// var_dump($where);die();
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'queues/maternity-queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		

		$v_data['title'] = $data['title'] = 'Maternity Queue';
		$page_name = 'reception';
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['rooms'] = $this->reception_model->get_rooms();
		$v_data['beds'] = $this->reception_model->get_rooms();

		$v_data['page_name'] = $page_name;
		$v_data['department_id'] = $parent_department;
		$v_data['type'] = $this->reception_model->get_types();
		// $v_data['rooms'] = $this->rooms_model->all_rooms();

		
		$v_data['doctors'] = $this->reception_model->get_all_doctors();
		
		$data['content'] = $this->load->view('maternity_queue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}

	public function discharges()
	{
		$segment = 3;
		
		$where = 'visit.ward_id = ward.ward_id AND visit.inpatient = 1 AND visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND (visit.close_card = 2 OR visit.close_card = 7 OR visit.close_card = 1) AND visit_type.visit_type_id = visit.visit_type ';
		
		$table = 'visit, patients, visit_type, ward';
		
		$visit_search = $this->session->userdata('discharge_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'queues/discharge-queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_inpatient_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['title'] = $v_data['title'] = 'Discharges';
		
		$v_data['page_name'] = 'reception';
		$v_data['wards'] = $this->reception_model->get_wards();
	    $v_data['rooms'] = $this->reception_model->get_rooms();
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('visit/discharges', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	public function search_discharge($page_name)
	{
		$ward_id = $this->input->post('ward_id');
		$visit_type_id = $this->input->post('visit_type_id');
		$patient_national_id = $this->input->post('patient_national_id');
		$patient_number = $this->input->post('patient_number');
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}
		
		if(!empty($patient_national_id))
		{
			$patient_national_id = ' AND patients.patient_national_id = \''.$patient_national_id.'\' ';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($ward_id))
		{
			$ward_id = ' AND visit.ward_id = '.$ward_id.' ';
		}
		
		//search surname
		if(!empty($_POST['surname']))
		{
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
				}
				
				else
				{
					$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['othernames']))
		{
			$other_names = explode(" ",$_POST['othernames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.$other_names[$r].'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $visit_type_id.$patient_number.$surname.$other_name.$patient_national_id.$ward_id;
		$this->session->set_userdata('discharge_search', $search);
		
		// $this->inpatients($page_name);
		redirect('queues/discharge-queue');
	}
	public function get_sidebar_details($patient_id,$visit_id=NULL)
	{


		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		

		$lab_test_where = 'patients.patient_id = '.$patient_id;
		$lab_test_table = 'patients';
		$lab_test_where .= ' AND patient_delete = 0';

		$this->db->where($lab_test_where);
		$this->db->limit(1);
		 // $this->db->join('v_patient_account_balances','patients.patient_id = v_patient_account_balances.patient_id','LEFT');
		$query = $this->db->get($lab_test_table);

		
		$v_data['query'] = $query;
		$v_data['patient_id'] = $patient_id;
		$v_data['visit_id'] = $visit_id;

		
		$result = $this->load->view('patient_views/sidebar_detail', $v_data,true);
		$response['message'] = 'success';
		$response['result'] = $result;

		echo json_encode($response);
	}

	
	public function get_patient_accounts($patient_id,$visit_type_id)
	{
		$this->db->where('patient_id = "'.$patient_id.'" AND visit_type_id = "'.$visit_type_id.'"');
		$query = $this->db->get('patient_account');

		if($query->num_rows() == 1)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$patient_account_id = $value->patient_account_id;
				$scheme_name = $value->scheme_name;
				$member_number = $value->member_number;
				$principal_member = $value->principal_member;
				$relation = $value->relation;
			}

			$response['scheme_name'] = $scheme_name;
			$response['member_number'] = $member_number;
			$response['principal_member'] = $principal_member;
			$response['relation'] = $relation;
		}

		else
		{
			$response['scheme_name'] = '';
			$response['member_number'] = '';
			$response['principal_member'] = '';
			$response['relation'] = '';

		}
		echo json_encode($response);
	}

}



?>