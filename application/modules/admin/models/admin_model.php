<?php

class Admin_model extends CI_Model 
{
	/*
	*	Check if parent has children
	*
	*/
	public function check_children($children, $section_id, $web_name)
	{
		$section_children = array();
		
		if($children->num_rows() > 0)
		{
			foreach($children->result() as $res)
			{
				$parent = $res->section_parent;
				
				if($parent == $section_id)
				{
					$section_name = $res->section_name;
					
					$child_array = array
					(
						'section_name' => $section_name,
						'link' => site_url().$web_name.'/'.strtolower($this->site_model->create_web_name($section_name)),
					);
					
					array_push($section_children, $child_array);
				}
			}
		}
		
		return $section_children;
	}
	
	public function get_breadcrumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$crumbs = '<li><a href="'.site_url().'dashboard"><i class="fa fa-home"></i></a></li>';
		
		for($r = 0; $r < $total; $r++)
		{
			$name = $this->site_model->decode_web_name($page[$r]);
			if($r == $last)
			{
				$crumbs .= '<li><span>'.strtoupper($name).'</span></li>';
			}
			else
			{
				if($total == 3)
				{
					if($r == 1)
					{
						$crumbs .= '<li><a href="'.site_url().$page[$r-1].'/'.strtolower($name).'">'.strtoupper($name).'</a></li>';
					}
					else
					{
						$crumbs .= '<li><a href="'.site_url().strtolower($name).'">'.strtoupper($name).'</a></li>';
					}
				}
				else
				{
					$crumbs .= '<li><a href="'.site_url().strtolower($name).'">'.strtoupper($name).'</a></li>';
				}
			}
		}
		
		return $crumbs;
	}
	
	public function create_breadcrumbs($title)
	{
		$crumbs = '<li><a href="'.site_url().'dashboard"><i class="fa fa-home"></i></a></li>';
		$crumbs .= '<li><span>'.strtoupper($title).'</span></li>';
		
		return $crumbs;
	}
	
	public function get_configuration()
	{
		return $this->db->get('configuration');
	}
	
	public function edit_configuration($configuration_id)
	{
		$data = array(
			'mandrill' => $this->input->post('mandrill'),
			'sms_key' => $this->input->post('sms_key'),
			'sms_user' => $this->input->post('sms_user')
		);
		
		if($configuration_id > 0)
		{
			$this->db->where('configuration_id', $configuration_id);
			if($this->db->update('configuration', $data))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
		
		else
		{
			if($this->db->insert('configuration', $data))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
	}
	
	public function create_preffix($yourString)
	{
		$vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", " ");
		$yourString = str_replace($vowels, "", $yourString);
		$trimed = substr($yourString, 0, 3);
		$preffix = strtoupper($trimed);
		return $preffix;
	}
	public function check_if_admin($personnel_id)
	{
		$this->db->where('job_title_id = 1 AND personnel_id ='.$personnel_id);
		$query=$this->db->get('personnel_job');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_departments()
	{
		$this->db->where('department_id > 0');
		$query = $this->db->get('departments');

		return $query;
	}

	public function get_inquery_types()
	{
		$this->db->where('inquery_type_id > 0');
		$query = $this->db->get('inquery_types');

		return $query;
	}

	public function post_inquery()
	{
			$description = $this->input->post('description');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$remarks = $this->input->post('remarks');
			$department_id = $this->input->post('department_id');
			$attention_to = $this->input->post('attention_to');
			$priority_id = $this->input->post('priority_id');
			$date = $this->input->post('date');



			$array['description'] = $description;
			$array['name'] = $name;
			$array['phone'] = $phone;
			$array['remarks'] = $remarks;
			$array['date'] = $date;
			$array['department_id'] = $department_id;
			$array['attention_to'] = $attention_to;
			$array['priority_id'] = $priority_id;
			$array['created'] = date('Y-m-d H:i:s');
			$array['created_by'] = $this->session->userdata('personnel_id');


			if($this->db->insert('visit_inqueries',$array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}



	}
	
	public function get_doctor()
	{
		$table = "personnel, personnel_job,personnel_type";
		$where = "personnel_job.personnel_id = personnel.personnel_id AND personnel_job.job_title_id = 12 AND personnel.personnel_type_id = personnel_type.personnel_type_id AND personnel_type.personnel_type_name <> 'Service Provider'";
		$items = "personnel.personnel_onames, personnel.personnel_fname, personnel.personnel_id";
		$order = "personnel_onames";

		$result = $this->database->select_entries_where($table, $where, $items, $order);

		return $result;
	}
	public function get_all_inqueries()
	{

		$this->db->where('visit_inqueries.inquery_id > 0');
		$this->db->join('departments','departments.department_id = visit_inqueries.department_id','LEFT');
		$this->db->join('personnel','personnel.personnel_id = visit_inqueries.created_by','LEFT');
		$this->db->order_by('visit_inqueries.created','DESC');

		$query = $this->db->get('visit_inqueries');

		return $query;

	}

		public function get_all_personnel()
	{
		$this->db->select('*');
		$query = $this->db->get('personnel');
		
		return $query;
	}

	
	public function get_all_visits_parent($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		// $this->db->from($table);
		// $this->db->select('visit.*,patients.*');
		// $this->db->where($where);
		// $this->db->order_by('visit.time_start','ASC');

		$query = $this->db->query("SELECT
			  visit_date, TIME(STR_TO_DATE(time_start, '%l:%i %p')),patients.*,visit.*
			FROM
			  visit,patients
			where
			".$where."
			ORDER BY
			  STR_TO_DATE(time_start, '%l:%i %p')");
					// $query = $this->db->get('', $per_page, $page);
					
					return $query;
	}

}
?>