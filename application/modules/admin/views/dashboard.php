<?php
$date_today = date('Y-m-d');


 $community_where ='patients.patient_type = 0 AND patients.patient_delete = 0 AND  DATE(patients.patient_date) = "'.$date_today.'"';
$community_table = 'patients';
$total_number_new = $this->reception_model->count_items($community_table, $community_where);




$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
$community_table = 'patients,visit';
$total_outpatients = $this->reception_model->count_items($community_table, $community_where);


$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 7)  AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  DATE(visit.visit_date) <> "'.$date_today.'"';
$community_table = 'patients,visit';
$total_inpatients = $this->reception_model->count_items($community_table, $community_where);


$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 7)  AND visit.visit_delete = 0  AND patients.patient_delete = 0';
$community_table = 'patients,visit';
$new_inpatients = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 2)  AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND DATE(visit.visit_time_out) = "'.$date_today.'"';
$community_table = 'patients,visit';
$discharges = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.revisit <= 1 AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  visit.visit_date = "'.$date_today.'"';
$community_table = 'patients,visit';
$total_newisits = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.revisit = 2 AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  visit.visit_date = "'.$date_today.'"';
$community_table = 'patients,visit';
$total_revisits = $this->reception_model->count_items($community_table, $community_where);


// var_dump($total_newisits);die();
?>
<div class="row">
	<!-- <div class="col-md-12"> -->

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-success">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">TODAY'S OUTPATIENTS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_outpatients?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-primary">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">1st TIME VISIT  <?php echo $this->session->userdata('branch_code')?></h5>
								<div class="info">
									<strong class="amount"><?php echo $total_number_new?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
				<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-danger">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">NEW VISITS VS REVISITS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_newisits.' VS '.$total_revisits?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-info">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">ACTIVE INPATIENTS</h5>
								<div class="info">
									<strong class="amount"><?php echo $new_inpatients?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-warning">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">TODAY'S DISCHARGES</h5>
								<div class="info">
									<strong class="amount"><?php echo $discharges?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">

			<section class="card mb-4">
				<div class="card-body bg-default">
					<div class="widget-summary">
						
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title" style="color: #fff">APPOINTMENTS</h5>
								<div class="info">
									<strong class="amount" style="color: #fff"><?php echo '0';?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
			
		</div>
		


	<!-- </div> -->
</div>

<div class="row" style="margin-top:20px;">


			<div class="col-md-4">	

				<h5>PATIENTS SEEN/REGISTERED TODAY (PER VISIT TYPE - : CLOSED)</h5>	
				<br>
					<div class="panel-body" style="height:40vh !important;overflow-y: scroll; padding: 0px;">
					<?php
						$date_today = date('Y-m-d');
						$visit_types_rs = $this->reception_model->get_visit_types();
						$visit_results = '<tbody>';
					
						$grand_total_outpatients = 0;
						$grand_total_inpatients = 0;
						$grand_total_visits = 0;
						if($visit_types_rs->num_rows() > 0)
						{
							foreach ($visit_types_rs->result() as $key => $value) {
								# code...

								$visit_type_name = $value->visit_type_name;
								$visit_type_id = $value->visit_type_id;


								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 0 AND visit.close_card <> 2 AND patients.patient_delete = 0 AND visit.visit_date = "'.$date_today.'"';
								$total_outpatient_patients = $this->reception_model->count_items($table,$where);
							




								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 1 AND visit.close_card <> 2  AND patients.patient_delete = 0  AND visit.visit_date = "'.$date_today.'"';
								$total_inpatient_patients = $this->reception_model->count_items($table,$where);



								$total_visits = $total_outpatient_patients + $total_inpatient_patients;

								$grand_total_outpatients += $total_outpatient_patients;
								$grand_total_inpatients += $total_inpatient_patients;
								$grand_total_visits += $total_visits;
								// calculate amounts paid
							
								$visit_results .='<tr>
												  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
												  		<td style="text-align:center;"> '.$total_outpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_inpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_visits.'</td>
												  	</tr>';
								
					


							}

							$visit_results .='		</tbody>
													<tfoot>
												  		<th style="text-align:left;"> TOTAL  </th>
												  		<th style="text-align:center;"> '.$grand_total_outpatients.'</th>
												  		<th style="text-align:center;"> '.$grand_total_inpatients.'</th>
												  		<th style="text-align:center;"> '.$grand_total_visits.'</th>
												  	</tfoot>';
								

							
						}


					?>


					<table  class="table table-hover table-bordered table-linked">
						<thead>
							<tr>
								<th style="padding:5px;">VISIT TYPE</th>
								<th style="padding:5px;">OUTPATIENTS</th>
								<th style="padding:5px;">INPATIENTS</th>
								<th style="padding:5px;">TOTAL PATIENTS</th>
								
							</tr>
						</thead>
						
						  	<?php echo $visit_results?>
					  
					</table>
				</div>
			</div>
			<div class="col-md-4">

				<h5>NEW VISITS AND REVISITS</h5>	
				<br>
				<div class="panel-body" style="height:40vh !important;overflow-y: scroll; ">
				<?php


				$visits_rs = '{
			                        label: "NEW VISITS ",
			                        data: [
			                            [1, '.$total_newisits.']
			                        ],
			                        color: "#0088cc"
			                    }, {
			                        label: "REVISITS",
			                        data: [
			                            [1, '.$total_revisits.']
			                        ],
			                        color: "#734ba9"
			                    }';


				?>
				<!-- Flot: Pie -->
                <div class="chart chart-md" id="flotPie3" style="height:250px!important"></div>
                <script type="text/javascript">
                    var flotPieData3 = [<?php echo $visits_rs?>];

                    // See: js/examples/examples.charts.js for more settings.
                </script>
                <?php

                    $date_today = date('Y-m-d');

					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 1 AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
					$community_table = 'patients,visit';
					$total_men = $this->reception_model->count_items($community_table, $community_where);


					  $date_today = date('Y-m-d');
					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 2 AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
					$community_table = 'patients,visit';
					$total_female = $this->reception_model->count_items($community_table, $community_where);


					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 0 AND visit.visit_delete = 0  AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
					$community_table = 'patients,visit';
					$not_indicated = $this->reception_model->count_items($community_table, $community_where);
                    ?>
                <h5>VISITS PER GENDER</h5>	
        			<br>
                	<table class="table table-hover table-bordered ">
                  
                        <tr>
                          <th>MALE</th>
                          <td><?php echo $total_men?></td>
                          <th>FEMALE</th>
                          <td><?php echo $total_female?></td>
                          <th>NOT INDICATED</th>
                          <td><?php echo $not_indicated?></td>
                        </tr>
                     
                 
                     </table>
            	</div>
			</div>
			<div class="col-md-4">
				<h5>NEW VISITS AND REVISITS PER DEPARTMENT</h5>	
				<br>
				<div class="panel-body" style="height:40vh !important;overflow-y: scroll; padding:0px; ">
                <?php
                   
                     $this->db->where('report_item = 1');
                    $query = $this->db->get('departments');
                    $result = '';
                    
                    //if users exist display them
                    if ($query->num_rows() > 0)
                    {
                        $count = $page;
                        
                        $result .= 
                            '
                                <table class="table table-hover table-bordered table-linked">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>DEPARTMENT</th>
                                      <th>NEW VISITS</th>
                                      <th>REVISITS</th>
                                      <th>TOTAL</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                            ';
                        $grand_number_new = 0;
                        $grand_number_old = 0;
                        $grand_number = 0;
                        foreach ($query->result() as $row)
                        {
                            
                            $department_name = $row->department_name;
                            $department_id = $row->department_id;
                            
                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id  AND visit.revisit <= 1 AND patients.patient_type = 0 AND DATE(visit.visit_date) = "'.$date_today.'"';
                            $community_table = 'visit,patients';
                            $total_number_new = $this->reception_model->count_items($community_table, $community_where);

                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id  AND visit.revisit = 2 AND patients.patient_type = 0 AND DATE(visit.visit_date) = "'.$date_today.'"';
                            $community_table = 'visit,patients';
                            $total_number_old = $this->reception_model->count_items($community_table, $community_where);
                            $total = $total_number_new + $total_number_old;
                            $count++;
                             $grand_number_new += $total_number_new ;
                        	$grand_number_old += $total_number_old;
                        	$grand_number += $total;
                            $result .= 
                                '
                                    <tr>
                                        <td>'.$count.'</td>
                                        <td>'.strtoupper($department_name).'</td>
                                        <td>'.$total_number_new.'</td>
                                        <td>'.$total_number_old.'</td>
                                        <td>'.$total.'</td>
                                        
                                    </tr> 
                                ';
                        }
                        
                        $result .= 
                                '
                                	</tbody>
                                    <tfoot>
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th>'.$grand_number_new.'</th>
                                        <th>'.$grand_number_old.'</th>
                                        <th>'.$grand_number.'</th>
                                        
                                    </tfoot> 
                                ';
                        $result .= 
                        '
                                      
                                    </table>
                        ';
                    }
                    
                    else
                    {
                        $result .= "There are no departments";
                    }
                    
                    echo $result;
                    ?>

                    
                    


                </div>
				
         
				
			</div>
			

</div>




<script type="text/javascript">
	

	$(document).on("submit","form#search-price-list",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		
		var config_url = $('#config_url').val();	

		 var url = config_url+"admin/search_price_list";
		 // var values = $('#visit_type_id').val();
		 
	       $.ajax({
	       type:'POST',
	       url: url,
	       data:form_data,
	       dataType: 'text',
	       processData: false,
	       contentType: false,
	       success:function(data){
	          var data = jQuery.parseJSON(data);
	        
	          	if(data.message == "success")
				{
					// alert(data.result);
	    			$('#price-list').html(data.result);
					
				}
				else
				{
					alert('Please ensure you have added included all the items');
				}
	       
	       },
	       error: function(xhr, status, error) {
	       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	       
	       }
	       });
		 
		
	   
		
	});
</script>


<script type="text/javascript">
	function open_patients(place_id,year,month)
	{
		open_sidebar();

		var config_url = $('#config_url').val();
        var data_url = config_url+"admin/get_all_patients/"+place_id+"/"+year+"/"+month;

    	// alert(data_url);
        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: 1},
        dataType: 'text',
		success:function(data)
		{
			$("#sidebar-div").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	}
</script>


