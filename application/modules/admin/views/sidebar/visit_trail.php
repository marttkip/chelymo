<?php 




$billing_trail = '';
$main_trail = $this->reception_model->get_main_trail($visit_id);
$billing_trail = 
    '
      <table class="table table-bordered table-responsive table-condensed table-striped table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>Date Time</th>
            <th>Personnel Name</th>
          </tr>
        </thead>
    ';
if($main_trail->num_rows() > 0)
  {
    
    $counting = 0;
    foreach($main_trail->result() as $res)
    {
      $counting++;
      $title = $res->title;
      $description = $res->description;
      $personnel_name = $res->personnel_name;
      $datetime = $res->datetime;


      
      
      
      $billing_trail .=
      '
        <tr>
          <td>'.$count.'</td>
          <td>'.ucwords(strtolower($title)).'</td>
          <td>'.ucfirst(strtolower($description)).'</td>
          <td>'.date('Y-m-d H:i A',strtotime($datetime)).'</td>
          <td>'.ucfirst(strtolower($personnel_name)).'</td>
        </tr>
      ';
    }
    

  }
  
  else
  {
    $billing_trail .= '<tr>
                        <td colspan="5"> All invoice</td>
                      </tr>';
  }

$billing_trail .= '</table>';
?>
<section class="panel" >
  <div class="panel-body" style="height:80vh;overflow-y:scroll;">
    
     <div class="col-md-12" id="sidebar-container" style="margin-bottom: 20px;" >
        <h4>Visit Trail</h4>
        <div class="col-md-12">
            <?php echo $billing_trail;?>
        </div>
        
     </div>
  </div>
  
</section>
<div class="row" style="margin-top: 5px;">
  <ul>
    <li style="margin-bottom: 5px;">
      <div class="row">
            <div class="col-md-12 center-align">
                <!-- <div id="old-patient-button" style="display:none">
                                  
                  
                </div> -->
                <!-- <div> -->
                  <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
                <!-- </div> -->
                  
                   
            </div>
        </div>
      
    </li>
  </ul>
</div>

</script>