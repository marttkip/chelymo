<?php

?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
           	<div class="col-md-12" style="height: 40vh; overflow-y: scroll;">
               	<div class="col-md-12">
              		<?php //echo form_open("", array("class" => "form-horizontal",));?>

              		<form method="post" id="add-inqueries">
              			
                  		
                  			<div class="col-md-5">
		 						<div class="form-group">
					                <label class="col-md-4 control-label">Department: </label>
					                <div class="col-md-8">
					                	<select name="department_id" id="department_id" class="form-control" required>
											<option value="">----Select a department----</option>
											<?php
																	
												if($departments->num_rows() > 0){

													foreach($departments->result() as $row):
														$department_name = $row->department_name;
														$department_id = $row->department_id;

														if($department_id == set_value('department_id'))
														{
															echo "<option value='".$department_id."' selected='selected'>".$department_name."</option>";
														}
														
														else
														{
															echo "<option value='".$department_id."'>".$department_name."</option>";
														}
													endforeach;
												}
											?>
										</select>
					                </div>
					            </div>
					            <div class="form-group">
					                <label class="col-md-4 control-label">Priority: </label>
					                <div class="col-md-8">
					                	<select name="priority_id" id="priority_id" class="form-control">
											<option value="0">----Select Priority----</option>
											<option value="1">Modarate</option>
											<option value="2">Urgent</option>
											<option value="3">Very Urgent</option>
										</select>
					                </div>
					            </div>
					            <div class="form-group">
					                <label class="col-md-4 control-label">Name: </label>
					                <div class="col-md-8">
					                	<input  type="text" class="form-control" id="name" name="name" placeholder="Name" value="" autocomplete="off" required>
						             
					                </div>
					            </div>
					            <div class="form-group">
					                <label class="col-md-4 control-label">Phone / Contact: </label>
					                <div class="col-md-8">
					                	<input  type="text" class="form-control" id="phone" name="phone" placeholder="Phone / Contact" value="" autocomplete="off">
						             
					                </div>
					            </div>
					            <div class="form-group">
					                <label class="col-md-4 control-label">Attend (To): </label>
					                <div class="col-md-8">
					                	<select name="attention_to" id="attention_to" class="form-control">
											<option value="0">----Select a Attention to----</option>
											<option value="1">---- Hospital Manager ----</option>
											<option value="2">---- Head of Department ----</option>
											<option value="3">---- Director ----</option>


										</select>
					                </div>
					            </div>
		 					</div>
                  			<div class="col-md-1">
                  			</div>

		 					<div class="col-md-4">
		 						<div class="form-group">
					                <label class="col-md-12 control-label">Inquiry: </label>
					                <div class="col-md-12">
					                	<textarea class="form-control" name="description" id="description"></textarea>
					                </div>
					                
					            </div>
					            <div class="form-group">
					                <label class="col-md-12 control-label">Remarks: </label>
					                <div class="col-md-12">
					                	<textarea class="form-control" name="remarks" id="remarks"></textarea>
					                </div>
					                
					            </div>
					    <div class="form-group">
						    <label class="col-lg-4 control-label">Date: </label>
						       <div class="col-lg-8">
			                      <div class="input-group">
			                    <span class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </span>
			                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date" id="date" placeholder="date" value="<?php echo date('Y-m-d');?>">
			                </div>
						</div>
					</div>
		 						
		 						
		                  		 
					        </div>
					     
					        <div class="col-md-2">
				        	  <div class="center-align" style="margin-top:10px;">
									<button type="submit"  class="btn btn-info btn-sm" > Add Inqueries</button>
								</div>
		 					</div>
			        <?php echo form_close();?>
                </div>
           	</div>
           	<div class="col-md-12" style="height: 40vh; overflow-y: scroll;">
           		<table class='table table-bordered table-hover table-condensed'>
           			<thead>
           				<th></th>
           				<th>Department Name</th>
           				<th>Name</th>
           				<th>Phone</th>
           				<th>Description</th>
           				<th>Remarks</th>
           				<th>Attn</th>
           				<th>Priority</th>
           				<th>Date</th>
           				<th>Personnel</th>
           			</thead>
           			<tbody id="inqueries-list">
           				<!-- <div ></div> -->
           			</tbody>
           			
           		</table>
           	</div>
            
              
           
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">
         $(document).on("submit","form#add-inqueries",function(e)
        {
            alert("changed");
            e.preventDefault(); 

            var form_data = new FormData(this);

            var res = confirm('Are you sure you want to post this inquery ?');

            if(res)
            {
                var config_url = $('#config_url').val();
                // var bill_visit = $('#bill_visit').val();
                var url = "<?php echo base_url();?>admin/post_inquery";       
                
                $.ajax({
                    type:'POST',
                    url: url,
                    data:form_data,
                    dataType: 'text',
                    processData: false,
                    contentType: false,
                    success:function(data){
                        // close_side_bar();
                        display_inquery();
                    },
                    error: function(xhr, status, error) {
                    alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                        
                    }
                });

                 display_inquery();
                return false;

            }
            
        });
</script>
        