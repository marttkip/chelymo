<div class="row statistics">
    <div class="col-md-3 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Visits</h2>
              </header>             
	        	<?php
               $visit_search = $this->session->userdata('visit_collection_search');
        
                if(!empty($visit_search))
                {
                    $date_where = $visit_search;
                }
	        		$where2 = $where_invoices.' AND visit.visit_type = 1 AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 '.$date_where;
					$cash_invoices = $this->reports_model->get_normal_invoices($where2, $table_invoices);

					$where4 = 'visit.visit_type = 1 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 '.$date_where;
					$table4 = 'visit,patients';
					$cash_patients = $this->reception_model->count_items($table4, $where4);


					$where5 = 'visit.visit_type = 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 '.$date_where;
					$table5 = 'visit,patients';
					$aon_patients = $this->reception_model->count_items($table5, $where5);

					$total_copay = $aon_patients*50;
	        	?>
              <!-- Widget content -->
              <div class="panel-body">
                <h5>Visit Breakdown</h5>
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Total Visit</th>
                            <th>Expected  (KES)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>CASH</th>
                            <td><?php echo $cash_patients;?></td>
                            <td><?php echo number_format($cash_invoices,2);?></td>
                        </tr>
                        <tr>
                            <th>COPAY </th>
                            <td><?php echo $aon_patients;?></td>
                            <td><?php echo number_format($total_copay,2);?></td>
                        </tr>

                        <tr>
                            <th><strong>TOTAL</strong> </th>
                            <td><?php echo $aon_patients+$cash_patients;?></td>
                            <td><strong><?php echo number_format($total_copay+$cash_invoices,2);?></strong></td>
                        </tr>
                    </tbody>
                </table>
                <!-- Text -->
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
    <div class="col-md-3 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Cards Summary</h2>
              </header>             
                <?php
                    $where2 = $where_invoices.' AND visit.visit_type = 1 AND visit_charge.visit_charge_delete = 0';
                    $cash_invoices = $this->reports_model->get_normal_invoices($where2, $table_invoices);

                    $where4 = 'patients.patient_type = 0 AND patients.patient_id = visit.patient_id AND visit.close_card = 1 AND visit.visit_delete = 0 '.$date_where;
                    $table4 = 'visit,patients';
                    $closed_cards = $this->reception_model->count_items($table4, $where4);

                    // closed cards value


                    $where22 = $where4.' AND visit_charge.charged = 1 AND visit.visit_id= visit_charge.visit_id';
                    $table_charges = 'visit,patients,visit_charge';
                    $closed_cards_value = $this->reports_model->get_normal_invoices($where22, $table_charges);

                    // end of closed cards value

                    $where3 = 'patients.patient_type = 0 AND patients.patient_id = visit.patient_id AND visit.close_card = 0 AND visit.visit_delete = 0 AND visit.hold_card = 0 '.$date_where;
                    $table3 = 'visit,patients';
                    $open_cards = $this->reception_model->count_items($table3, $where3);

                    // un closed cards value
                    $where23 = $where3.' AND visit_charge.charged = 1 AND visit.visit_id= visit_charge.visit_id';
                    $table_open = 'visit,patients,visit_charge';
                    $open_cards_value = $this->reports_model->get_normal_invoices($where23, $table_open);
                    // end of un closed cards value

                    $where5 = 'patients.patient_type = 0 AND visit.visit_type <> 1 AND patients.patient_id = visit.patient_id  AND visit.hold_card = 1 AND visit.visit_delete = 0 '.$date_where;
                    $table5 = 'visit,patients';
                    $held_cards = $this->reception_model->count_items($table5, $where5);

                    // closed held cards value
                    $where24 = $where5.' AND visit_charge.charged = 1 AND visit.visit_id= visit_charge.visit_id';
                    $table_held = 'visit,patients,visit_charge';
                    $held_cards_value = $this->reports_model->get_normal_invoices($where24, $table_held);

                    // end of  held cards value

                     $where6 = '(patients.patient_type = 1 OR visit.visit_type = 1) AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 '.$date_where;
                    $table6 = 'visit,patients';
                    $cash_cards = $this->reception_model->count_items($table6, $where6);

                    // closed held cards value

                    $where24 = $where6.' AND visit_charge.charged = 1 AND visit.visit_id= visit_charge.visit_id';
                    $table_cash = 'visit,patients,visit_charge';
                    $cash_cards_value = $this->reports_model->get_normal_invoices($where24, $table_cash);

                    // end of  held cards value

                ?>
              <!-- Widget content -->
              <div class="panel-body">
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>No</th>
                            <th>Value (KES)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Closed Cards</th>
                            <td><?php echo $closed_cards;?></td>
                            <td><?php echo number_format($closed_cards_value,2);?></td>
                        </tr>
                        <tr>
                            <th>Open Cards </th>
                            <td><?php echo $open_cards;?></td>
                            <td><?php echo number_format($open_cards_value,2);?></td>
                        </tr>

                        <tr>
                            <th>Held Cards </th>
                            <td><?php echo $held_cards;?></td>
                            <td><?php echo number_format($held_cards_value,2);?></td>
                        </tr>

                        <tr>
                            <th><strong>Cash Cards </strong> </th>
                            <td><?php echo $cash_cards;?></td>
                            <td><?php echo number_format($cash_cards_value,2);?></td>
                        </tr>
                    </tbody>
                </table>
                <!-- Text -->
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
    
    <div class="col-md-6 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown </h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                
                <div class="row">
                    <!-- End Transaction Breakdown -->
                    <?php

                        $yesterday = date('Y-m-d 20:00:00',strtotime("-1 days"));
                        $today = date('Y-m-d 09:00:00');
                        $time_where = ' AND payments.time >= "'.$yesterday.'" AND payments.time <= "'.$today.'"';

                        $where_payments = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND payments.payment_type = 1 AND cancel = 0 AND visit_type.visit_type_id = visit.visit_type AND payments.visit_id = visit.visit_id '.$time_where;
                    	$where3 = $where_payments;


						$normal_payments = $this->reports_model->get_normal_collections($where3, $table_payments);
                        // var_dump($normal_payments);die();
						$staff_query = $this->reports_model->get_all_cash_staff($time_where);
						$night_staffs = '';
						// var_dump($staff_query); die();
                        $total_collection = 0;
						if($staff_query->num_rows() > 0)
						{
							foreach ($staff_query->result() as $key => $value) {
								# code...
								$personnel_fname = $value->personnel_fname;
								$personnel_onames = $value->personnel_onames;
								$personnel_id = $value->personnel_id;

								$cash_collected = $this->reports_model->get_collected_staff_cash($personnel_id,$time_where);
                                $total_collection +=$cash_collected;
								$night_staffs .= '<tr>
					                            <th>'.strtoupper($personnel_fname).'</th>
					                            <td>'.number_format($cash_collected,2).'</td>
					                        </tr>';
							}
						}
                        // day time


                        $yesterday = date('Y-m-d 09:00:00');
                        $today = date('Y-m-d H:i:s');
                        $time_where = ' AND payments.time > "'.$yesterday.'" AND payments.time <= "'.$today.'"';
                        
                        $where_payments = 'visit.visit_delete = 0 AND visit.patient_id = patients.patient_id AND payments.payment_type = 1 AND cancel = 0 AND visit_type.visit_type_id = visit.visit_type AND payments.visit_id = visit.visit_id '.$time_where;
                        $where3 = $where_payments;


                        $normal_payments = $this->reports_model->get_normal_collections($where3, $table_payments);
                        // var_dump($normal_payments);die();
                        $staff_query = $this->reports_model->get_all_cash_staff($time_where);
                        $day_staffs = '';
                        // var_dump($staff_query); die();
                        if($staff_query->num_rows() > 0)
                        {
                            foreach ($staff_query->result() as $key => $value) {
                                # code...
                                $personnel_fname = $value->personnel_fname;
                                $personnel_onames = $value->personnel_onames;
                                $personnel_id = $value->personnel_id;

                                $cash_collected = $this->reports_model->get_collected_staff_cash($personnel_id,$time_where);
                                $total_collection +=$cash_collected;
                                $day_staffs .= '<tr>
                                                <th>'.strtoupper($personnel_fname).'</th>
                                                <td>'.number_format($cash_collected,2).'</td>
                                            </tr>';
                            }
                        }
                    ?>
                    
                    <div class="col-md-4">
                        <h5>Night Summary <br><br> (2000Hrs - 0900Hrs)</h5>
                        <br>
                        <table class="table table-striped table-hover table-condensed">
		                    <thead>
		                        <tr>
		                            <th>Name</th>
		                            <th>Collections (KES)</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        		<?php echo $night_staffs?>
		                    </tbody>
		                </table>
                    </div>
                    <div class="col-md-4">
                        <h5>Day Summary <br><br> (0900Hrs - 2000Hrs)</h5>
                        <br>
                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Collections (KES)</th>
                                </tr>
                            </thead>
                            <tbody>
                                        <?php echo $day_staffs?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <h4>Total Collection</h4>   
                        <h3>Ksh <?php echo number_format($total_collection, 2);?></h3>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>