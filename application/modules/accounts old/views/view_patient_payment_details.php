
<?php 
    echo form_open("accounts/search_document", array("class" => "form-horizontal"));
   
?>
<section class="panel">
    <header class="panel-heading">
            <h4 class="pull-left"><i class="icon-reorder"></i>Search Patient Documents</h4>
          <div class="widget-icons pull-right">
            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
            <a href="#" class="wclose"><i class="icon-remove"></i></a>
          </div>
          <div class="clearfix"></div>
    </header>
      <div class="panel-body">
        <div class="padd">
            <div class="row center-align">              
                <div class="col-md-8">
                	<div class="form-group">
	                    <label class="col-lg-4 control-label">Document Number: </label>
	                    
	                    <div class="col-lg-6">
	                        <input type="text" class="form-control" name="document_number" placeholder="Document Number" value="<?php echo set_value('document_number');?>" >
	                    </div>
	                </div>  
                	
                </div>
                <div class="col-md-4">
                	<div class="center-align">
			            <button class="btn btn-info btn-sm" type="submit">Search Document</button>
			        </div>
                </div>
                            
                
            </div>        
           
        </div>
       
    </div>
</section>
<?php echo form_close();?>

<?php

$edit_status = $this->session->userdata('edit_status');
$can_change = $this->accounts_model->check_if_can_change();

if($edit_status == TRUE AND $can_change == TRUE)
{

	$visit_amount = $this->accounts_model->total_invoice($visit_id);
	?>

		<div class="row">
    		<div class="col-lg-12">
    			<h5><strong>NAME :</strong><?php echo strtoupper($patient_othernames.' '.$patient_surname)?></h5>
    			<h5><strong>DATE :</strong> <?php echo $visit_date?></h5>
    			<h5><strong>BALANCE :</strong> <?php echo number_format($visit_amount,2)?></h5>
    		</div>
    	</div>
    	<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
    	<div class="row">
    		<div class="col-md-7">
    			<section class="panel">
				    <header class="panel-heading">
				            <h4 class="pull-left"><i class="icon-reorder"></i>Invoice</h4>
				          <div class="widget-icons pull-right">
				            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
				            <a href="#" class="wclose"><i class="icon-remove"></i></a>
				          </div>
				          <div class="clearfix"></div>
				    </header>
				    <div class="panel-body">
				        <div class="padd">
				        	<?php
								echo "
								<table align='center' class='table table-striped table-hover table-condensed'>
									<tr>
										<th>#</th>
										<th >Services/Items</th>
										<th >Units</th>
										<th >Unit Cost (Ksh)</th>
										<th > Total</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>		
								"; 

								$total= 0; 
								$count = 0;
								
								$sub_total= 0; 
								$personnel_query = $this->personnel_model->retrieve_personnel();	
									
								if($invoice_items->num_rows() >0){
									
									$visit_date_day = '';
									foreach ($invoice_items->result() as $value => $key1):
										$v_procedure_id = $key1->visit_charge_id;
										$procedure_id = $key1->service_charge_id;
										$service_name = $key1->service_name;
										$date = $key1->date;
										$time = $key1->time;
										$visit_charge_timestamp = $key1->visit_charge_timestamp;
										$visit_charge_amount = $key1->visit_charge_amount;
										$units = $key1->visit_charge_units;
										$procedure_name = $key1->service_charge_name;
										$service_id = $key1->service_id;
										$provider_id = $key1->provider_id;


									
										$sub_total= $sub_total +($units * $visit_charge_amount);
										$visit_date = date('l d F Y',strtotime($date));
										$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
										
										if($visit_date_day != $visit_date)
										{
											
											$visit_date_day = $visit_date;
										}
										else
										{
											$visit_date_day = '';
										}

										

										
										$buttons = "
													
														<div class='modal fade' id='remove_charge".$v_procedure_id."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
														    <div class='modal-dialog' role='document'>
														        <div class='modal-content'>

														            <div class='modal-header'>
														            	<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
														            	<h4 class='modal-title' id='myModalLabel'>Remove Charge</h4>
														            </div>
														            <div class='modal-body'>";
														            	$buttons .=form_open("remove-charge/".$v_procedure_id."/".$visit_id, array("class" => "form-horizontal"));
														            	$buttons .="
														            	<p>Charge Name : ".$procedure_name." </p>	
														            	<p>Charge Amount : KSH. ".$visit_charge_amount." </p>											              
														                <div class='form-group'>
														                    <label class='col-md-4 control-label'>Description: </label>
														                    
														                    <div class='col-md-8'>
														                        <textarea class='form-control' id='remove_description".$v_procedure_id."' name='remove_description".$v_procedure_id."' ></textarea>
														                    </div>
														                </div>
														                <input type='hidden' value='".$this->uri->uri_string()."' name='redirect_url'>
														                <div class='row'>
														                	<div class='col-md-8 col-md-offset-4'>
														                    	<div class='center-align'>
														                        	<button type='submit' class='btn btn-primary' onclick='return confirm(\"Do you want to remove this charge\")'>Remove Charge</button>
														                        </div>
														                    </div>
														                </div>";
														                $buttons .=form_close();
														                $buttons .="
														            </div>
														            <div class='modal-footer'>
														                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
														            </div>
														        </div>
														    </div>
														</div>

													

													";
										


										$counted++;
										
										echo"
												<tr> 
													<td>".$counted."</td>		
													<td >".$procedure_name."</td>";
													echo form_open("update-visit-charge/".$v_procedure_id."/".$visit_id, array("class" => "form-horizontal"));

													echo "<td align='center'>
														<input type='text' id='units".$v_procedure_id."' name='units".$v_procedure_id."' class='form-control' value='".$units."' size='3' />
													</td>

													<td align='center'><input type='text' id='billed_amount".$v_procedure_id."' name='billed_amount".$v_procedure_id."' class='form-control'  size='5' value='".$visit_charge_amount."'></div>
													</td>
													<input type='hidden' value='".$this->uri->uri_string()."' name='redirect_url'>
													<td >".number_format($visit_charge_amount*$units)."</td>
													<td>
														<button class='btn btn-sm btn-primary' type='submit' onclick='return confirm(\"Do you want to update this charge\")'><i class='fa fa-pencil'></i></button>
													</td>";

													echo form_close();
													echo "
													<td>
						                            	<button type='button' class='btn btn-sm btn-default' data-toggle='modal' data-target='#remove_charge".$v_procedure_id."'><i class='fa fa-times'></i></button>
						                            </td>
						                            <td>
														<a class='btn btn-sm btn-warning' href='".site_url()."reverse-charge/".$v_procedure_id."/".$visit_id."'  onclick='return confirm(\"Do you want to reverse this charge ?\")'><i class='fa fa-refresh'></i></a>
													</td>
													".$buttons."
												</tr>	
										";
										
										$visit_date_day = $visit_date;
										endforeach;
										

								}		
								echo"
								 </table>
								";
								?>
				        </div>
				    </div>
				</section>
    			
    		</div>
    		<div class="col-md-5">
    			<section class="panel">
				    <header class="panel-heading">
				            <h4 class="pull-left"><i class="icon-reorder"></i>Receipts</h4>
				          <div class="widget-icons pull-right">
				            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
				            <a href="#" class="wclose"><i class="icon-remove"></i></a>
				          </div>
				          <div class="clearfix"></div>
				    </header>
				    <div class="panel-body">
				        <div class="padd">
				        	<table class="table table-hover table-bordered col-md-12">
								<thead>
									<tr>
										<th>#</th>
										<th>Time</th>
										<th>Method</th>
										<th>Amount</th>
										<th colspan="2"></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$payments_rs = $this->accounts_model->payments($visit_id);
									$total_payments = 0;
									$total_amount = ($total + $debit_note_amount) - $credit_note_amount;
									$total_waiver = 0;
									if(count($payments_rs) > 0)
									{
										$x=0;

										foreach ($payments_rs as $key_items):
											$x++;
											$payment_method = $key_items->payment_method;

											$time = $key_items->time;
											$payment_type = $key_items->payment_type;
											$payment_id = $key_items->payment_id;
											$payment_status = $key_items->payment_status;
											$payment_service_id = $key_items->payment_service_id;
											$service_name = '';

											if($payment_type == 2 && $payment_status == 1)
											{
												$waiver_amount = $key_items->amount_paid;
												$total_waiver += $waiver_amount;
											}
											
											if($payment_type == 1 && $payment_status == 1)
											{
												$amount_paid = $key_items->amount_paid;
												$amount_paidd = number_format($amount_paid,2);
												
												if(count($item_invoiced_rs) > 0)
												{
													foreach ($item_invoiced_rs as $key_items):
													
														$service_id = $key_items->service_id;
														
														if($service_id == $payment_service_id)
														{
															$service_name = $key_items->service_name;
															break;
														}
													endforeach;
												}
											
												//display DN & CN services
												if((count($payments_rs) > 0) && ($service_name == ''))
												{
													foreach ($payments_rs as $key_items):
														$payment_type = $key_items->payment_type;
														
														if(($payment_type == 2) || ($payment_type == 3))
														{
															$payment_service_id2 = $key_items->payment_service_id;
															
															if($payment_service_id2 == $payment_service_id)
															{
																$service_name = $this->accounts_model->get_service_detail($payment_service_id);
																break;
															}
														}
														
													endforeach;
												}
												?>
												<tr>
													<td><?php echo $x;?></td>
													<td><?php echo $time;?></td>
													<td><?php echo $payment_method;?></td>
													<td><?php echo $amount_paidd;?></td>
													<td><a href="<?php echo site_url().'accounts/print_single_receipt/'.$payment_id;?>" class="btn btn-small btn-warning" target="_blank"><i class="fa fa-print"></i></a></td>
													

														<td>
	                                                    	<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>
															<!-- Modal -->
															<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															    <div class="modal-dialog" role="document">
															        <div class="modal-content">
															            <div class="modal-header">
															            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
															            </div>
															            <div class="modal-body">
															            	<?php echo form_open("accounts/cancel_payment/".$payment_id.'/'.$visit_id, array("class" => "form-horizontal"));?>
															                <div class="form-group">
															                    <label class="col-md-4 control-label">Action: </label>
															                    
															                    <div class="col-md-8">
															                        <select class="form-control" name="cancel_action_id">
															                        	<option value="">-- Select action --</option>
															                            <?php
															                                if($cancel_actions->num_rows() > 0)
															                                {
															                                    foreach($cancel_actions->result() as $res)
															                                    {
															                                        $cancel_action_id = $res->cancel_action_id;
															                                        $cancel_action_name = $res->cancel_action_name;
															                                        
															                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
															                                    }
															                                }
															                            ?>
															                        </select>
															                    </div>
															                </div>
															                <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
															                
															                <div class="form-group">
															                    <label class="col-md-4 control-label">Description: </label>
															                    
															                    <div class="col-md-8">
															                        <textarea class="form-control" name="cancel_description"></textarea>
															                    </div>
															                </div>
															                
															                <div class="row">
															                	<div class="col-md-8 col-md-offset-4">
															                    	<div class="center-align">
															                        	<button type="submit" class="btn btn-primary">Save action</button>
															                        </div>
															                    </div>
															                </div>
															                <?php echo form_close();?>
															            </div>
															            <div class="modal-footer">
															                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															            </div>
															        </div>
															    </div>
															</div>

	                                                    </td>
	                                              
												</tr>
												<?php
												$total_payments =  $total_payments + $amount_paid;
											}
										endforeach;

										?>
										<tr>
											<td colspan="3"><strong>Total : </strong></td>
											<td><strong> <?php echo number_format($total_payments,2);?></strong></td>
										</tr>
										<?php
									}
									
									else
									{
										?>
										<tr>
											<td colspan="4"> No payments made yet</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
				    </div>
				</section>    			
    		</div>    		
    	</div>

	<?php
}
?>