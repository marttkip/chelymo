 <section class="panel panel-primary">
    <header class="panel-heading">
        <h2 class="panel-title pull-right"></h2>
        <h2 class="panel-title">Search Visits</h2>
    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php
            echo form_open("accounts/search_visits", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Patient Number: </label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="patient_number" placeholder="Patient Number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Names: </label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="surname" placeholder="Names">
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">   
                    <div class="center-align">
                        <button type="submit" class="btn btn-info btn-sm">Search</button>
                    </div>
                </div>
            </div>
            <br/>
            
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>