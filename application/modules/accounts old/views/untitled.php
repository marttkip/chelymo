<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = 'Admin';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);


$today = date('d/m/Y',strtotime($payment_date));



$this->db->where('pos_order.pos_order_id = pos_order_return.pos_order_id AND pos_order_return.pos_order_return_id = '.$pos_order_return_id.' AND pos_order.pos_order_id ='.$pos_order_id);
$this->db->join('customer','customer.customer_id = pos_order.customer_id','left');
$this->db->join('order_invoice','order_invoice.pos_order_id = pos_order.pos_order_id','left');
$query = $this->db->get('pos_order,pos_order_return');
// var_dump($query);die();
$customer_id = 0;
$name = '';


if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		# code...
		$customer_id = $value->customer_id;
		$customer_name = $value->customer_name;
		$customer_email = $value->customer_email;
		$customer_phone = $value->customer_phone;
        $visit_date = date('jS F Y',strtotime($value->order_return_date));
		$phone = $value->phone;
		$order_invoice_number = $value->order_invoice_number;
        $pos_order_return_number = $value->pos_order_return_number;
		$lpo_number = $value->lpo_number;

		if(empty($phone) && empty($lpo_number))
        {
            $phone = '&nbsp;';
        }
        else if(empty($phone) && !empty($lpo_number))
        {
        	$phone = $lpo_number;
        }
        else
        {
        	$phone = '&nbsp;';
        }
	}
}

// var_dump($order_invoice_number);die();

$name .= '<br>'.$customer_name.'<br>'.$customer_email.'<br>'.$customer_phone;


?>

<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = 'Admin';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);


$today = date('d/m/Y',strtotime($payment_date));

$this->db->where('pos_order.pos_order_id = pos_order_return.pos_order_id AND pos_order_return.pos_order_return_id = '.$pos_order_return_id.' AND pos_order.pos_order_id ='.$pos_order_id);
$this->db->join('customer','customer.customer_id = pos_order.customer_id','left');
$this->db->join('order_invoice','order_invoice.pos_order_id = pos_order.pos_order_id','left');
$query = $this->db->get('pos_order,pos_order_return');
// var_dump($query);die();
$customer_id = 0;
$name = '';


if($query->num_rows() > 0)
{
    foreach ($query->result() as $key => $value) {
        # code...
        $customer_id = $value->customer_id;
        $customer_name = $value->customer_name;
        $customer_email = $value->customer_email;
        $customer_phone = $value->customer_phone;
        $visit_date = date('jS F Y',strtotime($value->order_return_date));
        $phone = $value->phone;
        $order_invoice_number = $value->order_invoice_number;
        $pos_order_return_number = $value->pos_order_return_number;
        $lpo_number = $value->lpo_number;

        if(empty($phone) && empty($lpo_number))
        {
            $phone = '&nbsp;';
        }
        else if(empty($phone) && !empty($lpo_number))
        {
            $phone = $lpo_number;
        }
        else
        {
            $phone = '&nbsp;';
        }
    }
}

// var_dump($order_invoice_number);die();

$name .= '<br>'.$customer_name.'<br>'.$customer_email.'<br>'.$customer_phone;


?>

<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            p
            {
                /*margin: 0 0 0px !important;*/
                margin-bottom: 10px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:10.5%;  float:left;}
            .col-print-2 {width:12%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:55.5%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:12px;
                padding-top: 15px;
                margin-top:10px;
                margin-bottom: 10px !important;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }

            table tr td
            {
                padding: 5px !important;
            }
            
            .padd
            {
                padding:20px;
            }

            @page { size: A5;font-size: 15px !important;}
            @media print
            {
                @page { size: A5;font-size: 15px !important;}
            }

            @media print
            {
                td.col1
                {
                    height: 200px !important;width:10% !important;
                }
                td.col2
                {
                    height: 200px !important;width:70% !important;
                }
                td.col3
                {
                    height: 200px !important;width:10% !important;
                }
                td.col4
                {
                    height: 200px !important;width:10% !important;
                }
            }

            .pull-left
            {
                text-align: left !important;
            }
            
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="col-md-12">
            <div class="padd sheet padding-120mm" >
           

            
            <div class="row" style="margin-top: 123px;">
                <div class="col-md-12">
                    <div class="col-print-4 left-align" style="">
                       <!-- <div class="center-align">&nbsp;</div> -->
                       <!-- <strong>To:</strong> -->

                       <?php echo $name; ?> 
                       
                    </div>

                    <div class="col-print-4">
                       <h3 class="center-align" style="padding-top:20px !important;margin-top:5px !important;margin-left: -24px !important;">CREDIT NOTE</h3>
                    </div>
                     <div class="col-print-4">
                        <div style="margin-left: 50%; text-align: right; margin-top:-36px;" >
                             <p><?php echo $pos_order_return_number; ?> </p>
                          
                            <p><?php echo $visit_date; ?> </p>
                         
                            <p><?php echo $phone; ?></p>
                           
                            <p><?php echo $served_by; ?> </p>
                        </div>
                       
                        
                        
                    </div>
                </div>
            </div>
              <?php


                $order_invoice_number ='';
                $preauth_date = date('Y-m-d');
                $preauth_amount = '';
                
                // $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id);
                $visit__rs1 = $this->pos_model->get_visit_charges_returned($pos_order_id,$order_invoice_id,$pos_order_return_id);  
                
                // var_dump($pos_order_id);die();
                // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
                $visit_type_id = 1;
                $close_card = 3;
                $total_amount= 0; 
                $days = 0;
                $count = 0;
                $item_list = '';
                $description_list = "";
                $quantity_list = "";
                $rate_list = "";
                $amount_list = "";
                $total= 0;  
                $total_units= 0;
                $number = 0;
                $vat_charged = 0;
                $vat_charged =0;
                if($visit__rs1->num_rows() > 0)
                {                       
                    foreach ($visit__rs1->result() as $key1 => $value) :
                        $v_procedure_id = $value->pos_order_item_id;
                        $procedure_id = $value->service_charge_id;
                        $product_code = $value->product_code;
                        $pos_order_item_return_id = $value->pos_order_item_return_id;
                        $product_order_item_amount = $value->product_order_item_amount;
                        $pos_order_item_amount = $value->pos_order_item_amount;
                        $units = $value->units;
                        $procedure_name = $value->service_charge_name;
                        $service_id = $value->service_id;
                        $vatable = $value->vatable;
                        $product_id = $value->product_id;
                        $discount = $value->discount;
                        $vat_added = $value->vat_added;
                        $part_no = $value->part_no;


                        if($vat_added == 1)
                        {
                            $vat = 1.14;
                            $checked_box = 'checked';
                            $no_checked_box = '';
                        }
                        else
                        {
                            $vat = 1;
                            $checked_box = '';
                            $no_checked_box = 'checked';
                        }

                        // var_dump($discount);die();
                        // $order_invoice_id = $value->order_invoice_id;
                        // $visit_type_id = 1;
                        $total= $total +(($units * $product_order_item_amount) *$vat) - $discount;



                        if($order_invoice_id > 0)
                        {
                            $text_color = "success";
                        }
                        else
                        {
                            $text_color = 'default';
                        }

                    
                        $checked="";
                        $number++;

                        $vat_charged += (($units * $product_order_item_amount)*$vat) / 1.14;


                       

                        $item_list .= strtoupper($product_code).'<br> ';
                        $description_list .= strtoupper($procedure_name)."<br>";
                        $quantity_list .= $units."<br>";
                        $rate_list .= number_format($product_order_item_amount,2)."<br>";
                        $amount_list .= number_format(((($units * $product_order_item_amount))-$discount),2)."<br>";

                        endforeach;

                }
                else
                {
                    
                }
                
                $preauth_amount = $total;


                $payments_value = $total_payments;
                $balance = $preauth_amount - $total_payments;

                ?>

            <div class="row" style="margin-top: 20px">
                    <div class="col-print-12" style="height: 190px !important;">
                        <div class="col-print-2">
                            <?php echo $item_list;?> 
                        </div>
                        <div class="col-print-7" style="padding-left: 10px !important;">
                            <?php echo $description_list;?> 
                        </div>
                        <div class="col-print-1" style="width:9% !important;">
                            <?php echo $quantity_list;?> 
                        </div>
                        <div class="col-print-1" >
                            <?php echo $rate_list;?> 
                        </div>
                        <div class="col-print-1">
                            <?php echo $amount_list;?> 
                        </div>
                    </div>
                    <div class="row" style="margin-top: 135px;">
                        <div class="col-md-12">
                            <div class="col-print-4 left-align" style="">
                               &nbsp;
                               
                            </div>

                            <div class="col-print-4">
                              &nbsp;
                            </div>
                            <div class="col-print-4">
                           <div style="margin-left: 48%; text-align: right; margin-top:-19px; margin-right:15px !important;" >
                                     <p> <?php echo number_format($vat_charged,2); ?></p>
                                  
                                    <p><?php echo number_format($preauth_amount - $vat_charged,2); ?></p>
                                 
                                    <p><?php echo number_format($preauth_amount,2); ?> </p>
                                   
                                </div>
                               
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
                  
            
            </div>
        </div>
       
    </body>
    
