       <?php
            $reach = '';
            $where = 'diseases.diseases_id > 0 AND diseases.diseases_name != "Suspected Malaria"';
            $table = 'diseases';
            $this->db->where($where);
            $diseases_list = $this->db->get($table);

            foreach ($diseases_list->result() as $row)
            {
                $diseases_name = $row->diseases_name;
                $diseases_id = $row->diseases_id;
                $diseases_code = $row->diseases_code;
                $reach .='<option value="'.$diseases_id.'">'.$diseases_name.' '.$diseases_code.' </option>';
            }
       ?>
       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-morbidity", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	<div class="col-md-4">

            		<div class="form-group">
                        <label class="col-md-4 control-label">Range: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="mobidity_type">
                            	<option value="">---Select type of Morbidity---</option>
                                <option value="1"> > 5 Years</option>
                        		<option value="2"> < 5 Years </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Year: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                        		<select class="form-control" name="year">
                        			<option value="0">---Select Year---</option>
                        			<option value="2016">2016</option>
                        			<option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                        		</select>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Month: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <select class="form-control" name="month">
                                	<option value="">---Select Month---</option>
                        			<option value="01">Jan</option>
                        			<option value="02">Feb</option>
                        			<option value="03">Mar</option>
                        			<option value="04">Apr</option>
                        			<option value="05">May</option>
                        			<option value="06">Jun</option>
                        			<option value="07">Jul</option>
                        			<option value="08">Aug</option>
                        			<option value="09">Sept</option>
                        			<option value="10">Oct</option>
                        			<option value="11">Nov</option>
                        			<option value="12">Dec</option>
                        		</select>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <br>
            <div class="row">
            	<div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Disease List: </label>
                            
                            <div class="col-md-8">
                                <select id='diseases_id' name='diseases_id' class='form-control custom-select ' >
                                    <option value="">---Select type of disease---</option>
                                    <?php echo $reach?>
                                </select>
                            </div>
                        </div>
            		<div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search Report</button>
                        </div>
                </div>
            		
            	</div>
            	
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>
<script text="javascript">
$(function() {
    $("#diseases_id").customselect();
});
$(document).ready(function(){

    $(function() {
        $("#diseases_id").customselect();
    });
});
</script>