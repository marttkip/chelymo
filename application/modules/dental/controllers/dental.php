<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/auth/controllers/auth.php";
error_reporting(0);
class Dental extends auth
{	
	var $document_upload_path;
	var $document_upload_location;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->library('image_lib');

		$this->document_upload_path = realpath(APPPATH . '../assets/document_uploads');
		$this->document_upload_location = base_url().'assets/document_uploads/';
		
		$this->load->model('dental_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('reception/reception_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('reception/database');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/file_model');
		$this->load->model('online_diary/rooms_model');
		// $this->load->model('medical_admin/medical_admin_model');
		// $this->load->model('pharmacy/pharmacy_model');
		
		$this->load->model('auth/auth_model');
		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// }
	}
	public function index()
	{
		$this->session->unset_userdata('visit_search');
		$this->session->unset_userdata('patient_search');
		
		$where = 'visit_department.visit_id = visit.visit_id AND visit_department.department_id = 2 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND visit.close_card = 0 AND visit.visit_date = \''.date('Y-m-d').'\' AND visit.personnel_id = '.$this->session->userdata('personnel_id');
		
		$table = 'visit_department, visit, patients';
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, 6, 0);
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		
		$v_data['visit'] = 0;
		$v_data['doctor_appointments'] = 1;
		$v_data['department'] = 2;
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('nurse/nurse_dashboard', $v_data, TRUE);
		
		$data['title'] = 'Dashboard';
		$data['sidebar'] = 'dental_sidebar';
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function dental_queue($page_name = NULL)
	{
		// this is it
		
		$where = 'visit_department.visit_id = visit.visit_id AND visit_department.department_id = 10 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND visit.close_card = 0 AND visit.visit_date = \''.date('Y-m-d').'\' AND visit.visit_type = visit_type.visit_type_id';
		
		$table = 'visit_department, visit, patients, visit_type';
		$visit_search = $this->session->userdata('visit_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		if($page_name != NULL)
		{
			$segment = 4;
		}
		
		else
		{
			$segment = 3;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'dental/dental_queue/'.$page_name;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Dental Queue';
		$v_data['title'] = 'Dental Queue';
		$v_data['module'] = 1;
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('dental_queue', $v_data, true);
		
		$data['sidebar'] = 'dental_sidebar';
		
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	public function queue_cheker($page_name = NULL)
	{
		$where = 'visit_department.visit_id = visit.visit_id AND visit_department.department_id = 2 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND visit.close_card = 0 AND visit.visit_date = \''.date('Y-m-d').'\' AND visit.personnel_id = '.$this->session->userdata('personnel_id');
		$table = 'visit_department, visit, patients';
		$items = "*";
		$order = "visit.visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}

	}
	public function patient_card($visit_id, $mike = NULL)
	{
		// $str = "ABC"; 
		// $n = strlen($str); 
		// $var = $this->dental_model->permute($str, 0, $n - 1); 

		

		// var_dump($var);die();




		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$visit_type = $patient['visit_type'];
		$patient_type = $patient['patient_type'];
		$patient_othernames = $patient['patient_othernames'];
		$patient_surname = $patient['patient_surname'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$v_data['age'] = $age;
		$gender = $patient['gender'];
		$account_balance = $patient['account_balance'];
		$phone_number = $patient['patient_phone_number'];
		$patient_id = $patient['patient_id'];
		$visit_type_name = $patient['visit_type_name'];
		$chart_type = $patient['chart_type'];
		$v_data['patient_details'] = $this->reception_model->get_patient_data($patient_id);
		$v_data['insurance'] = $this->reception_model->get_insurance();
		$v_data['relationships'] = $this->reception_model->get_relationship();
		// $v_data['religions'] = $this->reception_model->get_religion();
		// $v_data['civil_statuses'] = $this->reception_model->get_civil_status();
		// $v_data['titles'] = $this->reception_model->get_title();
		$v_data['genders'] = $this->reception_model->get_gender();

		$insurance_company = $this->reception_model->get_patient_insurance_company($patient_id);
		$v_data['document_types'] = $this->dental_model->all_document_types();
		$v_data['doctor'] = $this->reception_model->get_doctor();
		$v_data['patient_other_documents'] = $this->dental_model->get_document_uploads($patient_id);

		$personnel_id = $this->session->userdata('personnel_id');
		$department_id = $this->reception_model->get_personnel_department($personnel_id);
		// var_dump($department_id); die();
		$personnel_check = FALSE;
		if($department_id == 4)
		{
			//  check if the doctor is the one seing the patient 
			$this->db->where('visit_id ='.$visit_id.' AND personnel_id ='.$personnel_id.' AND visit.close_card = 0');
			$query = $this->db->get('visit');
			if($query->num_rows() == 1)
			{
				$update_array['close_card'] = 4;
				$this->db->where('visit_id',$visit_id);
				$this->db->update('visit',$update_array);
				$personnel_check = TRUE;
			}

			

			
			
		}
		$cash_balance = $this->accounts_model->get_cash_balance($patient_id);
		$insurance_balance = $this->accounts_model->get_insurance_balance($patient_id);
		
		// $v_data['patient'] = '
		// 						<div class="row">
		// 							<div class="col-md-5">
		// 								Name: <span style="font-weight: normal;">
		// 										'.strtoupper($patient_surname).' '.strtoupper($patient_othernames).'
		// 										</span>
		// 								Visit.: <span style="font-weight: normal;">'.$visit_type_name.' </span>
									
		// 								Age.: <span style="font-weight: normal;">'.$age.' Years </span>
		// 							</div>
		// 							<div class="col-md-5">
		// 								Balance Cash : <span style="font-weight: bold;">'.$cash_balance.'</span>  
										
		// 								Insurance : <span style="font-weight: bold;">'.$insurance_balance.'</span> 
										
		// 								<a href="'.site_url().'administration/individual_statement/'.$patient_id.'/2" class="btn btn-sm btn-warning" target="_blank">Statement</a>

		// 							</div>
		// 							<div class="col-md-2">
		// 							 <a href="'.site_url().'queue"  class="btn btn-sm btn-info " ><i class="fa fa-arrow-left"></i> Back to Queue </a>
		// 							</div>
		// 						</div>';
		$v_data['patient'] = $patient;
		
		$v_data['mike'] = $mike;
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['chart_type'] = $chart_type;
		$v_data['dental'] = 1;

		// $order = 'service_charge.service_charge_name';
		// $where = 'service_charge.service_id = service.service_id AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1 AND service_charge.service_charge_delete = 0';

		// $table = 'service_charge,visit_type,service';
		// $config["per_page"] = 0;
		// $procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		// $rs9 = $procedure_query->result();
		// $procedures = '';
		// foreach ($rs9 as $rs10) :


		// $procedure_id = $rs10->service_charge_id;
		// $proced = $rs10->service_charge_name;
		// $visit_type = $rs10->visit_type_id;
		// $visit_type_name = $rs10->visit_type_name;

		// $stud = $rs10->service_charge_amount;

		//     $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		// endforeach;

		// $v_data['services_list'] = $procedures;
		$v_data['personnel_check'] = $personnel_check;

		// var_dump($personnel_check); die();
		
		$data['content'] = $this->load->view('patient_card', $v_data, true);
		
		$data['title'] = 'Patient Card';
		
		$data['sidebar'] = 'dental_sidebar';
		
		if(($mike != NULL) && ($mike != 'a')){
			$this->load->view('admin/templates/general_page', $data);	
		}else{
			$this->load->view('admin/templates/general_page', $data);	
		}
	}
	public function search_dental_billing($visit_id)
	{
		$this->form_validation->set_rules('search_item', 'Search', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$search = ' AND service_charge_name LIKE \'%'.$this->input->post('search_item').'%\'';
			$this->session->set_userdata('billing_search', $search);
		}
		
		$this->dental_services($visit_id);
	}
	public function close_dental_billing_search($visit_id)
	{
		$this->session->unset_userdata('billing_search');
		$this->dental_services($visit_id);
	}
	function dental_services($visit_id)
	{
		//check patient visit type
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
			# code...
			  $visit_t = $rs1->visit_type;
		  }
		}
		
		$order = 'service_charge_name';
		
		$where = 'service.service_id = service_charge.service_id AND service.service_name ="Dental Procedures" AND service_charge.service_charge_status = 1 ';
		$billing_search = $this->session->userdata('billing_search');
		
		if(!empty($billing_search))
		{
			$where .= $billing_search;
		}
		
		$table = 'service,service_charge';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'dental/dental_services/'.$visit_id;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 15;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->nurse_model->get_procedures($table, $where, $config["per_page"], $page, $order);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Billing List';
		$v_data['title'] = 'Billing List';
		
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('billing_list', $v_data, true);
		
		$data['title'] = 'Billing List';
		$this->load->view('admin/templates/no_sidebar', $data);	
	}

	public function view_billing($visit_id)
	{
		$personnel_id = $this->session->userdata('personnel_id');
		// $this->db->where('visit_id ='.$visit_id.' AND personnel_id ='.$personnel_id.' AND visit.close_card = 4');
		$this->db->where('visit_id ='.$visit_id.' ');
		$query = $this->db->get('visit');
		$personnel_check = FALSE;
		if($query->num_rows() == 1)
		{
			$row = $query->row();

			$patient_id = $row->patient_id;
			$personnel_check = TRUE;
		}


		// var_dump($patient_id);die();
		$data = array('visit_id'=>$visit_id,'personnel_check'=>$personnel_check,'patient_id'=>$patient_id);

		
		$page_info =  $this->load->view('view_billing',$data);

		echo $page_info;
	}
	public function view_patient_notes($patient_id)
	{
		$personnel_id = $this->session->userdata('personnel_id');
		// $this->db->where('visit_id ='.$visit_id.' AND personnel_id ='.$personnel_id.' AND visit.close_card = 4');
	

		// var_dump($patient_id);die();
		$data = array('visit_id'=>NULL,'personnel_check'=>$personnel_check,'patient_id'=>$patient_id);

		
		$page_info =  $this->load->view('view_billing',$data);

		echo $page_info;
	}
	public function view_quotation($visit_id)
	{
		$personnel_id = $this->session->userdata('personnel_id');
		
		$this->db->where('visit_id ='.$visit_id.' AND visit.close_card = 4');
		$query = $this->db->get('visit');
		$personnel_check = FALSE;
		if($query->num_rows() == 1)
		{
			$personnel_check = TRUE;
		}
		$data = array('visit_id'=>$visit_id,'personnel_check'=>$personnel_check);

		$this->load->view('view_quotation',$data);
	}

	public function view_patients_quotation($visit_id)
	{
		$personnel_id = $this->session->userdata('personnel_id');
			

		$visit_rs = $this->accounts_model->get_visit_details($visit_id);
		$visit_type_id = 0;
		if($visit_rs->num_rows() > 0)
		{
			foreach ($visit_rs->result() as $key => $value) {
				# code...
				$patient_id = $value->patient_id;
			}
		}

		
		$this->db->where('patient_id ='.$patient_id.' AND visit.visit_id IN (SELECT visit_id FROM visit_quotation) ');
		$query = $this->db->get('visit');
		
		$data = array('visit_id'=>$visit_id,'query'=>$query,'patient_id'=>$patient_id);

		$this->load->view('view_patient_quotations',$data);
	}
	function billing_service($service_id,$visit_id,$suck){
		$data = array('procedure_id'=>$service_id,'visit_id'=>$visit_id,'suck'=>$suck);
		$this->load->view('billing/billing',$data);	
	}
	public function billing_total($procedure_id,$units,$amount){
		$visit_data = array('visit_charge_units'=>$units,'charged'=>1);
		$this->db->where(array("visit_charge_id"=>$procedure_id));
		$this->db->update('visit_charge', $visit_data);
	}
	public function save_other_deductions($visit_id)
	{
		$visit_data = array('payment_info'=>$this->input->post('notes'));
		$this->db->where(array("visit_id"=>$visit_id));
		$this->db->update('visit', $visit_data);

	}
	public function save_other_patient_sickoff($visit_id)
	{
		$visit_data = array('sick_leave_note'=>$this->input->post('notes'));
		$this->db->where(array("visit_id"=>$visit_id));
		$this->db->update('visit', $visit_data);

	}
	function delete_billing($procedure_id)
	{
		$this->db->where(array("visit_charge_id"=>$procedure_id));
		$this->db->delete('visit_charge', $visit_data);
	}
	public function send_to_accounts($visit_id,$patient_id)
	{

		// var_dump($patient_id);die();

		$checker_one = $this->dental_model->check_if_notes_written($visit_id,$patient_id);
		$checker_two = TRUE;
		$checker_one = TRUE;
		if($checker_one == TRUE)
		{

			if($this->reception_model->set_visit_department($visit_id, 6))
			{
				// $update_array['close_card'] = 3;
				// $this->db->where('visit_id',$visit_id);
				// $this->db->update('visit',$update_array);

				$this->session->set_userdata('success_message', 'Patient has been sent successfully to accounts office');
				echo json_encode("Patient has been sent successfully to accounts office");
				redirect("queue");
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry something went wrong please try to send the patient again');
				echo json_encode("Sorry something went wrong please try to send the patient again");
				redirect("dental/".$visit_id);
			}

		}
		else
		{
			echo json_encode("Sorry, you have to write todays notes to be able to send to accounts");
			$this->session->set_userdata('error_message', 'Sorry, you have to write todays notes to be able to send to accounts');
			redirect("dental/".$visit_id);
		}

		
		

	}
	public function send_to_pharmacy($visit_id)
	{
		if($this->reception_model->set_visit_department($visit_id, 5))
		{
			redirect("dental/dental_queue");
		}
		else
		{
			FALSE;
		}
	}
	public function send_to_labs($visit_id)
	{
		if($this->reception_model->set_visit_department($visit_id, 4))
		{
			redirect("dental/dental_queue");
			
		}
		else
		{
			FALSE;
		}
	}
	// new things ending
	public function save_current_notes($visit_id)
	{
		$notes=$this->input->post('oral_examination'.$visit_id);
		
		
		$rs = $this->nurse_model->get_oe_notes($visit_id);
		$num_oe_notes = count($rs);
		if($num_oe_notes == 0){	

			$visit_data = array('visit_id'=>$visit_id,'oe_description'=>$notes);
			$this->db->insert('visit_oe', $visit_data);

		}
		else {

			$visit_data = array('oe_description'=>$notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_oe', $visit_data);
		}



		// hpco
		$hpco_notes=$this->input->post('hpco'.$visit_id);

		$rs = $this->nurse_model->get_hpco_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'hpco_description'=>$hpco_notes);
			$this->db->insert('visit_hpco', $visit_data);

		}
		else {
			$visit_data = array('hpco_description'=>$hpco_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_hpco', $visit_data);
		}

		// tca
		$tca_notes=$this->input->post('tca'.$visit_id);

		$rs = $this->nurse_model->get_tca_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'tca_description'=>$tca_notes);
			$this->db->insert('visit_tca', $visit_data);

		}
		else {
			$visit_data = array('tca_description'=>$tca_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_tca', $visit_data);
		}

		// xra

		$rx_notes= $this->input->post('rx'.$visit_id);

		$rs = $this->nurse_model->get_rxdone_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'rx_description'=>$rx_notes);
			$this->db->insert('visit_rx', $visit_data);

		}
		else {
			$visit_data = array('rx_description'=>$rx_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_rx', $visit_data);
		}
		// save investigations

		$investigations_notes=$this->input->post('investigations'.$visit_id);

		$rs = $this->nurse_model->get_investigations_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'investigation'=>$investigations_notes);
			$this->db->insert('visit_investigations', $visit_data);

		}
		else {
			$visit_data = array('investigation'=>$investigations_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_investigations', $visit_data);
		}

		
		redirect('dental/patient_card/'.$visit_id);
	}
	public function save_new_notes($visit_id)
	{
		// hpco
		$hpco_notes=$this->input->post('hpco'.$visit_id);

		$rs = $this->nurse_model->get_hpco_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'hpco_description'=>$hpco_notes);
			$this->db->insert('visit_hpco', $visit_data);

		}
		else {
			$visit_data = array('hpco_description'=>$hpco_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_hpco', $visit_data);
		}

		// tca
		$tca_notes=$this->input->post('tca'.$visit_id);

		$rs = $this->nurse_model->get_tca_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'tca_description'=>$tca_notes);
			$this->db->insert('visit_tca', $visit_data);

		}
		else {
			$visit_data = array('tca_description'=>$tca_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_tca', $visit_data);
		}

		

		// xra

		$rx_notes= $this->input->post('rx'.$visit_id);

		$rs = $this->nurse_model->get_rxdone_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'rx_description'=>$rx_notes);
			$this->db->insert('visit_rx', $visit_data);

		}
		else {
			$visit_data = array('rx_description'=>$rx_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_rx', $visit_data);
		}
		// occlusal report

		$occlusal_exam=$this->input->post('occlusal_exam'.$visit_id);

		$rs = $this->nurse_model->get_occlusal_exam_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'occlusal_exam_description'=>$occlusal_exam);
			$this->db->insert('visit_occlusal_exam', $visit_data);

		}
		else {
			$visit_data = array('occlusal_exam_description'=>$occlusal_exam);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_occlusal_exam', $visit_data);
		}

		// save findings

		$findings_notes=$this->input->post('findings'.$visit_id);

		$rs = $this->nurse_model->get_findings_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'finding_description'=>$findings_notes);
			$this->db->insert('visit_finding', $visit_data);

		}
		else {
			$visit_data = array('finding_description'=>$findings_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_finding', $visit_data);
		}

		// save histories

		$past_medical_hx=$this->input->post('past_medical_hx'.$visit_id);
		$past_dental_hx=$this->input->post('past_dental_hx'.$visit_id);

		$rs = $this->nurse_model->get_histories_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'past_medical_history'=>$past_medical_hx, 'past_dental_history'=>$past_dental_hx);
			$this->db->insert('visit_history', $visit_data);

		}
		else {
			$visit_data = array('past_medical_history'=>$past_medical_hx, 'past_dental_history'=>$past_dental_hx);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_history', $visit_data);
		}

		// save oc

		$filled=$this->input->post('filled'.$visit_id);
		$missing=$this->input->post('missing'.$visit_id);
		$decayed=$this->input->post('decayed'.$visit_id);
		$soft_tissue=$this->input->post('soft_tissue'.$visit_id);
		$general=$this->input->post('general'.$visit_id);
		$others=$this->input->post('others'.$visit_id);

		$rs = $this->nurse_model->get_oc_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'filled'=>$filled, 'missing' => $missing, 'decayed' => $decayed, 'soft_tissue'=>$soft_tissue , 'general'=>$general , 'other'=>$others);
			$this->db->insert('visit_oc', $visit_data);

		}
		else {
			$visit_data = array('filled'=>$filled, 'missing' => $missing, 'decayed' => $decayed, 'soft_tissue'=>$soft_tissue , 'general'=>$general , 'other'=>$others);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_oc', $visit_data);
		}

		// save investigations

		$investigations_notes=$this->input->post('investigations'.$visit_id);

		$rs = $this->nurse_model->get_investigations_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'investigation'=>$investigations_notes);
			$this->db->insert('visit_investigations', $visit_data);

		}
		else {
			$visit_data = array('investigation'=>$investigations_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_investigations', $visit_data);
		}

		// plan

		$plan_notes=$this->input->post('plan'.$visit_id);

		$rs = $this->nurse_model->get_plan_notes($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'plan_description'=>$plan_notes);
			$this->db->insert('visit_plan', $visit_data);

		}
		else {
			$visit_data = array('plan_description'=>$plan_notes);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_plan', $visit_data);
		}

		redirect('dental/patient_card/'.$visit_id);

	}

	/*
	*
	*	Add documents 
	*	@param int $personnel_id
	*
	*/
	public function upload_documents($patient_id, $visit_id) 
	{
		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';
		
		//upload image if it has been selected
		$response = $this->dental_model->upload_any_file($this->document_upload_path, $this->document_upload_location, $document_name, 'document_scan');
		if($response)
		{
			$document_upload_location = $this->document_upload_location.$this->session->userdata($document_name);
		}
		
		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);
		$this->form_validation->set_rules('document_item_name', 'Document Name', 'xss_clean');
		$this->form_validation->set_rules('document_type_id', 'Document Type', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->dental_model->upload_personnel_documents($patient_id, $document))
			{
				// $this->session->set_userdata('success_message', 'Document uploaded successfully');
				$this->session->unset_userdata($document_name);
				$data['message'] = 'success';
				$data['result'] ='You have added the sick leave note';
			}
			
			else
			{
				// $this->session->set_userdata('error_message', 'Could not upload document. Please try again');
				$data['message'] = 'fail';
				$data['result'] ='Sorry could not upload file';
			}
		}
		else
		{
			// $this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			$data['message'] = 'fail';
		  	$data['result'] ='Sorry could not upload file';
		}
		
		// redirect('dental/patient_card/'.$visit_id);

		echo json_encode($data);
	}
    
	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function delete_document_scan($document_upload_id, $visit_id)
	{
		if($this->dental_model->delete_document_scan($document_upload_id))
		{
			$this->session->set_userdata('success_message', 'Document has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Document could not deleted');
		}
		redirect('dental/patient_card/'.$visit_id);
	}
	function doc_schedule($personnel_id,$date)
	{
		$data = array('personnel_id'=>$personnel_id,'date'=>$date);
		$this->load->view('reception/show_schedule',$data);	
	}

	public function save_dentine($visit_id,$patient_id)
	{

		$this->form_validation->set_rules('cavity_status', 'Cavity Status', 'trim|xss_clean');
		$this->form_validation->set_rules('tooth_id', 'tooth', 'trim|xss_clean');
		$this->form_validation->set_rules('patient_id', 'Patient', 'required|trim|xss_clean');
		$this->form_validation->set_rules('service_charge_id', 'Service Charge ID', 'trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == TRUE)
		{
			$cavity_status = $this->input->post('cavity_status');
			$tooth_id = $this->input->post('tooth_id');
			$surface_id = $this->input->post('surface_id');
			$service_charge_id = $this->input->post('service_charge_id');
			$plan_status = $this->input->post('plan_status');

			
			$data['cavity_status'] = $cavity_status;
			$data['teeth_id'] = $tooth_id;
			$data['patient_id'] = $patient_id;
			$data['plan_status'] = $plan_status;
			

			$data['created'] = date('Y-m-d');
			$data['created_by'] = $this->session->userdata('personnel_id');

			// var_dump($data);die();
			$dentine_id = 0;

			if(!empty($tooth_id))
			{
				
				

				if(empty($surface_id) OR $surface_id == 'null')
				{
					// $teeth_section = NULL;
					$data['teeth_section'] = 0;
				}
				else
				{

					$input = $surface_id;

					
					$characters = array();
					for ($i = 0; $i < strlen($input); $i++)
					    $characters[] = $input[$i];
					$permutations = array();

					function string_getpermutations($prefix, $characters, &$permutations)
					{
					    if (count($characters) == 1)
					        $permutations[] = $prefix . array_pop($characters);
					    else
					    {
					        for ($i = 0; $i < count($characters); $i++)
					        {
					            $tmp = $characters;
					            unset($tmp[$i]);

					            string_getpermutations($prefix . $characters[$i], array_values($tmp), $permutations);
					        }
					    }
					}
					string_getpermutations("", $characters, $permutations);
					$string_items = count($permutations);

					// print_r($string_items);die();
					$check_items =' AND  (surface_id = "'.$input.'" ';
					if(count($permutations) > 0)
					{
						for ($i=0; $i < $string_items; $i++) { 
							# code...
							$permutation = $permutations[$i];

							if($permutation != $input)
							{
								$check_items .= ' OR surface_id = "'.$permutation.'"';

							}
						
						}
					}
					$check_items .=' )';
					// var_dump($check_items);die();
					$this->db->where('tooth_id = '.$tooth_id.' '.$check_items);
					$this->db->limit(1);
					$query_notations = $this->db->get('charting_notations');

					if($query_notations->num_rows() > 0)
					{
						foreach ($query_notations->result() as $key => $value) {
							# code...
							$surface_id = $value->surface_id;
						}
						$data['teeth_section'] = $surface_id;
					}
					else
					{
						$data['teeth_section'] = 0;
					}

					
				}




				$this->db->where('teeth_id = '.$tooth_id.' AND patient_id = '.$patient_id.'');
				$query = $this->db->get('dentine');

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$dentine_id = $value->dentine_id;
					}

					$this->db->where('dentine_id',$dentine_id);
					$this->db->update('dentine',$data);		
					$response['status'] = 'success';
					$response['message'] = 'successfully updated updated dentine info';	

				}
				else
				{
					$this->db->insert('dentine',$data);
					$dentine_id = $this->db->insert_id();
					$response['status'] = 'success';
					$response['message'] = 'successfully updated updated dentine info';					
				}

				
			}

			

			
			
			if($plan_status == 3)
			{
				$service_charge_id = 200;
				$service_charge_amount = 0;
				$visit_rs = $this->reception_model->get_visit($visit_id);

				if($visit_rs->num_rows() > 0)
				{
					foreach ($visit_rs->result() as $key => $value) {
						# code...
						$visit_type = $value->visit_type;
					}
				}
				$visit_type_id = $visit_type;

			}
			else
			{


				$service_charge_explode = explode('#', $service_charge_id);

				$service_charge_id = $service_charge_explode[0];
				$service_charge_amount = $service_charge_explode[1];
				$visit_type_id = $service_charge_explode[2];
			}




				$provider_id = $this->session->userdata("personnel_id");
				$visit_date = date('Y-m-d');

				// $amount = $this->accounts_model->get_service_charge_detail($service_charge_id);

				if(empty($tooth_id))
				{
					$tooth_id = NULL;
				}


				$visit_data = array('visit_charge_units'=>1,
									'visit_id'=>$visit_id,
									'visit_charge_amount'=>$service_charge_amount,
									'service_charge_id'=>$service_charge_id, 
									'created_by'=>$this->session->userdata("personnel_id"),
									'provider_id'=>$provider_id,
									'personnel_id'=>$provider_id,
									'charged'=>0,
									'charge_to'=>$visit_type_id,
									'dentine_id'=>$dentine_id,
									'surface_id'=>$surface_id,
									'patient_id'=>$patient_id,
									'teeth_id'=>$tooth_id,
									'plan_status'=>$plan_status
									);
				
				
				$visit_data['date'] = NULL;
				$visit_data['time'] = NULL;
				

				if($this->db->insert('visit_charge', $visit_data))
				{
					$response['status'] = 'success';
					$response['message'] = 'successfully updated updated dentine info';
				}
				else
				{
					$response['status'] = 'success';
					$response['message'] = 'successfully added dentine ino';
				}
			

		}
		else
		{
			$response['status'] = 'fail';
			$response['message'] = 'Please fill in all the required fields with (*)';
		}


		echo json_encode($response);

	}

	public function get_page_item($page_id,$patient_id,$visit_id=null,$chart_type=NULL)
	{
		// if($chart_type)

		$this->db->where('patient_id',$patient_id);
		$query = $this->db->get('patients');


		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$chart_type = $value->chart_type;
			}
		}
		$data = array('page_id'=>$page_id,'patient_id'=>$patient_id,'visit_id'=>$visit_id,'chart_type'=>$chart_type);




		// if($page_id == 1)
		// {

		// 	$response['page_item'] = $this->load->view('history_page',$data,true);	

		// }
		// else if($page_id == 2)
		// {

		// 	$response['page_item'] = $this->load->view('diagnosis',$data,true);	
		// }
		// else if($page_id == 3)
		// {

		// 	$response['page_item'] = $this->load->view('treatment',$data,true);	
		// }
		// else if($page_id == 4)
		// {

		// 	$response['page_item'] = $this->load->view('bills',$data,true);	
		// }

		// else if($page_id == 5)
		// {

		// 	$response['page_item'] = $this->load->view('medical_history',$data,true);	
		// }
		// else if($page_id == 6)
		// {

			$response['page_item'] = $this->load->view('dentine',$data,true);	
		// }
		// else if($page_id == 7)
		// {

		// 	$response['page_item'] = $this->load->view('uploads',$data,true);	
		// }
		
		echo json_encode($response);
	}
	function display_dental_formula($teeth_id,$visit_id,$patient_id,$teeth_section=NULL)
	{
		$this->session->unset_userdata('teeth_session');
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['teeth_id'] = $teeth_id;
		$v_data['teeth_section'] = $teeth_section;
		$page = $this->load->view('dental_formula',$v_data,true);


		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['teeth_id'] = $teeth_id;
		$v_data['teeth_section'] = $teeth_section;
		$page_two = $this->load->view('teeth_marker',$v_data,true);
		// $page = $this->load->view('sidebar/appointment_sidebar',$data);

		$response['dental_formula'] = $page;
		$response['teeth_marker'] = $page_two;
		echo json_encode($response);
	}




	function display_patient_prescription($visit_id=null,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$this->load->view('prescription',$v_data);
	}

	public function bill_patient($visit_id,$module =null)
	{
		
		$service_charge_id = $this->input->post('service_charge_id');
		$provider_id = $this->input->post('provider_id');
		$visit_date = $this->input->post('visit_date_date');
		$amount = $this->accounts_model->get_service_charge_detail($service_charge_id);

		$visit_data = array('visit_charge_units'=>1,'visit_id'=>$visit_id,'visit_charge_amount'=>$amount,'service_charge_id'=>$service_charge_id, 'created_by'=>$this->session->userdata("personnel_id"),'provider_id'=>$provider_id,'date'=>$visit_date,'time'=>date('H:i:s'),'personnel_id'=>$procedure_id,'charged'=>1);

		if($this->db->insert('visit_charge', $visit_data))
		{
			$this->session->set_userdata('success_message', 'You have successfully added to bill');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Sorry please try again');
		}

		
		redirect('patient-card/'.$visit_id);
        
	}

	public function print_sick_leave($visit_id)
	{
		
		$branch_id = $this->session->userdata('branch_id');
		$data['contacts'] = $this->site_model->get_contacts($branch_id);

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$data['visit_id'] = $visit_id;		
		$this->load->view('print_sick_leave', $data);
	}
	public function print_prescription($visit_id)
	{
		$branch_id = $this->session->userdata('branch_id');
		$data['contacts'] = $this->site_model->get_contacts($branch_id);

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$data['visit_id'] = $visit_id;		
		$this->load->view('print_prescription', $data);
	}
	public function view_lab_work($visit_id)
	{
		$data = array('visit_id'=>$visit_id);
		$this->load->view('view_lab_work',$data);
	}
	public function save_lab_work($visit_id)
	{
		$this->form_validation->set_rules('notes', 'Lab Work', 'required|trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == TRUE)
		{
			$lab_work['lab_work_done'] = $this->input->post('notes');
			$lab_work['visit_id'] = $visit_id;
			$lab_work['created'] = date('Y-m-d');
			$lab_work['created_by'] = $this->session->userdata('personnel_id');
			$lab_work['lab_work_deleted'] = 0;

			$this->db->insert('visit_lab_work', $lab_work);

			$response['status'] = 'success';
			$response['message'] ='You have successfully created the lab work';
		}
		else
		{
			$response['status'] = 'fail';
			$response['message'] ='Sorry, ensure that you added a lab work';
		}	

		echo json_encode($response);
	}

	function delete_lab_work($visit_lab_work_id)
	{
		$this->db->where(array("visit_lab_work_id"=>$visit_lab_work_id));
		$this->db->delete('visit_lab_work');
	}
	public function save_prescription($patient_id,$visit_id)
	{
		// prescription
		$prescription_notes=$this->input->post('prescription');

		$rs = $this->nurse_model->get_prescription_notes_visit($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'visit_prescription'=>$prescription_notes,'patient_id'=>$patient_id);
			$this->db->insert('visit_prescription', $visit_data);

		}
		else {
			$visit_data = array('visit_prescription'=>$prescription_notes,'patient_id'=>$patient_id);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_prescription', $visit_data);
		}

		$response['status'] = 'success';
		$response['message'] ='You have added the prescription';
		echo json_encode($response);
	}
	public function get_patient_balance($patient_id){

	
		// $this->db->where('v_patient_balances.patient_id = '.$patient_id);
		// $query = $this->db->get('v_patient_balances');
		// $balance = 0;
		// if($query->num_rows() > 0)
		// {
		// 	foreach ($query->result() as $key => $value) {
		// 		# code...
		// 		$balance = $value->balance;
		// 	}
		// }
		$cash_balance = $this->accounts_model->get_cash_balance($patient_id);
		$insurance_balance = $this->accounts_model->get_insurance_balance($patient_id);
		$balance = $cash_balance+$insurance_balance;
		echo "<h3>Balance: KSH. ".number_format($balance,2)."</h3>";
	}

	public function get_patient_waivers($patient_id){

		$data = array('patient_id'=>$patient_id);
		$this->load->view('view_patient_waivers',$data);
		
	}
	public function add_patient_waiver($patient_id,$visit_id)
	{
		$this->form_validation->set_rules('waiver_amount', 'Waiver Amount', 'required|trim|xss_clean');
		$this->form_validation->set_rules('reason', 'Reason', 'required|trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == TRUE)
		{
			$reason = $this->input->post('reason');
			$waiver_amount = $this->input->post('waiver_amount');
			$data['reason'] = $reason;
			$data['amount_paid'] = $waiver_amount;
			$data['visit_id'] = $visit_id;
			$data['payment_type'] = 2;
			$data['payment_method_id'] = 7;
			$data['payment_created'] = date('Y-m-d');
			$data['payment_created_by'] = $this->session->userdata('personnel_id');
			
			$this->db->insert('payments',$data);
			$response['status'] = 'success';
			$response['message'] = 'successfully added dentine ino';

		}
		else
		{
			$response['status'] = 'fail';
			$response['message'] = 'Please fill in all the required fields with (*)';
		}


		echo json_encode($response);
	}
	public function remove_patient_waiver($payment_id)
	{

		$data['cancel'] = 1;
		$data['cancelled_date'] = date('Y-m-d');
		$data['cancelled_by'] = $this->session->userdata('personnel_id');
		$this->db->where('payment_id',$payment_id);
		$this->db->update('payments',$data);
		$response['status'] = 'success';
		$response['message'] = 'successfully added dentine ino';
		echo json_encode($response);
	}

	public function get_visit_procedures($visit_id,$patient_id)
	{
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
			# code...
			  $visit_t = $rs1->visit_type;
		  }
		}

		$symptoms_search = $this->input->post('query');
		$query = null;
		$lab_test_where = 'service_charge.service_charge_delete = 0 AND service_charge.service_charge_status = 1 AND visit_type_id = 1';
		if(!empty($symptoms_search))
		{
			

			$surnames = explode(" ",$symptoms_search);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{

					$surname .= ' (service_charge.service_charge_name LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (service_charge.service_charge_name LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';

			// $lab_test_where .= ' AND patient_surname LIKE \'%'.$symptoms_search.'%\'';
			$lab_test_where .=$surname;

			

		}
		
		if(!empty($symptoms_search))
		{


			$lab_test_table = 'service_charge';
			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

			if($query->num_rows() == 0)
			{
				$lab_test_where = 'service_charge.service_charge_delete = 0 AND service_charge.service_charge_status = 1 AND visit_type_id = 1 '.$surname;
				$lab_test_table = 'service_charge';
				$this->db->where($lab_test_where);
				$this->db->limit(10);
				$query = $this->db->get($lab_test_table);
			}

		}
		else
		{
			$lab_test_table = 'service_charge';
			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

			if($query->num_rows() == 0)
			{
				$lab_test_where = 'service_charge.service_charge_delete = 0 AND service_charge.service_charge_status = 1 AND visit_type_id = 1';
				$lab_test_table = 'service_charge';
				$this->db->where($lab_test_where);
				$this->db->limit(10);
				$query = $this->db->get($lab_test_table);
			}
		}
		


		$data['query'] = $query;
		$data['patient_id'] = $patient_id;
		$data['visit_id'] = $visit_id;
		$page = $this->load->view('billing/billing_list',$data);

		echo $page;
	}


	public function add_visit_bill($visit_id,$patient_id,$service_charge_id,$service_charge_amount,$visit_type_id)
	{
		// $service_charge_id = $this->input->post('service_charge_id');
		$provider_id = $this->session->userdata("personnel_id");
		$visit_date = date('Y-m-d');
		$amount = $this->accounts_model->get_service_charge_detail($service_charge_id);

		$visit_data = array('visit_charge_units'=>1,
							'visit_id'=>$visit_id,
							'visit_charge_amount'=>$service_charge_amount,
							'service_charge_id'=>$service_charge_id, 
							'created_by'=>$this->session->userdata("personnel_id"),
							'provider_id'=>$provider_id,
							'date'=>$visit_date,
							'time'=>date('H:i:s'),
							'personnel_id'=>$provider_id,
							'charged'=>1,
							'charge_to'=>$visit_type_id
							);

		if($this->db->insert('visit_charge', $visit_data))
		{
			// $this->session->set_userdata('success_message', 'You have successfully added to bill');
			$response['status'] = 'success';
			$response['message'] = 'You have successfully added to bill';
		}
		
		else
		{
			// $this->session->set_userdata('error_message', 'Sorry please try again');
			$response['status'] = 'fail';
			$response['message'] = 'You have successfully added to bill';
		}
		echo json_encode($response);
	}

	public function visit_charge_information($visit_charge_id,$visit_id)
	{
		$patient_id = $this->input->post('patient_id');
		$lab_test_table = 'visit_charge';
		$this->db->where('visit_charge_id',$visit_charge_id);
		$query = $this->db->get($lab_test_table);
		$data['query'] = $query;
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_charge_id'] = $visit_charge_id;
		$page = $this->load->view('billing/visit_charge_information',$data);

		echo $page;
	}


	public function patient_prescription($visit_id,$patient_id)
	{

		
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$page = $this->load->view('sidebar/prescription_view',$data);

		echo $page;
	}

	function display_patient_sick_leave($visit_id=null,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$this->load->view('sick_leave',$v_data);
	}

	public function save_sick_leave_note($patient_id,$visit_id)
	{
		// prescription
		$sick_leave_notes=$this->input->post('sick_leave');

		$rs = $this->nurse_model->get_sick_leave_notes_visit($visit_id);
		$num_doc_notes = count($rs);
		
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'visit_sick_leave'=>$sick_leave_notes,'patient_id'=>$patient_id);
			$this->db->insert('visit_sick_leave', $visit_data);

		}
		else {
			$visit_data = array('visit_sick_leave'=>$sick_leave_notes,'patient_id'=>$patient_id);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('visit_sick_leave', $visit_data);
		}

		$response['status'] = 'success';
		$response['message'] ='You have added the sick leave note';
		echo json_encode($response);
	}

	public function patient_sick_note($visit_id,$patient_id)
	{

		
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$page = $this->load->view('sidebar/sickleave_view',$data);

		echo $page;
	}

	public function display_patient_uploads($visit_id,$patient_id)
	{
		$v_data['document_types'] = $this->dental_model->all_document_types();
		$v_data['patient_other_documents'] = $this->dental_model->get_document_uploads($patient_id);
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$this->load->view('uploads',$v_data);
	}

	public function patient_uploads($visit_id,$patient_id)
	{

		$data['document_types'] = $this->dental_model->all_document_types();
		$data['patient_other_documents'] = $this->dental_model->get_document_uploads($patient_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$page = $this->load->view('sidebar/uploads_view',$data);

		echo $page;
	}

	public function set_tooth_surfaces($teeth_number,$visit_id,$patient_id,$teeth_section)
	{
		$teeth_session = $this->session->userdata('teeth_session');

		if($teeth_section == 1)
		{
			$session = 'B';
		}
		else if($teeth_section == 6)
		{
			$session = 'V';
		}
		else if($teeth_section == 2)
		{
			$session = 'M';
		}
		else if($teeth_section == 5)
		{
			$session = 'O';
		}
		else if($teeth_section == 4)
		{
			$session = 'D';
		}
		else if($teeth_section == 3)
		{
			$session = 'P';
		}
											
		if(!empty($teeth_session))
		{
			// $teeth_session = str_replace($session, $session, $teeth_session);

			if(stripos($teeth_session, $session) !== false){
			   

			} else{

			   // $teeth_session .= $session;
				 $teeth_session .= $session;

			}


			$this->session->set_userdata('teeth_session',$teeth_session);
		}
		else
		{
			// $teeth_session = str_replace($session, $session, $teeth_session);
			$teeth_session .= $session;
			$this->session->set_userdata('teeth_session',$teeth_session);
		}

		echo json_encode($teeth_session);
	}

	public function set_chart_type($patient_id,$chart_type)
	{
		$array['chart_type'] = $chart_type;
		$this->db->where('patient_id',$patient_id);
		$this->db->update('patients',$array);

		$response['status'] = 'success';

		echo json_encode($response);
	}

	public function bill_procedure($visit_charge_id)
	{
		
		$visit_data['date'] = date('Y-m-d');
		$visit_data['time'] = date('H:i:s');
		$visit_data['charged'] = 1;
		
		$this->db->where('visit_charge_id',$visit_charge_id);
		$this->db->update('visit_charge',$visit_data);

		$response['status'] = 'success';

		echo json_encode($response);
				
	}


	public function remove_bill_procedure($visit_charge_id)
	{
		
		$visit_data['date'] = NULL;
		$visit_data['time'] = NULL;
		$visit_data['charged'] = 0;

		$this->db->where('visit_charge_id',$visit_charge_id);
		$this->db->update('visit_charge',$visit_data);

		$response['status'] = 'success';

		echo json_encode($response);
				
	}

	public function view_days_billing($patient_id,$visit_id)
	{

		$visit_data['date'] = date('Y-m-d');
		$visit_data['charged'] = 1;
		$visit_data['patient_id'] = $patient_id;

		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS total_amount');
		$this->db->where($visit_data);
		$query = $this->db->get('visit_charge');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		echo ' <h3 class="center-align"> Day\'s Bill Kes. '.number_format($total_amount,2).'</h3>';

		// $response['status'] = 'success';

		// echo json_encode($response);

	}

	public function update_patient_ids()
	{
		$this->db->select('visit.patient_id,visit_charge.visit_charge_id');
		$this->db->where('visit_charge.patient_id IS NULL AND visit.visit_id = visit_charge.visit_id');

		$query = $this->db->get('visit_charge,visit');
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_charge_id = $value->visit_charge_id;
				$patient_id = $value->patient_id;


				$update_query['patient_id'] = $patient_id;
				// var_dump($visit_charge_id);die();
				$this->db->where('visit_charge_id',$visit_charge_id);
				$this->db->update('visit_charge',$update_query);
			}
		}
	}

	public function add_new_service()
	{
		$this->form_validation->set_rules('service_charge_name', 'Name', 'required|trim|xss_clean');
		// $this->form_validation->set_rules('reason', 'Reason', 'required|trim|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run() == TRUE)
		{
			$service_charge_name = $this->input->post('service_charge_name');
			$visit_data = array(
								'service_id'=>2,
								'service_charge_name'=>$service_charge_name,
								'service_charge_amount'=>0,
								'visit_type_id'=>1,
								'service_charge_status'=>1,
								'created'=>date('Y-m-d')
							);
			$this->db->insert('service_charge', $visit_data);



			$response['status'] = 'success';
			$response['message'] = 'successfully added dentine ino';

		}
		else
		{
			$response['status'] = 'fail';
			$response['message'] = 'Please fill in the name of the prcedure you want to add';
		}


		echo json_encode($response);
	}
	
	
	public function attend_to_patient($visit_id)
	{
		$array['personnel_id'] = $this->session->userdata('personnel_id');
		$this->db->where('visit_id',$visit_id);

		if($this->db->update('visit',$array))
		{
			

			redirect('dental/'.$visit_id);
		}
		else
		{
			FALSE;
		}
	}

	public function display_patient_dental_notes($visit_id=null,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$this->load->view('dental_notes',$v_data);
	}

	public function patient_dental_notes($visit_id,$patient_id)
	{

		
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$page = $this->load->view('sidebar/dental_notes_view',$data);

		echo $page;
	}


	public function save_dental_notes($patient_id,$visit_id)
	{
		// prescription
		$prescription_notes=$this->input->post('prescription_notes');
		
		$rs = $this->dental_model->get_dental_notes_visit($visit_id);
		$num_doc_notes = count($rs);
		// var_dump($num_doc_notes);die();
		if($num_doc_notes == 0){	
			$visit_data = array('visit_id'=>$visit_id,'notes_name'=>$prescription_notes,'notes_type_id'=>19,'patient_id'=>$patient_id);
			$this->db->insert('notes', $visit_data);

		}
		else {
			$visit_data = array('notes_name'=>$prescription_notes,'patient_id'=>$patient_id);
			$this->db->where('visit_id = '.$visit_id);
			$this->db->update('notes', $visit_data);
		}

		$response['status'] = 'success';
		$response['message'] ='You have added the prescription';
		echo json_encode($response);
	}
}
?>