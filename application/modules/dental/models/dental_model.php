<?php
class Dental_model extends CI_Model 
{
	function submitvisitbilling($procedure_id,$visit_id,$suck){
		$visit_data = array('procedure_id'=>$procedure_id,'visit_id'=>$visit_id,'units'=>$suck);
		$this->db->insert('visit_procedure', $visit_data);
	}

	 function get_payment_info($visit_id)
	{
		$table = "visit";
		$where = "visit_id = '$visit_id'";
		$items = "payment_info,sick_leave_note,sick_leave_start_date,sick_leave_days,visit_date";
		$order = "visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	function get_rejection_info($visit_id)
	{
		$table = "visit";
		$where = "visit_id = '$visit_id'";
		$items = "*";
		$order = "visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	function get_visit_rejected_updates_sum($visit_id,$visit_type_id)
	{
		$table = "visit_bill,visit";
		$where = "visit_parent = '$visit_id' AND  visit.visit_id = visit_bill.visit_id AND visit.visit_delete = 0  ";
		$items = "SUM(visit_bill_amount) AS total_rejected";
		$order = "visit.visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}


	function get_current_visit_rejected_updates_sum($visit_id,$visit_type_id)
	{
		$table = "visit_bill,visit";
		$where = "visit_bill.visit_id = '$visit_id' AND  visit.visit_id = visit_bill.visit_id AND visit.visit_delete = 0 ";
		$items = "SUM(visit_bill_amount) AS total_rejected";
		$order = "visit.visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}


	function get_visit_rejected_updates($visit_id)
	{
		$table = "visit,visit_bill,visit_type";
		$where = "visit_parent = '$visit_id' AND visit.visit_delete = 0 AND visit.visit_id = visit_bill.visit_parent AND visit_type.visit_type_id = visit_bill.visit_type_id";
		$items = "*";
		$order = "visit.visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	
	public function all_document_types()
	{
		$this->db->order_by('document_type_name');
		$query = $this->db->get('document_type');
		
		return $query;
	}
	
	public function upload_any_file($path, $location, $name, $upload, $edit = NULL)
	{
		if(!empty($_FILES[$upload]['tmp_name']))
		{
			$image = $this->session->userdata($name);
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				
				//delete any other uploaded image
				if($this->file_model->delete_file($path."\\".$image, $location))
				{
					//delete any other uploaded thumbnail
					$this->file_model->delete_file($path."\\thumbnail_".$image, $location);
				}
				
				else
				{
					$this->file_model->delete_file($path."/".$image, $location);
					$this->file_model->delete_file($path."/thumbnail_".$image, $location);
				}
			}
			//Upload image
			$response = $this->file_model->upload_any_file($path, $upload);
			if($response['check'])
			{
				$file_name = $response['file_name'];
					
				//Set sessions for the image details
				$this->session->set_userdata($name, $file_name);
			
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('upload_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('upload_error_message', '');
			return FALSE;
		}
	}

	function upload_personnel_documents($patient_id, $document)
	{
		$data = array(
			'document_type_id'=> $this->input->post('document_type_id'),
			'document_name'=> $this->input->post('document_item_name'),
			'document_upload_name'=> $document,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'patient_id'=>$patient_id
		);
		
		if($this->db->insert('patient_document_uploads', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	function get_document_uploads($patient_id)
	{
		$this->db->from('patient_document_uploads, document_type');
		$this->db->select('*');
		$this->db->where('patient_document_uploads.document_type_id = document_type.document_type_id AND patient_id = '.$patient_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function delete_document_scan($document_upload_id)
	{
		//delete parent
		if($this->db->delete('patient_document_uploads', array('document_upload_id' => $document_upload_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_dentine_item($patient_id,$teeth_id,$teeth_section)
	{
		# code...
		$this->db->where('patient_id = '.$patient_id.' AND teeth_id = '.$teeth_id.' AND teeth_section = "'.$teeth_section.'"');
		$query = $this->db->get('dentine');		
		return $query;
	}
	public function get_dentine_value($patient_id,$teeth_id)
	{
		# code...


		$this->db->where('dentine.patient_id = '.$patient_id.' AND dentine.plan_status <> 6 
			AND dentine.teeth_id = '.$teeth_id);
		$query_first = $this->db->get('dentine');
		$cavity_status = NULL;
		if($query_first->num_rows() == 1)
		{

			foreach ($query_first->result() as $key => $value) {
				# code...
				$cavity_status = $value->cavity_status;
			}
		}

		if($cavity_status == 21)
		{
			$one = '-';
		}
		else
		{




			$this->db->where('dentine.patient_id = '.$patient_id.' AND dentine.teeth_id = charting_notations.tooth_id 
				AND charting_notations.dental_procedure_id = dentine.cavity_status 
				AND charting_notations.surface_id = dentine.teeth_section AND dentine.plan_status <> 6 
				AND dentine.teeth_id = '.$teeth_id);
			$query = $this->db->get('dentine,charting_notations');		
			// var_dump($query);die();
			
			$cavity_status = 0;
			// $query = $this->dental_model->get_dentine_item($patient_id,$teeth_id);
			$teeths = array();
			$section_b = '';
			$section_m = '';
			$section_p = '';
			$section_d = '';
			$section_o = '';

			$section_b_color = '';
			$section_m_color = '';
			$section_p_color = '';
			$section_d_color = '';
			$section_o_color = '';

			if($query->num_rows() > 0)
			{
				
				foreach ($query->result() as $key => $value) {
					# code...
					$cavity_status = $value->cavity_status;
					$teeth_section = $value->teeth_section;
					$image_connotation_blue = $value->image_connotation_blue;
					$image_connotation_red = $value->image_connotation_red;
					$image_connotation_green = $value->image_connotation_green;
					$plan_status = $value->plan_status;

					if($plan_status == 1)
					{
						$one = $image_connotation_red;
					}
					else if($plan_status == 2)
					{
						$one = $image_connotation_blue;
					}
					else if($plan_status == 3)
					{
						$one = $image_connotation_green;
					}
					

					// if($cavity_status == 1)
					// {
					// 	$one = 'O';
					// }
					// else if($cavity_status == 2)
					// {
					// 	$one = 'P';
					// }
					// else if($cavity_status == 3)
					// {
					// 	$one = '<span>&#x25cf;</span>';
					// }
					// else if($cavity_status == 4)
					// {
					// 	$one = '/';
					// }
					// else if($cavity_status == 5)
					// {
					// 	$one = '--';
					// }
					// else if($cavity_status == 6)
					// {
					// 	$one = 'C';
					// }
					// else if($cavity_status == 7)
					// {
					// 	$one = 'X';
					// }else
					// {
					// 	$one = '';
					// }

					// if($teeth_section == 1)
					// {
					// 	$section_b = $one;
					// 	$section_b_color = 'warning';
					// }
					// else if($teeth_section == 2)
					// {
					// 	$section_m = $one;
					// 	$section_m_color = 'warning';
					// }
					// else if($teeth_section == 3)
					// {
					// 	$section_p = $one;
					// 	$section_p_color = 'warning';
					// }
					// else if($teeth_section == 4)
					// {
					// 	$section_d = $one;
					// 	$section_d_color = 'warning';
					// }
					// else if($teeth_section == 5)
					// {
					// 	$section_o = $one;
					// 	$section_o_color = 'warning';
					// }

					// array_push($teeths, $child);


				}
			}
			else
			{
				$one = NULL;
			}
		}


		// if($teeths->count() > 0)
		// {
		// 	for
		// }

		// var_dump($section_d);die();
		$letter_b = "b";


		// $teeth_response = '<div class="continer-tab grand_section ">
		// 							<table class="table table-bordered " style="margin-bottom: 0px !important;">
		// 								<tr>
		// 									<td colspan="3" class="'.$section_b_color.'"  >'.$section_b.'</td>									
		// 								</tr>
		// 								<tr>
		// 									<td class="'.$section_d_color.'">'.$section_d.'</td>
		// 									<td class="'.$section_o_color.'" >'.$section_o.'</td>
		// 									<td class="'.$section_m_color.'" >'.$section_m.'</td>
		// 								</tr>
		// 								<tr>
		// 									<td colspan="3" class="'.$section_p_color.'" >'.$section_p.'</td>
		// 								</tr>
										
		// 							</table>
									
		// 						</div>';

		// if($teeth_id == 51)
		// {
		// 	var_dump($one);die();
		// }
		// if($cavity_status == 21)
		// {
		// 	$one = '-';
		// }
		if($one == NULL)
		{
			$one= $teeth_id.".jpg";
		}
			
		// if($teeth_id == 44)
		// {
		// 	var_dump($one);die();
		// }

		return $one;
	}

	public function get_visit_lab_work($v_id){
		$table = "visit_lab_work";
		$where = "visit_id = $v_id";
		$items = "*";
		$order = "visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	public function get_patient_waivers($patient_id){
		
		$this->db->where('visit.visit_id = payments.visit_id AND payments.cancel = 0 AND payments.payment_type = 2 AND visit.patient_id = '.$patient_id);
		$query = $this->db->get('visit,payments');

		return $query;

	}
	public function get_all_procedures()
	{
		$this->db->where('dental_procedure_id > 0 AND dental_procedure_delete = 0 ');
		$query = $this->db->get('dental_procedure');

		return $query;
	}


	
	function permute($str, $l, $r) 
	{ 
		$string = '';
		if ($l == $r) 
		{
			$string .= " OR surface_id = '".$str."' "; 
			// return $string;
			// break;
		}
			
		else
		{ 
			for ($i = $l; $i <= $r; $i++) 
			{ 
				$str = $this->swap($str, $l, $i); 
				$this->permute($str, $l + 1, $r); 
				$str = $this->swap($str, $l, $i); 
				// $string .= " AND ".$str;
				// $string .= " OR surface_id = '".$str."' "; 
			} 
		} 
		// var_dump($string);die();
		
	} 

	/** 
	* Swap Characters at position 
	* @param a string value 
	* @param i position 1 
	* @param j position 2 
	* @return swapped string 
	*/
	function swap($a, $i, $j) 
	{ 
		$temp; 
		$charArray = str_split($a); 
		$temp = $charArray[$i] ; 
		$charArray[$i] = $charArray[$j]; 
		$charArray[$j] = $temp; 
		return implode($charArray); 
	} 

	// Driver Code 
	
	// This code is contributed by mits. 

	function get_visit_procedure_notes($v_id =  NULL,$visit_charge_id = NULL,$patient_id=NULL,$visit_date=NULL)
	{
		if($visit_charge_id > 0)
		{
			$add = ' AND visit_charge_procedure.visit_charge_id = '.$visit_charge_id;
		}
		else
		{
			$add = '';
		}


		// if(!empty($patient_id))
		// {
		// 	$add .= ' AND visit.patient_id ='.$patient_id;
		// }
		if(!empty($v_id))
		{
			$add .= ' AND visit_charge_procedure.visit_id = '.$v_id;
		}
		if(!empty($visit_date))
		{
			$add .= ' AND visit.visit_id < '.$visit_date;
		}
		$table = "visit_charge_procedure,visit";
		$where = "visit_charge_procedure_status = 1 AND visit_charge_procedure.visit_id = visit.visit_id ".$add;
		$items = "visit_charge_procedure.*,personnel.personnel_fname,personnel_onames,visit.visit_date";
		$order = "visit_charge.visit_id";

		$this->db->where($where);
		// $this->db->table($table);
		$this->db->select($items);
		$this->db->join('personnel','visit.personnel_id = personnel.personnel_id','left');
		$result = $this->db->get($table);
		return $result;
	}
	// function get_patients_visit_notes()

	function check_if_notes_written($visit_id,$patient_id)
	{
		// check if there is charge created 

		$this->db->where('patient_id = '.$patient_id.' AND DATE(created) = "'.date('Y-m-d').'" ');

		$query = $this->db->get('visit_charge_procedure');

		if($query->num_rows() > 0)
		{
			return TRUE;

		}
		else
		{
			return FALSE;
		}
	}

	function get_items_from_table($table,$where,$select)
	{
		$this->db->where($where);
		$this->db->select($select);
		$query = $this->db->get($table);
		return $query;
	}

	public function get_patient_history_forms($form_id,$per_page,$page)
	{
		if($per_page == 0)
		{
			$per_page = '';
		}
		else
		{
			$per_page = $per_page;
		}


		$this->db->select('*');
		$this->db->where('form_id',$form_id);

		if($per_page == 0)
		{
			$this->db->order_by('patient_history_id', 'ASC');
			$query = $this->db->get('patient_history_form',$page);
		}
		else if($per_page == 7 OR $per_page == 9) 
		{ 
			// if($form_id == 2){
			// 	$this->db->order_by('patient_history_id', 'ASC');
			// }
			// else
			// {
				$this->db->order_by('patient_history_id', 'ASC');
			// }
			
			$query = $this->db->get('patient_history_form',$per_page,$page);
		}
		else 
		{
			$this->db->order_by('patient_history_id', 'ASC');
			$query = $this->db->get('patient_history_form',$page,$per_page);
		}
		
		
		
		return $query;
	}

	function get_patient_history_results($patient_history_id,$patient_id,$visit_id)
	{
		$this->db->where('patient_history_id = '.$patient_history_id.' AND patient_id = '.$patient_id.'');
		$query = $this->db->get('patient_history_result');
		
		return $query;
	}

	public function get_dental_notes($notes_type_id, $visit_id)
	{
		$this->db->select('notes.*, notes_type.notes_type_name, personnel.personnel_fname');
		$this->db->where('notes.visit_id = '.$visit_id.' AND notes.notes_type_id = '.$notes_type_id.' AND notes.notes_status = 1 AND notes.notes_type_id = notes_type.notes_type_id');
		$this->db->join('personnel', 'personnel.personnel_id = notes.created_by', 'left');
		$this->db->order_by('notes_date', 'ASC');
		$this->db->order_by('notes_time', 'ASC');
		$query = $this->db->get('notes, notes_type');
		
		return $query;
	}
	public function add_notes($visit_id, $notes_type_id, $signature_name, $personnel_id,$patient_id)
	{
		$notes=$this->input->post('notes');
		$date=$this->input->post('date');
		$time=$this->input->post('time');

		//  enter into the nurse notes trail 
		$trail_data = array(
        		"notes_type_id" => $notes_type_id,
        		"visit_id" => $visit_id,
        		"patient_id" => $patient_id,
        		"notes_name" => $notes,
        		"notes_time" => $time,
        		"notes_date" => $date,
        		"notes_signature" => $signature_name,
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$personnel_id,
				'modified_by'=>$personnel_id
	    	);

		if($this->db->insert('notes', $trail_data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}

	function get_dental_notes_list($patient_id,$visit_id=NULL){

		if(!empty($visit_id))
		{
			$add = ' AND notes.visit_id = '.$visit_id;
		}
		else
		{
			$add = '';
		}
		$table = "notes,visit";
		$where = 'visit.visit_id = notes.visit_id AND notes.notes_type_id = 19 AND notes.patient_id = '.$patient_id.$add;
		$items = "*";
		$order = "visit.visit_date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
		
	}

	function get_dental_notes_visit($visit_id)
	{
		$table = "notes,visit";
		$where = 'visit.visit_id = notes.visit_id AND notes.notes_type_id = 19 AND notes.visit_id = '.$visit_id;
		$items = "*";
		$order = "notes_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
		
	}


}
?>