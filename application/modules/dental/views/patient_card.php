<?php
// get visit details


$visit_rs = $this->reception_model->get_visit_details($visit_id);

foreach ($visit_rs as $key) {
	# code...

	$time_end=$key->time_end;
	$time_start=$key->time_start;
	$visit_date=$key->visit_date;
	$doctor_id=$key->personnel_id;
	$room_id=$key->room_id;
	$patient_id=$key->patient_id;
	// $scheme_name=$key->scheme_name;
	$insurance_description=$key->insurance_description;
	$patient_insurance_number=$key->patient_insurance_number;
	$insurance_limit=$key->insurance_limit;
	$principal_member=$key->principal_member;
}
// var_dump($patient_id);die();

$patient_details = $this->reception_model->get_patient_data($patient_id);

foreach ($patient_details->result() as $key => $value) {
	# code...

	$patient_surname = $value->patient_surname;
	$total_balance = 0;//$value->total_balance;
	$patient_date_of_birth = $value->patient_date_of_birth;
	$gender = $value->gender;
	$chart_type = $value->chart_type;
	$total_balance = 0;//$value->total_balance;

	if(!empty($patient_date_of_birth) AND $patient_date_of_birth != "0000-00-00")
	{
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
	}
	else
	{
		$age = '';
	}


	if($gender == 2)
	{
		$gender_title = 'Female';
	}
	else
	{
		$gender_title = 'Male';
	}
	

}

	?>

	<?php
			$personnel_id = $this->session->userdata('personnel_id');
			
			$is_dentist = $this->reception_model->check_personnel_department_id($personnel_id,11);
			


			// var_dump($is_physician);die();

			if(($is_dentist) AND $doctor_id > 0 OR $personnel_id == 0)
			{
				
				?>
				<div class="well well-sm info">
					<h5 style="margin:0;">
						<div class="row">
							<div class="col-md-2">
								<div class="row">
									<div class="col-lg-6">
										<strong>Name:</strong>
									</div>
									<div class="col-lg-6">
										<?php echo $patient_surname;?>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-md-2">
								<div class="row">
									<div class="col-lg-6">
										<strong>Gender:</strong>
									</div>
									<div class="col-lg-6">
										<?php echo $gender;?>
									</div>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="row">
									<div class="col-lg-6">
										<strong>Age:</strong>
									</div>
									<div class="col-lg-6">
										<?php echo $age;?>
									</div>
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="row">
									<div class="col-lg-6">
										<strong> balance:</strong>
									</div>
									<div class="col-lg-6">
										Kes <?php echo number_format($total_balance, 2);?>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
							</div>
						</div>
					</h5>
				</div>
				<?php
			}
			else
			{
				?>
				<div class="col-md-12">
					<div class="center-align">
						<?php echo form_open("dental/attend_to_patient/".$visit_id, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php
				
			}
			
			?>


	


<?php
if(($is_dentist) AND $doctor_id > 0 OR $personnel_id == 0)
{
?>

<div class="row">
	

	<div class="center-align">
		<?php
			$error = $this->session->userdata('error_message');
			$validation_error = validation_errors();
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($validation_error))
			{
				echo '<div class="alert alert-danger">'.$validation_error.'</div>';
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}


			if($chart_type == 1)
			{
				$child_items = '';
				$adult_items = 'checked';
			}
			else
			{
				$child_items = 'checked';
				$adult_items = '';
			}
		?>
	</div>
	
	<?php echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
	<?php //echo $this->load->view("patient_history", '', TRUE);?>
	<div class="panel-body">
		<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">
		<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
		<div class="col-print-6" >
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-lg-4 control-label">Type of chart? </label>
			        <div class="col-lg-4">
			            <div class="radio">
			                <label>
			                    <input id="optionsRadios1" type="radio" name="chart_type" <?php echo $child_items;?> id="chart_type_one" onclick="get_chart_type(0)" value="0">
			                    Child
			                </label>
			            </div>
			        </div>
			        
			        <div class="col-lg-4">
			            <div class="radio">
			                <label>
			                    <input id="optionsRadios1" type="radio" name="chart_type" <?php echo $adult_items;?> id="chart_type_two" onclick="get_chart_type(1)"  value="1">
			                    Adult
			                </label>
			            </div>
			        </div>
				</div>

			</div>
			<div id="page_item"></div>
			<div class="row">
				<div class="col-md-12" style="margin-top:10px;">
					<table class="table table-condensed table-stripped table-bordered">
						<tr>
							<td>Patient Name</td>
							<td><?php echo $patient['patient_surname']?></td>
						</tr>
						<tr>
							<td>Gender</td>
							<td><?php echo $patient['gender']?></td>
						</tr>
						<tr>
							<td>Age</td>
							<td><?php echo $age?></td>
						</tr>

						<tr>
							<td>Account</td>
							<td><?php echo $patient['visit_type_name']?></td>
						</tr>
						<tr>
							<td>Scheme</td>
							<td><?php echo $insurance_description?></td>
						</tr>
						<tr>
							<td>Limit</td>
							<td><?php echo $insurance_limit?></td>
						</tr>
						<tr>
							<td>Principal Member</td>
							<td><?php echo $principal_member?></td>
						</tr>

						<tr>
							<td>Account Balance</td>
							<td><?php echo number_format($total_balance,2)?></td>
						</tr>
					</table>
					
				</div>
				
			</div>
			
			
		</div>
		<div class="col-print-6" >
			<div class="col-md-12" style="border-bottom: 1px solid grey; margin-bottom: 2px;">
				<div class="col-md-12">
					<?php 

					// echo '<div class="alert alert-info">
					// 	1.  Click on  <a  class="btn btn-xs btn-success" ><i class="fa fa-arrow-right"></i> Bill </a> to bill the patient on that particular day and <a  class="btn btn-danger btn-xs  " ><i class="fa fa-arrow-left"></i> unbill </a> to remove from that day\'s bill.<br>
					// 	2. Ensure you have clicked on the button <a  class="btn btn-danger btn-xs  " > Send to accounts </a> for the accounts office to get the bill. Below the page
					// 	</div>';

						?>
					
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<h4 class="center-align"><i id="toothnumber"></i></h4>
					</div>
					<div class="col-md-6">
						<a class="btn btn-sm btn-warning pull-right" href="<?php echo site_url().'queues/outpatient-queue'?>" > <i class="fa fa-arrow-left"></i> Back to queue </a>
					</div>
					
				</div>
				
						
			</div>
			<div class="col-md-12" style="height: 300px; overflow-y:  scroll;">
				
				<!-- <hr> -->
				<div class="col-md-12" >
					<div class="col-print-3">  
						<div id="dentine-list-div"></div>	
						
					</div>
					<div class="col-print-5">   

					    <div class="form-group">                    
					        <div class="col-md-10">
					        	<div id="procedure-search-div" style="display: block">
					        		<input type="text" class="form-control" name="q" id="q" placeholder="procedures" onkeyup="procedure_lists(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" >
					        	</div>
					        	<div id="service_charge-div" style="display: none">
					        		<div class="col-md-12" style="margin-bottom: 10px;">
					        			<input type="text" class="form-control" name="service_charge_name" id="service_charge_name" placeholder="Procedure Name" >
					        		</div>
					        		<div class="col-md-12" >
					        			<a onclick="add_service_charge(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> Add Procedure</a>
					        		</div>
					        	</div>
					            
					        </div>
					        <div class="col-md-2">
					        	<div id="new-button-div" style="display: block">
						        	<a onclick="add_new_procedure(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></a>
						        </div>
						        <div id="add-button-div" style="display: none">
						        	<a onclick="back_procedure_list(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i></a>
						        </div>
					        </div>
					    </div>
					    <div class="col-print-12" id="procedure-div" style="display: block;">
					    	<div id="procedure-lists"></div>
					    </div>
					</div>
					<div class="col-print-4">
						<div id="teeth-marker"></div>
						<div class="col-md-12" >
							<div class="center-align">
								<a class="btn btn-sm btn-success  " onclick="pass_tooth()"> Update Dental</a>	
							</div>
											 	
						</div>
					</div>
				</div>


			</div>
			<div class="col-md-12" style="height: 500px; overflow-y:  scroll;" >

				<ul class="nav nav-tabs nav-justified">
					<li class="active" ><a href="#notes-billing" data-toggle="tab" onclick="visit_display_billing(<?php echo $visit_id;?>)">Billing <i class="fa fa-recycle"></i></a></li>
					<li><a href="#notes" data-toggle="tab" onclick="notes_view()">Notes</a></li>
					<li><a href="#prescription-old" data-toggle="tab" onclick="prescription_view()">Prescription</a></li>
					<li><a href="#sicksheet" data-toggle="tab" onclick="sicksheet_view()">Sick Leave</a></li>
					<li><a href="#uploads" data-toggle="tab" onclick="uploads_view()">Uploads</a></li>				
					<li><a href="#quotation-form" data-toggle="tab">Patient History</a></li>
					<!--<li><a href="#patient_details" data-toggle="tab">Patient Details</a></li>
					<li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li> -->
				</ul>
				<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
					
					<div class="tab-pane active" id="notes-billing" >
						<div id="visit-billing-items"></div>
					</div>
					<div class="tab-pane" id="notes">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<a class="btn btn-sm btn-success pull-right" onclick="add_dental_notes(<?php echo $visit_id;?>,<?php echo $patient_id?>)"> add a note </a>
						</div>
						<div id="dental-notes"></div>
					</div>
					<div class="tab-pane" id="prescription-old">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<a class="btn btn-sm btn-success pull-right" onclick="add_patient_prescription(<?php echo $visit_id;?>,<?php echo $patient_id?>)"> add a prescription </a>
						</div>
						<div id="visit-prescription"></div>
					</div>
					<div class="tab-pane" id="sicksheet">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<a class="btn btn-sm btn-success pull-right" onclick="add_patient_sick_note(<?php echo $visit_id;?>,<?php echo $patient_id?>)"> add a sick leave </a>
						</div>
						<div id="sick-sheet"></div>
					</div>
					<div class="tab-pane " id="uploads">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<a class="btn btn-sm btn-success pull-right" onclick="add_patient_upload(<?php echo $visit_id;?>,<?php echo $patient_id?>)"> add an upload </a>
						</div>
						<div id="patients-uploads"></div>
						
					</div>
					<div class="tab-pane " id="diary">
					</div>
					<div class="tab-pane" id="patient_details">

						<?php 
						$v_data['patient_details'] = $patient_details;
						//echo $this->load->view("patient_details", '', TRUE);?>
					</div>
					<div class="tab-pane" id="billing-form">

						<?php //echo $this->load->view("billing", '', TRUE);?>
					</div>
					<div class="tab-pane" id="quotation-form">

						<?php echo $this->load->view("medical_history", '', TRUE);?>
						<?php echo $this->load->view("examination", '', TRUE);?>	
					</div>
					<div class="tab-pane" id="visit_trail">
						<?php echo $this->load->view("visit_trail", '', TRUE);?>
					</div>
				</div>
				<div id="days-bill" style="margin-top:10px;"></div>

			</div>
		</div>
			
	</div>
	
</div>	  

<div class="row">
	<div class="center-align"> 
	 
		<div class="col-md-12">
		  <div class="center-align">
			<?php echo form_open("dental/send_to_accounts/".$visit_id."/".$patient_id, array("class" => "form-horizontal"));?>
			  <input type="submit" class="btn btn-large btn-danger center-align" value="Send To Accounts"/>
			<?php echo form_close();?>
		  </div>
		</div>
	</div>

</div>
<?php
}
else
{
	?>
	<div class="alert alert-danger">Please click attend to patient to view the file</div>
	<?php
}
?>	
	
  
  <script type="text/javascript">
  	
	var config_url = $("#config_url").val();
		
	$(document).ready(function(){
		

	 //  	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
		// 	$("#new-nav").html(data);
		// 	$("#checkup_history").html(data);
		// });

		// get_medication(<?php echo $visit_id;?>);
		// prescription_view();

		// get_surgeries(<?php echo $visit_id;?>);

		get_page_item(1,<?php echo $patient_id;?>);
		visit_display_billing(<?php echo $visit_id;?>);

		procedure_lists(<?php echo $visit_id;?>,<?php echo $patient_id;?>);
		window.localStorage.setItem('tooth_item_checked',0);

		get_days_bill(<?php echo $visit_id;?>,<?php echo $patient_id;?>);
		



	});

	function get_medication(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_medication/"+visit_id;

	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("medication");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function get_surgeries(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_surgeries/"+visit_id;
	    
	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("surgeries");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_surgery(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var date = document.getElementById("datepicker").value;
	    var description = document.getElementById("surgery_description").value;
	    var month = document.getElementById("month").value;
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/surgeries/"+date+"/"+description+"/"+month+"/"+visit_id;
	   
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function delete_surgery(id, visit_id){
	    //alert(id);
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	      var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_surgeries/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_medication(visit_id){
	    var config_url = document.getElementById("config_url").value;
	    var data_url = config_url+"nurse/medication/"+visit_id;
	   
	     var patient_medication = $('#medication_description').val();
	     var patient_medicine_allergies = $('#medicine_allergies').val();
	     var patient_food_allergies = $('#food_allergies').val();
	     var patient_regular_treatment = $('#regular_treatment').val();
	     
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{medication: patient_medication,medicine_allergies: patient_medicine_allergies, food_allergies: patient_food_allergies, regular_treatment: patient_regular_treatment },
	    dataType: 'text',
	    success:function(data){
	     get_medication(visit_id);
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });

	       
	}
  </script>

<script type="text/javascript">
		$(document).ready(function() {
		
		// check_date();
		// load_patient_appointments();

		// tinymce.init({
  //               selector: ".cleditor",
  //               height: "300"
  //           });
  	// alert('dasda');
	});

	function check_date(){
	     var datess=document.getElementById("scheduledate").value;
	     load_schedule();
	     load_patient_appointments_two();
	     if(datess){
		  $('#show_doctor').fadeToggle(1000); return false;
		 }
		 else{
		  alert('Select Date First')
		 }
	}

	function load_schedule(){
		var config_url = $('#config_url').val();
		var datess=document.getElementById("scheduledate").value;
		var doctor= <?php echo $this->session->userdata('personnel_id');?>//document.getElementById("doctor").value;

		var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;
	
		  $('#doctors_schedule').load(url);
		  $('#doctors_schedule').fadeIn(1000); return false;	
	}
	function load_patient_appointments(){
		var patient_id = $('#patient_id').val();
		var current_date = $('#current_date').val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule').load(url);
		$('#patient_schedule').fadeIn(1000); return false;	

		$('#patient_schedule2').load(url);
		$('#patient_schedule2').fadeIn(1000); return false;	
	}
	function load_patient_appointments_two(){
		var patient_id = $('#patient_id').val();
		var current_date = $('#current_date').val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule2').load(url);
		$('#patient_schedule2').fadeIn(1000); return false;	
	}
	function schedule_appointment(appointment_id)
	{
		if(appointment_id == '1')
		{
			$('#appointment_details').css('display', 'block');
		}
		else
		{
			$('#appointment_details').css('display', 'none');
		}
	}

	var config_url = $("#config_url").val();
		
	// $(document).ready(function(){
		
	//   	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
	// 		$("#new-nav").html(data);
	// 		$("#checkup_history").html(data);
	// 	});
	// });

	function pass_tooth()
    {

     var tooth_item_checked = window.localStorage.getItem('tooth_item_checked');


    
   

		var cavity_status = 0;
		var procedure_status = 2;
		var tooth_id ='';
		var visit_id = document.getElementById("visit_id").value;
		var patient_id = document.getElementById("patient_id").value;
		var teeth_section = '';
		var surface_id = '';

		// alert(tooth_item_checked);

      if(tooth_item_checked == 1)
      {
	      	 var tooth_id = document.getElementById("tooth_id").value;
		     
      	    var teeth_section = document.getElementById("teeth_section").value;
      		var surface_id = document.getElementById("surface_id").value;

      		var radios = document.getElementsByName('cavity_status');
		    var cavity_status = null;
		    for (var i = 0, length = radios.length; i < length; i++)
		    {
		     if (radios[i].checked)
		     {
		      // do whatever you want with the checked radio
		       cavity_status = radios[i].value;
		      // only one radio can be logically checked, don't check the rest
		      break;
		     }
		    }   


		    var radios = document.getElementsByName('procedure_status');
		    var procedure_status = null;
		    for (var i = 0, length = radios.length; i < length; i++)
		    {
		     if (radios[i].checked)
		     {
		      // do whatever you want with the checked radio
		       procedure_status = radios[i].value;
		      // only one radio can be logically checked, don't check the rest
		      break;
		     }
		    }

      }
    

    var radios = document.getElementsByName('service_charges');
    // alert(radios);
    var service_charge = null;
    for (var i = 0, length = radios.length; i < length; i++)
    {
	     if (radios[i].checked)
	     {
	      // do whatever you want with the checked radio
	       service_charge = radios[i].value;
	      // only one radio can be logically checked, don't check the rest
	      break;
	     }
    }
    
	// alert(service_charge);

	var checked = false;
	var checked_response = '';
    if(service_charge != null && procedure_status < 3)
    {
    	checked = true;
    }
	else if(service_charge == null && procedure_status < 3)
	{
		checked = false;
		checked_response ='Please select a a service charge on the list';
	}
	else if(service_charge == null && procedure_status == 3)
	{
		checked = true;
		// checked_response ='Please select a a service charge on the list';
	}
	

	if(checked)
	{
    	if(cavity_status == 3 && surface_id === '')
    	{
    		alert('Kindly select a surface of the tooth before you proceed ');
    	}
    	else
    	{


		     var url = "<?php echo base_url();?>dental/save_dentine/"+visit_id+"/"+patient_id;
		     //
		     $.ajax({
		     type:'POST',
		     url: url,
		     data:{tooth_id: tooth_id,patient_id: patient_id,cavity_status: cavity_status,teeth_section: teeth_section,service_charge_id: service_charge,visit_id: visit_id,surface_id:surface_id,plan_status: procedure_status},
		     dataType: 'text',
		     success:function(data){
		       // var prescription_view = document.getElementById("prescription_view");
		       // prescription_view.style.display = 'none';

		         var data = jQuery.parseJSON(data);
		            
		            var status = data.status;

		            if(status == 'success')
		            {
		              // alert(data.message);
		           
		              // display_patient_history(visit_id,patient_id);
		              // close_side_bar(); 
		              if(tooth_item_checked == 1)
		              {


		                if(tooth_id <= 38)
						{
							var newcol = 'chart_type_two';
							get_page_item(7,patient_id,1);
						}
						else
						{
							var newcol = 'chart_type_one';
							get_page_item(7,patient_id,0);
						}
						// alert(newcol);
						// $('a').on('click', function () {
					    $('#' + newcol).prop('checked',true);
					 
						// });
					 }
					 else
					 {
					 	alert(data.message);
					 	visit_display_billing(visit_id);
					 }


		            }
		            else
		            {
		              alert(data.message);
		            }

		     
		     },
		     error: function(xhr, status, error) {
		     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		     
		     }
		     });

	 	}
	}
	else
	{
		alert(checked_response);
	}
	 if(tooth_item_checked == 1)
	 {
	 	check_department_type(tooth_id,teeth_section=null);
	 }
     


     return false;
    }

     function get_page_item(page_id,patient_id,chart_type = null)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = <?php echo $visit_id;?>;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            if(visit_id > 0)
            {

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#page_item').html(data.page_item);
              },
              error: function(xhr, status, error) {
              // alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            }
            else
            {
              var visit_id = null;

              if(page_id > 1)
              {
                alert("Please select a date of a visit you wish to see");
              }
              else
              {
                var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id;  
            
                $.ajax({
                type:'POST',
                url: url,
                data:{page_id: page_id,patient_id: patient_id},
                dataType: 'text',
                success:function(data){
                    var data = jQuery.parseJSON(data);
                    var page_item = data.page_item;
                    $('#page_item').html(data.page_item);
                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                    // display_patient_bill(visit_id);
                }
                });   
              }
              
            }
       
       	visit_display_billing(visit_id);
       // return false;

    }

    function set_tooth_number(teeth_number,teeth_section=null)
  	{ 	

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;

		var config_url = document.getElementById("config_url").value;
		// alert(teeth_section);
     	var url = config_url+"dental/set_tooth_surfaces/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			// document.getElementById("current-sidebar-div").style.display = "block"; 
			// $("#current-sidebar-div").html(data);
			// alert(data);
			document.getElementById("surface_id").value = data;
			// $("#dentine-list-div").html(data);
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

  }

    function check_department_type(teeth_number,teeth_section=null)
  	{

  		window.localStorage.setItem('tooth_item_checked',0);

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;


	    var old_tooth = window.localStorage.getItem('teeth_number_old');
	    // alert(old_tooth);
		if(old_tooth > 0)
		{
			document.getElementById(''+old_tooth+'').style.color = "#000";
		}
		
		// document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			// document.getElementById("current-sidebar-div").style.display = "block"; 
			// $("#current-sidebar-div").html(data);

			window.localStorage.setItem('tooth_item_checked',1);
			$("#dentine-list-div").html(data.dental_formula);
			document.getElementById(''+teeth_number+'').style.color = "#FF0000";
			// alert(teeth_number);
			$("#toothnumber").html('TOOTH # '+teeth_number);

			$("#teeth-marker").html(data.teeth_marker);




			window.localStorage.setItem('teeth_number_old',teeth_number);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

		procedure_lists(visit_id,patient_id);

     // var XMLHttpRequestObject = false;
          
     //  if (window.XMLHttpRequest) {
      
     //      XMLHttpRequestObject = new XMLHttpRequest();
     //  } 
          
     //  else if (window.ActiveXObject) {
     //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     //  }
      
     //  var config_url = document.getElementById("config_url").value;
     //  var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
     //  // alert(url);
     //  if(XMLHttpRequestObject) {
                  
     //      XMLHttpRequestObject.open("GET", url);
                  
     //      XMLHttpRequestObject.onreadystatechange = function(){
              
     //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

     //              document.getElementById("dental-formula").innerHTML=XMLHttpRequestObject.responseText;
     //          }
     //      }
                  
     //      XMLHttpRequestObject.send(null);
     //  }
  }




  function save_other_sick_off(visit_id)
	{
		 // start of saving rx
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_other_patient_sickoff/"+visit_id;
        //window.alert(data_url);
         var doctor_notes_rx = tinymce.get('deductions_and_other'+visit_id).getContent();
         // var doctor_notes_rx = $('#deductions_and_other').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes_rx},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the payment information");
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}

  function prescription_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_prescription/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-prescription").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }


	function save_prescription(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_prescription/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var prescription = tinymce.get('visit_prescription'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(prescription);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{prescription: prescription},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           close_side_bar();
           prescription_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
  
	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}
	function get_chart_type(chart_type)
	{

		var config_url = $('#config_url').val();
		var patient_id = $('#patient_id').val();
        var data_url = config_url+"dental/set_chart_type/"+patient_id+"/"+chart_type;

        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{chart_type: chart_type},
        dataType: 'text',
		success:function(data)
		{

		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);

        }

        });


        window.localStorage.setItem('tooth_item_checked',0);
		if(chart_type == 0)
		{
			$('#children-div').css('display', 'block');
			$('#adult-div').css('display', 'none');
		}
		else
		{
			$('#children-div').css('display', 'none');
			$('#adult-div').css('display', 'block');
		}

	}
	function procedure_lists(visit_id,patient_id)
	{

		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/get_visit_procedures/"+visit_id+"/"+patient_id;

         var lab_test = $('#q').val();
        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id,query: lab_test},
        dataType: 'text',
		success:function(data)
		{
			$("#procedure-lists").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
      // end of saving rx
	}
	function update_service_charge_bill(visit_id,patient_id,service_charge_id,amount,visit_type_id)
	{

		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/add_visit_bill/"+visit_id+"/"+patient_id+"/"+service_charge_id+"/"+amount+"/"+visit_type_id;

        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// $("#procedure-lists").html(data);		
			visit_display_billing(visit_id);
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
      // end of saving rx
	}

	function visit_display_billing(visit_id)
	{



		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_billing/"+visit_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#visit-billing-items").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	    
	}




	// accounts


	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"accounts/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         visit_display_billing(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        visit_display_billing(v_id);
		    	alert(error);
		    }

		    });

		}

	}
	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  
	   // alert(billed_amount);
	    grand_total(id, units, billed_amount, v_id);

	}
	function grand_total(procedure_id, units, amount, v_id){



		 var config_url = document.getElementById("config_url").value;
	     var data_url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   
	      // var tooth = document.getElementById('tooth'+procedure_id).value;
	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         visit_display_billing(v_id);
	     alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        visit_display_billing(v_id);
	    alert(error);
	    }

	    });
	}

	function delete_procedure(id, visit_id,dentine_id = null){

		var res = confirm('Are you sure you want to delete this procedure ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;
	        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"nurse/delete_procedure/"+id+"/"+dentine_id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	close_side_bar();
		                visit_display_billing(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	}

	function get_procedure_information(visit_charge_id,visit_id)
	{

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;



		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/visit_charge_information/"+visit_charge_id+"/"+visit_id;
		// window.alert(url);
		$.ajax({
		type:'POST',
		url: url,
		data:{patient_id: patient_id},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function update_visit_charges(visit_id,visit_charge_id)
	{
		var config_url = $('#config_url').val();		

		var notes = tinymce.get('notes'+visit_charge_id).getContent();	
		var units = $('#units'+visit_charge_id).val();
		var teeth_id = $('#teeth_id'+visit_charge_id).val();
		var dentine_id = $('#dentine_id'+visit_charge_id).val();
		var billed_amount = $('#billed_amount'+visit_charge_id).val();

		var patient_id = $('#patient_id'+visit_charge_id).val();

		var radios = document.getElementsByName('work_status');
	    var work_status = null;
	    for (var i = 0, length = radios.length; i < length; i++)
	    {
	     if (radios[i].checked)
	     {
	      // do whatever you want with the checked radio
	       work_status = radios[i].value;
	      // only one radio can be logically checked, don't check the rest
	      break;
	     }
	    }


		var url = config_url+"accounts/update_charges_account/"+visit_id+"/"+visit_charge_id;	
		 
		$.ajax({
		type:'POST',
		url: url,
		data:{notes: notes,visit_id: visit_id,visit_charge_id: visit_charge_id,units: units,billed_amount: billed_amount,work_status: work_status,dentine_id: dentine_id, teeth_id: teeth_id,patient_id: patient_id},
		dataType: 'text',
		
		success:function(data)
		{
		  	var data = jQuery.parseJSON(data);

		  	if(data.message == "success")
			{	

				close_side_bar();	

			}
			else
			{
				alert(data.result);
			}
			visit_display_billing(visit_id);

			if(teeth_id <= 38)
			{
				var newcol = 'chart_type_two';
				get_page_item(7,patient_id,1);
			}
			else
			{
				var newcol = 'chart_type_one';
				get_page_item(7,patient_id,0);
			}
			// alert(newcol);
			// $('a').on('click', function () {
		    $('#' + newcol).prop('checked',true);
					 

		},
		error: function(xhr, status, error) {

				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		 
		visit_display_billing(visit_id);	
	   
		
	// });
	}


	function add_patient_prescription(visit_id,patient_id)
	{



		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_prescription/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function sicksheet_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_sick_leave/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("sick-sheet").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}


	function save_leave_note(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_sick_leave_note/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var sick_leave = tinymce.get('sick_leave'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(sick_leave);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{sick_leave: sick_leave},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           close_side_bar();
           sicksheet_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
	function add_patient_sick_note(visit_id,patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_sick_note/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	// uploads


	function uploads_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_uploads/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patients-uploads").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}


	$(document).on("submit","form#uploads-form",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		var patient_id = $('#patient_id').val();
		var visit_id = $('#visit_id').val();

		var config_url = $('#config_url').val();	

		var url = config_url+"dental/upload_documents/"+patient_id+"/"+visit_id;
		
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
       success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
         
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				uploads_view();
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function add_patient_upload(visit_id,patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_uploads/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function delete_upload_file(document_upload_id,visit_id)
	{
		var res = confirm('Are you sure you want to delete this file ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/delete_document_scan/"+document_upload_id+"/"+visit_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				uploads_view();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		
		
	}

	function bill_charge(visit_charge_id,visit_id,patient_id)
	{

		var res = confirm('Are you sure you want to bill this procedure ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/bill_procedure/"+visit_charge_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				visit_display_billing(visit_id);


			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		get_days_bill(visit_id,patient_id);
	}

	function unbill_charge(visit_charge_id,visit_id,patient_id)
	{

		var res = confirm('Are you sure you want to unbill this procedure ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/remove_bill_procedure/"+visit_charge_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				visit_display_billing(visit_id);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		get_days_bill(visit_id,patient_id);
	}
	function get_days_bill(visit_id,patient_id)
	{
		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_days_billing/"+patient_id+"/"+visit_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#days-bill").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	}

	function add_new_procedure(visit_id, patient_id)
	{

		$('#procedure-div').css('display', 'none');
		// $('#procedure-list-div').css('display', 'none');	
		$('#service_charge-div').css('display', 'block');	
		$('#procedure-search-div').css('display', 'none');	
		$('#new-button-div').css('display', 'none');	
		$('#add-button-div').css('display', 'block');
		
		
	}

	function back_procedure_list(visit_id, patient_id)
	{

		$('#procedure-div').css('display', 'block');
		// $('#procedure-list-div').css('display', 'block');

		$('#service_charge-div').css('display', 'none');	
		$('#procedure-search-div').css('display', 'block');	
		$('#new-button-div').css('display', 'block');	
		$('#add-button-div').css('display', 'none');	
		
	}
	function add_service_charge(visit_id, patient_id)
	{

		var res = confirm('Are you sure you want to add this as a service charge ?');


		if(res)
		{
			var config_url = $('#config_url').val();

			var service_charge_name = $('#service_charge_name').val();
	        var data_url = config_url+"dental/add_new_service";

	        // alert(data_url);
	        $.ajax({
	        type:'POST',
	        url: data_url,
	        data:{service_charge_name: service_charge_name},
	        dataType: 'text',
			success:function(data)
			{
				var data = jQuery.parseJSON(data);
	    
				// alert(data);

				if(data.status == "success")
				{
					alert('You have successfully added the procedure');

					back_procedure_list(visit_id, patient_id);

				}
				else
				{
					alert(data.message);
				}
				
					
			},
	        error: function(xhr, status, error) {
		        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        alert(error);
	        }

	        });
		}
	}


function notes_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_dental_notes/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("dental-notes").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

function add_dental_notes(visit_id,patient_id)
{



	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = document.getElementById("config_url").value;
 	var url = config_url+"dental/patient_dental_notes/"+visit_id+"/"+patient_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){

		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);

		tinymce.init({
            selector: ".cleditor",
            height: "300"
        });
	
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}


function save_dental_notes(patient_id,visit_id)
{
	 // start of saving rx

    var config_url = $('#config_url').val();
    var data_url = config_url+"dental/save_dental_notes/"+patient_id+"/"+visit_id;
   
    var prescription = tinymce.get('dental_notes'+visit_id).getContent();

    $.ajax({
    type:'POST',
    url: data_url,
    data:{prescription_notes: prescription},
    dataType: 'text',
    success:function(data){
    //obj.innerHTML = XMLHttpRequestObject.responseText;
       // window.alert("You have successfully updated the notes");
       close_side_bar();
       notes_view();
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
  // end of saving rx
}
</script>