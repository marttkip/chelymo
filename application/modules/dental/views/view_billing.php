<?php

$rs2 = $this->nurse_model->get_visit_procedure_billed_list($visit_id,NULL,$patient_id);


echo "
<table align='center' class='table table-striped table-hover table-condensed'>
	<tr>
		<th>Date</th>
		<th>Th</th>
		<th>Surf</th>
		<th>Dx</th>
		<th>Status</th>
		<th>Description</th>
		<th>Start</th>
		<th>Provider</th>
		<th>Total</th>
		<th>Charge to</th>
	</tr>		
";                     
		$total= 0;  
		if($rs2->num_rows() >0){
			foreach ($rs2->result() as $key1):
				$v_procedure_id = $key1->visit_charge_id;
				$procedure_id = $key1->service_charge_id;
				$visit_charge_amount = $key1->visit_charge_amount;
				$units = $key1->visit_charge_units;
				$procedure_name = $key1->service_charge_name;
				$service_id = $key1->service_id;
				$diagnosis = $key1->diagnosis;
				$teeth = $key1->teeth_id;
				$surface = $key1->surface_id;
				$visit_type_id = $key1->visit_type_id;
				$visit_charge_notes = $key1->visit_charge_notes;
				$personnel_fname = $key1->personnel_fname;
				$personnel_onames = $key1->personnel_onames;
				$date = $key1->date;
				$plan_status = $key1->plan_status;
				$charged = $key1->charged;
				$visit_invoice_id = $key1->visit_invoice_id;

				$date = $key1->date;

				if($visit_type_id == 1)
				{
					$visit = 'SELF';
				}
				else
				{
					$visit = 'INSURANCE';
				}
				if($plan_status == 1)
				{
					$plan_color = 'red';
					$status = 'TP';
				}else if($plan_status == 2)
				{
					$plan_color = 'black';
					$status = 'CP';
				}
				else{
					$plan_color = 'green';
					$status = 'EX';
				}

				$total= $total +($units * $visit_charge_amount);
				$checked="";
				$personnel_check = TRUE;
				if($personnel_check)
				{
					$checked = "<td>
								<a class='btn btn-xs btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
								</td>
								<td>
								<a class='btn btn-xs btn-danger'  onclick='change_payer(".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-recycle'></i></a>
								</td>
								<td>
									<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
								</td>";
				}
				if($plan_status  < 3)
				{
					if($charged == 1 )
					{
						if($date == date('Y-m-d') AND empty($visit_invoice_id))
						{
							$charge_button = '<td>
												<a class="btn btn-xs btn-danger"  onclick="unbill_charge('.$v_procedure_id.', '.$visit_id.','.$patient_id.')"><i class="fa fa-arrow-left"></i> Unbill</a>
											</td>';	
						}
						else
						{
							$charge_button = '<td>Billed</td>';
						}
						
					}
					else
					{
						$charge_button = '<td>
												<a class="btn btn-xs btn-success"  onclick="bill_charge('.$v_procedure_id.', '.$visit_id.','.$patient_id.')"><i class="fa fa-arrow-right"></i> Bill</a>
											</td>';
					}
				}
				else
				{
					$charge_button = '<td>-</td>';
				}


				
				echo"

						<tr  style='color:".$plan_color." !important;'> 
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$date."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$teeth."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$surface."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$diagnosis."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$status."</td>
							<td align='left' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$procedure_name."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$units."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".$personnel_fname." ".$personnel_onames."</td>
							<td align='center' onclick='get_procedure_information(".$v_procedure_id.",".$visit_id.")'>".number_format($units*$visit_charge_amount)."</td>
							".$charge_button."
						</tr>	
				";

				if(!empty($visit_charge_notes))
				{
					echo"
							<tr style='color:".$plan_color." !important;'> 
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td colspan='3'>".$visit_charge_notes."</td>
								
								<td ></td>
								<td ></td>
							</tr>	
						";

				}

				$procedure_rs = $this->dental_model->get_visit_procedure_notes(NULL,$v_procedure_id);

				if($procedure_rs->num_rows() > 0)
				{
					foreach ($procedure_rs->result() as $key => $value) {
						# code...
						$notes = $value->notes;
						$visit_date = $value->visit_date;
						$personnel_fname = $value->personnel_fname;

						echo"
							<tr style='color:".$plan_color." !important;'> 
								<td></td>
								<td></td>
								<td colspan='3'>".date('jS M Y',strtotime($visit_date))."</td>
								<td colspan='3'>".$notes."</td>
								<td colspan='1'>".$personnel_fname."</td>
								<td ></td>
							</tr>	
						";
					}
				}
				


				endforeach;

		}
// echo"
// <tr bgcolor='#D9EDF7'>
// <td></td>
// <td></td>
// <th>Grand Total: </th>
// <th colspan='3'><div id='grand_total'>".number_format($total)."</div></th>
// <td></td>
// <td></td>
// </tr>
//  </table>
// ";

$rs_pa = $this->dental_model->get_payment_info($visit_id);

if(count($rs_pa) >0){
	foreach ($rs_pa as $r2):
		# code...
		$payment_info = $r2->payment_info;

		// get the visit charge

	endforeach;


	


}

// echo '<div class="row">
//       	<div class="col-md-12">
//   			<div class="form-group">
//    				<label class="col-lg-4 control-label">Deductions and/or additions:  </label>
//     		<div class="col-lg-8">
//       				<textarea id="deductions_and_other_info" rows="5" cols="50" class="form-control col-md-12" > '.$payment_info.' </textarea>
//       		</div>
//       	</div>
//     </div>
//   </div>';

//   echo '
//   <br>
// 	<div class="row">
//         <div class="form-group">
//             <div class="col-lg-12">
//                 <div class="center-align">
//                       <a hred="#" class="btn btn-large btn-info" onclick="save_other_deductions('.$visit_id.')">Save other payment information</a>
//                   </div>
//             </div>
//         </div>
//     </div>';
?>