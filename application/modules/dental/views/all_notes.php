<?php

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] =  $query_data = $this->dental_model->get_notes($type, $visit_id);

if($query_data->num_rows() > 0)
{
	foreach ($query_data->result() as $key => $value_two) {
		# code...
		$summary = $value_two->notes_name;
		$notes_type_name = $value_two->notes_type_name;
	}
	
}
else
{
	$summary = '';
}
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}

if($type == 1)
{
	$checked = 'facial_asymmetry_description';

}else if($type == 2)
{
	$checked = 'intra_oral_description';
}
else if($type == 3)
{
	$checked = 'crossibet_description';
}
else if($type == 4)
{
	$checked = 'opg_description';
}
else if($type == 5)
{
	$checked = 'ceph_description';
}
else if($type == 6)
{
	$checked = 'pa_description';
}
else if($type == 7)
{
	$checked = 'bw_description';
}
else if($type == 8)
{
	$checked = 'diagnosis_description';
}
else if($type == 9)
{
	$checked = 'problem_description';
}
else if($type == 10)
{
	$checked = 'treatment_description';
}
else if($type == 11)
{
	$checked = 'anchorage_description';
}
else if($type == 12)
{
	$checked = 'appliance_description';
}
// var_dump($summary); die();
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['module'] = $module;
$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);
if($module <> 1)
{


?>
<div class='col-md-12'>
	<h4><?php echo $notes_type_name;?></h4>
	<div class="row">
    	<div class='col-md-12'>
        	<input type="hidden" name="date" value="<?php echo date('Y-m-d');?>" />
        	<input type="hidden" name="time" value="<?php echo date('H:i');?>" />
            <textarea class='form-control cleditor' id='<?php echo $checked.$visit_id;?>' rows="5" placeholder="Describe" ><?php echo $summary?></textarea>
        </div>
    </div>
    <br>
    <div class="row" >
    	<div class='col-md-12 center-align'>
    		<a class='btn btn-info btn-sm' type='submit' onclick='save_doctors_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,<?php echo $type;?>)'> Save Note</a>
    	</div>
    </div>
    <br>
</div>
<div class='col-md-12'>

	<?php echo $notes?>
</div>
<?php

}
else
{
?>
<div class='col-md-12'>

	<?php echo $notes?>
</div>
<?php

}
?>
