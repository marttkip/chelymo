
<div class="row" style="margin-top:10px;">
		<div class="col-md-12">
       <?php
       if($patient_other_documents->num_rows() > 0)
        {
            $count = 0;
                
            $identification_result = 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Date Uploaded</th>
                        <th>Document Name</th>
                        <th>Download Link</th>
                        <th colspan="3">Actions</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            foreach ($patient_other_documents->result() as $row)
            {
                $document_type_name = $row->document_type_name;
                $document_upload_id = $row->document_upload_id;
                $document_name = $row->document_name;
                $document_upload_name = $row->document_upload_name;
                $document_status = $row->document_status;

					$created = $row->created;
                $created = date('jS M Y H:i a',strtotime($row->created));
                //create deactivated status display
                if($document_status == 0)
                {

                    $status = '<span class="label label-default">Deactivated</span>';
                    $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-personnel-identification/'.$document_upload_id.'/'.$visit_id.'" onclick="return confirm(\'Do you want to activate?\');" title="Activate "><i class="fa fa-thumbs-up"></i></a>';
                }
                //create activated status display
                else if($document_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                    $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-personnel-identification/'.$document_upload_id.'/'.$visit_id.'" onclick="return confirm(\'Do you want to deactivate ?\');" title="Deactivate "><i class="fa fa-thumbs-down"></i></a>';
                }
                
                $count++;
                $identification_result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$document_type_name.'</td>
                        <td>'.$created.'</td>
                        <td>'.$document_name.'</td>
                        <td><a href="'.$this->document_upload_location.''.$document_upload_name.'" target="_blank" >Download Here</a></td>
                        <td>'.$status.'</td>
                        <!--<td>'.$button.'</td>-->
                        <td><a  class="btn btn-sm btn-danger" onclick="delete_upload_file('.$document_upload_id.','.$visit_id.')" title="Delete"><i class="fa fa-trash"></i></a></td>
                    </tr> 
                ';
            }
            
            $identification_result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $identification_result = "<p>No plans have been added</p>";
        }
        echo $identification_result;
       ?>
   </div>
</div>
   