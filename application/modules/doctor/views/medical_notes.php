<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));



$title = 'Patient Visit Chart';
$heading = 'Checkup';
$number = 'MI/MED/00'.$visit_id;



?>


<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
		.receipt_spacing{letter-spacing:0px; font-size: 10px;}
		.center-align{margin:0 auto; text-align:center;}
		
		.receipt_bottom_border{border-bottom: #888888 medium solid;}
		.row .col-md-12 table {
			border:solid #000 !important;
			border-width:1px 0 0 1px !important;
			font-size:9px;
			padding: 0px;
		}
		.row .col-md-12 th, .row .col-md-12 td {
			border:solid #000 !important;
			border-width:0 1px 1px 0 !important;
			padding-top: 0;
			padding-bottom: 0;
			padding: 3px;
		}
		
		.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 10px;}
		.title-img{float:left; padding-left:30px;}
		 .col-print-1 {width:8%;  float:left;}
        .col-print-2 {width:16%; float:left;}
        .col-print-3 {width:25%; float:left;}
        .col-print-4 {width:33%; float:left;}
        .col-print-5 {width:42%; float:left;}
        .col-print-6 {width:50%; float:left;}
        .col-print-7 {width:58%; float:left;}
        .col-print-8 {width:66%; float:left;}
        .col-print-9 {width:75%; float:left;}
        .col-print-10{width:83%; float:left;}
        .col-print-11{width:92%; float:left;}
        .col-print-12{width:100%; float:left;}

        .radio, .checkbox {
			position: relative;
			display: block;
			margin-top: 0px !important;
			margin-bottom: 0px !important;
		}
	</style>
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $title;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
       <!--  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
         <!-- <script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery/dist/jquery.min.js"></script> -->
         <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script> <!-- jQuery -->
    </head>
    <body class="receipt_spacing">
    	     <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" width="100%" height="100%" style="position:fixed; opacity: 0.1;z-index: -1; position: fixed;" />
    	  <input type="hidden" id="base_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
    	<div class="row" >
        	<div class="row">
	        	<div class="col-xs-12">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo center-align" style="height:100px"/>
	            </div>
	        </div>
        	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        </div>
        
      <div class="row receipt_bottom_border" >
        	<div class="col-print-12 center-align">
            	<strong><?php echo $title;?></strong>
            </div>
        </div>
        
        <!-- Patient Details -->
    	<div class="row receipt_bottom_border" >
    		<div class="col-md-12">
	        	<div class="col-print-6">
	            	<div class="row">
	                	<div class="col-print-12">
	                    	
	                    	<div class="title-item">Patient Name:</div>
	                        
	                    	<?php echo $patient_surname.' '.$patient_othernames; ?>
	                    </div>
	                </div>
	            	
	            	<div class="row">
	                	<div class="col-print-12">
	                    	<div class="title-item">Patient Number:</div> 
	                        
	                    	<?php echo $patient_number; ?>
	                    </div>
	                </div>
	            
	            </div>
	            
	        	<div class="col-print-6">
	            	<div class="row">
	                	<div class="col-print-12">
	                    	<div class="title-item">Doc No.:</div>
	                        
	                    	<?php echo $number; ?>
	                    </div>
	                </div>
	            	
	            	<div class="row">
	                	<div class="col-print-12">
	                    	<div class="title-item">Att. Doctor: </div> 
	                        
	                    	<?php echo $doctor; ?>
	                    </div>
	                </div>
	                <div class="row">
	                	<div class="col-print-12">
	                    	<div class="title-item">Visit Date: </div> 
	                        
	                    	<?php echo $visit_date; ?>
	                    </div>
	                </div>
	            </div>
	        </div>
        </div>
        
    
	    <div class=" receipt_bottom_border">
	        <div class="col-md-12">
	        	  
	        	<div class="row">
	                <div class="col-print-12 pull-left">
	                	<h5 style="font-weight:bold">VITAL</h5>
	                	<tr style="height:5px;"><?php echo $this->load->view("nurse/show_previous_vitals", TRUE); ?></tr>
	                </div>
	            </div>
	            
	        
	            <div class="row">
	                <div class="col-print-6 pull-left">
	                	<h5 style="font-weight:bold">CHIEF COMPLAIN</h5>
	                	<div class="doctor-chief-complain"></div>
	                </div>
	                <div class="col-print-6 pull-left">
	                	<h5 style="font-weight:bold">HISTORY OR PRESENTING COMPLAIN</h5>
	                	<div class="other-plans"></div>  
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-print-6 pull-left">
	                	<h5 style="font-weight:bold">EXAMINATION AND PLAN</h5>
	                	 <div class="examination-plans"></div>  
	                </div>
	                <div class="col-print-6 pull-left">
	                	<h5 style="font-weight:bold">IMPRESSION</h5>
	                	<div class="assessment-list"></div>  
	                </div>
	            </div>
	         	<div class="row">
	                <div class="col-print-12 pull-left">
	                	<h5 style="font-weight:bold">LABS</h5>
             			<div class="view-lab-result-view"></div>            
         			</div>
	            </div>
	            
	          
	           	<div class="row">
	                
	                <div class="col-print-12 pull-right">
	                   <h5  style="font-weight:bold">PLAN</h5>
	                	<div class="other-plans"></div> 
	                </div>
	            </div>

	            <div class="row">
	             	<div class="col-md-12">
		            	<h5 class="left-align">DIAGNOSIS</h5>
		            </div>
	            	<div class="col-md-6">
	            		<h5 class="left-align">MOH</h5>
	                	<div class="visit_diagnosis_moh"></div>
	                </div> 
	                <div class="col-md-6">
	                	<h5 class="left-align">ICD 10 DIAGNOSIS</h5>
	                    <div class='visit_diagnosis_original'></div>
	                </div>  

               
	            </div>

                <div class="row" style="margin-top:20px;">
                    <div class="col-md-12">
                        <strong>DRUGS PRESCRIBED</strong>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered table-striped col-md-12">
                            <thead>
                            <tr>
                              <th>#</th>
                              <th>Drug Name</th>
                              <th>Prescription</th>
                              <th>Days</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $rs = $this->pharmacy_model->select_prescription($visit_id);
                            $temp_visit_charge_amount =0;
                           // $total_selected_drugs = count($selected_drugs);
                            $num_rows =count($rs);
                            $s=0;
                            if($num_rows > 0){
                            foreach ($rs as $key_rs):
                                //var_dump($key_rs->prescription_substitution);
                                $service_charge_id =$key_rs->product_id;
                                $checker_id = $key_rs->checker_id;
                                if(empty($checker_id))
                                {
                                    $checker_id = $service_charge_id;
                                }
                                $frequncy = $key_rs->drug_times_name;
                                $id = $key_rs->prescription_id;
                                $date1 = $key_rs->prescription_startdate;
                                $date2 = $key_rs->prescription_finishdate;
                                $sub = $key_rs->prescription_substitution;
                                $duration = $key_rs->drug_duration_name;
                                $sub = $key_rs->prescription_substitution;
                                $duration = $key_rs->drug_duration_name;
                                $consumption = $key_rs->drug_consumption_name;
                                $quantity = $key_rs->prescription_quantity;
                                $medicine = $key_rs->product_name;
                                $charge = $key_rs->product_charge;
                                $visit_charge_id = $key_rs->visit_charge_id;
                                $number_of_days = $key_rs->number_of_days;
                                $units_given = $key_rs->units_given;


                                $rs2 = $this->pharmacy_model->get_drug($service_charge_id);
                                $doseunit = '';
                                foreach ($rs2 as $key_rs2 ):
                                    $drug_type_id = $key_rs2->drug_type_id;
                                    $admin_route_id = $key_rs2->drug_administration_route_id;
                                    $doseunit = $key_rs2->unit_of_measure;
                                    $drug_type_name = $key_rs2->drug_type_name;
                                endforeach;

                                if(!empty($admin_route_id)){
                                    $rs3 = $this->pharmacy_model->get_admin_route2($admin_route_id);
                                    $num_rows3 = count($rs3);
                                    if($num_rows3 > 0){
                                        foreach ($rs3 as $key_rs3):
                                            $admin_route = $key_rs3->drug_administration_route_name;
                                        endforeach;
                                    }
                                }
                                

                                $rsf = $this->pharmacy_model->select_invoice_drugs($visit_id,$service_charge_id);
                                $num_rowsf = count($rsf);
                                $sum_units = 0;
                                $visit_charge_amount = 0;
                                
                                foreach ($rsf as $key_price):
                                    $sum_units = $key_price->visit_charge_units;
                                    $visit_charge_amount = $key_price->visit_charge_amount;
                                endforeach;
                                
                                //display selected drugs
                              
                                
                                       
                                $amoun=$visit_charge_amount* $sum_units ;
                                $total_visit_charge_amount=$amoun+$temp_visit_charge_amount;
                                $temp_visit_charge_amount=$total_visit_charge_amount; 
                                $s++;
                                 ?>
                                  
                                            <?php
                                        
                                    
                                
                                
                            
                                
                                    $amoun=$visit_charge_amount* $sum_units ;
                                    $total_visit_charge_amount=$amoun+$temp_visit_charge_amount;
                                    $temp_visit_charge_amount=$total_visit_charge_amount; 
                                    $s++;
                                    ?>
                                        <tr>
                                            <td><?php echo $s;?></td>
                                            <td><?php echo $medicine;?></td>
                                            <td><?php echo $consumption;?> - <?php echo $duration;?> - <?php echo $frequncy;?></td>
                                            <td><?php echo $number_of_days;?></td>
                                        </tr>
                                          
                                    <?php
                                
                                endforeach;

                                ?>
                                <?php
                                $this->session->unset_userdata('selected_drugs');
                                }
                                ?>
                                
                            </tbody>
                       </table>
                    </div>
                </div>
        
        
      

       
        
       
       
        
    	
       
 		<div class="row" style="font-style:bold; font-size:11px;">
	            <div class="col-print-12 ">
	                <div class="col-md-6 pull-left">
	                    <p>ATTENING DOCTOR: </p>
	                    <p><strong><?php echo $doctor;?></strong></p>
	                    <br>

	                    <p>Sign : .................................. </p>
	                     <p>Printed : <?php echo date('jS M Y H:i')?> </p>
	                </div>
	            </div>
	        </div>
    </body>
    
</html>

<script type="text/javascript">

  $(document).ready(function(){
      var visit_id = <?php echo $visit_id?>;

    
      display_notes(<?php echo $visit_id?>,3);
      display_notes(<?php echo $visit_id?>,7);
      display_notes(<?php echo $visit_id?>,4);
      display_notes(<?php echo $visit_id?>,5);
      display_notes(<?php echo $visit_id?>,9);
        // alert(visit_id);
      // display_notes(<?php echo $visit_id?>,21);
      // display_notes(<?php echo $visit_id?>,22);
      // objective_ros(visit_id);
      get_disease(visit_id);
      get_disease_moh_new(visit_id);

      view_lab_test_done(visit_id);



    


      
  });

   function display_notes(visit_id,type,module_id=null)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_notes/"+visit_id+"/"+type+"/1";

     


     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                if(type == 3)
                {

                 // document.getElementById("doctor-chief-complain").innerHTML=XMLHttpRequestObject.responseText;
                  $(".doctor-chief-complain").html(XMLHttpRequestObject.responseText);
                }
                else if(type == 4)
                {
                  // alert(url);
                   $(".examination-plans").html(XMLHttpRequestObject.responseText);
                  // document.getElementById("").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 7)
                {
                  // document.getElementById("other-plans").innerHTML=XMLHttpRequestObject.responseText;
                   $(".other-plans").html(XMLHttpRequestObject.responseText);
                }
                 else if(type == 5)
                {
                  // document.getElementById("assessment-list").innerHTML=XMLHttpRequestObject.responseText;
                   $(".assessment-list").html(XMLHttpRequestObject.responseText);
                }
                else if(type == 9)
                {
                  // alert("sasjahsa");
                  // document.getElementById("other-diagnosis").innerHTML=XMLHttpRequestObject.responseText;
                    $(".other-diagnosis").html(XMLHttpRequestObject.responseText);
                }
                else if(type == 21)
                {
                  // document.getElementById("other-diagnosis").innerHTML=XMLHttpRequestObject.responseText;
                    $(".nurse-chief-complain").html(XMLHttpRequestObject.responseText);

                    // alert("sasa");
                }
                else if(type == 22)
                {
                  // document.getElementById("other-diagnosis").innerHTML=XMLHttpRequestObject.responseText;
                    $(".nurse-other-plans").html(XMLHttpRequestObject.responseText);
                }
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }


function objective_ros(visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
   var config_url = $('#config_url').val();

  var url = config_url+"nurse/objective_ros/"+visit_id;
  // alert(url);

  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("objective-ros");
      
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        // patient_details(visit_id);
        // objective_findings(visit_id);
        // assessment(visit_id);
        // plan(visit_id);
        // doctor_notes(visit_id);
        // nurse_notes(visit_id);

         tinymce.init({
                  selector: ".cleditor",
                  height: "150"
                });
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}


function get_disease(visit_id){
  
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/get_diagnosis/"+visit_id+"/1";

  
  if(XMLHttpRequestObject) {
    var obj = document.getElementById("visit_diagnosis_original");
    var obj2 = document.getElementById("visit_diagnosis");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        // obj.innerHTML = XMLHttpRequestObject.responseText;
        // obj2.innerHTML = XMLHttpRequestObject.responseText;

        $(".visit_diagnosis_original").html(XMLHttpRequestObject.responseText);
        $(".visit_diagnosis").html(XMLHttpRequestObject.responseText);

        items_get_check_items(visit_id);
      }
    }
    
    XMLHttpRequestObject.send(null);
  }
}


   function view_lab_test_done(visit_id)
   {

   		var config_url = $('#config_url').val();
	  var data_url = config_url+"laboratory/view_visit_lab_results/"+visit_id;
	  // window.alert(data_url);
	  $.ajax({
	  type:'POST',
	  url: data_url,
	  data:{visit_id: visit_id},
	  dataType: 'text',
	  success:function(data){
	 
	   $(".view-lab-result-view").html(data);
	
	  },
	  error: function(xhr, status, error) {
	  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	  alert(error);
	  }

	  });

   }


function get_disease_moh_new(visit_id){
  
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/get_moh_diagnosis/"+visit_id;
// alert(url);
  
  if(XMLHttpRequestObject) {
    var obj = document.getElementById("visit_diagnosis_moh");
    var obj2 = document.getElementById("visit_moh_diagnosis");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        // obj.innerHTML = XMLHttpRequestObject.responseText;
        // obj2.innerHTML = XMLHttpRequestObject.responseText;

        $(".visit_diagnosis_moh").html(XMLHttpRequestObject.responseText);
        $(".visit_moh_diagnosis").html(XMLHttpRequestObject.responseText);

        // items_get_check_items(visit_id);


      }
    }
    
    XMLHttpRequestObject.send(null);
  }
}


  </script>

  