<?php
$data['visit_id'] = $visit_id;
?>
<div class="row">
 	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Continuation notes</h2>
			</header>

			<div class="panel-body">
                <!-- vitals from java script -->
                <?php echo $this->load->view("nurse/soap/doctor_notes", $data, TRUE); ?>
                <!-- end of vitals data -->
            </div>
		</section>
    </div>
</div>
	<?php
$dental = 0;
 if($dental == 1)
 {
 
 }
 else{
 $data['visit_id'] = $visit_id;
 $data['lab_test'] = 100;
 ?>
<div class="row">
   <div class="col-md-12">
    <!-- Widget -->
    <section class="panel panel-featured panel-featured-info">
          <header class="panel-heading">
              <h2 class="panel-title">Diagnosis</h2>
          </header>
          <div class="panel-body">
            <div class="padd">
               <!-- vitals from java script -->
               <?php echo $this->load->view("nurse/soap/view_diagnosis", $data, TRUE); ?>
               <!-- end of vitals data -->
            </div>
          </div>
      </section>
    </div>
 </div>
<div class="row">
   <div class="col-md-12">
      <!-- Widget -->
      <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Presenting Complaint</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                 <!-- vitals from java script -->
                 <div id="symptoms">
                    <?php echo $this->load->view("nurse/soap/view_symptoms", $data, TRUE); ?>
                 </div>
                 <!-- end of vitals data -->
              </div>
            </div>
        </section>
      </div>
   </div>

<div class="row">
   <div class="col-md-12">
      <!-- Widget -->
      <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Presenting Illness History</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                 <!-- vitals from java script -->
                 <?php echo $this->load->view("nurse/soap/view_presenting_complant", $data, TRUE); ?>
                 <!-- end of vitals data -->
              </div>
           </div>
      </section>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <!-- Widget -->
      <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Family Social History</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                 <!-- vitals from java script -->
                 <?php echo $this->load->view("nurse/soap/view_assessment", $data, TRUE); ?>
                 <!-- end of vitals data -->
              </div>
            </div>
      </section>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <!-- Widget -->
      <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Review of system</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                 <!-- vitals from java script -->
                 <?php echo $this->load->view("nurse/soap/system_review", $data, TRUE); ?>
                 <!-- end of vitals data -->
              </div>
            </div>
      </section>
   </div>
</div>

 <!--<div class="row">
   <div class="col-md-12">
       Widget 
       <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Objective Findings</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                 visit Procedures from java script 
                 <?php echo $this->load->view("nurse/soap/view_objective_findings", $data, TRUE); ?>
                 end of visit procedures
              </div>
           </div>
      </section>
   </div>
</div>-->



<?php
   }
   ?>

<script type="text/javascript">
   $(function() {
       $("#drug_id").customselect();
       $("#lab_test_id").customselect();
       $("#diseases_id").customselect();
       $("#xray_id").customselect();
       $("#ultrasound_id").customselect();
   });
   $(document).ready(function(){
   
   	//symptoms(<?php echo $visit_id;?>);
   	//objective_findings(<?php echo $visit_id;?>);
   	//assessment(<?php echo $visit_id;?>);
   	//plan(<?php echo $visit_id;?>);
   	//doctor_notes(<?php echo $visit_id;?>);
   	//nurse_notes(<?php echo $visit_id?>);
     	get_disease(<?php echo $visit_id?>);
     	display_prescription(<?php echo $visit_id?>,0);
     	get_lab_table(<?php echo $visit_id;?>);
       get_xray_table(<?php echo $visit_id;?>);
       get_ultrasound_table(<?php echo $visit_id;?>);
       //get_disease(<?php echo $visit_id;?>);
   
   
                    // suregies
       // get_orthopaedic_surgery_table(<?php echo $visit_id;?>);
       // get_opthamology_surgery_table(<?php echo $visit_id;?>);
       // get_obstetrics_surgery_table(<?php echo $visit_id;?>);
       // get_theatre_procedures_table(<?php echo $visit_id;?>);

       get_visit_plan(<?php echo $visit_id;?>)
   
     
       display_inpatient_prescription(<?php echo $visit_id;?>,0);
   });
     
   function symptoms(visit_id)
   {
   	var XMLHttpRequestObject = false;
       
     	if (window.XMLHttpRequest) {
     
       	XMLHttpRequestObject = new XMLHttpRequest();
     	} 
       
     	else if (window.ActiveXObject) {
       	XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     	}
     
      	var config_url = $('#config_url').val();
   
     	var url = config_url+"nurse/view_symptoms/"+visit_id;
    
     	if(XMLHttpRequestObject) {
       
       	var obj = document.getElementById("symptoms");
         
       	XMLHttpRequestObject.open("GET", url);
           
       	XMLHttpRequestObject.onreadystatechange = function(){
         
         		if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           		obj.innerHTML = XMLHttpRequestObject.responseText;
           
   				// patient_details(visit_id);
   				// objective_findings(visit_id);
   				// assessment(visit_id);
   				// plan(visit_id);
   				// doctor_notes(visit_id);
   				// nurse_notes(visit_id);
         		}
       	}
           
       	XMLHttpRequestObject.send(null);
     	}
   }
   
   
   function objective_findings(visit_id){
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
      var config_url = $('#config_url').val();
     var url = config_url+"nurse/view_objective_findings/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       var obj = document.getElementById("objective_findings");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
         }
       }
           
       XMLHttpRequestObject.send(null);
     }
   }
   function save_illness_notes(visit_id){
         var config_url = $('#config_url').val();
           var data_url = config_url+"nurse/save_illness_notes/"+visit_id;
           //window.alert(data_url);
   		console.debug(tinymce.activeEditor.getContent());
   	   	var illness_notes = tinymce.get('doctor_notes_item').getContent();
   		 var illness_notes_date = $('#illness_notes_date').val();
   		 var illness_notes_time = $('#illness_notes_time').val();
           //var doctor_notes = $('#doctor_notes_item').val();//document.getElementById("vital"+vital_id).value;
   		
   		if(doctor_notes_date != '')
   		{
   			if(doctor_notes_time != '')
   			{
   				$.ajax({
   					type:'POST',
   					url: data_url,
   					data:{notes: illness_notes, date: illness_notes_date, time: illness_notes_time},
   					dataType: 'json',
   					success:function(data){
   						if(data.result == 'success')
   						{
   							$('#doctor_notes_section').html(data.message);
   							alert("You have successfully updated the doctors' notes");
   						}
   						else
   						{
   							alert("Unable to update the doctors' notes");
   						}
   					//obj.innerHTML = XMLHttpRequestObject.responseText;
   					},
   					error: function(xhr, status, error) {
   						//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   						alert(error);
   					}
   		
   				});
   			}
   		
   			else
   			{
   				alert('Please select the notes time');
   			}
   		}
   		
   		else
   		{
   			alert('Please select the notes date');
   		}
   
         
   }
   
   function save_doctor_notes(visit_id){
         var config_url = $('#config_url').val();
           var data_url = config_url+"nurse/save_doctor_notes/"+visit_id;
           //window.alert(data_url);
   		console.debug(tinymce.activeEditor.getContent());
   	   	var doctor_notes = tinymce.get('doctor_notes_item').getContent();
   		 var doctor_notes_date = $('#doctor_notes_date').val();
   		 var doctor_notes_time = $('#doctor_notes_time').val();
           //var doctor_notes = $('#doctor_notes_item').val();//document.getElementById("vital"+vital_id).value;
   		
   		if(doctor_notes_date != '')
   		{
   			if(doctor_notes_time != '')
   			{
   				$.ajax({
   					type:'POST',
   					url: data_url,
   					data:{notes: doctor_notes, date: doctor_notes_date, time: doctor_notes_time},
   					dataType: 'json',
   					success:function(data){
   						if(data.result == 'success')
   						{
   							$('#doctor_notes_section').html(data.message);
   							alert("You have successfully updated the doctors' notes");
   						}
   						else
   						{
   							alert("Unable to update the doctors' notes");
   						}
   					//obj.innerHTML = XMLHttpRequestObject.responseText;
   					},
   					error: function(xhr, status, error) {
   						//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   						alert(error);
   					}
   		
   				});
   			}
   		
   			else
   			{
   				alert('Please select the notes time');
   			}
   		}
   		
   		else
   		{
   			alert('Please select the notes date');
   		}
   
         
   }
   
   
   
   
   function assessment(visit_id){
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/view_assessment/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       var obj = document.getElementById("assessment");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
         }
       }
           
       XMLHttpRequestObject.send(null);
     }
   }
      function past(visit_id)
      {
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/view_past/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       var obj = document.getElementById("past");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
         }
       }
           
       XMLHttpRequestObject.send(null);
     }
   }
   
   function plan(visit_id){
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/view_plan/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       var obj = document.getElementById("plan");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
            get_test_results(100, visit_id);
            //closeit(79, visit_id);
            display_prescription(visit_id, 2);
         }
       }
           
       XMLHttpRequestObject.send(null);
     }
   }
   
   function get_test_results(page, visit_id){
    
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     if((page == 1) || (page == 65) || (page == 85)){
     // request from the lab
       url = config_url+"laboratory/test/"+visit_id;
     }
     
     else if ((page == 75) || (page == 100)){
       // this is for the doctor and the nurse
       url = config_url+"laboratory/test2/"+visit_id;
     }
     
     if(XMLHttpRequestObject) {
       if((page == 75) || (page == 85)){
         var obj = window.opener.document.getElementById("test_results");
       }
       else{
         var obj = document.getElementById("test_results");
       }
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
       
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
     //window.alert(XMLHttpRequestObject.responseText);
           obj.innerHTML = XMLHttpRequestObject.responseText;
           if((page == 75) || (page == 85)){
             window.close(this);
           }
           
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   
   function display_prescription(visit_id, page){
     
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
      var config_url = $('#config_url').val();
     var url = config_url+"pharmacy/display_prescription/"+visit_id;
     
     if(page == 1){
       var obj = window.opener.document.getElementById("prescription");
     }
     
     else{
       var obj = document.getElementById("visit_prescription");
     }
     if(XMLHttpRequestObject) {
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
           
           if(page == 1){
             window.close(this);
           
           }
           //plan(visit_id);
         }
       }
       
       XMLHttpRequestObject.send(null);
     }
   }
   
   function open_window_lab(test, visit_id){
   	
     var config_url = $('#config_url').val();
     var win = window.open(config_url+"laboratory/laboratory_list/"+test+"/"+visit_id,"Popup","height=1200, width=600, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
   }
   
   function open_window_xray(test, visit_id){
   	
     var config_url = $('#config_url').val();
     var win = window.open(config_url+"radiology/xray/xray_list/"+test+"/"+visit_id,"Popup","height=1200, width=600, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
   }
   
   function open_window_ultrasound(test, visit_id){
   	
     var config_url = $('#config_url').val();
     var win = window.open(config_url+"radiology/ultrasound/ultrasound_list/"+test+"/"+visit_id,"Popup","height=1200, width=600, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
   }
   
   function open_window_surgery(test, visit_id){
   	
     var config_url = $('#config_url').val();
     var win = window.open(config_url+"theatre/surgery_list/"+test+"/"+visit_id,"Popup","height=1200, width=600, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
   }
   
   function open_symptoms(visit_id){
     var config_url = $('#config_url').val();
     var win = window.open(config_url+"nurse/symptoms_list/"+visit_id,"Popup","height=1000,width=600,,scrollbars=yes,"+ 
                           "directories=yes,location=yes,menubar=yes," + 
                            "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
       
     
   }
   
   function open_objective_findings(visit_id){
     var config_url = $('#config_url').val();
     var win = window.open(config_url+"nurse/objective_finding/"+visit_id,"Popup","height=600,width=1000,,scrollbars=yes,"+ 
                           "directories=yes,location=yes,menubar=yes," + 
                            "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
     
   }
   
   
   function save_assessment(visit_id)
   {
   	var assessment = tinymce.get('visit_assessment').getContent();
   	var visit_id = '<?php echo $visit_id;?>';
   	var config_url = $('#config_url').val();
   	var notes_type_id = 5;
   	var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
   	
   	var notes_date = '<?php echo date('Y-m-d');?>';
   	var notes_time = '<?php echo date('H:i');?>';
   	$.ajax({
   		type:'POST',
   		url: data_url,
   		data:{notes: assessment, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
   		dataType: 'json',
   		success:function(data){
   			if(data.result == 'success')
   			{
   				$('#assessment_section').html(data.message);
   				tinymce.get('visit_assessment').setContent('');
   				$('#add_assessment').modal('hide');
   				//initiate WYSIWYG editor
   				tinymce.init({
   					selector: ".cleditor",
   					height : "300"
   				});
   				alert("You have successfully added the assessment");
   			}
   			else
   			{
   				alert("Unable to add the assessment");
   			}
   		//obj.innerHTML = XMLHttpRequestObject.responseText;
   		},
   		error: function(xhr, status, error) {
   			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   			alert(error);
   		}
   
   	});
   }
   function save_presenting_complaint(visit_id)
   {
    var assessment = tinymce.get('visit_presenting_complaint').getContent();
    var visit_id = '<?php echo $visit_id;?>';
    var config_url = $('#config_url').val();
    var notes_type_id = 7;
    var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
    
    var notes_date = '<?php echo date('Y-m-d');?>';
    var notes_time = '<?php echo date('H:i');?>';
    $.ajax({
      type:'POST',
      url: data_url,
      data:{notes: assessment, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
      dataType: 'json',
      success:function(data){
        if(data.result == 'success')
        {
          $('#presenting_history_complaint_section').html(data.message);
          tinymce.get('visit_presenting_complaint').setContent('');
          $('#add_presenting_complaint_history').modal('hide');
          //initiate WYSIWYG editor
          tinymce.init({
            selector: ".cleditor",
            height : "150"
          });
          alert("You have successfully added the presenting complaint history");
        }
        else
        {
          alert("Unable to add the presenting complaint history");
        }
      //obj.innerHTML = XMLHttpRequestObject.responseText;
      },
      error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
      }
   
    });
   }

   function save_diagnosis_complaint(visit_id)
   {
    var assessment = tinymce.get('visit_diagnosis').getContent();
    var visit_id = '<?php echo $visit_id;?>';
    var config_url = $('#config_url').val();
    var notes_type_id = 9;
    var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
    
    var notes_date = '<?php echo date('Y-m-d');?>';
    var notes_time = '<?php echo date('H:i');?>';
    $.ajax({
      type:'POST',
      url: data_url,
      data:{notes: assessment, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
      dataType: 'json',
      success:function(data){
        if(data.result == 'success')
        {
          $('#presenting_diagnosis_section').html(data.message);
          tinymce.get('visit_diagnosis').setContent('');
          $('#add_diagnosis').modal('hide');
          //initiate WYSIWYG editor
          tinymce.init({
            selector: ".cleditor",
            height : "150"
          });
          alert("You have successfully added the presenting complaint history");
        }
        else
        {
          alert("Unable to add the presenting complaint history");
        }
      //obj.innerHTML = XMLHttpRequestObject.responseText;
      },
      error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
      }
   
    });
   }


   function save_system_review(visit_id)
   {
    var assessment = tinymce.get('visit_system_review').getContent();
    var visit_id = '<?php echo $visit_id;?>';
    var config_url = $('#config_url').val();
    var notes_type_id = 8;
    var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
    
    var notes_date = '<?php echo date('Y-m-d');?>';
    var notes_time = '<?php echo date('H:i');?>';
    $.ajax({
      type:'POST',
      url: data_url,
      data:{notes: assessment, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
      dataType: 'json',
      success:function(data){
        if(data.result == 'success')
        {
          $('#system_review_section').html(data.message);
          tinymce.get('visit_system_review').setContent('');
          $('#add_system_review').modal('hide');
          //initiate WYSIWYG editor
          tinymce.init({
            selector: ".cleditor",
            height : "150"
          });
          alert("You have successfully added review of the system");
        }
        else
        {
          alert("Unable to add the review of the system");
        }
      //obj.innerHTML = XMLHttpRequestObject.responseText;
      },
      error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
      }
   
    });
   }
   
   function open_window(plan, visit_id){
       var config_url = $('#config_url').val();
     if(plan == 6){
     
       var win = window.open(config_url+"nurse/disease/"+visit_id,"Popup","height=1000,width=600,,scrollbars=yes,"+ 
                           "directories=yes,location=yes,menubar=yes," + 
                            "resizable=no status=no,history=no top = 50 left = 100");
     	win.focus();
     }
     else if (plan == 1){
       
       var win = window.open(config_url+"pharmacy/prescription/"+visit_id,"Popup","height=1200,width=1300,,scrollbars=yes,"+ 
                           "directories=yes,location=yes,menubar=yes," + 
                            "resizable=yes status=yes,history=yes top = 50 left = 100");
     	win.focus();
     }
   }
   
   
   
   function doctor_notes(visit_id){
       var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/doctor_notes/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       var obj = document.getElementById("doctor_notes");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
           //symptoms3(visit_id);
           
         }
       }
           
       XMLHttpRequestObject.send(null);
     }
   }
   
   function nurse_notes(visit_id){
       var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/nurse_notes/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       var obj = document.getElementById("nurse_notes");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
         }
       }
           
       XMLHttpRequestObject.send(null);
     }
   }
   
   function save_symptoms(visit_id)
   {
   	var symptoms = tinymce.get('visit_symptoms').getContent();
   	var visit_id = '<?php echo $visit_id;?>';
   	var config_url = $('#config_url').val();
   	var notes_type_id = 3;
   	var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
   	
   	var notes_date = '<?php echo date('Y-m-d');?>';
   	var notes_time = '<?php echo date('H:i');?>';
   	$.ajax({
   		type:'POST',
   		url: data_url,
   		data:{notes: symptoms, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
   		dataType: 'json',
   		success:function(data){
   			if(data.result == 'success')
   			{
   				$('#symptoms_section').html(data.message);
   				tinymce.get('visit_symptoms').setContent('');
   				//tinyMCE.activeEditor.setContent('');
   				$('#add_symptoms').modal('hide');
   				//initiate WYSIWYG editor
   				tinymce.init({
   					selector: ".cleditor",
   					height : "300"
   				});
   				alert("You have successfully added the symptoms");
   			}
   			else
   			{
   				alert("Unable to add the symptoms");
   			}
   		//obj.innerHTML = XMLHttpRequestObject.responseText;
   		},
   		error: function(xhr, status, error) {
   			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   			alert(error);
   		}
   
   	});
     
   	/*console.debug(tinymce.activeEditor.getContent());
   	//alert(tinymce.activeEditor.getContent());
     var config_url = $('#config_url').val();
     var data_url = config_url+"nurse/save_symptoms/"+visit_id;
     //window.alert(data_url);
     // var symptoms = tinymce.activeEditor.getContent();
      var symptoms = tinymce.get('visit_symptoms').getContent();
      //$('#visit_symptoms').val();//document.getElementById("vital"+vital_id).value;
      //alert(symptoms);
     $.ajax({
     type:'POST',
     url: data_url,
     data:{notes: symptoms},
     dataType: 'text',
     success:function(data){
       window.alert("You have successfully updated the symptoms");
     //obj.innerHTML = XMLHttpRequestObject.responseText;
     },
     error: function(xhr, status, error) {
     //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
     alert(error);
     }
   
     });*/
     
   }
   
   function save_objective_findings(visit_id)
   {
   	var objective_findings = tinymce.get('visit_objective_findings').getContent();
   	var visit_id = '<?php echo $visit_id;?>';
   	var config_url = $('#config_url').val();
   	var notes_type_id = 4;
   	var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
   	
   	var notes_date = '<?php echo date('Y-m-d');?>';
   	var notes_time = '<?php echo date('H:i');?>';
   	$.ajax({
   		type:'POST',
   		url: data_url,
   		data:{notes: objective_findings, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
   		dataType: 'json',
   		success:function(data){
   			if(data.result == 'success')
   			{
   				$('#objective_findings_section').html(data.message);
   				tinymce.get('visit_objective_findings').setContent('');
   				$('#add_objective_findings').modal('hide');
   				//initiate WYSIWYG editor
   				tinymce.init({
   					selector: ".cleditor",
   					height : "300"
   				});
   				alert("You have successfully added the objective findings");
   			}
   			else
   			{
   				alert("Unable to add the objective findings");
   			}
   		//obj.innerHTML = XMLHttpRequestObject.responseText;
   		},
   		error: function(xhr, status, error) {
   			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   			alert(error);
   		}
   
   	});
    
   	/*var config_url = $('#config_url').val();
   	var data_url = config_url+"nurse/save_objective_findings/"+visit_id;
   	//window.alert(data_url);
      var objective_findings = tinymce.get('visit_objective_findings').getContent();
   	//var objective_findings = $('#visit_objective_findings').val();//document.getElementById("vital"+vital_id).value;
   	//alert(objective_findings);
   	$.ajax({
   		type:'POST',
   		url: data_url,
   		data:{notes: objective_findings},
   		dataType: 'text',
   		success:function(data){
   			window.alert("You have successfully updated the objective findings");
   			//obj.innerHTML = XMLHttpRequestObject.responseText;
   		},
   		error: function(xhr, status, error) {
   			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   			alert(error);
   		}
   	});*/
   }
   function save_plan(visit_id)
   {
   	var plan = tinymce.get('visit_plan').getContent();
   	var visit_id = '<?php echo $visit_id;?>';
   	var config_url = $('#config_url').val();
   	var notes_type_id = 6;
   	var data_url = config_url+"nurse/save_notes/"+visit_id+'/'+notes_type_id;
   	
   	var notes_date = '<?php echo date('Y-m-d');?>';
   	var notes_time = '<?php echo date('H:i');?>';
   	$.ajax({
   		type:'POST',
   		url: data_url,
   		data:{notes: plan, date: notes_date, time: notes_time, notes_type_id: notes_type_id},
   		dataType: 'json',
   		success:function(data){
   			if(data.result == 'success')
   			{
   				$('#plan_section').html(data.message);
   				tinymce.get('visit_plan').setContent('');
   				$('#add_plan').modal('hide');
   				//initiate WYSIWYG editor
   				tinymce.init({
   					selector: ".cleditor",
   					height : "300"
   				});
   				alert("You have successfully added the objective findings");
   			}
   			else
   			{
   				alert("Unable to add the objective findings");
   			}
   		//obj.innerHTML = XMLHttpRequestObject.responseText;
   		},
   		error: function(xhr, status, error) {
   			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   			alert(error);
   		}
   
   	});
   }
   
   function get_disease(visit_id){
     
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/get_diagnosis/"+visit_id;
   
         
     if(XMLHttpRequestObject) {
       var obj = document.getElementById("visit_diagnosis_original");
       //var obj2 = document.getElementById("visit_diagnosis");
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
           //obj2.innerHTML = XMLHttpRequestObject.responseText;
         }
       }
       
       XMLHttpRequestObject.send(null);
     }
   }
   
   $(document).on("click","a.delete_diagnosis",function() 
   {
       var diagnosis_id = $(this).attr('href');
       var visit_id = $(this).attr('id');
   	var config_url = $('#config_url').val();
   	var url = config_url+"nurse/delete_diagnosis/"+diagnosis_id;
   	
   	$.get(url, function( data ) {
   		get_disease(visit_id);
   	});
   	
   	return false;
   });
   
   function print_previous_test(visit_id, patient_id){
   	var config_url = $('#config_url').val();
   	var win = window.open(config_url+"laboratory/print_test/"+visit_id+"/"+patient_id,"Popup","height=900,width=1200,,scrollbars=yes,"+
   					"directories=yes,location=yes,menubar=yes," +
   					 "resizable=no status=no,history=no top = 50 left = 100");
     win.focus();
   }
</script>
<script type="text/javascript">
   function parse_lab_test(visit_id)
   {
     var lab_test_id = document.getElementById("lab_test_id").value;
      lab(lab_test_id, visit_id);
     
   }
    function get_lab_table(visit_id){
         var XMLHttpRequestObject = false;
             
         if (window.XMLHttpRequest) {
         
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
             
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id;
     
         if(XMLHttpRequestObject) {
                     
             XMLHttpRequestObject.open("GET", url);
                     
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                 }
             }
             
             XMLHttpRequestObject.send(null);
         }
     }
   
    function lab(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_lab_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function parse_xray(visit_id)
   {
   var xray_id = document.getElementById("xray_id").value;
   xray(xray_id, visit_id);
   
   }
   
   function xray(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_xray_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   
   function get_xray_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function get_ultrasound_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>radiology/ultrasound/test_ultrasound/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("ultrasound_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function get_orthopaedic_surgery_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_orthopaedic_surgery/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("orthopaedic_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function get_opthamology_surgery_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_opthamology_surgery/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("opthamology_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function get_obstetrics_surgery_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_obstetrics_surgery/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("obstetrics_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function get_theatre_procedures_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_theatre_procedures/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("theatre_procedures_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function parse_ultrasound(visit_id)
   {
   var ultrasound_id = document.getElementById("ultrasound_id").value;
   ultrasound(ultrasound_id, visit_id);
   
   }
   function ultrasound(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>radiology/ultrasound/test_ultrasound/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("ultrasound_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_ultrasound_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   
   function parse_orthopaedic_surgery(visit_id)
   {
   var orthopaedic_surgery_id = document.getElementById("orthopaedic_surgery_id").value;
   orthopaedic(orthopaedic_surgery_id, visit_id);
   
   }
   
   function orthopaedic(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_orthopaedic_surgery/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("orthopaedic_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_surgery_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function parse_opthamology_surgery(visit_id)
   {
   var opthamology_surgery_id = document.getElementById("opthamology_surgery_id").value;
   // alert(opthamology_surgery_id);
   opthamology(opthamology_surgery_id, visit_id);
   
   }
   
   function opthamology(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_opthamology_surgery/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("opthamology_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_surgery_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   
   function parse_obstetrics_surgery(visit_id)
   {
   var obstetrics_surgery_id = document.getElementById("obstetrics_surgery_id").value;
   obsterics(obstetrics_surgery_id, visit_id);
   
   }
   
   function obsterics(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_obstetrics_surgery/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("obstetrics_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_surgery_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   
   function parse_theatre_procedures(visit_id)
   {
   var theatre_procedure_id = document.getElementById("theatre_procedure_id").value;
   theatre_procedure(theatre_procedure_id, visit_id);
   
   }
   
   function theatre_procedure(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>theatre/test_theatre_procedures/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("theatre_procedures_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_surgery_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   
   function delete_inpatient_surgery_cost(visit_charge_id, visit_id,surgery_type)
   {
   var res = confirm('Are you sure you want to delete this charge?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = config_url+"theatre/delete_inpatient_cost/"+visit_charge_id+"/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
           
           if(surgery_type == 25)
           {
               // orthopaedic procedures
               get_orthopaedic_surgery_table(visit_id);
                 
   
           }
           else if(surgery_type == 29)
           {
               // opthamology procedures
               get_opthamology_surgery_table(visit_id);
                 
           }
           else if(surgery_type == 30)
           {
               // obstetrics procedures
               get_obstetrics_surgery_table(visit_id);
                 
           }
           else if(surgery_type == 27)
           {
               // theatre procedures
               get_theatre_procedures_table(visit_id);
   
           }
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }
   
   function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }


  function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }
   
   function get_visit_trail(visit_id){
   
   var myTarget2 = document.getElementById("visit_trail"+visit_id);
   var button = document.getElementById("open_visit"+visit_id);
   var button2 = document.getElementById("close_visit"+visit_id);
   
   myTarget2.style.display = '';
   button.style.display = 'none';
   button2.style.display = '';
   }
   function close_visit_trail(visit_id){
   
   var myTarget2 = document.getElementById("visit_trail"+visit_id);
   var button = document.getElementById("open_visit"+visit_id);
   var button2 = document.getElementById("close_visit"+visit_id);
   
   myTarget2.style.display = 'none';
   button.style.display = '';
   button2.style.display = 'none';
   }
   
   function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var url = "<?php echo base_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
   dataType: 'text',
   success:function(data){
   
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }
   
   function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var charge = $('#charge'+prescription_id).val();
   var units_given = $('#units_given'+prescription_id).val();
   
   var url = "<?php echo base_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
   dataType: 'text',
   success:function(data){
     window.alert(data.result);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }
   
   
   function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,0);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }
   
   function delete_ultrasound_cost(visit_charge_id, visit_id)
   {
     var res = confirm('Are you sure you want to delete this charge?');
     
     if(res)
     {
         var XMLHttpRequestObject = false;
         
         if (window.XMLHttpRequest) {
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
         
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var url = config_url+"radiology/ultrasound/delete_cost/"+visit_charge_id+"/"+visit_id;
         
         if(XMLHttpRequestObject) {
             var obj = document.getElementById("ultrasound_table");
             
             XMLHttpRequestObject.open("GET", url);
             
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     obj.innerHTML = XMLHttpRequestObject.responseText;
                     get_ultrasound_table(visit_id);
                 }
             }
             XMLHttpRequestObject.send(null);
         }
     }
   }
    function delete_cost(visit_charge_id, visit_id){
   
       var XMLHttpRequestObject = false;
       
       if (window.XMLHttpRequest) {
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
       
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       }
       var url = config_url+"laboratory/delete_cost/"+visit_charge_id+"/"+visit_id;
       
       if(XMLHttpRequestObject) {
         var obj = document.getElementById("lab_table");
         
         XMLHttpRequestObject.open("GET", url);
         
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             
             obj.innerHTML = XMLHttpRequestObject.responseText;
             //window.location.href = config_url+"data/doctor/laboratory.php?visit_id="+visit_id;
           }
         }
         XMLHttpRequestObject.send(null);
       }
     }
   
   function delete_xray_cost(visit_charge_id, visit_id)
   {
     var res = confirm('Are you sure you want to delete this charge?');
     
     if(res)
     {
         var XMLHttpRequestObject = false;
         
         if (window.XMLHttpRequest) {
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
         
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var url = config_url+"radiology/xray/delete_cost/"+visit_charge_id+"/"+visit_id;
         
         if(XMLHttpRequestObject) {
             var obj = document.getElementById("xray_table");
             
             XMLHttpRequestObject.open("GET", url);
             
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     obj.innerHTML = XMLHttpRequestObject.responseText;
                     get_xray_table(visit_id);
                 }
             }
             XMLHttpRequestObject.send(null);
         }
     }
   }
   
   
   
   function display_inpatient_prescription(visit_id,module){
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }
   function get_drug_to_prescribe(visit_id)
   {
   var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var drug_id = document.getElementById("drug_id").value;
   
     var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
   
      if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
               var prescription_view = document.getElementById("prescription_view");
              
               document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
                prescription_view.style.display = 'block';
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   
   
   }
   
   function pass_diagnosis(visit_id)
   {
   var diseases_id = document.getElementById("diseases_id").value;
   save_disease(diseases_id, visit_id);
   
   }
   
   function save_disease(val, visit_id){
   
   var XMLHttpRequestObject = false;
     
   if (window.XMLHttpRequest) {
   
     XMLHttpRequestObject = new XMLHttpRequest();
   } 
     
   else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   } 
   var config_url = $('#config_url').val();
   var url = config_url+"nurse/save_diagnosis/"+val+"/"+visit_id;
   if(XMLHttpRequestObject) {
         
     XMLHttpRequestObject.open("GET", url);
         
     XMLHttpRequestObject.onreadystatechange = function(){
       
       if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
         get_disease(visit_id);
       }
     }
     
     XMLHttpRequestObject.send(null);
   }
   }
   
   function get_disease(visit_id){
   
   var XMLHttpRequestObject = false;
     
   if (window.XMLHttpRequest) {
   
     XMLHttpRequestObject = new XMLHttpRequest();
   } 
     
   else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   }
    var config_url = $('#config_url').val();
   var url = "<?php echo site_url();?>nurse/get_diagnosis/"+visit_id;
   
   
       
   if(XMLHttpRequestObject) {
       var obj = document.getElementById("patient_diagnosis");
     XMLHttpRequestObject.open("GET", url);
         
     XMLHttpRequestObject.onreadystatechange = function(){
       
       if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
         obj.innerHTML = XMLHttpRequestObject.responseText;
       }
     }
     
     XMLHttpRequestObject.send(null);
   }
   }
  
    function get_visit_plan(visit_id){
    
      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"nurse/load_plan_details/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
          
          var obj = document.getElementById("visit_plan_detail");
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  obj.innerHTML = XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

  function delete_plan_detail(id, visit_id){
    //alert(id);
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
      var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_plan_detail/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_surgeries(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function save_visit_plan_detail(visit_id){
   // var description =  document.getElementById("plan_description_value").value;
    var description = tinymce.get('plan_description_value').getContent();
   var plan_id = document.getElementById("plan_id").value;
   var config_url = document.getElementById("config_url").value;
  var url = config_url+"nurse/plan_description/"+plan_id+"/"+visit_id;   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{description: description, plan_id: plan_id, visit_id: visit_id},
   dataType: 'text',
   success:function(data){
        tinymce.get('plan_description_value').setContent('');
          //tinyMCE.activeEditor.setContent('');
          //initiate WYSIWYG editor
          tinymce.init({
            selector: ".cleditor",
            height : "200"
          });   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   get_visit_plan(visit_id);
   return false;

}
</script>

