<?php

 $data['visit_id'] = $visit_id;
 $data['lab_test'] = 100;
 ?>
<div id="test_results">
  <div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Lab Tests</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='lab_test_id' name='lab_test_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select a Lab test</option>
                      <?php echo $lab_tests;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="parse_lab_test(<?php echo $visit_id;?>);"> Add Lab Test</button>
                  </div>
                </div>
                 <!-- visit Procedures from java script -->
                
                <!-- end of visit procedures -->
            </div>
            <div id="lab_table"></div>
            <?php echo $this->load->view("laboratory/tests/test2", $data, TRUE); ?>
         </section>
    </div>
</div>

</div>



