<?php

$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows2 = count($rs2);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] =  $query_data = $this->nurse_model->get_notes($type, $visit_id);

if($query_data->num_rows() > 0)
{
	foreach ($query_data->result() as $key => $value_two) {
		# code...
		$summary = strip_tags($value_two->notes_name);
		$notes_date = $value_two->notes_date;
	}
	
}
else
{
	$summary = '';
	$notes_date = date('Y-m-d');
}
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}

if($type == 3)
{
	$checked = 'doctor_chief_complain';

}else if($type == 4)
{
	$checked = 'examination_plans';
}
else if($type == 7)
{
	$checked = 'other_plan';
}
else if($type == 5)
{
	$checked = 'assessment_notes';
}
else if($type == 9)
{
	$checked = 'other_diagnosis';
}
else if($type == 21)
{
	$checked = 'nurse_chief_complain';
}
else if($type == 22)
{
	$checked = 'nurse_other_plan';
}
// var_dump($summary); die();
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['module'] = $module;
$notes = $this->load->view('nurse/patients/patient_notes', $v_data, TRUE);
if($module <> 1)
{


?>
<div class='col-md-12'>
	<div class="row">
    	<div class='col-md-12'>
        	<input type="hidden" name="notes_time" id="notes_time<?php echo $visit_id;?>#<?php echo $type;?>" value="<?php echo date('H:i');?>" />
        	<!-- <div class="row"> -->
				<div class="col-md-12" >
					<input data-format="yyyy-MM-dd" data-plugin-datepicker  class="form-control datepicker"  type="hidden" name="date" placeholder="Date" id="notes_date<?php echo $visit_id;?>#<?php echo $type;?>" value="<?php echo $notes_date?>">
					
				</div>
				<br>
			<!-- </div> -->
				


            <textarea class='form-control' id='<?php echo $checked.$visit_id;?>' rows="6" placeholder="Describe" ><?php echo $summary?></textarea>
        </div>
    </div>
    <br>
    <div class="row" >
    	<div class='col-md-12 center-align'>
    		<a class='btn btn-info btn-sm' type='submit' onclick='save_doctors_notes(<?php echo $visit_id;?>,<?php echo $type;?>)'> Save Note</a>
    	</div>
    </div>
    <br>
</div>
<div class='col-md-12'>

	<?php echo $notes?>
</div>
<?php

}
else
{
?>
<div class='col-md-12'>

	<?php echo $notes?>
</div>
<?php

}
?>
