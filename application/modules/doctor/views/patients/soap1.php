<?php
$data['visit_id'] = $visit_id;
?>
<div class="row">
  <div class="col-md-12">
      <div class="panel-body">
         	<div class="col-md-12">
        		<section class="panel panel-featured panel-featured-info">
        			<header class="panel-heading">
        				<h2 class="panel-title">Continuation notes</h2>
        			</header>

        			<div class="panel-body">
                        <!-- vitals from java script -->
                        <?php echo $this->load->view("nurse/soap/doctor_notes", $data, TRUE); ?>
                        <!-- end of vitals data -->
                  </div>
            </section>
            </div>
        </div>
    </div>
</div>