
<div id="loader" style="display: none;"></div>
<input type="hidden" value="<?php echo base_url();?>" id="config_url">
        
<div class="well well-sm info" style="margin-top: 20px;">
    <h5 style="margin:0;">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-lg-6">
                        <strong>Patient Name:</strong>
                    </div>
                    <div class="col-lg-6">
                        <?php echo $patient_surname;?>  <?php echo $patient_othernames;?>
                    </div>
                </div>
            </div>         
            
            
            <div class="col-md-2">
                <div class="row">
                    <div class="col-lg-6">
                        <strong>Gender:</strong>
                    </div>
                    <div class="col-lg-6">
                        <?php echo $gender;?>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="row">
                    <div class="col-lg-6">
                        <strong>Age:</strong>
                    </div>
                    <div class="col-lg-6">
                        <?php echo $age;?>
                    </div>
                </div>
            </div>
            
        </div>
    </h5>
</div>

  <div class="center-align">
    <?php
        $error = $this->session->userdata('error_message');
        $validation_error = validation_errors();
        $success = $this->session->userdata('success_message');
        
        if(!empty($error))
        {
            echo '<div class="alert alert-danger">'.$error.'</div>';
            $this->session->unset_userdata('error_message');
        }
        
        if(!empty($validation_error))
        {
            echo '<div class="alert alert-danger">'.$validation_error.'</div>';
        }
        
        if(!empty($success))
        {
            echo '<div class="alert alert-success">'.$success.'</div>';
            $this->session->unset_userdata('success_message');
        }
    ?>
 <div class="clearfix"></div>
  </div>


<div class="clearfix"></div>

<div class="tabbable" style="margin-bottom: 18px;">
<ul class="nav nav-tabs nav-justified">
<li class="active"><a href="#about-pane" data-toggle="tab">Patient details</a></li>
<li><a href="#vitals-pane" data-toggle="tab" onclick="get_patient_vitals(<?php echo $visit_id?>)">Vitals</a></li>
  <li><a href="#soap1" data-toggle="tab">Continuation Notes</a></li>
   <li><a href="#lifestyle" data-toggle="tab">Nurse Notes</a></li>
   <li><a href="#treatment-sheet" data-toggle="tab"  onclick="get_patient_treatment_sheet(<?php echo $visit_id?>)">Treatment Sheet</a></li>
    <li><a href="#investigations" data-toggle="tab" onclick="get_patient_investigation(<?php echo $visit_id?>)">Investigations</a></li>
    <li><a href="#plan-visit" data-toggle="tab" onclick="get_patient_plan(<?php echo $visit_id?>)">Plan</a></li>
    <li><a href="#previous-vitals" data-toggle="tab" onclick="get_patient_billing(<?php echo $visit_id?>)">Billing</a></li>
    <li><a href="#discharge-summary" data-toggle="tab">Discharge Summary</a></li>
<!--<li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li>-->
</ul>
<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
<!-- <div class="tab-pane active" id="about-pane">-->
<div class="tab-pane active" id="about-pane">
    <?php echo $this->load->view("doctor/inpatient/about", '', TRUE);?>
</div>

<div class="tab-pane" id="vitals-pane">
    <div id="patient-vitals-view"></div>
	  <?php //echo $this->load->view("doctor/patients/vitals", '', TRUE);?>
</div>
<div class="tab-pane" id="investigations">
    <div id="patient-investigation-view"></div>
</div>
<div class="tab-pane" id="treatment-sheet">
    <!-- <div id="patient-investigation-view"></div> -->
    <?php echo $this->load->view("doctor/patients/treatment_sheet", '', TRUE);?>  
</div>  
<div class="tab-pane" id="plan-visit">
    <div id="patient-plan-view"></div>
<?php //echo $this->load->view("doctor/patients1/plan_visit", '', TRUE);?>
</div>             

<div class="tab-pane" id="soap1">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <?php echo $this->load->view("doctor/patients/admission_notes", '', TRUE);?>
            </div>
            <div class="col-md-6">
                <?php echo $this->load->view("doctor/patients/admission_diagnosis", '', TRUE);?>
            </div>
        </div>
        
    </div>
    
  <?php echo $this->load->view("doctor/patients/soap1", '', TRUE);?>                  
</div>
 <div class="tab-pane" id="lifestyle">
    <?php echo $this->load->view("doctor/patients1/nurse_notes", '', TRUE);?>
</div>


<div class="tab-pane" id="previous-vitals">
    <div id="patient-billing-view"></div>
</div>
<div class="tab-pane" id="discharge-summary">
    <?php echo $this->load->view("doctor/patients/discharge_summary", '', TRUE);?>
</div>


</div>
</div>

<script text="javascript">

var config_url = document.getElementById("config_url").value;
$(document).ready(function(){

  // vitals_interface(<?php echo $visit_id;?>);
   
  // display_treatment_prescription(<?php echo $visit_id;?>,1);
  // display_todays_tsheet(<?php echo $visit_id;?>,1);

});

 function vitals_interface(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var url = config_url+"nurse/vitals_interface/"+visit_id;

            
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("vitals");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                obj.innerHTML = XMLHttpRequestObject.responseText;
                
                var count;
                for(count = 1; count < 12; count++){
                    load_vitals(count, visit_id);
                }
                display_bed_charges(visit_id);
                display_consultation_charges(visit_id);
                previous_vitals(visit_id);
                // get_family_history(visit_id);
                // nurse_notes(visit_id);
                 get_xray_table(visit_id);
                 get_ultrasound_table(visit_id);


                 // suregies
                get_orthopaedic_surgery_table(visit_id);
                get_opthamology_surgery_table(visit_id);
                get_obstetrics_surgery_table(visit_id);
                get_theatre_procedures_table(visit_id);

                display_procedure(visit_id);
                //get_medication(visit_id);
                get_lab_table(visit_id);
                //get_vaccines(visit_id);
                display_vaccines(visit_id);
                display_visit_vaccines(visit_id);
                display_visit_consumables(visit_id);
                display_inpatient_prescription(visit_id,0);

                // alert(visit_id);

            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}


function load_vitals(vitals_id, visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_vitals/"+vitals_id+"/"+visit_id;//window.alert(url);
  
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("vital"+vitals_id);
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
                
                if((vitals_id == 8) || (vitals_id == 9)){
                    calculate_bmi(visit_id);
                }
                
                if((vitals_id == 3) || (vitals_id == 4)){
                    calculate_hwr(vitals_id, visit_id);
                }
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function previous_vitals(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/previous_vitals/"+visit_id;//window.alert(url);

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("previous_vitals");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function getXMLHTTP() {
     //fuction to return the xml http object
        var xmlhttp=false;  
        try{
            xmlhttp=new XMLHttpRequest();
        }
        catch(e)    {       
            try{            
                xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch(e){
                try{
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch(e1){
                    xmlhttp=false;
                }
            }
        }
            
        return xmlhttp;
    }
    
    function save_vital(visit_id)
    {
        var config_url = document.getElementById("config_url").value;
        var data_url = config_url+"nurse/save_vitals/"+visit_id;
       
        var vital5_systolic = $('#vital5').val();
        var vital6_diastolic = $('#vital6').val();
        var vital8_weight = $('#vital8').val();
        var vital9_height = $('#vital9').val();
        var vital4_hip = $('#vital4').val();
        var vital3_waist = $('#vital3').val();
        var vital1_temperature = $('#vital1').val();
        var vital7_pulse = $('#vital7').val();
        var vital2_respiration = $('#vital2').val();
        var vital11_oxygen = $('#vital11').val();
        var vital10_pain = $('#vital10').val();
         
        $.ajax({
        type:'POST',
        url: data_url,
        data:{systolic: vital5_systolic, diastolic : vital6_diastolic, weight: vital8_weight, height : vital9_height,hip : vital4_hip,waist : vital3_waist, temperature : vital1_temperature,pulse : vital7_pulse,respiration: vital2_respiration,oxygen: vital11_oxygen, pain: vital10_pain},
        dataType: 'text',
        success:function(data)
        {
            //calculate_bmi(visit_id);
         //get_medication(visit_id);
         alert("You have successfully entered the vitals");
          previous_vitals(visit_id);
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
        
    }

    function calculate_bmi(visit_id){
    
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"nurse/calculate_bmi/"+visit_id;

        if(XMLHttpRequestObject) {
            
            var obj = document.getElementById("bmi_out");
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    //obj.innerHTML = XMLHttpRequestObject.responseText;
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }

function calculate_hwr(vitals_id, visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/calculate_hwr/"+vitals_id+"/"+visit_id;//window.alert(url);

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("hwr_out");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                //obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function display_procedure(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"doctor/inpatient/view_procedure/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function display_bed_charges(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/view_bed_charges/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("bed_charges").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function display_consultation_charges(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/view_consultation_charges/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("consultation_charges").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function myPopup3(visit_id) {
    var config_url = document.getElementById("config_url").value;
    var win = window.open( config_url+"nurse/procedures/"+visit_id, "myWindow", "status = 1, height = auto, width = 600, resizable = 0" );
    win.focus();
}

function calculatevaccinetotal(amount, id, procedure_id, v_id)
{
    var units = document.getElementById('units'+id).value;  

    grand_vaccine_total(id, units, amount, v_id);
}
function calculateconsumabletotal(amount, id, procedure_id, v_id)
{
    var units = document.getElementById('units'+id).value;  

    grand_consumable_total(id, units, amount, v_id);
}

function display_visit_vaccines(visit_id)
{
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/visit_vaccines/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        var obj = document.getElementById("vaccines_to_patients");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function display_visit_consumables(visit_id)
{
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/visit_consumables/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        var obj = document.getElementById("consumables_to_patients");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function grand_vaccine_total(vaccine_id, units, amount, v_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/vaccine_total/"+vaccine_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                display_visit_vaccines(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function grand_consumable_total(vaccine_id, units, amount, v_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/consuamble_total/"+vaccine_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                display_visit_consumables(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

//Calculate procedure total
function calculatetotal(amount, id, procedure_id, v_id){
       
    var units = document.getElementById('units'+id).value;  

    grand_total(id, units, amount, v_id);
}

//Calculate bed total
function calculatebedtotal(amount, visit_charge_id, service_charge_id, v_id){
       
    var units = document.getElementById('bed_charge_units'+visit_charge_id).value;  

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/bed_total/"+visit_charge_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                display_bed_charges(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function delete_bed_charge(visit_charge_id, v_id)
{
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/delete_bed/"+visit_charge_id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                display_bed_charges(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

//Calculate consultation total
function calculateconsultationtotal(amount, visit_charge_id, service_charge_id, v_id){
       
    var units = document.getElementById('consultation_charge_units'+visit_charge_id).value;  

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/bed_total/"+visit_charge_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                display_consultation_charges(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function grand_total(procedure_id, units, amount, v_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/procedure_total/"+procedure_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                display_procedure(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_procedure(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_procedure/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_procedure(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_bed(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_bed/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_bed_charges(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_consultation(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_bed/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_bed_charges(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_vaccine(id, visit_id){
    
    var confirmation = confirm('Do you really want to delete this vaccine ?');
    
    if(confirmation)
    {
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var config_url = document.getElementById("config_url").value;
        var url = config_url+"nurse/delete_vaccine/"+id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
    
                    display_visit_vaccines(visit_id);
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }
}
function delete_consumable(id, visit_id){
    
    var confirmation = confirm('Delete consumable?');
    
    if(confirmation)
    {
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"nurse/delete_consumable/"+id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
    
                    display_visit_consumables(visit_id);
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }
}




// start of vaccine

function display_vaccines(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/visit_vaccines/"+visit_id;
  
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("vaccines_to_patients").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function myPopup4(visit_id) {
    var config_url = document.getElementById("config_url").value;
    var win = window.open( config_url+"nurse/vaccines_list/"+visit_id, "myWindow", "status = 1, height = auto, width = 600, resizable = 0" );
    win.focus();
}
function myPopup5(visit_id) {
    var config_url = document.getElementById("config_url").value;
    var win = window.open( config_url+"nurse/consumables_list/"+visit_id, "myWindow", "status = 1, height = auto, width = 600, resizable = 0" );
    win.focus();
}
// end of vaccine
function save_medication(visit_id){
    var config_url = document.getElementById("config_url").value;
    var data_url = config_url+"nurse/medication/"+visit_id;
   
     var patient_medication = $('#medication_description').val();
     var patient_medicine_allergies = $('#medicine_allergies').val();
     var patient_food_allergies = $('#food_allergies').val();
     var patient_regular_treatment = $('#regular_treatment').val();
     
    $.ajax({
    type:'POST',
    url: data_url,
    data:{medication: patient_medication,medicine_allergies: patient_medicine_allergies, food_allergies: patient_food_allergies, regular_treatment: patient_regular_treatment },
    dataType: 'text',
    success:function(data){
     get_medication(visit_id);
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

       
}

function get_medication(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_medication/"+visit_id;

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("medication");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function get_surgeries(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_surgeries/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("surgeries");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function save_surgery(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var date = document.getElementById("datepicker").value;
    var description = document.getElementById("surgery_description").value;
    var month = document.getElementById("month").value;
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/surgeries/"+date+"/"+description+"/"+month+"/"+visit_id;
   
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_surgeries(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_surgery(id, visit_id){
    //alert(id);
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
      var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_surgeries/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_surgeries(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function get_vaccines(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_vaccine/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("patient_vaccine");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function save_vaccine(vaccine_id, value, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/";
    
    if(value == 1){
        var yes =  document.getElementById("yes"+vaccine_id);
        
        if(yes.checked == true){
        
            url = url + "vaccine/"+vaccine_id+"/1" ;
            
        }
        else if(yes.checked == false){
        
            url = url + "vaccine/"+vaccine_id+"/2";
            
        }
    }
    
    else if(value == 0){
        var no =  document.getElementById("no"+vaccine_id);
        
        if(no.checked == false){
            url = url + "vaccine/"+vaccine_id+"/1";
            
        }
        else if(no.checked == true){
        
            url = url + "vaccine/"+vaccine_id+"/2";
            
        }
    }
    url = url + "/"+visit_id;
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                
                get_vaccines(visit_id);
            }
        }
        
        XMLHttpRequestObject.send(null);
    }
    
}

</script>


<!-- soap items -->
<script type="text/javascript">
    function get_lab_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id;
    
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    function get_xray_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
     function get_ultrasound_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>radiology/ultrasound/test_ultrasound/"+visit_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("ultrasound_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    function get_orthopaedic_surgery_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>theatre/test_orthopaedic_surgery/"+visit_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("orthopaedic_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    function get_opthamology_surgery_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>theatre/test_opthamology_surgery/"+visit_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("opthamology_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    function get_obstetrics_surgery_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>theatre/test_obstetrics_surgery/"+visit_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("obstetrics_surgery_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    function get_theatre_procedures_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>theatre/test_theatre_procedures/"+visit_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("theatre_procedures_table").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    // laboratory
    function delete_cost(visit_charge_id, visit_id){
  
      var XMLHttpRequestObject = false;
      
      if (window.XMLHttpRequest) {
        XMLHttpRequestObject = new XMLHttpRequest();
      } 
      
      else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var url = config_url+"laboratory/delete_cost/"+visit_charge_id+"/"+visit_id;
      
      if(XMLHttpRequestObject) {
        var obj = document.getElementById("lab_table");
        
        XMLHttpRequestObject.open("GET", url);
        
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            
            obj.innerHTML = XMLHttpRequestObject.responseText;
            //window.location.href = config_url+"data/doctor/laboratory.php?visit_id="+visit_id;
          }
        }
        XMLHttpRequestObject.send(null);
      }
    }
    function delete_procedure(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_procedure/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_procedure(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_ultrasound_cost(visit_charge_id, visit_id)
{
    var res = confirm('Are you sure you want to delete this charge?');
    
    if(res)
    {
        var XMLHttpRequestObject = false;
        
        if (window.XMLHttpRequest) {
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
        
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = config_url+"radiology/ultrasound/delete_cost/"+visit_charge_id+"/"+visit_id;
        
        if(XMLHttpRequestObject) {
            var obj = document.getElementById("ultrasound_table");
            
            XMLHttpRequestObject.open("GET", url);
            
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    obj.innerHTML = XMLHttpRequestObject.responseText;
                    get_ultrasound_table(visit_id);
                }
            }
            XMLHttpRequestObject.send(null);
        }
    }
}

function delete_xray_cost(visit_charge_id, visit_id)
{
    var res = confirm('Are you sure you want to delete this charge?');
    
    if(res)
    {
        var XMLHttpRequestObject = false;
        
        if (window.XMLHttpRequest) {
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
        
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = config_url+"radiology/xray/delete_cost/"+visit_charge_id+"/"+visit_id;
        
        if(XMLHttpRequestObject) {
            var obj = document.getElementById("xray_table");
            
            XMLHttpRequestObject.open("GET", url);
            
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    obj.innerHTML = XMLHttpRequestObject.responseText;
                    get_xray_table(visit_id);
                }
            }
            XMLHttpRequestObject.send(null);
        }
    }
}

function display_inpatient_prescription(visit_id,module){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;

            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function save_symptoms(visit_id){
  

  var config_url = $('#config_url').val();
  var data_url = "<?php echo site_url();?>nurse/save_symptoms/"+visit_id;
  //window.alert(data_url);
   var symptoms = document.getElementById("visit_symptoms").value; //$('#visit_symptoms').val();
   window.alert(symptoms);
  $.ajax({
  type:'POST',
  url: data_url,
  data:{notes: symptoms},
  dataType: 'text',
  success:function(data){
    window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}


function save_discharge_notes(visit_id){
  

  var config_url = $('#config_url').val();
  var data_url = "<?php echo site_url();?>nurse/save_discharge_notes/"+visit_id;
  //window.alert(data_url);
   //var symptoms = document.getElementById("discharge_note"+visit_id).value; //$('#visit_symptoms').val();
    var discharge_date = document.getElementById("discharge_date"+visit_id).value;
    var admission_date = document.getElementById("admission_date"+visit_id).value;
    var symptoms = tinymce.get("discharge_note"+visit_id).getContent();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{notes: symptoms,admission_date :admission_date,discharge_date :discharge_date},
  dataType: 'json',
  success:function(data){
     // window.alert(symptoms);
    if(data.result == 'success')
    {
      $('#discharge_summary_notes').html(data.message);
      // tinymce.get('visit_presenting_complaint').setContent('');
     
      alert("You have successfully saved the discharge summary");
    }
    else
    {
      alert("Unable to add the discharge summary");
    }
  //obj.innerHTML = XMLHttpRequestObject.responseText;
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

</script>
<script type="text/javascript">
    function get_patient_investigation(visit_id)
    {
         // document.getElementById("loader-circle").style.display = "block";
         document.getElementById("loader").style.display = "block";
         var XMLHttpRequestObject = false;       
         if (window.XMLHttpRequest) {
         
           XMLHttpRequestObject = new XMLHttpRequest();
         } 
           
         else if (window.ActiveXObject) {
           XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"nurse/patient_investigation_view/"+visit_id;
         if(XMLHttpRequestObject) {
           var obj = document.getElementById("patient-investigation-view");
           XMLHttpRequestObject.open("GET", url);
               
           XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

               obj.innerHTML = XMLHttpRequestObject.responseText;
               //obj2.innerHTML = XMLHttpRequestObject.responseText;
               $("#lab_test_id").customselect();
               $("#xray_id").customselect();
               get_lab_table(visit_id);
               get_xray_table(visit_id);
               get_xray_scans(visit_id);
               document.getElementById("loader").style.display = "none";
             }
           }
           
           XMLHttpRequestObject.send(null);
         }
          // document.getElementById("loader").style.display = "none";
    }
    function get_xray_scans(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>radiology/xray/test_scans/"+visit_id;
        // alert(url);
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("xray_scans").innerHTML = XMLHttpRequestObject.responseText;
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
    function get_patient_plan(visit_id)
    {
        

        document.getElementById("loader").style.display = "block";
         var XMLHttpRequestObject = false;       
         if (window.XMLHttpRequest) {
         
           XMLHttpRequestObject = new XMLHttpRequest();
         } 
           
         else if (window.ActiveXObject) {
           XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"nurse/patient_plan_view/"+visit_id;
         // alert(url);
         if(XMLHttpRequestObject) {
           var obj = document.getElementById("patient-plan-view");
           XMLHttpRequestObject.open("GET", url);
               
           XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

               obj.innerHTML = XMLHttpRequestObject.responseText;
               //obj2.innerHTML = XMLHttpRequestObject.responseText;
               $("#drug_id").customselect();
               $("#diseases_id").customselect();
               get_disease(visit_id);
               display_prescription(visit_id,0);
               display_inpatient_prescription(visit_id,0);
                tinymce.init({
                    selector: ".cleditor",
                    height : "100"
                });

               document.getElementById("loader").style.display = "none";
               // get_lab_table(visit_id);

             }
           }
           
           XMLHttpRequestObject.send(null);
         }

    }
    function display_prescription(visit_id, page){
     
     var XMLHttpRequestObject = false;
       
     if (window.XMLHttpRequest) {
     
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
       
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
      var config_url = $('#config_url').val();
     var url = config_url+"pharmacy/display_prescription/"+visit_id;
     
     if(page == 1){
       var obj = window.opener.document.getElementById("prescription");
     }
     
     else{
       var obj = document.getElementById("visit_prescription");
     }
     if(XMLHttpRequestObject) {
           
       XMLHttpRequestObject.open("GET", url);
           
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           obj.innerHTML = XMLHttpRequestObject.responseText;
           
           if(page == 1){
             window.close(this);
           
           }
           //plan(visit_id);
         }
       }
       
       XMLHttpRequestObject.send(null);
     }
   }

   function get_drug_to_prescribe(visit_id)
   {
   var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var drug_id = document.getElementById("drug_id").value;
   
     var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
   
      if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
               var prescription_view = document.getElementById("prescription_view");
              
               document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
                prescription_view.style.display = 'block';
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   
   
   }
   
   function pass_diagnosis(visit_id)
   {
   var diseases_id = document.getElementById("diseases_id").value;
   save_disease(diseases_id, visit_id);
   
   }
   
   function save_disease(val, visit_id){
   
   var XMLHttpRequestObject = false;
     
   if (window.XMLHttpRequest) {
   
     XMLHttpRequestObject = new XMLHttpRequest();
   } 
     
   else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   } 
   var config_url = $('#config_url').val();
   var url = config_url+"nurse/save_diagnosis/"+val+"/"+visit_id;
   if(XMLHttpRequestObject) {
         
     XMLHttpRequestObject.open("GET", url);
         
     XMLHttpRequestObject.onreadystatechange = function(){
       
       if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
         get_disease(visit_id);
       }
     }
     
     XMLHttpRequestObject.send(null);
   }
   }
   
   function get_disease(visit_id){
   
   var XMLHttpRequestObject = false;
     
   if (window.XMLHttpRequest) {
   
     XMLHttpRequestObject = new XMLHttpRequest();
   } 
     
   else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   }
    var config_url = $('#config_url').val();
   var url = "<?php echo site_url();?>nurse/get_diagnosis/"+visit_id;
   
   
       
   if(XMLHttpRequestObject) {
       var obj = document.getElementById("patient_diagnosis");
     XMLHttpRequestObject.open("GET", url);
         
     XMLHttpRequestObject.onreadystatechange = function(){
       
       if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
         obj.innerHTML = XMLHttpRequestObject.responseText;
       }
     }
     
     XMLHttpRequestObject.send(null);
   }
   }


   function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }


  function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }
   
   function get_visit_trail(visit_id){
   
   var myTarget2 = document.getElementById("visit_trail"+visit_id);
   var button = document.getElementById("open_visit"+visit_id);
   var button2 = document.getElementById("close_visit"+visit_id);
   
   myTarget2.style.display = '';
   button.style.display = 'none';
   button2.style.display = '';
   }
   function close_visit_trail(visit_id){
   
   var myTarget2 = document.getElementById("visit_trail"+visit_id);
   var button = document.getElementById("open_visit"+visit_id);
   var button2 = document.getElementById("close_visit"+visit_id);
   
   myTarget2.style.display = 'none';
   button.style.display = '';
   button2.style.display = 'none';
   }
   
   function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var url = "<?php echo base_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
   dataType: 'text',
   success:function(data){
   
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }
   
   function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var charge = $('#charge'+prescription_id).val();
   var units_given = $('#units_given'+prescription_id).val();
   
   var url = "<?php echo base_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
   dataType: 'text',
   success:function(data){
     window.alert(data.result);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }
   
   
   function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,0);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }
  

</script>

<script type="text/javascript">
    function get_patient_vitals(visit_id)
    {
         // document.getElementById("loader-circle").style.display = "block";
         document.getElementById("loader").style.display = "block";
         var XMLHttpRequestObject = false;       
         if (window.XMLHttpRequest) {
         
           XMLHttpRequestObject = new XMLHttpRequest();
         } 
           
         else if (window.ActiveXObject) {
           XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"nurse/patient_vitals_view/"+visit_id;
         if(XMLHttpRequestObject) {
           var obj = document.getElementById("patient-vitals-view");
           XMLHttpRequestObject.open("GET", url);
               
           XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

               obj.innerHTML = XMLHttpRequestObject.responseText;
               //obj2.innerHTML = XMLHttpRequestObject.responseText;
               $("#procedure_id").customselect();
               vitals_interface(visit_id);
               document.getElementById("loader").style.display = "none";
             }
           }
           
           XMLHttpRequestObject.send(null);
         }
          // document.getElementById("loader").style.display = "none";
    }
</script>
<!-- patient billing  -->
<script type="text/javascript">
    
    function get_patient_billing(visit_id)
    {
         // document.getElementById("loader-circle").style.display = "block";
         document.getElementById("loader").style.display = "block";
         var XMLHttpRequestObject = false;       
         if (window.XMLHttpRequest) {
         
           XMLHttpRequestObject = new XMLHttpRequest();
         } 
           
         else if (window.ActiveXObject) {
           XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"accounts/get_patient_bill_view/"+visit_id;

         // alert(url);
         if(XMLHttpRequestObject) {
           var obj = document.getElementById("patient-billing-view");
           XMLHttpRequestObject.open("GET", url);
               
           XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

               obj.innerHTML = XMLHttpRequestObject.responseText;
               //obj2.innerHTML = XMLHttpRequestObject.responseText;
               // $("#procedure_id").customselect();
               // vitals_interface(visit_id);

               $("#service_id_item").customselect();
               $("#provider_id_item").customselect();
               $("#parent_service_id").customselect();
               get_visit_detail(visit_id);
               document.getElementById("loader").style.display = "none";
             }
           }
           
           XMLHttpRequestObject.send(null);
         }
          // document.getElementById("loader").style.display = "none";
    }
</script>


<script type="text/javascript">
    function get_visit_detail(visit_id)
    {
        // alert(visit_id);
        
        document.getElementById("visit_id_checked").value = visit_id;
        document.getElementById("visit_id_payments").value = visit_id;
        document.getElementById("visit_id_visit").value = visit_id;
        document.getElementById("visit_discharge_visit").value = visit_id;

        display_patient_bill(visit_id);
    }
    function get_next_page(page,visit_id)
    {
        // alert()
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_visits_div/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visits_div").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }

    function get_next_invoice_page(page,visit_id)
    {
        // alert(page);
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }

    function get_next_payments_page(page,visit_id)
    {
        // alert(page);
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_receipt/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }


    function get_page_header(visit_id)
    {
        // alert()
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_details_bill_header/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("page_header").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }
</script>

<script type="text/javascript">



     
  function getservices(id){

        var type_payment =  $("input[name='type_payment']:checked").val();

        // var myTarget1 = document.getElementById("service_div");
        var myTarget5 = document.getElementById("normal_div");
        var myTarget6 = document.getElementById("waiver_div");
        // alert(id);
        if(type_payment == 1)
        {
          myTarget6.style.display = 'none';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget6.style.display = 'block';
          myTarget5.style.display = 'none';
        }
        
  }



  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget4 = document.getElementById("debit_card_div");

    var myTarget5 = document.getElementById("bank_deposit_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 7)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'block';
    }
    else if(payment_type_id == 8)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'block';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 6)
    {
       myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';  
    }

  }

   function display_patient_bill(visit_id){

    // alert(visit_id);
      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/patient_bill_view/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;

                 get_services_offered(visit_id);
                 get_patient_receipt(visit_id);
                 get_page_header(visit_id);
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

      
  }
  function get_services_offered(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_services_billed/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("billed_services").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      get_page_header(visit_id);
  }

  function get_patient_receipt(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_receipt/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      get_page_header(visit_id);
  }
  function get_all_visits_div(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_visits_div/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visits_div").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

    //Calculate procedure total
    function calculatetotal(amount, id, procedure_id, v_id){
           
        var units = document.getElementById('units'+id).value;  
        var billed_amount = document.getElementById('billed_amount'+id).value;  

        grand_total(id, units, billed_amount, v_id);
    }
    function grand_total(procedure_id, units, amount, v_id){
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
    
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_id: v_id},
        dataType: 'json',
        success:function(data){
            alert(data.message);
            display_patient_bill(v_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(v_id);
        }
        });
        return false;

        
       
    }
    function delete_service(id, visit_id){

        var res = confirm('Are you sure you want to delete this charge?');
        
        if(res)
        {

            var config_url = document.getElementById("config_url").value;
            var url = config_url+"accounts/delete_service_billed/"+id+"/"+visit_id;
        
            $.ajax({
            type:'POST',
            url: url,
            data:{visit_id: visit_id,id: id},
            dataType: 'json',
            success:function(data){
                alert(data.message);
                display_patient_bill(visit_id);
                // get_all_visits_div(patient_id);
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                display_patient_bill(visit_id);
            }
            });
            return false;
            var XMLHttpRequestObject = false;
                
            if (window.XMLHttpRequest) {
            
                XMLHttpRequestObject = new XMLHttpRequest();
            } 
                
            else if (window.ActiveXObject) {
                XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            }
             var config_url = document.getElementById("config_url").value;
            var url = config_url+"accounts/delete_service_billed/"+id;
            
            if(XMLHttpRequestObject) {
                        
                XMLHttpRequestObject.open("GET", url);
                        
                XMLHttpRequestObject.onreadystatechange = function(){
                    
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                        display_patient_bill(visit_id);
                    }
                }
                        
                XMLHttpRequestObject.send(null);
            }
        }
    }
    function save_service_items(visit_id)
    {
        var provider_id = $('#provider_id'+visit_id).val();
        var service_id = $('#service_id'+visit_id).val();
        var visit_date = $('#visit_date_date'+visit_id).val();
        var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
        dataType: 'text',
        success:function(data){
            alert("You have successfully billed");
            display_patient_bill(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    }

    $(document).on("submit","form#add_bill",function(e)
    {
        e.preventDefault(); 

        var service_id = $('#service_id_item').val();
        var patient_id = $('#patient_id_item').val();
        var provider_id = $('#provider_id_item').val();
        var visit_date = $('#visit_date_date').val();
        var visit_id = $('#visit_id_checked').val();
        var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
        dataType: 'json',
        success:function(data){

            // alert(data.message);
            $('#add_to_bill').modal('toggle');

            display_patient_bill(visit_id);
            get_all_visits_div(patient_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });

    $(document).on("submit","form#visit_type_change",function(e)
    {
        e.preventDefault(); 

        var visit_type_id = $('#visit_type_id').val();
        var visit_id = $('#visit_id_visit').val();
        var url = "<?php echo base_url();?>accounts/change_patient_visit/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_type_id: visit_type_id},
        dataType: 'text',
        success:function(data){
            alert("You have successfully changed patient type");
            $('#change_patient_type').modal('toggle');
            display_patient_bill(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });

    $(document).on("submit","form#discharge-patient",function(e)
    {
        e.preventDefault(); 

        var visit_date_charged = $('#visit_date_charged').val();
        var visit_id = $('#visit_discharge_visit').val();
        var url = "<?php echo base_url();?>accounts/discharge_patient/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_date_charged: visit_date_charged},
        dataType: 'json',
        success:function(data){
            alert(data.message);
            $('#end_visit_date').modal('toggle');
            display_patient_bill(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });

    $(document).on("submit","form#payments-paid-form",function(e)
    {
        // alert("changed");
        e.preventDefault(); 

        var cancel_action_id = $('#cancel_action_id').val();
        var cancel_description = $('#cancel_description').val();
        var visit_id = $('#visit_id').val();
        var payment_id = $('#payment_id').val();
        var url = "<?php echo base_url();?>accounts/cancel_payment/"+payment_id+"/"+visit_id;       
        $.ajax({
        type:'POST',
        url: url,
        data:{cancel_description: cancel_description, cancel_action_id: cancel_action_id},
        dataType: 'text',
        success:function(data){
            alert("You have successfully cancelled a payment");
            $('#refund_payment'+visit_id).modal('toggle');
            get_page_header(visit_id);
            get_patient_receipt(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            get_page_header(visit_id);
            get_patient_receipt(visit_id);
        }
        });
        return false;
    });


    $(document).on("submit","form#add_payment",function(e)
    {
        e.preventDefault(); 

        var payment_method = $('#payment_method').val();
        var amount_paid = $('#amount_paid').val();
        var type_payment =  $("input[name='type_payment']:checked").val(); //$('#type_payment').val();
        // alert(amount_paid); die();
        var service_id = $('#service_id').val();
        var waiver_amount = $('#waiver_amount').val();
        var waiver_service_id = $('#waiver_service_id').val();      
        var cheque_number = $('#cheque_number').val();
        var insuarance_number = $('#insuarance_number').val();
        var mpesa_code = $('#mpesa_code').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var change_payment = $('#change_payment').val();

        var debit_card_detail = $('#debit_card_detail').val();
        var deposit_detail = $('#deposit_detail').val();
        var password = $('#password').val();


        var visit_id = $('#visit_id_payments').val();

        var payment_service_id = $('#payment_service_id').val();

    
        var url = "<?php echo base_url();?>accounts/make_payments/"+visit_id;
        // alert(type_payment);
        $.ajax({
        type:'POST',
        url: url,
        data:{payment_method: payment_method, amount_paid: amount_paid, type_payment: type_payment,service_id: service_id, cheque_number: cheque_number, insuarance_number: insuarance_number, mpesa_code: mpesa_code,username: username,password: password, payment_service_id: payment_service_id,debit_card_detail: debit_card_detail,deposit_detail: deposit_detail,change_payment:change_payment,waiver_amount: waiver_amount, waiver_service_id},
        dataType: 'json',
        success:function(data){

            if(data.result == 'success')
            {
                alert(data.message);

                $('#add_payment_modal').modal('toggle');
                get_page_header(visit_id);
                get_patient_receipt(visit_id,null);
                 
            }
            else
            {
                alert(data.message);
            }
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });
    function close_visit(visit_id)
    {
        var res = confirm('Are you sure you want send to accounts ?');
     
        if(res)
        {
            var url = "<?php echo base_url();?>accounts/close_visit/"+visit_id;
        
            $.ajax({
            type:'POST',
            url: url,
            data:{visit_id: visit_id},
            dataType: 'json',
            success:function(data){
                alert(data.message);
                // setTimeout(function() {
                //  send_message(visit_id);
                //   }, 2000);
                // display_patient_bill(visit_id);
                window.location.href = '<?php echo base_url();?>queues/outpatient-queue';
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                display_patient_bill(visit_id);
            }
            });
            return false;

        }
    }
    function send_message(visit_id)
    {
        var url = "<?php echo base_url();?>accounts/send_message/"+visit_id;
        
            $.ajax({
            type:'POST',
            url: url,
            data:{visit_id: visit_id},
            dataType: 'json',
            success:function(data){
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                
            }
            });
            return false;
    }
    function get_change()
    {

        var visit_id = $('#visit_id_payments').val();
    
        var amount_paid = $('#amount_paid').val();
        var url = "<?php echo base_url();?>accounts/get_change/"+visit_id;
    
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_id: visit_id, amount_paid: amount_paid},
        dataType: 'json',
        success:function(data){
            var change = data.change;

            document.getElementById("change_payment").value = change;
            $('#change_item').html("Kes."+data.change);

        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    }


    function display_treatment_prescription(visit_id,module){

        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"doctor/display_previous_prescription/"+visit_id+"/"+module;
        // alert(url);
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("visit_prescription_two").innerHTML=XMLHttpRequestObject.responseText;
                    
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    }
    function display_todays_tsheet(visit_id,module){

        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"doctor/display_tsheet_prescription/"+visit_id+"/"+module;
        // alert(url);
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("todays_tsheet_drugs").innerHTML=XMLHttpRequestObject.responseText;

                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });
                    $("#prescription_items").customselect();

                    
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

    }

    function pass_days_tsheet(visit_id)
   {
   var prescription_items = document.getElementById("prescription_items").value;
   var t_sheet_date = document.getElementById("t_sheet_date").value;

   save_drug_tsheet(prescription_items, visit_id,t_sheet_date);
   
   }
   
   function save_drug_tsheet(prescription_items, visit_id,t_sheet_date){
   
       var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/save_tsheet/"+prescription_items+"/"+visit_id+"/"+t_sheet_date;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
             display_treatment_prescription(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }
   function update_morning_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#morning'+prescription_id).val();
    
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_morning_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }


    function update_midday_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#midday'+prescription_id).val();
    // alert(checkedValue);
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_midday_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }

    function update_evening_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#evening'+prescription_id).val();
    
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_evening_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }

   function update_night_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#night'+prescription_id).val();
    
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_night_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }


   function delete_tsheet_prescription(prescription_id, visit_id)
   {
   var res = confirm('Are you sure you want to mark this drug as completed?. This drug will not appear tommorow when prescribing');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = config_url+"doctor/delete_tsheet_prescription/"+prescription_id+"/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
             display_todays_tsheet(visit_id,1);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }

   function get_patient_treatment_sheet(visit_id)
   {
        document.getElementById("loader").style.display = "block";
        display_treatment_prescription(visit_id,1);
        display_todays_tsheet(visit_id,1);
        document.getElementById("loader").style.display = "none";

   }


   function open_nurse_model(notes_id)
   {
    
     // tinymce.remove('#nurse_notes'+notes_id);
     $('#edit_notes'+notes_id).modal('show');
     tinymce.init({
                    selector: ".cleditor",
                    height : "100"
                });
      $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });
   }

  
</script>