<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";
error_reporting(0);



class Debtors extends accounts 
{
	var $document_upload_path;
	var $document_upload_location;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('debtors/debtors_model');
		$this->load->model('reception/reception_model');
		$this->load->model('admin/file_model');
		$this->load->model('finance/purchases_model');
		$this->load->model('finance/transfer_model');
		$this->load->model('hospital_reports/hospital_reports_model');

		$this->document_upload_path = realpath(APPPATH . '../assets/debtor_batches');
		$this->document_upload_location = base_url().'assets/debtor_batches/';
	}


	public function index()
	{

		
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('visit_type_id > 0');
		$this->db->order_by('visit_type.alias','ASC');
		$query = $this->db->get('visit_type');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$visit_type_name = $row->visit_type_name;
		}
		
		else
		{
			$visit_type_name = '';
		}
		$where = 'visit_type.visit_type_id > 0';
		$search = $this->session->userdata('search_debtors_batches');		

		if(!empty($search))
		{
			// $where .= $search;
		}
		else
		{
			$this->session->set_userdata('report_detail',180);
		}
		


		$table = 'visit_type';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/debtors-accounts';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 200;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
         $v_data["visit_type_id"] = $visit_type_id;

         // var_dump($visit_type_id);die();
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query_list'] = $this->debtors_model->get_all_debtors($table, $where, $config["per_page"], $page);
		$data['title'] = $v_data['title'] = 'Debtors';
		$data['content'] = $this->load->view('debtors/debtors/all_debtors', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_debtors_accounts()
	{
		$visit_type_name = $this->input->post('visit_type_name');
		$date_to = $this->input->post('date_to');
		$date_from = $this->input->post('date_from');
		$interval_limit = $this->input->post('interval_limit');
		
		if(!empty($visit_type_name))
		{

			$this->session->set_userdata('search_debtors_batches', ' AND visit_type.visit_type_name LIKE \'%'.$visit_type_name.'%\'');
		}

		if(!empty($date_from))
		{
			$this->session->set_userdata('date_debtors_batches', $date_from);
		}

		if(!empty($interval_limit))
		{
			$this->session->set_userdata('report_detail', $interval_limit);
		}



		// if(!empty($visit_date_from) && !empty($visit_date_to))
		// {
		// 	$visit_payments = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
		// 	$visit_invoices = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
		// 	$search_title .= date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		// }
		
		// else if(!empty($visit_date_from))
		// {
		// 	$visit_payments = ' AND data.transaction_date = \''.$visit_date_from.'\'';
		// 	$visit_invoices = ' AND data.transaction_date <= \''.$visit_date_from.'\'';
		// 	$search_title .= date('jS M Y', strtotime($visit_date_from)).' ';
		// }
		
		// else if(!empty($visit_date_to))
		// {
		// 	$visit_payments = ' AND data.transaction_date = \''.$visit_date_to.'\'';
		// 	$visit_invoices = ' AND data.transaction_date <= \''.$visit_date_to.'\'';
		// 	$search_title .= date('jS M Y', strtotime($visit_date_to)).' ';
		// }
		
		// else
		// {
		// 	$visit_payments = '';
		// 	$visit_invoices = '';
		// }


		
		redirect('accounts/debtors-accounts');
	}
	public function search_hospital_debtors()
	{
		$visit_type_name = $this->input->post('visit_type_name');
		
		if(!empty($visit_type_name))
		{
			$this->session->set_userdata('search_hospital_debtors', ' AND visit_type.visit_type_name LIKE \'%'.$visit_type_name.'%\'');
		}
		
		redirect('accounts/debtors-batches');
	}
	
	public function close_search_hospital_debtors()
	{
		$this->session->unset_userdata('search_hospital_debtors');
		
		redirect('accounts/debtors-batches');
	}

	public function print_debtors_accounts()
	{

		$v_data['page'] = 0;
		$v_data['total_patients'] = 1;		
	
		
		$data['title'] = $v_data['title'] = 'Customer Balances Print Summary';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		
		$v_data['module'] = $module;


		$v_data['month'] = $this->accounts_model->get_months();
		$v_data['contacts'] = $this->site_model->get_contacts();


		// $this->load->view('reports/print_customer_balance_summary', $v_data);

		
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('visit_type_id > 0');
		$query = $this->db->get('visit_type');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$visit_type_name = $row->visit_type_name;
		}
		
		else
		{
			$visit_type_name = '';
		}
		$where = 'visit_type.visit_type_id >0';
		
		$search = $this->session->userdata('search_debtors_batches');		

		if(!empty($search))
		{
			// $where .= $search;
		}
		else
		{
			$this->session->set_userdata('report_detail',180);
		}
		



		$table = 'visit_type';
		$segment = 3;
	
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
         $v_data["visit_type_id"] = $visit_type_id;

         // var_dump($visit_type_id);die();
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query_list'] = $this->debtors_model->get_all_debtors($table, $where, $config["per_page"], $page);
		$data['title'] = $v_data['title'] = 'DEBTORS';
		// $data['content'] = $this->load->view('debtors/debtors/print_debtors_accounts', $v_data, TRUE);

		$this->load->view('debtors/debtors/print_debtors_accounts', $v_data);
		
		// $this->load->view('admin/templates/general_page', $data);
	}
	public function debtor_statement($debtor_id)
	{

		$where = 'visit_type_id = '.$debtor_id;
		$table = 'visit_type';
		
		
		// echo $where;die();
		// $v_data['balance_brought_forward'] = $this->creditors_model->calculate_balance_brought_forward($date_from,$creditor_id);
		// $creditor = $this->creditors_model->get_creditor($creditor_id);
		// $row = $creditor->row();
		// $creditor_name = $row->creditor_name;
		// $opening_balance = $row->opening_balance;
		// $debit_id = $row->debit_id;
		// // var_dump($opening_balance); die();
		// $v_data['module'] = 1;
		// $v_data['creditor_name'] = $creditor_name;
		
		// $v_data['creditor_id'] = $creditor_id;
		// $v_data['date_from'] = $date_from;
		// $v_data['date_to'] = $date_to;
		// $v_data['opening_balance'] = $opening_balance;
		// $v_data['debit_id'] = $debit_id;
		// $v_data['query'] = $this->creditors_model->get_creditor_account($where, $table);
		$v_data['title'] = 'Debtor ';
		$v_data['debtor_id'] = $debtor_id;
		$data['title'] = 'Statement';
		$data['content'] = $this->load->view('debtors/debtors/statement', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function export_debtor_statement($visit_type_id,$start_date,$end_date)
	{
		$this->debtors_model->export_debtor_statement($visit_type_id,$start_date,$end_date);
	}

	//export debtors accounts

	public function export_debtors_aging_summary()
	{
		$this->debtors_model->export_debtors_aging_summary();
	}

	

	public function all_debtors()
	{
		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;
		
		// $where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.close_card <> 2 AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL)';
		// $table = 'visit, patients, visit_type';


		$where = 'patients.patient_id > 0 AND patients.patient_delete = 0';
		$table = 'patients';

		$visit_search = $this->session->userdata('all_debtors_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= '';

		}
		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/patients-accounts';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 30;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->debtors_model->get_all_visits_view($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['search'] = $visit_search;
		$v_data['total_patients'] = $config['total_rows'];		
		
		$page_title = $this->session->userdata('page_title');
		if(empty($page_title))
		{
			$page_title = 'Debtors';
		}
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = $config['total_rows'];
		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('accounts/all_debtors_view', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	public function search_all_debtors_report()
	{
		// alert("dasdhakjh");
		$patient_number = $this->input->post('patient_number');
		$patient_phone = $this->input->post('patient_phone');
		$patient_name = $this->input->post('patient_name');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($patient_phone))
		{

			$patient_phone = ' AND patients.patient_phone1 LIKE \'%'.$patient_phone.'%\' ';


		}
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\' ';
			
			$search_title .= 'Patient number. '.$patient_number;
		}
		
		
		
		$surname = '';

		//search surname
		if(!empty($_POST['patient_name']))
		{
			$search_title .= ' first name <strong>'.$_POST['patient_name'].'</strong>';
			$surnames = explode(" ",$_POST['patient_name']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		$search = $patient_number.$surname.$patient_phone;
		// var_dump($search); die();
		$this->session->set_userdata('all_debtors_search_query', $search);
		$this->session->set_userdata('search_title', $search_title);
		
		redirect('accounts/patients-accounts');
	}

	public function close_all_reports_search()
	{
		$this->session->unset_userdata('all_debtors_search_query');

		// redirect('hospital-reports/debtors');
		redirect('accounts/patients-accounts');
	}
	public function export_debtors()
	{
		$this->debtors_model->export_debtors();
	}






	// debtors accounts

	public function debtors_accounts()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('visit_type_id > 0');
		$query = $this->db->get('visit_type');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$visit_type_name = $row->visit_type_name;
		}
		
		else
		{
			$visit_type_name = '';
		}


		$where = 'visit_type.visit_type_id > 0 ';
		// $search = $this->session->userdata('search_debtors_batches');	
		// var_dump($search);die();	
		// $where .= $search;		
		$table = 'visit_type';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/debtors-batching';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->debtors_model->get_all_debtors($table, $where, $config["per_page"], $page);
		$data['title'] = $v_data['title'] = 'Debtors';
		$data['content'] = $this->load->view('debtors/debtors/debtors_accounts', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_debtors_batches()
	{
		$visit_type_name = $this->input->post('visit_type_name');
		$date_to = $this->input->post('date_to');
		$date_from = $this->input->post('date_from');
		$interval_limit = $this->input->post('interval_limit');
		
		if(!empty($visit_type_name))
		{

			$this->session->set_userdata('search_debtors_batches', ' AND visit_type.visit_type_name LIKE \'%'.$visit_type_name.'%\'');
		}

		if(!empty($date_from))
		{
			$this->session->set_userdata('date_debtors_batches', $date_from);
		}

		if(!empty($interval_limit))
		{
			$this->session->set_userdata('report_detail', $interval_limit);
		}



		// if(!empty($visit_date_from) && !empty($visit_date_to))
		// {
		// 	$visit_payments = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
		// 	$visit_invoices = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
		// 	$search_title .= date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		// }
		
		// else if(!empty($visit_date_from))
		// {
		// 	$visit_payments = ' AND data.transaction_date = \''.$visit_date_from.'\'';
		// 	$visit_invoices = ' AND data.transaction_date <= \''.$visit_date_from.'\'';
		// 	$search_title .= date('jS M Y', strtotime($visit_date_from)).' ';
		// }
		
		// else if(!empty($visit_date_to))
		// {
		// 	$visit_payments = ' AND data.transaction_date = \''.$visit_date_to.'\'';
		// 	$visit_invoices = ' AND data.transaction_date <= \''.$visit_date_to.'\'';
		// 	$search_title .= date('jS M Y', strtotime($visit_date_to)).' ';
		// }
		
		// else
		// {
		// 	$visit_payments = '';
		// 	$visit_invoices = '';
		// }


		
		redirect('accounts/debtors-statements');
	}

	public function close_search_debtors()
	{
		$this->session->unset_userdata('search_debtors_batches');
		$this->session->unset_userdata('date_debtors_batches');
		
		redirect('accounts/debtors-statements');
	}
	
	public function close_search_debtors_batches()
	{
		$this->session->unset_userdata('search_debtors_batches');
		$this->session->unset_userdata('date_debtors_batches');
		
		redirect('accounts/debtors-batches');
	}

	public function debtor_batches($debtor_id)
	{

		$where = 'visit_type_id = '.$debtor_id;
		$table = 'visit_type';
		
		

		// $v_data['creditor_id'] = $creditor_id;
		// $v_data['date_from'] = $date_from;
		// $v_data['date_to'] = $date_to;
		// $v_data['opening_balance'] = $opening_balance;
		// $v_data['debit_id'] = $debit_id;
		// $v_data['query'] = $this->creditors_model->get_creditor_account($where, $table);
		$v_data['title'] = 'Debtor ';
		$v_data['debtor_id'] = $debtor_id; 
		$data['title'] = 'Statement';
		$data['content'] = $this->load->view('debtors/debtors/batches', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function calculate_billed_items()
	{
		$billed_items = $this->input->post('billed');
		

		$billed_items = str_replace(' ', '', $billed_items);
		$split = explode(",", $billed_items);
		$total_count = count($split);
		$total_amount = 0;
		if(is_array($split))
		{
			if($total_count > 0)
			{
				for ($i=0; $i <= $total_count; $i++) { 
					# code...

					$total_amount += $split[$i];


				}
			}
		}

		// var_dump($total_amount);die();
	
		$response['message'] ='success';
		$response['billing'] = $total_amount;
		echo json_encode($response);
	}


	public function clear_session_search_filter()
	{
		$this->session->unset_userdata('date_from');
		$this->session->unset_userdata('date_from');

		echo "success";

	}

	public function get_unallocated_invoices_data($id)
	{
		//var_dump($id);
		//die();

		$creditor_result = $this->debtors_model->get_unallocated_invoices($id);
		//var_dump($creditor_result);
		//die();
		// if($creditor_result->num_rows() > 0)
		// {
		// 	echo $creditor_result->result();
		// 	//var_dump($creditor_result->result());
		// 	//die();
		// }
		$data['creditor_result'] = $creditor_result;
		$data['debtor_id'] = $id;

		$page = $this->load->view('debtors/debtors/unbatched_invoices',$data,true);
		echo $page;
	}

	public function create_debtor_batch($visit_type_id)
	{
		if($this->debtors_model->create_debtor_batch($visit_type_id))
		{ 
			$this->session->set_userdata('success_message', 'Batch has been successfully created');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not create batch. Please try again');
		}

		redirect('accounts/batches/'.$visit_type_id);
	}

	public function approve_batch($debtor_batch_id,$visit_type_id)
	{
		$array['batch_status'] = 2;
		$array['date_approved'] = date('Y-m-d');
		$array['approved_by'] = $this->session->userdata('personnel_id');

		$this->db->where('debtor_batch_id',$debtor_batch_id);
		if($this->db->update('debtor_batches',$array))
		{
			$this->session->set_userdata('success_message', 'Batch has been successfully approved');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not approve batch. Please try again');
		}

		redirect('accounts/batches/'.$visit_type_id);

	}
	public function view_dispatch_invoices($debtor_batch_id)
	{
		$data['debtor_batch_id'] = $debtor_batch_id;
		$where = 'debtor_batches.visit_type_id = visit_type.visit_type_id AND debtor_batch_id = '.$debtor_batch_id;
		$table = 'debtor_batches,visit_type';

		$this->db->where($where);
		$this->db->select('visit_type.visit_type_name,debtor_batches.*');
		$query = $this->db->get($table);
		

		$data['query'] = $query;
		$data['debtor_batch_id'] = $debtor_batch_id;
		
		$page = $this->load->view('debtors/debtors/view_dispatch_invoices',$data,true);
		// var_dump($page);die();
		echo $page;
	}
	public function print_batch($debtor_batch_id)
	{
		$where = 'debtor_batches.visit_type_id = visit_type.visit_type_id AND debtor_batch_id = '.$debtor_batch_id;
		$table = 'debtor_batches,visit_type';

		$this->db->where($where);
		$this->db->select('visit_type.visit_type_name,debtor_batches.*');
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_type_name = $value->visit_type_name;
				$batch_date_from = $value->batch_date_from;
				$batch_date_to = $value->batch_date_to;
				$debtor_id = $value->visit_type_id;
			}
		}
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['title'] = $visit_type_name.' Batch '.$batch_date_from.' '.$batch_date_to;
		$v_data['debtor_id'] = $debtor_id;
		$v_data['query'] = $query;
		$v_data['debtor_batch_id'] = $debtor_batch_id;
		$data['title'] = $v_data['title'];
		echo $this->load->view('debtors/debtors/print_debtor_batch', $v_data);
		
		// $this->load->view('admin/templates/general_page', $data);
	}






	// remittance reconcilliations


	function import_payments()
	{


		$where = 'account.account_id = batch_receipts.bank_id AND visit_type.visit_type_id = batch_receipts.insurance_id AND batch_receipts.batch_receipt_delete = 0';


		$search_item = $this->session->userdata('batch_payments_search');

		if(!empty($search_item))
		{
			$where .= $search_item;
		}

		// var_dump($where);die();
		$table = 'batch_receipts,account,visit_type';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/remittance-reconcilliations';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->debtors_model->get_current_payments($table, $where, $config["per_page"], $page);
		
		//change of order method 
		
		
		$data['title'] = 'Payments';		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		// var_dump($query);die();
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('remittances/payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	
	function do_payment_import()
	{
		$data_where = array(
							'receipt_number' => $this->input->post('receipt_number'),
							'payment_date'=>$this->input->post('payment_date'),
							'payment_method_id'=>$payment_method_id,
							'bank_id'=>$this->input->post('bank_id'),
							'insurance_id'=>$this->input->post('insurance_id'),
							'total_amount_paid'=>$this->input->post('total_amount_paid')
						);

		
		$this->db->where($data_where);
		$query = $this->db->get('batch_receipts');
		// var_dump($query); die();
		if($query->num_rows() === 1)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$batch_receipt_id = $value->batch_receipt_id;
			}
		}
		else
		{

			$data_checked = array(
									'receipt_number' => $this->input->post('receipt_number'),
									'payment_date'=>$this->input->post('payment_date'),
									'payment_method_id'=>$payment_method_id,
									'bank_id'=>$this->input->post('bank_id'),
									'insurance_id'=>$this->input->post('insurance_id'),
									'total_amount_paid'=>$this->input->post('total_amount_paid')
								  );

			// var_dump($data); die();
			$this->db->insert('batch_receipts',$data_checked);
			$batch_receipt_id = $this->db->insert_id();



		}
		redirect('accounts/remittance-reconcilliations');
	}

	public function search_batch_payments()
	{

		$payment_date_from = $this->input->post('payment_date_from');
		$payment_date_to = $this->input->post('payment_date_to');
		$receipt_number = $this->input->post('receipt_number');
		$bank_id = $this->input->post('bank_id');
		$insurance_id = $this->input->post('insurance_id');

		$search_title = '';
		if(!empty($bank_id))
		{
			$bank = ' AND bank_id = '.$bank_id;
		}
		else
		{
			$bank = '';
		}

		if(!empty($insurance_id))
		{
			$insurance = ' AND insurance_id = '.$insurance_id;
		}
		else
		{
			$insurance = '';
		}


		if(!empty($receipt_number))
		{
			$receipt = ' AND receipt_number LIKE \'%'.$receipt_number.'%\'';
		}
		else
		{
			$receipt = '';
		}


		if(!empty($payment_date_from) && !empty($payment_date_to))
		{
			$dates = ' AND DATE(payment_date) BETWEEN \''.$payment_date_from.'\' AND \''.$payment_date_to.'\'';
			$search_title .= 'Payments from '.date('jS M Y', strtotime($payment_date_from)).' to '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else if(!empty($payment_date_from))
		{
			$dates = ' AND DATE(payment_date) = \''.$payment_date_from.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($payment_date_from)).' ';
		}
		
		else if(!empty($payment_date_to))
		{
			$dates = ' AND DATE(payment_date) = \''.$payment_date_to.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($payment_date_to)).' ';
		}
		
		else
		{
			$dates = '';
		}



		$search = $dates.$insurance.$bank.$receipt;

		$this->session->set_userdata('batch_payments_search',$search);
		$this->session->set_userdata('batch_payment_search_title', $search_title);
		
		redirect('accounts/remittance-reconcilliations');



	}

	public function close_batch_search()
	{

		$this->session->unset_userdata('batch_payments_search');
		$this->session->unset_userdata('batch_payment_search_title');
		
		redirect('accounts/remittance-reconcilliations');
	}

	public function calculate_billed_items_amount()
	{
		$billed_items = $this->input->post('billed');
		$batch_receipt_id = $this->input->post('batch_receipt_id');
		$total_paid = $this->input->post('total_paid');

		$billed_items = str_replace(' ', '', $billed_items);
		$split = explode(",", $billed_items);
		$total_count = count($split);
		$total_amount = 0;
		if(is_array($split))
		{
			if($total_count > 0)
			{
				for ($i=0; $i < $total_count; $i++) { 
					# code...
					$total_amount += $split[$i];
				}
			}
		}

		$total_unallocated = $this->debtors_model->get_all_unallocated_payments($batch_receipt_id);
		$total_amount += $total_unallocated;
		$balance = $total_paid - $total_amount;
		$response['message'] ='success';
		$response['billing'] = $total_amount;
		$response['balance'] = $balance;
		echo json_encode($response);
	}

	public function confirm_payments($batch_receipt_id)
	{
		if($this->debtors_model->confirm_batch_payments($batch_receipt_id))
		{

			$array_update['current_payment_status'] = 1;
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$this->db->update('batch_receipts',$array_update);


			$this->session->set_userdata('success_message', 'Remittance successfully added');
			redirect('accounts/remittance-reconcilliations');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not update remittance. Please try again');
			redirect('accounts/remittance-reconcilliations');
		}

		
	}
	public function add_unallocated_payment_view($batch_receipt_id)
	{
		$v_data['batch_receipt_id']   = $batch_receipt_id;

		$page = $this->load->view('remittances/unallocated_payment_view',$v_data);

		echo $page;


	}
	public function add_unallocated_payment()
	{
		$array['amount_paid'] = $this->input->post('amount_paid_unreconcilled');
		$array['invoice_number'] = $this->input->post('invoice_referenced');
		$array['reason'] = $this->input->post('payment_description');
		$array['batch_receipt_id'] = $this->input->post('batch_receipt_id');
		$array['allocation_type_id'] = $this->input->post('allocation_type_id');

		if($this->db->insert('batch_unallocations',$array))
		{
			$response['message'] ='success';
			$response['result'] = 'You have successfully added this payment item';
		}
		else
		{
			$response['message'] ='success';
			$response['result'] = 'Sorry could not add this payment';
		}

		echo json_encode($response);
	}

	public function get_unreconcilled_payments($batch_receipt_id)
	{
		$v_data['batch_receipt_id']   = $batch_receipt_id;

		$response['message'] = 'success';
		$response['results'] = $this->load->view('remittances/unallocated_payments',$v_data,true);

		echo json_encode($response);
	}
	public function delete_unallocated_payment($unallocated_payment_id)
	{
		$array_update['current_payment_status'] = 0;
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$this->db->update('batch_receipts',$array_update);


		$update['unallocated_payment_delete'] = 1;
		$this->db->where('unallocated_payment_id',$unallocated_payment_id);
		if($this->db->update('batch_unallocations',$update))
		{
			$response['message'] = 'success';
			$response['results'] = 'You successfully removed the item';

		}
		else
		{
			$response['message'] = 'fail';
			$response['results'] = 'Sorry could not remove the item. Please try again';
		}

		echo json_encode($response);
	}


	public function update_visit_invoice($visit_invoice_id)
	{
		// $update_array['visit_invoice_id'] = $visit_invoice_id;
		$array_update['current_payment_status'] = 1;
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$this->db->update('batch_receipts',$array_update);


		$this->db->where('batch_receipt_id > 0 AND visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('visit_invoice');


		if($query->num_rows() > 0)
		{
		


			$update_array['batch_receipt_id'] = 0;
			$update_array['amount_to_pay'] = 0;
			$this->db->where('visit_invoice_id',$visit_invoice_id);
			if($this->db->update('visit_invoice',$update_array))
			{
				$response['message'] = 'success';
				$response['results'] = 'You successfully removed the item';

			}
			else
			{
				$response['message'] = 'fail';
				$response['results'] = 'Sorry could not remove the item. Please try again';
			}

		}
		else
		{
			$update_array['batch_receipt_id'] = $this->input->post('batch_receipt_id');
			$update_array['amount_to_pay'] = $this->input->post('amount_payable');

			$this->db->where('visit_invoice_id',$visit_invoice_id);

			if($this->db->update('visit_invoice',$update_array))
			{
				$response['message'] = 'success';
				$response['results'] = 'You successfully removed the item';

			}
			else
			{
				$response['message'] = 'fail';
				$response['results'] = 'Sorry could not remove the item. Please try again';
			}
		}



		

		echo json_encode($response);
	}

	public function view_batch_items($batch_receipt_id,$insurance_id,$location=NULL)
	{

		
		
		$data['title'] = 'Payments';		
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['insurance_id'] = $insurance_id;

		if($location == 1)
		{

		}
		else
		{
			$update_array['batch_receipt_id'] = 0;
			$update_array['amount_to_pay'] = 0;
			$this->db->where('visit_invoice_status <> 1 AND  bill_to = '.$insurance_id);
			$this->db->update('visit_invoice',$update_array);
		}
		// var_dump($query);die();
		//open the add new product
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['content'] = $this->load->view('remittances/view_batch_items', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);

	}


	public function unsettled_invocies($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';



		$patient_surname = $this->input->post('patient_surname');
  		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$invoice_number = $this->input->post('invoice_number');
		$patient_number = $this->input->post('patient_number');
		$phone_number = $this->input->post('phone_number');

		$add = '';
		if(!empty($patient_surname))
		{
			// $search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['patient_surname']);
			$total = count($surnames);
			
			$count = 1;
			$add .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$add .= ') ';
		}


		if(!empty($date_from) AND !empty($date_to) )
		{
			$add .= ' AND visit_invoice.created >= "'.$date_from.'" AND visit_invoice.created <= "'.$date_to.'"';
		}
		else if(empty($date_from) AND !empty($date_to))
		{
			$add .= ' AND visit_invoice.created = "'.$date_to.'"';
		}
		else if(!empty($date_from) AND empty($date_to))
		{
			$add .= ' AND visit_invoice.created = "'.$date_from.'"';
		}


		if(!empty($invoice_number))
		{
			$add .= ' AND visit_invoice.visit_invoice_number LIKE \'%'.$invoice_number.'%\'';
		}

		if(!empty($phone_number))
		{
			$add .= ' AND patients.patient_phone1 LIKE \'%'.$phone_number.'%\'';
		}

		if(!empty($patient_number))
		{
			$add .= ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}





		// $visit_invoice_number = $this->input->post('visit_invoice_number');
		// $query = null;
		// if(!empty($visit_invoice_number))
		// {

		// 	$where = 'visit_invoice.visit_invoice_status <> 1 AND patients.patient_id = visit_invoice.patient_id  AND visit_invoice.invoice_balance >  0 AND visit_invoice.batch_receipt_id <> '.$batch_receipt_id.' AND visit_invoice.bill_to = '.$insurance_id;
		// 	$table = 'visit_invoice,patients';
		// 	$where .= ' AND visit_invoice.visit_invoice_number LIKE \'%'.$visit_invoice_number.'%\'';

		// 	$this->db->limit(10000);

		// }
		// else
		// {
			$where = 'visit_invoice.visit_invoice_status <> 1 AND patients.patient_id = visit_invoice.patient_id  AND visit_invoice.invoice_balance >  0 AND visit_invoice.batch_receipt_id <> '.$batch_receipt_id.' AND visit_invoice.bill_to = '.$insurance_id.$add;
			$table = 'visit_invoice,patients';

			$this->db->limit(10000);
		// }


		
        
       
        $query = $this->debtors_model->get_all_unpaid_invoices($table, $where);



        // var_dump($query);die();
        $v_data['query'] = $query;


		$data['result'] = $this->load->view('remittances/unsettled_invocies', $v_data,true);
		
		echo json_encode($data);
	}

	public function picked_invocies($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;

		$patient_surname = $this->input->post('patient_surname');
        $invoice_number = $this->input->post('invoice_number');

        $add = '';
        if(!empty($patient_surname))
        {
            // $search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
            $surnames = explode(" ",$_POST['patient_surname']);
            $total = count($surnames);
            
            $count = 1;
            $add .= ' AND (';
            for($r = 0; $r < $total; $r++)
            {
                if($count == $total)
                {
                    $add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\')';
                }
                
                else
                {
                    $add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
                }
                $count++;
            }
            $add .= ') ';
        }


      

        if(!empty($invoice_number))
        {
            $add .= ' AND visit_invoice.visit_invoice_number LIKE \'%'.$invoice_number.'%\'';
        }

        $where = 'visit_invoice.visit_invoice_status <> 1 AND patients.patient_id = visit_invoice.patient_id AND visit_invoice.batch_receipt_id = '.$batch_receipt_id.' AND visit_invoice.bill_to = '.$insurance_id.$add;
        
        $table = 'visit_invoice,patients';
        
    
        $query = $this->debtors_model->get_all_unpaid_invoices($table, $where);

        $v_data['query'] = $query;

		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';
		$data['result'] = $this->load->view('remittances/selected_invocies', $v_data,true);
		
		echo json_encode($data);
	}

	public function get_total_amount_reconcilled($batch_receipt_id)
	{

		// check if the batch is closed 
		$this->db->from('payments,payment_item,batch_receipts');
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_amount_paid');
		$this->db->where('batch_receipts.batch_receipt_id = payments.batch_receipt_id AND payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND batch_receipts.current_payment_status = 1 AND payments.batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount = 0;

		// var_dump($query->result());die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount_paid;
			}

			if(empty($total_amount))
			{
				$this->db->where('batch_receipt_id',$batch_receipt_id);
				$this->db->select('SUM(visit_invoice.amount_to_pay) AS total_amount');
				$query=$this->db->get('visit_invoice');

				$total_amount = 0;
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$total_amount = $value->total_amount;
					}
				}

				if(empty($total_amount))
				{
					$total_amount = 0;
				}

			}


		}
		else
		{
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$this->db->select('SUM(visit_invoice.amount_to_pay) AS total_amount');
			$query=$this->db->get('visit_invoice');

			$total_amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$total_amount = $value->total_amount;
				}
			}

			if(empty($total_amount))
			{
				$total_amount = 0;
			}


		}

		
		
		$amount_unallocated = $this->debtors_model->get_all_unallocated_payments($batch_receipt_id);

		$total_paid = $this->input->post('total_paid');
		$balance = $total_paid - ($total_amount +$amount_unallocated);
		// var_dump($total_paid);die();
		
		$result['billing'] = $total_amount+$amount_unallocated;
		$result['balance'] = $balance;
		$result['message'] = 'success';


		echo json_encode($result);
	}
	public function update_amount_to_pay($visit_invoice_id,$batch_receipt_id)
	{




		$update_array['batch_receipt_id'] = $batch_receipt_id;
		$update_array['amount_to_pay'] = $this->input->post('billed');

		$this->db->where('visit_invoice_id',$visit_invoice_id);

		if($this->db->update('visit_invoice',$update_array))
		{
			$response['message'] = 'success';
			$response['results'] = 'You successfully removed the item';

		}
		else
		{
			$response['message'] = 'fail';
			$response['results'] = 'Sorry could not remove the item. Please try again';
		}

		echo json_encode($response);
	}
		public function product_list($visit_invoice_id)
	{
		$data = array('visit_invoice_id'=>$visit_invoice_id);


		$drug_name = $this->input->post('drug');
		$query = null;
		if(!empty($drug_name))
		{

			$ultra_sound_where = 'visit_invoice_delete = 0';


			$ultra_sound_table = 'visit_invoice';

			$ultra_sound_where .= ' AND visit_invoice_number LIKE \'%'.$drug_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'visit_invoice_delete = 0';
			$ultra_sound_table = 'visit_invoice';
	

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('remittances/search_invoice',$data,true);
		$response['message'] = 'success';
		$response['results'] = $page;

		
		

		echo json_encode($response);
	}	

	public function paid_invoices($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';
		$data['result'] = $this->load->view('remittances/paid_invoices', $v_data,true);
		
		echo json_encode($data);
	}

	public function open_debtor_payment($insurance_id,$batch_receipt_id)
	{


		// get all payments
		$where = 'payments.batch_receipt_id = '.$batch_receipt_id.' AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payments.payment_id = payment_item.payment_id AND visit_invoice.bill_to = '.$insurance_id;
        
        $table = 'visit_invoice,payment_item,payments';


		$this->db->where($where);
		$query = $this->db->get($table);

		// var_dump($query->result());die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_invoice_id = $value->visit_invoice_id;
				$payment_item_amount = $value->payment_item_amount;
				$payment_id = $value->payment_id;
				


				// cancel the payments

				$payment_array['cancel'] = 1;
				$payment_array['cancelled_by'] = $this->session->userdata('personnel_id');

				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$payment_array);



				// set status of the visit invoice as not cleared

				$visit_invoice_array['visit_invoice_status'] = 0;
				$visit_invoice_array['batch_receipt_id'] = $batch_receipt_id;
				$visit_invoice_array['amount_to_pay'] = $payment_item_amount;

				$this->db->where('visit_invoice_id',$visit_invoice_id);
				$this->db->update('visit_invoice',$visit_invoice_array);


					$this->accounts_model->update_invoice_totals($visit_invoice_id);
			}

			$array_update['current_payment_status'] = 0;
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$this->db->update('batch_receipts',$array_update);
		}
		

		redirect('debtors/view_batch_items/'.$batch_receipt_id.'/'.$insurance_id.'/1');

	}

	public function update_opening_balance($visit_type_id)
	{

		// var_dump($_POST); die();
		$this->form_validation->set_rules('start_date', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('opening_balance', 'Amount', 'trim|required|xss_clean');
		
		if ($this->form_validation->run())
		{
			if($this->debtors_model->update_debtor_account($visit_type_id))
			{
				$this->session->set_userdata('success_message', 'Updated provider account successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to update. Please try again');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}



	public function update_debtor_batch_date($visit_type_id)
	{

		// var_dump($_POST); die();
		$this->form_validation->set_rules('start_date', 'Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run())
		{
			if($this->debtors_model->update_debtor_batch_date($visit_type_id))
			{
				$this->session->set_userdata('success_message', 'Updated provider account successfully');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Unable to update. Please try again');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function close_batch($debtor_batch_id)
	{
		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';
		
		//upload image if it has been selected
		$response = $this->debtors_model->upload_any_file($this->document_upload_path, $this->document_upload_location, $document_name, 'document_scan');
		if($response)
		{
			$document_upload_location = $this->document_upload_location.$this->session->userdata($document_name);
		}
		
		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);
		
		$this->form_validation->set_rules('received_date', 'Date Received', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->debtors_model->upload_batched_documents($document,$debtor_batch_id))
			{
				$this->session->set_userdata('success_message', 'Document uploaded successfully');
				$this->session->unset_userdata($document_name);

				$response2['message'] = 'success';
				$response2['result'] = 'You have successfully uploaded the result';
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
				$response2['message'] = 'fail';
				$response2['result'] = 'Sorry could not upload the document. Please try again';
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			$response2['message'] = 'fail';
			$response2['result'] = 'Sorry could not upload the document. Please try again';
		}
		
		$debtor_id = $this->input->post('debtor_id');
		
		redirect('accounts/batches/'.$debtor_id);

		// echo json_encode($response2);
	}



	// statements of accounts 


	public function statements_of_accounts_older()
	{
		$module = NULL;
		
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.visit_invoice_status <> 1 AND visit_invoice.created >= "2014-03-01"';
		
		$table = 'v_transactions_by_date,visit_invoice';


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->debtors_model->get_statements_of_accounts($table, $where,1);

			// var_dump($query);die();
			
		}
		else
		{
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		

			$query = NULL;

		}

	


		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Statements of accounts';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// $v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['query'] = $query;
		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('reports/debtors', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function statements_of_accounts()
	{
		$module = NULL;
		
		
		// $where = 'visit_invoice.visit_invoice_delete = 0   ';
		
		// $table = 'visit_invoice';


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->debtors_model->get_invoice_balances_per_visit();

			// var_dump($query->num_rows());die();
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		

			$query = NULL;

		}

	


		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Statements of accounts';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// $v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['query'] = $query;
		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('reports/debtors', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function statements_of_accounts_old()
	{
		$module = NULL;
		
		
		$where = 'visit_invoice.visit_invoice_delete = 0   ';
		
		$table = 'visit_invoice';


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->debtors_model->get_statements_of_accounts($table, $where,1);

			// var_dump($query->num_rows());die();
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		

			$query = NULL;

		}

	


		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Statements of accounts';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// $v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['query'] = $query;
		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('reports/debtors', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_statements_of_accounts()
	{
		$visit_type_id = $visit_type_idd = $this->input->post('visit_type_id');
		$branch_id = $branch_idd= $this->input->post('branch_id');
		$visit_date_from = $this->input->post('visit_date');
		$visit_date_to = $this->input->post('visit_date_to');

		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'STATEMENT OF ACCOUNTS FOR : ';
		
		if(!empty($visit_type_id))
		{
			$visit_type = $visit_type_id;
			$visit_type_id = ' AND visit_invoice.bill_to = '.$visit_type_id.' ';


			
			$this->db->where('visit_type_id', $visit_type_idd);
			$query = $this->db->get('visit_type');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->alias.' ';

				$visit_type_name =$row->visit_type_name;
			}
		}
		
	
		//date filter for cash report
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_payments = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'AS AT '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_payments = ' AND data.transaction_date = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND data.transaction_date <= \''.$visit_date_from.'\'';
			$search_title .= 'AS AT '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_payments = ' AND data.transaction_date = \''.$visit_date_to.'\'';
			$visit_invoices = ' AND data.transaction_date <= \''.$visit_date_to.'\'';
			$search_title .= 'AS AT '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_payments = '';
			$visit_invoices = '';
		}

		$surname = '';

		
		
		$search = $visit_type_id.$visit_invoices;
		
		$this->session->set_userdata('statements_search_query', $search);
		$this->session->set_userdata('statements_visit_invoices', $visit_invoices);
		$this->session->set_userdata('statements_visit_payments', $visit_payments);
		$this->session->set_userdata('statements_visit_type_id', $visit_type_id);
		$this->session->set_userdata('statements_visit_type', $visit_type);
		$this->session->set_userdata('statements_patient_number', $patient_number);
		$this->session->set_userdata('statements_search_title', $search_title);
		$this->session->set_userdata('statements_visit_type', $visit_type);
		$this->session->set_userdata('statements_visit_date', $visit_date_from);
		$this->session->set_userdata('statements_visit_type_name', $visit_type_name);

		
		
		redirect('accounts/statements-of-accounts');
	}

	public function search_statements_of_accounts_old()
	{
		$visit_type_id = $visit_type_idd = $this->input->post('visit_type_id');
		$branch_id = $branch_idd= $this->input->post('branch_id');
		$visit_date_from = $this->input->post('visit_date');
		$visit_date_to = $this->input->post('visit_date_to');

		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'STATEMENT OF ACCOUNTS FOR: ';
		
		if(!empty($visit_type_id))
		{
			$visit_type = $visit_type_id;
			$visit_type_id = ' AND visit_invoice.bill_to = '.$visit_type_id.' ';


			
			$this->db->where('visit_type_id', $visit_type_idd);
			$query = $this->db->get('visit_type');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->alias.' ';

				$visit_type_name =$row->visit_type_name;
			}
		}
		
	
		//date filter for cash report
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_payments = ' AND visit_invoice.created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit_invoice.created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'AS AT '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_payments = ' AND visit_invoice.created = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND visit_invoice.created <= \''.$visit_date_from.'\'';
			$search_title .= 'AS AT '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_payments = ' AND visit_invoice.created = \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit_invoice.created <= \''.$visit_date_to.'\'';
			$search_title .= 'AS AT '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_payments = '';
			$visit_invoices = '';
		}

		$surname = '';

		
		
		$search = $visit_type_id.$visit_invoices;
		
		$this->session->set_userdata('statements_search_query', $search);
		$this->session->set_userdata('statements_visit_invoices', $visit_invoices);
		$this->session->set_userdata('statements_visit_payments', $visit_payments);
		$this->session->set_userdata('statements_visit_type_id', $visit_type_id);
		$this->session->set_userdata('statements_visit_type', $visit_type);
		$this->session->set_userdata('statements_patient_number', $patient_number);
		$this->session->set_userdata('statements_search_title', $search_title);
		$this->session->set_userdata('statements_visit_type', $visit_type);
		$this->session->set_userdata('statements_visit_date', $visit_date_from);
		$this->session->set_userdata('statements_visit_type_name', $visit_type_name);

		
		
		redirect('accounts/statements-of-accounts');
	}

	public function close_statement_reports_search()
	{
		$this->session->unset_userdata('statements_search_query');
		$this->session->unset_userdata('statements_visit_invoices');
		$this->session->unset_userdata('statements_visit_payments');
		$this->session->unset_userdata('statements_visit_type_id');
		$this->session->unset_userdata('statements_visit_type');
		$this->session->unset_userdata('statements_patient_number');
		$this->session->unset_userdata('statements_search_title');
		$this->session->unset_userdata('statements_visit_type');
		$this->session->unset_userdata('statements_visit_type_name');

		redirect('accounts/statements-of-accounts');
	}

	public function export_statements_of_accounts()
	{
		$this->debtors_model->export_statements_of_accounts();
	}

	public function print_statements_of_accounts()
	{


		$module = NULL;
		
		
		
		// $where = 'visit_invoice.visit_invoice_delete = 0 AND visit_invoice.invoice_balance > 0  ';
		
		// $table = 'visit_invoice';


		// $visit_search = $this->session->userdata('statements_search_query');
		// // var_dump($visit_search);die();
		// if(!empty($visit_search))
		// {
		// 	$where .= $visit_search;
		
		// 	$query = $this->debtors_model->get_statements_of_accounts($table, $where,1);

		// 	// var_dump($query->num_rows());die();
			
		// }
		// else
		// {
		// 	$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
		// 	$visit_payments = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
		// 	$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
		// 	$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

		// 	$this->session->set_userdata('visit_invoices', $visit_invoices);
		// 	// $this->session->set_userdata('debtors_search_query', $visit_invoices);
		// 	$this->session->set_userdata('visit_payments', $visit_payments);
		

		// 	$query = NULL;

		// }

		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->debtors_model->get_invoice_balances_per_visit();

			// var_dump($query->num_rows());die();
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		

			$query = NULL;

		}


	
		$v_data['contacts'] = $this->site_model->get_contacts();

		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Statements of accounts';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// $v_data['branches'] = $this->reports_model->get_all_active_branches();
		$v_data['query'] = $query;
		
		$v_data['module'] = $module;
		
		$this->load->view('debtors/reports/print_statement_of_accounts', $v_data);
		
	}


	public function update_all_cash_invoice()
	{
		$this->db->where('payment_method_id <> 9 AND suffix = 0');
		$query = $this->db->get('payments');
		// var_dump($query->result());die();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$payment_id = $value->payment_id;
				$confirm_number = $value->confirm_number;
				$prefix = $value->prefix;
				$suffix = $value->suffix;


				$number = preg_replace("/[^0-9]/", "", $confirm_number );

				// var_dump($confirm_number);die();

				$array_update['confirm_number'] = $number;
				$array_update['suffix'] = $number;
				$array_update['prefix'] = 'PAY';
				$array_update['confirm_number'] = $number;

				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$array_update);
			}
		}
	}

	public function update_all_insurance_invoice()
	{
		$this->db->where('payment_method_id = 9 AND suffix = 0');
		$query = $this->db->get('payments');
		// var_dump($query->result());die();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$payment_id = $value->payment_id;
				$confirm_number = $value->confirm_number;
				$prefix = $value->prefix;
				$suffix = $value->suffix;

				$prefix = 'REM';
				$suffix = $this->accounts_model->create_visit_receipt_number($prefix);
				if($suffix < 10)
				{
					$number = '0000'.$suffix;
				}
				else if($suffix < 100 AND $suffix >= 10)
				{
					$number = '000'.$suffix;
				}
				else if($suffix < 1000 AND $suffix >= 100)
				{
					$number = '00'.$suffix;
				}
				else if($suffix < 10000 AND $suffix >= 1000)
				{
					$number = '0'.$suffix;
				}
				
				$array_update['branch_id'] = $branch_id;
				$array_update['prefix'] = $prefix;
				$array_update['suffix'] = $suffix;
				$array_update['confirm_number'] = $prefix.$number;


				// var_dump($array_update);die();


				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$array_update);
			}
		}
	}

	public function edit_remittance($batch_receipt_id)
	{
		$data['title'] = 'Batch Receipt';
		
		$creditor_where = 'batch_receipt_id = '.$batch_receipt_id;

		$creditor_table = 'batch_receipts';

		$this->db->where($creditor_where);
		$creditor_query = $this->db->get($creditor_table);

		$data['batch_receipt_id'] = $batch_receipt_id;
		$data['query'] = $creditor_query;

		
			
		$page = $this->load->view('remittances/edit_remittance',$data);

		echo $page;
	}
	public function edit_remittance_data($batch_receipt_id)
	{
		$data_checked = array(
								'receipt_number' => $this->input->post('receipt_number'),
								'payment_date'=>$this->input->post('payment_date'),
								'payment_method_id'=>$payment_method_id,
								'bank_id'=>$this->input->post('bank_id'),
								'insurance_id'=>$this->input->post('insurance_id'),
								'total_amount_paid'=>$this->input->post('total_amount_paid')
							  );

		// var_dump($data); die();
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$this->db->update('batch_receipts',$data_checked);
	

		$response['message'] = 'success';
		
		echo json_encode($response);
		
	}

	public function delete_remittance($batch_receipt_id)
	{
		$data_checked = array(
								'batch_receipt_delete' => 1
							  );

		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$this->db->update('batch_receipts',$data_checked);
	

		$response['message'] = 'success';
		
		redirect('accounts/remittance-reconcilliations');
		
	}

	public function update_all_visit_invoice_number($visit_type_id)
	{
		$this->db->where('visit_invoice_id = 0 AND visit_charge_id > 0 AND bill_to = '.$visit_type_id);
		$query = $this->db->get('invoice_items');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$visit_charge_id = $value->visit_charge_id;
				$item_id = $value->item_id;

				$this->db->where('visit_charge_id = '.$visit_charge_id);
				$query_two = $this->db->get('visit_charge');

				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value_two) {

						$visit_invoice_id  = $value_two->visit_invoice_id;
					}

					// update thh

					if($visit_invoice_id > 0)
					{
						$add['visit_invoice_id'] = $visit_invoice_id;
						$this->db->where('item_id',$item_id);
						$this->db->update('invoice_items',$add);

					}
					
				}


			}
		}
	}


	public function update_visit_charges_preauth_date($visit_type_id)
	{
		$this->db->where('visit_invoice_id > 0 AND bill_to ='.$visit_type_id);
		$this->db->group_by('visit_invoice_id');
		$query = $this->db->get('invoice_items');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$preauthorisation_date = $value->preauthorisation_date;
				$preauthorisation_officer = $value->preauthorisation_officer;
				$member_number = $value->member_number;
				$scheme_name = $value->scheme_name;
				$visit_invoice_id = $value->visit_invoice_id;



				$array['preauth_date'] = $preauthorisation_date;
				$array['authorising_officer'] = $preauthorisation_officer;
				$array['member_number'] = $member_number;
				$array['scheme_name'] = $scheme_name;


				$this->db->where('visit_invoice_id',$visit_invoice_id);
				$this->db->update('visit_invoice',$array);

			}
		}
	}

	public function waiver_sidebar($visit_invoice_id,$visit_id,$balance,$patient_id)
	{
		 $data['accounts'] = $accounts = $this->purchases_model->get_all_accounts();
		$data['visit_invoice_id'] = $visit_invoice_id;
		$data['visit_id'] = $visit_id;
		$data['balance'] = $balance;
		$data['patient_id'] = $patient_id;

		$page = $this->load->view('reports/waiver_sidebar_view',$data);

		echo $page;

	}

	public function add_journal($visit_invoice_id,$visit_id,$patient_id)
	{


		$this->form_validation->set_rules('cancellation_date', 'Cancelling Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('write_off_description', 'Write off Description', 'trim|required|xss_clean');

		
		if ($this->form_validation->run())
		{
			$document_number = $this->debtors_model->create_visit_journal_number();
					
			$amount_paid = $journal_amount= $this->input->post('amount');
			// $patient_id = $this->input->post('patient_id');
			// $visit_id = $this->input->post('visit_id');
			$visit_invoice_id = $this->input->post('visit_invoice_id');


			$cancellation_date = $journal_date = $this->input->post('cancellation_date');
			$journal_reason = $this->input->post('write_off_description');



			$this->db->where('visit_invoice_id ='.$visit_invoice_id);

			$query_two = $this->db->get('visit_invoice');
			$visit_invoice_number = '';
			if($query_two->num_rows() > 0)
			{
				foreach ($query_two->result() as $key => $value_two) {
					// code...
					$visit_invoice_number = $value_two->visit_invoice_number;
					$bill_to = $value_two->bill_to;
					$patient_account_id = $value_two->patient_account_id;
				}
			}
			if(!empty($journal_reason))
			{
				$journal_insert_array['journal_reason'] = $journal_reason;

			}else
			{
				$journal_insert_array['journal_reason'] = $visit_invoice_number;
			}

			// var_dump($patient_account_id);die();
			$journal_insert_array['journal_date'] = $journal_date;
			$journal_insert_array['patient_id'] = $patient_id;
			$journal_insert_array['patient_account_id'] = $patient_account_id;
			$journal_insert_array['journal_amount'] = -$journal_amount;
			
			$journal_insert_array['account_from_id'] = $this->input->post('account_from_id');
			$journal_insert_array['account_to_id'] = $this->input->post('account_to_id');
			$journal_insert_array['visit_type_id'] = $bill_to;
			$journal_insert_array['visit_invoice_id'] = $visit_invoice_id;
			$journal_insert_array['patients_journal_number'] = $document_number;

			// var_dump($journal_insert_array);die();


			$this->db->insert('patients_journals',$journal_insert_array);
			$patient_journal_id = $this->db->insert_id();



			$this->accounts_model->update_invoice_totals($visit_invoice_id);
			$response['message'] ='success';
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			$response['message'] ='fail';
			$response['results'] =strip_tags(validation_errors());
		}

		echo json_encode($response);
		

	}

	public function add_waiver($visit_invoice_id,$visit_id,$patient_id)
	{


		$this->form_validation->set_rules('cancellation_date', 'Cancelling Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('write_off_description', 'Write off Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run())
		{
			$document_number = $this->accounts_model->create_visit_credit_note_number();
					
			$amount_paid = $this->input->post('amount');
			$cancellation_date = $this->input->post('cancellation_date');
			$reason = $this->input->post('write_off_description');



			$insertarray['patient_id'] = $patient_id;
			$insertarray['visit_id'] = $visit_id;
			$insertarray['created_by'] = $this->session->userdata('personnel_id');	

	    	$amount_paid = str_replace(",", "", $amount_paid);
			$amount_paid = str_replace(".00", "", $amount_paid);
			$insertarray['visit_cr_note_amount'] = $amount_paid;
			$insertarray['created'] = $cancellation_date;
			$insertarray['visit_invoice_id'] = $visit_invoice_id;
			$insertarray['visit_credit_note_type'] = 1;
	      // var_dump($insertarray);die();
	      	$this->db->insert('visit_credit_note', $insertarray);
	      

	        $visit_credit_note_id = $this->db->insert_id();

			$visit_data = array(
									'service_charge_id'=>2675,
									'visit_id'=>$visit_id,
									'patient_id'=>$patient_id,
									'visit_cr_note_amount'=>$amount_paid,
									'visit_cr_note_units'=>1,
									'created_by'=>$this->session->userdata('personnel_id'),
									'visit_cr_note_comment'=>$reason,
									'date'=>date("Y-m-d")
								);


			$visit_data['visit_credit_note_id'] = $visit_credit_note_id;
			$this->db->insert('visit_credit_note_item', $visit_data);

			$this->accounts_model->update_visit_invoice_data_credit($visit_invoice_id);
			$response['message'] ='success';
		}
		
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			$response['message'] =strip_tags(validation_errors());
		}

		echo json_encode($response);
		

	}

	public function view_debtor_dispatches($debtor_id,$visit_invoice_id)
	{
		$data['debtor_id'] = $debtor_id;
		$where = 'debtor_batches.visit_type_id = visit_type.visit_type_id AND debtor_batches.visit_type_id = '.$debtor_id;
		$table = 'debtor_batches,visit_type';

		$this->db->where($where);
		$this->db->select('visit_type.visit_type_name,debtor_batches.*');
		$query = $this->db->get($table);
		

		$data['query'] = $query;
		$data['debtor_id'] = $debtor_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		
		$page = $this->load->view('debtors/debtors/view_debtor_disptach',$data,true);
		// var_dump($page);die();
		echo $page;
	}

	public function allocate_invoice($debtor_batch_id,$visit_invoice_id)
	{

		$array_one['batch_id'] = $debtor_batch_id;
		$this->db->where('visit_invoice_id = '.$visit_invoice_id.' AND batch_id = 0');
		if($this->db->update('visit_invoice',$array_one))
		{

			$this->db->where('batch_id',$debtor_batch_id);
			$this->db->select('SUM(invoice_bill) AS total_invoices');
			$query = $this->db->get('visit_invoice');

			$this->db->where('batch_id',$debtor_batch_id);
			$this->db->select('*');
			$query_two = $this->db->get('visit_invoice');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...
					$total_invoices = $value->total_invoices;
				}
			}
			if(empty($total_invoices))
			{
				$total_invoices = 0;
			}
			$array_two['batch_amount'] = $total_invoices;
		    $array_two['total_invoice_count'] = $query_two->num_rows();

	        $this->db->where('debtor_batch_id',$debtor_batch_id);
	        $this->db->update('debtor_batches',$array_two);

		}

	}


	public function unallocate_invoice($debtor_batch_id,$visit_invoice_id)
	{	
		$array_one['batch_id'] = 0;
		$this->db->where('visit_invoice_id = '.$visit_invoice_id.' AND batch_id = '.$debtor_batch_id);
		if($this->db->update('visit_invoice',$array_one))
		{

			$this->db->where('batch_id',$debtor_batch_id);
			$this->db->select('SUM(invoice_bill) AS total_invoices');
			$query = $this->db->get('visit_invoice');

			$this->db->where('batch_id',$debtor_batch_id);
			$this->db->select('*');
			$query_two = $this->db->get('visit_invoice');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...
					$total_invoices = $value->total_invoices;
				}
			}
			if(empty($total_invoices))
			{
				$total_invoices = 0;
			}
			$array_two['batch_amount'] = $total_invoices;
		    $array_two['total_invoice_count'] = $query_two->num_rows();

	        $this->db->where('debtor_batch_id',$debtor_batch_id);
	        $this->db->update('debtor_batches',$array_two);

		}


			
	}
	public function get_allocated_invoices_data($id)
	{
		//var_dump($id);
		//die();

		$creditor_result = $this->debtors_model->get_unallocated_invoices($id);
		//var_dump($creditor_result);
		//die();
		// if($creditor_result->num_rows() > 0)
		// {
		// 	echo $creditor_result->result();
		// 	//var_dump($creditor_result->result());
		// 	//die();
		// }
		$data['creditor_result'] = $creditor_result;
		$data['debtor_id'] = $id;

		$page = $this->load->view('debtors/debtors/batched_invoices',$data,true);
		echo $page;
	}


	public function unprocessed_invoices()
    {
    	$data['title'] = 'Unprocessed  / Processed Invoices';
		$v_data['title'] = $data['title'];

		$this->session->set_userdata('supplier_invoice_search');
		$v_data['type'] = $this->reception_model->get_types();

		
		$v_data['query'] = '';
		$v_data['page'] = 0;
		$data['content'] = $this->load->view('debtors/accounting/invoices', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

    }


	public function unprocessed_bills()
    {
    	$data['title'] = 'Unprocessed  / Processed Bills';
		$v_data['title'] = $data['title'];
		$v_data['type'] = $this->reception_model->get_types();
		$this->session->set_userdata('supplier_invoice_search');

		
		$v_data['query'] = '';
		$v_data['page'] = 0;
		$data['content'] = $this->load->view('debtors/accounting/cash', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

    }

    public function get_all_invoices()
    {

    	$query = $this->debtors_model->get_all_uncleared_invoices();
    	$data['query'] = $query;

		$page = $this->load->view('debtors/accounting/patients_invoices_list',$data,true);


		$data['results'] = $page;
		$data['message'] = 'success';
		echo json_encode($data);

    }

    public function get_all_bills()
    {

    	$query = $this->debtors_model->get_all_uncleared_bills();
    	$data['query'] = $query;

		$page = $this->load->view('debtors/accounting/unprocessed_bills',$data,true);


		$data['results'] = $page;
		$data['message'] = 'success';
		echo json_encode($data);

    }
    

    public function view_invoice_details($invoice_id,$visit_id,$patient_id,$inpatient)
    {

    	
    		
    		
    	$data['visit_invoice_id'] = $invoice_id;
    	$data['patient_id'] = $patient_id;
    	$data['visit_id'] = $visit_id;
    	$data['inpatient'] = $inpatient;
		$page = $this->load->view('debtors/accounting/invoice_item_list',$data,true);


		$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($invoice_id);

		if($visit_invoice_detail->num_rows() > 0)
		{
			foreach ($visit_invoice_detail->result() as $key => $value) {
				# code...
				$invoice_number = $value->visit_invoice_number;
				$billing_status = $value->visit_invoice_status;
				$created = $value->created;
				$preauth_amount = $value->preauth_amount;
				$scheme_name = $value->scheme_name;
				$member_number = $value->member_number;
				$bill_to = $value->bill_to;
				$insurance_limit = $value->insurance_limit;
				$claim_number = $value->claim_number;
				$preauth_status = $value->preauth_status;
				$visit_invoice_status = $value->visit_invoice_status;
				$open_status = $value->open_status;
				$preauth = $value->preauth;
				$approved_amount = $value->approved_amount;
				

			}
		}


		$visit_rs = $this->accounts_model->get_visit_details($visit_id);
		$visit_type_id = 0;
		$close_card = 3;
		if($visit_rs->num_rows() > 0)
		{
			foreach ($visit_rs->result() as $key => $value) {
				# code...
				$close_card = $value->close_card;
			}
		}

    	

    	$creditor = $this->reception_model->get_patient_data($patient_id);
		$row = $creditor->row();
		$patient_surname = $row->patient_surname;
		$add = '';
		if($close_card != 1)
		{

			$add = '<a onclick="close_visit('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-correct"></i>Close Visit</a>';



		}
		else
		{
			$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
			if($authorize_invoice_changes)
			{
				$add = '<a onclick="open_visit('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')" class="btn btn-xs btn-success" ><i class="fa fa-correct"></i>Open Visit</a>';
			}
		}

		if($inpatient == 1)
		{
			$print = '<a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'/'.$invoice_id.'" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-print"></i> Print Invoice</a>';

			$open = '<a href="'.site_url().'accounts/payments/'.$patient_id.'/'.$visit_id.'" class="btn btn-xs btn-default"><i class="fa fa-arrow-right"></i>View</a>';

			
		}
		else
		{
			$print = '<a href="'.site_url().'print_invoice_new/'.$visit_id.'/'.$invoice_id.'" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-print"></i> Print Invoice</a>';
			$open = '<a href="'.site_url().'receipt-payment/'.$patient_id.'/0/'.$visit_id.'" class="btn btn-xs btn-default"><i class="fa fa-arrow-right"></i>View</a>';
		}

		if($visit_invoice_status == 0)
		{
			$add .= ' <a onclick="close_invoice('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')" class="btn btn-xs btn-warning" ><i class="fa fa-correct"></i>Close Invoice</a>';
		}
		else
		{
			$add .= ' <a onclick="close_invoice('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')" class="btn btn-xs btn-success"><i class="fa fa-correct"></i>Open Invoice</a>';
		}

    	
    	$data['header'] = 'Name: <span style="font-weight: normal;">'.$patient_surname.' Invoice No.: <span style="font-weight: normal;">'.$invoice_number.'  '.$print.'</span> 
    		 '.$open.'
    	';

		$data['results'] = $page;
		$data['message'] = 'success';
		echo json_encode($data);

    }


     public function view_visit_bills_details($visit_id,$patient_id,$inpatient)
    {

    	
    		$invoice_id = 1;
    	$data['patient_id'] = $patient_id;

    	$data['visit_id'] = $visit_id;
    	$data['inpatient'] = $inpatient;
		$page = $this->load->view('debtors/accounting/view_unprocessed_bills',$data,true);


		


		$visit_rs = $this->accounts_model->get_visit_details($visit_id);
		$visit_type_id = 0;
		$close_card = 3;
		if($visit_rs->num_rows() > 0)
		{
			foreach ($visit_rs->result() as $key => $value) {
				# code...
				$close_card = $value->close_card;
				$visit_date = $value->visit_date;
			}
		}

    	

    	$creditor = $this->reception_model->get_patient_data($patient_id);
		$row = $creditor->row();
		$patient_surname = $row->patient_surname;
		$add = '';
		if($close_card != 1)
		{

			// $add = '<a onclick="close_visit('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-correct"></i>Close Visit</a>';



		}
		else
		{
			$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
			if($authorize_invoice_changes)
			{
				// $add = '<a onclick="open_visit('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')" class="btn btn-xs btn-success" ><i class="fa fa-correct"></i>Open Visit</a>';
			}
		}

		if($inpatient == 1)
		{
			

			$open = '<a href="'.site_url().'accounts/payments/'.$patient_id.'/'.$visit_id.'" class="btn btn-xs btn-default"><i class="fa fa-arrow-right"></i>View</a>';

			
		}
		else
		{
			
			$open = '<a href="'.site_url().'receipt-payment/'.$patient_id.'/0/'.$visit_id.'" class="btn btn-xs btn-default"><i class="fa fa-arrow-right"></i>View</a>';
		}

		

    	
    	$data['header'] = 'Name: <span style="font-weight: normal;">'.$patient_surname.' Visit Date.: <span style="font-weight: normal;">'.$visit_date.' </span> 
    		 '.$open.'
    	';

		$data['results'] = $page;
		$data['message'] = 'success';
		echo json_encode($data);

    }

	public function paid_invoices_searched($insurance_id,$batch_receipt_id)
	{
		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;

		$patient_surname = $this->input->post('patient_surname');
        $invoice_number = $this->input->post('invoice_number');

        $add = '';
        if(!empty($patient_surname))
        {
            // $search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
            $surnames = explode(" ",$_POST['patient_surname']);
            $total = count($surnames);
            
            $count = 1;
            $add .= ' AND (';
            for($r = 0; $r < $total; $r++)
            {
                if($count == $total)
                {
                    $add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\')';
                }
                
                else
                {
                    $add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
                }
                $count++;
            }
            $add .= ') ';
        }


      

        if(!empty($invoice_number))
        {
            $add .= ' AND visit_invoice.visit_invoice_number LIKE \'%'.$invoice_number.'%\'';
        }

        

		$where = 'payments.batch_receipt_id = '.$batch_receipt_id.' AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payments.payment_id = payment_item.payment_id AND payments.cancel = 0 AND patients.patient_id = visit_invoice.patient_id AND visit_invoice.bill_to = '.$insurance_id.$add;
        
        $table = 'visit_invoice,payment_item,payments,patients';
        
    
        $query = $this->debtors_model->get_paid_invoices($table, $where);

        $v_data['query'] = $query;

		// $v_data['title'] = 'Batch Items';
		// $data['title'] = 'Batch Items';
		// $data['message']='success';

		$v_data['insurance_id'] = $insurance_id;
		$v_data['batch_receipt_id'] = $batch_receipt_id;
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['message']='success';
		$data['result'] = $this->load->view('remittances/paid_invoices', $v_data,true);
		
		echo json_encode($data);
		// $data['result'] = $this->load->view('remittances/selected_invocies', $v_data,true);
		
		// echo json_encode($data);
	}


}
?>