<?php
class debtors_model extends CI_Model 
{

	public function get_all_visits_view($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patients.*');
		$this->db->where($where);
		$this->db->order_by('patients.patient_id','DESC');
		$this->db->group_by('patients.patient_id');
		// $this->db->join('visit_invoice','visit_invoice.patient_id = patients.patient_id','LEFT');
		// $this->db->join('personnel','visit.personnel_id = personnel.personnel_id','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	function export_debtors()
	{
		$this->load->library('excel');
		
		
		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id';
		
		$table = 'v_transactions_by_date,visit_invoice';
		$visit_search = $this->session->userdata('debtors_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		}
		

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
			// $where .= $visit_search;
		
		}
		
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);

		$visits_query = $this->db->get($table);
		
		$title = 'Transactions Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient No.';
			$col_count++;
			$report[$row_count][$col_count] = 'Patient';
			$col_count++;
			$report[$row_count][$col_count] = 'Category';
			$col_count++;
			$report[$row_count][$col_count] = 'Doctor';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice No.';
			$col_count++;
			$report[$row_count][$col_count] = 'Branch Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Payments';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;	
			//display all patient data in the leftmost columns
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			foreach ($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$doctor = $row->personnel_fname;
				$count++;
				$invoice_total = $row->dr_amount;
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;


				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $visit_date;
				$col_count++;
				$report[$row_count][$col_count] = $patient_number;
				$col_count++;
				$report[$row_count][$col_count] = ucwords(strtolower($patient_surname));
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = $doctor;
				$col_count++;
				$report[$row_count][$col_count] = $visit_invoice_number;
				$col_count++;
				$report[$row_count][$col_count] = $branch_code;
				$col_count++;
				$report[$row_count][$col_count] = number_format($invoice_total,2);
				$col_count++;
				$report[$row_count][$col_count] = (number_format($payments_value,2));
				$col_count++;
				$report[$row_count][$col_count] = (number_format($balance,2));
				$col_count++;				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_debtors($table, $where, $per_page, $page, $order = 'alias', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->join('visit_type_account','visit_type.visit_type_id = visit_type_account.visit_type','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	public function update_debtor_account($visit_type_id)
	{
		$account = array(
			'visit_type'=>$visit_type_id,//$this->input->post('account_to_id'),
			'opening_balance'=>$this->input->post('opening_balance'),
			'debit_id'=>$this->input->post('debit_id'),
			'created'=>$this->input->post('start_date')
			);

		// check if it exists

		$this->db->where('visit_type',$visit_type_id);
		$query = $this->db->get('visit_type_account');

		if($query->num_rows() > 0)
		{
			// update
			$this->db->where('visit_type',$visit_type_id);
			if($this->db->update('visit_type_account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			// insert
			if($this->db->insert('visit_type_account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		// var_dump($account); die();
		
		
	}

	

	public function get_unallocated_invoices($visit_type_id)
	{
		 $patient_surname = $this->input->post('patient_surname');
  		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$invoice_number = $this->input->post('invoice_number');

		$add = '';
		if(!empty($patient_surname))
		{
			// $search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['patient_surname']);
			$total = count($surnames);
			
			$count = 1;
			$add .= ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$add .= ') ';
		}


		if(!empty($date_from) AND !empty($date_to) )
		{
			$add .= ' AND visit_invoice.created >= "'.$date_from.'" AND visit_invoice.created <= "'.$date_to.'"';
		}
		else if(empty($date_from) AND !empty($date_to))
		{
			$add .= ' AND visit_invoice.created = "'.$date_to.'"';
		}
		else if(!empty($date_from) AND empty($date_to))
		{
			$add .= ' AND visit_invoice.created = "'.$date_from.'"';
		}

		if(!empty($invoice_number)){

			$add = ' AND  visit_invoice.visit_invoice_number LIKE \'%'.addslashes($invoice_number[$r]).'%\'';
		}





	

		$this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_invoice.visit_invoice_status <> 1 AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_invoice.batch_id = 0 AND visit_invoice.bill_to = '.$visit_type_id.$add);
		$this->db->select('visit_invoice.*,SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_amount,patients.*,visit_invoice.created as invoice_date');
		$this->db->order_by('visit_invoice.created','ASC');
		$this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_invoice,visit_charge,patients');


		

		return $query;
	}

	public function create_debtor_batch($visit_type_id)
	{
		$batch_number = $this->input->post('batch_number');
		$batch_amount = $this->input->post('batch_amount');
		// var_dump($batch_amount);die();
		// $total_visits = sizeof($_POST['creditor_invoice_items']);
		// $visit = $_POST['creditor_invoice_items'];
		// var_dump($visit);die();
		$branch_code = $this->session->userdata('branch_code');

		$suffix = str_replace($branch_code.'-BAT', '', $batch_number);

		$array['suffix'] = $suffix;
		$array['batch_number'] = $batch_number;
		$array['created_by'] = $this->session->userdata('personnel_id');
		$array['batch_amount'] = $batch_amount;
		$array['created'] = date('Y-m-d');
		$array['visit_type_id'] = $visit_type_id;
		$array['batch_status'] = 1;
		// var_dump($array);die();
		if($this->db->insert('debtor_batches',$array))
		{
			$debtor_batch_id = $this->db->insert_id();


			// 

			$total_visits = sizeof($_POST['creditor_invoice_items']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['creditor_invoice_items'];
	            $visit_invoice_id = $visit[$r];
	            //check if card is held
	            if($r == 0)
	            {
	            	$visit_start_date = $this->get_visit_invoice_date($visit_invoice_id);
	            }

	            if($r == ($total_visits-1))
	            {
	            	$visit_end_date = $this->get_visit_invoice_date($visit_invoice_id);

	            }
	            // echo $r.' '.$total_visits.' '.$visit_end_date.'<br>';
	            $service = array(
	                      'batch_id'=>$debtor_batch_id,
	                    );
	            $this->db->where('visit_invoice_id',$visit_invoice_id);
	            $this->db->update('visit_invoice',$service);

	            
	          }
	        }

	        // update debtor_invoice

	        $array_two['batch_date_from'] = $visit_start_date;
	        $array_two['batch_date_to'] = $visit_end_date;
	        $array_two['total_invoice_count'] = $total_visits;

	        $this->db->where('debtor_batch_id',$debtor_batch_id);
	        $this->db->update('debtor_batches',$array_two);

	        return TRUE;
		}
		else
		{
			return FALSE;
		}
		// die();
	}
	public function get_visit_invoice_date($visit_invoice_id)
	{
		$this->db->where('visit_invoice_id',$visit_invoice_id);
		$this->db->from('visit_invoice');
		$this->db->select('created');
		// $this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
		  $result = $query->result();
		  $created =  $result[0]->created;
		}

		return $created;
	}
	public function create_batch_number()
	{
	  //select product code
		$branch_code = $this->session->userdata('branch_code');

		$this->db->where('debtor_batch_id > 0');
		$this->db->from('debtor_batches');
		$this->db->select('MAX(suffix) AS number');
		$this->db->order_by('suffix','ASC');
		// $this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
		  $result = $query->result();
		  $number =  $result[0]->number;

		   // var_dump($number); die();

		   
		    $number++;
		    

		 
		  
		}
		else{//start generating rece$number= 1;ipt numbers
		  
		 
		      $number = 1;
		    
		}

		// var_dump($number); die();
		return $branch_code.'-BAT'.$number;
	}

	public function get_debtor_batches($visit_type_id)
	{

		$this->db->where('visit_type_id',$visit_type_id);
		$this->db->from('debtor_batches');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;

	}
	public function get_allocated_invoices($debtor_batch_id)
	{


		$this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_charge.visit_charge_delete = 0  AND visit_invoice.visit_invoice_delete = 0  AND visit_charge.charged = 1 AND visit_invoice.batch_id = '.$debtor_batch_id);
		$this->db->select('visit_invoice.*,SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_amount,patients.*,visit_invoice.created as invoice_date');
		$this->db->order_by('visit_invoice.created','ASC');
		$this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_invoice,visit_charge,patients');


		// $this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_invoice.batch_id = '.$debtor_batch_id);
		// $this->db->select('visit_invoice.*,patients.*,visit_invoice.created as invoice_date');
		// $this->db->order_by('visit_invoice.created','ASC');
		// $query = $this->db->get('visit_invoice,patients');

		return $query;
	}
	public function get_receivables_aging_report()
	{
		$search = $this->session->userdata('search_debtors_batches');	
		$add_search = '';
		// if(!empty($search))
		// {
		// 	$add_search = $search;
		// }
		//retrieve all users
		$this->db->where('visit_type.visit_type_id > 0'.$add_search);
		$this->db->from('visit_type');
		$this->db->select('*');
		$this->db->join('visit_type_account','visit_type.visit_type_id = visit_type_account.visit_type','LEFT');
		$this->db->order_by('visit_type.visit_type_name','ASC');
		$query = $this->db->get();

		return $query;
	}

	public function get_total_invoices($bill_to,$status,$batch_start_date=NULL)
	{

			// var_dump($status)
		if($status == 0)
		{
			$add = ' AND visit_invoice.batch_id = 0';
		}
		else
		{
			$add = ' AND visit_invoice.batch_id > 0';
		}

		if(!empty($batch_start_date))
		{
			$add .= ' AND visit_invoice.created >= "'.$batch_start_date.'" ';
		}

		$this->db->where('visit_invoice.patient_id = patients.patient_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1  AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.bill_to = '.$bill_to.$add);
		$this->db->select('SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_amount');
		// $this->db->order_by('visit_invoice.created','ASC');
		// $this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_invoice,visit_charge,patients');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}

	public function get_total_payment_batches($bill_to)
	{

		
		$this->db->where('batch_receipts.insurance_id = '.$bill_to);
		$this->db->select('SUM(total_amount_paid) AS total_amount');
		$query = $this->db->get('batch_receipts');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}


	public function get_total_unallocated_payment($bill_to)
	{

		
		$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.batch_receipt_delete = 0 AND batch_receipts.insurance_id = '.$bill_to);
		$this->db->select('SUM(batch_unallocations.amount_paid) AS total_amount');
		$query = $this->db->get('batch_unallocations,batch_receipts');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}




	// remmitances 

	public function get_transacting_accounts($parent_account_name,$type=null)
	{
	  $branch_session = $this->session->userdata('branch_id');
	  $personnel_id = $this->session->userdata('personnel_id');

	  $branch_add = '';
	  // if($branch_session > 0)
	  // {
	  //   $branch_add = ' AND (branch_id = '.$branch_session.')';
	  // }
	  $this->db->from('account');
	  $this->db->select('*');
	  $this->db->where('(parent_account = 2 OR parent_account =19) AND paying_account = 0'.$branch_add);
	  $query = $this->db->get();     

	  return $query;     

	}
	public function get_receipt_amount($batch_receipt_id)
	{

		$this->db->from('batch_payments');
		$this->db->select('SUM(amount) AS total_amount_paid');
		$this->db->where('batch_receipt_id',$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		return $total_amount_paid;

	}

	public function get_receipt_amount_paid($batch_receipt_id)
	{
		$this->db->from('payments,batch_receipts');
		$this->db->select('SUM(amount_paid) AS total_amount_paid');
		$this->db->where('batch_receipts.batch_receipt_id = payments.batch_receipt_id AND payments.cancel = 0 AND batch_receipts.current_payment_status = 1 AND payments.batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get();  
		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		return $total_amount_paid;
	}

	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_batch_receipts_payments($table, $where, $per_page, $page, $order = 'batch_payments.invoice_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('batch_payments.*,visit_invoice.visit_invoice_number,payments.confirm_number,account.account_name');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->join('payments','batch_payments.payment_id = payments.payment_id','LEFT');
		$this->db->join('payment_item','payment_item.payment_id = payments.payment_id','LEFT');
		$this->db->join('visit_invoice','visit_invoice.visit_invoice_id = payment_item.visit_invoice_id','LEFT');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	
	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_current_payments($table, $where, $per_page, $page, $order = 'batch_receipts.batch_receipt_id', $order_method = 'DESC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->group_by('receipt_number');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function confirm_batch_payments($batch_receipt_id)
	{
			$this->db->where('batch_receipt_id',$batch_receipt_id);
			$query_items = $this->db->get('batch_receipts');

			if($query_items->num_rows() > 0)
			{
				foreach ($query_items->result() as $key => $value) {
					// code...

					$receipt_number = $value->receipt_number;
				}
			}		
			// $receipt_number = $this->input->post('receipt_number');
			$payment_date = $this->input->post('payment_date');
			$bank_id = $this->input->post('bank_id');
			$batch_receipt_id = $this->input->post('batch_receipt_id');
			$payment_method = 9;




			$total_visits = sizeof($_POST['visit_invoices']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['visit_invoices'];
	            $visit_invoice_id = $visit[$r];
	            //check if card is held
	            
	            $amount_paid = $this->input->post('amount_paid'.$visit_invoice_id);
	            $patient_id = $this->input->post('patient_id'.$visit_invoice_id);


	            if($amount_paid > 0)
	            {
		            $data = array(
								'patient_id' => $patient_id,
								'payment_method_id'=>9,
								'amount_paid'=>$amount_paid,
								'personnel_id'=>$this->session->userdata("personnel_id"),
								'payment_type'=>1,
								'transaction_code'=>$receipt_number,
								'reason'=>'Insurance Payment',
								'payment_service_id'=>1,
								'change'=>0,
								'payment_date'=>$payment_date,
								'payment_created_by'=>$this->session->userdata("personnel_id"),
								'approved_by'=>$this->session->userdata("personnel_id"),
								'date_approved'=>date('Y-m-d'),
								'bank_id'=>$bank_id,
								'batch_receipt_id'=>$batch_receipt_id
							);
		            
					// $prefix = $suffix = $this->accounts_model->create_receipt_number();
					// var_dump($prefix);die();
					$branch_code = $this->session->userdata('branch_code');
					$branch_id = $this->session->userdata('branch_id');

					// $branch_code = str_replace("IDC", "CT", $branch_code);


					// $prefix = 'REM';
					// $suffix = $this->accounts_model->create_visit_receipt_number($prefix);
					// if($suffix < 10)
					// {
					// 	$number = '0000'.$suffix;
					// }
					// else if($suffix < 100 AND $suffix >= 10)
					// {
					// 	$number = '000'.$suffix;
					// }
					// else if($suffix < 1000 AND $suffix >= 100)
					// {
					// 	$number = '00'.$suffix;
					// }
					// else if($suffix < 10000 AND $suffix >= 1000)
					// {
					// 	$number = '0'.$suffix;
					// }
					
					// $data['branch_id'] = $branch_id;
					// $data['prefix'] = $prefix;
					// $data['suffix'] = $suffix;
					$data['confirm_number'] = $receipt_number;

					

					if($this->db->insert('payments', $data))
					{
						$payment_id = $this->db->insert_id();


						 $service = array(
							              'visit_invoice_id'=>$visit_invoice_id,
							              'invoice_type'=>1,
							              'patient_id' => $patient_id,
							              'created_by' => $this->session->userdata('personnel_id'),
							              'created' => $payment_date,
							              'payment_item_amount'=>$amount_paid,
							              'payment_id'=>$payment_id
							              // 'payment_id'=>NULL
							            );
					    $this->db->insert('payment_item',$service);


					    $this->accounts_model->update_invoice_totals($visit_invoice_id);
					}
	          	}
	        }
			
			return TRUE;
		}
	}

	public function get_all_unallocated_batch_payments($batch_receipt_id)
	{
		$this->db->where('unallocated_payment_delete = 0 AND batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get('batch_unallocations');

		return $query;
	}

	public function get_all_unallocated_payments($batch_receipt_id)
	{
		$this->db->select('SUM(amount_paid) AS total_amount');
		$this->db->where('unallocated_payment_delete = 0 AND batch_receipt_id = '.$batch_receipt_id);
		$query = $this->db->get('batch_unallocations');

		$total_amount = 0;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}
	public function get_all_unpaid_invoices($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		// $this->db->order_by('visit_invoice.dentist_id','DESC');
		$query = $this->db->get('');
		
		return $query;
		
	}

	public function get_paid_invoices($table, $where,$group_by=NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit_invoice.created AS invoice_date,payments.*,payment_item.payment_item_amount,patients.patient_surname,patients.patient_othernames,visit_invoice.visit_invoice_number');
		$this->db->where($where);
		// $this->db->join('visit_charge','visit_invoice.visit_invoice_id','DESC');
		$query = $this->db->get('');
		
		return $query;
		
	}


	public function get_personnel_name($personnel_id)
	{
		//retrieve all users


		$this->db->from('personnel');
		$this->db->select('*');
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get();

		$names = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$personnel_fname = $value->personnel_fname;
				$personnel_onames = $value->personnel_onames;
				$names = $personnel_fname.' '.$personnel_onames;
			}
		}

		if($personnel_id == 0)
		{
			$names = 'Administrator';
		}
		
		return $names;
	}

	public function upload_any_file($path, $location, $name, $upload, $edit = NULL)
	{
		if(!empty($_FILES[$upload]['tmp_name']))
		{
			$image = $this->session->userdata($name);
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				
				//delete any other uploaded image
				if($this->file_model->delete_file($path."\\".$image, $location))
				{
					//delete any other uploaded thumbnail
					$this->file_model->delete_file($path."\\thumbnail_".$image, $location);
				}
				
				else
				{
					$this->file_model->delete_file($path."/".$image, $location);
					$this->file_model->delete_file($path."/thumbnail_".$image, $location);
				}
			}
			//Upload image
			$response = $this->file_model->upload_any_file($path, $upload);
			if($response['check'])
			{
				$file_name = $response['file_name'];
					
				//Set sessions for the image details
				$this->session->set_userdata($name, $file_name);
			
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('upload_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('upload_error_message', '');
			return FALSE;
		}
	}
	public function upload_batched_documents($document,$debtor_batch_id)
	{
		$array['document_name'] = $document;
		$array['received_by'] = $this->input->post('received_by');
		$array['received_date'] = $this->input->post('received_date');

		$this->db->where('debtor_batch_id',$debtor_batch_id);
		if($this->db->update('debtor_batches',$array))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_debtor_statement_value($visit_type_id,$date,$checked)
	{
		// invoices
		$invoice = '';
		$start_date = '2014-03-01';
		$first_date = date('Y-m').'-01';
		if($checked == 1)
		{
			// 30 days
			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date >= "'.$first_date.'" AND visit.visit_date <= "'.$date.'" ';
		
		}
		else if($checked == 2)
		{
			// 30 days
			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-1 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			// $last_date = date('Y-m-d', strtotime('-2 months'));
			// var_dump($last_date); die();
			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
		else if($checked == 3)
		{
			// 60 days
			// var_dump($checked); die();
			// 30 days
			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-2 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));


			$invoice = ' AND visit.visit_date >= "'.$start_date.'"AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
		}

		else if($checked == 4)
		{
			// over 90 days

			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-3 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			$invoice = ' AND visit.visit_date >= "'.$start_date.'"AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
			// $invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
		else if($checked == 5)
		{
			// over 120 days

			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-4 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date >= "'.$sixty_months.'" AND visit.visit_date <= "'.$last_date.'" ';
			// $invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
		else if($checked == 6)
		{
			// over 120 days

			$sixty_months = date('Y-m-01');
			$sixty_months = date('Y-m-d',strtotime ( '-5 month' , strtotime ( $sixty_months ) ) );
			// var_dump($sixty_months); die();
			$newdate = date('Y-m-t',strtotime ( '+0 month' , strtotime ( $sixty_months ) ) );
			$last_date = date('Y-m-t', strtotime($newdate));

			// var_dump($last_date); die();

			$invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
			// $invoice = ' AND visit.visit_date >= "'.$start_date.'" AND visit.visit_date <= "'.$last_date.'" ';
		}
	

		$this->db->where('visit_charge.visit_charge_delete = 0 AND (visit.parent_visit IS NULL OR visit.parent_visit = 0) AND visit.visit_id = visit_charge.visit_id AND visit.visit_delete = 0 AND visit_charge.charged = 1 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(visit_charge_amount*visit_charge_units) AS total_invoice');
		$query = $this->db->get('visit_charge,visit');
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}


		$this->db->where('visit.visit_id = visit_bill.visit_parent AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_delete = 0 AND visit_bill.visit_type_id = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(visit_bill_amount) AS total_invoice');
		$query = $this->db->get('visit_bill,visit');
		$total_rejected_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_rejected_invoice = $value->total_invoice;
			}
		}




		$this->db->where('(visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_delete = 0 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(rejected_amount) AS total_invoice');
		$rejected = $this->db->get('visit');
		$rejections = 0;
		if($rejected->num_rows() > 0)
		{
			foreach ($rejected->result() as $key => $value) {
				# code...
				$rejections = $value->total_invoice;
			}
		}

		$total_rejected_amount += $rejections;

		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 2 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query_waiver = $this->db->get('payments,visit');
		$total_waiver = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $value) {
				# code...
				$total_waiver = $value->total_payments;
			}
		}




		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 1 AND visit.visit_type = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query_payments = $this->db->get('payments,visit');
		$total_payments = 0;
		if($query_payments->num_rows() > 0)
		{
			foreach ($query_payments->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		
		$amount = ($total_invoice) - ($total_payments + $total_waiver);
		// if($visit_type_id == 1 AND $checked == 2)
		// {
		// 	var_dump($total_invoice); die();
		// }
		// if($amount < 0)
		// {
		// 	$amount = -$amount;
		// }

		return $amount;

	}

	public function get_debtor_total_payments($visit_type_id)
	{
		$start_date = date('2014-03-01');
		$invoice = ' AND visit_invoice.created >= "'.$start_date.'"';

		$this->db->where('payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND payments.payment_type = 1 AND visit_invoice.bill_to = '.$visit_type_id.' '.$invoice);
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$query_payments = $this->db->get('payments,payment_item,visit_invoice');
		$total_payments = 0;
		if($query_payments->num_rows() > 0)
		{
			foreach ($query_payments->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}

		return $total_payments;

	}

	public function get_debtor_statement($visit_type_id)
	{
		$creditor_query = $this->get_opening_debtor_balance($visit_type_id);
		// $bills = $this->get_all_provider_invoices($visit_type_id);
		$all_collections = $this->get_all_provider_work_done($visit_type_id);
		// var_dump($all_collections); die();
		// $payments = $this->get_all_payments_provider($visit_type_id);

		// $brought_forward_balance = $this->get_provider_balance_brought_forward($visit_type_id);

		// var_dump($creditor_query);die();


		$x=0;

		$bills_result = '';
		$last_date = '';
		$visit_last_date = '';
		$current_year = date('Y');
		// $total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;


		$opening_balance = 0;

		$total_invoice_amount = 0;
			$total_transfer_amount = 0;
			$total_credit_amount = 0;
			$total_bill_amount = 0;
			$total_payment_amount = 0;
			$total_rejected_amount = 0;
			$total_arrears_amount = 0;

		$opening_date = date('Y-m-d');
		$debit_id = 2;
		// var_dump($creditor_query->num_rows()); die();
		if($creditor_query->num_rows() > 0)
		{
			$row = $creditor_query->row();
			$opening_balance = $row->opening_balance;
			$opening_date = $row->created;
			$debit_id = $row->debit_id;
			// var_dump($debit_id); die();
			if($debit_id == 2)
			{
				// this is deniget_all_provider_credit_month
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td colspan=5>Opening Balance</td>
									<td>'.number_format($opening_balance, 2).'</td>
								</tr> 
							';
				$total_arrears_amount += $opening_balance;

			}
			else
			{
				// this is a prepayment
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td colspan=5>Opening Balance</td>
									<td>'.number_format($opening_balance, 2).'</td>
								</tr> 
							';
				$total_payment_amount = $opening_balance;
				$total_arrears_amount -= $opening_balance;
			}
		}
		

		if($brought_forward_balance == FALSE)
		{
			$result .='';
		}

		else
		{
			$search_title = $this->session->userdata('creditor_search_title');
			if($brought_forward_balance < 0)
			{
				$positive = -$brought_forward_balance;
				$result .= 
							'
								<tr>
									<td colspan=5> B/F</td>
									<td>'.number_format($positive, 2).'</td>
									<td></td>
								</tr> 
							';
				$total_invoice_balance += $positive;

			}
			else
			{
				$result .= 
							'
								<tr>
									<td colspan=6> B/F</td>
									<td></td>
									<td>'.number_format($brought_forward_balance, 2).'</td>
								</tr> 
							';


				$total_invoice_balance += $brought_forward_balance;
			}
		}
		if($all_collections->num_rows() > 0)
		{
			
			foreach ($all_collections->result() as $collections_key) {
				# code...
				$visit_date = $collections_key->visit_date;
				$bill_explode = explode('-', $visit_date);
				$billing_year = $bill_explode[0];
				$billing_month = $bill_explode[1];
				$start_date = $billing_year.'-'.$billing_month.'-01';

				$end_date =  date("Y-m-t", strtotime($start_date));
				$invoice_amount = $this->get_total_invoice_collection($visit_type_id,$start_date,$end_date,$week);
			
				$payments = $this->get_all_payments_debtor_monthly($visit_type_id,$start_date,$end_date,$week);
				$credit = $this->get_all_provider_waiver_month($visit_type_id,$start_date,$end_date,$week);
				// $total_payment_amount += $payments;

				// var_dump($invoice_amount);die();

				$total_bill = ($invoice_amount) - $credit;

				$total_invoice_amount += $invoice_amount;
				$total_waiver_amount += $credit;
				$total_rejected_amount += $rejected_amount;
				$total_payment_amount += $payments;
				$total_bill_amount += $total_bill;
				$total_arrears = $total_bill - $payments - $rejected_amount;
				$total_arrears_amount += $total_arrears;
				
					$result .= 
					'
						<tr>
							<td>'.date('M Y',strtotime($visit_date)).' Invoice </td>
							<td>'.number_format($invoice_amount - $credit, 2).'</td>
							<td>'.number_format($rejected_amount, 2).'</td>
							<td>('.number_format($credit, 2).')</td>
							<td>'.number_format($total_bill, 2).'</td>
							<td>('.number_format($payments, 2).')</td>
							<td>'.number_format($total_arrears, 2).'</td>
							<td><a href="'.site_url().'export-debtor-invoices/'.$visit_type_id.'/'.$start_date.'/'.$end_date.'"  class="btn btn-xs btn-success" >export invoices</a></td>
						</tr> 
					';
				
				$total_invoice_balance += $amount_value;

					
				$visit_last_date = $end_month;
			}

			$result .= 
					'
						<tr>
							<td><strong>Total Amount</strong> </td>
							<td><strong>'.number_format($total_invoice_amount, 2).'</strong></td>
							<td><strong>'.number_format($total_rejected_amount, 2).'</strong></td>
							<td><strong>('.number_format($total_credit_amount, 2).')</strong></td>
							<td><strong>'.number_format($total_bill_amount, 2).'</strong></td>
							<td><strong>('.number_format($total_payment_amount, 2).')</strong></td>
							<td><strong>'.number_format($total_arrears_amount, 2).'</strong></td>
						</tr> 
					';
		}

		
		
	



		$response['total_arrears'] = $total_arrears;
		$response['total_invoice_balance'] = $total_invoice_balance;
		$response['invoice_date'] = $invoice_date;
		$response['opening_balance'] = $opening_balance;
		$response['opening_date'] = $opening_date;
		$response['debit_id'] = $debit_id;
		$response['result'] = $result;
		$response['total_payment_amount'] = $total_payment_amount;

		// var_dump($response); die();

		return $response;
	}

	public function get_opening_debtor_balance($visit_type_id)
	{
		$this->db->select('*'); 
		$this->db->where('visit_type = '.$visit_type_id.'' );
		$query = $this->db->get('visit_type_account');
		
		return $query;
	}

	public function get_all_provider_work_done($visit_type)
	{
		$search = $this->session->userdata('provider_invoice_search');

		if(!empty($search))
		{
			$invoice_search = $search;
		}
		else
		{
			$invoice_search = '';
		}
		$start_date = date('2014-03-01');
		$invoice = ' AND visit_invoice.created >= "'.$start_date.'"';
		
		$this->db->from('visit_invoice,visit');
		$this->db->select('created AS visit_date');
		$this->db->where('visit.visit_delete = 0 AND visit.visit_id = visit_invoice.visit_id AND visit_invoice.visit_invoice_delete = 0  AND visit_invoice.bill_to = '.$visit_type.''.$invoice);
		$this->db->order_by('YEAR(visit_invoice.created),MONTH(visit_invoice.created)','ASC');
		$this->db->group_by('YEAR(visit_invoice.created),MONTH(visit_invoice.created)');
		$query = $this->db->get();
		return $query;
	}

		public function get_total_invoice_collection($visit_type_id,$start_date,$end_date,$week)
	{
		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$end_date.'\'';
		}
		$start_date = date('2014-03-01');
		$invoice = ' AND visit.visit_date >= "'.$start_date.'"';
		$this->db->from('visit,visit_charge,visit_invoice');
		$this->db->select('SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_charged_amount');
		$this->db->where('visit.visit_id = visit_invoice.visit_id AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0  AND visit.visit_delete = 0 AND visit_invoice.bill_to = '.$visit_type_id.''.$invoice.''.$search_add);
		$query = $this->db->get();
		$total_charged_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_charged_amount = $value->total_charged_amount;
			}
		}
		// var_dump($total_charged_amount); die();
		return $total_charged_amount;
	}

	public function get_all_payments_debtor_monthly($visit_type,$start_date,$end_date,$payment_week)
	{
		$search = $this->session->userdata('provider_payment_search');

		if(!empty($search))
		{
			$payment_search = $search;
		}
		else
		{
			$payment_search = '';
		}

		$date_from = $this->session->userdata('providers_date_from');
		$date_to = $this->session->userdata('providers_date_to');

		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$end_date.'\'';
		}
		$start_date = date('2014-03-01');
			$invoice = ' AND visit_invoice.created >= "'.$start_date.'"';

		
		$this->db->from('visit_invoice,payments,payment_item');
		$this->db->select('SUM(payment_item.payment_item_amount) AS total_payments');
		$this->db->where('visit_invoice.visit_invoice_delete = 0 AND payments.payment_id = payment_item.payment_id AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id  AND payments.payment_type = 1 AND payments.cancel = 0 AND visit_invoice.bill_to = '.$visit_type.$search_add.$invoice.'');
		$waiver_query = $this->db->get('');
		$total_amount = 0;
		if($waiver_query->num_rows() > 0)
		{
			foreach ($waiver_query->result() as $key => $value) {
				# code...
				$total_amount =$value->total_payments;

			}
		}
		// var_dump($total_amount); die();
		return $total_amount;
	}
	public function get_all_provider_waiver_month($visit_type,$start_date,$end_date,$payment_week)
	{
		$search = $this->session->userdata('provider_payment_search');

		if(!empty($search))
		{
			$payment_search = $search;
		}
		else
		{
			$payment_search = '';
		}

		$date_from = $this->session->userdata('providers_date_from');
		$date_to = $this->session->userdata('providers_date_to');

		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_invoice.created >= \''.$start_date.'\' AND visit_invoice.created <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_invoice.created = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_invoice.created = \''.$end_date.'\'';
		}


		
		$this->db->from('visit_invoice,visit_credit_note,visit_credit_note_item');
		$this->db->select('SUM(visit_credit_note_item.visit_cr_note_amount) AS total_payments');
		$this->db->where('visit_invoice.visit_invoice_delete = 0  AND visit_credit_note.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_credit_note.visit_credit_note_id = visit_credit_note_item.visit_credit_note_id AND visit_credit_note.visit_cr_note_delete = 0  AND visit_invoice.bill_to = '.$visit_type.$search_payment_add.'');
		$waiver_query = $this->db->get('');
		$total_amount = 0;
		if($waiver_query->num_rows() > 0)
		{
			foreach ($waiver_query->result() as $key => $value) {
				# code...
				$total_amount =$value->total_payments;

			}
		}

		return $total_amount;
	}
	public function get_all_debtor_rejections($visit_type,$start_date,$end_date,$payment_week)
	{

		if(!empty($start_date) AND !empty($end_date))
		{
			$search_add =  ' AND (visit_date >= \''.$start_date.'\' AND visit_date <= \''.$end_date.'\') ';
			$search_payment_add =  ' AND (visit_date >= \''.$start_date.'\' AND visit_date <= \''.$end_date.'\') ';
		}
		else if(!empty($start_date))
		{
			$search_add = ' AND visit_date = \''.$start_date.'\'';
			$search_payment_add = ' AND visit_date = \''.$start_date.'\'';
		}
		else if(!empty($end_date))
		{
			$search_add = ' AND visit_date = \''.$end_date.'\'';
			$search_payment_add = ' AND visit_date = \''.$end_date.'\'';
		}
		$start_date = date('2014-03-01');
		$invoice = ' AND visit.visit_date >= "'.$start_date.'"';

		$this->db->where('visit.visit_id = visit_bill.visit_parent AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_delete = 0 AND visit.visit_type = '.$visit_type.' '.$search_add.''.$invoice);
		$this->db->select('SUM(visit_bill_amount) AS total_invoice');
		$rejected_query = $this->db->get('visit_bill,visit');
		$total_rejected_invoice = 0;
		if($rejected_query->num_rows() > 0)
		{
			foreach ($rejected_query->result() as $key => $value) {
				# code...
				$total_rejected_invoice = $value->total_invoice;
			}
		}
		return $total_rejected_invoice;

	}

	function export_debtor_statement($visit_type_id,$start_date,$end_date)
	{
		$this->load->library('excel');
		

		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.bill_to = '.$visit_type_id.' AND (visit_invoice.created >= "'.$start_date.'" AND visit_invoice.created <= "'.$end_date.'" )';
		
		$table = 'v_transactions_by_date,visit_invoice';
		
		
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,visit_invoice.created AS invoice_date,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$visits_query = $this->db->get($table);

		// var_dump($visits_query); die();
		
		$title = 'Debtors Summary for  '.date('jS M Y',strtotime($start_date)).' '.date('jS M Y',strtotime($end_date));
		$col_count = 0;
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Visit Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Name';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Number';
			$col_count++;
			$report[$row_count][$col_count] = 'Procedures';
			$col_count++;
			$report[$row_count][$col_count] = 'Doctor';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Amount';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$invoice_date = $row->invoice_date;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$doctor = $row->personnel_fname;
				$count++;
				$invoice_total = $row->dr_amount;
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

				$invoice_total -= $credit_note;
				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);


				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;



				$item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items($visit_id,$visit_invoice_id);
			
				$procedures = '';
				if(count($item_invoiced_rs) > 0)
				{
					foreach ($item_invoiced_rs as $key_items):
						$s++;
						$service_charge_name = $key_items->service_charge_name;
						$visit_charge_amount = $key_items->visit_charge_amount;
						$service_name = $key_items->service_name;
						$units = $key_items->visit_charge_units;
						$visit_total = $visit_charge_amount * $units;
						$personnel_id = $key_items->personnel_id;
						$procedures .= strtoupper($service_charge_name).',';
					endforeach;
				}

				// $result .= 
				// 	'
				// 		<tr>
				// 			<td>'.$count.'</td>
				// 			<td>'.$visit_date.'</td>
				// 			<td>'.$patient_number.'</td>
				// 			<td>'.ucwords(strtolower($patient_surname)).'</td>
				// 			<td>'.$visit_type_name.'</td>
				// 			<td>'.$doctor.'</td>
				// 			<td>'.$visit_invoice_number.'</td>
				// 			<td>'.$branch_code.'</td>
				// 			<td>'.number_format($invoice_total,2).'</td>
				// 			<td>'.(number_format($payments_value,2)).'</td>
				// 			<td>'.(number_format($balance,2)).'</td>
				// 			<td><a href="'.site_url().'print-invoice/'.$visit_invoice_id.'/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Invoice</a></td>
				// 		</tr> 
				// ';

				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $invoice_date;
				$col_count++;
				$report[$row_count][$col_count] = $patient_surname.' '.$patient_othernames;
				$col_count++;
				$report[$row_count][$col_count] = $visit_invoice_number;
				$col_count++;
				$report[$row_count][$col_count] = $procedures;
				$col_count++;
				$report[$row_count][$col_count] = $doctor;
				$col_count++;
				$report[$row_count][$col_count] = $invoice_total;
				$col_count++;
				$report[$row_count][$col_count] = $balance;
				$col_count++;
				
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	function export_debtors_aging_summary()
	{
		$this->load->library('excel');
		
		
		// $where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id';
		
		// $table = 'v_transactions_by_date,visit_invoice';
		// $visit_search = $this->session->userdata('debtors_search_query');
		// // var_dump($visit_search);die();
		// if(!empty($visit_search))
		// {
		// 	$where .= $visit_search;
		
			
			
		// }
		// else
		// {
		// 	// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'" ';
		// 	$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		// }
		

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
			// $where .= $visit_search;
		
		}
		
		$query = $this->debtors_model->get_v_receivables();
		

		// $this->db->where($where);

		// $visits_query = $this->db->get($table);
		
		$title = 'Transactions Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;
		
		if($query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Debtors Name';
			$col_count++;
			$report[$row_count][$col_count] = 'Current';
			$col_count++;
			$report[$row_count][$col_count] = '30 Days';
			$col_count++;
			$report[$row_count][$col_count] = '60 Days';
			$col_count++;
			$report[$row_count][$col_count] = '90 Days';
			$col_count++;
			$report[$row_count][$col_count] = '120 Days';
			$col_count++;
			$report[$row_count][$col_count] = '150 Days';
			$col_count++;
			$report[$row_count][$col_count] = '180 Days';
			$col_count++;
			$report[$row_count][$col_count] = 'Over 180 Days';
			$col_count++;
			$report[$row_count][$col_count] = 'Balance';
			$col_count++;	
			//display all patient data in the leftmost columns
			$total_this_month = 0;
			$total_three_months = 0;
			$total_six_months = 0;
			$total_nine_months = 0;
			$total_payments = 0;
			$total_invoices =0;
			$total_balance = 0;
			$total_one_twenty_days =0;
			$total_one_fifty_days =0;
			$total_one_eighty_days =0;
			$total_three_sixty_days =0;
			$total_over_three_sixty_days =0;
			$total_over_one_eighty_days = 0;
			foreach ($query->result() as $row)
			{
				$row_count++;
				$count++;
				$visit_type_id = $row->recepientId;
				$visit_type_name = $row->receivable;
				$opening_balance = $row->opening_balance;
				$debit_id = $row->debit_id;

				$this_month = $row->coming_due;
				$three_months = $row->thirty_days;
				$six_months = $row->sixty_days;
				$nine_months = $row->ninety_days;
				$one_twenty_days = $row->one_twenty_days;
				$one_fifty_days = $row->one_fifty_days;
				$one_eighty_days = $row->one_eighty_days;
				$over_one_eighty_days = $row->over_one_eighty_days;
				// $three_sixty_days = $row->three_sixty_days;
				// $over_three_sixty_days = $row->over_three_sixty_days;

				$opening_date = $row->created;

				// $invoice_total = $this->creditors_model->get_invoice_total($visit_type_id);
				// $payments_total = $this->creditors_model->get_payments_total($visit_type_id);
				//$payments_total = 0;

				
				// var_dump($invoice_total);
				if($debit_id == 2)
				{
					$opening_balance = $opening_balance;	
				}
				else
				{
					$opening_balance = -$opening_balance;
				}

				// $payments_total = $this->debtors_model->get_debtor_total_payments($visit_type_id);


				$date = date('Y-m-d');
	        

	            $total_this_month +=$this_month;
	            $total_three_months +=$three_months;
	            $total_six_months +=$six_months;
	            $total_nine_months +=$nine_months;
	            $total_one_twenty_days +=$one_twenty_days;
	            $total_one_fifty_days +=$one_fifty_days;
	            $total_one_eighty_days +=$one_eighty_days;
	            $total_over_one_eighty_days +=$over_one_eighty_days;

	            $total_payments += $payments_total;
	            $total_invoices += $invoice_total;
	            $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $over_one_eighty_days;
	            $total_balance += $invoice_total;


				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = number_format($this_month, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($three_months, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($six_months, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($nine_months, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($one_twenty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($one_fifty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($one_eighty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($over_one_eighty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($invoice_total, 2);
				$col_count++;				
				
			}

				$row_count++;
				$report[$row_count][$col_count] = 'TOTAL';
				$col_count++;
				$report[$row_count][$col_count] = $total_this_month;
				$col_count++;
				$report[$row_count][$col_count] = number_format($this_month, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_three_months, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_six_months, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_nine_months, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_one_twenty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_one_fifty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_one_eighty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_over_one_eighty_days, 2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($total_balance, 2);
				$col_count++;
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_v_receivables($visit_type_id)
	{
		$date_searched = $this->session->userdata('date_debtors_batches');
		$report_detail = $this->session->userdata('report_detail');
		// $date_searched = 'curdate( )';

		if(empty($date_searched))
		{
			$date_searched = date('Y-m-d');
		}

		if(!empty($visit_type_id))
		{
			$add = ' AND v_transaction_ledger.payment_type = '.$visit_type_id;
		}
		else
		{
			$add = '';	
		}

		// var_dump($date_searched);die();
		// $date_searched = '2022-01-01';

		// var_dump($date_searched);die();


		if($report_detail == 180)
		{

			$sql = "
				
					SELECT 
					 *
					FROM 
					(
						SELECT
							v_transaction_ledger.payment_type AS recepientId,
							v_transaction_ledger.payment_type_name as receivable,
							2 as branch_id,
							
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) = 0 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `coming_due`,
								(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 1 AND 30 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `thirty_days`,
								(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 31 AND 60 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `sixty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 61 AND 90 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `ninety_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 91 AND 120 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_twenty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 121 AND 150 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_fifty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 151 AND 180 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_eighty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) > 180 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `over_one_eighty_days`     
						  FROM
						  `v_transaction_ledger`
						  WHERE `v_transaction_ledger`.`payment_type` > 0 ".$add."
						  GROUP BY  `v_transaction_ledger`.`payment_type`
						 ) AS data ORDER BY data.`receivable` ASC
					 ";

		}
		else if($report_detail == 360)
		{
			$sql = "
				
					SELECT 
					 *
					FROM 
					(
						SELECT
							v_transaction_ledger.payment_type AS recepientId,
							v_transaction_ledger.payment_type_name as receivable,
							2 as branch_id,
							
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) = 0 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `coming_due`,
								(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 1 AND 30 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `thirty_days`,
								(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 31 AND 60 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `sixty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 61 AND 90 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `ninety_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 91 AND 120 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_twenty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 121 AND 150 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_fifty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 151 AND 180 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_eighty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 181 AND 210 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `two_ten_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 211 AND 240 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `two_fourty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 241 AND 270 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `two_seventy_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 271 AND 300 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_hundred_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 301 AND 330 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_thirty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 331 AND 360 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_sixty_days`,
							
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) > 360 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `over_three_sixty_days`     
						  FROM
						  `v_transaction_ledger`
						  WHERE `v_transaction_ledger`.`payment_type` > 0 ".$add."
						  GROUP BY  `v_transaction_ledger`.`payment_type`
						 ) AS data ORDER BY data.`receivable` ASC
					 ";
		}
		else if($report_detail == 720)
		{
			$sql = "
				
					SELECT 
					 *
					FROM 
					(
						SELECT
							v_transaction_ledger.payment_type AS recepientId,
							v_transaction_ledger.payment_type_name as receivable,
							2 as branch_id,
							
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) = 0 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `coming_due`,
								(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 1 AND 30 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `thirty_days`,
								(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 31 AND 60 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `sixty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 61 AND 90 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `ninety_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 91 AND 120 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_twenty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 121 AND 150 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_fifty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 151 AND 180 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `one_eighty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 181 AND 210 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `two_ten_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 211 AND 240 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `two_fourty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 241 AND 270 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `two_seventy_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 271 AND 300 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_hundred_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 301 AND 330 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_thirty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 331 AND 360 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_sixty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 361 AND 390 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `three_ninety_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 391 AND 420 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `four_twenty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 421 AND 450 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `four_fifty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 451 AND 480 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `four_eighty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 481 AND 510 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `five_ten_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 510 AND 540 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `five_fourty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 541 AND 570 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `five_seventy_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 571 AND 600 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `six_hundred_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 601 AND 630 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `six_thirty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 631 AND 660 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `six_sixty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 661 AND 690 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `six_ninety_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) BETWEEN 691 AND 720 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `seven_twenty_days`,
							(
								sum(
						      IF(
						         ( DATEDIFF('$date_searched', v_transaction_ledger.invoice_date) > 720 ),v_transaction_ledger.dr_amount,0
										)
						      )
						 
						    
							) AS `over_seven_twenty_days`     
						  FROM
						  `v_transaction_ledger`
						  WHERE `v_transaction_ledger`.`payment_type` > 0 ".$add."
						  GROUP BY  `v_transaction_ledger`.`payment_type`
						 ) AS data ORDER BY data.`receivable` ASC
					 ";
		}

		

		$query = $this->db->query($sql);
		// $this->db->where('recepientId > 0');
		// $this->db->order_by('receivable','ASC');
		// // $this->db->join('visit_type_account','v_aging_receivables.recepientId = visit_type_account.visit_type','LEFT');

		// $query = $this->db->get('v_aging_receivables');

		return $query;
	}



	public function get_v_receivables_old()
	{
		$this->db->where('recepientId > 0');
		$this->db->order_by('receivable','ASC');
		// $this->db->join('visit_type_account','v_aging_receivables.recepientId = visit_type_account.visit_type','LEFT');

		$query = $this->db->get('v_aging_receivables');

		return $query;
	}


	public function export_statements_of_accounts()
	{


		$this->load->library('excel');
		

		
		$statements_visit_type_name = $this->session->userdata('statements_visit_type_name');

		$statement_name = $statements_visit_type_name;




		// $this->db->select('visit_invoice.*,patients.*,visit_invoice.created AS invoice_date,visit_invoice.scheme_name AS scheme');
		// $this->db->join('patients', 'patients.patient_id = visit_invoice.patient_id', 'left');

		// $this->db->where($where);
		// $this->db->order_by('visit_invoice.created','ASC');


		// $this->db->from($table);

	
		// $visits_query = $this->db->get();

		// $visit_type_name = $this->session->userdata('statements_visit_type_name');


		$statement_date = $this->session->userdata('statements_visit_date');
		
		if(!empty($statement_date))
		{
			$statement_date = date('d.m.y',strtotime($statement_date));
			$title = 'STATEMENT OF ACCOUNTS AS AT '.$statement_date.' '.$statement_name;
		}
		else
		{
			$title = 'STATEMENT OF ACCOUNTS FOR '.$statement_name;
		}
		


		$row_count = 1;

		$report[$row_count][0] = $title;
		$report[$row_count][1] = '';
		$report[$row_count][2] = '';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';					
		$report[$row_count][10] = '';			
		$report[$row_count][11] = '';
		$report[$row_count][12] = '';
		$report[$row_count][13] = '';
		$row_count++;
		
		

		$report[$row_count][0] = 'Ref No';
		$report[$row_count][1] = '';
		$report[$row_count][2] = 'FILE No';
		$report[$row_count][3] = 'Patient\'s Name. ';
		$report[$row_count][4] = 'Scheme/Company';
		$report[$row_count][5] = 'Member No./Policy No.';
		$report[$row_count][6] = 'Preauthorisation Date';
		$report[$row_count][7] = 'Preauthorisation Officer';
		$report[$row_count][8] = 'Transaction Type';
		$report[$row_count][9] = 'Ageing';
		$report[$row_count][10] = 'Invoice Date';		
		$report[$row_count][11] = 'Invoice Number';
		$report[$row_count][12] = 'Invoice Amount';
		$report[$row_count][13] = 'Invoice Balance';
		$report[$row_count][14] = 'Remarks';

		

		$row_count++;
		


		$start_year = 2015;
		$end_year = date('Y');

		$grand_total = 0;
		$grand_balance = 0;
		

		for ($i=$start_year; $i <= $end_year; $i++) 
		{ 
			// code...

			

			// $where = 'visit_invoice.visit_invoice_delete = 0  AND YEAR(visit_invoice.created) = '.$i;
		
			// $table = 'visit_invoice';
		


			// $visit_search = $this->session->userdata('statements_search_query');
			// // var_dump($visit_search);die();
			// if(!empty($visit_search))
			// {
			// 	$where .= $visit_search;
			
			// }
				
			
			// else
			// {

			
			// }

			// $this->db->select('visit_invoice.*,patients.*,visit_invoice.created AS invoice_date,visit_invoice.scheme_name AS scheme');
			// $this->db->join('patients', 'patients.patient_id = visit_invoice.patient_id', 'left');

			// $this->db->where($where);
			// $this->db->order_by('visit_invoice.created','ASC');


			// $this->db->from($table);

			$visit_search = $this->session->userdata('statements_search_query');
			// var_dump($visit_search);die();
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
				$visits_query = $this->debtors_model->get_invoice_balances_per_visit_charge($i);

				// var_dump($query->num_rows());die();
				
			}
			else
			{
				$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
			
				$visit_payments = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
				$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
				$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

				$this->session->set_userdata('visit_invoices', $visit_invoices);
				// $this->session->set_userdata('debtors_search_query', $visit_invoices);
				$this->session->set_userdata('visit_payments', $visit_payments);
			

				$visits_query = NULL;

			}


			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;

			if($visits_query->num_rows() > 0)
			{


				$report[$row_count][0] = '';
				$report[$row_count][1] = 'Insurance -  '.$statements_visit_type_name.' '.$i;
				$report[$row_count][2] = '';
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = '';
				$report[$row_count][8] = '';
				$report[$row_count][9] = '';					
				$report[$row_count][10] = '';			
				$report[$row_count][11] = '';
				$report[$row_count][12] = '';
				$report[$row_count][13] = '';
				$report[$row_count][14] = '';
				$row_count++;
				$count = 0;


				

				foreach ($visits_query->result() as $key => $row) {
					// code...

					$total_invoiced = 0;
					$visit_date = date('d.m.y',strtotime($row->invoice_date));
					
					
					$visit_id = $row->visit_id;
					$patient_id = $row->patient_id;
					$personnel_id = $row->personnel_id;
					$dependant_id = $row->dependant_id;
					$patient_number = $row->patient_number;
					$visit_invoice_number = $row->visit_invoice_number;
					$scheme_name = $row->scheme_name;
					$member_number = $row->member_number;
					$preauth_date = $row->preauth_date;
					$authorising_officer = $row->authorising_officer;
					$visit_invoice_id = $row->visit_invoice_id;
					$branch_code = $row->branch_code;

					$visit_type_name = $row->payment_type_name;
					$patient_othernames = $row->patient_othernames;
					$patient_surname = $row->patient_surname;
					$patient_date_of_birth = $row->patient_date_of_birth;
					$patient_first_name = $row->patient_first_name;
					$preauth_date = date('d.m.y',strtotime($row->preauth_date));
					$doctor = $row->personnel_fname;
				
					$invoice_bill = $row->invoice_bill;
					$bill_to = $row->bill_to;
					$invoice_balance = $row->invoice_balance;
					$invoice_credit_note = $row->invoice_credit_note;
					$invoice_total = $invoice_bill - $invoice_credit_note;

					$balance = $row->balance;
					$payments_value = $row->invoice_payments;

					$date = $row->date_invoice;

					// $total_payable_by_patient += $invoice_total;
					// $total_payments += $payments_value;
					// $total_balance += $balance;

					// $date = $row->invoice_date;
									
					$ageing  = $this->debtors_model->get_aging_time($date);


					$date = date('d/m/Y',strtotime($row->date_invoice));

					// $query_patient = $this->hospital_reports_model->get_visit_type_patient_balances($bill_to,$patient_id);
					// $open_balance = 0;
					// if($query_patient->num_rows() > 0)
					// {
					// 	foreach ($query_patient->result() as $key => $value_two) {
					// 		// code...
					// 		$open_balance = $value_two->invoice_balance;
					// 	}
					// }


					if($patient_number == "DO994")
					{
						// var_dump($balance);die();
					}
					
					if($balance == 0)
					{

						

					}
					else
					{
						$total_payable_by_patient += $invoice_total;
						$total_payments += $payments_value;
						$total_balance += $balance;

						$grand_total += $invoice_total;
						$grand_balance += $balance;

						$row_count++;
						$count++;
						

						//display the patient data
						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = $scheme_name;
						$report[$row_count][5] = $member_number;
						$report[$row_count][6] = $preauth_date;
						$report[$row_count][7] = $authorising_officer;
						$report[$row_count][8] = 'Invoice';
						$report[$row_count][9] = $ageing;
						$report[$row_count][10] = $date;
						$report[$row_count][11] = $visit_invoice_number;					
						$report[$row_count][12] = (number_format($invoice_total,2));			
						$report[$row_count][13] = (number_format($balance,2));
						$report[$row_count][14] = '';
					}

				}


				



			}


			
			$visit_type = $this->session->userdata('statements_visit_type');


			$visit_search = $this->session->userdata('statements_visit_invoices');
			$customers_add = '';
			$orders_add = '';
			$payments_add = '';
			$credit_note_add = '';
			$patients_add = '';
			$allocations_add = '';
			if(!empty($visit_search))
			{

				$customers_add = str_replace('data.transaction_date', 'customers_journals.journal_date', $visit_search);
				$orders_add = str_replace('data.transaction_date', 'pos_order.order_date', $visit_search);
				$payments_add = str_replace('data.transaction_date', 'payments.payment_date', $visit_search);
				$credit_note_add = str_replace('data.transaction_date', 'visit_credit_note.created', $visit_search);
				$patients_add = str_replace('data.transaction_date', 'patients_journals.journal_date', $visit_search);
				$allocations_add = str_replace('data.transaction_date', 'batch_receipts.payment_date', $visit_search);
			}
		
			

			$this->db->where('customer.customer_id = customers_journals.customer_id AND customers_journals.visit_type_id ='.$visit_type.$customers_add.' AND YEAR(customers_journals.journal_date) ='.$i);
			$query_three = $this->db->get('customers_journals,customer');


			if($query_three->num_rows() > 0)
			{
			
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$customer_name = $value5->customer_name;
					$journal_amount = $value5->customer_journal_amount;
					$invoice_number = $value5->customer_journal_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->customer_journal_type;

				

					
					// $journal_type_name = 'Payment';
					$journal_type_name = 'General Journal';
					
					$ageing  = $this->debtors_model->get_aging_time($journal_date);

					// calculate the balance = 
					$count++;

					$row_count++;
					

					

					$total_payable_by_patient += $journal_amount;
					$total_payments += 0;
					$total_balance += $journal_amount;

					$grand_total += $journal_amount;
					$grand_balance += $journal_amount;


					$report[$row_count][0] = $count;
					$report[$row_count][1] = '';
					$report[$row_count][2] = '';
					$report[$row_count][3] = ucwords(strtolower($customer_name));
					$report[$row_count][4] = '';
					$report[$row_count][5] = '';
					$report[$row_count][6] = $journal_type_name;
					$report[$row_count][7] = '';
					$report[$row_count][8] = '';
					$report[$row_count][9] = $ageing;
					$report[$row_count][10] = $journal_date;
					$report[$row_count][11] = $invoice_number;					
					$report[$row_count][12] = number_format($journal_amount,2);			
					$report[$row_count][13] = number_format($journal_amount,2);
					$report[$row_count][14] = '';
					
					
					
				}
			}


			$this->db->where('customers_journals.customer_journal_type = 2 AND customers_journals.visit_type_id ='.$visit_type.$customers_add.' AND YEAR(customers_journals.journal_date) ='.$i);
			$query_three = $this->db->get('customers_journals');

			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$customer_name = '';//$value5->customer_name;
					$journal_amount = -$value5->customer_journal_amount;
					$invoice_number = $value5->customer_journal_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->customer_journal_type;

					$count++;

					
					// $journal_type_name = 'Payment';
					$journal_type_name = 'Payment';
					
					$ageing  = $this->debtors_model->get_aging_time($journal_date);

					// calculate the balance = 
	

					$row_count++;
					

					

					$total_payable_by_patient += $journal_amount;
					$total_payments += 0;
					$total_balance += $journal_amount;

					$grand_total += $journal_amount;
					$grand_balance += $journal_amount;


					$report[$row_count][0] = $count;
					$report[$row_count][1] = '';
					$report[$row_count][2] = '';
					$report[$row_count][3] = ucwords(strtolower($customer_name));
					$report[$row_count][4] = '';
					$report[$row_count][5] = '';
					$report[$row_count][6] = $journal_type_name;
					$report[$row_count][7] = '';
					$report[$row_count][8] = '';
					$report[$row_count][9] = $ageing;
					$report[$row_count][10] = $journal_date;
					$report[$row_count][11] = $invoice_number;					
					$report[$row_count][12] = number_format($journal_amount,2);			
					$report[$row_count][13] = number_format($journal_amount,2);
					$report[$row_count][14] = '';
					
					
					
				}
			}

			$this->db->where('customer.customer_id = pos_order.customer_id AND `pos_order_item`.`charged` = 1
AND pos_order_item.pos_order_item_deleted = 0
AND pos_order.pos_order_id = pos_order_item.pos_order_id
AND pos_order.pos_order_deleted = 0
AND visit_type.visit_type_id = pos_order.sale_type AND pos_order.sale_type ='.$visit_type.$orders_add.' AND YEAR(pos_order.order_date) ='.$i);


			
			$query_three = $this->db->get('pos_order_item,pos_order,visit_type,customer');


			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$customer_name = $value5->customer_name;
					$pos_order_item_quantity = $value5->pos_order_item_quantity;
					$pos_order_item_amount = $value5->pos_order_item_amount;
					$invoice_number = $value5->pos_order_number;
					$journal_date = $value5->order_date;
					$journal_type = $value5->customer_journal_type;
					$journal_amount = $pos_order_item_quantity *$pos_order_item_amount;
					$count++;

					
					// $journal_type_name = 'Payment';
					$journal_type_name = 'Invoice';
					
					$ageing  = $this->debtors_model->get_aging_time($journal_date);

					// calculate the balance = 

					

					$count++;

					$row_count++;
					

					

					$total_payable_by_patient += $journal_amount;
					$total_payments += 0;
					$total_balance += $journal_amount;

					$grand_total += $journal_amount;
					$grand_balance += $journal_amount;


					$report[$row_count][0] = $count;
					$report[$row_count][1] = '';
					$report[$row_count][2] = '';
					$report[$row_count][3] = ucwords(strtolower($customer_name));
					$report[$row_count][4] = '';
					$report[$row_count][5] = '';
					$report[$row_count][6] = $journal_type_name;
					$report[$row_count][7] = '';
					$report[$row_count][8] = '';
					$report[$row_count][9] = $ageing;
					$report[$row_count][10] = $journal_date;
					$report[$row_count][11] = $invoice_number;					
					$report[$row_count][12] = number_format($journal_amount,2);			
					$report[$row_count][13] = number_format($journal_amount,2);
					$report[$row_count][14] = '';
					
				}
			}

		
			$visit_type = $this->session->userdata('statements_visit_type');

			
			$this->db->select('SUM(payment_item.payment_item_amount) AS total_payment_amount,payments.*,patients.*,payment_item.invoice_type');

			$this->db->where('payments.patient_id = patients.patient_id AND payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND payments.payment_type = '.$visit_type.' AND  payment_item.invoice_type >= 2 AND payment_item.invoice_type <> 3  '.$payments_add.' AND YEAR(payments.payment_date) = '.$i);

			$this->db->group_by('payment_item.payment_id');

			$query_three = $this->db->get('payments,payment_item,patients');

			if($query_three->num_rows() > 0)
			{
				$row_count++;
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$patient_number = $value5->patient_number;
					$patient_surname = $value5->patient_surname;
					$patient_othernames = $value5->patient_othernames;
					$patient_first_name = $value5->patient_first_name;
					$receipt_number = $value5->receipt_number;
					$payment_date = $value5->payment_date;
					$patient_id = $value5->patient_id;
					$total_amount = $value5->total_payment_amount;
					$payment_amount = $value5->total_payment_amount;
					$invoice_type = $value5->invoice_type;

				
					
					// // $journal_type_name = 'Payment';

					if($invoice_type == 5)
					{
						$journal_type_name = 'Refund';
						$journal_balance = $total_amount;
					}
					else
					{
						$journal_type_name = 'Payment';

						$journal_balance = -$total_amount;
					}
					
					

					// calculate the balance = 


					

					if($journal_balance != 0)
					{
						$count++;

						$row_count++;
						$total_payable_by_patient += $journal_amount;
						$total_payments += $payment_amount;
						$total_balance += $journal_balance;

						$grand_total += $journal_amount;
						$grand_balance += $journal_balance;




						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = '';
						$report[$row_count][5] = '';
						$report[$row_count][6] = $journal_type_name;
						$report[$row_count][7] = '';
						$report[$row_count][8] = '';
						$report[$row_count][9] = '';
						$report[$row_count][10] = $journal_date;
						$report[$row_count][11] = $invoice_number;					
						$report[$row_count][12] = number_format($journal_amount,2);			
						$report[$row_count][13] = number_format($journal_balance,2);
						$report[$row_count][14] = '';
						
					}
					
				}

			}




			// $this->db->where('patients_journals.patient_id = patients.patient_id AND patients_journals.visit_invoice_id = 0 AND patients_journals.account_to_id = 19 AND  patients_journals.visit_type_id ='.$visit_type.' AND YEAR(patients_journals.journal_date) = '.$i);
			$this->db->where('patients_journals.patient_id = patients.patient_id AND patients_journals.visit_invoice_id = 0 AND patients_journals.account_to_id = 19 AND  patients_journals.visit_type_id ='.$visit_type.$patients_add.' AND YEAR(patients_journals.journal_date) = '.$i);
				$query_three = $this->db->get('patients_journals,patients');

			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) 
				{
					# code...
					$patient_number = $value5->patient_number;
					$patient_surname = $value5->patient_surname;
					$patient_othernames = $value5->patient_othernames;
					$patient_first_name = $value5->patient_first_name;
					$journal_amount = $value5->journal_amount;
					$invoice_number = $value5->invoice_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->journal_type;
					$journal_amount = $value5->journal_amount;
					$patient_id = $value5->patient_id;

					

					if($journal_type == 1)
					{
						$journal_type_name = 'General Journal';
					}
					else
					{
						// $journal_type_name = 'Payment';
						$journal_type_name = 'General Journal';
					}

					// calculate the balance = 

					$payment_amount = $this->debtors_model->get_patient_account_payments($patient_id,$visit_type);
					$journal_payment = $this->debtors_model->get_patient_account_journals($patient_id,$visit_type);

					
					$journal_balance = ($journal_amount + $journal_payment) - $payment_amount;

					if($journal_balance > 0 OR $journal_balance < 0)
					{
						$count++;
						$row_count++;


						$total_payable_by_patient += $journal_balance;
						$total_payments += $payment_amount;
						$total_balance += $journal_balance;


						$grand_total += $journal_amount;
						$grand_balance += $journal_balance;


						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = '';
						$report[$row_count][5] = '';
						$report[$row_count][6] = $journal_type_name;
						$report[$row_count][7] = '';
						$report[$row_count][8] = '';
						$report[$row_count][9] = '';
						$report[$row_count][10] = $journal_date;
						$report[$row_count][11] = $invoice_number;					
						$report[$row_count][12] = number_format($journal_amount,2);			
						$report[$row_count][13] = number_format($journal_balance,2);
						$report[$row_count][14] = '';
						
					}
				}
			}


			
			$this->db->where('patients_journals.patient_id = patients.patient_id AND patients_journals.account_from_id = 19 AND patients_journals.journal_type = 6 AND  patients_journals.visit_type_id ='.$visit_type.$patients_add.' AND YEAR(patients_journals.journal_date) = '.$i);
				$query_three = $this->db->get('patients_journals,patients');

			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) 
				{
					# code...
					$patient_number = $value5->patient_number;
					$patient_surname = $value5->patient_surname;
					$patient_othernames = $value5->patient_othernames;
					$patient_first_name = $value5->patient_first_name;
					$journal_amount = $value5->journal_amount;
					$invoice_number = $value5->invoice_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->journal_type;
					$journal_amount = $value5->journal_amount;
					$patient_id = $value5->patient_id;

					

					if($journal_type == 1)
					{
						$journal_type_name = 'General Journal';
					}
					else
					{
						// $journal_type_name = 'Payment';
						$journal_type_name = 'General Journal';
					}

					// calculate the balance = 

					
						$count++;
						$row_count++;


						$total_payable_by_patient += $journal_amount;
						$total_payments += 0;
						$total_balance += $journal_amount;


						$grand_total += $journal_amount;
						$grand_balance += $journal_amount;


						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = '';
						$report[$row_count][5] = '';
						$report[$row_count][6] = $journal_type_name;
						$report[$row_count][7] = '';
						$report[$row_count][8] = '';
						$report[$row_count][9] = '';
						$report[$row_count][10] = $journal_date;
						$report[$row_count][11] = $invoice_number;					
						$report[$row_count][12] = number_format($journal_amount,2);			
						$report[$row_count][13] = number_format($journal_amount,2);
						$report[$row_count][14] = '';
						
					
				}
			}


			if($total_payable_by_patient > 0 )
			{
				$row_count++;
			
				$report[$row_count][0] = '';
				$report[$row_count][1] = '';
				$report[$row_count][2] = '';
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = '';
				$report[$row_count][8] = '';
				$report[$row_count][9] = '';
				$report[$row_count][10] = '';
				$report[$row_count][11] = 'Total '.$i;					
				$report[$row_count][12] = number_format($total_payable_by_patient,2);			
				$report[$row_count][13] = number_format($total_balance,2);
				$report[$row_count][14] = '';

			}


			
			$row_count++;

			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';					
			$report[$row_count][10] = '';			
			$report[$row_count][11] = '';
			$report[$row_count][12] = '';
			$report[$row_count][13] = '';
			$report[$row_count][14] = '';

			// add the unallocated payments
		}



		$row_count++;
		$report[$row_count][0] = '';
		$report[$row_count][1] = 'Unallocated Payments';
		$report[$row_count][2] = '';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';					
		$report[$row_count][10] = '';			
		$report[$row_count][11] = '';
		$report[$row_count][12] = '';
		$report[$row_count][13] = '';
		$report[$row_count][14] = '';


		$visit_type = $this->session->userdata('statements_visit_type');

		// $this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type);
		$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type.$allocations_add);
		$query = $this->db->get('batch_unallocations,batch_receipts');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$payment_date = $value->payment_date;
						
				$payment_date = $value->payment_date;
				$receipt_number = $value->receipt_number;
				$allocation_type_id = $value->allocation_type_id;


				if($allocation_type_id == 1)
				{
					$amount = $value->amount_paid;
				}
				else
				{
					$amount = -$value->amount_paid;
				}

				$payment_date = date('d.m.y',strtotime($value->payment_date));

				$total_payable_by_patient += $amount;

				$grand_total += $amount;
				$grand_balance += $amount;


				$visit_type_name = $this->session->userdata('statements_visit_type_name');
				$row_count++;
				$report[$row_count][0] = '';
				$report[$row_count][1] = 'Insurance - '.$visit_type_name;
				$report[$row_count][2] = '';
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = '';
				$report[$row_count][8] = 'Payment';
				$report[$row_count][9] = '';
				$report[$row_count][10] = $payment_date;					
				$report[$row_count][11] = $receipt_number;			
				$report[$row_count][12] = number_format($amount,2);
				$report[$row_count][13] = number_format($amount,2);
				$report[$row_count][14] = '';
			}

			$row_count++;
			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';					
			$report[$row_count][10] = '';			
			$report[$row_count][11] = '';
			$report[$row_count][12] = '';
			$report[$row_count][13] = '';

			
		}




		
		$statement_date = date('d.m.y',strtotime($statement_date));
		$row_count++;
		$report[$row_count][0] = '';
		$report[$row_count][1] = 'Total owed as at '.$statement_date;
		$report[$row_count][2] = '';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';
		$report[$row_count][10] = '';		
		$report[$row_count][11] = '';					
		$report[$row_count][12] = number_format($grand_total,2);			
		$report[$row_count][13] = number_format($grand_balance,2);
		$report[$row_count][14] = '';


		// var_dump($report);die();
		
		//create the excel document
		// $sheet->setCellValue('A1', 'Hello World');

		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function export_statements_of_accounts_old()
	{


		$this->load->library('excel');
		

		$where = 'visit_invoice.visit_invoice_delete = 0';
		
		$table = 'visit_invoice';
	


		$visit_search = $this->session->userdata('statements_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
		}
			
		
		else
		{

		
		}
		$statements_visit_type_name = $this->session->userdata('statements_visit_type_name');

		$statement_name = $statements_visit_type_name;

		$this->db->select('visit_invoice.*,patients.*,visit_invoice.created AS invoice_date,visit_invoice.scheme_name AS scheme');
		$this->db->join('patients', 'patients.patient_id = visit_invoice.patient_id', 'left');

		$this->db->where($where);
		$this->db->order_by('visit_invoice.created','ASC');


		$this->db->from($table);

	
		$visits_query = $this->db->get();

		// $visit_type_name = $this->session->userdata('statements_visit_type_name');


		$statement_date = $this->session->userdata('statements_visit_date');
		
		if(!empty($statement_date))
		{
			$statement_date = date('d.m.y',strtotime($statement_date));
			$title = 'STATEMENT OF ACCOUNTS AS AT '.$statement_date.' '.$statement_name;
		}
		else
		{
			$title = 'STATEMENT OF ACCOUNTS FOR '.$statement_name;
		}
		


		$row_count = 1;

		$report[$row_count][0] = $title;
		$report[$row_count][1] = '';
		$report[$row_count][2] = '';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';					
		$report[$row_count][10] = '';			
		$report[$row_count][11] = '';
		$report[$row_count][12] = '';
		$report[$row_count][13] = '';
		$row_count++;
		
		

	
		$row_count++;
		$report[$row_count][0] = 'Ref No';
		$report[$row_count][1] = '';
		$report[$row_count][2] = 'FILE No';
		$report[$row_count][3] = 'Patient\'s Name. ';
		$report[$row_count][4] = 'Scheme/Company';
		$report[$row_count][5] = 'Member No./Policy No.';
		$report[$row_count][6] = 'Preauthorisation Date';
		$report[$row_count][7] = 'Preauthorisation Officer';
		$report[$row_count][8] = 'Transaction Type';
		$report[$row_count][9] = 'Ageing';
		$report[$row_count][10] = 'Invoice Date';		
		$report[$row_count][11] = 'Invoice Number';
		$report[$row_count][12] = 'Invoice Amount';
		$report[$row_count][13] = 'Invoice Balance';
		$report[$row_count][14] = 'Remarks';

		

		$row_count++;
		


		$start_year = 2015;
		$end_year = date('Y');

		$grand_total = 0;
		$grand_balance = 0;
		

		for ($i=$start_year; $i <= $end_year; $i++) 
		{ 
			// code...

			

			$where = 'visit_invoice.visit_invoice_delete = 0  AND YEAR(visit_invoice.created) = '.$i;
		
			$table = 'visit_invoice';
		


			$visit_search = $this->session->userdata('statements_search_query');
			// var_dump($visit_search);die();
			if(!empty($visit_search))
			{
				$where .= $visit_search;
			
			}
				
			
			else
			{

			
			}

			$this->db->select('visit_invoice.*,patients.*,visit_invoice.created AS invoice_date,visit_invoice.scheme_name AS scheme');
			$this->db->join('patients', 'patients.patient_id = visit_invoice.patient_id', 'left');

			$this->db->where($where);
			$this->db->order_by('visit_invoice.created','ASC');


			$this->db->from($table);

		
			$visits_query = $this->db->get();

			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;

			if($visits_query->num_rows() > 0)
			{


				$report[$row_count][0] = '';
				$report[$row_count][1] = 'Insurance -  '.$statements_visit_type_name.' '.$i;
				$report[$row_count][2] = '';
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = '';
				$report[$row_count][8] = '';
				$report[$row_count][9] = '';					
				$report[$row_count][10] = '';			
				$report[$row_count][11] = '';
				$report[$row_count][12] = '';
				$report[$row_count][13] = '';
				$report[$row_count][14] = '';
				$row_count++;
				$count = 0;


				

				foreach ($visits_query->result() as $key => $row) {
					// code...

					$total_invoiced = 0;
					$visit_date = date('d.m.y',strtotime($row->invoice_date));
					
					
					
					
					$visit_id = $row->visit_id;
					$patient_id = $row->patient_id;
					$personnel_id = $row->personnel_id;
					$dependant_id = $row->dependant_id;
					$patient_number = $row->patient_number;
					$visit_invoice_number = $row->visit_invoice_number;
					$scheme_name = $row->scheme;
					$member_number = $row->member_number;
					$preauth_date = $row->preauth_date;
					$authorising_officer = $row->authorising_officer;
					$visit_invoice_id = $row->visit_invoice_id;
					$branch_code = $row->branch_code;

					$visit_type_name = $row->payment_type_name;
					$patient_othernames = $row->patient_othernames;
					$patient_surname = $row->patient_surname;
					$patient_date_of_birth = $row->patient_date_of_birth;
					$patient_first_name = '';//$row->patient_first_name;
					$preauth_date = date('d.m.y',strtotime($row->preauth_date));
					$doctor = $row->personnel_fname;
					
					$invoice_bill = $row->invoice_bill;
					$invoice_balance = $row->invoice_balance;
					$invoice_credit_note = $row->invoice_credit_note;
					$invoice_total = $invoice_bill - $invoice_credit_note;

					$balance = $row->invoice_balance;
					$payments_value = $row->invoice_payments;

					$date = $row->invoice_date;

					
					// $date = $row->invoice_date;
									
					$ageing  = $this->get_aging_time($date);

					
					$date = date('d/m/Y',strtotime($row->invoice_date));

					if($invoice_balance != 0)
					{

						$total_payable_by_patient += $invoice_total;
						$total_payments += $payments_value;
						$total_balance += $balance;

						$grand_total += $invoice_total;
						$grand_balance += $balance;

						$row_count++;
						$count++;
						

						//display the patient data
						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = $scheme_name;
						$report[$row_count][5] = $member_number;
						$report[$row_count][6] = $preauth_date;
						$report[$row_count][7] = $authorising_officer;
						$report[$row_count][8] = 'Invoice';
						$report[$row_count][9] = $ageing;
						$report[$row_count][10] = $date;
						$report[$row_count][11] = $visit_invoice_number;					
						$report[$row_count][12] = (number_format($invoice_total,2));			
						$report[$row_count][13] = (number_format($balance,2));
						$report[$row_count][14] = '';
					}

				}


				



			}


			
			$visit_type = $this->session->userdata('statements_visit_type');

			$this->db->where('customer.customer_id = customers_journals.customer_id AND customers_journals.visit_type_id ='.$visit_type.' AND YEAR(customers_journals.journal_date) ='.$i);
			$query_three = $this->db->get('customers_journals,customer');

			if($query_three->num_rows() > 0)
			{
			
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$customer_name = $value5->customer_name;
					$journal_amount = $value5->customer_journal_amount;
					$invoice_number = $value5->customer_journal_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->customer_journal_type;

				

					
					// $journal_type_name = 'Payment';
					$journal_type_name = 'General Journal';
					
					$ageing  = $this->debtors_model->get_aging_time($journal_date);

					// calculate the balance = 
					$count++;

					$row_count++;
					

					

					$total_payable_by_patient += $journal_amount;
					$total_payments += 0;
					$total_balance += $journal_amount;

					$grand_total += $journal_amount;
					$grand_balance += $journal_amount;


					$report[$row_count][0] = $count;
					$report[$row_count][1] = '';
					$report[$row_count][2] = '';
					$report[$row_count][3] = ucwords(strtolower($customer_name));
					$report[$row_count][4] = '';
					$report[$row_count][5] = '';
					$report[$row_count][6] = $journal_type_name;
					$report[$row_count][7] = '';
					$report[$row_count][8] = '';
					$report[$row_count][9] = $ageing;
					$report[$row_count][10] = $journal_date;
					$report[$row_count][11] = $invoice_number;					
					$report[$row_count][12] = number_format($journal_amount,2);			
					$report[$row_count][13] = number_format($journal_amount,2);
					$report[$row_count][14] = '';
					
					
					
				}
			}


			$this->db->where('customer.customer_id = pos_order.customer_id AND `pos_order_item`.`charged` = 1
							AND pos_order_item.pos_order_item_deleted = 0
							AND pos_order.pos_order_id = pos_order_item.pos_order_id
							AND pos_order.pos_order_deleted = 0
							AND visit_type.visit_type_id = pos_order.sale_type AND pos_order.sale_type ='.$visit_type.' AND YEAR(pos_order.order_date) ='.$i);
			$query_three = $this->db->get('pos_order_item,pos_order,visit_type,customer');


			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$customer_name = $value5->customer_name;
					$pos_order_item_quantity = $value5->pos_order_item_quantity;
					$pos_order_item_amount = $value5->pos_order_item_amount;
					$invoice_number = $value5->pos_order_number;
					$journal_date = $value5->order_date;
					$journal_type = $value5->customer_journal_type;
					$journal_amount = $pos_order_item_quantity *$pos_order_item_amount;
					$count++;

					
					// $journal_type_name = 'Payment';
					$journal_type_name = 'Invoice';
					
					$ageing  = $this->debtors_model->get_aging_time($journal_date);

					// calculate the balance = 

					

					$count++;

					$row_count++;
					

					

					$total_payable_by_patient += $journal_amount;
					$total_payments += 0;
					$total_balance += $journal_amount;

					$grand_total += $journal_amount;
					$grand_balance += $journal_amount;


					$report[$row_count][0] = $count;
					$report[$row_count][1] = '';
					$report[$row_count][2] = '';
					$report[$row_count][3] = ucwords(strtolower($customer_name));
					$report[$row_count][4] = '';
					$report[$row_count][5] = '';
					$report[$row_count][6] = $journal_type_name;
					$report[$row_count][7] = '';
					$report[$row_count][8] = '';
					$report[$row_count][9] = $ageing;
					$report[$row_count][10] = $journal_date;
					$report[$row_count][11] = $invoice_number;					
					$report[$row_count][12] = number_format($journal_amount,2);			
					$report[$row_count][13] = number_format($journal_amount,2);
					$report[$row_count][14] = '';
					
				}
			}

		
			$visit_type = $this->session->userdata('statements_visit_type');

			

			$this->db->select('SUM(payment_item.payment_item_amount) AS total_payment_amount,payments.*,patients.*,payment_item.invoice_type');
			$this->db->where('payments.patient_id = patients.patient_id AND payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND payments.payment_type = '.$visit_type.' AND  payment_item.invoice_type >= 2 AND payment_item.invoice_type <> 3 AND YEAR(payments.payment_date) = '.$i);
			$this->db->group_by('payment_item.payment_id');
			$query_three = $this->db->get('payments,payment_item,patients');

			if($query_three->num_rows() > 0)
			{
				$row_count++;
				foreach ($query_three->result() as $key => $value5) {
					# code...
					$patient_number = $value5->patient_number;
					$patient_surname = $value5->patient_surname;
					$patient_othernames = $value5->patient_othernames;
					$patient_first_name = $value5->patient_first_name;
					$receipt_number = $value5->receipt_number;
					$payment_date = $value5->payment_date;
					$patient_id = $value5->patient_id;
					$total_amount = $value5->total_payment_amount;
					$payment_amount = $value5->total_payment_amount;
					$invoice_type = $value5->invoice_type;

				
					
					// // $journal_type_name = 'Payment';

					if($invoice_type == 5)
					{
						$journal_type_name = 'Refund';
						$journal_balance = $total_amount;
					}
					else
					{
						$journal_type_name = 'Payment';

						$journal_balance = -$total_amount;
					}
					
					

					// calculate the balance = 


					

					if($journal_balance != 0)
					{
						$count++;

						$row_count++;
						$total_payable_by_patient += $journal_amount;
						$total_payments += $payment_amount;
						$total_balance += $journal_balance;

						$grand_total += $journal_amount;
						$grand_balance += $journal_balance;




						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = '';
						$report[$row_count][5] = '';
						$report[$row_count][6] = $journal_type_name;
						$report[$row_count][7] = '';
						$report[$row_count][8] = '';
						$report[$row_count][9] = '';
						$report[$row_count][10] = $journal_date;
						$report[$row_count][11] = $invoice_number;					
						$report[$row_count][12] = number_format($journal_amount,2);			
						$report[$row_count][13] = number_format($journal_balance,2);
						$report[$row_count][14] = '';
						
					}
					
				}

			}




			$this->db->where('patients_journals.patient_id = patients.patient_id AND patients_journals.visit_invoice_id = 0 AND patients_journals.account_to_id = 19 AND  patients_journals.visit_type_id ='.$visit_type.' AND YEAR(patients_journals.journal_date) = '.$i);
				$query_three = $this->db->get('patients_journals,patients');

			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) 
				{
					# code...
					$patient_number = $value5->patient_number;
					$patient_surname = $value5->patient_surname;
					$patient_othernames = $value5->patient_othernames;
					$patient_first_name = $value5->patient_first_name;
					$journal_amount = $value5->journal_amount;
					$invoice_number = $value5->invoice_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->journal_type;
					$journal_amount = $value5->journal_amount;
					$patient_id = $value5->patient_id;

					

					if($journal_type == 1)
					{
						$journal_type_name = 'General Journal';
					}
					else
					{
						// $journal_type_name = 'Payment';
						$journal_type_name = 'General Journal';
					}

					// calculate the balance = 

					$payment_amount = $this->debtors_model->get_patient_account_payments($patient_id,$visit_type);
					$journal_payment = $this->debtors_model->get_patient_account_journals($patient_id,$visit_type);

					
					$journal_balance = ($journal_amount + $journal_payment) - $payment_amount;

					if($journal_balance > 0 OR $journal_balance < 0)
					{
						$count++;
						$row_count++;


						$total_payable_by_patient += $journal_balance;
						$total_payments += $payment_amount;
						$total_balance += $journal_balance;


						$grand_total += $journal_amount;
						$grand_balance += $journal_balance;


						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = '';
						$report[$row_count][5] = '';
						$report[$row_count][6] = $journal_type_name;
						$report[$row_count][7] = '';
						$report[$row_count][8] = '';
						$report[$row_count][9] = '';
						$report[$row_count][10] = $journal_date;
						$report[$row_count][11] = $invoice_number;					
						$report[$row_count][12] = number_format($journal_amount,2);			
						$report[$row_count][13] = number_format($journal_balance,2);
						$report[$row_count][14] = '';
						
					}
				}
			}


			$this->db->where('patients_journals.patient_id = patients.patient_id  AND patients_journals.account_from_id = 19 AND patients_journals.journal_type = 6 AND  patients_journals.visit_type_id ='.$visit_type.' AND YEAR(patients_journals.journal_date) = '.$i);
				$query_three = $this->db->get('patients_journals,patients');

			if($query_three->num_rows() > 0)
			{
				foreach ($query_three->result() as $key => $value5) 
				{
					# code...
					$patient_number = $value5->patient_number;
					$patient_surname = $value5->patient_surname;
					$patient_othernames = $value5->patient_othernames;
					$patient_first_name = $value5->patient_first_name;
					$journal_amount = $value5->journal_amount;
					$invoice_number = $value5->invoice_number;
					$journal_date = $value5->journal_date;
					$journal_type = $value5->journal_type;
					$journal_amount = $value5->journal_amount;
					$patient_id = $value5->patient_id;

					

					if($journal_type == 1)
					{
						$journal_type_name = 'General Journal';
					}
					else
					{
						// $journal_type_name = 'Payment';
						$journal_type_name = 'General Journal';
					}

					// calculate the balance = 

					
						$count++;
						$row_count++;


						$total_payable_by_patient += $journal_amount;
						$total_payments += 0;
						$total_balance += $journal_amount;


						$grand_total += $journal_amount;
						$grand_balance += $journal_amount;


						$report[$row_count][0] = $count;
						$report[$row_count][1] = '';
						$report[$row_count][2] = $patient_number;
						$report[$row_count][3] = ucwords(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name));
						$report[$row_count][4] = '';
						$report[$row_count][5] = '';
						$report[$row_count][6] = $journal_type_name;
						$report[$row_count][7] = '';
						$report[$row_count][8] = '';
						$report[$row_count][9] = '';
						$report[$row_count][10] = $journal_date;
						$report[$row_count][11] = $invoice_number;					
						$report[$row_count][12] = number_format($journal_amount,2);			
						$report[$row_count][13] = number_format($journal_amount,2);
						$report[$row_count][14] = '';
						
					
				}
			}


			if($total_payable_by_patient > 0 OR $total_balance > 0)
			{
				$row_count++;
			
				$report[$row_count][0] = '';
				$report[$row_count][1] = '';
				$report[$row_count][2] = '';
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = '';
				$report[$row_count][8] = '';
				$report[$row_count][9] = '';
				$report[$row_count][10] = '';
				$report[$row_count][11] = 'Total '.$i;					
				$report[$row_count][12] = number_format($total_payable_by_patient,2);			
				$report[$row_count][13] = number_format($total_balance,2);
				$report[$row_count][14] = '';

			}


			
			$row_count++;

			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';					
			$report[$row_count][10] = '';			
			$report[$row_count][11] = '';
			$report[$row_count][12] = '';
			$report[$row_count][13] = '';
			$report[$row_count][14] = '';

			// add the unallocated payments
		}



		$row_count++;
		$report[$row_count][0] = '';
		$report[$row_count][1] = 'Unallocated Payments';
		$report[$row_count][2] = '';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';					
		$report[$row_count][10] = '';			
		$report[$row_count][11] = '';
		$report[$row_count][12] = '';
		$report[$row_count][13] = '';
		$report[$row_count][14] = '';


		$visit_type = $this->session->userdata('statements_visit_type');

		$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type);
		$query = $this->db->get('batch_unallocations,batch_receipts');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$payment_date = $value->payment_date;
						
				$payment_date = $value->payment_date;
				$receipt_number = $value->receipt_number;
				$allocation_type_id = $value->allocation_type_id;


				if($allocation_type_id == 1)
				{
					$amount = $value->amount_paid;
				}
				else
				{
					$amount = -$value->amount_paid;
				}

				$payment_date = date('d.m.y',strtotime($value->payment_date));

				$total_payable_by_patient += $amount;

				$grand_total += $amount;
				$grand_balance += $amount;


				$visit_type_name = $this->session->userdata('statements_visit_type_name');
				$row_count++;
				$report[$row_count][0] = '';
				$report[$row_count][1] = 'Insurance - '.$visit_type_name;
				$report[$row_count][2] = '';
				$report[$row_count][3] = '';
				$report[$row_count][4] = '';
				$report[$row_count][5] = '';
				$report[$row_count][6] = '';
				$report[$row_count][7] = '';
				$report[$row_count][8] = 'Payment';
				$report[$row_count][9] = '';
				$report[$row_count][10] = $payment_date;					
				$report[$row_count][11] = $receipt_number;			
				$report[$row_count][12] = number_format($amount,2);
				$report[$row_count][13] = number_format($amount,2);
				$report[$row_count][14] = '';
			}

			$row_count++;
			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = '';
			$report[$row_count][3] = '';
			$report[$row_count][4] = '';
			$report[$row_count][5] = '';
			$report[$row_count][6] = '';
			$report[$row_count][7] = '';
			$report[$row_count][8] = '';
			$report[$row_count][9] = '';					
			$report[$row_count][10] = '';			
			$report[$row_count][11] = '';
			$report[$row_count][12] = '';
			$report[$row_count][13] = '';

			
		}




		
		$statement_date = date('d.m.y',strtotime($statement_date));
		$row_count++;
		$report[$row_count][0] = '';
		$report[$row_count][1] = 'Total owed as at '.$statement_date;
		$report[$row_count][2] = '';
		$report[$row_count][3] = '';
		$report[$row_count][4] = '';
		$report[$row_count][5] = '';
		$report[$row_count][6] = '';
		$report[$row_count][7] = '';
		$report[$row_count][8] = '';
		$report[$row_count][9] = '';
		$report[$row_count][10] = '';		
		$report[$row_count][11] = '';					
		$report[$row_count][12] = number_format($grand_total,2);			
		$report[$row_count][13] = number_format($grand_balance,2);
		$report[$row_count][14] = '';


		// var_dump($report);die();
		
		//create the excel document
		// $sheet->setCellValue('A1', 'Hello World');

		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_aging_time($date_from)
	{

		$date = $this->session->userdata('statements_visit_date');

		if(!empty($date))
		{
			$date_to = $date;
		}
		else
		{
			$date_to = date('Y-m-d');
		}

		$diff = abs(strtotime($date_to)-strtotime($date_from));
		$years = floor($diff / (365*60*60*24));
		// var_dump($years);die();
		$days = round($diff / (60 * 60 * 24));

		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));


		if($years > 0)
		{
			$months += $years*12;
		}

		return $days;
	}

	public function get_statements_of_accounts_old($table,$where)
	{
		//retrieve all users
		$this->db->from($table);

		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id,visit_invoice.member_number,visit_invoice.authorising_officer,visit_invoice.scheme_name,visit_invoice.preauth_date,visit_invoice.created AS invoice_date');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('visit_invoice.created','ASC');


		$query = $this->db->get('');
		
		return $query;
	}

	public function get_statements_of_accounts($table,$where)
	{
		//retrieve all users
		$this->db->from($table);

		$this->db->select('visit_invoice.*,patients.*,visit_invoice.created AS invoice_date,visit_invoice.scheme_name AS scheme');
		$this->db->join('patients', 'patients.patient_id = visit_invoice.patient_id', 'left');

		$this->db->where($where);
		$this->db->order_by('visit_invoice.created','ASC');


		$query = $this->db->get('');
		
		return $query;
	}
	public function get_patient_account_payments($patient_id,$visit_type_id)
	{
		
		$this->db->select('SUM(payment_item_amount) AS total_amount');
		$this->db->where('payments.cancel = 0 AND payment_item.invoice_type = 3  AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id and payments.visit_type_id = '.$visit_type_id.' AND payments.patient_id ='.$patient_id);
	
	

		$query = $this->db->get('payments,payment_item');

		


		

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;

		
	}

	public function get_patient_account_journals($patient_id,$visit_type_id)
	{
		
		$this->db->select('SUM(journal_amount) AS total_amount');
		$this->db->where('patients_journals.visit_type_id = '.$visit_type_id.' AND patients_journals.visit_invoice_id = 0 AND patients_journals.account_from_id = 19 AND patients_journals.patient_id ='.$patient_id);
	
		$query = $this->db->get('patients_journals');

		


		

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;

		
	}

	public function get_invoice_balances_per_visit()
	{
		$customer_balance_search_dates = $this->session->userdata('statements_visit_invoices');
		$add = '';
		if(!empty($customer_balance_search_dates))
		{
			$add = $customer_balance_search_dates;
		}

		$visit_type_id = $this->session->userdata('statements_visit_type');

		// var_dump($visit_type_id);die();

		$select_statement = "SELECT 
									patients.patient_surname,patients.patient_othernames,
									patients.patient_first_name,
									patients.patient_number,
									SUM(data.dr_amount +data.cr_amount) AS balance,

									visit_invoice.*,
									visit_invoice.created as date_invoice

								FROM 
								(
									SELECT
											visit_invoice.visit_invoice_id AS transaction_id,
											visit_invoice.visit_invoice_id AS reference_id,
											visit_invoice.visit_invoice_number AS reference_code,
											visit_invoice.patient_id AS patient_id,
											visit_invoice.dentist_id  AS personnel_id,
											visit_invoice.bill_to AS payment_type,
											visit_type.visit_type_name AS payment_type_name,
											CONCAT( '<strong>Invoice </strong> <br> Invoice Number :  ', visit_invoice.visit_invoice_number,'<br> <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transaction_description,
											visit_invoice.invoice_bill AS dr_amount,
											'0' AS cr_amount,
											visit_invoice.created AS invoice_date,
											visit_invoice.created AS transaction_date,
											2 AS branch_id,
											'1' AS party,
											'Invoice' AS transactionCategory,
											'Invoice Patients' AS transactionClassification,
											'visit_charge' AS transactionTable,
											'visit' AS referenceTable
									FROM
												visit_invoice,visit_type
											WHERE 
											visit_invoice.visit_invoice_delete = 0 AND  visit_invoice.bill_to = visit_type.visit_type_id
									


								 UNION ALL

								SELECT
										visit_credit_note.visit_credit_note_id AS transaction_id,
										visit_invoice.visit_invoice_id AS reference_id,
										visit_credit_note.visit_cr_note_number AS reference_code,
										visit_credit_note.patient_id AS patient_id,
										visit_invoice.dentist_id AS personnel_id,
										visit_invoice.bill_to AS payment_type,
										visit_type.visit_type_name AS payment_type_name,
										CONCAT( '<strong>Credit Note for </strong> <br> Invoice Number: ', visit_invoice.visit_invoice_number,'<br><strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transaction_description,
										'0' AS dr_amount,
										-(visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units) AS cr_amount,
										visit_invoice.created AS invoice_date,
										visit_credit_note.created AS transaction_date,
										2 AS branch_id,
										'2' AS party,
										'Credit Note' AS transactionCategory,
										'Invoice Patients' AS transactionClassification,
										'creditnte' AS transactionTable,
										'visit' AS referenceTable
									FROM
										visit_credit_note_item,visit_credit_note,visit_invoice,visit_type
										
										WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

										AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
										AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
										AND visit_credit_note_item.visit_cr_note_amount > 0
										AND visit_type.visit_type_id = visit_invoice.bill_to

									-- GROUP BY visit_credit_note_item.visit_credit_note_id




									UNION ALL


									SELECT
										payments.payment_id AS transaction_id,
										visit_invoice.visit_invoice_id AS reference_id,
										payments.confirm_number AS reference_code,
										payment_item.patient_id AS patient_id,
										visit_invoice.dentist_id AS personnel_id,
										visit_invoice.bill_to AS payment_type,
										visit_type.visit_type_name AS payment_type_name,
										CONCAT( '<strong>Payment</strong> <br> Invoice Number: ', visit_invoice.visit_invoice_number,'<br><strong>Method:</strong> ',payment_method.payment_method,'<br><strong>Receipt No:</strong> ',payments.confirm_number) AS transaction_description,
										'0' AS dr_amount,
										-(payment_item.payment_item_amount) AS cr_amount,
										visit_invoice.created AS invoice_date,
										payments.payment_date AS transaction_date,
										2 AS branch_id,
										'2' AS party,
										'Payment' AS transactionCategory,
										'Invoice Patients Payment' AS transactionClassification,
										'payments' AS transactionTable,
										'visit' AS referenceTable
									FROM
										payments,payment_item,visit_invoice,payment_method,visit_type
										WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 1
										AND payments.payment_id = payment_item.payment_id 
										AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id 
										AND payments.payment_method_id = payment_method.payment_method_id
										AND visit_invoice.bill_to = visit_type.visit_type_id
									
									-- GROUP BY visit_invoice.visit_invoice_id




									UNION ALL

									SELECT
											patients_journals.patient_journal_id AS transaction_id,
											visit_invoice.visit_invoice_id AS reference_id,
											'' AS reference_code,
											patients_journals.patient_id AS patient_id,
											''  AS personnel_id,
											patients_journals.visit_type_id AS payment_type,
											visit_type.visit_type_name AS payment_type_name,
											patients_journals.journal_reason AS transaction_description,
											0 AS dr_amount,
											patients_journals.journal_amount AS cr_amount,
											patients_journals.journal_date AS invoice_date,
											patients_journals.journal_date AS transaction_date,
											2 AS branch_id,
											'1' AS party,
											'Journal' AS transactionCategory,
											'JOURNAL' AS transactionClassification,
											'visit_charge' AS transactionTable,
											'visit' AS referenceTable
									FROM
												patients_journals,visit_type,visit_invoice
											WHERE 
											patients_journals.visit_type_id = visit_type.visit_type_id AND patients_journals.visit_invoice_id = visit_invoice.visit_invoice_id

		


								) AS data,patients,visit_invoice WHERE data.patient_id = patients.patient_id AND visit_invoice.visit_invoice_id = data.reference_id    AND data.payment_type = '".$visit_type_id."' $add  GROUP BY data.reference_id  ORDER BY data.reference_id ASC";
	


		
		$query = $this->db->query($select_statement);

		
		// var_dump($product_value);
		return  $query;
	

	}


	public function get_invoice_balances_per_visit_charge($i)
	{
		$customer_balance_search_dates = $this->session->userdata('statements_visit_invoices');
		$add = '';
		if(!empty($customer_balance_search_dates))
		{
			$add = $customer_balance_search_dates;
		}

		$visit_type_id = $this->session->userdata('statements_visit_type');

		// var_dump($visit_type_id);die();

		$select_statement = "SELECT 
									patients.patient_surname,patients.patient_othernames,
									patients.patient_first_name,
									patients.patient_number,
									SUM(data.dr_amount +data.cr_amount) AS balance,

									visit_invoice.*,
									visit_invoice.created as date_invoice

								FROM 
								(
									SELECT
											visit_invoice.visit_invoice_id AS transaction_id,
											visit_invoice.visit_invoice_id AS reference_id,
											visit_invoice.visit_invoice_number AS reference_code,
											visit_invoice.patient_id AS patient_id,
											visit_invoice.dentist_id  AS personnel_id,
											visit_invoice.bill_to AS payment_type,
											visit_type.visit_type_name AS payment_type_name,
											CONCAT( '<strong>Invoice </strong> <br> Invoice Number :  ', visit_invoice.visit_invoice_number,'<br> <strong>Bill to:</strong>  ', visit_type.visit_type_name ) AS transaction_description,
											visit_invoice.invoice_bill AS dr_amount,
											'0' AS cr_amount,
											visit_invoice.created AS invoice_date,
											visit_invoice.created AS transaction_date,
											2 AS branch_id,
											'1' AS party,
											'Invoice' AS transactionCategory,
											'Invoice Patients' AS transactionClassification,
											'visit_charge' AS transactionTable,
											'visit' AS referenceTable
									FROM
												visit_invoice,visit_type
											WHERE 
											visit_invoice.visit_invoice_delete = 0 AND  visit_invoice.bill_to = visit_type.visit_type_id
									


								 UNION ALL

								SELECT
										visit_credit_note.visit_credit_note_id AS transaction_id,
										visit_invoice.visit_invoice_id AS reference_id,
										visit_credit_note.visit_cr_note_number AS reference_code,
										visit_credit_note.patient_id AS patient_id,
										visit_invoice.dentist_id AS personnel_id,
										visit_invoice.bill_to AS payment_type,
										visit_type.visit_type_name AS payment_type_name,
										CONCAT( '<strong>Credit Note for </strong> <br> Invoice Number: ', visit_invoice.visit_invoice_number,'<br><strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transaction_description,
										'0' AS dr_amount,
										-(visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units) AS cr_amount,
										visit_invoice.created AS invoice_date,
										visit_credit_note.created AS transaction_date,
										2 AS branch_id,
										'2' AS party,
										'Credit Note' AS transactionCategory,
										'Invoice Patients' AS transactionClassification,
										'creditnte' AS transactionTable,
										'visit' AS referenceTable
									FROM
										visit_credit_note_item,visit_credit_note,visit_invoice,visit_type
										
										WHERE visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0 

										AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id 
										AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id 
										AND visit_credit_note_item.visit_cr_note_amount > 0
										AND visit_type.visit_type_id = visit_invoice.bill_to

									-- GROUP BY visit_credit_note_item.visit_credit_note_id




									UNION ALL


									SELECT
										payments.payment_id AS transaction_id,
										visit_invoice.visit_invoice_id AS reference_id,
										payments.confirm_number AS reference_code,
										payment_item.patient_id AS patient_id,
										visit_invoice.dentist_id AS personnel_id,
										visit_invoice.bill_to AS payment_type,
										visit_type.visit_type_name AS payment_type_name,
										CONCAT( '<strong>Payment</strong> <br> Invoice Number: ', visit_invoice.visit_invoice_number,'<br><strong>Method:</strong> ',payment_method.payment_method,'<br><strong>Receipt No:</strong> ',payments.confirm_number) AS transaction_description,
										'0' AS dr_amount,
										-(payment_item.payment_item_amount) AS cr_amount,
										visit_invoice.created AS invoice_date,
										payments.payment_date AS transaction_date,
										2 AS branch_id,
										'2' AS party,
										'Payment' AS transactionCategory,
										'Invoice Patients Payment' AS transactionClassification,
										'payments' AS transactionTable,
										'visit' AS referenceTable
									FROM
										payments,payment_item,visit_invoice,payment_method,visit_type
										WHERE payments.cancel = 0 AND payments.payment_type = 1 AND payment_item.invoice_type = 1
										AND payments.payment_id = payment_item.payment_id 
										AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id 
										AND payments.payment_method_id = payment_method.payment_method_id
										AND visit_invoice.bill_to = visit_type.visit_type_id
									
									-- GROUP BY visit_invoice.visit_invoice_id




									UNION ALL

									SELECT
											patients_journals.patient_journal_id AS transaction_id,
											visit_invoice.visit_invoice_id AS reference_id,
											'' AS reference_code,
											patients_journals.patient_id AS patient_id,
											''  AS personnel_id,
											patients_journals.visit_type_id AS payment_type,
											visit_type.visit_type_name AS payment_type_name,
											patients_journals.journal_reason AS transaction_description,
											0 AS dr_amount,
											patients_journals.journal_amount AS cr_amount,
											patients_journals.journal_date AS invoice_date,
											patients_journals.journal_date AS transaction_date,
											2 AS branch_id,
											'1' AS party,
											'Journal' AS transactionCategory,
											'JOURNAL' AS transactionClassification,
											'visit_charge' AS transactionTable,
											'visit' AS referenceTable
									FROM
												patients_journals,visit_type,visit_invoice
											WHERE 
											patients_journals.visit_type_id = visit_type.visit_type_id AND patients_journals.visit_invoice_id = visit_invoice.visit_invoice_id

		


								) AS data,patients,visit_invoice WHERE data.patient_id = patients.patient_id AND visit_invoice.visit_invoice_id = data.reference_id    AND data.payment_type = '".$visit_type_id."' AND YEAR(data.invoice_date) = $i $add  GROUP BY data.reference_id   ORDER BY data.reference_id ASC";
	


		
		$query = $this->db->query($select_statement);

		
		// var_dump($product_value);
		return  $query;
	

	}

	public function create_visit_journal_number()
	{
		//select product code
		$this->db->where('patients_journal_number > 2441');
		$this->db->from('patients_journals');
		$this->db->select('patients_journal_number AS number');
		$this->db->order_by('patients_journal_number','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query->result()); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			
			if(empty($number))
			{
				$number = 2441;
			}
			else
			{
				$number++;//go to the next number
			}

			
			
		}
		else{//start generating receipt numbers
			
			$number= 2441;
		}
		return $number;
	}
	public function get_all_uncleared_invoices()
	{

  		$patient_surname = $this->input->post('patient_surname');
  		$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			$invoice_number = $this->input->post('invoice_number');
			$invoice_status = $this->input->post('invoice_status');
			$visit_type_id = $this->input->post('visit_type_id');
			$bill_status = $this->input->post('bill_status');

			$add = '';
			if(!empty($patient_surname))
			{
				// $search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
				$surnames = explode(" ",$_POST['patient_surname']);
				$total = count($surnames);
				
				$count = 1;
				$add .= ' AND (';
				for($r = 0; $r < $total; $r++)
				{
					if($count == $total)
					{
						$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\')';
					}
					
					else
					{
						$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
					}
					$count++;
				}
				$add .= ') ';
			}


			if(!empty($date_from) AND !empty($date_to) )
			{
				$add .= ' AND data.invoice_date >= "'.$date_from.'" AND data.invoice_date <= "'.$date_to.'"';
			}
			else if(empty($date_from) AND !empty($date_to))
			{
				$add .= ' AND data.invoice_date = "'.$date_to.'"';
			}
			else if(!empty($date_from) AND empty($date_to))
			{
				$add .= ' AND data.invoice_date = "'.$date_from.'"';
			}
			else
			{
				$date_from = date('Y-m-01');
				$date_to = date('Y-m-t');
				$add .= ' AND data.invoice_date >= "'.$date_from.'" AND data.invoice_date <= "'.$date_to.'"';
			}


			if(!empty($invoice_number))
			{
				$add .= ' AND data.invoice_number = "'.$invoice_number.'"';
			}


			if($invoice_status != 2)
			{


				$add .= ' AND data.invoice_status = "'.$invoice_status.'"';
			}


			if(!empty($visit_type_id))
			{

				$add .= ' AND data.bill_to = "'.$visit_type_id.'"';
			}


			if(!empty($bill_status))
			{
				if($bill_status == 1)
				{
					$add .= ' AND data.invoice_balance <= 0';
				}
				else if($bill_status == 2)
				{
					$add .= ' AND data.invoice_balance > 0 ';
				}
				
			}

		// invoice number

	    // $this->db->where('v_creditors_invoice_balances.creditor_id = '.$creditor_id);
	    // $this->db->select('*');
	    // return $this->db->get('v_creditors_invoice_balances');

    	$select_statement = "
                        SELECT
                          data.invoice_id AS invoice_id,
                          data.invoice_number AS invoice_number,
                          data.invoice_date AS invoice_date,
                          data.patient_id AS patient_id,
                          patients.patient_surname AS patient_name,
                          data.invoice_status AS invoice_status,
                          data.open_status AS open_status,
                          data.visit_type_name AS visit_type_name,
                          data.bill_to AS bill_to,
                          data.visit_id AS visit_id,
                          data.visit_date AS visit_date,
                          data.inpatient AS inpatient,
                          data.invoice_bill AS invoice_bill,
                          data.invoice_credit_note AS invoice_credit_note,
                          data.invoice_payments AS invoice_payments,
                          data.invoice_journal AS invoice_journal,
                          data.invoice_balance AS invoice_balance,
                          data.visit_closed AS visit_closed
                        FROM 
                        (
                          SELECT
                            `visit_invoice`.`patient_id` AS patient_id,
                            `visit_invoice`.`visit_invoice_id` as invoice_id,
                            `visit_invoice`.`visit_invoice_number` as invoice_number,
                            `visit_invoice`.`created` as invoice_date,
                            visit_invoice.bill_to AS bill_to,
                            visit_type.visit_type_name AS visit_type_name,
                            visit_invoice.open_status AS open_status,
                            visit_invoice.visit_invoice_status AS invoice_status,
                            visit_invoice.invoice_bill AS invoice_bill,
                            visit_invoice.invoice_payments AS invoice_payments,
                            visit_invoice.invoice_credit_note AS invoice_credit_note,
                            visit_invoice.invoice_balance AS invoice_balance,
                            visit_invoice.invoice_journal AS invoice_journal,
                            visit.visit_date AS visit_date,
                            visit.visit_id AS visit_id,
                            visit.inpatient AS inpatient,
                            visit.close_card AS visit_closed
                            FROM (`visit`,visit_invoice,visit_type)
                            WHERE `visit`.`visit_id` = `visit_invoice`.`visit_id`
                            AND `visit_invoice`.`bill_to` = `visit_type`.`visit_type_id`
                            AND visit_invoice.visit_invoice_delete = 0
                            AND visit.visit_delete = 0


                          ) AS data,patients WHERE data.patient_id = patients.patient_id $add GROUP BY data.invoice_number ORDER BY data.invoice_date ASC ";
                          $query = $this->db->query($select_statement);
                  return $query;


  }


  public function get_all_uncleared_bills()
	{

  		$patient_surname = $this->input->post('patient_surname');
  		$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			$invoice_number = $this->input->post('invoice_number');
			$invoice_status = $this->input->post('invoice_status');
			$visit_type_id = $this->input->post('visit_type_id');
			$processed_bills  = $this->input->post('processed_bills');
			$bills_status  = $this->input->post('bills_status');

			$add = '';
			if(!empty($patient_surname))
			{
				// $search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
				$surnames = explode(" ",$_POST['patient_surname']);
				$total = count($surnames);
				
				$count = 1;
				$add .= ' AND (';
				for($r = 0; $r < $total; $r++)
				{
					if($count == $total)
					{
						$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\')';
					}
					
					else
					{
						$add .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
					}
					$count++;
				}
				$add .= ') ';
			}


			if(!empty($date_from) AND !empty($date_to) )
			{
				$add .= ' AND data.invoice_date >= "'.$date_from.'" AND data.invoice_date <= "'.$date_to.'"';
			}
			else if(empty($date_from) AND !empty($date_to))
			{
				$add .= ' AND data.invoice_date = "'.$date_to.'"';
			}
			else if(!empty($date_from) AND empty($date_to))
			{
				$add .= ' AND data.invoice_date = "'.$date_from.'"';
			}
			else
			{
				$date_from = date('Y-m-01');
				$date_to = date('Y-m-t');
				$add .= ' AND data.invoice_date >= "'.$date_from.'" AND data.invoice_date <= "'.$date_to.'"';
			}


			if(!empty($visit_type_id))
			{
				$add .= ' AND data.bill_to = "'.$visit_type_id.'"';
			}


			if($processed_bills > 0)
			{


				$add .= ' AND data.unprocessed_bill > 0';
			}

			if($bills_status > 0)
			{


				$add .= ' AND data.visit_balance > 0';
			}


			// if(!empty($visit_type_id))
			// {

			// 	$add .= ' AND data.bill_to = "'.$visit_type_id.'"';
			// }



		// invoice number

	    // $this->db->where('v_creditors_invoice_balances.creditor_id = '.$creditor_id);
	    // $this->db->select('*');
	    // return $this->db->get('v_creditors_invoice_balances');

    	$select_statement = "
                        SELECT
                          data.invoice_id AS invoice_id,
                          data.invoice_number AS invoice_number,
                          data.invoice_date AS invoice_date,
                          data.patient_id AS patient_id,
                          patients.patient_surname AS patient_name,
                          data.invoice_status AS invoice_status,
                          data.open_status AS open_status,
                          data.visit_type_name AS visit_type_name,
                          data.bill_to AS bill_to,
                          data.visit_id AS visit_id,
                          data.visit_date AS visit_date,
                          data.inpatient AS inpatient,
                          SUM(data.unprocessed_bill) AS unprocessed_bill,
                          SUM(data.processed_bill) AS processed_bill,
                          SUM(data.visit_balance) AS visit_balance,
                          data.visit_closed AS visit_closed
                        FROM 
                        (
                          SELECT
                            `visit`.`patient_id` AS patient_id,
                            `visit`.`visit_id` as invoice_id,
                            '' as invoice_number,
                            `visit`.`visit_date` as invoice_date,
                            visit.visit_type AS bill_to,
                            visit_type.visit_type_name AS visit_type_name,
                            0 AS open_status,
                            visit.close_card AS invoice_status,
                            visit_charge.visit_charge_units * visit_charge.visit_charge_amount AS unprocessed_bill,
                            0 AS processed_bill,
                            0 AS visit_balance,
                            visit.visit_date AS visit_date,
                            visit.visit_id AS visit_id,
                            visit.inpatient AS inpatient,
                            visit.close_card AS visit_closed
                            FROM (`visit`,visit_charge,visit_type)
                            WHERE `visit`.`visit_type` = `visit_type`.`visit_type_id`
                            AND visit_charge.visit_charge_delete = 0
                            AND visit_charge.charged = 1
                            AND visit_charge.visit_id = visit.visit_id
                            AND visit_charge.visit_invoice_id = 0
                            AND visit.visit_delete = 0

                         UNION ALL


                          SELECT
                            `visit_invoice`.`patient_id` AS patient_id,
                            `visit`.`visit_id`  as invoice_id,
                            '' as invoice_number,
                            `visit_invoice`.`created` as invoice_date,
                            visit.visit_type AS bill_to,
                            visit_type.visit_type_name AS visit_type_name,
                            0 AS open_status,
                            visit.close_card AS invoice_status,
                            0 AS unprocessed_bill,
                            visit_invoice.invoice_bill AS processed_bill,
                            visit_invoice.invoice_balance AS visit_balance,
                            visit.visit_date AS visit_date,
                            visit.visit_id AS visit_id,
                            visit.inpatient AS inpatient,
                            visit.close_card AS visit_closed
                            FROM (`visit`,visit_invoice,visit_type)
                            WHERE `visit`.`visit_id` = `visit_invoice`.`visit_id`
                            AND `visit_invoice`.`bill_to` = `visit_type`.`visit_type_id`
                            AND visit_invoice.visit_invoice_delete = 0
                            AND visit.visit_delete = 0



                          ) AS data,patients WHERE data.patient_id = patients.patient_id $add GROUP BY data.visit_id ORDER BY data.visit_date ASC ";
                          $query = $this->db->query($select_statement);
                  return $query;


  }

	


	// 

	
}

?>