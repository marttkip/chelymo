
<?php
       
        
        // $where = 'v_statement_of_accounts.dr_amount <> v_statement_of_accounts.cr_amount AND v_statement_of_accounts.payment_type = '.$insurance_id;
        // $table = 'v_statement_of_accounts';

        // $where = 'payments.batch_receipt_id = '.$batch_receipt_id.' AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND payments.payment_id = payment_item.payment_id AND payments.cancel = 0 AND patients.patient_id = visit_invoice.patient_id AND visit_invoice.bill_to = '.$insurance_id;
        
        // $table = 'visit_invoice,payment_item,payments,patients';
        
    
        // $query = $this->debtors_model->get_paid_invoices($table, $where);


        $result = '';
        
        //if users exist display them
        if ($query->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed table-linked">
                <thead>
                    <tr>
                    
                        <th>#</th>
                        <th>Name</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Amount Paid</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
           
            
            foreach ($query->result() as $row)
            {
               

                $transaction_id = $row->visit_invoice_id;
                $patient_name = $row->patient_surname.' '.$row->patient_othernames;
                $reference_code = $row->visit_invoice_number;
                $invoice_date = $row->invoice_date;
                $amount_to_pay = $row->payment_item_amount;



                $count++;
                $result .= 
                '
                    <tr>
                       
                        <td >'.$count.'</td>
                        <td >'.$patient_name.'</td>
                        <td>'.$invoice_date.'</td>
                        <td >'.$reference_code.'</td>
                        <td>'.number_format($amount_to_pay,2).'</td>
                       
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no selected invoices";
        }
?>

<div class="table-responsive">
            
    <?php echo $result;?>

</div>