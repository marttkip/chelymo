<?php
        $where = 'batch_receipts.bank_id = account.account_id AND batch_receipts.batch_receipt_id = '.$batch_receipt_id;
        $table = 'batch_receipts,account';
        
    
        $transaction_detail_rs = $this->debtors_model->get_all_unpaid_invoices($table, $where);

        if($transaction_detail_rs->num_rows() > 0)
        {
            foreach ($transaction_detail_rs->result() as $key => $value) {
                # code...
                $account_name = $value->account_name;
                $receipt_number = $value->receipt_number;
                $payment_date = $value->payment_date;
                $total_amount_paid = $value->total_amount_paid;
                $bank_id = $value->bank_id;
                $payment_date = $value->payment_date;
                $receipt_number = $value->receipt_number;

            }
        }
        
        // $where = 'v_statement_of_accounts.dr_amount <> v_statement_of_accounts.cr_amount AND v_statement_of_accounts.payment_type = '.$insurance_id;
        // $table = 'v_statement_of_accounts';

        // $where = 'visit_invoice.visit_invoice_status <> 1 AND patients.patient_id = visit_invoice.patient_id  AND visit_invoice.invoice_balance >  0 AND visit_invoice.batch_receipt_id <> '.$batch_receipt_id.' AND visit_invoice.bill_to = '.$insurance_id;
        
        // $table = 'visit_invoice,patients';
        
    
        // $query = $this->debtors_model->get_all_unpaid_invoices($table, $where);


        $result = '';
        
        //if users exist display them
        if ($query->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-condensed table-bordered table-striped table-condensed table-linked" id="PATIENTS INVOICES">
                <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Amount Invoiced</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                  
            ';
            
            //get all administrators
           
            $total_invoice_amount = 0;
            foreach ($query->result() as $row)
            {
               

                $visit_invoice_id = $row->visit_invoice_id;
                $patient_name = $row->patient_surname.' '.$row->patient_othernames;
                $visit_invoice_number = $row->visit_invoice_number;
                 $invoice_bill = $row->invoice_bill;
                 $invoice_balance = $row->invoice_balance;
                $invoice_date = $row->created;



                // $dr_amount =  $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
                // $total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
                // $credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

                $dr_amount = $balance = $dr_amount - ($total_payments+$credit_note);
                // $cr_amount = $row->cr_amount;
                $status = $row->status;
                $patient_id = $row->patient_id;
                $invoice_date = date('jS M Y',strtotime($row->created));

                if($status == 0)
                {
                	$color ='warning';
                	$status = 'Not Reconcilled';
                }
                else
                {
                	$color = 'success';
                	$status = 'Reconcilled';
                }
                 $checkbox_data = array(
                                        'name'        => 'visit_invoices[]',
                                        'id'          => 'checkbox',
                                        'class'          => 'css-checkbox  lrg ',
                                        // 'checked'=>'checked',
                                        'value'       => $visit_invoice_id,
                                        'onclick'=>'get_values('.$visit_invoice_id.','.$batch_receipt_id.')'
                                      );
                $count++;
                $total_invoice_amount += $invoice_balance;
                $result .= 
                '
                    <tr>
                        <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
                        <td >'.$count.'</td>
                        <td >'.$patient_name.'</td>
                        <td>'.$invoice_date.'</td>
                        <td >'.$visit_invoice_number.'</td>
                        <td>'.number_format($invoice_bill,2).'<input type="hidden" class="form-control" colspan="3" name="invoiced_amount'.$visit_invoice_id.'" id="invoiced_amount'.$visit_invoice_id.'" value="'.$invoice_bill.'" />
                        <input type="hidden" class="form-control" colspan="3" name="patient_id'.$visit_invoice_id.'" id="patient_id'.$visit_invoice_id.'" value="'.$patient_id.'"/></td>
                        
                        <td>'.number_format($invoice_balance,2).'</td>
                       
                    </tr> 
                ';
            }
            
            $result .= 
                    ' </tbody>
                       <tfoot>  
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>TOTALS</th>
                                <th>'.number_format($total_invoice_amount,2).'</th>
                            
                        </tfoot>          
                    </table>
                    ';
        }
        
        else
        {
            $result .= "There are no remitance uploaded";
        }
?>

<!-- <div class="table-responsive"> -->
            
    <?php echo $result;?>

<!-- </div> -->