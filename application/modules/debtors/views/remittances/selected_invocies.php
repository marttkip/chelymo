
<?php
       
        
        // $where = 'v_statement_of_accounts.dr_amount <> v_statement_of_accounts.cr_amount AND v_statement_of_accounts.payment_type = '.$insurance_id;
        // $table = 'v_statement_of_accounts';
        


        $result = '';

        $updated_balance = 0;
        
        //if users exist display them
        if ($query->num_rows() > 0)
        {
            $count = 0;
            
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed table-linked" id="SELECTED INVOICES">
                <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Invoice Date</th>
                        <th>Invoice Number</th>
                        <th>Amount Invoiced</th>
                        <th>Amount Paid</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
            //get all administrators
           
            
            foreach ($query->result() as $row)
            {
               

                $visit_invoice_id = $row->visit_invoice_id;
                $patient_name = $row->patient_surname.' '.$row->patient_othernames;
                $visit_invoice_number = $row->visit_invoice_number;
                $invoice_date = $row->created;
                $amount_to_pay = $row->amount_to_pay;
                $invoice_balance = $row->invoice_balance;



                $dr_amount = $row->invoice_balance;
                // $total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
                // $credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

                // $dr_amount = $balance = $dr_amount - ($total_payments+$credit_note);
                // $cr_amount = $row->cr_amount;
                $status = $row->status;
                $patient_id = $row->patient_id;
                $invoice_date = date('jS M Y',strtotime($row->created));

                if($status == 0)
                {
                	$color ='warning';
                	$status = 'Not Reconcilled';
                }
                else
                {
                	$color = 'success';
                	$status = 'Reconcilled';
                }
                 $checkbox_data = array(
                                        'name'        => 'visit_invoices[]',
                                        'id'          => 'checkbox',
                                        'class'          => 'css-checkbox  lrg ',
                                        'checked'=>'checked',
                                        'value'       => $visit_invoice_id,
                                        'onclick'=>'get_values('.$visit_invoice_id.','.$batch_receipt_id.')'
                                      );
                  $updated_balance += $amount_to_pay;
                $count++;
                $result .= 
                '
                    <tr>
                        <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
                        <td >'.$count.'</td>
                        <td >'.$patient_name.'</td>
                        <td>'.$invoice_date.'</td>
                        <td >'.$visit_invoice_number.'</td>
                        <td>'.number_format($dr_amount,2).'<input type="hidden" class="form-control" colspan="3" name="invoiced_amount'.$visit_invoice_id.'" id="invoiced_amount'.$visit_invoice_id.'" value="'.$dr_amount.'" />
                        <input type="hidden" class="form-control" colspan="3" name="patient_id'.$visit_invoice_id.'" id="patient_id'.$visit_invoice_id.'" value="'.$patient_id.'"/></td>
                        <td><input type="text" class="form-control" colspan="3" name="amount_paid'.$visit_invoice_id.'" id="amount_paid'.$visit_invoice_id.'" value="'.$amount_to_pay.'" onkeyup="update_amount_to_pay('.$visit_invoice_id.')"/></td>
                     
                       
                    </tr> 
                ';
            }
            
            $result .= 
                        '</tbody>
                            <tfoot>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>TOTALS</th>
                                <th>'.number_format($updated_balance,2).'</th>
                            
                            </tfoot>
                        </table>
                        ';
        }
        
        else
        {
            $result .= "There are no selected invoices";
        }
?>


            
    <?php echo $result;?>

