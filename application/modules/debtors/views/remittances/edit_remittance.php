<?php
$patient_items = '';
$visit_type_id = null;
$close_card = 3;
$visit_type_id = 1;
if($query->num_rows() > 0)
{
    foreach ($query->result() as $key => $res) 
    {
        # code...

        $visit_date = date('D M d Y',strtotime($res->visit_date)); 
        $receipt_number = $res->receipt_number;
        $invoice_number = $res->invoice_number;
        $payment_date = $res->payment_date;
        $bank_id = $res->bank_id;
        // $patient_id = $res->patient_id;
        $insurance_id = $res->insurance_id;
        $total_amount_paid = $res->total_amount_paid;
         $close_card = $res->close_card;
        

    }

}

 $accounts = $this->debtors_model->get_transacting_accounts("Bank");
?>
<div>

<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
           <?php echo form_open_multipart('administration/import-payments-values1', array("class" => "form-horizontal", "role" => "form" , "id"=> 'edit-remittance'));?>
       
      
        <div class="row">
            <div class="col-md-12">
                 <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Date: </label>
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="payment_date" placeholder="Visit Date"  required="required" autocomplete="off" value="<?php echo $payment_date;?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> EFT/Code: </label>
                        <div class="col-lg-8">
                            <input type="text"  class="form-control" name="receipt_number" placeholder="Code" value="<?php echo $receipt_number;?>" autocomplete="off" >
                        </div>
                    </div>
                </div>
                <input type="hidden" name="batch_receipt_id" id="batch_receipt_id" value="<?php echo $batch_receipt_id?>">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Paid By: </label>
                        <div class="col-lg-8">
                            <select class="form-control" name="insurance_id" required="required">
                                <option value=""> ------ SELECT AN INSURANCE ------</option>
                                <?php
                                    $visit_types_rs = $this->reception_model->get_visit_types();

                                    if($visit_types_rs->num_rows() > 0)
                                    {
                                        foreach ($visit_types_rs->result() as $key => $value) {
                                            # code...
                                            $visit_type_id = $value->visit_type_id;
                                            $visit_type_name = $value->visit_type_name;

                                            if($insurance_id == $visit_type_id)
                                            {
                                            	echo ' <option value="'.$visit_type_id.'" selected> '.strtoupper($visit_type_name).'</option>';
                                            }
                                            else
                                            {
                                            	echo ' <option value="'.$visit_type_id.'"> '.strtoupper($visit_type_name).'</option>';
                                            }
                                            
                                        }
                                    }
                                ?>
                                
                               
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Total Paid: </label>
                        <div class="col-lg-8">
                            <input type="text"  class="form-control" name="total_amount_paid" placeholder="Amount Paid" value="<?php echo $total_amount_paid;?>" autocomplete="off">
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-3">
                     <div class="form-group">
                        <label class="col-lg-4 control-label">Bank: </label>
                        
                        <div class="col-lg-8">
                            <select class="form-control" name="bank_id" required="required">
                               
                                <?php
                                    

                                    if($accounts->num_rows() > 0)
                                    {
                                        foreach ($accounts->result() as $key => $value) {
                                            # code...
                                            $account_id = $value->account_id;
                                            $account_name = $value->account_name;

                                            if($bank_id == $account_id)
                                            {
                                            	echo ' <option value="'.$account_id.'" selected> '.strtoupper($account_name).'</option>';
                                            }
                                            else
                                            {
                                            	echo ' <option value="'.$account_id.'"> '.strtoupper($account_name).'</option>';
                                            }
                                            
                                        }
                                    }
                                ?>
                                
                               
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <div class="center-align">
                          <button type="submit" class="btn btn-info" onclick="return confirm('Are you sure you want to edit this remittance ?')"> Edit remitance</button>
                        </div>
                    </div>

                    
                </div>

                <div class="col-md-12">
                    
                </div>
            </div>
        </div>
        <?php echo form_close();?>
          
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>

        