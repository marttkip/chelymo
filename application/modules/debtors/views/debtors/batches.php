
<div class="col-md-12">
				<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				$search = $this->session->userdata('creditor_payment_search');
				$search_title = $this->session->userdata('creditor_search_title');



				if(!empty($search))
				{
					echo '
					<a href="'.site_url().'accounting/creditors/close_creditor_search/'.$creditor_id.'" class="btn btn-warning btn-sm ">Close Search</a>
					';
					echo $search_title;
				}	
			
				?>

				
		<div class="col-md-6">
			<section class="panel panel-success">
			              
			        <div class="panel-body" >
			    
						<?php

						$this->session->set_userdata('supplier_invoice_search');
						
						
						echo form_open("accounts/search_invoices", array("class" => "form-horizontal","id"=>"search-invoices"));
						
			            
			            ?>
			            <div class="row">
			                <div class="col-md-6">
			                     <div class="form-group">
			                        <label class="col-md-2 control-label">Patient: </label>
			                        
			                        <div class="col-md-10">
			                            <input type="text" class="form-control" name="patient_surname" placeholder="Patient Name">
			                        </div>
			                    </div>
			                    
			                    <div class="form-group">
			                        <label class="col-md-2 control-label">Invoice: </label>
			                        
			                        <div class="col-md-10">
			                            <input type="text" class="form-control" name="invoice_number" placeholder="Invoice Number">
			                        </div>
			                    </div>
			                    
			                   
			                    
			                </div>
			                
			                <div class="col-md-6">
			                                
			                    <div class="form-group">
	                                <label class="col-md-2 control-label">From: </label>
	                                
	                                <div class="col-md-10">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-calendar"></i>
	                                        </span>
	                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From">
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-md-2 control-label">To: </label>
	                                
	                                <div class="col-md-10">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-calendar"></i>
	                                        </span>
	                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To">
	                                    </div>
	                                </div>
	                            </div>


	                            <div class="form-group">
		                            <div class="col-md-6 col-md-offset-3">
		                                <div class="center-align">
		                                    <button type="submit" class="btn btn-sm btn-info">Search</button>
		                                     <a class="btn btn-sm btn-warning" onclick="javascript:xport.toCSV('UNBATCHED INVOICES');"> Exports List</a>
		                                </div>
		                            </div>
		                        </div>
			                </div>
			            </div>
			            <?php
			            echo form_close();
			            ?>
			    	</div>
			</section>
			<?php
            echo form_open("debtors/create_debtor_batch/".$debtor_id, array("class" => "form-horizontal"));
            ?>
			<div class="panel-body" style="height:40vh !important;overflow-y:scroll;padding: 0px !important;">
				
				<input type="hidden" name="debtor_id" id="debtor_id" value="<?php echo $debtor_id;  ?>">
				
				<div id="ubatched_table"> </div>
			</div>
			<br>
			<div class="panel-body" style="height:20vh !important;overflow-y:scroll;padding: 0px !important;">
				<div class="col-md-12">
			        <div class="col-md-6">
			           <input type="hidden" name="batch_amount" id="batch_amount">
			        </div>
			        <div class="col-md-6">
			        	<?php
			        	$next_purchase_number = $this->debtors_model->create_batch_number();

			        	?>
			            <div class="table-responsive" id="complete-batch" style="display: none;margin: 0 auto;" >
			                <table class="table table-responsive table-condensed table-striped table-bordered">
			                    <thead>
			                        <th>Title</th>
			                        <th>Description</th>
			                    </thead>
			                     <tbody>
			                     	<tr>
			                            <td>Batch Number</td>
			                            <td><input type="text" class="form-control" name="batch_number" value="<?php echo $next_purchase_number?>" id="batch_number" readonly></td>
			                        </tr>
			                        
			                        <tr>
			                            
			                            <td>Batch Amount</td>
			                            <td><span id="amount_reconcilled"></span></td>
			                        </tr>


			                        <tr>
			                           
			                            
			                        </tr>
			                        
			                    </tbody>
			                </table>
			                <div class="col-md-12">
			                	 <div class="center-align" >
			                        <button type="submit" id="submit-button" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to complete this batch')"> Create Batch </button>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			

			</div>
			</form>
		</div>

		<div class="col-md-6" >
			<div class="col-md-12">
				<div class="col-md-6">
					<h3 class="panel-title">Insurance Batches</h3>
				</div>
				<div class="col-md-6">
					<a href="<?php echo site_url().'accounts/debtors-batching'?>" class="btn btn-sm btn-warning"><i class="fa fa-arrow-left"></i> Back to batching</a>
				</div>
			</div>
			
			<div class="panel-body" style="height: 60vh !important;overflow-y: scroll;">
				<div id="batched_table"> </div>
			</div>
		
		</div>
						
				
					


				
				
</div>

<script type="text/javascript">
	$(function() {
		$("#billed_account_id").customselect();
	});
</script>

<script type="text/javascript">

	// Search unbatched invoice by name
	function search_unbatched_invoices_by_name() {
	  // Declare variables
	  var input, filter, table, tr, td, i, txtValue;
	  input = document.getElementById("search_unbatched_invoices_by_name");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("unbatched_invoices");
	  tr = table.getElementsByTagName("tr");

	  // Loop through all table rows, and hide those who don't match the search query
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[3];
	    if (td) {
	      txtValue = td.textContent || td.innerText;
	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    }
	  }
	}

	function get_unbatched_invoices(){
		//debtor_id
		var debtor_id = document.getElementById('debtor_id').value;
		var tbl = document.getElementById('ubatched_table');
		//console.log(debtor_id);
		var config_url = $('#config_url').val();
		var url = config_url+"debtors/get_unallocated_invoices_data/" + debtor_id;
        // alert(total_bill);
        $.ajax({
        	type:'GET',
        	url: url,
        	//data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	//var data = jQuery.parseJSON(data);
        	//console.log(data);
        	tbl.innerHTML = data;
        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });

    }

    window.onload = (event) => {
    	console.log('page is fully loaded');
    	get_unbatched_invoices();
    	get_batched_invoices();
    };

    function get_values(visit_invoice_id)
    {
    	var favorite = [];
    	$.each($("input[id='checkbox']:checked"), function(){    

    		var invoiced_value = $("#preauth_amount"+$(this).val()).val(); 

            // favorite.push($(this).val());

            favorite.push(invoiced_value);
        });
        // alert("My favourite sports are: " + favorite.join(", "));
        var total_bill = favorite.join(", ");


        var config_url = $('#config_url').val();
        var url = config_url+"debtors/calculate_billed_items";

        // alert(total_bill);
        $.ajax({
        	type:'POST',
        	url: url,
        	data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	var data = jQuery.parseJSON(data);
          // alert(data);
          if(data.message == 'success')  
          {
                 // alert(data.billing);
                 document.getElementById("batch_amount").value = data.billing;
                // document.getElementById("difference").value = data.balance;
                $("#amount_reconcilled").html(data.billing);
                // alert(data.billing);

                if(data.billing == "0")
                {
                    // alert('herer');
                    $('#complete-batch').css('display', 'none');
                }
                else
                {
                	$('#complete-batch').css('display', 'block');
                }


            }
            else
            {
            	alert(data.result);
            }


        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });
    }

    function view_dispatch_invoices(debtor_batch_id)
    {
    	close_side_bar();
    	document.getElementById("sidebar-right").style.display = "block"; 
            // document.getElementById("existing-sidebar-div").style.display = "none"; 

            var config_url = $('#config_url').val();
            var data_url = config_url+"debtors/view_dispatch_invoices/"+debtor_batch_id;
        // window.alert(data_url);
        $.ajax({
        	type:'POST',
        	url: data_url,
        	data:{account_payment_id: 1},
        	dataType: 'text',
        	success:function(data){

        		document.getElementById("current-sidebar-div").style.display = "block"; 
        		$("#current-sidebar-div").html(data);

     //         $('.datepicker').datepicker({
					//     format: 'yyyy-mm-dd'
					// });

				},
				error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
    }

});
    }

    function close_side_bar()
    {
        // $('html').removeClass('sidebar-right-opened');
        document.getElementById("sidebar-right").style.display = "none"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 
        document.getElementById("existing-sidebar-div").style.display = "none"; 
        tinymce.remove();
    }

    function clearUserdata(){
    	var config_url = $('#config_url').val();
    	var url = config_url+"debtors/clear_session_search_filter";

        // alert(total_bill);
        $.ajax({
        	type:'GET',
        	url: url,
        	//data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	//var data = jQuery.parseJSON(data);
        	//console.log(data);
        	//tbl.innerHTML = data;
        	get_unbatched_invoices();
        	// get_batched_invoices();
        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });

    }



    $(document).on("submit","form#submit-search-data",function(e)
    {
    	//alert('clicked');
    	e.preventDefault();
    	var config_url = $('#config_url').val();
    	var data_url1 = config_url+"debtors/search_creditor_account";
    	// alert(data_url);
    	var form_data_search = new FormData(this);

    	//alert(form_data.values());

    	$.ajax({
    		type:'POST',
    		url: data_url1,
    		data:form_data_search,
    		dataType: 'text',
    		processData: false,
            contentType: false,
    		success:function(data){
    			get_unbatched_invoices();
    			get_batched_invoices();

				},
			error: function(xhr, status, error) {
        			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
      			alert(error);
      			// console.log(error);
    		}
		});
    });


    function unallocate_invoice_from_dispatch(visit_invoice_id,debtor_batch_id)
    {
    	var res = confirm('Are you sure you want to unallocate this invoice from this batch ?');

    	if(res)
    	{

    	
	    	// var debtor_id = document.getElementById('debtor_id').value;
			// var tbl = document.getElementById('ubatched_table');
			//console.log(debtor_id);
			var config_url = $('#config_url').val();
			var url = config_url+"debtors/unallocate_invoice/"+debtor_batch_id+"/"+visit_invoice_id;
	        // alert(total_bill);
		        $.ajax({
		        	type:'POST',
		        	url: url,
		        	data:{visit_invoice_id: visit_invoice_id},
		        	dataType: 'text',
		        // processData: false,
		        // contentType: false,
		        success:function(data){
		        	view_dispatch_invoices(debtor_batch_id);
		        	get_unbatched_invoices();
		        	get_batched_invoices();
		        },
		        error: function(xhr, status, error) {
		        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		        }
		    });
	    }


    }

    function allocate_to_batches_created(visit_invoice_id,debtor_id)
    {
    	close_side_bar();
    	document.getElementById("sidebar-right").style.display = "block"; 
            // document.getElementById("existing-sidebar-div").style.display = "none"; 

            var config_url = $('#config_url').val();
            var data_url = config_url+"debtors/view_debtor_dispatches/"+debtor_id+"/"+visit_invoice_id;
        // window.alert(data_url);
        $.ajax({
        	type:'POST',
        	url: data_url,
        	data:{account_payment_id: 1},
        	dataType: 'text',
        	success:function(data){

        		document.getElementById("current-sidebar-div").style.display = "block"; 
        		$("#current-sidebar-div").html(data);

    

				},
				error: function(xhr, status, error) {
			        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			        alert(error);
			    }
		});


    }

    function allocate_invoice_to_dispatch(visit_invoice_id,debtor_batch_id)
    {
    	var res = confirm('Are you sure you want to allocate this invoice from this batch ?');

    	if(res)
    	{

    	
	    	// var debtor_id = document.getElementById('debtor_id').value;
			// var tbl = document.getElementById('ubatched_table');
			//console.log(debtor_id);
			var config_url = $('#config_url').val();
			var url = config_url+"debtors/allocate_invoice/"+debtor_batch_id+"/"+visit_invoice_id;
	        // alert(total_bill);
		        $.ajax({
		        	type:'POST',
		        	url: url,
		        	data:{visit_invoice_id: visit_invoice_id},
		        	dataType: 'text',
		        // processData: false,
		        // contentType: false,
		        success:function(data){
		        	// view_dispatch_invoices(debtor_batch_id);
		        	close_side_bar();
		        	get_unbatched_invoices();
		        	get_batched_invoices();
		        },
		        error: function(xhr, status, error) {
		        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		        }
		    });
	    }

    }

    function get_batched_invoices()
    {
    	//debtor_id
		var debtor_id = document.getElementById('debtor_id').value;
		var tbl = document.getElementById('batched_table');
		//console.log(debtor_id);
		var config_url = $('#config_url').val();
		var url = config_url+"debtors/get_allocated_invoices_data/" + debtor_id;
        // alert(total_bill);
        $.ajax({
        	type:'GET',
        	url: url,
        	//data:{billed: total_bill},
        	dataType: 'text',
        // processData: false,
        // contentType: false,
        success:function(data){
        	//var data = jQuery.parseJSON(data);
        	//console.log(data);
        	tbl.innerHTML = data;
        },
        error: function(xhr, status, error) {
        	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
    });
    }




$(document).on("submit","form#search-invoices",function(e)
{

	e.preventDefault();
	
	var form_data = new FormData(this);

	var config_url = $('#config_url').val();	

	 // var url = config_url+"accounts/get_all_invoices";

	 var debtor_id = document.getElementById('debtor_id').value;
		
	 var url = config_url+"debtors/get_unallocated_invoices_data/"+debtor_id;

	 // alert(url);
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          // var data = jQuery.parseJSON(data);
        // alert(data);
        
		  $('#ubatched_table').html(data);
		  	
		 
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
	 
	
   
	
});



var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};






</script>