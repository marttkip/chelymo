<?php
if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		# code...
		$visit_type_name = $value->visit_type_name;
		$batch_date_from = $value->batch_date_from;
		$batch_date_to = $value->batch_date_to;

		$received_date = $value->received_date;
		$received_by = $value->received_by;
		$document_name = $value->document_name;

		if(empty($received_date))
		{
			$received_date = date('Y-m-d');
		}

		$debtor_id = $value->visit_type_id;
	}
}

$creditor_result = $this->debtors_model->get_allocated_invoices($debtor_batch_id);
?>

<div class="col-md-12" style="margin-top:40px !important;">
	<section class="panel">
		<div class="panel-body">
			<div class="padd">
			 	<div class="row" style="height: 55vh !important;overflow-y: scroll;">
			 		<table class="table table-hover table-bordered ">
					 	<thead>
							<tr>
							  <th></th>
							  <th>#</th>
							  <th>File No.</th>
							  <th>Patient</th>
							  <th>Invoice Date</th>	
							  <th>Member No</th>
							  <th>Scheme</th>
							  <th>Invoice Number</th>  
							  <th>Invoice Amount</th>				
							</tr>
						 </thead>
					  	<tbody>
					  		<?php
					  		$x=0;
					  		$result_items='';
					  		// var_dump($creditor_result->num_rows());die();
					  		if($creditor_result->num_rows() > 0)
					  		{
					  			foreach ($creditor_result->result() as $key => $value) {
					  				# code...
					  				$patient_number = $value->patient_number;
					  				$scheme_name = $value->scheme_name;
					  				$member_number = $value->member_number;
					  				$visit_invoice_id = $value->visit_invoice_id;
					  				$created = $value->invoice_date;
					  				$patient_surname = $value->patient_surname;
					  				$patient_onames = $value->patient_onames;
					  				$patient_first_name = $value->patient_first_name;
					  				$visit_invoice_number = $value->visit_invoice_number;
					  				$preauth_amount = $value->total_amount;
					  				$name = $patient_first_name.' '.$patient_onames.' '.$patient_surname;

					  				$checkbox_data = array(
	                                        'name'        => 'creditor_invoice_items[]',
	                                        'id'          => 'checkbox',
	                                        'class'          => 'css-checkbox  lrg ',
	                                        'checked'=>'checked',
	                                        'value'       => $visit_invoice_id,
	                                        'onclick' => 'unallocate_invoice_from_dispatch('.$visit_invoice_id.','.$debtor_batch_id.')'
	                                      );


					  				$x++;
					  				$result_items .= '
					  									<tr>
					  										<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
					  										<td>'.$x.'</td>
					  										<td>'.$patient_number.'</td>
					  										<td>'.$name.'</td>
					  										<td>'.date('jS M Y',strtotime($created)).'</td>
					  										<td>'.$member_number.'</td>
					  										<td>'.$scheme_name.'</td>
					  										<td>'.$visit_invoice_number.'</td>
					  										<td>'.number_format($preauth_amount,2).'<input type="hidden" class="form-control" colspan="3" name="preauth_amount'.$visit_invoice_id.'" id="preauth_amount'.$visit_invoice_id.'" value="'.$preauth_amount.'"/></td>
					  									</tr>
					  									';

					  			}
					  		}
					  		echo $result_items;
					  		?>
						</tbody>
					</table>
			 	</div>
			 	<div class="row">
			 		<div class="col-md-6">
			 			
			 		</div>
			 		<div class="col-md-6">
			 			<h3>Batch Submission Form</h3>


						<?php echo form_open_multipart("debtors/close_batch/".$debtor_batch_id, array("class" => "form-horizontal", "id" => "add_result"));?>
				 			<div class="form-group">
			                    <label class="col-md-4 control-label">Received By: </label>
			                    
			                    <div class="col-md-8">
			                        <input type="text" class="form-control" name="received_by" placeholder="Name" value="<?php echo $received_by?>" required >
			                    </div>
			                </div>
			                <div class="form-group">
			                    <label class="col-md-4 control-label">Date Received : </label>            
			                    <div class="col-md-8">
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <i class="fa fa-calendar"></i>
			                            </span>
			                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="received_date" placeholder="Received Date" autocomplete="off" value="<?php echo $received_date?>" required="required">
			                        </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <label class="col-md-4 control-label">Submitted Document: </label>
			                    
			                    <div class="col-md-8">
			                        <input type="file" class="form-control" name="document_scan" placeholder="File" required="required">
			                    </div>
			                </div>

			                <input type="hidden" name="debtor_batch_id" id="debtor_batch_id" value="<?php echo $debtor_batch_id?>">
			                <input type="hidden" name="debtor_id" id="debtor_id" value="<?php echo $debtor_id?>">
			                <!-- <input type="text" name="redirect_url" id="redirect_url" value="<?php echo $this->uri->uri_string()?>"> -->

			                 <div class="center-align">
					            <button class="btn btn-info btn-sm" type="submit" onclick="return confirm('Are you sure you want to mark this dispatch as completed ?')">Complete Dispatch</button>
					        </div>
					    </form>

			 		</div>
			 	</div>
			 	
		    </div>
		</div>
	</section>
</div>
<br/>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
