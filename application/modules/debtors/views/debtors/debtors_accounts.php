<!-- search -->
<?php //echo $this->load->view('search/debtor_search', '', TRUE);?>
<?php

$income_rs = $this->debtors_model->get_receivables_aging_report();
$income_result = '';
$total_income = 0;
$total_coming = 0;
$total_thirty = 0;
$total_sixty = 0;
$total_ninety = 0;
$total_over_ninety = 0;
$total_grand = 0;
$total_b = 0;
$total_u = 0;
$total_p = 0;
$total_up = 0;
if($income_rs->num_rows() > 0)
{
	foreach ($income_rs->result() as $key => $value) {
		# code...
		// $total_amount = $value->total_amount;
		$receivables = $value->visit_type_name;
		$batch_start_date = $value->batch_start_date;
	
		$receivables_id = $value->visit_type_id;
		$opening_balance = $value->opening_balance;

		$opening_date = $value->created;
		
		if(empty($opening_date))
		{
			$opening_date = date('Y-m-d');
		}



		$total_unbatched = $this->debtors_model->get_total_invoices($receivables_id,0,$batch_start_date);
		$total_batched = $this->debtors_model->get_total_invoices($receivables_id,1,$batch_start_date);

		// var_dump($total_batched);die();
		$total_payments_receipts = $this->debtors_model->get_total_payment_batches($receivables_id);
		$total_unallocated_payments = $this->debtors_model->get_total_unallocated_payment($receivables_id);

		$balance = ($total_unbatched+$total_batched) - $total_payments_receipts;


		$total_b += $total_batched;
		$total_u += $total_unbatched;
		$total_p += $total_payments_receipts;
		$total_up += $total_unallocated_payments;

		$total_grand += $balance;
		$income_result .='<tr>
							<td class="text-left">'.strtoupper($receivables).'</td>
							<td class="text-right">'.number_format($total_unbatched,2).'</td>
							<td class="text-right">'.number_format($total_batched,2).'</td>
							<td class="text-right">'.number_format($total_payments_receipts,2).'</td>
							<td class="text-right">'.number_format($total_unallocated_payments,2).'</td>
							<td class="text-right">'.number_format($balance,2).'</td>
							<td><a href="'.site_url().'accounts/debtors-batches/'.$receivables_id.'" class="btn btn-sm btn-info" >View Account</a></td>
							<td><button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#book-appointment'.$receivables_id.'"><i class="fa fa-plus"></i> SET BATCH START DATE </button>
								<div class="modal fade " id="book-appointment'.$receivables_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content ">
								            <div class="modal-header">
								            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								            	<h4 class="modal-title" id="myModalLabel">Update Balance '.$receivables.' </h4>
								            </div>
								            '.form_open("update-debtor-batch-date/".$receivables_id, array("class" => "form-horizontal")).'

								            <div class="modal-body">
								            	<div class="row">
								            		<input type="hidden" name="redirect_url" id="redirect_url'.$receivables_id.'" value="'.$this->uri->uri_string().'">
								            		<div class="col-md-12">
								            			<div class="col-md-12">
								            				<div class="form-group">
																<label class="col-lg-4 control-label">From: </label>
																
																<div class="col-lg-8">
							                                        <div class="input-group">
							                                            <span class="input-group-addon">
							                                                <i class="fa fa-calendar"></i>
							                                            </span>
							                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" id="scheduledate" placeholder="Date" value="'.$batch_start_date.'" required>
							                                        </div>
																</div>
															</div>
														</div>
														
								            			
								            		</div>
								            	</div>
								            	
														
								              	
								            </div>
								            <div class="modal-footer">
								            	<button  class="btn btn-sm btn-success" type="submit">Update Opening Balance</a>
								                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
								            </div>

								               '.form_close().'
								        </div>
								    </div>
								</div>

							</td>
							</tr>';
	}

	$income_result .='<tr>
							<th class="text-left">Total</th>
							<th class="text-right">'.number_format($total_u,2).'</th>
							<th class="text-right">'.number_format($total_b,2).'</th>
							<th class="text-right">'.number_format($total_p,2).'</th>
							<th class="text-right">'.number_format($total_up,2).'</th>
							<th class="text-right">'.number_format($total_grand,2).'</th>

							</tr>';

}
?>
<!-- end search -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> </h2>
        <!-- <a href="<?php echo site_url();?>accounting/creditors/add_creditor" class="btn btn-sm btn-primary pull-right" style="margin-top: -25px;"><i class="fa fa-plus"></i> Add creditors</a> -->
                	
    </header>

    <!-- Widget content -->
    <div class="panel-body">
    	<div class="padd">
          <?php
            	$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
			?>
           
          <div style="min-height:30px;">
            	<div class="pull-right">
                	<?php
					$search = $this->session->userdata('search_debtors_batches');
		
					if(!empty($search))
					{
						echo '<a href="'.site_url().'debtors/close_search_debtors_batches" class="btn btn-warning btn-sm">Close Search</a>';
					}
					?>
                </div>
            </div>

            <table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
		    			<th class="text-left">Debtor Name</th>
						<th class="text-right">Unbatched Invoices</th>
						<th class="text-right">Batched Invoices</th>
						<th class="text-right">Total Payments</th>
						<th class="text-right">Unreconcilled Payments</th>
						<th class="text-right">Balance</th>
						<th colspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $income_result?>
					<!-- <tr>
		    			<td >Web Development</td>
						<td class="text-right">200,000.00</td>
						<td class="text-right">200,000.00</td>
						<td class="text-right">200,000.00</td>
						<td class="text-right">200,000.00</td>
						<td class="text-right">200,000.00</td>
						<td class="text-right">200,000.00</td>
					</tr> -->

				</tbody>
			</table>
                
          
        
        </div>
        <!-- Widget ends -->

      </div>
</section>

