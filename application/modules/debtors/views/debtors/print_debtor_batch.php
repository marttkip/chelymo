<?php

if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		# code...
		$visit_type_name = $value->visit_type_name;
		$batch_date_from = $value->batch_date_from;
		$batch_date_to = $value->batch_date_to;
		$created = $value->created;
		$batch_number = $value->batch_number;
		$batch_amount = $value->batch_amount;
		$debtor_id = $value->visit_type_id;
	}
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Batch</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 10px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:10px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			th, td {
			    border: 1.5px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			tr, td {
			    border: 1.5px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			/* the first 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:first-child {
			    /*border-radius: 10px 0 0 0 !important;*/
			}
			/* the last 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:last-child {
			    /*border-radius: 0 10px 0 0 !important;*/
			}
			/* the first 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:first-child {
			    /*border-radius: 0 0 0 10px !important;*/
			}
			/* the last 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:last-child {
			    /*border-radius: 0 0 10px 10px !important;*/
			}
			tbody tr:first-child td:first-child {
			    /*border-radius: 10px 10px 0 0 !important;*/
			    /*border-bottom: 0px solid #000 !important;*/
			}
			.padd
			{
				padding:10px;
			}
			
		</style>
    </head>
    <body>
    	
 		

    	<div class="padd " >
    		<div class="row">
    			<div class="col-md-12">
		    		<div class="col-print-6" style="margin-bottom: 10px;">
		            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" />
		            	<!-- <p style="font-size: 13px !important;margin-top:10px !important;">Dr. Lorna Sande | Dr. Kinoti M. Dental Surgeons</p> -->
		            	
		            	<!-- <p style="font-size: 13px;">Pin. PO51455684R </p> -->
		            </div>
		            <div class="col-print-6" style="margin-bottom: 10px;padding: 10px">
		            	<h3 class="pull-right"><strong>Batch# <?php echo $batch_number;?></strong></h3>
		            </div>
		        	<!-- <div  class="center-align">
		            	<p style="font-size: 40px;">
		                	<h3 style="text-decoration: underline;"><?php echo $contacts['company_name'];?></h3> <br/>
		                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
		                    Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
		                    Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
		                    Website: <strong> http://www.irisdental.co.ke </strong> E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
		                </p>
		            </div> -->
		        </div>
		    </div>
    
        
	      	<div class="row" >
	      		<div class="col-md-12">
	      			<div class="col-print-6 left-align">
	      				
		            	<table class="table">
		            		<tr>
		            			<td><strong>Dispatch to</strong></td>
		            		</tr>
		            		<tr>
		            			
		            			<td>
		            				
		            				<strong>INSURANCE COMPANY:</strong> <?php echo $visit_type_name; ?> <br>
		            				<strong>BATCH DATE:</strong> <?php echo $batch_date_from.' - '.$batch_date_to; ?>

		            			</td>
		            		</tr>
		            	</table>
		            </div>
		            <div class="col-print-3">
		            	&nbsp;
		            </div>
		            <div class="col-print-3">
		            	<p style="font-size: 12px;"> &nbsp;</p>
		            	<table class="table">
		            		<tr>
		            			<td>
		            				<span class="pull-left">Date Created:</span> 
		            				<span class="pull-right"> <strong><?php echo date('jS M Y',strtotime($created)); ?> </strong></span>
		            			</td>
		            		</tr>
		            		<tr>
		            			<td>
		            				<span class="pull-left">Batch Number:</span> 
		            				<span class="pull-right"> <strong><?php echo $batch_number; ?> </strong></span>
		            			</td>
		            		</tr>
		            		
		            	</table>
		            </div>
	      			
	      		</div>
	        	
	        </div>
	       
		    
	      	<div class="row">
	      		<div class="col-md-12">
	      			<?php


					$creditor_result = $this->debtors_model->get_allocated_invoices($debtor_batch_id);
					?>
				 	<table class="table table-hover table-bordered " id ="testTable">
					 	<thead>
							<tr>
							  <th>#</th>
							  <th>File No.</th>
							  <th>Patient</th>
							  <th>Invoice Date</th>	
							  <th>Member No</th>
							  <th>Scheme</th>
							  <th>Invoice Number</th>  
							  <th>Invoice Amount</th>				
							</tr>
						 </thead>
					  	<tbody>
					  		<?php
					  		$x=0;
					  		$result_items='';
					  		// var_dump($creditor_result->num_rows());die();
					  		if($creditor_result->num_rows() > 0)
					  		{
					  			foreach ($creditor_result->result() as $key => $value) {
					  				# code...
					  				$patient_number = $value->patient_number;
					  				$scheme_name = $value->scheme_name;
					  				$member_number = $value->member_number;
					  				$visit_invoice_id = $value->visit_invoice_id;
					  				$created = $value->invoice_date;
					  				$patient_surname = $value->patient_surname;
					  				$patient_othernames = $value->patient_othernames;
					  				$patient_first_name = $value->patient_first_name;
					  				$visit_invoice_number = $value->visit_invoice_number;
					  				$preauth_amount = $value->total_amount;
					  				$name = $patient_first_name.' '.$patient_othernames.' '.$patient_surname;

					  				$checkbox_data = array(
	                                        'name'        => 'creditor_invoice_items[]',
	                                        'id'          => 'checkbox',
	                                        'class'          => 'css-checkbox  lrg ',
	                                        'checked'=>'checked',
	                                        'value'       => $visit_invoice_id
	                                      );


					  				$x++;
					  				$result_items .= '
					  									<tr>
					  										
					  										<td>'.$x.'</td>
					  										<td>'.$patient_number.'</td>
					  										<td>'.$name.'</td>
					  										<td>'.date('Y.m.d',strtotime($created)).'</td>
					  										<td>'.$member_number.'</td>
					  										<td>'.$scheme_name.'</td>
					  										<td>'.$visit_invoice_number.'</td>
					  										<td>'.number_format($preauth_amount,2).'</td>
					  									</tr>
					  									';

					  			}
					  			$result_items .= '
					  									<tr>
					  										
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th></th>
					  										<th>Total Batch Amount</th>
					  										<th>'.number_format($batch_amount,2).'</th>
					  									</tr>
					  									';
					  		}
					  		echo $result_items;
					  		?>
						</tbody>
					</table>
		      

	      		</div>
	      	</div>
	      	<div class="row" style="margin-top: 20px; font-size: 10px !important; ">

	      		<br>
	          	<br>
	      		<div class="col-md-12">
	                <div class="col-print-4">
						Approved By: ...................................................................
	                </div>
	                <div class="col-print-4">
	                	
	                  	Signature: .....................................................................
	                       
	                </div>
	                <div class="col-print-4">
	                	
	                  	Date: .........................................................................
	                        
	                </div>
	          	</div>
	          	<br>
	          	<br>
	          	<br>
	          	<br>
	          	<br>
	          	<br>
	          	<div class="col-md-12">
	                <div class="col-print-4">
						Received By: ...................................................................
	                </div>
	                <div class="col-print-4">
	                	
	                  	Signature: .....................................................................
	                       
	                </div>
	                <div class="col-print-4">
	                	
	                  	Date: .........................................................................
	                        
	                </div>
	          	</div>
	      	</div>

	      	   <a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>

	  


    </body>
    
</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>