
<div class="col-md-12" style="margin-top:40px !important;">
	<section class="panel">
		<div class="panel-body">
			<div class="padd">
			 	<div class="row" style="height: 55vh !important;overflow-y: scroll;">
			 		<?php
					$debtor_batches_rs = $this->debtors_model->get_debtor_batches($debtor_id);
					?>
					
					<table class="table table-hover table-bordered ">
						<thead>
							<tr>
								<th>#</th>
								<th>Batch Number</th>	
								<th>Batch Dates</th>						  
								<th>Total Invoices</th>
								<th>Batch Amount</th>
								<th>Created By</th>
								<th>Created</th>
								<th>Approved By</th>
								<th>Approved Date</th>
								<th>Status</th>
								<th>Document</th>
								<th colspan="3">Action</th>			
							</tr>
						</thead>
						<tbody>
							<?php
							$batch_result = '';
							$y=0;
							if($debtor_batches_rs->num_rows() > 0)
							{
								foreach ($debtor_batches_rs->result() as $key => $value_two) {
						  				# code...
									$debtor_batch_id = $value_two->debtor_batch_id;
									$batch_number = $value_two->batch_number;
									$batch_date_from = $value_two->batch_date_from;
									$batch_date_to = $value_two->batch_date_to;
									$batch_amount = $value_two->batch_amount;
									$batch_status = $value_two->batch_status;
									$batch_status = $value_two->batch_status;
									$created = $value_two->created;
									$total_invoice_count = $value_two->total_invoice_count;
									$approved_by = $value_two->approved_by;
									$created_by = $value_two->created_by;
									$date_approved = $value_two->date_approved;
									$document_name = $value_two->document_name;
									

									if(empty($date_approved))
									{
										$date_approved = '';
									}
									else
									{
										$date_approved = date('jS M Y',strtotime($date_approved));
									}
									$created_by_name = $this->debtors_model->get_personnel_name($created_by);

									if(empty($approved_by))
									{
										$approved_by = 0;
										$approved_by_name = '';
									}
									else
									{
										$approved_by_name = $this->debtors_model->get_personnel_name($approved_by);
									}
									

									if($batch_status == 1)
									{
										$status = 'Not Approved';
										$personnel_id = $this->session->userdata('personnel_id');
										$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

										// if($personnel_id )
										// {
											$button = '<a href="'.site_url().'approve-to-dispach/'.$debtor_batch_id.'/'.$debtor_id.'" class="btn btn-sm btn-danger" onclick="confirm(\' Are you sure you want to approve to dispatch ? \')"> Approve for dispatch</a>';
										// }
										// else
										// {
										// 	$button = '';
										// }


									}
									else if($batch_status == 2)
									{

										$status = 'Ready for Dispatch';

										$button = '<a href="'.site_url().'print-dispatch/'.$debtor_batch_id.'" target="_blank" class="btn btn-sm btn-warning" > <i class="fa fa-print"></i></a>';
									}
									else if($batch_status == 3)
									{
										$status = 'Closed';
										$button = '';
									}

									if(!empty($document_name))
									{
										$document_link = '<a href="'.site_url().'assets/debtor_batches/'.$document_name.'" target="_blank"><i class="fa fa-folder-open"></i></a>';
									}
									else
									{
										$document_link = '';
									}

									$y++;
									$batch_result .= '<tr onclick="allocate_invoice_to_dispatch('.$visit_invoice_id.','.$debtor_batch_id.')">
														<td>'.$y.'</td>
														<td>'.$batch_number.'</td>
														<td>'.date('d.m.y',strtotime($batch_date_from)).' - '.date('d.m.y',strtotime($batch_date_to)).'</td>
														<td>'.$total_invoice_count.'</td>
														<td>'.number_format($batch_amount,2).'</td>
														<td>'.$created_by_name.'</td>
														<td>'.date('jS M Y',strtotime($created)).'</td>
														<td>'.$approved_by_name.'</td>
														<td>'.$date_approved.'</td>
														
														<td>'.$status.'</td>
														<td>'.$document_link.'</td>

													</tr>';
								}
							}
							echo $batch_result;
							?>
						</tbody>
					</table>
			 	</div>
			 	
			 	
		    </div>
		</div>
	</section>
</div>
<br/>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
