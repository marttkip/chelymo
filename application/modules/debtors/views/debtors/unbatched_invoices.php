<?php

	$search_filter_from = $this->session->userdata('date_from');
	$search_filter_to = $this->session->userdata('date_to');

	$clearbtn = '';
	if(!empty($search_filter_from))
	{
	
		$clearbtn .= '<button class="btn btn-primary btn-s" style="margin: 10px;" onclick="clearUserdata()">Clear</button>';
	}


?>




<table id="UNBATCHED INVOICES" class="table table-hover table-bordered  table-linked">
	<thead>
		<tr>
			<th></th>
			<th>#</th>
			<th>File No.</th>
			<th>Patient</th>
			<th>Invoice Date</th>	
			<th>Member No</th>
			<th>Scheme</th>
			<th>Invoice Number</th>

			<th>Invoice Amount</th>				
		</tr>
	</thead>
	<tbody>
		<?php
		$x=0;
		$result_items='';
		$total_invoiced = 0;
  		// var_dump($creditor_result->num_rows());die();
		if($creditor_result->num_rows() > 0)
		{
			foreach ($creditor_result->result() as $key => $value) {
  				# code...
				$patient_number = $value->patient_number;
				$scheme_name = $value->scheme_name;
				$member_number = $value->member_number;
				$visit_invoice_id = $value->visit_invoice_id;
				$created = $value->invoice_date;
				$patient_surname = $value->patient_surname;
				$patient_othernames = $value->patient_othernames;
				$patient_first_name = $value->patient_first_name;
				$visit_invoice_number = $value->visit_invoice_number;
				$preauth_amount = $value->total_amount;
				$name = $patient_first_name.' '.$patient_othernames.' '.$patient_surname;
				$total_invoiced += $preauth_amount;
				$checkbox_data = array(
					'name'        => 'creditor_invoice_items[]',
					'id'          => 'checkbox',
					'class'          => 'css-checkbox  lrg ',
                        // 'checked'=>'checked',
					'value'       => $visit_invoice_id,
					'onclick'=>'get_values('.$visit_invoice_id.')'
				);


				$x++;
				$result_items .= '
				<tr>
				<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
				<td>'.$x.'</td>
				<td>'.$patient_number.'</td>
				<td>'.$name.'</td>
				<td>'.date('jS M Y',strtotime($created)).'</td>
				<td>'.$member_number.'</td>
				<td>'.$scheme_name.'</td>
				<td>'.$visit_invoice_number.'</td>
				<td>'.number_format($preauth_amount,2).'<input type="hidden" class="form-control" colspan="3" name="preauth_amount'.$visit_invoice_id.'" id="preauth_amount'.$visit_invoice_id.'" value="'.$preauth_amount.'"/></td>
				<td><a class="btn btn-xs btn-warning" onclick="allocate_to_batches_created('.$visit_invoice_id.','.$debtor_id.')"> allocate to a batch </a></td>
				</tr>
				';

			}
			$result_items .= '
				</tbody>
				<tfoot>
					
						<th colspan="7"></th>
				  		<th>Total Amount</th>
				  		<th>'.number_format($total_invoiced,2).'</th>
				  	
				</tfoot>
			  
					
					';
		}
		echo $result_items;
		?>

</table>

