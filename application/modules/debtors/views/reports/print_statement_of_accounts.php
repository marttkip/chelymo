
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Statement of Accounts</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 9px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

          	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 4px;
				line-height: 1.42857143;
				vertical-align: top;
				border-top: 1px solid #dddddd;
			}
            .padd
            {
                padding:10px;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                        <p style="margin-top: 20px !important;"><?php echo strtoupper($this->session->userdata('statements_search_title'));?> </p>
                      
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                            <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
                        </strong>
                    </div>
                    
                </div>
		       <div class="col-md-12">
		       		<div style="margin-top: 10px">
		       			<?php
		       			
		$result .= 
					'
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <thead>
							<tr>
								<th>Ref No.</th>
								<th>FILE No.</th>
								<th>Patient\'s Name.</th>
								<th>Scheme/Company </th>
								<th>Member No./Policy No.</th>
								<th>Transaction Type</th>
								<th>Preauthorisation Date</th>
								<th>Preauthorisation Officer</th>
								<th>Ageing</th>
								<th>Invoice Date</th>
								<th>Invoice Number</th>
								<th>Invoice Amount</th>
								<th>Invoice Balance</th>
							</tr>
						  </thead>
						  <tbody>
				';

				// var_dump($query);die();
		if(!empty($query))
		{
			//if users exist display them
			if ($query->num_rows() > 0)
			{
				$count = 0;
			
				
				
				// $personnel_query = $this->accounting_model->get_all_personnel();
				$total_waiver = 0;
				$total_payments = 0;
				$total_invoice = 0;
				$total_balance = 0;
				$total_rejected_amount = 0;
				$total_cash_balance = 0;
				$total_insurance_payments =0;
				$total_insurance_invoice =0;
				$total_payable_by_patient = 0;
				$total_payable_by_insurance = 0;
				$total_debit_notes = 0;
				$total_credit_notes= 0;
				foreach ($query->result() as $row)
				{
					$total_invoiced = 0;
					$visit_date = date('d.m.y',strtotime($row->invoice_date));
					
					
					$visit_id = $row->visit_id;
					$patient_id = $row->patient_id;
					$personnel_id = $row->personnel_id;
					$dependant_id = $row->dependant_id;
					$patient_number = $row->patient_number;
					$visit_invoice_number = $row->visit_invoice_number;
					$scheme_name = $row->scheme_name;
					$member_number = $row->member_number;
					$preauth_date = $row->preauth_date;
					$authorising_officer = $row->authorising_officer;
					$visit_invoice_id = $row->visit_invoice_id;
					$branch_code = $row->branch_code;

					$visit_type_name = $row->payment_type_name;
					$patient_othernames = $row->patient_othernames;
					$patient_surname = $row->patient_surname;
					$patient_date_of_birth = $row->patient_date_of_birth;
					$patient_first_name = $row->patient_first_name;
					$preauth_date = date('d.m.y',strtotime($row->preauth_date));
					$doctor = $row->personnel_fname;
				
					$invoice_bill = $row->invoice_bill;
					$bill_to = $row->bill_to;
					$invoice_balance = $row->invoice_balance;
					$invoice_credit_note = $row->invoice_credit_note;
					$invoice_total = $invoice_bill - $invoice_credit_note;

					$balance = $row->balance;
					$payments_value = $row->invoice_payments;

					$date = $row->date_invoice;

					$total_payable_by_patient += $invoice_total;
					$total_payments += $payments_value;
					$total_balance += $balance;

					// $date = $row->invoice_date;
									
					$ageing  = $this->debtors_model->get_aging_time($date);


					$date = date('d/m/Y',strtotime($row->date_invoice));

					// $query_patient = $this->hospital_reports_model->get_visit_type_patient_balances($bill_to,$patient_id);
					// $open_balance = 0;
					// if($query_patient->num_rows() > 0)
					// {
					// 	foreach ($query_patient->result() as $key => $value_two) {
					// 		// code...
					// 		$open_balance = $value_two->invoice_balance;
					// 	}
					// }


					if($patient_number == "DO994")
					{
						// var_dump($balance);die();
					}
					
					if($balance == 0)
					{

						

					}
					else
					{
							$count++;


						$result .= 
									'
											<tr>
												<td>'.$count.'</td>
												<td>'.$patient_number.'</td>
												<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
												<td>'.$scheme_name.'</td>
												<td>'.$member_number.'</td>
												<td>Invoice</td>
												<td>'.$preauth_date.'</td>
												<td>'.$authorising_officer.'</td>
												<td>'.$ageing.'</td>
												<td>'.$date.'</td>
												<td>'.$visit_invoice_number.'</td>
												<td>'.(number_format($invoice_bill,2)).'</td>
												<td>'.(number_format($balance,2)).' </td>
											</tr> 
									';
					}


					
					
				}

			}
			
			else
			{
				// $result = "";
			}

		}
		else
		{
			// $result = "";
		}
		// jouran,e s
		$visit_type = $this->session->userdata('statements_visit_type');
		// var_dump($visit_type);die();
		if(!empty($visit_type))
		{		

				$visit_search = $this->session->userdata('statements_visit_invoices');
				$customers_add = '';
				$orders_add = '';
				$payments_add = '';
				$credit_note_add = '';
				$patients_add = '';
				$allocations_add = '';
				if(!empty($visit_search))
				{

					$customers_add = str_replace('data.transaction_date', 'customers_journals.journal_date', $visit_search);
					$orders_add = str_replace('data.transaction_date', 'pos_order.order_date', $visit_search);
					$payments_add = str_replace('data.transaction_date', 'payments.payment_date', $visit_search);
					$credit_note_add = str_replace('data.transaction_date', 'visit_credit_note.created', $visit_search);
					$patients_add = str_replace('data.transaction_date', 'patients_journals.journal_date', $visit_search);
					$allocations_add = str_replace('data.transaction_date', 'batch_receipts.payment_date', $visit_search);
				}
			
				

				$this->db->where('customer.customer_id = customers_journals.customer_id AND customers_journals.visit_type_id ='.$visit_type.$customers_add);
				$query_three = $this->db->get('customers_journals,customer');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$customer_name = $value5->customer_name;
						$journal_amount = $value5->customer_journal_amount;
						$invoice_number = $value5->customer_journal_number;
						$journal_date = $value5->journal_date;
						$journal_type = $value5->customer_journal_type;

						$count++;

						
						// $journal_type_name = 'Payment';
						$journal_type_name = 'General Journal';
						
						$ageing  = $this->debtors_model->get_aging_time($journal_date);

						// calculate the balance = 
						$journal_date = date('d/m/Y',strtotime($journal_date));
						

						$total_payable_by_patient += $journal_amount;
						$total_payments += 0;
						$total_balance += $journal_amount;
						$result .= 
								'
										<tr>
											<td>'.$count.'</td>
											<td></td>
											<td>'.strtoupper(strtolower($customer_name)).'</td>
											<td></td>
											<td></td>
											<td>'.$journal_type_name.'</td>
											<td></td>
											<td></td>
											<td>'.$ageing.'</td>
											<td>'.$journal_date.'</td>
											<td>'.$invoice_number.'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
										</tr> 
								';
						
						
					}
				}


				$this->db->where('customers_journals.customer_journal_type = 2 AND customers_journals.visit_type_id ='.$visit_type.$customers_add);
				$query_three = $this->db->get('customers_journals');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$customer_name = '';//$value5->customer_name;
						$journal_amount = -$value5->customer_journal_amount;
						$invoice_number = $value5->customer_journal_number;
						$journal_date = $value5->journal_date;
						$journal_type = $value5->customer_journal_type;

						$count++;

						
						// $journal_type_name = 'Payment';
						$journal_type_name = 'Payment';
						
						$ageing  = $this->debtors_model->get_aging_time($journal_date);

						// calculate the balance = 

						

						$total_payable_by_patient += $journal_amount;
						$total_payments += 0;
						$total_balance += $journal_amount;
						$result .= 
								'
										<tr>
											<td>'.$count.'</td>
											<td></td>
											<td>'.strtoupper(strtolower($customer_name)).'</td>
											<td></td>
											<td></td>
											<td>'.$journal_type_name.'</td>
											<td></td>
											<td></td>
											<td>'.$ageing.'</td>
											<td>'.$journal_date.'</td>
											<td>'.$invoice_number.'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
										</tr> 
								';
						
						
					}
				}


				$this->db->where('customer.customer_id = pos_order.customer_id AND `pos_order_item`.`charged` = 1
AND pos_order_item.pos_order_item_deleted = 0
AND pos_order.pos_order_id = pos_order_item.pos_order_id
AND pos_order.pos_order_deleted = 0
AND visit_type.visit_type_id = pos_order.sale_type AND pos_order.sale_type ='.$visit_type.$orders_add);
				$query_three = $this->db->get('pos_order_item,pos_order,visit_type,customer');


				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$customer_name = $value5->customer_name;
						$pos_order_item_quantity = $value5->pos_order_item_quantity;
						$pos_order_item_amount = $value5->pos_order_item_amount;
						$invoice_number = $value5->pos_order_number;
						$journal_date = $value5->order_date;
						$journal_type = $value5->customer_journal_type;
						$journal_amount = $pos_order_item_quantity *$pos_order_item_amount;
						$count++;

						
						// $journal_type_name = 'Payment';
						$journal_type_name = 'Invoice';
						
						$ageing  = $this->debtors_model->get_aging_time($journal_date);

						// calculate the balance = 
						$journal_date = date('d/m/Y',strtotime($journal_date));
						

						$total_payable_by_patient += $journal_amount;
						$total_payments += 0;
						$total_balance += $journal_amount;
						$result .= 
								'
										<tr>
											<td>'.$count.'</td>
											<td></td>
											<td>'.strtoupper(strtolower($customer_name)).'</td>
											<td></td>
											<td></td>
											<td>'.$journal_type_name.'</td>
											<td></td>
											<td></td>
											<td>'.$ageing.'</td>
											<td>'.$journal_date.'</td>
											<td>'.$invoice_number.'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
										</tr> 
								';
						
						
					}
				}


				$this->db->select('SUM(payment_item.payment_item_amount) AS total_payment_amount,payments.*,patients.*,payment_item.invoice_type');

				$this->db->where('payments.patient_id = patients.patient_id AND payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND payments.payment_type = '.$visit_type.' AND  payment_item.invoice_type >= 2 AND payment_item.invoice_type <> 3  '.$payments_add);

				$this->db->group_by('payment_item.payment_id');
				$query_three = $this->db->get('payments,payment_item,patients');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patient_surname = $value5->patient_surname;
						$patient_othernames = $value5->patient_othernames;
						$patient_first_name = $value5->patient_first_name;
						$receipt_number = $value5->receipt_number;
						$payment_date = $value5->payment_date;
						$patient_id = $value5->patient_id;
						$total_amount = $value5->total_payment_amount;
						$payment_amount = $value5->total_payment_amount;
						$invoice_type = $value5->invoice_type;

						$count++;

						
						// // $journal_type_name = 'Payment';

						if($invoice_type == 5)
						{
							$journal_type_name = 'Refund';
							$journal_balance = $total_amount;
						}
						else
						{
							$journal_type_name = 'Payment';

							$journal_balance = -$total_amount;
						}
						
						

						// calculate the balance = 

						$journal_date = date('d/m/Y',strtotime($payment_date));
						

						if($journal_balance != 0)
						{

							$total_payable_by_patient += $journal_amount;
							$total_payments += $payment_amount;
							$total_balance += $journal_balance;
							$result .= 
									'
											<tr>
												<td>'.$count.'</td>
												<td>'.$patient_number.'</td>
												<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
												<td></td>
												<td></td>
												<td>'.$journal_type_name.'</td>
												<td></td>
												<td></td>
												<td></td>
												<td>'.$journal_date.'</td>
												<td>'.$invoice_number.'</td>
												<td>'.(number_format($journal_amount,2)).'</td>
												<td>'.(number_format($journal_balance,2)).'</td>
											</tr> 
									';
						}
						
					}
				}




				$this->db->where('patients_journals.patient_id = patients.patient_id AND patients_journals.visit_invoice_id = 0 AND patients_journals.account_to_id = 19 AND  patients_journals.visit_type_id ='.$visit_type.$patients_add);
				$query_three = $this->db->get('patients_journals,patients');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patient_surname = $value5->patient_surname;
						$patient_othernames = $value5->patient_othernames;
						$patient_first_name = $value5->patient_first_name;
						$journal_amount = $value5->journal_amount;
						$invoice_number = $value5->invoice_number;
						$journal_date = $value5->journal_date;
						$journal_type = $value5->journal_type;
						$journal_amount = $value5->journal_amount;
						$patient_id = $value5->patient_id;

						$count++;

						if($journal_type == 1)
						{
							$journal_type_name = 'General Journal';
						}
						else
						{
							// $journal_type_name = 'Payment';
							$journal_type_name = 'General Journal';
						}
						$journal_date = date('d/m/Y',strtotime($journal_date));
						// calculate the balance = 

						$payment_amount = $this->debtors_model->get_patient_account_payments($patient_id,$visit_type);
						$journal_payment = $this->debtors_model->get_patient_account_journals($patient_id,$visit_type);

						
						$journal_balance = ($journal_amount + $journal_payment) - $payment_amount;

						if($journal_balance != 0)
						{

							$total_payable_by_patient += $journal_amount;
							$total_payments += $payment_amount;
							$total_balance += $journal_balance;
							$result .= 
									'
											<tr>
												<td>'.$count.'</td>
												<td>'.$patient_number.'</td>
												<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
												<td></td>
												<td></td>
												<td>'.$journal_type_name.'</td>
												<td></td>
												<td></td>
												<td></td>
												<td>'.$journal_date.'</td>
												<td>'.$invoice_number.'</td>
												<td>'.(number_format($journal_amount,2)).'</td>
												<td>'.(number_format($journal_balance,2)).'</td>
											</tr> 
									';
						}
						
					}
				}


				$this->db->where('patients_journals.patient_id = patients.patient_id AND patients_journals.account_from_id = 19 AND patients_journals.journal_type = 6 AND  patients_journals.visit_type_id ='.$visit_type.$patients_add);
				$query_three = $this->db->get('patients_journals,patients');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patient_surname = $value5->patient_surname;
						$patient_othernames = $value5->patient_othernames;
						$patient_first_name = $value5->patient_first_name;
						$journal_amount = $value5->journal_amount;
						$invoice_number = $value5->invoice_number;
						$journal_date = $value5->journal_date;
						$journal_type = $value5->journal_type;
						$journal_amount = $value5->journal_amount;
						$patient_id = $value5->patient_id;

						$count++;

						if($journal_type == 1)
						{
							$journal_type_name = 'General Journal';
						}
						else
						{
							// $journal_type_name = 'Payment';
							$journal_type_name = 'General Journal';
						}

						// calculate the balance = 

						$journal_date = date('d/m/Y',strtotime($journal_date));

						$total_payable_by_patient += $journal_amount;
						$total_payments += 0;
						$total_balance += $journal_amount;
						$result .= 
								'
										<tr>
											<td>'.$count.'</td>
											<td>'.$patient_number.'</td>
											<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
											<td></td>
											<td></td>
											<td>'.$journal_type_name.'</td>
											<td></td>
											<td></td>
											<td></td>
											<td>'.$journal_date.'</td>
											<td>'.$invoice_number.'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
											<td>'.(number_format($journal_amount,2)).'</td>
										</tr> 
								';
						
						
					}
				}




				$visit_type = $this->session->userdata('statements_visit_type');

				$this->db->where('visit_credit_note.patient_id = patients.patient_id AND visit_credit_note.visit_credit_note_id =  visit_credit_note_item.visit_credit_note_id AND insurance_invoices.item_id = visit_credit_note.item_id AND insurance_invoices.visit_type_id ='.$visit_type.$credit_note_add);
				$query_three = $this->db->get('visit_credit_note,visit_credit_note_item,patients,insurance_invoices');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patient_surname = $value5->patient_surname;
						$patient_othernames = $value5->patient_othernames;
						$patient_first_name = $value5->patient_first_name;
						$journal_amount = $value5->visit_cr_note_amount;
						$invoice_number = $value5->visit_cr_note_number;
						$journal_date = $value5->created;
						$invoice_amount = $value5->visit_cr_note_amount;
						$invoice_balance = $value5->invoice_balance;

						$count++;

						

						// $total_payable_by_patient += -$invoice_amount;
						// $total_payments += -$journal_amount;
						// $total_balance += -$invoice_balance;

						// $result .= 
						// 	'
						// 			<tr>
						// 				<td>'.$count.'</td>
						// 				<td>'.$patient_number.'</td>
						// 				<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
						// 				<td></td>
						// 				<td></td>
						// 				<td>CREDIT NOTE</td>
						// 				<td></td>
						// 				<td>'.$journal_date.'</td>
						// 				<td>'.$invoice_number.'</td>
						// 				<td>'.(number_format($invoice_amount,2)).'</td>
						// 				<td>'.(number_format($invoice_balance,2)).'</td>
						// 			</tr> 
						// 	';
					}
				}





				$result .= 
						'
							<tr>
								<td colspan=11> Totals</td>
								<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
								<td><strong>'.number_format($total_balance,2).'</strong></td>
							</tr> 
					';


				$result .= 
						'
							<tr>
								<td colspan=13> Unallocated Payments</td>
								
							</tr> 
						';

				

				$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type.$allocations_add);
				// $this->db->get('batch_unallocations.*,batch_receipts.*');
				$query = $this->db->get('batch_unallocations,batch_receipts');
				$visit_type_name = $this->session->userdata('statements_visit_type_name');

				$total_unallocated_payments= 0;
				if($query->num_rows() > 0)
				{
					$counting =0;
					foreach ($query->result() as $key => $value) {
						# code...

						$payment_date = $value->payment_date;
						
						$payment_date = $value->payment_date;
						$receipt_number = $value->receipt_number;
						$allocation_type_id = $value->allocation_type_id;


						if($allocation_type_id == 1)
						{
							$amount = $value->amount_paid;
						}
						else
						{
							$amount = -$value->amount_paid;
						}

						$payment_date = date('d.m.y',strtotime($value->payment_date));

						$total_unallocated_payments += $amount;
						$counting++;
						$result .= 
							'
									<tr>
										<td>'.$counting.'</td>
										<td></td>
										<td>Insurance - '.$visit_type_name.'</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td>Payment</td>
										<td></td>
										<td>'.$payment_date.'</td>
										<td>'.$receipt_number.'</td>
										<td>'.number_format($amount,2).'</td>
										<td>'.number_format($amount,2).'</td>
									</tr> 
							';
					}



					

						$result .= 
							'
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<th>Total</th>
										<th>'.(number_format($total_unallocated_payments,2)).'</th>
										<th>'.(number_format($total_unallocated_payments,2)).'</th>
									</tr> 
							';

							$result .= 
						'
							<tr>
								<td colspan=13></td>
								
							</tr> 
						';
							$result .= 
							'
								<tr>
									<td colspan=13></td>
									
								</tr> 
							';
					$total_payable_by_patient += $total_unallocated_payments;
					$total_balance += $total_unallocated_payments;
					$result .= 
							'
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<th>Total</th>
										<th>'.(number_format($total_payable_by_patient,2)).'</th>
										<th>'.(number_format($total_balance,2)).'</th>
									</tr> 
							';
				}
		}
				
				$result .= 
				'
							  </tbody>
							</table>
				';

		
		echo $result;
?>

		       		</div>
		       </div>
		    </div>
		</div>
	</body>
</html>
   		