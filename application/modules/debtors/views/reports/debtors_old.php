<!-- search -->
<?php echo $this->load->view('search_debtors', '', TRUE);?>
<!-- end search -->
<?php //echo $this->load->view('transaction_statistics_invoices', '', TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '<a href="'.site_url().'export-statements-of-accounts" target="_blank" class="btn btn-sm btn-success pull-right">Export Statement </a>
		<a href="'.site_url().'print-statements-of-accounts" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-left:5px;">Print Statement </a>';
		$search = $this->session->userdata('statements_search_query');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'debtors/close_statement_reports_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		$result .= 
					'
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <thead>
							<tr>
								<th>Ref No.</th>
								<th>FILE No.</th>
								<th>Patient\'s Name.</th>
								<th>Scheme/Company </th>
								<th>Member No./Policy No.</th>
								<th>Transaction Type</th>
								<th>Ageing</th>
								<th>Invoice Date</th>
								<th>Invoice Number</th>
								<th>Invoice Amount</th>
								<th>Invoice Balance</th>
							</tr>
						  </thead>
						  <tbody>
				';
		if(!empty($query))
		{
			//if users exist display them
			if ($query->num_rows() > 0)
			{
				$count = 0;
			
				
				
				// $personnel_query = $this->accounting_model->get_all_personnel();
				$total_waiver = 0;
				$total_payments = 0;
				$total_invoice = 0;
				$total_balance = 0;
				$total_rejected_amount = 0;
				$total_cash_balance = 0;
				$total_insurance_payments =0;
				$total_insurance_invoice =0;
				$total_payable_by_patient = 0;
				$total_payable_by_insurance = 0;
				$total_debit_notes = 0;
				$total_credit_notes= 0;
				foreach ($query->result() as $row)
				{
					$total_invoiced = 0;
					$visit_date = date('d.m.y',strtotime($row->transaction_date));
					
					
					$visit_id = $row->visit_id;
					$patient_id = $row->patient_id;
					$personnel_id = $row->personnel_id;
					$dependant_id = $row->dependant_id;
					$patient_number = $row->patient_number;
					$visit_invoice_number = $row->visit_invoice_number;
					$scheme_name = $row->scheme_name;
					$member_number = $row->member_number;
					$preauth_date = $row->preauth_date;
					$authorising_officer = $row->authorising_officer;
					$visit_invoice_id = $row->visit_invoice_id;
					$branch_code = $row->branch_code;

					$visit_type_name = $row->payment_type_name;
					$patient_othernames = $row->patient_othernames;
					$patient_surname = $row->patient_surname;
					$patient_date_of_birth = $row->patient_date_of_birth;
					$patient_first_name = '';//$row->patient_first_name;
					$preauth_date = date('d.m.y',strtotime($row->preauth_date));
					$doctor = $row->personnel_fname;
					$count++;
					$invoice_total = $row->dr_amount;
					$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

					$invoice_total -= $credit_note;

					$balance  = $this->accounts_model->balance($payments_value,$invoice_total);


					$total_payable_by_patient += $invoice_total;
					$total_payments += $payments_value;
					$total_balance += $balance;

					$date = $row->transaction_date;
					// $date = $row->invoice_date;
									
					$ageing  = $this->debtors_model->get_aging_time($date);


					$result .= 
						'
								<tr>
									<td>'.$count.'</td>
									<td>'.$patient_number.'</td>
									<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
									<td>'.$scheme_name.'</td>
									<td>'.$member_number.'</td>
									<td>Invoice</td>
									<td>'.$ageing.'</td>
									<td>'.$date.'</td>
									<td>'.$visit_invoice_number.'</td>
									<td>'.(number_format($invoice_total,2)).'</td>
									<td>'.(number_format($balance,2)).'</td>
								</tr> 
						';
					
				}

			}
			
			else
			{
				// $result = "";
			}

		}
		else
		{
			// $result = "";
		}
		// jouran,e s
		$visit_type = $this->session->userdata('statements_visit_type');

		if(!empty($visit_type))
		{



				$this->db->where('patients_journals.patient_id = patients.patient_id AND  patients_journals.visit_type_id ='.$visit_type);
				$query_three = $this->db->get('patients_journals,patients');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patient_surname = $value5->patient_surname;
						$patient_othernames = $value5->patient_othernames;
						$patient_first_name = $value5->patient_first_name;
						$journal_amount = $value5->journal_amount;
						$invoice_number = $value5->invoice_number;
						$journal_date = $value5->journal_date;
						$journal_type = $value5->journal_type;
						$invoice_amount = $value5->invoice_amount;

						$count++;

						if($journal_type == 1)
						{
							$journal_type_name = 'General Journal';
						}
						else
						{
							$journal_type_name = 'Payment';
						}

						$total_payable_by_patient += $invoice_amount;
						$total_payments += $journal_amount;
						$total_balance += $journal_amount;

						$result .= 
							'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.'</td>
										<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
										<td></td>
										<td></td>
										<td>'.$journal_type_name.'</td>
										<td></td>
										<td>'.$journal_date.'</td>
										<td>'.$invoice_number.'</td>
										<td>'.(number_format($invoice_amount,2)).'</td>
										<td>'.(number_format($journal_amount,2)).'</td>
									</tr> 
							';
					}
				}



				$visit_type = $this->session->userdata('statements_visit_type');

				$this->db->where('visit_credit_note.patient_id = patients.patient_id AND visit_credit_note.visit_credit_note_id =  visit_credit_note_item.visit_credit_note_id AND insurance_invoices.item_id = visit_credit_note.item_id AND insurance_invoices.visit_type_id ='.$visit_type);
				$query_three = $this->db->get('visit_credit_note,visit_credit_note_item,patients,insurance_invoices');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patient_surname = $value5->patient_surname;
						$patient_othernames = $value5->patient_othernames;
						$patient_first_name = $value5->patient_first_name;
						$journal_amount = $value5->visit_cr_note_amount;
						$invoice_number = $value5->visit_cr_note_number;
						$journal_date = $value5->created;
						$invoice_amount = $value5->visit_cr_note_amount;
						$invoice_balance = $value5->invoice_balance;

						$count++;

						

						$total_payable_by_patient += -$invoice_amount;
						$total_payments += -$journal_amount;
						$total_balance += -$invoice_balance;

						$result .= 
							'
									<tr>
										<td>'.$count.'</td>
										<td>'.$patient_number.'</td>
										<td>'.strtoupper(strtolower($patient_surname.' '.$patient_othernames.' '.$patient_first_name)).'</td>
										<td></td>
										<td></td>
										<td>CREDIT NOTE</td>
										<td></td>
										<td>'.$journal_date.'</td>
										<td>'.$invoice_number.'</td>
										<td>'.(number_format($invoice_amount,2)).'</td>
										<td>'.(number_format($invoice_balance,2)).'</td>
									</tr> 
							';
					}
				}



				$this->db->where('remote_id = 0 AND synced_status = 0 AND insurance_invoices.visit_type_id ='.$visit_type);
				$query_three = $this->db->get('insurance_invoices');

				if($query_three->num_rows() > 0)
				{
					foreach ($query_three->result() as $key => $value5) {
						# code...
						$patient_number = $value5->patient_number;
						$patients_name = $value5->patients_name;

						 $invoice_date = $value5->invoice_date;
                        $invoice_amount = $value5->invoice_amount;
                        $invoice_balance = $value5->invoice_balance;
                        $scheme_name = $value5->scheme_name;
                        $patient_number = $value5->patient_number;
                        $patients_name = $value5->patients_name;
                        $member_number = $value5->member_number;
                        $preauthorisation_date = $value5->preauthorisation_date;
                        $preauthorisation_officer = $value5->preauthorisation_officer;
                        $transaction_type = $value5->transaction_type;
                        $ageing = $value5->ageing;
                        $invoice_number = $value5->invoice_number;
                        $remarks = $value5->remarks;
                        $synced_status = $value5->synced_status;
                        $remote_id = $value5->remote_id;

						$count++;

						

						$total_payable_by_patient += $invoice_amount;
						$total_payments += $journal_amount;
						$total_balance += $invoice_balance;

						$result .= 
							'
									<tr>
										<td class="warning">'.$count.'</td>
										<td>'.$patient_number.'</td>
										<td>'.strtoupper(strtolower($patients_name)).'</td>
										<td >'.$scheme_name.'</td>
	                                    <td>'.$member_number.'</td>
	                                    <td>'.$transaction_type.'</td>
	                                    <td>'.$ageing.'</td>
										<td>'.$invoice_date.'</td>
										<td>'.$invoice_number.'</td>
										<td>'.(number_format($invoice_amount,2)).'</td>
										<td>'.(number_format($invoice_balance,2)).'</td>
									</tr> 
							';
					}
				}


				$result .= 
						'
							<tr>
								<td colspan=9> Totals</td>
								<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
								<td><strong>'.number_format($total_balance,2).'</strong></td>
							</tr> 
					';


				$result .= 
						'
							<tr>
								<td colspan=11> Unallocated Payments</td>
								
							</tr> 
						';

				

				$this->db->where('batch_unallocations.batch_receipt_id = batch_receipts.batch_receipt_id AND batch_unallocations.unallocated_payment_delete = 0 AND batch_receipts.insurance_id = '.$visit_type);
				// $this->db->get('batch_unallocations.*,batch_receipts.*');
				$query = $this->db->get('batch_unallocations,batch_receipts');
				$visit_type_name = $this->session->userdata('statements_visit_type_name');

				$total_unallocated_payments= 0;
				if($query->num_rows() > 0)
				{
					$counting =0;
					foreach ($query->result() as $key => $value) {
						# code...

						$payment_date = $value->payment_date;
						$amount = -$value->amount_paid;
						$payment_date = $value->payment_date;
						$receipt_number = $value->receipt_number;

						$payment_date = date('d.m.y',strtotime($value->payment_date));

						$total_unallocated_payments += $amount;
						$counting++;
						$result .= 
							'
									<tr>
										<td>'.$counting.'</td>
										<td></td>
										<td>Insurance - '.$visit_type_name.'</td>
										<td></td>
										<td></td>
										<td>Payment</td>
										<td></td>
										<td>'.$payment_date.'</td>
										<td>'.$receipt_number.'</td>
										<td>'.number_format($amount,2).'</td>
										<td>'.number_format($amount,2).'</td>
									</tr> 
							';
					}



					

						$result .= 
							'
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<th>Total</th>
										<th>'.(number_format($total_unallocated_payments,2)).'</th>
										<th>'.(number_format($total_unallocated_payments,2)).'</th>
									</tr> 
							';

							$result .= 
						'
							<tr>
								<td colspan=11></td>
								
							</tr> 
						';
							$result .= 
							'
								<tr>
									<td colspan=11></td>
									
								</tr> 
							';
					$total_payable_by_patient += $total_unallocated_payments;
					$total_balance += $total_unallocated_payments;
					$result .= 
							'
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<th>Total</th>
										<th>'.(number_format($total_payable_by_patient,2)).'</th>
										<th>'.(number_format($total_balance,2)).'</th>
									</tr> 
							';
				}
		}
				
				$result .= 
				'
							  </tbody>
							</table>
				';

		
		echo $result;
?>
          </div>
          <div class="row">

          	
          </div>
        
		</section>
    </div>
  </div>