 <section class="panel">
   
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php

            echo form_open("debtors/search_debtors_batches", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Aging Report as at : </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date from" value="<?php echo date('Y-m-d')?>">
                            </div>
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Interval (days): </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="visit_type_name" placeholder="Interval Period" value="30" readonly >
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Report Detail : </label>
                        
                        <div class="col-md-8">
                           <select class="form-control" name="interval_limit">

                               
                                <?php
                                $report_detail = $this->session->userdata('report_detail');

                                if($report_detail == 180)
                                {
                                    ?>
                                    <option value="180" selected> 180</option>
                                    <option value="360" > 360</option>
                                     <option value="720" selected> 720</option>
                                    <?php
                                }
                                else if($report_detail == 360)
                                {
                                    ?>
                                    <option value="360" selected> 360</option>
                                    <option value="180" > 180</option>
                                     <option value="720" > 720</option>
                                    <?php
                                }
                                else if($report_detail == 720)
                                {
                                    ?>
                                    <option value="720" selected> 720</option>
                                    <option value="360"> 360</option>
                                    <option value="180" > 180</option>
                                    <?php
                                }
                                ?>
                                

                           </select>
                               
                       
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-3">
                     <div class="center-align">
                        <button type="submit" class="btn btn-info btn-sm">Search</button>
                    </div>
                </div>
                 
            </div>
     
           
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>