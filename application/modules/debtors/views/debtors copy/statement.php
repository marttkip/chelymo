<!-- search -->
<?php echo $this->load->view('search_debtor_account', '', TRUE);?>
<!-- end search -->

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url();?>accounts/debtors-accounts" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px; "><i class="fa fa-arrow-left"></i> Back to debtors</a>
             

                	
            </header>
            
            <div class="panel-body">
                <div class="pull-right">
                	
                	<!--<a href="<?php echo base_url().'administration/sync_app_creditor_account';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a>-->
                </div>
                <!-- Modal -->
             
                
			<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
					
			$search = $this->session->userdata('creditor_payment_search');
			$search_title = $this->session->userdata('creditor_search_title');
		
			if(!empty($search))
			{
				echo '
				<a href="'.site_url().'accounting/creditors/close_creditor_search/'.$creditor_id.'" class="btn btn-warning btn-sm ">Close Search</a>
				';
				echo $search_title;
			}	
			// var_dump($debtor_id); die();
			$creditor_result = $this->debtors_model->get_debtor_statement($debtor_id);
			?>

				<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th>Transaction Date</th>						  
						  <th>Invoice Total</th>
						  <th>Bill Transfers</th>
						  <th>Waiver</th>
						  <th>Billed Amount</th>
						  <th>Paid Amount</th>	
                          <th>Balance</th>   					
						</tr>
					 </thead>
				  	<tbody>
				  		<?php  echo $creditor_result['result'];?>
					</tbody>
				</table>
          	</div>
		</section>
    </div>
</div>
<script type="text/javascript">
    $(function() {
       $("#billed_account_id").customselect();
    });
</script>