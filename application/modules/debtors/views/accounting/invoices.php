
<div class="row">
	<div class="col-md-12">
		<div class="col-md-7">
			<section class="panel panel-success">
			              
			        <div class="panel-body" >
			    
						<?php

						$this->session->set_userdata('supplier_invoice_search');
						
						
						echo form_open("accounts/search_invoices", array("class" => "form-horizontal","id"=>"invoices-form"));
						
			            
			            ?>
			            <div class="row">
			                <div class="col-md-6">
			                     <div class="form-group">
			                        <label class="col-md-2 control-label">Patient: </label>
			                        
			                        <div class="col-md-10">
			                            <input type="text" class="form-control" name="patient_surname" placeholder="Patient Name">
			                        </div>
			                    </div>
			                    
			                    <div class="form-group">
			                        <label class="col-md-2 control-label">Invoice: </label>
			                        
			                        <div class="col-md-10">
			                            <input type="text" class="form-control" name="invoice_number" placeholder="Invoice Number">
			                        </div>
			                    </div>
			                    
			                    <div class="form-group">
			                    	<div class="col-md-2">
			                    		Status
			                    	</div>
			                        <div class="col-md-3">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="invoice_status" value="2"  checked>
			                                    All
			                                </label>
			                            </div>
			                        </div>
			                        
			                        <div class="col-md-3">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="invoice_status" value="0" >
			                                    Unprocessed Invoices
			                                </label>
			                            </div>
			                        </div>
			                        <div class="col-md-3">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="invoice_status" value="1" >
			                                   	Processed Invoices
			                                </label>
			                            </div>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                    	<div class="col-md-2">
			                    		Balance
			                    	</div>
			                        <div class="col-md-3">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="bill_status" value="0"  checked>
			                                    All
			                                </label>
			                            </div>
			                        </div>
			                        
			                        <div class="col-md-3">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="bill_status" value="1" >
			                                    Cleared bills
			                                </label>
			                            </div>
			                        </div>
			                        <div class="col-md-3">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="bill_status" value="2" >
			                                   	Uncleared
			                                </label>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="col-md-6">
			                                
			                    <div class="form-group">
	                                <label class="col-md-2 control-label">From: </label>
	                                
	                                <div class="col-md-10">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-calendar"></i>
	                                        </span>
	                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From">
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-md-2 control-label">To: </label>
	                                
	                                <div class="col-md-10">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-calendar"></i>
	                                        </span>
	                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="form-group">
			                        <label class="col-md-4 control-label">Patient Type: </label>
			                        <div class="col-md-8">
			                            <select class="form-control custom-select" name="visit_type_id" id="visit_type_id">
			                                <option value="">---Select Visit Type---</option>
			                                <?php
			                                    if(count($type) > 0){
			                                        foreach($type as $row):
			                                            $type_name = $row->visit_type_name;
			                                            $type_id= $row->visit_type_id;
			                                            ?><option value="<?php echo $type_id; ?>" ><?php echo $type_name ?></option>
			                                        <?php   
			                                        endforeach;
			                                    }
			                                ?>
			                            </select>
			                        </div>
			                    </div>

	                            <div class="form-group">
		                            <div class="col-md-6 col-md-offset-3">
		                                <div class="center-align">
		                                    <button type="submit" class="btn btn-sm btn-info">Search</button>
		                                     <a class="btn btn-sm btn-warning" onclick="javascript:xport.toCSV('PATIENT INVOICES');"> Exports List</a>
		                                </div>
		                            </div>
		                        </div>
			                </div>
			            </div>
			            <?php
			            echo form_close();
			            ?>
			    	</div>
			</section>

			<div class="panel-body" style="height:60vh !important;overflow-y:scroll;padding: 0px !important;">
				<div id="invoices-list"></div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel-body" style="margin-bottom:5px !important;" >
				<div id="invoice-header"></div>
			</div>
		
			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				<div id="invoice-detail"></div>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	$(document).ready(function(){
			$("#visit_type_id").customselect();
      	get_all_invoices();
      	// get_selected_list();
	 });



	function get_all_invoices()
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"debtors/get_all_invoices";
			// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);
			    	// alert(data);
			  if(data.message == 'success')	 
			  {

			  		$('#invoices-list').html(data.results);
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


  	function view_invoice_details(invoice_id,visit_id,patient_id,inpatient)
  	{
  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"debtors/view_invoice_details/"+invoice_id+"/"+visit_id+"/"+patient_id+"/"+inpatient;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#invoice-detail').html(data.results);
		  	$('#invoice-header').html(data.header);

		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function approve_invoice(invoice_id,invoice_type_id,creditor_id)
  	{
  		open_sidebar();

  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"auth/login/authenticate_user/2";
		window.localStorage.setItem('invoice_id',invoice_id);
		window.localStorage.setItem('invoice_type_id',invoice_type_id);
		window.localStorage.setItem('creditor_id',creditor_id);

		$.ajax({
		type:'POST',
		url: url,
		data:{invoice_id: invoice_id,invoice_type_id:invoice_type_id,creditor_id:creditor_id},
		dataType: 'text',
		success:function(data){
		  


		    document.getElementById("current-sidebar-div").style.display = "block"; 
   			$("#current-sidebar-div").html(data);
		 
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		
  	}

  	$(document).on("submit","form#confirm-authenitcation-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);

		var res = confirm('Are you sure you want authenticate this action ?');

		var invoice_id = window.localStorage.getItem('invoice_id');
		var invoice_type_id = window.localStorage.getItem('invoice_type_id');
		var creditor_id = window.localStorage.getItem('creditor_id');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"accounting/creditors/approve_invoice/"+invoice_id+"/"+invoice_type_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
	       	contentType: false,
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
				{
	    			close_side_bar();
					get_all_invoices();
					view_invoice_details(invoice_id,invoice_type_id,creditor_id);

				}
				else
				{
					alert(data.result);
				}
			 
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
		
		
		
	   
		
	});

	function decline_invoice(invoice_id,invoice_type_id,creditor_id)
  	{
  		open_sidebar();

  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"auth/login/authenticate_user/3";
		window.localStorage.setItem('invoice_id',invoice_id);
		window.localStorage.setItem('invoice_type_id',invoice_type_id);
		window.localStorage.setItem('creditor_id',creditor_id);

		$.ajax({
		type:'POST',
		url: url,
		data:{invoice_id: invoice_id,invoice_type_id:invoice_type_id,creditor_id:creditor_id},
		dataType: 'text',
		success:function(data){
		  


		    document.getElementById("current-sidebar-div").style.display = "block"; 
   			$("#current-sidebar-div").html(data);
		 
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		
  	}

  		$(document).on("submit","form#confirm-authenitcation-decline",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);

		var res = confirm('Are you sure you want authenticate this action ?');

		var invoice_id = window.localStorage.getItem('invoice_id');
		var invoice_type_id = window.localStorage.getItem('invoice_type_id');
		var creditor_id = window.localStorage.getItem('creditor_id');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"accounting/creditors/decline_invoice/"+invoice_id+"/"+invoice_type_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
	       	contentType: false,
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
				{
	    			close_side_bar();
					get_all_invoices();
					view_invoice_details(invoice_id,invoice_type_id,creditor_id);

				}
				else
				{
					alert(data.result);
				}
			 
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
		
		
		
	   
		
	});

	


  	function approve_invoice_old(invoice_id,invoice_type_id,creditor_id)
  	{

  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"accounting/creditors/approve_invoice/"+invoice_id+"/"+invoice_type_id+"/"+creditor_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	get_all_invoices();

		  	
		  }
		  else
		  {
		
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


  	function decline_invoice_old(invoice_id,invoice_type_id,creditor_id)
  	{

  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"accounting/creditors/decline_invoice/"+invoice_id+"/"+invoice_type_id+"/"+creditor_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	get_all_invoices();
		  }
		   

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function add_to_list(product_id,requisition_id)
  	{
  		var res = confirm('Are you sure you want to add this product to the list ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_item_to_list/"+product_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});

  		}
  		get_selected_list(requisition_id);
  		
  	}

  	function update_item(requisition_item_id,requisition_id)
  	{


  		var config_url = $('#config_url').val();
  		var units = $('#units'+requisition_item_id).val();
	 	var url = config_url+"inventory/requisition/update_items/"+requisition_item_id+"/"+requisition_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{units: units},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 
		  get_selected_list(requisition_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


	function remove_items(requisition_item_id,requisition_id)
  	{

  		var res = confirm('Are you sure you want to remove this product to the list ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/delete_requisition_item/"+requisition_item_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function confirm_requisition(requisition_id)
  	{

  		var res = confirm('Are you sure you want to confirm this request of requisition ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/confirm_requisition/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	window.location.href = 	config_url+'procurement/requisitions';
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

	function get_all_req_items(requisition_id=0)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/product_list/"+requisition_id;
			 var name = $('#requisition_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, drug : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#products-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}

	  function get_all_prod_items(requisition_id=0)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/selected_products/"+requisition_id;
			var name = $('#product_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, product_name : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


$(document).on("submit","form#invoices-form",function(e)
{

	e.preventDefault();
	
	var form_data = new FormData(this);

	var config_url = $('#config_url').val();	

	 var url = config_url+"debtors/get_all_invoices";

	 // alert(url);
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          var data = jQuery.parseJSON(data);
        
          if(data.message == 'success')	 
		  {

		  		$('#invoices-list').html(data.results);
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
	 
	
   
	
});



        var xport = {
          _fallbacktoCSV: true,  
          toXLS: function(tableId, filename) {   
            this._filename = (typeof filename == 'undefined') ? tableId : filename;
            
            //var ieVersion = this._getMsieVersion();
            //Fallback to CSV for IE & Edge
            if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
              return this.toCSV(tableId);
            } else if (this._getMsieVersion() || this._isFirefox()) {
              alert("Not supported browser");
            }

            //Other Browser can download xls
            var htmltable = document.getElementById(tableId);
            var html = htmltable.outerHTML;

            this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
          },
          toCSV: function(tableId, filename) {
            this._filename = (typeof filename === 'undefined') ? tableId : filename;
            // Generate our CSV string from out HTML Table
            var csv = this._tableToCSV(document.getElementById(tableId));
            // Create a CSV Blob
            var blob = new Blob([csv], { type: "text/csv" });

            // Determine which approach to take for the download
            if (navigator.msSaveOrOpenBlob) {
              // Works for Internet Explorer and Microsoft Edge
              navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
            } else {      
              this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
            }
          },
          _getMsieVersion: function() {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf("MSIE ");
            if (msie > 0) {
              // IE 10 or older => return version number
              return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
            }

            var trident = ua.indexOf("Trident/");
            if (trident > 0) {
              // IE 11 => return version number
              var rv = ua.indexOf("rv:");
              return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
            }

            var edge = ua.indexOf("Edge/");
            if (edge > 0) {
              // Edge (IE 12+) => return version number
              return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
            }

            // other browser
            return false;
          },
          _isFirefox: function(){
            if (navigator.userAgent.indexOf("Firefox") > 0) {
              return 1;
            }
            
            return 0;
          },
          _downloadAnchor: function(content, ext) {
              var anchor = document.createElement("a");
              anchor.style = "display:none !important";
              anchor.id = "downloadanchor";
              document.body.appendChild(anchor);

              // If the [download] attribute is supported, try to use it
              
              if ("download" in anchor) {
                anchor.download = this._filename + "." + ext;
              }
              anchor.href = content;
              anchor.click();
              anchor.remove();
          },
          _tableToCSV: function(table) {
            // We'll be co-opting `slice` to create arrays
            var slice = Array.prototype.slice;

            return slice
              .call(table.rows)
              .map(function(row) {
                return slice
                  .call(row.cells)
                  .map(function(cell) {
                    return '"t"'.replace("t", cell.textContent);
                  })
                  .join(",");
              })
              .join("\r\n");
          }
        };
	  
	

</script>
