
<?php

$visit_invoice_number ='';
$preauth_date = date('Y-m-d');
$created = date('Y-m-d');
$preauth_amount = '';
$bill_to = 0;
$preauth = 0;
$approved_amount = 0;
$claim_number = '';
if(!empty($visit_invoice_id))
{
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = $value->preauth_date;
			$created = $value->created;
			$preauth_amount = $value->preauth_amount;
			$scheme_name = $value->scheme_name;
			$member_number = $value->member_number;
			$bill_to = $value->bill_to;
			$insurance_limit = $value->insurance_limit;
			$claim_number = $value->claim_number;
			$preauth_status = $value->preauth_status;
			$preauth = $value->preauth;
			$approved_amount = $value->approved_amount;
			

		}
	}
}



$visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 0;
$close_card = 3;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$close_card = $value->close_card;
		$visit_type_id = $value->visit_type;
		$visit_time_out = $value->visit_time_out;
		$parent_visit = $value->parent_visit;
		$close_card = $value->close_card;
		$visit_id = $value->visit_id;
		$dentist_id = $value->personnel_id;
		$visit_date = $value->visit_date;
		// $patient_id = $value->patient_id;
		$visit_type = $value->visit_type;
		$member_number = $value->patient_insurance_number;
		$insurance_limit = $value->insurance_limit;
		$insurance_description = $value->insurance_description;
		$visit_type = $value->visit_type;
		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
	}
}


if($bill_to > 0)
{
	$visit_type = $bill_to;
}
else
{
	$scheme_name = $insurance_description;
	$member_number = $member_number;
	$insurance_limit = $insurance_limit;
}

if($visit_type == 257)
{
	$display = 'none';
}
else if($visit_type_id >= 1)
{
	$display = 'block';
}

if(empty($approved_amount))
{
	$approved_amount = 0;
}

// $visit__rs1  = $this->accounts_model->get_incomplete_invoices($patient_id,NULL,$visit_date,$visit_invoice_id);
// var_dump($visit_id)	;die();
// echo form_open("finance/creditors/confirm_invoice_note/".$visit_id."/".$patient_id, array("class" => "form-horizontal","id" => "confirm-invoice"));
  // $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_time_tree(null,$visit_invoice_id);
$result = "

				<table align='center' class='table table-striped table-bordered table-condensed'>
				<tr>
					<th>#</th>
					<th>Date</th>
					<th>Services/Items</th>
					<th style='text-align: center;'>Units</th>
					<th style='text-align: center;'>Unit Cost (Ksh)</th>
					<th style='text-align: center;'>Total</th>

				</tr>
			";
	$total = 0;
    $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_time_tree(null,$visit_invoice_id);
        // var_dump($item_invoiced_rs->result());die();
	$total_amount= 0; 
	$days = 0;
	$item_count = 0;
	if($item_invoiced_rs->num_rows() > 0)
	{
		foreach ($item_invoiced_rs->result() as  $value) {
			# code...
			// $service_id= $value->service_id;
			$billing_date = $value->billing_date;

			$rs2 = $this->accounts_model->get_visit_procedure_charges_per_date_inpatient($visit_id,$billing_date,$visit_invoice_id); 

			
			
			$sub_total= 0; 
			$personnel_query = $this->personnel_model->retrieve_personnel();
				
			if(count($rs2) >0){
				$count = 0;
				$visit_date_day = '';
				
				foreach ($rs2 as $key1):
					$v_procedure_id = $key1->visit_charge_id;
					$procedure_id = $key1->service_charge_id;
					$date = $key1->date;
					$time = $key1->time;
					$visit_charge_timestamp = $key1->visit_charge_timestamp;
					$visit_charge_amount = $key1->visit_charge_amount;
					$units = $key1->visit_charge_units;
					$procedure_name = $key1->service_charge_name;
					$service_id = $key1->service_id;
					$provider_id = $key1->provider_id;
					$service_name = $key1->service_name;
				
					
					$visit_date = date('l d F Y',strtotime($date));
					$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
					if($visit_date_day != $visit_date)
					{
						
						$visit_date_day = $visit_date;
					}
					else
					{
						$visit_date_day = '';
					}

					

					if($personnel_query->num_rows() > 0)
					{
						$personnel_result = $personnel_query->result();
						
						foreach($personnel_result as $adm)
						{
							$personnel_id = $adm->personnel_id;
							
							
								if($personnel_id == $provider_id)
								{
									$provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

									$procedure_name = $procedure_name.$provider_id;
								}
							
							

							
							
							
						}

					}
					
					else
					{
						$provider_id = '';
						
					}
					// if($service_name == 'Bed charge')
					// {
					// 	$procedure_name = $service_name;
					// 	$days++;
					// }




					$item_count++;
					// if($procedure_id != 7769)
					// {
						$sub_total= $sub_total +($units * $visit_charge_amount);
						$result .="
								<tr> 
									<td >".$item_count."</td>
									<td >".date('jS M Y',strtotime($date))."</td>
									<td >".ucwords(strtolower($procedure_name))."</td>
									<td align='center'>".$units."</td>
									<td align='center'>".number_format($visit_charge_amount,2)."</td>
									<td align='center'>".number_format($units * $visit_charge_amount,2)."</td>
									
								</tr>	
						";
					
					$visit_date_day = $visit_date;
					// }
					endforeach;
					

			}
			
			$total_amount = $total_amount + $sub_total;

		}
	}
	
	 $days = $this->accounts_model->days_billed($visit_invoice_id);
	$grand_total = $total_amount;
	if($rebate_status == 1)
	{
		$grand_total += ($days * 3000);
		$rebate_amount = ($days * 3000);
	}
	$result .="
				<tr >
					<td colspan='5'><strong>Grand Total: </strong></td>
					<td colspan='3' align='center'><strong>".number_format($grand_total,2)." </strong></td>
				</tr>
				<tr >
					<td colspan='7'></td>
				</tr>
				";
	$result .="</tbody>
                              </table>";
	

  echo $result;
      ?>
                                     