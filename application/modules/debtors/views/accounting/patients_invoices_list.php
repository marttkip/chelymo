<?php
		
		$result = '';
		$total_invoice = 0;
		$total_balance = 0;
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed table-linked" id="PATIENT INVOICES">
				<thead>
					<tr>
						<th>#</th>
						<th>PATIENT NAME</th>
						<th>VISIT DATE</th>
						<th>INVOICE NUMBER </th>
						<th>INVOICE DATE</th>
                        <th>INVOICE TYPE</th>
                        <th>INVOICE STATUS</th>
                        <th>VISIT STATUS</th>
                        <th>VISIT TPYE</th>
                        <th>INVOICE AMOUNT</th>
                        <th>INVOICE BALANCE</th>
					</tr>
				</thead>
				 <tbody>
				  
			';
			

			
			
			foreach ($query->result() as $key)
			{

				 

				    $patient_name = $key->patient_name;
				    $patient_id = $key->patient_id;
	                $invoice_number = $key->invoice_number;
	                $invoice_id = $key->invoice_id;
	                $invoice_date = $key->invoice_date;
	                $visit_type_name = $key->visit_type_name;
	                $invoice_status = $key->invoice_status;
	                $invoice_bill = $key->invoice_bill;
	                $invoice_balance = $key->invoice_balance;
	                $open_status = $key->open_status;
	                $visit_type_name = $key->visit_type_name;
	                $visit_id = $key->visit_id;
	                $visit_closed = $key->visit_closed;
	                $invoice_status = $key->invoice_status;
	                $inpatient = $key->inpatient;
	                $visit_date = $key->visit_date;

	                if($invoice_status == 1)
	                {
	                	$billing_status = 'Processed';
	                	$color_code = 'success';
	                }
	                else if($invoice_status == 0){
	                	$billing_status = 'Unprocessed';
	                	$color_code = 'danger';
	                }
	                else{
	                	$billing_status = '';
	                	$color_code = '';
	                }

	                if($visit_closed == 1)
	                {
	                	$visit_status = 'Closed';
	                }
	               	else
	               	{
	               		$visit_status = 'Open';
	               	}
					

					if($inpatient == 1)
					{
						$type_name = 'I.P';
					}
					else
					{
						$type_name = 'O.P';
					}
				$count++;
				$total_invoice += $invoice_bill;
				$total_balance += $invoice_balance;
				$result .= 
							'

								<tr onclick="view_invoice_details('.$invoice_id.','.$visit_id.','.$patient_id.','.$inpatient.')">
									<td class="'.$color_code.'">'.$count.' </td>
									<td class="'.$color_code.'">'.$patient_name.'</td>
									<td class="'.$color_code.'">'.$visit_date.'</td>
									<td class="'.$color_code.'">'.$invoice_number.'</td>
									<td class="'.$color_code.'">'.$invoice_date.'</td>
									<td class="'.$color_code.'">'.$visit_type_name.'</td>
									<td class="'.$color_code.'">'.$billing_status.'</td>
									<td class="'.$color_code.'">'.$visit_status.'</td>
									<td class="'.$color_code.'">'.$type_name.'</td>
									<td>'.number_format($invoice_bill,2).'</td>
									<td>'.number_format($invoice_balance,2).'</td>
								</tr> 
							';
			}
			$result .= 
			'
				</tbody>
				<tfoot>
					
						<th colspan="8"></th>
				  		<th>Total</th>
				  		<th>'.number_format($total_invoice,2).'</th>
				  		<th>'.number_format($total_balance,2).'</th>
				  	
				</tfoot>
			  
			</table>
			';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There Are No Invoices";
		}

		echo $result;
?>