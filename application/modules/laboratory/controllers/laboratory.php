<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// error_reporting();
class Laboratory  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		$this->load->model('lab_model');
		$this->load->model('reception/reception_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('inventory_management/inventory_management_model');

		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/database');
		$this->load->model('administration/personnel_model');
		
		/*$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}*/
	}
	
	public function index()
	{
		$this->session->unset_userdata('visit_search');
		$this->session->unset_userdata('patient_search');
		
		$where = 'visit_department.visit_id = visit.visit_id AND visit_department.department_id = 4 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit.visit_date = \''.date('Y-m-d').'\'';
		
		$table = 'visit_department, visit, patients';
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, 6, 0);
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		$v_data['visit'] = 4;
		$v_data['department'] = 4;
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('dashboard', $v_data, TRUE);
		
		$data['title'] = 'Dashboard';
		$data['sidebar'] = 'lab_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}
	public function lab_queue($page_name = 12)
	{
		//with branch code
		$branch_code = $this->session->userdata('branch_code');
		$where = 'visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.department_id = 6 AND visit_department.accounts = 1 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type AND visit.visit_date = \''.date('Y-m-d').'\' AND visit.branch_code = \''.$branch_code.'\'';
		
		$table = 'visit_department, visit, patients, visit_type';
		
		$visit_search = $this->session->userdata('patient_visit_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		$segment = 4;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'laboratory/lab_queue/'.$page_name;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Lab Queue';
		$v_data['title'] = 'Lab Queue';
		$v_data['module'] = 0;
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('lab_queue', $v_data, true);
		
		
		$data['sidebar'] = 'lab_sidebar';
		
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
	}
	public function queue_cheker($page_name = NULL)
	{
		$where = 'visit_department.visit_id = visit.visit_id AND visit_department.department_id = 4 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit.visit_date = \''.date('Y-m-d').'\'';
		$table = 'visit_department, visit, patients';
		$items = "*";
		$order = "visit.visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}

	}
	public function test($visit_id)
	{
		$v_data = array('visit_id'=>$visit_id,'visit'=>1);
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['close_card'] = $patient['close_card'];
		$v_data['inpatient'] = $patient['inpatient'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$visit_date = $this->reception_model->get_visit_date($visit_id);
		$gender = $patient['gender'];
		$visit_date = date('jS M Y',strtotime($visit_date));
		$v_data['age'] = $age;
		$v_data['visit_date'] = $visit_date;
		$v_data['gender'] = $gender;

		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}

		$lab_test_order = 'service_charge_name';

		$lab_test_where = 'service_charge.service_charge_status = 1 AND service_charge.service_charge_delete = 0 AND lab_test.lab_test_delete = 0 AND service_charge.service_charge_name = lab_test.lab_test_name AND lab_test_class.lab_test_class_id = lab_test.lab_test_class_id  AND service_charge.service_id = service.service_id AND (service.service_name = "Lab" OR service.service_name = "lab" OR service.service_name = "Laboratory" OR service.service_name = "laboratory" OR service.service_name = "Laboratory test")  AND  service_charge.visit_type_id = 1';
		    
		$lab_test_table = '`service_charge`, lab_test_class, lab_test, service';

		$lab_test_query = $this->lab_model->get_inpatient_lab_tests($lab_test_table, $lab_test_where, $lab_test_order);

		$rs11 = $lab_test_query->result();
		$lab_tests = '';
		foreach ($rs11 as $lab_test_rs) :


		  $lab_test_id = $lab_test_rs->service_charge_id;
		  $lab_test_name = $lab_test_rs->service_charge_name;

		  $lab_test_price = $lab_test_rs->service_charge_amount;

		  $lab_tests .="<option value='".$lab_test_id."'>".$lab_test_name." KES.".$lab_test_price."</option>";

		endforeach;

		$v_data['lab_tests'] = $lab_tests;

		$consumble_where = 'service_charge.product_id = product.product_id AND category.category_id = product.category_id AND category.category_name = "Consumable" AND service_charge.visit_type_id = 1';
		$consumable_table = 'service_charge,product,category';
		
// 		 $store_priviledges = $this->inventory_management_model->get_assigned_stores();
// 		if($store_priviledges->num_rows() > 0)
// 		{
// 			$consumble_where .= ' AND store_product.store_id = store.store_id AND product.product_id = store_product.product_id 
// AND store.store_parent > 0 AND store_product.store_id IN (';
// 			$consumable_table .=',store_product, store';
// 			$count = 0;
// 			foreach ($store_priviledges->result() as $key) 
// 			{
// 				# code...
// 				$store_parent = $key->store_parent;
// 				$store_id = $key->store_id;
// 				$count ++;
// 				if($count == $store_priviledges->num_rows())
// 				{
// 					$consumble_where .= ' '.$store_id;
// 				}
// 				else
// 				{
// 					$consumble_where .= ' '.$store_id.',';
// 				}
				
// 			}
// 			$consumble_where .=')';
// 		}
		$consumable_order = 'service_charge.service_charge_name';
		$consumable_query = $this->nurse_model->get_inpatient_consumable_list($consumable_table, $consumble_where, $consumable_order);
		$rs8 = $consumable_query->result();
		$consumables = '';
		foreach ($rs8 as $consumable_rs) :
		$consumable_id = $consumable_rs->service_charge_id;
		$consumable_name = $consumable_rs->service_charge_name;
		$consumable_name_stud = $consumable_rs->service_charge_amount;
		    $consumables .="<option value='".$consumable_id."'>".$consumable_name." KES.".$consumable_name_stud."</option>";
		endforeach;
		
		$v_data['consumables'] = $consumables;
		$data['content'] = $this->load->view('tests/test', $v_data, true);
		$data['sidebar'] = 'lab_sidebar';
		$data['title'] = 'Laboratory Test List';
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_laboratory_tests($visit_id)
	{
		$this->form_validation->set_rules('search_item', 'Search', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$search = ' AND lab_test_name LIKE \'%'.$this->input->post('search_item').'%\'';
			$this->session->set_userdata('lab_test_search', $search);
		}
		
		$this->laboratory_list(0,$visit_id);
	}
	
	public function close_lab_test_search($visit_id)
	{
		$this->session->unset_userdata('lab_test_search');
		$this->laboratory_list(0,$visit_id);
	}
	public function test1($visit_id)
	{
		$data = array('visit_id'=>$visit_id, 'lab_test'=>1, 'coming_from'=>'Lab');
		$this->load->view('tests/test1',$data);
	}
	public function visit_results($visit_id)
	{
		$data = array('visit_id'=>$visit_id, 'lab_test'=>1, 'coming_from'=>'Lab');
		// var_dump($data); die();
		$this->load->view('tests/visit_tests',$data);
	}
	public function test2($visit_id)
	{
		$data = array('visit_id'=>$visit_id);
		$this->load->view('tests/test2',$data);
	}
	public function laboratory_list($lab,$visit_id)
	{
		//check patient visit type
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
			# code...
			  $visit_t = $rs1->visit_type;
		  }
		}

		if ($lab ==2){
			$this->session->set_userdata('nurse_lab',$lab);
		}
		else {
			$this->session->set_userdata('nurse_lab',NULL);
		}

		
		$order = 'service_charge_name';
		
		//$where = 'service_charge.service_charge_name = lab_test.lab_test_name AND lab_test_class.lab_test_class_id = lab_test.lab_test_class_id  AND service_charge.service_id = service.service_id AND service.branch_code = "'.$this->session->userdata('branch_code').'" AND (service.service_name = "Lab" OR service.service_name = "lab" OR service.service_name = "Laboratory" OR service.service_name = "laboratory" OR service.service_name = "Laboratory test")  AND  service_charge.visit_type_id = '.$visit_t;
		$where = 'service_charge.service_charge_name = lab_test.lab_test_name AND lab_test_class.lab_test_class_id = lab_test.lab_test_class_id  AND service_charge.service_id = service.service_id AND (service.service_name = "Lab" OR service.service_name = "lab" OR service.service_name = "Laboratory" OR service.service_name = "laboratory" OR service.service_name = "Laboratory test")  AND  service_charge.visit_type_id = 1';
		$test_search = $this->session->userdata('lab_test_search');
		
		if(!empty($test_search))
		{
			$where .= $test_search;
		}
		
		$table = '`service_charge`, lab_test_class, lab_test, service';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'laboratory/laboratory_list/'.$lab.'/'.$visit_id;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = 5;
		$config['per_page'] = 15;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->lab_model->get_lab_tests($table, $where, $config["per_page"], $page, $order);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Laboratory Test List';
		$v_data['title'] = 'Laboratory Test List';
		
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('laboratory_list', $v_data, true);
		
		$data['title'] = 'Laboratory Test List';
		$this->load->view('admin/templates/no_sidebar', $data);	
	}

	public function delete_cost($service_charge_id, $visit_id)
	{
		$this->lab_model->delete_visit_lab_test($service_charge_id,$visit_id);
		
		//$this->laboratory_list(0, $visit_id);
		$this->test_lab($visit_id);
	}
	public function remove_cost($visit_charge_id, $visit_id)
	{
		if($this->lab_model->delete_cost($visit_charge_id))
		{
			redirect("laboratory/test/".$visit_id);
		}
		else
		{
			redirect("laboratory/test/".$visit_id);
		}
	}
	public function add_lab_cost($visit_lab_test_id,$visit_id)
	{
		if($this->lab_model->save_lab_visit($visit_id,$visit_lab_test_id))
		{
			redirect("laboratory/test/".$visit_id);
		}
		else
		{
			redirect("laboratory/test/".$visit_id);
		}
	}

	public function update_lab_charge_amount($visit_lab_test_id,$visit_id)
	{
		$amount = $this->input->post('amount');
		$charge_date = $this->input->post('charge_date');
		if($amount > 0)
		{
			$this->lab_model->update_lab_test_charge_to_visit_charge($visit_id,$visit_lab_test_id,$charge_date);
			$result = "You have successfully billed for the lab test";

		}
		else
		{
			$result = "Sorry something went wrong, please try again";
		}
		echo  $result;
		
	}

	public function test_lab($visit_id, $service_charge_id=NULL)
	{

		$data = array('service_charge_id' => $service_charge_id, 'visit_id' => $visit_id); 		
		$data = array('service_charge_id' => $service_charge_id, 'visit_id' => $visit_id);
		//$this->load->view('test_lab', $data);

		$this->load->view('test_lab', $data);

	}
	public function remove_lab_test($visit_lab_test_id,$visit_id)
	{
		if($this->lab_model->delete_visit_lab_test($visit_lab_test_id,$visit_id))
		{
			redirect("laboratory/test/".$visit_id);
		}
		else
		{
			redirect("laboratory/test/".$visit_id);
		}
	}

	public function confirm_lab_test_charge($visit_id, $service_charge_id=NULL){
		$data = array('service_charge_id' => $service_charge_id, 'visit_id' => $visit_id);
		$this->load->view('confirm_test_lab', $data);
	}

	public function save_result($id,$result,$visit_id)
	{
		$result = str_replace('%20', ' ',$result);
		$result = $this->input->post('result_found');
		$data = array('id'=>$id,'result'=>$result,'visit_id'=>$visit_id);
		$this->load->view('save_result',$data);

	}
	public function finish_lab_test($visit_id)
	{
		redirect('laboratory/lab_queue');
	}

	public function save_comment($comment,$id){
		$comment = str_replace('%20', ' ',$comment);
		$this->lab_model->save_comment($comment, $id);
	}

	public function send_to_doctor($visit_id)
	{
		if($this->reception_model->set_visit_department($visit_id, 2))
		{
			redirect('queues/outpatient-queue');
		}
		else
		{
			redirect('queues/outpatient-queue');
		}
	}
	public function send_to_accounts($visit_id,$module= NULL)
	{
		redirect("nurse/send_to_accounts/".$visit_id."/2");
	}
	public function test_history($visit_id,$page_name = NULL)
	{
		// this is it
		$where = 'visit.patient_id = patients.patient_id AND visit.patient_id = (SELECT patient_id FROM visit WHERE visit.visit_id = visit_department.visit_id ) AND visit_department.department_id = 4 AND visit_department.visit_id != '.$visit_id.'  AND visit.visit_id = '.$visit_id.' ';
		$visit_search = $this->session->userdata('visit_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		
		if($page_name == NULL)
		{
			$segment = 3;
		}
		
		else
		{
			$segment = 4;
		}
		$table = 'visit_department,visit, patients';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'laboratory/test_history/'.$page_name;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->lab_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Test History';
		$v_data['title'] = 'Test History';
		$v_data['module'] = 0;
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('test_history', $v_data, true);
		
		
		$data['sidebar'] = 'lab_sidebar';
		
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}
	public function get_doctor_data($visit_id)
	{
		# code...
		
	}

	function print_test($visit_id,$page=NULL)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$data['page'] = $page;
		$this->load->view('print_test', $data);
	}

	function print_test_with_date($visit_id,$date)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$data['page'] = NULL;
		$data['lab_test_date'] = $date;
		$this->load->view('print_test', $data);
	}

	function print_test_old($visit_id, $patient_id)
	{

		$personnel_query = $this->personnel_model->get_all_personnel();

		$personnel_id = $this->get_doctor_data($visit_id);

		//creators and editors
		if($personnel_query->num_rows() > 0)
		{
			$personnel_result = $personnel_query->result();
			
			foreach($personnel_result as $adm)
			{
				$personnel_id2 = $adm->personnel_id;
				
				if($personnel_id == $personnel_id2)
				{
					$doctor = $adm->personnel_fname;
					break;
				}
				
				else
				{
					$doctor = '-';
				}
			}
		}
		
		else
		{
			$doctor = '-';
		}
				
		$this->load->library('fpdf');
		$this->fpdf->AliasNbPages();
		$this->fpdf->AddPage();
		$this->fpdf->setFont('Times', '', 10);
		$this->fpdf->SetFillColor(190, 186, 211);
		
		$lab_rs = $this->lab_model->get_lab_visit($visit_id);
		$num_lab_visit = count($lab_rs);

		$rs2 = $this->lab_model->get_comment($visit_id);
		$num_rows2 = count($rs2);

		if($num_rows2 > 0){
			foreach ($rs2 as $key2):
				$comment = $key2->lab_visit_comment;
				$visit_date = $key2->visit_date;
				$this->session->set_userdata('visit_date',$visit_date);
			endforeach;
			
		}

		$s = 0;
		
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$visit_type = $patient['visit_type'];
		$patient_type = $patient['patient_type'];
		$patient_othernames = $patient['patient_othernames'];
		$patient_surname = $patient['patient_surname'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$patient_number = $patient['patient_number'];
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$gender = $patient['gender'];
		$contacts = $this->site_model->get_contacts();
		
		$lineBreak = 10;
		
		//Colors of frames, background and Text
		$this->fpdf->SetDrawColor(41, 22, 111);
		$this->fpdf->SetFillColor(190, 186, 211);
		$this->fpdf->SetTextColor(41, 22, 111);

		//thickness of frame (mm)
		//$this->fpdf->SetLineWidth(1);
		//Logo
		$this->fpdf->Image(base_url().'assets/logo/'.$contacts['logo'],65,3,0,30);
		//font
		$this->fpdf->SetFont('Arial', 'B', 12);
		//title

		$this->fpdf->Ln(25);
		$this->fpdf->Cell(0,5, 'DIAGNOSTIC LABORATORY SERVICES', 0, 1, 'C');
		$this->fpdf->Cell(0, 5, 'Laboratory Report Form', 0, 1, 'C');
		$this->fpdf->Cell(0, 5, 'Date: '.$visit_date, '0', 1, 'C');

		$this->fpdf->Cell(100,5,'Name:	'.$patient_surname." ".$patient_othernames, 0, 0, 'L');
		$this->fpdf->Cell(50,5,'Age:'.$age, 0, 0, 'L');
		
		$this->session->set_userdata('patient_sex',$gender);
		$this->fpdf->Cell(50,5,'Sex:'.$gender, 0, 1, 'L');
		$this->fpdf->Cell(180,7,'Requesting Doctor : Dr.'.$doctor, 0, 1, 'C');
		//$this->fpdf->Cell(-30);//move left
		$this->fpdf->Cell(0,7,'Clinic Number:'.$patient_number, 'B', 1, 'L');
		//line break
		$pageH = 7;
		$this->fpdf->SetTextColor(0, 0, 0);
		$this->fpdf->SetDrawColor(0, 0, 0);
		$this->fpdf->SetFont('Times','B',10);
			
		$this->fpdf->SetDrawColor(41, 22, 111);
		$personnel_id = $this->session->userdata('personnel_id');
	
		$rs2 = $this->lab_model->get_lab_personnel($personnel_id);
		$num_rows = count($rs2);

		if($num_rows > 0){
			foreach($rs2 as $key):
				$personnel = $key->personnel_surname;
				$personnel = $personnel." ".$key->personnel_fname;
			endforeach;
			
		}
		
		else{
			$personnel = "";
		}

		//HEADER
		$billTotal = 0;
		$linespacing = 2;
		$majorSpacing = 7;
		$pageH = 5;
		$counter = 0;
		 $next_name ="";  $test_format=""; $lab_test_name=""; $fill="";
		if($num_lab_visit > 0){
			foreach ($lab_rs as $key_lab){
				$visit_charge_id = $key_lab->visit_lab_test_id;
				
				$rsy2 = $this->lab_model->get_test_comment($visit_charge_id);
				$num_rowsy2 = count($rsy2);
				
				if($num_rowsy2 >0){
					$comment4= $rsy2[0]->lab_visit_format_comments;
				}
				else {
				
					$comment4="";	
				}
				$format_rs = $this->lab_model->get_lab_visit_result($visit_charge_id);
				$num_format = count($format_rs);
				
				if($num_format > 0){
					$rs = $this->lab_model->get_test($visit_charge_id);
					$num_lab = count($rs);
				}
				
				else{
					$rs = $this->lab_model->get_m_test($visit_charge_id);
					$num_lab = count($rs);
				}
				
				if($num_lab > 0){
					$counts =0;
					foreach ($rs as $key_what){
						$counts++;
						$lab_test_name = $key_what->lab_test_name;
						$lab_test_class_name = $key_what->lab_test_class_name;
						$lab_test_units = $key_what->lab_test_units;
						$lab_test_lower_limit = $key_what->lab_test_malelowerlimit;
						$lab_test_upper_limit = $key_what->lab_test_malelupperlimit;
						$lab_test_lower_limit1 = $key_what->lab_test_femalelowerlimit;
						$lab_test_upper_limit1 = $key_what->lab_test_femaleupperlimit;
						$visit_charge_id = $key_what->lab_visit_id;
						$lab_results = $key_what->lab_visit_result;
						
						//results for formats
						if($this->session->userdata('test') ==0){
						
							$test_format = $key_what->lab_test_formatname;
							$lab_test_format_id = $key_what->lab_test_format_id;
							$lab_results = $key_what->lab_visit_results_result;
							$lab_test_units = $key_what->lab_test_format_units;
							$lab_test_lower_limit = $key_what->lab_test_format_malelowerlimit;
							$lab_test_upper_limit = $key_what->lab_test_format_maleupperlimit;
							$lab_test_lower_limit1 = $key_what->lab_test_format_femalelowerlimit;
							$lab_test_upper_limit1 = $key_what->lab_test_format_femaleupperlimit;
						}
						
						//if there are no formats
						else{
							$test_format ="-";
						}
						
						if(($counter % 2) == 0){
							$fill = TRUE;
						}
						
						else{
							$fill = FALSE;
						}
						
						if ($counts < ($num_lab-1)){
							$next_name = $rs[$counts]->lab_test_name;
						}
						
						if(($lab_test_name <> $next_name) || ($counts == 1))
						{
							$this->fpdf->Ln(5);
							
							$this->fpdf->SetFont('Times', 'B', 10);
							$this->fpdf->Cell(50,$pageH,"TEST: ".$lab_test_name, 'B',1,'L', FALSE);
							$this->fpdf->Cell(50,$pageH,"CLASS: ".$lab_test_class_name, 'B',1,'L', FALSE);
							
							$this->fpdf->Ln(2);
							
							$this->fpdf->Cell(50,$pageH,"Sub Test", 1,0,'L', FALSE);
							$this->fpdf->Cell(50,$pageH,"Results",1,0,'L', FALSE);
							$this->fpdf->Cell(50,$pageH,"Units",1,0,'L', FALSE);
							$this->fpdf->Cell(30,$pageH,"Normal Limits",1,1,'L', FALSE);
							$this->fpdf->SetFont('Times', '', 10);
						}
						
						$this->fpdf->Cell(50,$pageH,$test_format, 1,0,'L', $fill);
						$this->fpdf->Cell(50,$pageH,$lab_results,1,0,'L', $fill);
						$this->fpdf->Cell(50,$pageH,$lab_test_units,1,0,'L', $fill);
						
						if($this->session->userdata('patient_sex') == "Male"){
							$this->fpdf->Cell(30,$pageH,$lab_test_lower_limit." - ".$lab_test_upper_limit,1,1,'L', $fill);
						}
						
						else{
							$this->fpdf->Cell(30,$pageH,$lab_test_lower_limit1." - ".$lab_test_upper_limit1,1,1,'L', $fill);
						}
						$counter++;
					}
			
				if($test_format !="-"){ 
					$this->fpdf->Ln(3);
					$this->fpdf->SetFont('Times', 'B', 10);
					$this->fpdf->Cell(0,10,"".$lab_test_name ."  Comment ",'B',1,'L', $fill);
					$this->fpdf->SetFont('Times', '', 10);
					$this->fpdf->Cell(0,10,$comment4,0,1,'L', $fill=true);
				
				}	
			}}
				
				if(($counter % 2) == 0){
					$fill = TRUE;
				}
				
				else{
					$fill = FALSE;
				}
				
				$this->fpdf->Ln(5);
				$this->fpdf->SetFont('Times', 'B', 10);
				$this->fpdf->Cell(0,10,"Comments ",'B',1,'L', $fill);
				$this->fpdf->SetFont('Times', '', 10);
				$this->fpdf->Cell(0,10,$comment,0,1,'L', $fill);
		}
		$this->fpdf->Output();

	}
	
	public function save_result_lab()
	{
		$visit_id = $this->input->post('visit_id');
		if($this->lab_model->save_tests_format2($visit_id))
		{
			echo 'true';
		}
		
		else
		{
			echo 'false';
		}
	}
	
	public function save_lab_comment()
	{
		$visit_charge_id = $this->input->post('visit_charge_id');
		$query = $this->lab_model->get_lab_comments($visit_charge_id);
		$num_rows = $query->num_rows();
		
		
		if ($num_rows == 0)
		{
			$this->lab_model->save_new_lab_comment();
		}
			
		else
		{
			$this->lab_model->update_existing_lab_comment($visit_charge_id);
		} 
	}
	public function update_lab_test_status()
	{
		$visit_lab_test_id = $this->input->post('visit_lab_test_id');
		
		$array['test_status'] = $this->input->post('test_status');

		$this->db->where('visit_lab_test_id',$visit_lab_test_id);
		$this->db->update('visit_lab_test',$array);
	}
	public function search_visit_patients($module = NULL)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$patient_number = $this->input->post('patient_number');
		$payroll_number = $this->input->post('payroll_number');
		
		if(!empty($payroll_number))
		{
			$payroll_number = ' AND patients.strath_no = \''.$payroll_number.'\'';
		}
		
		if(!empty($patient_number))
		{
			$patient_number = ' AND patients.patient_number = \''.$patient_number.'\'';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND patients.visit_type_id = '.$visit_type_id.' ';
		}
		
		//search surname
		if(!empty($_POST['surname']))
		{
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\'';
				}
				
				else
				{
					$surname .= ' patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['othernames']))
		{
			$other_names = explode(" ",$_POST['othernames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\'';
				}
				
				else
				{
					$other_name .= ' patients.patient_othernames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $visit_type_id.$patient_number.$surname.$other_name.$payroll_number;
		$this->session->set_userdata('patient_visit_search', $search);
		
		$this->lab_queue();
		
		
	}
	public function close_queue_search()
	{
		$this->session->unset_userdata('patient_visit_search');
		$this->lab_queue();
	}
	
	public function hold_card($visit_id)
	{
		$this->db->where('visit_id', $visit_id);
		if($this->db->update('visit', array('close_card' => 7, 'held_by' => $this->session->userdata('personnel_id'))))
		{
			$this->session->set_userdata('success_message', 'Card held successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Unable to hold card. Please try again');
		}
		
		redirect('laboratory/test/'.$visit_id);
	}
	
	public function release_card($visit_id)
	{
		$this->db->where('visit_id', $visit_id);
		if($this->db->update('visit', array('close_card' => 0)))
		{
			$this->session->set_userdata('success_message', 'Card released successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Unable to release card. Please try again');
		}
		
		redirect('laboratory/test/'.$visit_id);
	}
	
	public function update_visit_lab_tests()
	{
		$query = $this->db->get('visit_lab_test');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$visit_lab_test_id = $res->visit_lab_test_id;
				$visit_id = $res->visit_id;
				$service_charge_id = $res->service_charge_id;
				
				$data['visit_lab_test_id'] = $visit_lab_test_id;
				$this->db->where(array('visit_id' => $visit_id, 'service_charge_id' => $service_charge_id,));
				if($this->db->update('visit_charge', $data))
				{
				}
			}
		}
	}
	public function lab_test_reports($order = 'visit.visit_date', $order_method = 'DESC')
	{
		$where = 'lab_test.lab_test_id = service_charge.lab_test_id AND visit_lab_test.service_charge_id = service_charge.service_charge_id AND visit_lab_test.visit_id = visit.visit_id AND visit.patient_id = patients.patient_id';
		$table = 'lab_test, service_charge, visit_lab_test, visit, patients';
		
		$lab_test_search_report = $this->session->userdata('lab_test_search_report');
		if(!empty($lab_test_search_report))
		{
			$where .= $lab_test_search_report;
		}
		
		$segment = 5;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'laboratory/lab-tests/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->lab_model->get_all_visit_patient_tests($table, $where, $config["per_page"], $page, $order, $order_method);
		
		$page_title = 'Lab Test Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('lab_tests', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function search_patient_lab_test_reports()
	{
		$department_mane = $this->input->post('department_mane');
		$payroll_number = $this->input->post('payroll_number');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_type_id = $this->input->post('visit_type_id');
		$gender_id = $this->input->post('gender_id');
		$age = $this->input->post('age');
		$id_no = $this->input->post('id_no');
		
		if(!empty($id_no))
		{
			$id_no = ' AND patients.patient_national_id = \''.$id_no.'\'';
		}
		
		$year = date('Y');
		if(!empty($age))
		{
			//0 =below 5, 1 = 5-14 years, 2 = 15 and above
			if($age == 0)
			{
				$age = $year1 - 5;
				$add = ' AND YEAR(patients.patient_date_of_birth) > '.$age;
			}
			elseif($age == 1)
			{
				$lower_limit_age = $year - 14;
				$upper_limit_age = $year - 5;
				$add = ' AND YEAR(patients.patient_date_of_birth) >= '.$upper_limit_age.' AND (patients.patient_date_of_birth) < '.$lower_limit_age;
				
			}
			else
			{
				$lower_limit_age = $year - 15;
				$add = ' AND YEAR(patients.patient_date_of_birth) <= '.$lower_limit_age;
			}
			$age = $add;
		}
		
		else
		{
			$age = '';
		}
		
		if(!empty($department_mane))
		{
			$dpt_name = explode(" ",$department_mane);
			$total = count($dpt_name);
			
			$count = 1;
			$department_mane = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$department_mane .= ' visit.department_name LIKE \'%'.mysql_real_escape_string($dpt_name[$r]).'%\'';
				}
				
				else
				{
					$department_mane .= ' visit.department_name LIKE \'%'.mysql_real_escape_string($dpt_name[$r]).'%\' AND ';
				}
				$count++;
			}
			$department_mane .= ') ';
		}
		
		if(!empty($payroll_number))
		{
			$payroll_number = ' AND patients.strath_no = \''.$payroll_number.'\'';
		}
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date >= \''.$visit_date_from.'\' AND visit.visit_date <= \''.$visit_date_to.'\'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date >= \''.$visit_date_from.'\'';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date <= \''.$visit_date_to.'\'';
		}
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = \''.$visit_type_id.'\' ';
		}
		if(!empty($gender_id))
		{
			$gender = ' AND patients.gender_id = '.$gender_id;
			if($gender_id == 0)
			{
				$gender = '';
			}
		}
		$search = $visit_date.$payroll_number.$department_mane.$visit_type_id.$gender.$age.$id_no;

		$this->session->set_userdata('lab_test_search_report', $search);
		redirect('laboratory/lab-tests');
	}
	
	public function close_lab_test_search_report()
	{
		$this->session->unset_userdata('lab_test_search_report');
		redirect('laboratory/lab-tests');
	}

	public function view_visit_lab_results($visit_id)
	{
		$patient_id = $this->nurse_model->get_patient_id($visit_id);
		$data = array('visit_id'=>$visit_id,"patient_id"=>$patient_id);

		$page = $this->load->view('tests/view_lab',$data);

		echo $page;


	}

}
?>