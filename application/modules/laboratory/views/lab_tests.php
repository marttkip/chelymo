<?php
echo $this->load->view('search/search_lab_test_results');
$result = '';
if($query->num_rows() > 0)
{
	$count = $page;
	$result.=
		'
			<table class="table table-hover table-bordered table-striped table-responsive col-md-12" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Payroll No</th>
						<th>Visit Date</th>
						<th>Patient Names</th>
						<th>Results</th>
					</tr>
				</thead>
				<tbody>
		';
	foreach($query->result() as $patient_lab_tests)
	{
		$lab_test_results = '';
		$payroll_number = $patient_lab_tests->strath_no;
		$patient_fname = $patient_lab_tests->patient_surname;
		$patient_oname = $patient_lab_tests->patient_othernames;
		$patient_name = $patient_fname.' '.$patient_oname;
		$visit_date = $patient_lab_tests->visit_date;
		$visit_id = $patient_lab_tests->visit_id;
		$lab_test_id = $patient_lab_tests->lab_test_id;
		$lab_test_name = $patient_lab_tests->service_charge_name;
		$service_charge_id = $patient_lab_tests->service_charge_id;
		$count++;
		
		$result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$payroll_number.'</td>
						<td>'.date('jS M Y',strtotime($visit_date)).'</td>
						<td>'.$patient_name.'</td>
						<td><a href="'.site_url().'laboratory/print_test/'.$visit_id.'" target="_blank" class="btn btn-sm btn-info">Print</a></td>
					</tr>
					';
		
	}
	$result.=
			'
				</tbody>
			</table>
			';
}
else
{
	$result.='No lab patient had a lab test done';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    
    <div class="panel-body">
    	<?php 
    	$lab_test_search_report = $this->session->userdata('lab_test_search_report');
		if(!empty($lab_test_search_report))
		{
			echo '<a href="'.site_url().'laboratory/close_lab_test_search_report" class="btn btn-sm btn-warning">Close Search</a>';
		}
		echo $result;
		?>
    </div>
    <div class="widget-foot">
                                
		<?php if(isset($links)){echo $links;}?>
    
        <div class="clearfix"></div> 
    
    </div>
</section>