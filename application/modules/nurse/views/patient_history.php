<!-- search -->
<?php 
	$patient_id = $this->nurse_model->get_patient_id($visit_id);
	// this is it
	
	$where = 'visit.`visit_id` <> '.$visit_id.' AND visit.visit_date <= "'.date('Y-m-d').'" AND visit.patient_id = patients.patient_id AND visit.`patient_id`='.$patient_id;
	
	$table = 'visit,patients';
	//pagination
	$this->load->library('pagination');
	$config['base_url'] = site_url().'nurse/patient_card/'.$visit_id;
	$config['total_rows'] = $this->reception_model->count_items($table, $where);
	$config['uri_segment'] = 4;
	$config['per_page'] = 20;
	$config['num_links'] = 5;
	
	$config['full_tag_open'] = '<ul class="pagination pull-right">';
	$config['full_tag_close'] = '</ul>';
	
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	
	$config['next_tag_open'] = '<li>';
	$config['next_link'] = 'Next';
	$config['next_tag_close'] = '</span>';
	
	$config['prev_tag_open'] = '<li>';
	$config['prev_link'] = 'Prev';
	$config['prev_tag_close'] = '</li>';
	
	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';
	
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$this->pagination->initialize($config);
	
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	     $v_data["links"] = $this->pagination->create_links();
	$query = $this->nurse_model->get_all_patient_history($table, $where, $config["per_page"], $page);
	
	
	
	?>
<!-- end search -->
<!-- <div class="row"> -->
	<div class="col-md-12">
		<!-- Widget -->
		<div class="widget boxed">
			<!-- Widget head -->
			
			<!-- Widget content -->
			<div class="widget-content">
				<div class="padd">
					<?php
						$search = $this->session->userdata('visit_search');
						
						if(!empty($search))
						{
						    echo '<a href="'.site_url().'nurse/close_queue_search" class="btn btn-warning">Close Search</a>';
						}
						$result = '';
						
						//if users exist display them
						if ($query->num_rows() > 0)
						{
						    $count = $page;
						    
						    $result .= 
						        '
						            <table class="table table-hover table-bordered ">
						              <thead>
						                <tr>
						                  <th>#</th>
						                  <th width="10%">Visit Date</th>
						                  <th width="10%">Consultant</th>
						                  <th width="25%">Investigations</th>
						                  <th width="20%">Diagnosis</th>
						                  <th width="25%">Assessment</th>
						                  <th colspan="4">Actions</th>
						                </tr>
						              </thead>
						              <tbody>
						        ';
						    
						    // $personnel_query = $this->personnel_model->get_all_personnel();
						    $count = 1;
						    foreach ($query->result() as $row)
						    {
						        $visit_date = date('jS M Y',strtotime($row->visit_date));
						        $visit_time = date('H:i a',strtotime($row->visit_time));
						
						        $visit_id1 = $row->visit_id;
						        $waiting_time = $this->nurse_model->waiting_time($visit_id1);
						        $patient_id = $row->patient_id;
						        $personnel_id = $row->personnel_id;
						        $dependant_id = $row->dependant_id;
						        $strath_no = $row->strath_no;
						        $created_by = $row->created_by;
						        $modified_by = $row->modified_by;
						        $visit_type_id = $row->visit_type_id;
						        $visit_type = $row->visit_type;
						        $created = $row->patient_date;
						        $last_modified = $row->last_modified;
						        $last_visit = $row->last_visit;
						        
						        $patient_type = $this->reception_model->get_patient_type($visit_type_id);
						        
						        $visit_type = 'General';
						        
						        $patient_othernames = $row->patient_othernames;
						        $patient_surname = $row->patient_surname;
						        $title_id = $row->title_id;
						        $patient_date_of_birth = $row->patient_date_of_birth;
						        $civil_status_id = $row->civil_status_id;
						        $patient_address = $row->patient_address;
						        $patient_post_code = $row->patient_postalcode;
						        $patient_town = $row->patient_town;
						        $patient_phone1 = $row->patient_phone1;
						        $patient_phone2 = $row->patient_phone2;
						        $patient_email = $row->patient_email;
						        $patient_national_id = $row->patient_national_id;
						        $religion_id = $row->religion_id;
						        $gender_id = $row->gender_id;
						        $patient_kin_othernames = $row->patient_kin_othernames;
						        $patient_kin_sname = $row->patient_kin_sname;
						        $relationship_id = $row->relationship_id;
						        $doctor = $row->personnel_fname.' '.$row->personnel_onames;
						        //creators and editors
						        // start of diagnosis
						        $diagnosis_rs = $this->nurse_model->get_diagnosis($visit_id1);
						        $diagnosis_num_rows = count($diagnosis_rs);
						        //echo $num_rows;
						        $patient_diagnosis = "";
						        if($diagnosis_num_rows > 0){
						            foreach ($diagnosis_rs as $diagnosis_key):
						                $diagnosis_id = $diagnosis_key->diagnosis_id;
						                $diagnosis_name = $diagnosis_key->diseases_name;
						                $diagnosis_code = $diagnosis_key->diseases_code;
						                $patient_diagnosis .="".$diagnosis_code."-".$diagnosis_name."<br>";
						                
						            endforeach;
						        }
						        else
						        {
						            // $patient_diagnosis .= "-";
						        }


						        $moh_diagnosis_rs = $this->nurse_model->get_visit_diagnosis($visit_id1);
						        $moh_num_rows = count($moh_diagnosis_rs);
						        //echo $num_rows;
						        $patient_diagnosis .= "<br>";
						        if($moh_num_rows > 0){
						            foreach ($moh_diagnosis_rs as $moh_key):
						                $diagnosis_id = $moh_key->diagnosis_id;
						                $diagnosis_name = $moh_key->diseases_name;
						                $diagnosis_code = $moh_key->diseases_code;
						                $patient_diagnosis .="".$diagnosis_code."-".$diagnosis_name."<br>";
						                
						            endforeach;
						        }
						        else
						        {
						            // $patient_diagnosis .= "-";
						        }

						        $investigations_list ='';
						      	$lab_requests='';
						       	$rs_lab = $this->lab_model->get_lab_visit2($visit_id1);
						       	$num_rows_lab = count($rs_lab);
						       	if($num_rows_lab > 0)
						       	{
						       		$lab_requests = '<h6><strong>LAB WORKS:</strong></h6>';
						       		foreach ($rs_lab as $key7):
						       			$test = $key7->service_charge_name;
										$lab_requests .= '- '.$test.'<br>';

						       		endforeach;
						       		$lab_requests.='<br>';
						       	}


						        $rs = $this->xray_model->get_xray_visit2($visit_id1);
								$num_rows = count($rs);
								
								$total = 0;
								$s=0;
								$ultasound_requests = '';
								if($num_rows > 0)
								{
									$ultasound_requests = '<h6><strong>ULTRASOUND:</strong></h6>';
									foreach ($rs as $key6):
										$test = $key6->service_charge_name;
										$ultasound_requests .= '- '.$test.'<br>';
									endforeach;
									$ultasound_requests.='<br>';
								}
								
								 $investigations_list = $lab_requests.''.$ultasound_requests;


						        $data['visit_id'] = $visit_id1;
						        $data['type'] = 5;
						        $data['module'] = 1;

						        $assessment = $this->load->view('nurse/patients/all_notes', $data,true);


						        $data['visit_id'] = $visit_id1;
						        $data['type'] = 20;
						        $data['module'] = 1;

						        $notes = $this->load->view('nurse/patients/all_notes', $data,true);
								
						       

						        $result .= 
						            '
						                <tr>
						                    <td>'.$count.'</td>
						                    <td>'.$visit_date.'</td>
						                    <td>'.$doctor.'</td>
						                    <td>'.$investigations_list.'</td>
						                    <td>'.$patient_diagnosis.'<br> '.$notes.'</td>
						                    <td>'.$assessment.'</td>

						                    <td>
												<a onclick="view_previous_card('.$visit_id1.')" class="btn  btn-xs btn-info"> View</button>
											</td>
											<td><a href="'.site_url().'print-patient-chart/'.$visit_id.'" target="_blank" class="btn btn-xs btn-danger"> Print</a></td>
						                </tr> 
						            ';
						            $count++;
						    }
						    
						    $result .= 
						    '
						                  </tbody>
						                </table>
						    ';
						}
						
						else
						{
						    $result .= "There is no history";
						}
						
						echo $result;
						?>
				</div>
				<div class="widget-foot">
					<?php if(isset($links)){echo $links;}?>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
<!-- </div> -->