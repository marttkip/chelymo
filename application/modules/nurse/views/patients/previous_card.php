<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_id = $patient['patient_id'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
// $served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));



$title = 'Patient Visit Chart';
$heading = 'Checkup';
$number = 'MI/MED/00'.$visit_id;
?>

<section class="panel" >
	<div class="panel-body" style="height:90vh;overflow-y: scroll;overflow-x: none;">
		<div style="padding-left:10px;padding-right:10px">
	        <div class="col-md-12">
	        	<h4 style="font-weight:bold">NURSE INTAKE ASSESSMENT</h4>
	        	<br>
	        	<div class="row">
	                <div class="padd">
	                	

	                	 <div class="col-md-12">
	                	 	<h4 style="border-bottom: 1px solid #000;">VITALS DONE</h4>	                                
                            <div class="previous_vitals"></div>	                                
                        </div>
                        <div class="col-md-12">
	                	 	<h4  style="border-bottom: 1px solid #000;" >NURSE NOTES & CHIEF COMPLAINT</h4>	  
	                	 	<br>                              
                            <div class="chief-complain"></div>                             
                        </div>
                        
	                   
	                </div>
	            </div>

	            <h4 style="font-weight:bold">DOCTORS ASSESSMENT AND RECOMMENDATIONS</h4>
	        	<br>

	        	<div class="row">
	                <div class="padd">
	                	

	                	 <div class="col-md-12">
	                	 	<div class="col-md-6">
		                	 	<h4 style="border-bottom: 1px solid #000;">DOCTORS NOTE AND SUMMARY</h4>	                                
	                            <div class="doctor-chief-complain"></div> 
	                        </div>
	                        <div class="col-md-6">
                            	<h4 style="border-bottom: 1px solid #000;">ASSESMENT</h4>	                                
                            	<div class="assessment-list"></div>  
                            </div>                               
                        </div>
                        <div class="col-md-12">
	                	 	<h4  style="border-bottom: 1px solid #000;" >INVESTIGATIONS RESULTS</h4>	  
	                	 	<br> 
	                	 	<div class="view-lab-result-view"></div>
	                	 	<?php
	                	 	$v_d['visit_id'] = $visit_id;
	                	 	$v_d['patient_id'] = $patient_id;
	                	 	?>                             
                            <?php //echo $this->load->view("laboratory/tests/view_lab",$v_d, TRUE); ?>                         
                        </div>
                        <div class="col-md-12">
	                	 	<h4  style="border-bottom: 1px solid #000;" >DIAGNOSIS</h4>	  
	                	 	<br>                              
                            <div class="col-md-12">
		                        <div class="col-md-6">                        
		                            <h4 class="left-align">OTHER DIAGNOSIS</h4>
		                            <br>
		                            <div class="other-diagnosis"></div>                        
		                        </div>  
		                        <div class="col-md-6">   
		                            <div class="col-md-12">

					                  <div class="col-md-6">
					                     <h5>MOH</h5>
					                      <div class='visit_diagnosis_moh'></div>
					                  </div>
					                  <div class="col-md-6">
					                       <h5>ICD10</h5>
					                      <div class='visit_diagnosis_original'></div>
					                  </div>
					               </div>

		                        </div>                    
		                  </div>                          
                        </div>
                        <div class="col-md-12">
	                	 	<h4  style="border-bottom: 1px solid #000;" >PRESCRIPTION</h4>	  
	                	 	<br>                              
                             <div class ="visit_prescription"></div>                           
                        </div>
	                   
	                </div>
	            </div>
	            
	           

	        </div>	
		</div>
	</div>
</section>

<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>    
		        </div>
		    </div>
			
		</li>
	</ul>
</div>