<?php

$patient_id = $this->nurse_model->get_patient_id($visit_id);

$operation_list_rs = $this->nurse_model->get_operations_list();
$num_vaccines = count($operation_list_rs);

echo "
			<table align='center' class='table table-striped table-hover table-condensed'>
				<tr>
                        <th>Operation</th>
                        <th>Booked</th>  
                        <th align='center'>Operated</th> 
						</tr>
					";
						foreach ($operation_list_rs->result() as $key):
							
							$vaccine_id = $key->operation_id;
							$vaccine_name = $key->operation_name;
							$rs = $this->nurse_model->check_operation_list($visit_id, $vaccine_id);
							$num_patient_operation = count($rs);
							
							if($num_patient_operation == 0 ){
							
								echo "	
						                             <tr>  
						                                <td align='left'>".$vaccine_name."</td>
						                                <td ><input id='vaccine".$vaccine_id."' name='vaccine".$vaccine_id."' type='checkbox' value='".$vaccine_id."'onclick='save_operation(".$vaccine_id.", 1, ".$visit_id.")'/></td>
																						<td align='right'><input id='vaccine".$vaccine_id."' name='vaccine".$vaccine_id."' type='checkbox' value='".$vaccine_id."' onclick='save_operation(".$vaccine_id.", 0, ".$visit_id.")'/></td>
						                              </tr>
														";
							}
								
							else {
									foreach ($rs as $key2):
										$status = $key2->status_id;
										$patient_vaccine_id = $key2->operations_id;
									endforeach;
									if ($status == 1){
										
										echo "	
						                             <tr>  
						                                <td align='left'>".$vaccine_name."</td>
						                                <td ><input id='vaccine".$vaccine_id."' name='vaccine".$vaccine_id."' type='checkbox' value='".$vaccine_id."'onclick='delete_operation(".$patient_vaccine_id.", ".$visit_id.")' checked='checked'/></td>
																						<td align='right'><input id='vaccine".$vaccine_id."' name='vaccine".$vaccine_id."' type='checkbox' value='".$vaccine_id."' onclick='save_operation(".$vaccine_id.",0, ".$visit_id.")'/></td>
						                              </tr>
														";
									}
										
									else{
												echo "	
						                             <tr>  
						                                <td align='left'>".$vaccine_name."</td>
						                                <td ><input id='vaccine".$vaccine_id."' name='vaccine".$vaccine_id."' type='checkbox' value='".$vaccine_id."'onclick='save_operation(".$vaccine_id.", 1, ".$visit_id.")' /></td>
																						<td align='right'><input id='vaccine".$vaccine_id."' name='vaccine".$vaccine_id."' type='checkbox' value='".$vaccine_id."' onclick='delete_operation(".$patient_vaccine_id.", ".$visit_id.")' checked='checked'/></td>
						                              </tr>
														";
											}
									}
			
							endforeach;
                      echo "
                        </table>";

?>


						
                     