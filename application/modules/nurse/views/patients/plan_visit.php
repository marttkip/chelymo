
<div class="col-md-12">
    <div class="col-md-5">                        
        <section class="panel panel-primary">
          <!-- Widget head -->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-reorder"></i>OTHER DIAGNOSIS</h3>
            <div class="panel-tools pull-right">
              <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
              <a href="#" class="wclose"><i class="icon-remove"></i></a>
            </div>
            <div class="clearfix"></div>
          </div>             
          <div class="panel-body">
            <div class="other-diagnosis"></div>    
          </div>
        </section>

    </div>  
    <div class="col-md-7">   
      <section class="panel panel-primary">
          <!-- Widget head -->
          <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-reorder"></i>DIAGNOSIS</h3>
            <div class="panel-tools pull-right">
              <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
              <a href="#" class="wclose"><i class="icon-remove"></i></a>
            </div>
            <div class="clearfix"></div>
          </div>             
          <div class="panel-body">
  
               <div class="col-md-12">

                  <div class="col-md-6">
                     <h5>MOH</h5>
                     <a class="btn btn-sm btn-warning pull-right" onclick="mohdiagnosis_sidebar(<?php echo $visit_id;?>)"><i class="fa fa-plus"></i> MOH Diagnosis</a>
                      <div class='visit_diagnosis_moh'></div>
                  </div>
                  <div class="col-md-6">
                       <h5>ICD10</h5>
                          <a class="btn btn-sm btn-success pull-right" onclick="icd10codes_sidebar(<?php echo $visit_id;?>)"><i class="fa fa-plus"></i> ICD 10 Diagnosis</a>
                      <div class='visit_diagnosis_original'></div>
                  </div>
               </div>
            </div>
          </section>
        </div>
       
       
    </div>  
<div class="col-md-12">
<div id="prescription-notification" style="display: block;">
  <div class="alert alert-warning alert-sm">Kindly note you have to indicate a diagnosis before you enter a prescription to the patient. Thank you!</div>
</div>
</div>
<div class="col-md-12">
    <div class="col-md-6">
       <section class="panel panel-featured panel-featured-primary">
           <div class="panel-heading ">
              <h4 class="panel-title"><i class="icon-reorder"></i>CONSUMABLES</h4>
              <div class="panel-tools pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>  
          <div class="panel-body">
                  <div class="col-md-12">
                    <div class='col-md-12'>
                      <div class="center-align">
                                
                         <a class='btn btn-success'  onclick='consumables_sidebar(<?php echo $visit_id;?>)'> <i class="fa fa-plus"></i> Add Consumables</a>
           
                      </div>
                      <div class="col-md-12">
                            
                            <div id="consumables"></div>
                      </div>
                  </div>
           </div>
        
      </section>
  
    </div>
    <div class="col-md-6">
      <section class="panel panel-danger" id="prescription-notification-open" style="display: none;">
        <!-- Widget head -->
        <div class="panel-heading">
          <h3 class="panel-title"><i class="icon-reorder"></i>PRESCRIPTION</h3>
          <div class="panel-tools pull-right">
            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
            <a href="#" class="wclose"><i class="icon-remove"></i></a>
          </div>
          <div class="clearfix"></div>
        </div>             
        <div class="panel-body">
             
            <div class="col-md-12">
                <div class="center-align">
                  <a class="btn btn-sm btn-success" onclick='prescription_sidebar(<?php echo $visit_id;?>)' > <i class="fa fa-plus"></i> Add Prescription </a>                  
                </div>
                <div class="visit_prescription"></div>
            </div>
        </div>
      </section>
  
    </div>

    
</div>

<div class="col-md-12">
    <div class="col-md-6">
       <section class="panel panel-featured panel-featured-primary">
           <div class="panel-heading ">
              <h4 class="panel-title"><i class="icon-reorder"></i>MATERNITY SERVICES</h4>
              <div class="panel-tools pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>  
            <div class="widget-content">
          <div class="padd">
            <?php
              $v_data['patient_id'] = $this->reception_model->get_patient_id_from_visit($visit_id);
              $v_data['patient'] = $this->reception_model->patient_names2(NULL, $visit_id);
              
            ?>
            <!-- vitals from java script -->
            <?php echo $this->load->view("patients/maternity_service", $v_data, TRUE); ?>
            <!-- end of vitals data -->
          </div>
        </div>
        
      </section>
  
    </div>

    <div class="col-md-6">
       <section class="panel panel-featured panel-featured-primary">
           <div class="panel-heading ">
              <h4 class="panel-title"><i class="icon-reorder"></i>OPERATIONS</h4>
              <div class="panel-tools pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>  
            <div class="widget-content">
          <div class="padd">
            <?php
              $v_data['patient_id'] = $this->reception_model->get_patient_id_from_visit($visit_id);
              $v_data['patient'] = $this->reception_model->patient_names2(NULL, $visit_id);
              
              // $v_data['family_query'] = $this->nurse_model->get_family();
            ?>
            <!-- vitals from java script -->
            <div class="operation-list"></div>
           
            <!-- end of vitals data -->
          </div>
        </div>
        
      </section>
  
    </div>
    

    
</div>
