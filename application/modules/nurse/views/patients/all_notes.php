<?php

$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows2 = count($rs2);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] =  $query_data = $this->nurse_model->get_notes($type, $visit_id);

// var_dump($type);

if($query_data->num_rows() > 0)
{
	foreach ($query_data->result() as $key => $value_two) {
		# code...
		$summary = $value_two->notes_name;
		$notes_date = $value_two->notes_date;
		$notes_id = $value_two->notes_id;
		// $notes_date = date('jS M Y',strtotime($row->notes_date));
		// $notes_time = date('H:i a',strtotime($row->notes_time));
	}
	
}
else
{
	$summary = '';
	$notes_date = date('Y-m-d');
}
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}

if($type == 17)
{
	$checked = 'past_diagnosis_dx';

}else if($type == 18)
{
	$checked = 'past_treatment_rx';
	// var_dump($summary); die();
}

else if($type == 1)
{
	$checked = 'chief_complain';
	// var_dump($summary); die();
}
else if($type == 3)
{
	$checked = 'admission_notes';
	// var_dump($summary); die();
}


$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['module'] = $module;
$notes = $this->load->view('nurse/patients/patient_notes', $v_data, TRUE);

$summary = strip_tags($summary);

if($module <> 1)
{

?>
<div class='col-md-12'>
	<div class="row">

    	<div class='col-md-8'>
        	<!-- <input type="hidden" name="date" value="<?php echo date('Y-m-d');?>" /> -->
        	<input type="hidden" name="notes_time" id="notes_time<?php echo $visit_id;?>#<?php echo $type;?>" value="<?php echo date('H:i');?>" />
        	<!-- <div class="row"> -->
				<div class="col-md-12" >
					<input data-format="yyyy-MM-dd" data-plugin-datepicker  class="form-control datepicker"  type="text" name="date" placeholder="Date" id="notes_date<?php echo $visit_id;?>#<?php echo $type;?>" value="<?php echo $notes_date?>">
					
				</div>
				<br>
			<!-- </div> -->
				
            <textarea class='form-control' rows="10" id='<?php echo $checked.$visit_id;?>' placeholder="Describe" ><?php echo $summary?></textarea>
        </div>
    
    	<div class='col-md-4 center-align'>
    		<a class='btn btn-info btn-sm' type='submit' onclick='save_nurses_notes(<?php echo $visit_id;?>,<?php echo $type;?>)'> Save Note</a>
    	</div>
    </div>
</div>
<br>
<div class='col-md-12' style="margin-top: 20px !important;">

	<?php echo $notes?>
</div>
<?php
}
else
{
	?>
	<div class='col-md-12'>
		<?php echo $notes?>
	</div>
	<?php

}

?>
