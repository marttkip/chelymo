<section class="panel" >
	<div class="panel-body" style="height:90vh">
		<div id="sidebar-top">
		<div class="col-md-12">
			<!-- <div class="padd"> -->
			<form action="#" method="get" class="sidebar-form">
				<div class="input-group">
				<input type="text" name="q" id="q" class="form-control" onkeyup="search_radiology_tests(<?php echo $visit_id;?>)" placeholder="Search...">
				<span class="input-group-btn">
				    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
				    </button>
				  </span>
				</div>
			</form>
			<!-- </div> -->
		</div>
		</div>
		<div class="col-md-12" id="sidebar-container" >

			<h3 class="control-sidebar-heading">RADIOLOGY TESTS</h3>
			<div style="height:40vh !important; overflow-y: scroll;">

				<ul class="control-sidebar-menu" id="searched-radiology-test" style="list-style: none;">
			  
			   
			  
				</ul>
				
			</div>
			<div style="height:40vh !important; overflow-y: scroll;">

				<h3 class="control-sidebar-heading">SELECTED RADIOLOGY TESTS</h3>
				<ul class="control-sidebar-menu" id="view-list-radiology-tests" style="list-style: none;">

				  
				   
				  
				</ul>
			</div>
			<!-- <div class="col-md-12">
				<div class="center-align">
					 <?php echo form_open("nurse/send_to_ultrasound/".$visit_id."/1", array("class" => "form-horizontal"));?>
						<input type="submit" class="form-control btn btn-sm btn-success" value="Send To radiology" onclick="return confirm('Do you want to send patient to radiology ?');"/>
					 <?php echo form_close();?>
					
				</div>
			</div> -->

		</div>
	</div>
</section>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        	<!-- <div id="old-patient-button" style="display:none">
			        				        		
			        		
			        	</div> -->
			        	<!-- <div> -->
			        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			        	<!-- </div> -->
			        		
		               
		        </div>
		    </div>
			
		</li>
	</ul>
</div>