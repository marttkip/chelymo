<section class="panel" >
	<div class="panel-body" style="height:80vh">
	<div id="sidebar-top">
	<div class="col-md-12">
		<!-- <div class="padd"> -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
			<input type="text" name="q" id="q" class="form-control" onkeyup="search_laboratory_tests(<?php echo $visit_id;?>)" placeholder="Search...">
			<span class="input-group-btn">
			    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
			    </button>
			  </span>
			</div>
		</form>
		<!-- </div> -->
	</div>
	</div>
	<div class="col-md-12" id="sidebar-container" >

		<h3 class="control-sidebar-heading">LABORATORY TESTS</h3>
		<div style="height: 30vh; overflow-y: scroll;">
			<ul class="control-sidebar-menu" id="searched-lab-test" style="list-style: none;">
			</ul>
		</div>
		<hr>
		<div id="lab_checked_location" style="display: none;">
			<div class="col-md-12" style="padding: 10px;">
				<div class="center-align">
					 <div class="form-group">
						<label class="col-md-2 control-label">Billing Type? </label>
						
                        <div class="col-md-4">
                            <div class="radio alert-info">
                                <label class="">
                                    <input id="optionsRadios2" type="radio" name="billing_type" value="0"  onclick="update_lab_test_billing(0)">
                                    WALK IN ( NO BILLING)
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio alert-success">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="billing_type" value="1" onclick="update_lab_test_billing(1)">
                                    INHOUSE PATIENT (BILL PATIENT)
                                </label>
                            </div>
                        </div>
                        
                        
					</div>
				</div>
			</div>
		</div>

		<h3 class="control-sidebar-heading">SELECTED LAB TESTS</h3>
		<ul class="control-sidebar-menu" id="view-list-lab-test" style="list-style: none;">
		  
		   
		  
		</ul>
		<div class="col-md-12">
			<div class="center-align">
				 <?php echo form_open("nurse/send_to_labs/".$visit_id."/1", array("class" => "form-horizontal"));?>
					<!-- <input type="submit" class="form-control btn btn-sm btn-success" value="Send To Laboratory" onclick="return confirm('Do you want to send patient to laboratory ?');"/> -->
				 <?php echo form_close();?>
				 <!-- <input type='button' class='form-control btn btn-sm btn-danger' value='Close Side Bar ' data-toggle='control-sidebar' /> -->
			</div>
		</div>

	</div>

</div>
</section>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        	<!-- <div id="old-patient-button" style="display:none">
			        				        		
			        		
			        	</div> -->
			        	<!-- <div> -->
			        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			        	<!-- </div> -->
			        		
		               
		        </div>
		    </div>
			
		</li>
	</ul>
</div>