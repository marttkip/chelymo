<section class="panel" >
	<div class="panel-body" style="height:80vh">
<div class="col-md-12" id="sidebar-container" style="margin-bottom: 20px;" >
<?php

$next_app_rs = $this->nurse_model->get_next_appointment($visit_id,$patient_id);


$visit_date = '';
$appointment_note = '';
if($next_app_rs->num_rows() == 1)
{
	foreach ($next_app_rs->result() as $key => $value) {
		# code...
		$visit_date = $value->visit_date;
		$appointment_note = $value->appointment_note;
	}
	
}

?>
	
	<!-- <form id="next_appointment_detail" method="post" > -->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
		        <label class="col-md-2 control-label">Date: </label>
		        
		          <div class="col-md-9">
		              <div class="input-group">
		                  <span class="input-group-addon">
		                      <i class="fa fa-calendar"></i>
		                  </span>
		                  <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="datepicker form-control" name="next_appointment_date" id="next_appointment_date<?php echo $visit_id?>" placeholder="Date" value="<?php echo date('Y-m-d')?>" style="background-color: #ffffff;" >
		              </div>
		          </div>
		          <div class="col-md-1">
		          	<a onclick="view_schedule(<?php echo $visit_id;?>,<?php echo $personnel_id;?>)" class="btn btn-xs btn-warning"> view schedule </a>
		          </div>
		      </div>
		      <div class="form-group">
		        <label class="col-md-2 control-label">Note: </label>
		        <div class="col-md-10">
		            <textarea class="form-control" name="appointment_note" rows="5" id="appointment_note<?php echo $visit_id;?>"><?php echo $appointment_note?></textarea>
		        </div>
		      </div>

		      <div class="row">
		      		<div class="col-md-6">
		      			 <div class="form-group">
		                    <label class="col-lg-4 control-label">Start time : </label>
		                
		                    <div class="col-lg-8">
		                        <div class="input-group">
		                            <span class="input-group-addon">
		                                <i class="fa fa-clock-o"></i>
		                            </span>
		                            <input type="text" class="form-control timepicker" data-plugin-timepicker="" name="timepicker_start" id="timepicker_start<?php echo $visit_id;?>">
		                        </div>
		                    </div>
		                </div>
		      		</div>
		      		<div class="col-md-6">
		      			  <div class="form-group">
							<label class="col-lg-4 control-label"> Duration: </label>
							<div class="col-lg-8">
								<select name="event_duration" id="event_duration<?php echo $visit_id;?>" class="form-control">
				

									<option value="15" >15 Min</option>
									<option value="30" >30 Min</option>
									<option value="45" >45 Min</option>
									<option value="60" >1 Hrs</option>
									<option value="90" >1 Hrs 30 Min</option>
									<option value="120" >2 Hrs</option>
									<option value="180" >3 Hrs</option>
									<option value="240" >4 Hrs</option>
									<option value="300" >5 Hrs</option>
									<option value="360" >6 Hrs</option>
									
								</select>
							</div>
						 </div>
		      		</div>
		      	
		      </div>

		      </br>
		      <input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
		      <input type="hidden" name="patient_id" id="patient_id<?php echo $visit_id?>" value="<?php echo $patient_id?>">
		      <input type="hidden" name="department_id" id="department_id<?php echo $visit_id?>" value="<?php echo $department_id?>">
		      <input type="hidden" name="doctor_id" id="doctor_id<?php echo $visit_id?>" value="<?php echo $personnel_id?>">
		      <div class="row">
		        <div class="center-align ">
		          <button class="btn btn-info btn-sm" type="submit" onclick="save_appointment(<?php echo $visit_id?>)">Save Appointment Detail</button>
		        </div>
		      </div>
		     </div>
		     <div class="col-md-6">
		     	<div  id="doctors_schedule<?php echo $visit_id;?>"> </div>
		     </div>
		</div>
       
    <!-- </form> -->

</div>
</div>
</section>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        	<!-- <div id="old-patient-button" style="display:none">
			        				        		
			        		
			        	</div> -->
			        	<!-- <div> -->
			        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			        	<!-- </div> -->
			        		
		               
		        </div>
		    </div>
			
		</li>
	</ul>
</div>