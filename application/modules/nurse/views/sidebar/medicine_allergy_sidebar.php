<section class="panel" >
	<div class="panel-body" style="height:80vh">
		<div id="sidebar-top">
			<div class="col-md-12">
				<!-- <div class="padd"> -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
					<input type="text" name="q" id="q" class="form-control" onkeyup="search_drug_list(<?php echo $visit_id;?>)" placeholder="Search...">
					<span class="input-group-btn">
					    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					    </button>
					  </span>
					</div>
				</form>
				<!-- </div> -->
			</div>
		</div>
		<div class="col-md-12" id="sidebar-container" >

			<h3 class="control-sidebar-heading">MEDICINE ALLERGY</h3>

			<ul class="control-sidebar-menu" id="searched-medicine" style="list-style: none;">
			  
			   
			  
			</ul>
			<br/>
			<div id="adding-list" style="display: none;">

				<!-- <h3 class="control-sidebar-heading">SELECTED LAB TESTS</h3> -->
				<input type="hidden" class="form-control" id="product_id" value="" readonly="readonly" />
				<div class="row">
				    <div class="col-md-12">
						<input type="text" class="form-control" id="product_name" value="" readonly="readonly" />
					</div>
				</div>
				<br>
				<div class="row">
				    <div class="col-md-12">
						<textarea class="form-control" id="reaction_medicine" placeholder="Reaction" ></textarea>
					</div>
				</div>
				<br>
				<div class="row">
				    <div class="col-md-12">
						<input type="number" class="form-control" id="serviority_medicine" value=""  placeholder="serviority (1 - 5)" />
					</div>
				</div>
				
			</div>
			
			<br/>
			<div class="col-md-12">
				<div class="center-align">
					<input type="submit" class="form-control btn btn-sm btn-success" data-toggle='control-sidebar' value="Save medical allergy" onclick="save_drug_allergy(<?php echo $visit_id;?>)"/>
					 <!-- <input type='button' class='form-control btn btn-sm btn-danger' value='Close Side Bar ' data-toggle='control-sidebar' /> -->
				</div>
			</div>

		</div>
	</div>
</section>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        	<!-- <div id="old-patient-button" style="display:none">
				        				        		
				        		
				        	</div> -->
				        	<!-- <div> -->
				        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        	<!-- </div> -->
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
