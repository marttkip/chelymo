<section class="panel" >
	<div class="panel-body" style="height:80vh">
	<div id="sidebar-top">
	<div class="col-md-12">
		<!-- <div class="padd"> -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
			<input type="text" name="q" id="q" class="form-control" onkeyup="search_symptoms(<?php echo $visit_id;?>)" placeholder="Search...">
			<span class="input-group-btn">
			    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
			    </button>
			  </span>
			</div>
		</form>
		<!-- </div> -->
	</div>
	</div>
	<div class="col-md-12" id="sidebar-container" >

		<h3 class="control-sidebar-heading">Symptoms List</h3>

		<ul class="control-sidebar-menu" id="searched-items">
		  
		   
		  
		</ul>

		<h3 class="control-sidebar-heading">Reviewed Symptoms</h3>
		<ul class="control-sidebar-menu" id="view-list">
		  
		   
		  
		</ul>
	<div id="sidebar-inner">
		

	<div class="row">
	    <div class="col-md-12">
			<textarea class="form-control" id="visit_symptoms" cols="" ></textarea>
		</div>
	</div>
	<br/>

	<div class="col-md-12">
		<div class="center-align">
			 <a  class="form-control btn btn-info btn-sm " onclick='save_soap("<?php echo $visit_id;?>")' > Save Record </a>
			 <input type='button' class='form-control btn btn-sm btn-danger' value='Close Side Bar ' data-toggle='control-sidebar' />
		</div>
	</div>

	</div>
	</div>

</div>
</section>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        	<!-- <div id="old-patient-button" style="display:none">
			        				        		
			        		
			        	</div> -->
			        	<!-- <div> -->
			        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			        	<!-- </div> -->
			        		
		               
		        </div>
		    </div>
			
		</li>
	</ul>
</div>