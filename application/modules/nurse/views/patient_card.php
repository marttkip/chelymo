

<div class="col-md-2">
	<div id="sidebar-detail"></div>
</div>
<div class="col-md-10">
	<div id="loader" style="display: none;"></div>

		<?php if($mike == 1){
			}else{?>
		<section class="panel">
			<div class="row">
				<?php if ($module == 0){?>
				<div class="col-md-1">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_doctor/".$visit_id, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-xs btn-primary" value="To Doctor" onclick="return confirm('Send to Doctor?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php }

					$personnel_id = $this->session->userdata('personnel_id');
					$is_front_office = $this->reception_model->check_personnel_department_id($personnel_id,9);
					$is_doctor = $this->reception_model->check_personnel_department_id($personnel_id,2);
					$is_admin = $this->reception_model->check_personnel_department_id($personnel_id,1);
					$is_nurse = $this->reception_model->check_personnel_department_id($personnel_id,8);
					$is_obs = $this->reception_model->check_personnel_department_id($personnel_id,24);
					$is_urologist = $this->reception_model->check_personnel_department_id($personnel_id,23);
					$is_mch = $this->reception_model->check_personnel_department_id($personnel_id,26);




					if(($is_doctor OR $is_obs OR $is_mch OR $is_urologist) AND $module == 1 AND $doctor_id == 0 )
					{
						?>
						<div class="col-md-6">
							<div class="center-align">
								<?php echo form_open("nurse/attend_to_patient/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-sm btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<?php

					}
					else
					{
						?>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-success center-align" value="To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_maternity/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-default center-align" value="To Maternity" onclick="return confirm('Send to Maternity?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_mch/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-primary center-align" value="To MCH" onclick="return confirm('Send to MCH?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_dental/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-warning center-align" value="To Dental" onclick="return confirm('Send to Dental?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_xray/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-danger center-align" value="To XRAY" onclick="return confirm('Send to XRAY?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-info center-align" value="To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<?php
						

					}
				?>
					

				<div class="col-md-1">
					<div class='center-align '>

				        <a class='btn btn-warning btn-xs' target="_blank" href="<?php echo base_url().'print-patient-chart/'.$visit_id?>" ><i class="fa fa-print"></i> Print Chart</a>
				    </div>
				</div>
				<div class="col-md-2">
					<div class="center-align">
						<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-xs pull-right " ><i class="fa fa-arrow-left"></i> Back Outpatient Queue</a>
					</div>
				</div>
			</div>
		</section>
		<?php } ?>
		
		<div class="center-align">
			<?php
				$error = $this->session->userdata('error_message');
				$validation_error = validation_errors();
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
				}
				
				if(!empty($validation_error))
				{
				echo '<div class="alert alert-danger">'.$validation_error.'</div>';
				}
				
				if(!empty($success))
				{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
				}
				?>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<?php



		if(($is_doctor OR $is_obs OR $is_mch OR $is_urologist) AND $module == 1 AND $doctor_id > 0  OR $personnel_id == 0)
		{

		?>
			<div class="tabbable" style="margin-bottom: 18px;">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a href="#patient-history" data-toggle="tab">Past Medical History</a></li>
					<?php if($mike == 1){
						}else{?><li ><a href="#vitals-pane" data-toggle="tab">Nurse Intake Assesment</a></li>
					<?php
						}
						?>
					<li><a href="#soap" data-toggle="tab">Doctors Notes</a></li>
					<!-- <li><a href="#medical-checkup" data-toggle="tab">Examination Findings</a></li> -->
					<li><a href="#investigations" data-toggle="tab" onclick="get_patient_investigation(<?php echo $visit_id?>)">Investigations & Results</a></li>
					<li><a href="#plan-visit" data-toggle="tab" onclick="get_patient_plan(<?php echo $visit_id;?>)">Plan</a></li>
					<!-- <li><a href="#out_patient_discharge" data-toggle="tab">Discharge Notes</a></li> -->
				</ul>
				<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
					<div class="tab-pane active" id="patient-history" style="height: 70vh !important;overflow-y: scroll;">
						<div id="patient-history-view"></div>
					</div>	
				
					<div class="tab-pane " id="vitals-pane" style="height: 70vh !important;overflow-y: scroll;">

						<?php echo $this->load->view("patients/vitals", '', TRUE);?>

						<?php echo $this->load->view("patients/lifestyle", '', TRUE); ?>
					</div>

					<div class="tab-pane" id="soap" style="height: 70vh !important;overflow-y: scroll;">
						<?php echo $this->load->view("patients/soap", '', TRUE);?>

					</div>
					<div class="tab-pane" id="out_patient_discharge" style="height: 70vh !important;overflow-y: scroll;">
						 <?php //echo $this->load->view("doctor/patients/discharge_summary", '', TRUE);?>
					</div>
					<div class="tab-pane" id="investigations" style="height: 70vh !important;overflow-y: scroll;">
						<div id="patient-investigation-view"></div>
					</div>
					<div class="tab-pane" id="plan-visit" style="height: 70vh !important;overflow-y: scroll;">
						<div id="patient-plan-view"></div>
						<?php //echo $this->load->view("patients/plan_visit", '', TRUE);?>
					</div>
					<div class="tab-pane" id="visit_trail">
						<?php //echo $this->load->view("patients/visit_trail", '', TRUE);?>
					</div>
				</div>
			</div>
		<?php
		}
		else if(($is_nurse OR $is_front_office) AND $module == 0)
		{


			?>
			<div class="tabbable" style="margin-bottom: 18px;">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a href="#patient-history" data-toggle="tab">Past Medical History</a></li>
					<?php if($mike == 1){
						}else{?><li ><a href="#vitals-pane" data-toggle="tab">Vitals</a></li>
					<?php
						}
						?>

					<?php

					if($is_nurse)
					{
						?>
						<li><a href="#soap" data-toggle="tab">History</a></li>
						<li><a href="#investigations" data-toggle="tab" onclick="get_patient_investigation(<?php echo $visit_id?>)">Investigations</a></li>
						<li><a href="#plan-visit" data-toggle="tab" onclick="get_patient_plan(<?php echo $visit_id;?>)">Plan</a></li>
						<?php
					}
					?>
					
					<!-- <li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li> -->
				</ul>
				<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
					<div class="tab-pane active" id="patient-history">
						<div id="patient-history-view"></div>
					</div>	
				
					<div class="tab-pane " id="vitals-pane">
						<?php echo $this->load->view("patients/vitals", '', TRUE);?>
					</div>

					<div class="tab-pane" id="soap">
						<?php echo $this->load->view("patients/soap", '', TRUE);?>
					</div>
					<!-- <div class="tab-pane" id="medical-checkup"> -->
						<?php //echo $this->load->view("patients/medical_checkup", '', TRUE);?>
					<!-- </div> -->
					<div class="tab-pane" id="investigations">
						<div id="patient-investigation-view"></div>
					</div>
					<div class="tab-pane" id="plan-visit">
						<div id="patient-plan-view"></div>
						<?php //echo $this->load->view("patients/plan_visit", '', TRUE);?>
					</div>
					<div class="tab-pane" id="visit_trail">
						<?php //echo $this->load->view("patients/visit_trail", '', TRUE);?>
					</div>
				</div>
			</div>
		<?php
		}
		?>
		<?php if($mike == 1){
			}else{?>
		<div class="row">
			<?php if ($module == 0){?>
			<div class="col-md-1">
				<div class="center-align">
					<?php echo form_open("nurse/send_to_doctor/".$visit_id, array("class" => "form-horizontal"));?>
					<input type="submit" class="btn btn-xs btn-primary" value="Send To Doctor" onclick="return confirm('Send to Doctor?');"/>
					<?php echo form_close();?>
				</div>
			</div>
			<?php }
				if(($is_doctor OR $personnel_id == 0 OR $is_obs OR $is_mch OR $is_urologist) AND $module== 1 AND $doctor_id == 0)
				{
					?>
					<div class="col-md-6">
						<div class="center-align">
							<?php echo form_open("nurse/attend_to_patient/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
							<input type="submit" class="btn btn-sm btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
							<?php echo form_close();?>
						</div>
					</div>
					<?php

				}
				else
				{
					?>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-success center-align" value="To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_maternity/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-default center-align" value="To Maternity" onclick="return confirm('Send to Maternity?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_mch/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-primary center-align" value="To MCH" onclick="return confirm('Send to MCH?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_dental/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-warning center-align" value="To Dental" onclick="return confirm('Send to Dental?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_xray/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-danger center-align" value="To XRAY" onclick="return confirm('Send to XRAY?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<div class="col-md-1">
							<div class="center-align">
								<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
								<input type="submit" class="btn btn-xs btn-info center-align" value="To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
								<?php echo form_close();?>
							</div>
						</div>
						<?php
					

				}
			?>
		</div>
		<?php } ?>
</div>
<script type="text/javascript">
	var config_url = document.getElementById("config_url").value;

	
	$(document).ready(function(){
		var visit_id = <?php echo $visit_id?>;
		var patient_id = <?php echo $patient_id?>;
		var config_url = document.getElementById("config_url").value;
		get_patient_history(visit_id);
		get_sidebar_details(patient_id,visit_id);
		
		// $.get(config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
		// 	$("#new-nav").html(data);
		// 	$("#checkup_history").html(data);
		// });
	});

	function get_sidebar_details(patient_id,visit_id=null)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"reception/get_sidebar_details/"+patient_id+"/"+visit_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#sidebar-detail").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}

	function get_patient_history(visit_id)
	{
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_history_view/"+visit_id;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-history-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       // alert("sadhkasjdhakj");
       			
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}
	function get_patient_investigation(visit_id)
	{
		 // document.getElementById("loader-circle").style.display = "block";
		 document.getElementById("loader").style.display = "block";
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_investigation_view/"+visit_id;
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-investigation-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       $("#lab_test_id").customselect();
		       $("#xray_id").customselect();
		       get_lab_table(visit_id);
		       get_xray_table(visit_id);
		       get_xray_scans(visit_id);
		       document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}

	function parse_xray(visit_id)
	{
	  var xray_id = document.getElementById("xray_id").value;
	  xray(xray_id, visit_id);

	}

	function xray(id, visit_id){
	    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id+"/"+id;
	    // window.alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	               document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	               //get_xray_table(visit_id);
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_xray_table(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	                document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_xray_scans(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_scans/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	                document.getElementById("xray_scans").innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_patient_plan(visit_id)
	{
		

  		document.getElementById("loader").style.display = "block";
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_plan_view/"+visit_id;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-plan-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       $("#drug_id").customselect();
		       $("#diseases_id").customselect();
		       $("#theatre_procedure_id").customselect();
		       get_disease(visit_id);
		       get_disease_moh_new(visit_id);
		       display_prescription(visit_id,0);
		       display_inpatient_prescription(visit_id,0);
		       get_theatre_procedures_table(visit_id,0);
		        display_notes(visit_id,9);
		        display_consumable(visit_id);
		        display_operations_status(visit_id);
		       	tinymce.init({
   					selector: ".cleditor",
   					height : "100"
   				});

		       document.getElementById("loader").style.display = "none";
		       // get_lab_table(visit_id);

		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}
	// start of lab details
	function parse_lab_test(visit_id)
   {
     var lab_test_id = document.getElementById("lab_test_id").value;
      lab(lab_test_id, visit_id);
     
   }
    function get_lab_table(visit_id){
         var XMLHttpRequestObject = false;
             
         if (window.XMLHttpRequest) {
         
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
             
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"laboratory/test_lab/"+visit_id;
     
         if(XMLHttpRequestObject) {
                     
             XMLHttpRequestObject.open("GET", url);
                     
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                 }
             }
             
             XMLHttpRequestObject.send(null);
         }
     }
   
    function lab(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"laboratory/test_lab/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_lab_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }


    function open_nurse_model(notes_id)
   {
    
     // tinymce.remove('#nurse_notes'+notes_id);
     $('#edit_notes'+notes_id).modal('show');
     tinymce.init({
                    selector: ".cleditor",
                    height : "100"
                });
      $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });
   }
    function view_previous_card(visit_id)
   {
   		open_sidebar();

   		var config_url = $('#config_url').val();
	      var data_url = config_url+"nurse/previous_card/"+visit_id;
	      // window.alert(data_url);
	      $.ajax({
	      type:'POST',
	      url: data_url,
	      data:{visit_id: visit_id},
	      dataType: 'text',
	      success:function(data){
	      //window.alert("You have successfully updated the symptoms");
	      //obj.innerHTML = XMLHttpRequestObject.responseText;
	       $("#sidebar-div").html(data);

	        $('.datepicker').datepicker({
	              format: 'yyyy-mm-dd'
	          });

	        // $('.datepicker').datepicker();
	        $('.timepicker').timepicker();
	        open_old_visit(visit_id);
	        // alert(data);
	      },
	      error: function(xhr, status, error) {
	      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	      alert(error);
	      }

	      });
   }

   function open_old_visit(visit_id)
   {
   		previous_vitals(visit_id);
   		// display_nurse_notes(visit_id,19,1);
   		// get_procedures_table_view(visit_id);
   		// get_disease(visit_id);
   		// display_notes(visit_id,20,1);
   		// display_notes(visit_id,14,1);
   		// get_lab_table_view_result(visit_id);

   		display_notes(visit_id,1,1);
   	   get_disease(visit_id);
       get_disease_moh_new(visit_id);
       display_prescription(visit_id,0);
       display_inpatient_prescription(visit_id,0);
       get_theatre_procedures_table(visit_id,0);
       // display_notes(visit_id,9);
       display_notes(visit_id,5,1);
       display_notes(visit_id,3,1);
       display_notes(visit_id,7,1);
       display_consumable(visit_id);
       view_lab_test_done(visit_id);
   }

   function view_lab_test_done(visit_id)
   {

   		var config_url = $('#config_url').val();
	  var data_url = config_url+"laboratory/view_visit_lab_results/"+visit_id;
	  // window.alert(data_url);
	  $.ajax({
	  type:'POST',
	  url: data_url,
	  data:{visit_id: visit_id},
	  dataType: 'text',
	  success:function(data){
	 
	   $(".view-lab-result-view").html(data);
	
	  },
	  error: function(xhr, status, error) {
	  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	  alert(error);
	  }

	  });

   }



	// end of lab details
</script>