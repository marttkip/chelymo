<?php

 $department_id  = $this->session->userdata('searched_department_id');
 $result = '';
 if(!empty($department_id))
 {
    $department_query = $this->company_financial_model->get_department_expenses($department_id);
    $x=0;
    $balance = 0;
    if($department_query->num_rows() > 0)
    {
      foreach ($department_query->result() as $key => $value) {
        // code...
        $transactionClassification = $value->transactionClassification;

        $document_number = '';
        $transaction_number = '';
        $finance_purchase_description = '';
        $finance_purchase_amount = 0 ;


         $referenceId = $value->payingFor;
        $document_number =$transaction_number = $value->referenceCode;
        $finance_purchase_description = $value->transactionName;

        $cr_amount = $value->cr_amount;
        $dr_amount = $value->dr_amount;


        $transaction_date = $value->transactionDate;
        $transaction_date = date('jS M Y',strtotime($transaction_date));
        // $creditor_name = $value->creditor_name;
        $creditor_id = 0;//$value->creditor_id;
        $account_name = $value->accountName;
        $finance_purchase_id = '';//$value->finance_purchase_id;


        $balance += $dr_amount;
        $balance -=  $cr_amount;
        $x++;
         $result .= '
                   <tr>
                        <td class="text-left">'.$x.'</td>
                       <td class="text-left">'.$transaction_date.'</td>
                       <td class="text-left">'.$account_name.'</td>
                       <td class="text-left">'.$finance_purchase_description.'</td>

                       <td class="text-right">'.number_format($dr_amount).'</td>
                       <td class="text-right">'.number_format($balance).'</td>
                   </tr>
                    ';
      }
      $result .= '
                <tr>
                      <td ></td>
                      <td ></td>
                      <td ></td>
                     <td class="text-right" >Total</td>
                    <td class="text-right">'.number_format($balance).'</td>
                    <td class="text-right">'.number_format($balance).'</td>
                </tr>
                 ';
    }
 }



 $department_id = $this->session->userdata('searched_department_id');
 // var_dump($department_id);die();
 if(!empty($department_id))
 {
   $department_name = $this->company_financial_model->get_department_name($department_id);
   $search_title = $this->session->userdata('department_expense_title_search');
   $deparment_name = '<strong>'.$department_name.' Department Report</strong><br>';
   $checked = $search_title;

 }
 else {
   $checked = 'Please Search for something';
   $department_name = '';

 }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | P & L</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<?php echo $deparment_name;?>

            	<?php echo $checked;?>

            </div>
        </div>

    	<div class="row">
        	<div style="margin: auto;">
						<div class="col-md-12">

						<table class="table" id="departmentExpense">
							<thead>
								<th > #</th>
								<th >Date</th>
                <th >Account</th>
                <th >Description</th>
                <th >Amount</th>
                <th >Running</th>
							</thead>
							<tbody>
								<?php echo $result;?>
							</tbody>
						</table>
						</div>

        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: .................................................................. </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
          </div>
        </div>

        	<a href="#" onclick="javascript:xport.toCSV('departmentExpense');">XLS</a>
    </body>

</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
