<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Accounts extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();

		$this->load->model('site/site_model');
		$this->load->model('administration/reports_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('reception/reception_model');
		$this->load->model('dental/dental_model');
		$this->load->model('reception/database');

		$this->load->model('medical_admin/medical_admin_model');
		$this->load->model('pharmacy/pharmacy_model');
		$this->load->model('accounting/hospital_accounts_model');
			$this->load->model('inventory_management/inventory_management_model');
		$this->load->model('administration/sync_model');
		$this->load->model('messaging/messaging_model');
		$this->load->model('admin/email_model');
		$this->load->model('finance/purchases_model');
		$this->load->model('laboratory/lab_model');

		$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}

	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('dashboard', $v_data, true);

		$this->load->view('templates/general_page', $data);
	}
	public function index()
	{
		$this->session->unset_userdata('all_transactions_search');

		$data['content'] = $this->load->view('dashboard', '', TRUE);

		$data['title'] = 'Dashboard';
		$data['sidebar'] = 'accounts_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function accounts_queue()
	{
		$branch_code = $this->session->userdata('search_branch_code');

		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}

		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}

		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		$v_data['branches'] = $this->reports_model->get_all_active_branches();
		$where = 'visit.inpatient = 0 AND visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND (visit_department.department_id = 6 OR visit_department.accounts = 0) AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type AND visit.branch_code = \''.$branch_code.'\'AND visit.visit_date = \''.date('Y-m-d').'\'';

		$table = 'visit_department, visit, patients, visit_type';

		$visit_search = $this->session->userdata('visit_accounts_search');

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/accounts_queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = 3;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$v_data['type_links'] =1;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$data['title'] = 'Accounts Queue';
		$v_data['title'] = 'Accounts Queue';
		$v_data['module'] = 0;
		$v_data['close_page'] = 1;

		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();

		$data['content'] = $this->load->view('accounts_queue', $v_data, true);
		$data['sidebar'] = 'accounts_sidebar';

		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}
	public function search_visits($pager=NULL)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$surnames = $this->input->post('surname');
		$personnel_id = $this->input->post('personnel_id');
		$visit_date = $this->input->post('visit_date');
		$othernames = $this->input->post('othernames');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);

		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}

		if(!empty($personnel_id))
		{
			$personnel_id = ' AND visit.personnel_id = '.$personnel_id.' ';
		}

		if(!empty($visit_date))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date.'\' ';
		}

		//search surname
		$surnames = explode(" ",$surnames);
		$total = count($surnames);

		$count = 1;
		$surname = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$surname .= ' patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\'';
			}

			else
			{
				$surname .= ' patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' AND ';
			}
			$count++;
		}
		$surname .= ') ';

		//search other_names
		$other_names = explode(" ",$othernames);
		$total = count($other_names);

		$count = 1;
		$other_name = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\'';
			}

			else
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\' AND ';
			}
			$count++;
		}
		$other_name .= ') ';

		$search = $visit_type_id.$surname.$other_name.$visit_date.$personnel_id;
		$this->session->unset_userdata('visit_accounts_search');
		$this->session->set_userdata('visit_accounts_search', $search);

		redirect('cash-office/patient-visits');


	}
	public function close_queue_search($pager)
	{
		$this->session->unset_userdata('visit_accounts_search');
		redirect('cash-office/patient-visits');
	}
	public function accounts_unclosed_queue()
	{
		//$where = 'visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.department_id = 6 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7)';
		$branch_code = $this->session->userdata('search_branch_code');

		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}

		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}

		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		$v_data['branches'] = $this->reports_model->get_all_active_branches();
		$where = 'visit.inpatient = 0 AND visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type AND visit.branch_code = \''.$branch_code.'\'';

		$table = 'visit_department, visit, patients, visit_type';

		$visit_search = $this->session->userdata('visit_accounts_search');
		$segment = 3;

		if(!empty($visit_search))
		{
			$where .= $visit_search;
			$segment = 4;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/accounts_unclosed_queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
		$v_data['type_links'] =2;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits2($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['close_page'] = 2;

		$data['title'] = 'Accounts Unclosed Visits';
		$v_data['title'] = 'Accounts Unclosed Visits';
		$v_data['module'] = 0;

		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();

		$data['content'] = $this->load->view('accounts_queue', $v_data, true);
		$data['sidebar'] = 'accounts_sidebar';

		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}
	public function accounts_closed_visits()
	{
		$branch_code = $this->session->userdata('search_branch_code');

		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}

		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}

		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		$where = 'visit.visit_delete = 0  AND visit.patient_id = patients.patient_id AND visit.close_card = 1 ';
		$where = 'visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND visit.close_card = 1 AND visit_type.visit_type_id = visit.visit_type AND visit.branch_code = \''.$branch_code.'\'';

		$table = 'visit_department, visit, patients, visit_type';

		$visit_search = $this->session->userdata('visit_accounts_search');
		$segment = 3;

		if(!empty($visit_search))
		{
			$where .= $visit_search;
			$segment = 3;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/accounts_closed_visits';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
        $v_data['type_links'] =3;
		$query = $this->reception_model->get_all_ongoing_visits2($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$data['title'] = 'Accounts closed Visits';
		$v_data['title'] = 'Accounts closed Visits';
		$v_data['module'] = 7;
		$v_data['close_page'] = 3;
		$v_data['branches'] = $this->reports_model->get_all_active_branches();

		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();

		$data['content'] = $this->load->view('accounts_queue', $v_data, true);
		$data['sidebar'] = 'accounts_sidebar';

		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}
	public function invoice($visit_id)
	{
		?>
        	<script type="text/javascript">
        		var config_url = $('#config_url').val();
				window.open(config_url+"/accounts/print_invoice/<?php echo $visit_id;?>","Popup","height=900,width=1200,,scrollbars=yes,"+"directories=yes,location=yes,menubar=yes,"+"resizable=no status=no,history=no top = 50 left = 100");
				window.location.href="<?php echo base_url("index.php/accounts/accounts_queue")?>";
			</script>
        <?php

		$this->accounts_queue();
	}
	public function payments($patient_id,$visit_id=NULL)
	{
		$v_data = array('patient_id'=>$patient_id,'visit_id'=>$visit_id);

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
		$patient = $this->reception_model->get_patient_data($patient_id);
		$patient = $patient->row();
		$patient_othernames = $patient->patient_othernames;
		$patient_surname = $patient->patient_surname;

		$v_data['doctor'] = $this->reception_model->get_providers();


		$v_data['title'] = $patient_othernames.' '.$patient_surname;


		// $rs = $this->nurse_model->check_visit_type($visit_id);
		// if(count($rs)>0){
		//   foreach ($rs as $rs1) {
		//     # code...
		//       $visit_t = $rs1->visit_type;
		//   }
		// }
		// var_dump($visit_t); die();
		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND service.service_name <> "Pharmarcy" AND service.service_delete = 0 AND service_charge.service_charge_status = 1 AND  service_charge.service_charge_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;

			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;



		$this->accounts_model->update_visit_totals($patient_id);
	

		$this->accounts_model->update_all_other_charges($patient_id);



		$v_data['close_page'] = 1;
		$data['content'] = $this->load->view('payments', $v_data, true);

		$data['title'] = 'Payments';
		$data['sidebar'] = 'accounts_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function get_visits_div($patient_id,$page=NULL)
	{
		$v_data['patient_id'] = $patient_id;
		$v_data = array('patient_id'=>$patient_id);


		if($page == NULL)
		{
			$page = 0;
		}
		// $page = 1;

		// var_dump($counted);
		$table= 'visit,visit_invoice';
		$where='visit_invoice.visit_id = visit.visit_id AND visit.patient_id ='.$patient_id;
		$config["per_page"] = $v_data['per_page'] = $per_page = 100000;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}



		$v_data['page'] = $page;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_inpatient_accounts($table, $where, $config["per_page"], $page);

		$v_data['visit_list'] = $query;
		$primary_key = $patient['patient_id'];

		$this->load->view('visit_list', $v_data);
	}

	public function get_patient_details_header($visit_id,$visit_invoice_id=0)
	{

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$patient_othernames = $patient['patient_othernames'];
		$patient_surname= $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		// $account_balance= $patient['account_balance'];
		$visit_type_name= $patient['visit_type_name'];
		$patient_type = $patient['patient_type'];
		$v_data['patient_id'] = $patient['patient_id'];
		$close_card = $patient['close_card'];
		$v_data['inpatient'] = $inpatient = $patient['inpatient'];
		

		// if($visit_invoice_id > 0)
		// {
			$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
			$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
			$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

			$balance = $total_invoice_amount - ($total_payments+$credit_note);

			$v_data['current_balance'] = $balance;
		// }
		// else
		// {
		// 	$balance = 0;
		// }

		 $authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

		$rs = $this->nurse_model->check_visit_type($visit_id);
		    if(count($rs)>0){
		      foreach ($rs as $rs1) {
		        # code...
		          $visit_t = $rs1->visit_type;
		          $patient_id = $rs1->patient_id;
		          $close_card = $rs1->close_card;

		      }
		}


		

		// echo $visit_id;


		if($inpatient == 1)
		{
			// $visit_discharge = '<a target="_blank" onclick="close_visit('.$visit_id.')" class="btn btn-sm btn-danger pull-right" style="margin-top:-25px;" ><i class="fa fa-folder"></i> Discharge Patient</a>';

			$visit_discharge = ' <a href="'.site_url().'print-discharge-summary/'.$visit_id.'"  target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px; margin-right:2px;" ><i class="fa fa-print"></i> Discharge Notes</a>
								<a class="btn btn-sm btn-danger pull-right" style="margin-top:-25px;" data-toggle="modal" data-target="#end_visit_date" ><i class="fa fa-times"></i> Discharge</a>   ';
			$banner = 'success';
		}
		else if($inpatient == 0)
		{
			$visit_discharge = '<a class="btn btn-sm btn-danger pull-right"  onclick="close_visit('.$visit_id.')"  style="margin-top:-25px;" ><i class="fa fa-folder"></i> End Visit</a>';
			$banner = 'success';
		}


		if($patient_type == 0 AND $close_card <= 1)
		{
			// <a class="btn btn-sm btn-success pull-left" style="margin-top:-25px;margin-right:2px;" onclick="add_new_invoice('.$visit_id.','.$patient_id.')"><i class="fa fa-plus"></i> Add Invoice</a>
			$checked = '
						<a class="btn btn-sm btn-info pull-left" style="margin-top:-25px;margin-right:2px;" onclick="open_billing_details('.$visit_id.','.$visit_invoice_id.','.$patient_id.')" ><i class="fa fa-pencil"></i> Edit Billing Details</a>
					<a href="'.site_url().'accounts/print_inpatient_invoice_new/'.$visit_id.'/1" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-top:-25px;margin-right:2px;" ><i class="fa fa-print"></i> All Invoice</a>


					';
		}
		else
		{
			$checked = '';
		}

		$title = '<h2 class="panel-title panel-'.$banner.'"><strong>Visit: </strong>'.$visit_type_name.'.<strong> Current Bill: </strong> Kes '.number_format($balance,2).'</h2>
				<div class="pull-right">
				    '.$checked.'
					'.$visit_discharge.'
					<a href="'.site_url().'accounts/print_inpatient_invoice_new/'.$visit_id.'/'.$visit_invoice_id.'" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px; margin-right:2px;" ><i class="fa fa-print"></i> Current Bill </a>
				</div>';

		echo $title;
	}
	public function charge_sheet($visit_id, $close_page = NULL)
	{
		$v_data = array('visit_id'=>$visit_id);

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inatient'] = $patient['inatient'];

		$v_data['doctor'] = $this->reception_model->get_doctor();


		$primary_key = $patient['patient_id'];


		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}

		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND (service.service_name = "Others" OR service.service_name = "Procedures" OR service.service_name = "Consultation") AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id ='.$visit_t;

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;

			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;

		$table= 'visit_charge, service_charge, service';
		$where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND (service.service_name = "Others" OR service.service_name = "Procedures" OR service.service_name = "Consultation" OR service.service_name = "Laboratory") AND visit_charge.charged = 1';

		$config["per_page"] = $v_data['per_page'] = $per_page = 10;


		$v_data['visit_id'] = $visit_id;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);
		$v_data['charge_sheet_query'] = $query;





		$v_data['close_page'] = $close_page;
		$data['content'] = $this->load->view('charge_sheet', $v_data, true);

		$data['title'] = 'Payments';
		$data['sidebar'] = 'accounts_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function make_payments($visit_id, $visit_invoice_id)
	{

		$this->form_validation->set_rules('type_payment', 'Type of payment', 'trim|xss_clean');
		$payment_method = $this->input->post('payment_method');
		// normal or credit note or debit note
		$type_payment = 1;//$this->input->post('type_payment');


		// if($payment_method == 0)
		// {
		// 	$response['result'] ='fail';
		// 	$response['message'] ='Please select the type of payment';
		// }
		// else
		// {	// Normal
			if($type_payment == 1)
			{
				$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Payment Service', 'trim|required|xss_clean');
				$this->form_validation->set_rules('service_id', 'Service', 'xss_clean');
				if(!empty($payment_method))
				{
					if($payment_method == 1)
					{
						// check for cheque number if inserted
						$this->form_validation->set_rules('cheque_number', 'Cheque Number', 'trim|required|xss_clean');
					}
					else if($payment_method == 6)
					{
						// check for insuarance number if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Credit Card Detail', 'trim|required|xss_clean');
					}
					else if($payment_method == 5)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('mpesa_code', 'Amount', 'trim|required|xss_clean');
					}
					else if($payment_method == 7)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('deposit_detail', 'Bank Deposit', 'trim|required|xss_clean');
					}
					else if($payment_method == 8)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Debit Card', 'trim|required|xss_clean');
					}
				}
			}
			else if($type_payment == 2)
			{
				$this->form_validation->set_rules('waiver_amount', 'Amount', 'trim|required|xss_clean');
				// debit note
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			else if($type_payment == 3)
			{
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				if($type_payment == 2 OR $type_payment == 3)
				{
					$checked = $this->session->userdata('authorize_invoice_changes');


				}
				else
				{
					$checked = TRUE;
				}

				if($checked)
				{


					if($this->accounts_model->receipt_payment($visit_id,$visit_invoice_id))
					{
						$response['result'] ='success';
						$response['message'] ='You have successfully receipted the payment';
						$this->session->set_userdata('success_message', 'You have successfully receipted the payment');
					}
					else
					{
						$response['result'] ='fail';
						$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
						$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');
					}
				}
				else
				{
					$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
					$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');

				}


			}
			else
			{
				$response['result'] ='fail';
				$response['message'] =validation_errors();
			}
		// }
		echo json_encode($response);
	}

	public function receipt_payment_old($visit_id, $close_page = NULL)
	{
		$v_data = array('visit_id'=>$visit_id);

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inatient'] = $patient['inatient'];

		$v_data['doctor'] = $this->reception_model->get_doctor();


		$primary_key = $patient['patient_id'];


		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}

		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;
			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;

		$table= 'visit_charge, service_charge, service';
		$where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id';

		$config["per_page"] = $v_data['per_page'] = $per_page = 10;


		$v_data['visit_id'] = $visit_id;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);
		$v_data['charge_sheet_query'] = $query;

		$v_data['close_page'] = $close_page;
		$data['content'] = $this->load->view('receipt_payment', $v_data, true);

		$data['title'] = 'Payments';
		$data['sidebar'] = 'accounts_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}


	public function receipt_payment($patient_id,$close_page = NULL,$visit_id=NULL )
	{
		$v_data = array('patient_id'=>$patient_id,'visit_id'=>$visit_id);

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// $v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
		$patient = $this->reception_model->patient_names2($patient_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inatient'] = $patient['inatient'];
		// var_dump($v_data);die();



		// check the visit
		if(!empty($close_page) and $visit_id > 0)
		{
			$this->reception_model->update_visit_picked($visit_id,7,$close_page);

		}
		
		$v_data['doctor'] = $this->reception_model->get_doctor();


		$primary_key = $patient['patient_id'];


		// $rs = $this->nurse_model->check_visit_type($visit_id);
		// if(count($rs)>0){
		//   foreach ($rs as $rs1) {
		//     # code...
		//       $visit_t = $rs1->visit_type;
		//   }
		// }

		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;


		$this->accounts_model->update_visit_totals($patient_id);
		$this->db->where('patient_id',$patient_id);
		$query_new = $this->db->get('visit_invoice');

		if($query_new->num_rows() > 0)
		{
			foreach ($query_new->result() as $key => $value) {
				// code...
				$visit_invoice_id = $value->visit_invoice_id;

				$this->accounts_model->update_invoice_totals($visit_invoice_id);
			}
		}

		$v_data['close_page'] = $close_page;
		$data['content'] = $this->load->view('receipt_payment', $v_data, true);

		$data['title'] = 'Payments';
		$data['sidebar'] = 'accounts_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function add_billing($visit_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('billing_method_id', 'Billing Method', 'required|numeric');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->accounts_model->add_billing($visit_id))
			{
				$this->session->set_userdata('success_message', 'Billing method successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message","Unable to add billing method. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Fill in the fields");
		}

		redirect('accounts/payments/'.$visit_id.'/'.$close_page);
	}
	public function add_service_item()
	{

		$this->form_validation->set_rules('parent_service_id', 'Service Name', 'required|numeric');
		$this->form_validation->set_rules('service_charge_item', 'Charge Name', 'required');
		$this->form_validation->set_rules('service_amount', 'Charge Name', 'required|numeric');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->accounts_model->add_service_item())
			{
				$this->session->set_userdata('success_message', 'Service charge successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message","Unable to add service charge. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Fill in the fields");
		}

		// redirect('accounts/payments/'.$visit_id);

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}
	public function print_invoice($visit_id)
	{
		$this->accounts_model->receipt($visit_id);
	}

	public function print_invoice_old($visit_id)
	{
		$this->accounts_model->receipt($visit_id, TRUE);
	}

	public function print_invoice_new($visit_invoice_id,$visit_id,$page_item = NULL)
	{
		$data = array('visit_id'=>$visit_id);

		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$data['contacts'] = $this->site_model->get_contacts($branch_id);
		$data['page_item'] = $page_item;
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$data['visit_invoice_id'] = $visit_invoice_id;

		// var_dump($data);die();
		$this->load->view('invoice', $data);
	}

	public function print_inpatient_invoice_new($visit_id,$page_item = NULL)
	{
		$data = array('visit_id'=>$visit_id,'visit_invoice_id'=>$page_item);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = $page_item;
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$this->load->view('inpatient_invoice', $data);
		
	}
	public function print_self_invoice($visit_id,$page_item = NULL)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = $page_item;
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$this->load->view('self_invoice', $data);

	}
	public function print_receipt_new($visit_id)
	{
		$data = array('visit_id'=>$visit_id);
		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$data['contacts'] = $this->site_model->get_contacts($branch_id);

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$this->load->view('receipt', $data);
	}


	public function print_receipt_all($visit_id,$visit_invoice_id)
	{
		$data = array('visit_id'=>$visit_id);
		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$data['contacts'] = $this->site_model->get_contacts($branch_id);

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$data['visit_id'] = $visit_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		// var_dump($visit_id);die();
		$this->load->view('receipt_inpatient', $data);
	}
	public function print_single_receipt($payment_id,$visit_id,$visit_invoice_id)
	{
		$data = array('payment_id' => $payment_id,'visit_id'=>$visit_id);
		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$data['contacts'] = $this->site_model->get_contacts($branch_id);
		$data['receipt_payment_id'] = $payment_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$patient = $this->reception_model->patient_names3($payment_id);
		$data['patient'] = $patient;
		$this->load->view('single_receipt', $data);
	}
	public function bulk_close_visits($page)
	{
		$total_visits = sizeof($_POST['visit']);

		//check if any checkboxes have been ticked
		if($total_visits > 0)
		{
			for($r = 0; $r < $total_visits; $r++)
			{
				$visit = $_POST['visit'];
				$visit_id = $visit[$r];
				//check if card is held
				if($this->reception_model->is_card_held($visit_id))
				{
				}

				else
				{
					if($this->accounts_model->end_visit($visit_id))
					{
						$this->session->set_userdata('success_message', 'Visits ended successfully');
					}

					else
					{
						$this->session->set_userdata('error_message', 'Unable to end visits');
					}
				}
			}
		}

		else
		{
			$this->session->set_userdata('error_message', 'Please select visits to terminate first');
		}

		redirect('accounts/accounts_unclosed_queue/'.$page);
	}

	public function close_visit($visit_id)
	{

		$payments_value = $this->accounts_model->total_payments($visit_id);

		$invoice_total = $this->accounts_model->total_invoice($visit_id);

		$balance = $this->accounts_model->balance($payments_value,$invoice_total);

		if($balance > 0)
		{


			if($this->accounts_model->end_visit_with_status($visit_id,2))
			{


			}

			else
			{
				$this->session->set_userdata('error_message', 'Unable to end visits');


			}
			$response['message'] ="visit has been ended successfully";
		}
		else
		{

			if($this->accounts_model->end_visit_with_status($visit_id,1))
			{
				$response['message'] ="You have successfully ended the visit";

			}

			else
			{
				$response['message'] ="Sorry could not end visit at this time. Please try again";


			}
		}
		echo json_encode($response);
	}

	public function send_message($visit_id)
	{

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inatient'] = $patient['inatient'];
		$v_data['patient_phone1'] = $patient_phone = $patient['patient_phone_number'];

		$explode = explode(" ", $v_data['patient_surname']);
		$name = $explode[0];

		
		$message  = "Thank you ".$name." for visiting Grace Memorial Hospital.\nWe were pleased to serve you and we hope you can share how your experience was with us on https://shorturl.at/abgGP";



		$message_data = array(
						"phone_number" => $patient_phone,
						"entryid" => $v_data['patient_id'],
						"message" => $message,
						"message_batch_id"=>0,
						'message_status' => 0
					);
		$this->db->insert('messages', $message_data);
		$message_id = $this->db->insert_id();
		// $patient_phone = 704808007;
		$patient_phone = str_replace(' ', '', $patient_phone);
		// var_dump($patient_phone); die();
		if(!empty($patient_phone))
		{
			$response = $this->messaging_model->sms($patient_phone,$message);
			// var_dump($response); die();
			if($response == "Success" OR $response == "success")
			{

				$service_charge_update = array('message_status' => 1,'delivery_message'=>'Success','sms_cost'=>3,'message_type'=>'Thank You Note');
				$this->db->where('message_id',$message_id);
				$this->db->update('messages', $service_charge_update);

			}
			else
			{
				$service_charge_update = array('message_status' => 0,'delivery_message'=>'Success','sms_cost'=>0,'message_type'=>'Thank You Note');
				$this->db->where('message_id',$message_id);
				$this->db->update('messages', $service_charge_update);


			}
		}
		else
		{
			$response['status'] = 1;
			$response['message'] = "Sorry could not send message";
		}

		 echo json_encode($response);
	}

	public function discharge_patient($visit_id,$visit_invoice_id)
	{
		$visit_date = $this->input->post('visit_date_charged');

		$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
		$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
		$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

		$balance = $total_invoice_amount - ($total_payments+$credit_note);


		if($balance > 0)
		{


			if($this->accounts_model->discharge_visit_with_status($visit_id,2,$visit_date,$visit_invoice_id))
			{


			}

			else
			{
				$this->session->set_userdata('error_message', 'Unable to end visits');


			}
			$response['message'] ="visit has been ended successfully";
		}
		else
		{

			if($this->accounts_model->discharge_visit_with_status($visit_id,1,$visit_date,$visit_invoice_id))
			{
				$response['message'] ="You have successfully ended the visit";

			}

			else
			{
				$response['message'] ="Sorry could not end visit at this time. Please try again";


			}
		}
		 echo json_encode($response);
	}

	public function get_change($visit_id,$visit_invoice_id)
	{

		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean|numeric');

		if($this->form_validation->run())
		{
			$amount_paid = $this->input->post('amount_paid');


			$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
			$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
			$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

			$balance = $total_invoice_amount - ($total_payments+$credit_note);


			if($balance < $amount_paid)
			{
				$change = $amount_paid - $balance;
			}
			else
			{
				$change = 0;
			}
		}
		else
		{
			$change = 0;
		}

		$response['change'] = $change;

		echo json_encode($response);
	}

	public function get_visit_balance($visit_id,$visit_invoice_id)
	{

		$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
		$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
		$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

		$balance = $total_invoice_amount - ($total_payments+$credit_note);

			

		$response['balance'] = $balance;

		echo json_encode($response);
	}

	public function send_to_department($visit_id, $department_id)
	{
		
		$this->db->where('visit_id', $visit_id);
		$query = $this->db->get('visit');
		$row = $query->row();
		$visit_type = $row->visit_type;
		$patient_id = $row->patient_id;
		$visit_department_id = $this->input->post('visit_department_id');

	
		if(!empty($visit_department_id))
		{
			$this->reception_model->update_visit_left($visit_id,$visit_department_id);
		}

		$this->session->set_userdata('success_message', 'Patient has been sent');
		redirect('queues/outpatient-queue');
		
	}


	public function send_to_doctor_department($visit_id, $department_id)
	{
		
		$this->db->where('visit_id', $visit_id);
		$query = $this->db->get('visit');
		$row = $query->row();
		$visit_type = $row->visit_type;
		$patient_id = $row->patient_id;
		$visit_department_id = $this->input->post('visit_department_id');

		

		if(!empty($visit_department_id))
		{
			$this->reception_model->update_visit_left($visit_id,$visit_department_id);
		}

		$this->reception_model->set_visit_department($visit_id, $department_id);

		$this->session->set_userdata('success_message', 'Patient has been sent');
		redirect('queues/outpatient-queue');
		
	}

	public function cancel_payment($payment_id, $visit_id=null)
	{
		// $this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('visit_invoice_id', 'Action', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('patient_id', 'Action', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('cancel_description', 'Action', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('refund_date', 'Action', 'trim|required|xss_clean');

		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		

		

		$visit_id = $this->input->post('visit_id');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// var_dump($visit_id);die();
			// end of checker function
			if($this->accounts_model->cancel_payment($payment_id))
			{
				// $this->session->set_userdata("success_message", "Payment action saved successfully");
				$response['message'] = 'success';
			}
			else
			{
				// $this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
				$response['message'] = 'fail';
			}
		}
		else
		{
			// $this->session->set_userdata("error_message", validation_errors());
			$response['message'] = 'fail';
		}
		echo json_encode($response);
		// redirect('receipt-payment/'.$visit_id.'/1');
	}
	public function view_patient_bill($visit_id,$visit_invoice_id,$page=NULL)
	{
		// $data = array('visit_id'=>$visit_id);

		if($page == NULL)
		{
			$page = 0;
		}


		$symptoms_search = $this->input->post('query');
		$query = null;
		if(!empty($symptoms_search))
		{
			// AND service.service_name <> "Pharmacy" 
			$lab_test_table= 'visit_charge, service_charge, service';
			$lab_test_where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_name <> "Others" AND visit_charge.charged = 1';
			$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$symptoms_search.'%\' OR service_charge.service_charge_code LIKE \'%'.$symptoms_search.'%\')';
			
			if(!empty($visit_invoice_id))
			{
				$lab_test_where .= ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
			}
			$this->db->select('visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*,personnel.personnel_fname,personnel.personnel_onames');
				$this->db->where($lab_test_where);
				$this->db->join('personnel','personnel.personnel_id = visit_charge.created_by','LEFT');
			$this->db->limit(100);
			$query = $this->db->get($lab_test_table);

			if($query->num_rows() == 0)
			{
				// AND service.service_name <> "Pharmacy"
				$lab_test_table= 'visit_charge, service_charge, service';
				$lab_test_where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_name <> "Others" AND visit_charge.charged = 1';
				$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$symptoms_search.'%\' OR service_charge.service_charge_code LIKE \'%'.$symptoms_search.'%\')';
				if(!empty($visit_invoice_id))
				{
					$lab_test_where .= ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
				}
				$this->db->select('visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*,personnel.personnel_fname,personnel.personnel_onames');
				$this->db->where($lab_test_where);
				$this->db->join('personnel','personnel.personnel_id = visit_charge.created_by','LEFT');
				$this->db->limit(100);
				$query = $this->db->get($lab_test_table);
			}

		}
		else
		{
			$lab_test_table= 'visit_charge, service_charge, service';
			$lab_test_where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_name <> "Others" AND visit_charge.charged = 1';
			if(!empty($visit_invoice_id))
			{
				$lab_test_where .= ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
			}
			$this->db->select('visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*,personnel.personnel_fname,personnel.personnel_onames');
			$this->db->where($lab_test_where);
			$this->db->join('personnel','personnel.personnel_id = visit_charge.created_by','LEFT');
			$this->db->limit(100);
			$query = $this->db->get($lab_test_table);
		}
		// $table= 'visit_charge, service_charge, service';
		// $where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_name <> "Others" AND visit_charge.charged = 1';


		$config["per_page"] = $v_data['per_page'] = $per_page = 1000000000;
		// if($page==0)
		// {

		// 	$counted = 0;
		// }
		// else if($page > 0)
		// {

		// 	$counted = $per_page*$page;
		// }

		$v_data['page'] = $page;
		// $v_data['visit_id'] = $visit_id;
		$page = $counted;
		// $v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		// $query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);



		$v_data['invoice_items'] = $query;

		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		      $patient_id = $rs1->patient_id;
		  }
		}

		$patient_details = $this->reception_model->patient_names2($visit_id);
		$close_card = $patient_details['close_card'];
		$v_data['close_card'] = $close_card;


		$procedures = '';
		$v_data['services_list'] = $procedures;
		$v_data['visit_invoice_id'] = $visit_invoice_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['visit_id'] = $visit_id;



		// var_dump($visit_id);die();






		// payments


		// old
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('view_bill_inpatient',$v_data);

		

	}
	public function view_patient_bill_old($visit_id,$visit_invoice_id,$page=NULL)
	{
		// $data = array('visit_id'=>$visit_id);

		if($page == NULL)
		{
			$page = 0;
		}
		$table= 'visit_charge, service_charge, service';
		$where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_name <> "Others" AND visit_charge.charged = 1 AND visit_charge.visit_invoice_id = '.$visit_invoice_id;

		$config["per_page"] = $v_data['per_page'] = $per_page = 1000000000;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}

		$v_data['page'] = $page;
		$v_data['visit_id'] = $visit_id;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);



		$v_data['invoice_items'] = $query;

		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}

		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND service.service_name <> "Pharmarcy" AND service.service_name <> "Laboratory" AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = '.$visit_t;

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;
		$v_data['visit_invoice_id'] = $visit_invoice_id;








		// payments


		// old
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('view_bill_inpatient',$v_data);

	}

	public function get_patient_receipt($visit_id,$page=NULL)
	{
		if($page == NULL)
		{
			$page = 0;
		}
		// $visit_id = 23;
		$table= 'payments,payment_item, payment_method';
		$where="payments.cancel = 0 AND payments.payment_id = payment_item.payment_id AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =". $visit_id;
		$config["per_page"] = $v_data['per_page'] = $per_page = 1000000;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}

		$v_data['page'] = $page;
		$v_data['visit_id'] = $visit_id;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);

		$query = $this->accounts_model->get_all_visits_payments_items($table, $where, $config["per_page"], $page);



		$v_data['receipts_items'] = $query;

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('payments_made',$v_data);
	}

	public function get_services_billed($visit_id)
	{
		$v_data['visit_id'] = $visit_id;


		$this->load->view('billed_services',$v_data);
	}
	public function update_service_total($procedure_id,$units,$amount,$visit_id){

		// $status = $this->accounts_model->check_if_visit_active($visit_id);
		// if($status)
		// {
			$notes = $this->input->post('notes');
			$patient_id = $this->input->post('patient_id');
			$visit_charge_comment = $this->input->post('visit_comment');
			$visit_data = array('visit_charge_units'=>$units,'visit_charge_notes'=>$notes,'visit_charge_amount'=>$amount, 'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"),'time'=>date("H:i:s"),'visit_charge_comment'=>$visit_charge_comment,'patient_id'=>$patient_id);
			$this->db->where(array("visit_charge_id"=>$procedure_id));
			$this->db->update('visit_charge', $visit_data);

			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";
		// }
		// else
		// {
		// 	$response['status'] = "success";
		// 	$response['message'] = "Sorry the visit has been ended";
		// }
		echo json_encode($response);
	}


	public function update_quotation_total($procedure_id,$units,$amount,$visit_id){

		$status = $this->accounts_model->check_if_visit_active($visit_id);
		if($status)
		{
			$notes = $this->input->post('notes');
			$visit_data = array('visit_charge_units'=>$units,'visit_charge_notes'=>$notes,'visit_charge_amount'=>$amount, 'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"));
			$this->db->where(array("visit_charge_id"=>$procedure_id));
			$this->db->update('visit_quotation', $visit_data);

			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";
		}
		else
		{
			$response['status'] = "success";
			$response['message'] = "Sorry the visit has been ended";
		}
		echo json_encode($response);
	}
	public function delete_service_billed($procedure_id,$visit_id)
	{

		$status = $this->accounts_model->check_if_visit_active($visit_id);
		if($status)
		{

			$visit_data = array('visit_charge_delete'=>1,'deleted_by'=>$this->session->userdata("personnel_id"),'deleted_on'=>date("Y-m-d"),'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"));

			$this->db->where(array("visit_charge_id"=>$procedure_id));
			$this->db->update('visit_charge', $visit_data);
			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";
		}
		else
		{
			$response['status'] = "success";
			$response['message'] = "Sorry the visit has been ended";
		}
		echo json_encode($response);
	}

	public function add_patient_bill($visit_id,$visit_invoice_id)
	{
		
			$service_charge_id = $this->input->post('service_charge_id');
			$provider_id = $this->input->post('provider_id');
			$visit_date = $this->input->post('visit_date');
			$amount = $this->accounts_model->get_service_charge_detail($service_charge_id);

			$rs = $this->nurse_model->check_visit_type($visit_id);
			if(count($rs)>0){
			  foreach ($rs as $rs1) {
			    # code...
			      $visit_t = $rs1->visit_type;
			      $branch_id = $rs1->branch_id;
			  }
			}
			if($branch_id == 2)
			{
				$store_id = 6;
			}
			else
			{
				$store_id = 7;
			}

			// add the items 


			$this->db->where('open_status = 0 AND visit_id = '.$visit_id);
			$this->db->limit(1);
			$query = $this->db->get('visit_invoice');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...
					$visit_invoice_id = $value->visit_invoice_id;
				}
			}

			if(empty($visit_invoice_id))
			{
				$visit_invoice_id = 0;
			}

			$visit_data = array('visit_charge_units'=>1,'visit_id'=>$visit_id,'visit_charge_amount'=>$amount,'visit_charge_qty'=>1,'service_charge_id'=>$service_charge_id,'charged'=>1, 'created_by'=>$this->session->userdata("personnel_id"),'provider_id'=>$provider_id,'date'=>$visit_date,'time'=>date('H:i:s'),'personnel_id'=>$procedure_id,'store_id'=>$store_id,'visit_invoice_id'=>$visit_invoice_id);

			if($this->db->insert('visit_charge', $visit_data))
			{

				$response['status'] = "success";
				$response['message'] = 'Charge successfully added to visit';
			}
			else
			{
				$response['status'] = "fail";
				$response['message'] = 'Sorry this visit charge could not be added';
			}


	


		echo json_encode($response);
	}

	public function add_accounts_personnel()
	{
		//form validation rules
		$this->form_validation->set_rules('personnel_onames', 'Other Names', 'xss_clean');
		$this->form_validation->set_rules('personnel_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('personnel_phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('personnel_address', 'Address', 'xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$personnel_id = $this->accounts_model->add_personnel();
			if($personnel_id > 0)
			{
				$this->session->set_userdata("success_message", "Personnel added successfully");

			}

			else
			{
				$this->session->set_userdata("error_message","Could not add personnel. Please try again ".$personnel_id);
			}
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}


	public function change_patient_visit($visit_id)
	{
		$this->form_validation->set_rules('visit_type_id', 'Visit Type', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$visit_type_id = $this->input->post('visit_type_id');


			$array = array('visit_type'=>$visit_type_id,"branch_id"=>3);
			$this->db->where('visit_id',$visit_id);
			if($this->db->update('visit',$array))
			{

				$visit_invoice_id = $this->input->post('visit_invoice_id');
				$member_number = $this->input->post('member_number');
				$scheme_name = $this->input->post('scheme_name');
				$invoice_date = $this->input->post('invoice_date');
				$rebate_status = $this->input->post('rebate_status');
				$visit_id = $this->input->post('billing_visit_id');
				$patient_id = $this->input->post('billing_patient_id');
				$visit_type_id = $this->input->post('visit_type_id');

				// var_dump($rebate_status);die();
				// if($rebate_status == 1)
				// {
					// calculate days and update the invoice accordingly

				$this->accounts_model->update_rebate_items($visit_invoice_id,$patient_id,$visit_id,$rebate_status);

				// }

				$data_update['member_number'] = $member_number;
				$data_update['scheme_name'] = $scheme_name;
				$data_update['created'] = $invoice_date;
				$data_update['rebate_status'] = $rebate_status;
				$data_update['bill_to'] = $visit_type_id;
				$data_update['branch_id'] = 3;


				$this->db->where('visit_invoice_id',$visit_invoice_id);
				$this->db->update('visit_invoice',$data_update);


				// var_dump($visit_invoice_id);die();

				$this->session->set_userdata("success_message", "You have successfully changed the patient type");
			}

			else
			{
				$this->session->set_userdata("error_message","Please try again ");
			}
		}else
		{
			$this->session->set_userdata("error_message","Please try again ");
		}
		// $redirect_url = $this->input->post('redirect_url');
		// redirect($redirect_url);

	}

	public function patient_visits()
	{

		$delete = 0;
		$segment = 3;

		$patient_search = $this->session->userdata('visit_accounts_search');
		$where = 'patient_delete = 0 AND patients.patient_type = 0 AND patients.patient_id <> 293 AND patients.patient_id IN (SELECT patient_id FROM visit WHERE visit_delete =0)';
		if(!empty($patient_search))
		{
			$where .= $patient_search;
		}

		else
		{

		}

		$table = 'patients';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'cash-office/patient-visits';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;


		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_patients_accounts($table, $where, $config["per_page"], $page);


		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['delete'] = 0;
		$v_data['title'] = 'Patients Visits';
		$v_data['branches'] = $this->reception_model->get_branches();
		$data['content'] = $this->load->view('all_patients_list', $v_data, true);

		$data['sidebar'] = 'reception_sidebar';

		$this->load->view('admin/templates/general_page', $data);
		// end of it

	}


	// function get_visit_balance($visit_id)
	// {
	// 	$payments_value = $this->accounts_model->total_payments($visit_id);
	// 	$invoice_total = $this->accounts_model->total_invoice($visit_id);
	// 	$balance = $this->accounts_model->balance($payments_value,$invoice_total);
	// 	$response['balance'] = $balance;

	// 	echo json_encode($response);
	// }

	public function send_todays_report()
	{
		$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));
		$branch = $this->config->item('branch_name');
		$message['subject'] =  $branch_name.' '.$visit_date.' report';

		$where = $where1 = $where6 = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_date = "'.date('Y-m-d').'"';
		$payments_where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 ';
		$table = 'visit, patients';



		//cash payments
		$where2 = $payments_where.' AND payments.payment_method_id = 2 AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.payment_created = "'.date('Y-m-d').'"';
		$total_cash_collection = $this->reports_model->get_total_cash_collection($where2, $table);

		// mpesa
		$where2 = $payments_where.' AND payments.payment_method_id = 5 AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.payment_created = "'.date('Y-m-d').'"';
		$total_mpesa_collection = $this->reports_model->get_total_cash_collection($where2, $table);

		$where2 = $payments_where.' AND (payments.payment_method_id = 1 OR  payments.payment_method_id = 6 OR  payments.payment_method_id = 7 OR  payments.payment_method_id = 8)  AND payments.payment_type = 1 AND payments.cancel = 0 AND payments.payment_created = "'.date('Y-m-d').'"';
		$total_other_collection = $this->reports_model->get_total_cash_collection($where2, $table);


		$where4 = 'payments.payment_method_id = payment_method.payment_method_id AND payments.visit_id = visit.visit_id  AND visit.visit_delete = 0  AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND payments.cancel = 0 AND payments.payment_type = 3 AND payments.payment_created = "'.date('Y-m-d').'"';
		$total_waiver = $this->reports_model->get_total_cash_collection($where4, 'payments, visit, patients, visit_type, payment_method', 'cash');


		 // var_dump($total_other_collection+$total_mpesa_collection+$total_cash_collection); die();

		//count outpatient visits
		$where2 = $where1.' AND visit.inpatient = 0';

		(int)$outpatients = $this->reception_model->count_items($table, $where2);
		// var_dump($outpatients); die();
		//count inpatient visits
		$where2 = $where6.' AND visit.inpatient = 1';

		(int)$inpatients = $this->reception_model->count_items($table, $where2);


		$table1 = 'petty_cash,account';
		$where1 = 'petty_cash.account_id = account.account_id AND (account.account_name = "Cash Box" OR account.account_name = "Cash Collection") AND petty_cash.petty_cash_delete = 0';


		$where1 .=' AND petty_cash.petty_cash_date = "'.date('Y-m-d').'"';
		$total_transfers = $this->reports_model->get_total_transfers($where1,$table1);

		$total_patients = $outpatients + $inpatients;

		$message['text'] = ' <p>Good evening to you,<br>
								Herein is a report of todays transactions. This is sent at '.date('H:i:s A').'
								</p>
							  <table>
							  		<thead>
							  			<tr>
							  				<th width="50%"></th>
							  				<th width="50%"></th>
							  			</tr>
							  		</thead>
							  		</tbody>
							      	<tr>
							      		<td>Out-Patients </td><td>  '.$outpatients.'</td>
							      	</tr>
							      	<tr>
							      		<td>Inpatients </td><td> '.$inpatients.'</td>
							      	</tr>
							      	<tr>
							      		<td><strong>Total patients</strong> </td><td><strong> '.$total_patients.' </strong></td>
							      	</tr>
							      	<tr>
							      		<td>Total Cash Collection </td><td>KES. '.number_format($total_cash_collection,2).'</td>
							      	</tr>
							      	<tr>
							      		<td>Total M-pesa Collection </td><td> KES. '.number_format($total_mpesa_collection,2).'</td>
							      	</tr>
							      	<tr>
							      		<td>Total Other Collections </td><td> KES. '.number_format($total_other_collection,2).'</td>
							      	</tr>

							      	<tr>
							      		<td>Total Cash - Petty cash transer </td><td> ( KES. '.number_format($total_transfers,2).' )</td>
							      	</tr>
							      	<tr>
							      		<td>Total Waivers </td><td> KES. '.number_format($total_waiver,2).'</td>
							      	</tr>
							      	<tr>
							      		<td><strong>Total Revenue Collected</strong> </td><td><strong> KES. '.number_format(($total_mpesa_collection + $total_cash_collection + $total_other_collection) - $total_transfers,2).' </strong></td>
							      	</tr>
							      	</tbody>

							  </table>
							';
		$contacts = $this->site_model->get_contacts();
		$sender_email =$this->config->item('sender_email');//$contacts['email'];
		$shopping = "";
		$from = $sender_email;

		$button = '';
		$sender['email']= $sender_email;
		$sender['name'] = $contacts['company_name'];
		$receiver['name'] = $subject;
		// $payslip = $title;

		$sender_email = $sender_email;
		$tenant_email .= $this->config->item('recepients_email');;
		// var_dump($tenant_email); die();
		$email_array = explode('/', $tenant_email);
		$total_rows_email = count($email_array);

		for($x = 0; $x < $total_rows_email; $x++)
		{
			$receiver['email'] = $email_tenant = $email_array[$x];

			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip=NULL);


		}
	}


	public function make_payment_charge($visit_id, $close_page = NULL)
	{

		$this->form_validation->set_rules('type_payment', 'Type of payment', 'trim|required|xss_clean');
		$payment_method = $this->input->post('payment_method');
		// normal or credit note or debit note
		$type_payment = $this->input->post('type_payment');


			if($type_payment == 1)
			{
				$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_service_id', 'Payment Service', 'trim|xss_clean');
				$this->form_validation->set_rules('service_id', 'Service', 'xss_clean');
				if(!empty($payment_method))
				{
					if($payment_method == 1)
					{
						// check for cheque number if inserted
						$this->form_validation->set_rules('cheque_number', 'Cheque Number', 'trim|required|xss_clean');
					}
					else if($payment_method == 6)
					{
						// check for insuarance number if inserted
						$this->form_validation->set_rules('insuarance_number', 'Credit Card Detail', 'trim|required|xss_clean');
					}
					else if($payment_method == 5)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('mpesa_code', 'Amount', 'trim|xss_clean');
					}
					else if($payment_method == 7)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('deposit_detail', 'Bank Deposit', 'trim|xss_clean');
					}
					else if($payment_method == 8)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Debit Card', 'trim|required|xss_clean');
					}
				}
			}
			else if($type_payment == 2)
			{
				$this->form_validation->set_rules('waiver_amount', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('reason', 'Reason', 'trim|required|xss_clean');
				// var_dump($_POST); die();
				// debit note
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			else if($type_payment == 3)
			{
				$this->form_validation->set_rules('waiver_amount', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('reason', 'Reason', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				// var_dump($_POST); die();

				if($type_payment == 2 OR $type_payment == 3)
				{
					$checked = $this->session->userdata('authorize_invoice_changes');


				}
				else
				{
					$checked = TRUE;
				}

				if($checked)
				{


					if($this->accounts_model->receipt_payment($visit_id))
					{
						$response['result'] ='success';
						$response['message'] ='You have successfully receipted the payment';
						$this->session->set_userdata('success_message', 'You have successfully receipted the payment');
					}
					else
					{
						$response['result'] ='fail';
						$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
						$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');
					}
				}
				else
				{
					$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
					$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');

				}


			}
			else
			{
				$response['result'] ='fail';
				$response['message'] =validation_errors();
				$this->session->set_userdata('error_message', $validation_errors());
			}

			redirect('receipt-payment/'.$visit_id.'/'.$close_page);

			// }

	}

	public function send_back_to_department($visit_id,$close_page)
	{
		$array_charge['closed'] = 1;
		$this->db->where('visit_id',$visit_id);
		$this->db->update('visit',$array_charge);

		if($close_page == 5)
		{
			$array['charged'] = 1;
			$this->db->where('visit_id',$visit_id);
			$this->db->update('visit_charge',$array);
		}

		if($this->reception_model->set_visit_department($visit_id, $close_page))
		{
			redirect('queues/walkins');
		}
		else
		{
			FALSE;
		}
	}


	public function bill_patient($visit_id,$module =null)
	{
		$status = $this->accounts_model->check_if_visit_active($visit_id);
		if($status)
		{
			$service_charge_id = $this->input->post('service_charge_id');
			$provider_id = $this->input->post('provider_id');
			$visit_date = $this->input->post('visit_date_date');
			$amount = $this->accounts_model->get_service_charge_detail($service_charge_id);
			$rs = $this->nurse_model->check_visit_type($visit_id);
			if(count($rs)>0){
			  foreach ($rs as $rs1) {
			    # code...
			      $visit_t = $rs1->visit_type;
			      $branch_id = $rs1->branch_id;
			  }
			}
			if($branch_id == 2)
			{
				$store_id = 6;
			}
			else
			{
				$store_id = 7;
			}
			$visit_data = array('visit_charge_units'=>1,'visit_id'=>$visit_id,'visit_charge_amount'=>$amount,'service_charge_id'=>$service_charge_id, 'created_by'=>$this->session->userdata("personnel_id"),'provider_id'=>$provider_id,'date'=>$visit_date,'time'=>date('H:i:s'),'personnel_id'=>$procedure_id,'charged'=>1,'store_id'=>$store_id);

			if($this->db->insert('visit_charge', $visit_data))
			{
				$this->session->set_userdata('success_message', 'You have successfully added to bill');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Sorry please try again');
			}

		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry visit has been ended');
		}

		if(empty($module))
		{
			$module = 0;
		}
		redirect('receipt-payment/'.$visit_id.'/'.$module);

	}


	public function add_payment_to_invoice($visit_id, $close_page = NULL)
	{

		$this->form_validation->set_rules('type_payment'.$visit_id, 'Type of payment', 'trim|required|xss_clean');
		$payment_method = $this->input->post('payment_method'.$visit_id);
		// normal or credit note or debit note
		$type_payment = $this->input->post('type_payment'.$visit_id);


			if($type_payment == 1)
			{
				$this->form_validation->set_rules('amount_paid'.$visit_id, 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_method'.$visit_id, 'Payment Method', 'trim|required|xss_clean');
				if(!empty($payment_method))
				{
					if($payment_method == 1)
					{
						// check for cheque number if inserted
						$this->form_validation->set_rules('cheque_number', 'Cheque Number', 'trim|required|xss_clean');
					}
					else if($payment_method == 6)
					{
						// check for insuarance number if inserted
						$this->form_validation->set_rules('insuarance_number', 'Credit Card Detail', 'trim|required|xss_clean');
					}
					else if($payment_method == 5)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('mpesa_code', 'Amount', 'trim|required|xss_clean');
					}
					else if($payment_method == 7)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('deposit_detail', 'Bank Deposit', 'trim|required|xss_clean');
					}
					else if($payment_method == 8)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Debit Card', 'trim|required|xss_clean');
					}
				}
			}
			else if($type_payment == 2)
			{
				$this->form_validation->set_rules('waiver_amount', 'Amount', 'trim|required|xss_clean');


				// debit note
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			else if($type_payment == 3)
			{
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				// var_dump($_POST); die();
				if($type_payment == 2 OR $type_payment == 3)
				{
					$checked = $this->session->userdata('authorize_invoice_changes');


				}
				else
				{
					$checked = TRUE;
				}

				if($checked)
				{


					if($this->accounts_model->receipt_payment($visit_id))
					{
						$response['result'] ='success';
						$response['message'] ='You have successfully receipted the payment';
						$this->session->set_userdata('success_message', 'You have successfully receipted the payment');
					}
					else
					{
						$response['result'] ='fail';
						$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
						$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');
					}
				}
				else
				{
					$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
					$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');

				}


			}
			else
			{
				$response['result'] ='fail';
				$response['message'] =validation_errors();
			}
			redirect('receipt-payment/'.$visit_id.'/'.$close_page);

	}

	public function delete_service_visit_billed($visit_charge_id,$visit_id,$module)
	{

		$status = $this->accounts_model->check_if_visit_active($visit_id);
		if($status)
		{

			$visit_data = array('visit_charge_delete'=>1,'deleted_by'=>$this->session->userdata("personnel_id"),'deleted_on'=>date("Y-m-d"),'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"));

			$this->db->where(array("visit_charge_id"=>$visit_charge_id));
			$this->db->update('visit_charge', $visit_data);
			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";
		}
		else
		{
			$response['status'] = "success";
			$response['message'] = "Sorry the visit has been ended";
		}
		redirect('receipt-payment/'.$visit_id.'/0');
	}

	public function end_visit($visit_id,$checked=null)
	{
		if($this->accounts_model->end_visit($visit_id))
		{	

			$this->send_message($visit_id);

			
			$this->session->set_userdata('success_message', 'Visits ended successfully');
			

			if($checked == 'a')
			{
				redirect('queues/walkins');
			}
			else
			{
				redirect('queues/outpatient-queue');
			}
		}

		else
		{
			$this->session->set_userdata('error_message', 'Unable to end visits');
			redirect('receipt-payment/'.$visit_id.'/0');
		}
	}


	public function send_notification($visit_id,$patient_id)
	{
		// $this->send_message($visit_id);

		redirect('receipt-payment/'.$patient_id.'/0/'.$visit_id);
	}

	public function remove_invoice($visit_id,$parent_invoice)
	{
		if($this->accounts_model->close_visit($visit_id))
		{

			$this->session->set_userdata('success_message', 'Visits deleted successfully');
			redirect('receipt-payment/'.$visit_id.'/1');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Unable to end visits');
			redirect('receipt-payment/'.$visit_id.'/1');
		}
	}
	public function remove_rejected_amount($visit_id)
	{

		$array['rejected_reason'] = '';
		$array['rejected_amount'] = NULL;
		$this->db->where('visit_id',$visit_id);
		$this->db->update('visit',$array);

		redirect('receipt-payment/'.$visit_id.'/1');

	}
	public function update_rejected_reasons($visit_id)
	{
		$this->form_validation->set_rules('rejected_amount', 'Amount', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rejected_reason', 'Reason', 'trim|required|xss_clean');
        $this->form_validation->set_rules('visit_type_id', 'Visit Type', 'trim|required|xss_clean');
        $redirect_url = $this->input->post('redirect_url');
        //if form conatins invalid data
        if ($this->form_validation->run())
        {
            // end of checker function
            if($this->accounts_model->update_rejected_reasons($visit_id))
            {
                $this->session->set_userdata("success_message", "Reject  saved successfully");
            }
            else
            {
                $this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
            }
        }
        else
        {
            $this->session->set_userdata("error_message", validation_errors());

        }
        redirect($redirect_url);
	}
	public function update_invoices_amounts()
	{
		$this->db->where('visit.parent_visit > 0 AND visit.visit_id = visit_bill.visit_id AND payment_updated = 0 ');
		$query = $this->db->get('visit,visit_bill');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_id = $value->visit_id;
				$parent_visit = $value->parent_visit;

				$update_data['visit_id'] = $parent_visit;
				$this->db->where('visit_id',$visit_id);
				$this->db->update('payments',$update_data);



				$update['payment_updated'] = 1;
				$this->db->where('visit_id',$visit_id);
				$this->db->update('visit_bill',$update);


			}
		}
	}


	public function update_doctor_amounts()
	{
		$this->db->where('visit.parent_visit > 0 AND visit.visit_id = visit_bill.visit_id AND payment_updated = 1 ');
		$query = $this->db->get('visit,visit_bill');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_id = $value->visit_id;
				$parent_visit = $value->parent_visit;

				$update_data['visit_id'] = $parent_visit;
				$this->db->where('visit_id',$visit_id);
				$this->db->update('doctor_invoice',$update_data);



				$update['payment_updated'] = 2;
				$this->db->where('visit_id',$visit_id);
				$this->db->update('visit_bill',$update);


			}
		}
	}
	public function view_procedure($visit_id){
		$data = array('visit_id'=>$visit_id);
		$this->load->view('view_bill',$data);
	}

	public function approved_invoice($visit_id)
	{

		$visit_invoice_id = $this->input->post('visit_invoice_id');
		$approved_amount = $this->input->post('approved_amount');
		$total_invoice_amount = $this->input->post('total_invoice_amount');

		// var_dump($total_invoice_amount);
		if($approved_amount <= $total_invoice_amount)
		{

			if($this->accounts_model->approve_invoice($visit_id,$visit_invoice_id))
			{

				// $this->send_approved_message($visit_id);
				// $this->session->set_userdata('success_message', 'Invoice has been marked as approved');
				// redirect('preauths');
				$response['message']= 'success';
				$response['result']= 'Invoice was successfully marked as approved';
			}

			else
			{
				$this->session->set_userdata('error_message', 'Unable to end visits');
				// redirect('receipt-payment/'.$visit_id.'/2');
				$response['message']= 'fail';
				$response['result']= 'Sorry please try again';
			}
		}
		else
		{
			$response['message']= 'fail';
			$response['result']= 'Sorry the invoiced amount seems to be less than the amount you want to approve';
		}
		

		echo json_encode($response);
	}


	public function send_approved_message($visit_id)
	{

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['inatient'] = $patient['inatient'];
		$v_data['patient_phone1'] = $patient_phone = $patient['patient_phone_number'];

		$message  = 'Hello '.$v_data['patient_othernames'].' the preauthorization has been approved. Kindly book an appointment with us on 0740579064. Thank you Arrow Dental';



		$message_data = array(
						"phone_number" => $patient_phone,
						"entryid" => $v_data['patient_id'],
						"message" => $message,
						"message_batch_id"=>0,
						'message_status' => 0
					);
		$this->db->insert('messages', $message_data);
		$message_id = $this->db->insert_id();
		$patient_phone = 704808007;
		$patient_phone = str_replace(' ', '', $patient_phone);
		// var_dump($patient); die();
		if(!empty($patient_phone))
		{
			$response = $this->messaging_model->sms($patient_phone,$message);

			if($response == "Success" OR $response == "success")
			{

				$service_charge_update = array('message_status' => 1,'delivery_message'=>'Success','sms_cost'=>3);
				$this->db->where('message_id',$message_id);
				$this->db->update('messages', $service_charge_update);

			}
			else
			{
				$service_charge_update = array('message_status' => 0,'delivery_message'=>'Success','sms_cost'=>0);
				$this->db->where('message_id',$message_id);
				$this->db->update('messages', $service_charge_update);


			}
		}
		else
		{
			$response['status'] = 1;
			$response['message'] = "Sorry could not send message";
		}
		return TRUE;
		 // echo json_encode($response);
	}


	public function accounts_update_bill($procedure_id,$v_id,$suck)
	{
		$service_charge_rs = $this->accounts_model->get_service_charge($procedure_id);

		foreach ($service_charge_rs as $key) :
			# code...
			$visit_charge_amount = $key->service_charge_amount;
		endforeach;

		$rs = $this->nurse_model->check_visit_type($v_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
			# code...
			  $visit_t = $rs1->visit_type;
			  $branch_id = $rs1->branch_id;
		  }
		}

		
		if($branch_id == 2)
		{
			$store_id = 6;
		}
		else
		{
			$store_id = 7;
		}

		$visit_data = array('service_charge_id'=>$procedure_id,'visit_id'=>$v_id,'visit_charge_amount'=>$visit_charge_amount,'visit_charge_units'=>$suck,'charge_to'=>$visit_t,'created_by'=>$this->session->userdata("personnel_id"),'date'=>date("Y-m-d"),'charged'=>1,'store_id'=>$store_id);
		$this->db->insert('visit_charge', $visit_data);

		$response['status'] = 1;
		$response['message'] = "Sorry could not send message";
		 echo json_encode($response);
	}

	public function change_payer($visit_charge_id,$service_charge_id,$visit_id){

		// $status = $this->accounts_model->check_if_visit_active($visit_id);
		// if($status)
		// {

			$rs = $this->nurse_model->check_visit_type($visit_id);
			if(count($rs)>0){
			  foreach ($rs as $rs1) {
				# code...
				  $visit_t = $rs1->visit_type;
			  }
			}

			$this->db->where('visit_charge_id',$visit_charge_id);
			$query = $this->db->get('visit_charge');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$charge_to = $value->charge_to;
				}
			}

			if($charge_to == $visit_t)
			{
				$charge = 1;
			}
			else
			{
				$charge = $visit_t;
			}

			// if(empty($charge))
			// {
			// 	$charge = $visit_t;
			// }
			$visit_data = array('charge_to'=>$charge);
			$this->db->where(array("visit_charge_id"=>$visit_charge_id));
			$this->db->update('visit_charge', $visit_data);

			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";
		// }
		// else
		// {
		// 	$response['status'] = "success";
		// 	$response['message'] = "Sorry the visit has been ended";
		// }
		echo json_encode($response);
	}


	public function print_quote($visit_id,$page_item = NULL)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = $page_item;
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$this->load->view('quote', $data);

	}

	public function generate_invoice_number($visit_id,$visit_type_id)
	{
		$prefix = $this->reception_model->create_invoice_number();
		

		// $this->db->where('visit_type_id',$visit_type_id);
		// $query = $this->db->get('visit_type');
		// $visit_type_preffix = '';
		// if($query->num_rows() > 0)
		// {
		// 	foreach ($query->result() as $key => $value) {
		// 		# code...
		// 		$visit_type_preffix = $value->visit_type_preffix;
		// 	}
		// }

		$visit_rs = $this->accounts_model->get_visit_details($visit_id);
		$branch_code = 'RS';
		if($visit_rs->num_rows() > 0)
		{
			foreach ($visit_rs->result() as $key => $value) {
				# code...
				$branch_code = $value->branch_code;

				// $visit_time_out = date('jS F Y',strtotime($visit_time_out));
			}
		}
		if($prefix < 10)
		{
			$number = '00000'.$prefix;
		}
		else if($prefix < 100 AND $prefix >= 10)
		{
			$number = '0000'.$prefix;
		}
		else if($prefix < 1000 AND $prefix >= 100)
		{
			$number = '000'.$prefix;
		}
		else if($prefix < 10000 AND $prefix >= 1000)
		{
			$number = '00'.$prefix;
		}
		else if($prefix < 100000 AND $prefix >= 10000)
		{
			$number = '0'.$prefix;
		}

		$invoice_number = $branch_code.$number;
		
		// var_dump($branch_code);die();



		$visit_data = array(
			"invoice_number"=>$invoice_number,
			"invoice_year"=>date('Y'),
			"invoice_month"=>date('m'),
			"prefix"=>$prefix
		);
		$this->db->where('visit_id',$visit_id);
		$this->db->update('visit', $visit_data);

		redirect('receipt-payment/'.$visit_id.'/0');
	}
	public function update_charges_account($visit_id,$visit_charge_id)
	{


		$billed_amount = $this->input->post('billed_amount');
		$units = $this->input->post('units');
		$notes = $this->input->post('notes');
		$work_status = $this->input->post('work_status');
		$dentine_id = $this->input->post('dentine_id');
		$teeth_id = $this->input->post('teeth_id');
		$patient_id = $this->input->post('patient_id');

		$this->db->where('visit_charge_id',$visit_charge_id);
		$query = $this->db->get('visit_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_invoice_id = $value->visit_invoice_id;
				$work_status_items = $value->work_status;
				// $work_status_items = $value->work_status;
			}
		}
		$visit_data = array('visit_charge_amount'=>$billed_amount,'visit_charge_units'=>$units,'plan_status'=>$work_status);

		if($work_status == 2)
		{
			// $visit_data['date'] = date('Y-m-d');
			// $visit_data['time'] = date('H:i:s');
		}
		else
		{
			if($visit_invoice_id > 0)
			{
				
			}
			else
			{
				$visit_data['date'] = NULL;
				$visit_data['time'] = NULL;
				$visit_data['charged'] = 0;
				// $charg
			}
		}

		$this->db->where('visit_charge_id', $visit_charge_id);
		$this->db->update('visit_charge', $visit_data);

		if(!empty($dentine_id))
		{
			$dentine_data = array('plan_status'=>$work_status);
			$this->db->where('dentine_id', $dentine_id);
			$this->db->update('dentine', $dentine_data);

		}

		if(!empty($notes))
		{
			$this->db->where('visit_charge_id ='.$visit_charge_id.' AND visit_id = '.$visit_id);
			$query_items = $this->db->get('visit_charge_procedure');

			if($query_items->num_rows() > 0)
			{
				// update this table
				$procedure_data = array('patient_id'=>$patient_id,'notes'=>$notes,'visit_id'=>$visit_id,'visit_charge_id'=>$visit_charge_id);
				$this->db->where('visit_charge_id ='.$visit_charge_id.' AND visit_id = '.$visit_id);
				$this->db->update('visit_charge_procedure', $procedure_data);

			}
			else
			{
				$procedure_data = array('patient_id'=>$patient_id,'notes'=>$notes,'created'=>date('Y-m-d H:i:s'),'created_by'=>$this->session->userdata('personnel_id'),'visit_id'=>$visit_id,'visit_charge_id'=>$visit_charge_id);
				$this->db->where('visit_charge_id ='.$visit_charge_id.' AND visit_id = '.$visit_id);
				$this->db->insert('visit_charge_procedure', $procedure_data);
			}
			

		}
		

		$response['status'] = 1;
		$response['message'] = "success";
		 echo json_encode($response);

	}

	public function get_patient_statement($patient_id)
	{
		

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		

		$lab_test_where = 'patient_id = '.$patient_id;
		$lab_test_table = 'patients';
		$lab_test_where .= ' AND patient_delete = 0';

		$this->db->where($lab_test_where);
		$this->db->join('relationship','relationship.relationship_id = patients.relationship_id','LEFT');
		$this->db->limit(1);
		$query = $this->db->get($lab_test_table);

		
		$v_data['query'] = $query;
		$v_data['patient_id'] = $patient_id;

		
		// $result = $this->load->view('patients_views/patient_statement', $v_data,true);
		$result = $this->load->view('patients_views/patient_account', $v_data,true);
		$response['message'] = 'success';
		$response['result'] = $result;

		echo json_encode($response);
	
	}

	public function get_visit_invoices($visit_id,$patient_id,$visit_invoice_id = NULL,$visit_date=NULL)
	{

		$data['title'] = 'Visit Invoices';
		
		$creditor_where = 'visit_invoice.visit_id = visit.visit_id AND visit_invoice.visit_invoice_delete = 0 AND visit.visit_delete = 0  AND visit_invoice.patient_id = '.$patient_id;

		if(!empty($visit_invoice_id))
		{
			$creditor_where .= ' AND visit_invoice.visit_invoice_id = '.$visit_invoice_id;
		}

		$creditor_table = 'visit,visit_invoice';

		$this->db->where($creditor_where);
		$creditor_query = $this->db->get($creditor_table);

		$data['visit_notes'] = $creditor_query;
		$data['patient_id'] = $patient_id;
		$data['visit_id'] = $visit_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$data['billing_date'] = $visit_date;

		$this->db->where('visit_id',$visit_id);
		$this->db->select('visit.*');
		$query = $this->db->get('visit');
		$data['status'] = 1;
		$data['query'] = $query;

		// var_dump($data);die();
			
		$page = $this->load->view('sidebar/visit_invoices',$data);

		echo $page;


	}
	public function search_procedures($patient_id,$visit_id,$visit_type_id = NULL,$visit_invoice_id =NULL,$view=NULL)
	{
		$symptoms_search = $this->input->post('query');
		$query = null;
		if(!empty($symptoms_search))
		{
			$lab_test_where = 'service_charge.service_charge_delete = 0';
			$lab_test_table = 'service_charge';
			$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$symptoms_search.'%\' OR service_charge.service_charge_code LIKE \'%'.$symptoms_search.'%\')';
			// if(!empty($visit_type_id))
			// {
			// 	$lab_test_where .= ' AND service_charge.visit_type_id = '.$visit_type_id;
			// }
			// else
			// {
				// $lab_test_where .= ' AND service_charge.visit_type_id = 1';
			// }
			$this->db->where($lab_test_where);
			$this->db->limit(10);
			$query = $this->db->get($lab_test_table);

			if($query->num_rows() == 0)
			{

				$lab_test_where = 'service_charge.service_charge_delete = 0';
				$lab_test_table = 'service_charge';
				$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$symptoms_search.'%\' OR service_charge.service_charge_code LIKE \'%'.$symptoms_search.'%\')';
				
				// $lab_test_where .= ' AND service_charge.visit_type_id = 1';
				
				$this->db->where($lab_test_where);
				$this->db->limit(10);
				$query = $this->db->get($lab_test_table);
			}

		}
		$data['query'] = $query;
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['item_view'] = $view;
		$data['visit_type_id'] = $visit_type_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		$page = $this->load->view('sidebar/search_service_charge',$data);

		echo $page;
	}
	public function get_visit_charges($patient_id,$visit_id=NULL,$visit_invoice_id = null)
	{
		$patient = $this->reception_model->patient_names2(null,$visit_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		$data['patient'] = $patient;
		$page=$this->load->view('sidebar/get_visit_charges',$data,true);

		echo $page;
	}

	public function add_payment($patient_id,$visit_invoice_id=NULL)
	{

		$data['title'] = 'Add Payment';
		
		// $creditor_where = 'visit.visit_id = appointments.visit_id AND visit.close_card = 2 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_delete = 0 and patient_id = '.$patient_id;
		// $creditor_table = 'visit,appointments';

		// $this->db->where($creditor_where);
		// $creditor_query = $this->db->get($creditor_table);

		// $data['open_visits'] = $creditor_query;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_idd'] = $visit_invoice_id;


			
		$page = $this->load->view('sidebar/add_payment',$data);

		echo $page;
	}

	public function get_visit_payments($patient_id,$payment_id = NULL)
	{
		$data['title'] = 'Receipt';
		
		
		// $data['visit_notes'] = $creditor_query;
		// $data['visit_id'] = $visit_id;


		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// $v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
		// $patient = $this->reception_model->patient_names2(NULL, $visit_id);
		// $data['patient_type'] = $patient['patient_type'];
		// $data['patient_othernames'] = $patient['patient_othernames'];
		// $data['patient_surname'] = $patient['patient_surname'];
		// $data['patient_type_id'] = $patient['visit_type_id'];
		// $data['account_balance'] = $patient['account_balance'];
		// $data['visit_type_name'] = $patient['visit_type_name'];
		// $data['patient_id'] = $patient['patient_id'];
		// $data['inatient'] = $patient['inatient'];

		$data['payment_id'] = $payment_id;
		$data['patient_id'] = $patient_id;
		$data['doctor'] = $this->reception_model->get_doctor();


		// $primary_key = $patient['patient_id'];


		// $rs = $this->nurse_model->check_visit_type($visit_id);
		// if(count($rs)>0){
		//   foreach ($rs as $rs1) {
		//     # code...
		//       $visit_t = $rs1->visit_type;
		//   }
		// }
		$close_page = 0;
			
		$page = $this->load->view('sidebar/visit_payments',$data);

		echo $page;
	}
	public function get_visit_invoice_payments($patient_id,$visit_invoice_id=null)
	{
		$data['title'] = 'Receipt';
		
		
	
		

		$data['visit_invoice_id'] = $visit_invoice_id;
		$data['patient_id'] = $patient_id;
		$data['doctor'] = $this->reception_model->get_doctor();


		$close_page = 0;
			
		$page = $this->load->view('sidebar/visit_invoice_payments',$data);

		echo $page;
	}

	public function make_visit_payments($visit_id, $close_page = NULL)
	{
		
		$this->form_validation->set_rules('type_payment', 'Type of payment', 'trim|required|xss_clean');
		$payment_method = $this->input->post('payment_method');
		// normal or credit note or debit note
		$type_payment = $this->input->post('type_payment');
		

			if($type_payment == 1)
			{
				$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_service_id', 'Payment Service', 'trim|xss_clean');
				$this->form_validation->set_rules('service_id', 'Service', 'xss_clean');
				if(!empty($payment_method))
				{
					if($payment_method == 1)
					{
						// check for cheque number if inserted
						$this->form_validation->set_rules('cheque_number', 'Cheque Number', 'trim|required|xss_clean');
					}
					else if($payment_method == 6)
					{
						// check for insuarance number if inserted
						$this->form_validation->set_rules('insuarance_number', 'Credit Card Detail', 'trim|required|xss_clean');
					}
					else if($payment_method == 5)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('mpesa_code', 'Amount', 'trim|xss_clean');
					}
					else if($payment_method == 7)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('deposit_detail', 'Bank Deposit', 'trim|xss_clean');
					}
					else if($payment_method == 8)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Debit Card', 'trim|required|xss_clean');
					}
				}
			}
			else if($type_payment == 2)
			{
				$this->form_validation->set_rules('waiver_amount', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('reason', 'Reason', 'trim|required|xss_clean');
				// var_dump($_POST); die();
				// debit note
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			else if($type_payment == 3)
			{
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				// var_dump($_POST); die();
					if($this->accounts_model->receipt_payment($visit_id))
					{
						$response['message'] ='success';
						$response['result'] ='You have successfully receipted the payment';
						$this->session->set_userdata('success_message', 'You have successfully receipted the payment');
					}
					else
					{
						$response['message'] ='fail';
						$response['result'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
						$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');
					}

				
			}
			else
			{
				$response['message'] ='fail';
				$response['result'] =validation_errors();
				$this->session->set_userdata('error_message', $validation_errors());
			}
			
			// redirect('receipt-payment/'.$visit_id.'/'.$close_page);

			echo json_encode($response);

			// }
			
	}
	public function confirm_visit_charge($visit_id,$patient_id,$visit_invoice_id = NULL)
	{

		// $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		// $this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
	 //    $this->form_validation->set_rules('invoice_date', 'Invoice Date ', 'trim|required|xss_clean');
	    $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
	    $this->form_validation->set_rules('invoice_date', 'Invoice Date', 'trim|required|xss_clean');
// var_dump($_POST);die();
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
				$this->accounts_model->confirm_invoice_charges($visit_id,$patient_id,$visit_invoice_id);

				// $this->session->set_userdata("success_message", 'Creditor invoice successfully added');
				$response['message'] = 'success';
				$response['result'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['message'] = 'fail';
			$response['result'] = strip_tags(validation_errors());

		}


	  
		echo json_encode($response);

	}

	public function add_payment_item($patient_id,$payment_id = NULL)
	{

		$this->form_validation->set_rules('amount', 'Unit Price', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('invoice_id', 'Invoice', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{

		  $this->accounts_model->add_payment_item($patient_id,$payment_id);
		  $this->session->set_userdata("success_message", 'Payment successfully added');
		  $response['status'] = 'success';
		  $response['message'] = 'Payment successfully added';
		}
		else
		{
		  $this->session->set_userdata("error_message", validation_errors());
		  $response['status'] = 'fail';
		  $response['message'] = validation_errors();

		}
		// $redirect_url = $this->input->post('redirect_url');
		// redirect($redirect_url);

		echo json_encode($response);

	}



	public function confirm_payment($patient_id, $payment_id = NULL)
	{
		
		$this->form_validation->set_rules('type_payment', 'Type of payment', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('total_amount', 'Total Payment', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$payment_method = $this->input->post('payment_method');
		// normal or credit note or debit note
		$type_payment = $this->input->post('type_payment');
		

			if($type_payment == 1)
			{
				$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Payment Service', 'trim|xss_clean');
				// $this->form_validation->set_rules('service_id', 'Service', 'xss_clean');
				if(!empty($payment_method))
				{
					if($payment_method == 1)
					{
						// check for cheque number if inserted
						$this->form_validation->set_rules('cheque_number', 'Cheque Number', 'trim|required|xss_clean');
					}
					else if($payment_method == 6)
					{
						// check for insuarance number if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Credit Card Detail', 'trim|required|xss_clean');
					}
					else if($payment_method == 5)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('mpesa_code', 'Amount', 'trim|xss_clean');
					}
					else if($payment_method == 7)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('deposit_detail', 'Bank Deposit', 'trim|xss_clean');
					}
					else if($payment_method == 8)
					{
						//  check for mpesa code if inserted
						$this->form_validation->set_rules('debit_card_detail', 'Debit Card', 'trim|required|xss_clean');
					}
				}
			}
			else if($type_payment == 2)
			{
				$this->form_validation->set_rules('waiver_amount', 'Amount', 'trim|required|xss_clean');
				$this->form_validation->set_rules('reason', 'Reason', 'trim|required|xss_clean');
				// var_dump($_POST); die();
				// debit note
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			else if($type_payment == 3)
			{
				// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
				// $this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
			}
			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				// var_dump($_POST); die();
					if($this->accounts_model->confirm_payment($patient_id,$payment_id))
					{
						$response['message'] ='success';
						$response['result'] ='You have successfully receipted the payment';
						$this->session->set_userdata('success_message', 'You have successfully receipted the payment');
					}
					else
					{
						$response['result'] ='fail';
						$response['message'] ='Seems like you dont have the priviledges to effect this event. Please contact your administrator.';
						$this->session->set_userdata('error_message', 'Seems like you dont have the priviledges to effect this event. Please contact your administrator.');
					}

				
			}
			else
			{
				$response['message'] ='fail';
				$response['result'] =validation_errors();
				// $this->session->set_userdata('error_message', $validation_errors());
			}
			
		echo json_encode($response);
			
	}
	public function get_incomplete_invoices($patient_id,$visit_id=NULL)
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		

		$lab_test_where = 'patient_id = '.$patient_id;
		$lab_test_table = 'patients';
		$lab_test_where .= ' AND patient_delete = 0';

		$this->db->where($lab_test_where);
		$this->db->join('relationship','relationship.relationship_id = patients.relationship_id','LEFT');
		$this->db->limit(1);
		$query = $this->db->get($lab_test_table);

		
		$v_data['query'] = $query;
		$v_data['patient_id'] = $patient_id;
		$v_data['visit_id'] = $visit_id;

		
		$result = $this->load->view('patients_views/patient_incomplete_invoices', $v_data,true);
		$response['message'] = 'success';
		$response['result'] = $result;

		echo json_encode($response);
	}
	public function print_receipt($payment_id,$visit_invoice_id)
	{
		$data = array('visit_invoice_id'=>$visit_invoice_id,'payment_id'=>$payment_id);
		

		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}


		$data['contacts'] = $this->site_model->get_contacts($branch_id);

		
		// var_dump($data);die();
		// $patient = $this->reception_model->patient_names2(NULL, $visit_id);
		// $data['patient'] = $patient;
		$this->load->view('receipt', $data);
	}

	public function update_receipt()
	{

		$this->db->select('payments.payment_id, visit.branch_id,visit.branch_code');
		$this->db->group_by('payment_item.payment_id');
		$this->db->where('payments.payment_id = payment_item.payment_id AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id AND visit.visit_id = visit_invoice.visit_id AND payments.payment_id >= 2575');

		$query = $this->db->get('payments,payment_item,visit_invoice,visit');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$branch_code = $value->branch_code;
				$payment_id = $value->payment_id;
				$branch_id = $value->branch_id;

				// if()

				$prefix = $suffix = $this->accounts_model->create_receipt_number();
				$branch_code = str_replace("RS", "CT", $branch_code);

				if($branch_code == "CT")
				{
					$branch_code = "CTR";
				}


				if($prefix < 10)
				{
					$number = '00000'.$prefix;
				}
				else if($prefix < 100 AND $prefix >= 10)
				{
					$number = '0000'.$prefix;
				}
				else if($prefix < 1000 AND $prefix >= 100)
				{
					$number = '000'.$prefix;
				}
				else if($prefix < 10000 AND $prefix >= 1000)
				{
					$number = '00'.$prefix;
				}
				else if($prefix < 100000 AND $prefix >= 10000)
				{
					$number = '0'.$prefix;
				}

				$invoice_number = $branch_code.$number;
				$data['confirm_number'] = $invoice_number;
				$data['suffix'] = $suffix;
				$data['branch_id'] = $branch_id;

				// var_dump($payment_id); die();
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments', $data);
			}
		}

		
		


	}
	public function delete_payment_item($payment_item_id)
	{

			$this->db->where(array("payment_item_id"=>$payment_item_id));
			$this->db->delete('payment_item', $visit_data);
			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";

			echo json_encode($response);
	}

	public function submit_invoice($visit_invoice_id,$visit_id)
	{

		$visit_rs = $this->accounts_model->get_visit_details($visit_id);
		$visit_type_id = 0;
		if($visit_rs->num_rows() > 0)
		{
			foreach ($visit_rs->result() as $key => $value) {
				# code...
				$close_card = $value->close_card;
				$visit_type_id = $value->visit_type;
				$visit_date = $value->visit_date;
				$invoice_number = $value->invoice_number;
				$visit_time_out = $value->visit_time_out;
				$parent_visit = $value->parent_visit;
				$nr = $value->nr;
				$insurance_limit = $value->insurance_limit;
				$medicalaid_code = $value->medicalaid_code;
				$medicalaid_plan = $value->medicalaid_plan;
				$medicalaid_number = $value->patient_insurance_number;
				$global_id = $value->global_id;
				$patient_id = $value->patient_id;

				
				$visit_time_out = date('jS F Y',strtotime($visit_time_out));
			}
		}
		// var_dump($patient_insurance_number);die();
		$patient = $this->reception_model->get_patient_data($patient_id);
		$patient = $patient->row();
		$patient_othernames = $patient->patient_othernames;
		$patient_surname = $patient->patient_surname;
		$patient_date_of_birth = $patient->patient_date_of_birth;
		$gender_id = $patient->gender_id;
		$patient_number = $patient->patient_number;

		if($gender_id == 1)
		{
			$gender = 'FEMALE';
		}else
		{
			$gender = 'MALE';
		}

		$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

		if($visit_invoice_detail->num_rows() > 0)
		{
			foreach ($visit_invoice_detail->result() as $key => $value) {
				# code...
				$visit_invoice_number = $value->visit_invoice_number;
				$preauth_date = date('d/m/Y',strtotime($value->preauth_date));
				$invoice_date = $claim_date = $value->created;
				$preauth_amount = $value->preauth_amount;



			}
		}



		$total = 0;
        $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_tree($visit_id,$visit_invoice_id);
		$total_amount= 0; 
		$days = 0;
		$count = 0;
		$services_array['Service']=array();
		if($item_invoiced_rs->num_rows() > 0)
		{
			foreach ($item_invoiced_rs->result() as  $value) {
				# code...
				$service_id= $value->service_id;
				$service_name = $value->service_name;

			

				$rs2 = $this->accounts_model->get_visit_procedure_charges_per_service($visit_id,$service_id,$visit_invoice_id); 

				
				$sub_total= 0; 
				$personnel_query = $this->personnel_model->retrieve_personnel();
				

				if(count($rs2) >0){
				
					$visit_date_day = '';
					
					foreach ($rs2 as $key1):

						$v_procedure_id = $key1->visit_charge_id;
						$procedure_id = $key1->service_charge_id;
						$date = $key1->date;
						$time = $key1->time;
						$visit_charge_timestamp = $key1->visit_charge_timestamp;
						$visit_charge_amount = $key1->visit_charge_amount;
						$units = $key1->visit_charge_units;
						$procedure_name = $key1->service_charge_name;
						$service_id = $key1->service_id;
						$provider_id = $key1->provider_id;
						$service_charge_code = $key1->service_charge_code;
					
						$sub_total= $sub_total +($units * $visit_charge_amount);
						$visit_date = date('l d F Y',strtotime($date));
						$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
						if($visit_date_day != $visit_date)
						{
							
							$visit_date_day = $visit_date;
						}
						else
						{
							$visit_date_day == $visit_date;
						}

						

						if($personnel_query->num_rows() > 0)
						{
							$personnel_result = $personnel_query->result();
							
							foreach($personnel_result as $adm)
							{
								$personnel_id = $adm->personnel_id;
								
								
									if($personnel_id == $provider_id)
									{
										$provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

										$procedure_name = $procedure_name.$provider_id;
									}
								
							}

						}
						
						else
						{
							$provider_id = '';
							
						}
						if($procedure_name == 'General Female Ward' || $procedure_name == 'General Male Ward' || $procedure_name == 'Private Ward' || $procedure_name == 'Semi-Private Female Ward' || $procedure_name == 'High Dependancy Unit Ward'  || $procedure_name == 'Intensive Care Unit Ward' )
						{
							$days++;
						}

						$visit_charge_timestamp = $key1->visit_charge_timestamp;

						$time_explode = explode(' ', $visit_charge_timestamp);


						$time = $time_explode[1];
						$procedure['Number'] =$v_procedure_id;
						$procedure['Invoice_Number'] =$visit_invoice_number;
						$procedure['Global_Invoice_Nr'] =$visit_invoice_number;
						$procedure['Start_Date'] =$date;
						$procedure['Start_Time'] =$time;
						$procedure['Provider'] = array (
											            'Role' => 'SP',
											            'Practice_Number' => 'SKSP_2540',
											          );
						$procedure['Diagnosis'] =array (
											            'Stage' => 'P',
											            'Code_Type' => 'internal code',
											            'Code' => $procedure_id,
											          );
						$procedure['Encounter_Type'] = "$service_name"; 
						$procedure['Code_Type'] = 'internal code'; 
						$procedure['Code'] = "$procedure_id"; 
						$procedure['Code_Description'] = "$procedure_name"; 
						$procedure['Quantity'] = "$units"; 
						$procedure['Total_Amount'] = $units * $visit_charge_amount; 




						array_push($services_array['Service'], $procedure);

						$count++;
						// echo"
						// 		<tr> 
						// 			<td >".$count."</td>
						// 			<td >".strtoupper($procedure_name)."</td>
						// 			<td >".$units."</td>
						// 			<td >".number_format($visit_charge_amount,2)."</td>
						// 			<td >".number_format($units * $visit_charge_amount,2)."</td>
									
						// 		</tr>	
						// ";
						$visit_date_day = $visit_date;
						endforeach;
						

				}
			
				$total_amount = $total_amount + $sub_total;

			}
		}
		// var_dump($services_array['Service']); die();

		$items = array (
				  'Claim' => 
				  array (
				    'Claim_Header' => 
				    array (
				      'Invoice_Number' => "$visit_invoice_number",
				      'Claim_Date' => "$claim_date",
				      'Claim_Time' => date('H:i:s'),
				      'Pool_Number' => "$nr",
				      'Total_Services' => "$count",
				      'Gross_Amount' => "$total_amount",
				    ),
				    'Provider' => 
				    array (
				      'Role' => 'SP',
				      'Country_Code' => 'KEN',
				      'Group_Practice_Number' => 'SKSP_2540',
				      'Group_Practice_Name' => 'ROYAL SMILES DENTAL CLINIC-RUAKA',
				    ),
				    'Authorization' => 
				    array (
				      'Pre_Authorization_Number' => '12',
				      'Pre_Authorization_Amount' => '0',
				    ),
				    'Payment_Modifiers' => 
				    array (
				      'Payment_Modifier' => 
				      array (
				        'Type' => '1',
				        'Amount' => '0',
				        'Receipt' => '0',
				      ),
				    ),
				    'Member' => 
				    array (
				      'Membership_Number' => $medicalaid_number,
				      'Scheme_Code' => $medicalaid_code,
				      'Scheme_Plan' => $medicalaid_plan,
				    ),
				    'Patient' => 
				    array (
				      'Dependant' => 'PRINCIPAL',
				      'First_Name' => $patient_othernames,
				      'Middle_Name' => '',
				      'Surname' => $patient_surname,
				      'Date_Of_Birth' => $patient_date_of_birth,
				      'Gender' => $gender,
				    ),
				    'Claim_Data' => 
				    array (
				      'Service' => $services_array['Service']
				      
				    ),
				  ),
				);
		// var_dump($items);die();
		$data_string = json_encode($items);
		// var_dump($data_string);die();




		$url = 'https://data.smartapplicationsgroup.com/api/smartlink/json/submitinvoice?patientid='.$patient_number.'&globalid='.$global_id;
		// var_dump($url);die();
		// $data_string = 
		try{                                                                                                         

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string),
				'X-Gravitee-Api-Key: 913ff562-8101-4e1b-b0a1-882fcabc8c12')
			);
			$result = curl_exec($ch);

			$array = json_decode($result,true);

			

			 var_dump($array);die();

			curl_close($ch);

		}
		catch(Exception $e)
		{
			// $response = "something went wrong";
			// echo json_encode($response.' '.$e);
			$response['message'] = 'fail';
			
		}

		echo json_encode($response);
	}


	public function add_visit_charge($procedure_id,$visit_id,$patient_id,$visit_type_id=NULL,$visit_invoice_id=NULL)
	{

		// $visit_type_id = $this->input->post('visit_type_id');


		$data = array('procedure_id'=>$procedure_id,'visit_id'=>$visit_id,'suck'=>1);

		if(empty($visit_type_id))
		{

			if($this->accounts_model->visit_procedure_done($visit_id,$procedure_id,1))
			{
				$data['message'] = 'success';
				$data['result'] = 'You have successfully added the procedure charge';
			}
			else
			{
				$data['message'] = 'fail';
				$data['result'] = 'Sorry please try again';
			}
		}
		else
		{
				// var_dump($data);die();

			if($this->accounts_model->visit_charge_insert($visit_id,$procedure_id,1,$visit_invoice_id,$patient_id))
			{
				$data['message'] = 'success';
				$data['result'] = 'You have successfully added the service charge';
			}
			else
			{
				$data['message'] = 'fail';
				$data['result'] = 'Sorry please try again';
			}
		}
	

		

		echo json_encode($data);
		
	}


	public function update_visit_invoice_data()
	{
		$this->db->select('visit_invoice.visit_invoice_id,visit.*');
		$this->db->where('(visit_invoice.member_number IS NULL OR visit_invoice.member_number = "") AND visit.visit_id = visit_invoice.visit_id AND visit.patient_insurance_number IS NOT NULL');
		$query = $this->db->get('visit_invoice,visit');
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				// $visit_type = $value->visit_type;
				$visit_invoice_id = $value->visit_invoice_id;
				// $insurance_limit = $value->insurance_limit;
				$patient_insurance_number = $value->patient_insurance_number;
				// $insurance_description = $value->insurance_description;


				// $array['bill_to'] = $visit_type;
				// $array['insurance_limit'] = $insurance_limit;
				$array['member_number'] = $patient_insurance_number;
				// $array['scheme_name'] = $insurance_description;
				$this->db->where('visit_invoice_id',$visit_invoice_id);
				$this->db->update('visit_invoice',$array);


			}
		}

	}
		public function open_shift()
	{
		
		$this->form_validation->set_rules('redirect_url', 'URL', 'trim|required|xss_clean');
		// var_dump($_POST); die();
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
		
			$personnel_id = $this->input->post('personnel_id');
			$shift_type = $this->input->post('shift_type');
		

			$this->session->set_userdata('shift_type',$shift_type);
			$visit_data = array('personnel_id'=>$personnel_id, 
								'sign_time_in'=>date("Y-m-d H:i:s"),
								'shift_type'=>$shift_type
								);
			$this->db->insert('personnel_shift', $visit_data);

			$response['status'] = "success";
			$this->session->userdata('success_message','You have successfully updated the charge');

		}
		else
		{
			
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$this->session->userdata('error_message',validation_errors());

			

		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function close_shift()
	{
		
		$this->form_validation->set_rules('redirect_url', 'URL', 'trim|required|xss_clean');
		// var_dump($_POST); die();
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
		
			$personnel_id = $this->input->post('personnel_id');
			$shift_type = $this->input->post('shift_type');
		

			$this->session->set_userdata('shift_type',$shift_type);

			$this->db->where('DATE(sign_time_in) = "'.date('Y-m-d').'" AND personnel_id = '.$personnel_id);
			$this->db->order_by('shift_id','DESC');
			$this->db->limit(1);
			$query_old=$this->db->get('personnel_shift');

			$shift_type = 0;
			if($query_old->num_rows() > 0)
			{
				foreach ($query_old->result() as $key => $value) {
					# code...
					$shift_id = $value->shift_id;
				}

				$visit_data = array('personnel_id'=>$personnel_id, 
									'sign_time_out'=>date("Y-m-d H:i:s"),
									'shift_type'=>$shift_type
									);
				$this->db->where('shift_id',$shift_id);
				$this->db->update('personnel_shift', $visit_data);
			}

			$response['status'] = "success";
			$this->session->userdata('success_message','You have successfully updated the charge');

		}
		else
		{
			
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$this->session->userdata('error_message',validation_errors());

			

		}
		// $this->reporting->daily_report();
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function delete_invoice($visit_invoice_id,$patient_id)
	{
		$array['visit_invoice_delete'] = 1;
		$this->db->where('visit_invoice_id ='.$visit_invoice_id);
		if($this->db->update('visit_invoice',$array))
		{
			$response['message'] = 'success';
		}
		else
		{
			$response['message'] = 'fail';
		}

		echo json_encode($response);
	}

	public function delete_payment($payment_id,$patient_id)
	{
		$array['cancel'] = 1;
		$array['cancelled_by'] = $this->session->userdata('personnel_id');

		$this->db->where('payment_id ='.$payment_id);
		if($this->db->update('payments',$array))
		{

			if($this->accounts_model->update_visit_invoice_data($payment_id))
			{
				$response['message'] = 'success';
			}
			else
			{
				$response['message'] = 'fail';
			}
		}
		else
		{
			$response['message'] = 'fail';
		}

		echo json_encode($response);
	}

	public function get_visit_credit_notes($visit_id,$patient_id,$visit_invoice_id=NULL,$visit_credit_note_id = NULL)
	{

		$data['title'] = 'Credit Note';
		
		$creditor_where = 'visit_credit_note.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_credit_note.visit_cr_note_delete = 0 AND visit_invoice.visit_invoice_delete = 0  AND visit_credit_note.patient_id = '.$patient_id;

		if(!empty($visit_credit_note_id))
		{
			$creditor_where .= ' AND visit_credit_note.visit_credit_note_id = '.$visit_credit_note_id;
		}

		$creditor_table = 'visit_invoice,visit_credit_note';

		$this->db->where($creditor_where);
		$creditor_query = $this->db->get($creditor_table);

		$data['visit_notes'] = $creditor_query;
		$data['patient_id'] = $patient_id;
		$data['visit_id'] = $visit_id;
		$data['visit_credit_note_id'] = $visit_credit_note_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$this->db->where('visit_id',$visit_id);
		$this->db->select('visit.*');
		$query = $this->db->get('visit');
		$data['status'] = 1;
		$data['query'] = $query;

		// var_dump($data);die();
			
		$page = $this->load->view('sidebar/visit_credit_notes',$data);

		echo $page;


	}

	public function add_credit_note_charge($procedure_id,$visit_id=NULL,$patient_id,$visit_type_id=NULL,$visit_invoice_id=NULL)
	{

		$visit_type_id = $this->input->post('visit_type_id');


		$data = array('procedure_id'=>$procedure_id,'visit_id'=>$visit_id,'suck'=>1);

		

		if($this->accounts_model->visit_cr_note_insert($visit_id,$procedure_id,1,$visit_invoice_id,$patient_id))
		{
			$data['message'] = 'success';
			$data['result'] = 'You have successfully added the service charge';
		}
		else
		{
			$data['message'] = 'fail';
			$data['result'] = 'Sorry please try again';
		}
		
	

		

		echo json_encode($data);
		
	}

	public function get_visit_credit_note($visit_id=null,$patient_id,$visit_invoice_id = null,$visit_credit_note_id=NULL)
	{
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$page=$this->load->view('sidebar/get_visit_credit_notes',$data,true);

		echo $page;
	}

	public function get_all_credit_notes($patient_id,$visit_invoice_id = null,$visit_id=NULL)
	{
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$page=$this->load->view('sidebar/get_all_credit_notes',$data,true);

		echo $page;
	}


	public function update_credit_total($procedure_id,$units,$amount,$visit_id)
	{
		
		// $status = $this->accounts_model->check_if_visit_active($visit_id);
		// if($status)
		// {
			$visit_charge_notes = $this->input->post('visit_charge_notes');
			$amount = str_replace(",", "", $amount);
			$amount = str_replace(".00", "", $amount);
			$visit_data = array('visit_cr_note_units'=>$units,
								'visit_cr_note_amount'=>$amount, 
								'modified_by'=>$this->session->userdata("personnel_id"),
								'date_modified'=>date("Y-m-d"),
								'visit_cr_note_comment' => $visit_charge_notes
							);
			$this->db->where(array("visit_credit_note_item_id"=>$procedure_id));
			$this->db->update('visit_credit_note_item', $visit_data);

			$response['status'] = "success";
			$response['message'] = "You have successfully updated the charge";
		// }
		// else
		// {
		// 	$response['status'] = "success";
		// 	$response['message'] = "Sorry the visit has been ended";
		// }
		echo json_encode($response);
	}

	public function confirm_credit_note($visit_id,$patient_id,$visit_credit_note_id = NULL)
	{

		// $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		// $this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
	 //    $this->form_validation->set_rules('invoice_date', 'Invoice Date ', 'trim|required|xss_clean');
	    $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
	    // $this->form_validation->set_rules('invoice_number', 'Invoice Number', 'trim|required|xss_clean');

		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
				$this->accounts_model->confirm_credit_note($visit_id,$patient_id,$visit_credit_note_id);

				// $this->session->set_userdata("success_message", 'Creditor invoice successfully added');
				$response['message'] = 'success';
				$response['result'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['message'] = 'fail';
			$response['result'] = validation_errors();

		}


	  
		echo json_encode($response);

	}
	public function print_credit_note($visit_credit_note_id,$visit_id=NULL,$patient_id,$page_item = NULL)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['page_item'] = $page_item;
		$patient = $this->reception_model->patient_names2($patient_id);
		$data['patient'] = $patient;
		$data['visit_credit_note_id'] = $visit_credit_note_id;

		// var_dump($data);die();
		$this->load->view('credit_note', $data);
		
	}

	public function individual_statement($patient_id,$module = NULL)
	{
		$data['contacts'] = $this->site_model->get_contacts();

		// var_dump($data);die();
		$this->db->where('visit_invoice.visit_invoice_delete = 0 AND visit_invoice.patient_id ='.$patient_id);
		$this->db->order_by('created','desc');
		$query = $this->db->get('visit_invoice');
		$v_data['query'] = $query;
		$patient = $this->reception_model->patient_names2($patient_id);
		$data['patient'] = $patient;
		$v_data['patient_id'] = $patient_id;

		$data['content'] = $this->load->view('accounts/individual_statement', $v_data, true);
		
		
		$this->load->view('admin/templates/general_page', $data);

	}


	public function print_individual_statement($patient_id)
	{
		
		$data['contacts'] = $this->site_model->get_contacts();
		$this->db->where('visit_invoice.visit_invoice_delete = 0 AND visit_invoice.patient_id ='.$patient_id);
		$this->db->order_by('created','desc');
		$query = $this->db->get('visit_invoice');
		$data['query'] = $query;
		$patient = $this->reception_model->patient_names2($patient_id);
		$data['patient'] = $patient;	
		// var_dump($query);die();
		$this->load->view('print_individual_statement', $data);
	}


	public function update_charge()
	{
		$query = $this->db->query('SELECT  visit_charge.* from visit_charge,visit_invoice where visit_charge_notes IS NULL AND service_charge_id  = 2675 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND visit_invoice.bill_to = 1 AND visit_charge.visit_charge_amount > 0');
		// var_dump($query->num_rows());die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_charge_id = $value->visit_charge_id;
				$visit_charge_amount = $value->visit_charge_amount;


				$array['visit_charge_amount'] = -$visit_charge_amount;
				$array['visit_charge_comment'] = 'CREDIT NOTE ON INVOICE';
				// var_dump($array);die();
				$this->db->where('visit_charge_id',$visit_charge_id);
				$this->db->update('visit_charge',$array);
			}
		}
	}

	public function mark_as_invoice($visit_id)
	{
		$visit_invoice_id = $this->input->post('visit_invoice_id');


		$array['preauth_status'] = 3;
		
		$this->db->where('visit_invoice_id',$visit_invoice_id);
		if($this->db->update('visit_invoice',$array))
		{

			$response['message'] = 'success';
			$response['result'] = 'Payment successfully added';
		}
		else
		{
			
			$response['message'] = 'fail';
			$response['result'] = 'Sorry could not mark proforma as an invoice';

		}
	  
		echo json_encode($response);

	}

	public function update_payments()
	{
		$this->db->where('cancel = 0');
		$query = $this->db->get('payments');


		if($query->num_rows() > 0)
		{
			$x= 0;
			foreach($query->result()  as $value )
			{
				$payment_id = $value->payment_id;
				$x++;

				$prefix = $suffix = $this->accounts_model->create_receipt_number();
				
				$branch_code = "CTR";

				if($prefix < 10)
				{
					$number = '0000'.$prefix;
				}
				else if($prefix < 100 AND $prefix >= 10)
				{
					$number = '000'.$prefix;
				}
				else if($prefix < 1000 AND $prefix >= 100)
				{
					$number = '00'.$prefix;
				}
				else if($prefix < 10000 AND $prefix >= 1000)
				{
					$number = '0'.$prefix;
				}
				else if($prefix < 100000 AND $prefix >= 10000)
				{
					$number = $prefix;
				}

				$invoice_number = $branch_code.$number;
				$data['confirm_number'] = $invoice_number;
				$data['suffix'] = $suffix;
				$data['prefix'] = $suffix;
				$data['branch_id'] = $branch_id;
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$data);
			}
		}
	}



	public function get_patient_bill_view($visit_id)
	{
		$patient_id = $this->reception_model->get_patient_id_from_visit($visit_id);
		$v_data = array('patient_id'=>$patient_id);
		
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
		$patient = $this->reception_model->get_patient_data($patient_id);
		$patient = $patient->row();
		$patient_othernames = $patient->patient_othernames;
		$patient_surname = $patient->patient_surname;

		$v_data['doctor'] = $this->reception_model->get_providers();

		
		$v_data['title'] = $patient_othernames.' '.$patient_surname;




		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND (service.service_name <> "Pharmacy" OR service.service_name <> "Non Pharmaceuticals" OR service.service_name <> "Medical Services") AND service.service_delete = 0 AND service_charge.service_charge_status = 1 AND  service_charge.service_charge_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;

			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;



		$v_data['close_page'] = 1;
		echo  $this->load->view('patient_bill_view', $v_data, true);
		

	}


	public function update_all_charge()
	{
		$this->db->where('service_charge.service_charge_id = visit_charge.service_charge_id AND service_charge.product_id > 0');
		$this->db->select('service_charge.product_id,visit_charge.visit_charge_id');
		$query = $this->db->get('service_charge,visit_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$visit_charge_id = $value->visit_charge_id;
				$product_id = $value->product_id;

				$array['product_id'] = $product_id;

				$this->db->where('visit_charge_id',$visit_charge_id);
				$this->db->update('visit_charge',$array);
			}
		}
	}
	public function patient_bill($patient_id)
	{

		$v_data = array('patient_id'=>$patient_id);
		
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['visit_types_rs'] = $this->reception_model->get_visit_types();
		$patient = $this->reception_model->get_patient_data($patient_id);
		$patient = $patient->row();
		$patient_othernames = $patient->patient_othernames;
		$patient_surname = $patient->patient_surname;

		$v_data['doctor'] = $this->reception_model->get_providers();

		
		$v_data['title'] = $patient_othernames.' '.$patient_surname;




		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id  AND (service.service_name <> "Pharmacy" OR service.service_name <> "Non Pharmaceuticals" OR service.service_name <> "Medical Services") AND service.service_delete = 0 AND service_charge.service_charge_status = 1 AND  service_charge.service_charge_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;

			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;



		$v_data['close_page'] = 1;
		$data['content'] = $this->load->view('patient_bill', $v_data, true);
		
		$data['title'] = 'Payments';
		$data['sidebar'] = 'accounts_sidebar';
		$this->load->view('admin/templates/general_page', $data);

	}


	public function patient_bill_view($visit_id,$page=NULL)
	{
		// $data = array('visit_id'=>$visit_id);

		if($page == NULL)
		{
			$page = 0;
		}
		$table= 'visit_charge, service_charge, service';
		$where='visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id ';

		$config["per_page"] = $v_data['per_page'] = $per_page = 10;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}

		$v_data['page'] = $page;
		$v_data['visit_id'] = $visit_id;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);



		$v_data['invoice_items'] = $query;

		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND (service.service_name <> "Pharmacy" OR service.service_name <> "Non Pharmaceuticals" OR service.service_name <> "Medical Services") AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;



		$order = 'service.service_name';
		$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

		$table = 'service';
		$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $service_query->result();
		$services_items = '';
		foreach ($rs9 as $rs11) :


			$service_id = $rs11->service_id;
			$service_name = $rs11->service_name;

			$services_items .="<option value='".$service_id."'>".$service_name."</option>";

		endforeach;

		$v_data['services_items'] = $services_items;





		// payments


		// old
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('view_patient_bill',$v_data);

	}


	public function inpatient_bill_view($visit_id)
	{
		// $data = array('visit_id'=>$visit_id);

		if($page == NULL)
		{
			$page = 0;
		}
		$table= 'visit_charge, service_charge, service';
		$where='visit_charge.visit_charge_delete = 0 AND charged = 1 AND visit_charge.visit_id = '.$visit_id.' AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id ';

		$config["per_page"] = $v_data['per_page'] = $per_page = 100000000;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}

		$v_data['page'] = $page;
		$v_data['visit_id'] = $visit_id;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->accounts_model->get_all_visits_invoice_items($table, $where, $config["per_page"], $page);



		$v_data['invoice_items'] = $query;

		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}
		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND (service.service_name <> "Pharmacy" AND  service.service_name <> "Laboratory")  AND service.service_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = '.$visit_t;

		$table = 'service_charge,visit_type,service';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		foreach ($rs9 as $rs10) :


		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;

		$stud = $rs10->service_charge_amount;

		    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

		endforeach;

		$v_data['services_list'] = $procedures;
		$v_data['visit_id'] = $visit_id;






		// payments


		// old
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		echo $this->load->view('view_inpatient_bill',$v_data,true);
		

	}

	public function payments_update()
	{
		$this->db->where('payment_created IS NOT NULL and payment_date IS NULL');
		$query = $this->db->get('payments');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$payment_id = $value->payment_id;
				$visit_id = $value->visit_id;
				$payment_created = $value->payment_created;

				$patient_id = 0;
				if($visit_id > 0)
				{


					$rs = $this->nurse_model->check_visit_type($visit_id);
					if(count($rs)>0){
					  foreach ($rs as $rs1) {
					    # code...
					      $patient_id = $rs1->patient_id;
					  }
					}
				}

				$array['payment_date'] = $payment_created;
				$array['patient_id'] = $patient_id;



				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$array);
			}
		}
	}

	public function update_inpatient_invoice($patient_id,$visit_id=NULL,$visit_invoice_id = null)
	{
		// $patient = $this->reception_model->patient_names2(null,$visit_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		// $data['patient'] = $patient;
		$data['visit_types_rs'] = $this->reception_model->get_visit_types();

		$page=$this->load->view('sidebar/update_inpatient_invoice_details',$data,true);


		$response['result'] = $page;
		$response['message'] = 'success';
		echo json_encode($response);
	}

	public function items()
	{
		// $this->db->where('service_charge_name LIKE '"%?%"'';
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$service_charge_name = $value->service_charge_name;
				$service_charge_id = $value->service_charge_id;

				// $array_update['service_charge_name'] = str_replace('?', '', $service_charge_name);
				// $this->db->where('service_charge_id',$service_charge_id);
				// $this->db->update('service_charge',$array_update);
			}
		}
	}

	public function update_all_invoice_totals()
	{

		$this->db->where('visit_invoice_delete = 0');

		$query = $this->db->get('visit_invoice');

		// var_dump($query);die();
	
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$visit_invoice_id = $value->visit_invoice_id;

				$this->accounts_model->update_invoice_totals($visit_invoice_id);
			}
		}

	}


	public function allocate_to_invoice($patient_id,$visit_id,$visit_charge_id)
	{
		$data['title'] = 'Patient Invoices';
		
		
		$data['patient_id'] = $patient_id;
		$data['visit_charge_id'] = $visit_charge_id;
		$data['visit_id'] = $visit_id;


			
		$page = $this->load->view('sidebar/view_patient_invoices',$data);

		echo $page;
	}

	public function assign_charge_to_invoice($visit_invoice_id,$visit_charge_id,$patient_id)
	{
		$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

		if($visit_invoice_detail->num_rows() > 0)
		{
			foreach ($visit_invoice_detail->result() as $key => $value) {
				# code...
				$visit_invoice_number = $value->visit_invoice_number;
				$preauth_date = date('d/m/Y',strtotime($value->preauth_date));
				$invoice_date = date('d/m/Y',strtotime($value->created));
				$preauth_amount = $value->preauth_amount;
				$visit_id = $value->visit_id;


			}
		}

		$charged_array['visit_invoice_id'] = $visit_invoice_id;
		$charged_array['visit_id'] = $visit_id;
		$this->db->where('visit_charge_id',$visit_charge_id);
		$this->db->update('visit_charge',$charged_array);

		// $this->db->

		$this->accounts_model->update_visit_invoice_data_credit($visit_invoice_id);
	}

	public function update_account_details_to_invoice()
	{
		$this->db->where('patient_account_id = 0 AND patients.patient_id = visit_invoice.patient_id AND visit_type.visit_type_id = visit_invoice.bill_to');
		$this->db->select('visit_invoice.*,patients.patient_surname,patients.patient_othernames,patients.patient_first_name,visit_type.visit_type_name');
		$query = $this->db->get('visit_invoice,patients,visit_type');
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$patient_id = $value->patient_id;
				$bill_to = $value->bill_to;
				$visit_invoice_id = $value->visit_invoice_id;
				$patient_surname = $value->patient_surname;
				$patient_othernames = $value->patient_othernames;
				$patient_first_name = $value->patient_first_name;
				$scheme_name = $value->scheme_name;
				$member_number = $value->member_number;
				$visit_type_name = $value->visit_type_name;

				if(empty($scheme_name))
				{
					$scheme_name = 'Self';
				}


				if(empty($member_number))
				{
					$member_number = 'Self';
				}

				// var_dump($member_number);die();

				$alias_name = $visit_type_name.':'.$patient_first_name.' '.$patient_othernames.' '.$patient_surname;


				$this->db->where('patient_id = '.$patient_id.' AND visit_type_id = '.$bill_to);
				$query_item = $this->db->get('patient_account');

				if($query_item->num_rows() == 1)
				{
					foreach ($query_item->result() as $key => $value_two) {
						// code...
						$patient_account_id = $value_two->patient_account_id;
					}

					// update invoice with the account
					$array['patient_account_id'] = $patient_account_id;

					$this->db->where('visit_invoice_id',$visit_invoice_id);
					$this->db->update('visit_invoice',$array);

				}
				else if($query_item->num_rows() == 0)
				{
					// create the patient account

					$this->db->where('alias_name = "'.$alias_name.'" AND visit_type_id ='.$bill_to);
					$query_new = $this->db->get('patient_account');

					if($query_new->num_rows() == 0)
					{
						
						// insert 
						$array_add['patient_id'] = $patient_id;
						$array_add['alias_name'] = $alias_name;
						$array_add['scheme_name'] = $scheme_name;
						$array_add['member_number'] = $member_number;
						$array_add['visit_type_id'] = $bill_to;

						$this->db->insert('patient_account',$array_add);
						$patient_account_id = $this->db->insert_id();



						$array_update['patient_account_id'] = $patient_account_id;
						$this->db->where('visit_invoice_id',$visit_invoice_id);
						$this->db->update('visit_invoice',$array_update);

					}
					else if($query_new->num_rows() > 0)
					{
						$alias_name = $alias_name.'.';
						$this->db->where('alias_name = "'.$alias_name.'" AND visit_type_id ='.$bill_to);
						$query_new_two = $this->db->get('patient_account');

						if($query_new_two->num_rows() == 0)
						{
							
							// insert 
							$array_add['patient_id'] = $patient_id;
							$array_add['alias_name'] = $alias_name;
							$array_add['scheme_name'] = $scheme_name;
							$array_add['member_number'] = $member_number;
							$array_add['visit_type_id'] = $bill_to;

							$this->db->insert('patient_account',$array_add);
							$patient_account_id = $this->db->insert_id();



							$array_update['patient_account_id'] = $patient_account_id;
							$this->db->where('visit_invoice_id',$visit_invoice_id);
							$this->db->update('visit_invoice',$array_update);
						}
						else if($query_new_two->num_rows() > 0)
						{

							$alias_name = $alias_name.' .';
							$this->db->where('alias_name = "'.$alias_name.'" AND visit_type_id ='.$bill_to);
							$query_new_three = $this->db->get('patient_account');

							if($query_new_three->num_rows() == 0)
							{
								
								// insert 
								$array_add['patient_id'] = $patient_id;
								$array_add['alias_name'] = $alias_name;
								$array_add['scheme_name'] = $scheme_name;
								$array_add['member_number'] = $member_number;
								$array_add['visit_type_id'] = $bill_to;

								$this->db->insert('patient_account',$array_add);
								$patient_account_id = $this->db->insert_id();



								$array_update['patient_account_id'] = $patient_account_id;
								$this->db->where('visit_invoice_id',$visit_invoice_id);
								$this->db->update('visit_invoice',$array_update);
							}
						}

					}

				}
			}
		}


		redirect('reports/view-customers');
	}

	

	public function delete_payments_wrongly_entered()
	{
		$sql = "select * from payments where payment_date = '0000-00-00' AND batch_receipt_id = 0 and payment_method_id = 9";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$payment_id = $value->payment_id;

				$this->db->where('payment_id',$payment_id);
				$this->db->delete('payments');

				$this->db->where('payment_id',$payment_id);
				$this->db->delete('payment_item');
			}
		}
	}

	public function get_patient_details_bill_header($visit_id)
	{

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['going_to'] = $this->accounts_model->get_going_to($visit_id);
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$patient_othernames = $patient['patient_othernames'];
		$patient_surname= $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$account_balance= $patient['account_balance'];
		$visit_type_name= $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$close_card = $patient['close_card'];
		$v_data['inpatient'] = $inpatient = $patient['inpatient'];
		$payments_value = $this->accounts_model->total_payments($visit_id);
		$invoice_total = $this->accounts_model->total_invoice($visit_id);
		$balance = $this->accounts_model->balance($payments_value,$invoice_total);

	
		$visit_discharge = '<a class="btn btn-sm btn-danger pull-right"  onclick="close_visit('.$visit_id.')"  style="margin-top:-25px;" ><i class="fa fa-folder"></i> Send to Account</a>';
		$banner = 'success';
		
		

		$title = '<h2 class="panel-title panel-'.$banner.'"><strong>Visit: </strong>'.$visit_type_name.'.<strong> Total: </strong> Kes '.number_format($account_balance, 2).' <strong> Current: </strong> Kes '.number_format($balance,2).'</h2>
				<div class="pull-right">
					<a class="btn btn-sm btn-info pull-left" style="margin-top:-25px;margin-right:2px;" data-toggle="modal" data-target="#change_patient_type" ><i class="fa fa-pencil"></i> Edit Type</a>     
					<a href="'.site_url().'accounts/print_inpatient_invoice_new/'.$visit_id.'/1" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-top:-25px;margin-right:2px;" ><i class="fa fa-print"></i> All Invoice</a>

					'.$visit_discharge.'
					<a href="'.site_url().'accounts/print_inpatient_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px; margin-right:2px;" ><i class="fa fa-print"></i> Current Invoice</a>
				</div>';

		echo $title;
	}


	public function view_visit_statement($patient_id,$visit_id)
	{


		$data['title'] = 'Patient Visit Billings';
		
		$data['patient_id'] = $patient_id;
		$data['visit_id'] = $visit_id;


		$page = $this->load->view('patients_views/view_visit_statement',$data);

		echo $page;
	}



	public function get_all_doctors_summary_notes($patient_id,$visit_invoice_id = null,$visit_id=NULL)
	{
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$page=$this->load->view('sidebar/get_all_doctors_summary',$data,true);

		echo $page;
	}


	public function update_invoice_doctors_notes($patient_id,$visit_id=NULL,$visit_invoice_id = null)
	{
		// $patient = $this->reception_model->patient_names2(null,$visit_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		// $data['patient'] = $patient;
		$data['visit_types_rs'] = $this->reception_model->get_visit_types();

		$page=$this->load->view('sidebar/doctors_notes_details',$data,true);


		$response['result'] = $page;
		$response['message'] = 'success';
		echo json_encode($response);
	}



	public function get_all_notes_trail($patient_id,$visit_id=NULL,$visit_invoice_id = null)
	{
		// $patient = $this->reception_model->patient_names2(null,$visit_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		// $data['patient'] = $patient;
		$data['visit_types_rs'] = $this->reception_model->get_visit_types();

		$page=$this->load->view('sidebar/doctors_notes_trail',$data,true);


		$response['result'] = $page;
		$response['message'] = 'success';
		echo json_encode($response);
	}


	public function confirm_doctors_notes_add()
	{

		$this->form_validation->set_rules('doctors_notes', 'Visit', 'required');
		$this->form_validation->set_rules('billing_visit_id', 'Invoice', 'required');
		$this->form_validation->set_rules('billing_patient_id', 'Invoice', 'required');
		$this->form_validation->set_rules('visit_invoice_id', 'Invoice', 'required');

		if ($this->form_validation->run())
		{

			$doctors_notes = $this->input->post('doctors_notes');
			$billing_visit_id = $this->input->post('billing_visit_id');
			$billing_patient_id = $this->input->post('billing_patient_id');
			$visit_invoice_id = $this->input->post('visit_invoice_id');


			// update waiver
			$notes_array['doctors_notes'] = $doctors_notes;
			$this->db->where('visit_invoice_id = '.$visit_invoice_id);
			$this->db->update('visit_invoice',$notes_array);



			$notes_new_array['doctors_notes'] = $doctors_notes;
			$notes_new_array['created_by'] = $this->session->userdata('personnel_id');
			$notes_new_array['created_on'] = date('Y-m-d H:i:s');
			$notes_new_array['visit_invoice_id'] = $visit_invoice_id;
			$this->db->insert('visit_invoice_notes',$notes_new_array);

			$response['message'] ='success';


		}
		else
		{
			$response['message'] ='fail';
			$response['result'] =validation_errors();
		}
	// }
		echo json_encode($response);
	
	}



	public function edit_charge_invoice($patient_id,$visit_id,$visit_charge_id)
	{
		$data['title'] = 'Invoice Charged';


		
	

		$this->db->where('service_charge.service_charge_id = visit_charge.service_charge_id AND visit_charge.visit_charge_id ='.$visit_charge_id);
		$this->db->select('*,visit_charge.provider_id AS doctor_id');
		$query = $this->db->get('visit_charge,service_charge');

		$data['doctor'] = $this->reception_model->get_doctor();

		
		$data['patient_id'] = $patient_id;
		$data['visit_charge_id'] = $visit_charge_id;
		$data['visit_id'] = $visit_id;
		$data['query'] = $query;


			
		$page = $this->load->view('sidebar/edit_invoice_charge',$data);

		echo $page;
	}


	public function update_accounts_billing()
	{
		$visit_id = $this->input->post('visit_id');
		$patient_id = $this->input->post('patient_id');
		$visit_charge_id = $this->input->post('visit_charge_id');
		$provider_id = $this->input->post('provider_id');
		$amount = $this->input->post('visit_charge_amount');
		$service_charge_id = $this->input->post('service_charge_id');
		$service_charge_name = $this->input->post('service_charge_name');
		$units = $this->input->post('units_charged');
		$reason = $this->input->post('reason');
		$date = $this->input->post('date');


		
		$visit_data = array('visit_charge_units'=>$units,'reason'=>$reason,'date'=>$date,'visit_charge_amount'=>$amount, 'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"),'time'=>date("H:i:s"),'visit_charge_comment'=>'','patient_id'=>$patient_id,'provider_id'=>$provider_id);
		$this->db->where(array("visit_charge_id"=>$visit_charge_id));
		$this->db->update('visit_charge', $visit_data);

		$response['status'] = "success";
		$response['message'] = "You have successfully updated the charge";

		$patient_name = $this->site_model->get_patient_name_by_id($patient_id);

		$array = array(
					'title'=>'CHARGE UPDATED',
					'visit_id'=>$visit_id,
					'patient_id'=>$patient_id,
					'table_updated'=>'visit_charge',
					'personnel_id'=>$this->session->userdata('personnel_id'),
					'personnel_name'=>$this->session->userdata('personnel_fname').' '.$this->session->userdata('personnel_onames'),
					'name'=> ucwords(strtolower($service_charge_name)),
					'datetime'=>date('Y-m-d H:i:s'),
					'foreign_key'=>$visit_charge_id,
					'patient_name'=>ucwords(strtolower($patient_name)),
					'description'=> 'CHARGE UPDATED '.ucwords(strtolower($service_charge_name)).' Amount :'.$amount.'  Units : '.$units,
					'details'=>json_encode($visit_data)
					);

		$this->site_model->update_trail($array);



		
		echo json_encode($response);
	}

	public function update_waiver_details($patient_id,$visit_id=NULL,$visit_invoice_id = null)
	{
		// $patient = $this->reception_model->patient_names2(null,$visit_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		// $data['patient'] = $patient;
		$data['visit_types_rs'] = $this->reception_model->get_visit_types();

		$page=$this->load->view('sidebar/update_inpatient_waiver_details',$data,true);


		$response['result'] = $page;
		$response['message'] = 'success';
		echo json_encode($response);
	}
	public function get_waiver_details($patient_id,$visit_id=NULL,$visit_invoice_id = null)
	{
		// $patient = $this->reception_model->patient_names2(null,$visit_id);
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['visit_invoice_id'] = $visit_invoice_id;
		// $data['patient'] = $patient;
		$data['visit_types_rs'] = $this->reception_model->get_visit_types();

		$page=$this->load->view('sidebar/discounts_page',$data,true);


		$response['result'] = $page;
		$response['message'] = 'success';
		echo json_encode($response);
	}
	public function update_waiver_amount()
	{
		$this->form_validation->set_rules('discount_given', 'Discount', 'required');
		$this->form_validation->set_rules('visit_id', 'Visit', 'required');
		$this->form_validation->set_rules('visit_invoice_id', 'Invoice', 'required');
		$this->form_validation->set_rules('patient_id', 'Patient', 'required');

		if ($this->form_validation->run())
		{


			$discount_given = $this->input->post('discount_given');
			$visit_id = $this->input->post('visit_id');
			$visit_invoice_id = $this->input->post('visit_invoice_id');
			$patient_id = $this->input->post('patient_id');
			$service_id = $this->input->post('service_id');

			$this->db->where('visit_id = '.$visit_id.' AND patient_id = '.$patient_id.' AND service_id = '.$service_id.' AND visit_invoice_id = '.$visit_invoice_id);
			$query_discounts = $this->db->get('visit_invoice_discounts');


			if($query_discounts->num_rows() > 0)
			{
				$update_array['visit_invoice_discount'] = $discount_given;
				$update_array['visit_credit_note_id'] = 0;

				$this->db->where('visit_id = '.$visit_id.' AND patient_id = '.$patient_id.' AND service_id = '.$service_id.' AND visit_invoice_id = '.$visit_invoice_id);
				$this->db->update('visit_invoice_discounts',$update_array);



	        	$patient_name = $this->site_model->get_patient_name_by_id($patient_id);

		        $array1 = array(
						'title'=>'DISCOUNT UPDATED',
						'visit_id'=>$visit_id,
						'patient_id'=>$patient_id,
						'table_updated'=>'visit_invoice_discount',
						'personnel_id'=>$this->session->userdata('personnel_id'),
						'personnel_name'=>$this->session->userdata('personnel_fname').' '.$this->session->userdata('personnel_onames'),
						'name'=> ucwords(strtolower('DISCOUNTS')),
						'datetime'=>date('Y-m-d H:i:s'),
						'foreign_key'=>$visit_invoice_id,
						'patient_name'=>ucwords(strtolower($patient_name)),
						'description'=> 'DISCOUNTS UPDATED',
						'details'=>json_encode($update_array)
						);

		      	$this->site_model->update_trail($array1);
		      	$response['message'] ='success';
			}
			else
			{

				$insert_array['visit_invoice_discount'] = $discount_given;
				$insert_array['visit_credit_note_id'] = 0;
				$insert_array['service_id'] = $service_id;
				$insert_array['visit_id'] = $visit_id;
				$insert_array['patient_id'] = $patient_id;
				$insert_array['visit_invoice_id'] = $visit_invoice_id;
				$insert_array['created_by'] = $this->session->userdata('personnel_id');
				$insert_array['created_on'] = date('Y-m-d H:i:s');

				
				$this->db->insert('visit_invoice_discounts',$insert_array);


				$patient_name = $this->site_model->get_patient_name_by_id($patient_id);

		        $array1 = array(
						'title'=>'DISCOUNT ADDED',
						'visit_id'=>$visit_id,
						'patient_id'=>$patient_id,
						'table_updated'=>'visit_invoice_discount',
						'personnel_id'=>$this->session->userdata('personnel_id'),
						'personnel_name'=>$this->session->userdata('personnel_fname').' '.$this->session->userdata('personnel_onames'),
						'name'=> ucwords(strtolower('DISCOUNTS')),
						'datetime'=>date('Y-m-d H:i:s'),
						'foreign_key'=>$visit_invoice_id,
						'patient_name'=>ucwords(strtolower($patient_name)),
						'description'=> 'DISCOUNTS ADDED',
						'details'=>json_encode($insert_array)
						);

		      	$this->site_model->update_trail($array1);

		      	$response['message'] ='success';

			}

		}
		else
		{
			$response['message'] ='fail';
			$response['result'] =validation_errors();
		}
	// }
		echo json_encode($response);
	}

	public function confirm_waiver()
	{

		$this->form_validation->set_rules('discount_date', 'Discount', 'required');
		$this->form_validation->set_rules('discount_note', 'Visit', 'required');
		$this->form_validation->set_rules('billing_visit_id', 'Invoice', 'required');
		$this->form_validation->set_rules('billing_patient_id', 'Invoice', 'required');
		$this->form_validation->set_rules('visit_invoice_id', 'Invoice', 'required');
		$this->form_validation->set_rules('total_waiver', 'Invoice', 'required');

		if ($this->form_validation->run())
		{

			$discount_date = $this->input->post('discount_date');
			$discount_note = $this->input->post('discount_note');
			$billing_visit_id = $this->input->post('billing_visit_id');
			$billing_patient_id = $this->input->post('billing_patient_id');
			$visit_invoice_id = $this->input->post('visit_invoice_id');
			$total_waiver = $this->input->post('total_waiver');


			// update waiver

			$this->db->where('patient_id = '.$billing_patient_id.' AND visit_invoice_id = '.$visit_invoice_id.' AND visit_id = '.$billing_visit_id);
			$query = $this->db->get('visit_credit_note');


			if($query->num_rows() > 0)
			{

				foreach ($query->result() as $key => $value) {
					// code...
					$visit_credit_note_id = $value->visit_credit_note_id;
				}
				
				
				$insertarray['visit_cr_note_amount'] = $total_waiver;
				$insertarray['created'] = $discount_date;
				$this->db->where('visit_credit_note_id',$visit_credit_note_id);
				$this->db->update('visit_credit_note', $insertarray);

				$visit_data = array(
										'visit_cr_note_amount'=>$total_waiver,
										'visit_cr_note_units'=>1,
										'visit_cr_note_comment'=>$discount_note,
										'date'=>$discount_date,
									);
				$this->db->where('visit_credit_note_id',$visit_credit_note_id);
				$this->db->update('visit_credit_note_item', $visit_data);
				
				$array_update['visit_credit_note_id'] = $visit_credit_note_id;
		      	$this->db->where('visit_id = '.$billing_visit_id.' AND patient_id = '.$billing_patient_id.' AND visit_invoice_id = '.$visit_invoice_id.' ');
		      	$this->db->update('visit_invoice_discounts',$array_update);

				$this->accounts_model->update_visit_invoice_data_credit($visit_invoice_id);

				$response['message'] ='success';
		
			}
			else
			{
				
				$document_number = $this->accounts_model->create_visit_credit_note_number();
				
					  
		      	$insertarray['visit_cr_note_number'] = $document_number;
		     
			    if(empty($total_waiver))
		    	{
		    		$total_waiver = 0;
		    	}

				$total_waiver = str_replace(",", "", $total_waiver);
				$total_waiver = str_replace(".00", "", $total_waiver);
				$insertarray['visit_cr_note_amount'] = $total_waiver;
				$insertarray['visit_id'] = $billing_visit_id;
				$insertarray['patient_id'] = $billing_patient_id;
				$insertarray['created'] = $discount_date;
				$insertarray['visit_invoice_id'] = $visit_invoice_id;
				
				$this->db->insert('visit_credit_note', $insertarray);
				$visit_credit_note_id = $this->db->insert_id();

				$visit_data = array(
									'service_charge_id'=>12204,
									'visit_id'=>$billing_visit_id,
									'patient_id'=>$billing_patient_id,
									'visit_cr_note_amount'=>$total_waiver,
									'visit_cr_note_units'=>1,
									'created_by'=>$this->session->userdata("personnel_id"),
									'date'=>$discount_date,
									'visit_cr_note_comment'=>$discount_note,
									'visit_credit_note_id'=>$visit_credit_note_id
									);

				$this->db->insert('visit_credit_note_item', $visit_data);

				$patient_name = $this->site_model->get_patient_name_by_id($billing_patient_id);

		        $array1 = array(
						'title'=>'WAIVER COMPLETED',
						'visit_id'=>$billing_visit_id,
						'patient_id'=>$billing_patient_id,
						'table_updated'=>'visit_credit_note',
						'personnel_id'=>$this->session->userdata('personnel_id'),
						'personnel_name'=>$this->session->userdata('personnel_fname').' '.$this->session->userdata('personnel_onames'),
						'name'=> ucwords(strtolower('WAIVER COMPLETED')),
						'datetime'=>date('Y-m-d H:i:s'),
						'foreign_key'=>$visit_invoice_id,
						'patient_name'=>ucwords(strtolower($patient_name)),
						'description'=> 'DISCOUNTS COMPLETED '.$total_waiver,
						'details'=>json_encode($visit_data)
						);

		      	$this->site_model->update_trail($array1);

		      	$array_update['visit_credit_note_id'] = $visit_credit_note_id;
		      	$this->db->where('visit_id = '.$billing_visit_id.' AND patient_id = '.$billing_patient_id.' AND visit_invoice_id = '.$visit_invoice_id.' ');
		      	$this->db->update('visit_invoice_discounts',$array_update);

		      	$this->accounts_model->update_visit_invoice_data_credit($visit_invoice_id);


				$response['message'] ='success';
			}


		}
		else
		{
			$response['message'] ='fail';
			$response['result'] =validation_errors();
		}
	// }
		echo json_encode($response);
	
	}

	public function get_visit_invoices_inpatient($visit_id,$patient_id,$visit_invoice_id = NULL,$visit_date=NULL)
	{

		$data['title'] = 'Visit Invoices';
		
		$creditor_where = 'visit_invoice.visit_id = visit.visit_id AND visit_invoice.visit_invoice_delete = 0 AND visit.visit_delete = 0  AND visit_invoice.patient_id = '.$patient_id;

		if(!empty($visit_invoice_id))
		{
			$creditor_where .= ' AND visit_invoice.visit_invoice_id = '.$visit_invoice_id;
		}

		$creditor_table = 'visit,visit_invoice';

		$this->db->where($creditor_where);
		$creditor_query = $this->db->get($creditor_table);

		$data['visit_notes'] = $creditor_query;
		$data['patient_id'] = $patient_id;
		$data['visit_id'] = $visit_id;
		$data['visit_invoice_id'] = $visit_invoice_id;

		$data['billing_date'] = $visit_date;

		$this->db->where('visit_id',$visit_id);
		$this->db->select('visit.*');
		$query = $this->db->get('visit');
		$data['status'] = 1;
		$data['query'] = $query;

		// var_dump($data);die();
		$data['doctor'] = $this->reception_model->get_doctor();
		$page = $this->load->view('sidebar/inpatient_invoices',$data);

		echo $page;


	}
		public function search_inpatient_procedures($patient_id,$visit_id,$visit_type_id = NULL,$visit_invoice_id =NULL,$view=NULL)
	{

		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}

		$symptoms_search = $this->input->post('query');
		$query = null;
		if(!empty($symptoms_search))
		{
			// AND (service_charge.product_id = 0 OR service_charge.product_id IS NULL)
			// AND service.service_name <> "Pharmacy" 
			$lab_test_where = 'service_charge.service_charge_delete = 0 AND service.service_id = service_charge.service_id  AND service_charge.visit_type_id = 1 ';
			$lab_test_table = 'service_charge,service';
			$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$symptoms_search.'%\' OR service_charge.service_charge_code LIKE \'%'.$symptoms_search.'%\')';
			
			$this->db->where($lab_test_where);
			$this->db->limit(100);
			$query = $this->db->get($lab_test_table);

			if($query->num_rows() == 0)
			{
				// AND service.service_name <> "Pharmacy"
				$lab_test_where = 'service_charge.service_charge_delete = 0 AND service.service_id = service_charge.service_id AND service_charge.visit_type_id = 1';
				$lab_test_table = 'service_charge,service';
				$lab_test_where .= ' AND (service_charge.service_charge_name LIKE \'%'.$symptoms_search.'%\' OR service_charge.service_charge_code LIKE \'%'.$symptoms_search.'%\')';
				
				
				$this->db->where($lab_test_where);
				$this->db->limit(100);
				$query = $this->db->get($lab_test_table);
			}

		}
		$data['query'] = $query;
		$data['visit_id'] = $visit_id;
		$data['patient_id'] = $patient_id;
		$data['item_view'] = $view;
		$data['visit_type_id'] = $visit_t;
		$data['visit_invoice_id'] = $visit_invoice_id;
		$page = $this->load->view('sidebar/search_inpatient_service_charge',$data);

		echo $page;
	}

	public function get_service_charge_details($service_charge_id)
	{

		$this->db->where('service_charge_id',$service_charge_id);
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$service_charge_name = $value->service_charge_name;
				$service_charge_amount = $value->service_charge_amount;
				$insurance_price = $value->insurance_price;
			}
		}



		$items['service_charge_name'] = $service_charge_name;
		$items['service_charge_amount'] = $service_charge_amount;
		$items['insurance_price'] = $insurance_price;


		echo json_encode($items);
	}

	public function add_inpatient_bill($visit_id,$visit_invoice_id=null)
	{
		
			$service_charge_id = $this->input->post('service_charge_id');
			$provider_id = $this->input->post('provider_id');
			$visit_date = $this->input->post('visit_date');
			$units_charged = $this->input->post('units_charged');

			$this->db->where('service_charge_id',$service_charge_id);
			$query = $this->db->get('service_charge');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...
					$service_charge_name = $value->service_charge_name;
					$service_charge_amount = $value->service_charge_amount;
					$insurance_price = $value->insurance_price;
					$product_id = $value->product_id;
				}
			}

			$rs = $this->nurse_model->check_visit_type($visit_id);
			if(count($rs)>0){
			  foreach ($rs as $rs1) {
			    # code...
			      $visit_t = $rs1->visit_type;
			      $branch_id = $rs1->branch_id;
			  }
			}


			
			$amount = $service_charge_amount;
			
			if($product_id > 0)
			{
				$store_id = 5;
			}
			else
			{
				$store_id = 0;
			}
			


			$visit_data = array('visit_charge_units'=>$units_charged,'visit_id'=>$visit_id,'visit_charge_amount'=>$amount,'visit_charge_qty'=>1,'service_charge_id'=>$service_charge_id,'charged'=>1, 'created_by'=>$this->session->userdata("personnel_id"),'provider_id'=>$provider_id,'date'=>$visit_date,'time'=>date('H:i:s'),'personnel_id'=>$procedure_id,'store_id'=>$store_id,'visit_invoice_id'=>$visit_invoice_id,'product_id'=>$product_id);

			if($this->db->insert('visit_charge', $visit_data))
			{

				$response['status'] = "success";
				$response['message'] = 'Charge successfully added to visit';
			}
			else
			{
				$response['status'] = "fail";
				$response['message'] = 'Sorry this visit charge could not be added';
			}


	


		echo json_encode($response);
	}

	public function cancel_charge($visit_charge_id, $visit_id)
	{
		$this->form_validation->set_rules('remove_description', 'Description', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$array['charged'] = 0;
			$array['visit_charge_delete']  = 1;
			$array['delete_comment'] = $this->input->post('remove_description');
			$array['deleted_by'] = $this->session->userdata('personnel_id');


		    $this->db->where('visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_charge_id ='.$visit_charge_id);
		    $query = $this->db->get('visit_charge,service_charge');
		    
		    $service_charge = 0;
		    
		    if($query->num_rows() > 0)
		    {
		        $row = $query->row();
		        $service_charge_name = $row->service_charge_name;
		       
		    }

			$this->db->where('visit_charge_id',$visit_charge_id);
			if($this->db->update('visit_charge',$array))
			{
		     $patient_name = $this->site_model->get_patient_name_by_id($patient_id);

            $array_trail = array(
            'title'=>'CHARGE REMOVED',
            'visit_id'=>$visit_id,
            'patient_id'=>$patient_id,
            'table_updated'=>'visit_charge',
            'personnel_id'=>$this->session->userdata('personnel_id'),
            'personnel_name'=>$this->session->userdata('personnel_fname').' '.$this->session->userdata('personnel_onames'),
            'name'=> ucwords(strtolower($service_charge_name)),
            'datetime'=>date('Y-m-d H:i:s'),
            'foreign_key'=>$visit_charge_id,
            'patient_name'=>ucwords(strtolower($patient_name)),
            'description'=> 'CHARGE REMOVED '.ucwords(strtolower($service_charge_name)),
            'details'=>json_encode($array)
            );

            $this->site_model->update_trail($array_trail);
				$response['status'] = 'success';
				$response['message'] = 'You have successfully removed the charge';
			}
			else
			{
				$response['status'] = 'fail';
				$response['message'] = 'Please try again';
			}


		}
		else
		{
			
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}
		echo json_encode($response);
	}

	public function add_new_invoice($patient_id,$visit_id)
	{
		$data['title'] = 'Add Visit Invoice';

		$data['patient_id'] = $patient_id;
		$data['visit_id'] = $visit_id;

		$data['visit_types'] = $this->reception_model->get_visit_types();

			
		$page = $this->load->view('sidebar/add_invoice',$data);

		echo $page;
	}
	public function create_new_invoice()
	{

		
		$this->form_validation->set_rules('visit_type_id', 'Visit type', 'required|is_natural_no_zero');

		$visit_type_id = $this->input->post('visit_type_id');
		$patient_id = $this->input->post('patient_id');
		$visit_id = $this->input->post('visit_id');
		$doctor_id = $this->input->post('doctor_id');
		$insurance_number = $this->input->post('insurance_number');
		$scheme_name = $this->input->post('scheme_name');
		$principal_member = $this->input->post('principal_member');
		$relation = $this->input->post('relation');

		if($visit_type_id != 1)
		{
			$this->form_validation->set_rules('scheme_name', 'Scheme Name', 'required|trim|xss_clean');
			$this->form_validation->set_rules('insurance_number', 'Insurance Number', 'required|trim|xss_clean');
		}

		if ($this->form_validation->run())
		{

			$prefix = $this->accounts_model->create_invoice_number();
		
		  	$invoice_number = $prefix;

			$array_invoice['visit_id'] = $visit_id;
			$array_invoice['visit_invoice_number'] = $invoice_number;
			$array_invoice['patient_id'] = $patient_id;
			$array_invoice['created'] = date('Y-m-d');
			$array_invoice['dentist_id'] = $doctor_id;
			$array_invoice['member_number'] = $insurance_number;
			$array_invoice['scheme_name'] = $scheme_name;
			$array_invoice['principal_member'] = $principal_member;
			$array_invoice['relation'] = $relation;
			$array_invoice['bill_to'] = $visit_type_id;
			$array_invoice['open_status'] = 0;
			$array_invoice['preauth_date'] = date('Y-m-d');

		
			$this->db->insert('visit_invoice',$array_invoice);
			$response['result'] ='success';
			$response['message'] ='success';
		}
		else
		{
			$response['message'] ='fail';
			$response['result'] =validation_errors();
		}
	// }
		echo json_encode($response);
	}





}
?>
