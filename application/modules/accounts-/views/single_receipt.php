<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$visit_id = $patient['visit_id'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$paid_before = $this->accounts_model->get_payment_before($payment_id,$visit_id);
$paid_current = $this->accounts_model->get_payment_current($payment_id,$visit_id);
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
$credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
$debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);
?>

<!DOCTYPE html>
<html lang="en">

   <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .col-md-6 {
                width: 50%;
             }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
            {
                 padding: 4px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            body.receipt .sheet { width: 80mm !important;letter-spacing:1px; } /* change height as you like */
            @media print { body.receipt { width: 80mm !important;letter-spacing:1px;} } /* this line is needed for fixing Chrome's bug */
        </style>
    </head>
    <body class="receipt">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 center-align receipt_bottom_border" style="margin-bottom: 20px; ">
                <strong>
                    <?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?><br/>
                </strong>
            </div>
        </div>
        
        <div class="row" >
            <div class="col-md-12 center-align">
                <h2><strong>RECEIPT</strong></h2>
            </div>
        </div>
     
        
        <!-- Patient Details -->
        <div class="row receipt_bottom_border" style="margin-bottom: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">
            <div class="col-md-12">
                <div class="pull-left " >
                    <div class="row">
                        <div class="col-md-12">
                            <div >RECEIPT#: <strong><?php echo 'OTC-INV-00'.$payment_id; ?></strong> </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div >PATIENT: <strong><?php echo $patient_surname.' '.$patient_othernames; ?></strong> </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div >DATE SEEN: <strong><?php echo $visit_date; ?></strong> </div> 
                        </div>
                    </div>
                   
                </div>
            </div>
            
        </div>
        
        
        <div class="row receipt_bottom_border" style="margin-bottom: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped">
                  <tbody>
                    
                    <?php
                     $service_rs = $this->accounts_model->get_patient_visit_charge($visit_id);
                    $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items($visit_id);
                    $total = 0;
                    $s=0;
                    
                    if(count($service_rs) > 0)
                    {
                        foreach($service_rs as $serv)
                        {
                            $service_id = $serv->service_id;
                            $service_name = $serv->service_name;
                            $visit_total = 0;
                            
                            if(count($item_invoiced_rs) > 0){
                              
                              foreach ($item_invoiced_rs as $key_items):
                                $service_id2 = $key_items->service_id;
                                $service_charge_id = $key_items->service_charge_id;
                                $service_charge_name = $key_items->service_charge_name;
                                
                                if($service_id2 == $service_id)
                                {
                                    if($service_id == 4)
                                    {
                                        if($this->accounts_model->in_pres($service_charge_id, $visit_id))
                                        {
                                            $visit_charge_amount = $key_items->visit_charge_amount;
                                            $units = $key_items->visit_charge_units;
                                            $s++;
                                            ?>
                                              <tr >
                                                <td><?php echo $s;?></td>
                                                <td><?php echo $service_charge_name;?></td>
                                                <td>Ksh. <?php echo number_format($visit_charge_amount*$units,2);?></td>
                                              </tr>
                                            <?php

                                            $visit_total += $visit_charge_amount * $units;
                                        }
                                    }
                                    
                                    else
                                    {
                                        $visit_charge_amount = $key_items->visit_charge_amount;
                                        $units = $key_items->visit_charge_units;
                
                                         $s++;
                                         ?>
                                          <tr >
                                            <td><?php echo $s;?></td>
                                            <td><?php echo $service_charge_name;?></td>
                                            <td>Ksh. <?php echo number_format($visit_charge_amount*$units,2);?></td>
                                          </tr>
                                        <?php

                                        $visit_total += $visit_charge_amount * $units;
                                    }
                                }
                              endforeach;
                            }
                         
                            
                            // end of the payments

                            $total = $total + $visit_total;
                        }
                        // enterring the payment stuff
                        $payments_rs = $this->accounts_model->payments($visit_id);
                        $total_payments = 0;
                        $total_amount = ($total + $debit_note_amount) - $credit_note_amount - $total_payments;
                        $balance = $this->accounts_model->balance($payments_value,$invoice_total);
                        
                        if(count($payments_rs) > 0){
                            $x = $s;
                            foreach ($payments_rs as $key_items):
                                
                                $payment_method = $key_items->payment_method;
                                $amount_paid = $key_items->amount_paid;
                                $time = $key_items->time;
                                $payment_type = $key_items->payment_type;
                                $amount_paidd = number_format($amount_paid,2);
                                $payment_service_id = $key_items->payment_service_id;

                                if($payment_service_id > 0)
                                {
                                    $service_associate = $this->accounts_model->get_service_detail($payment_service_id);
                                }
                                else
                                {
                                    $service_associate = " ";
                                }

                                if($payment_type == 2)
                                {
                                    $amount_paidd = $amount_paidd;
                                    $x++;
                                    ?>
                                    <tr>
                                    <td><?php echo $x;?></td>
                                    <td><?php echo $service_associate;?></td>

                                    <td>Ksh. <?php echo $amount_paidd;?></td>
                                    </tr>
                                    <?php
                                }
                                else if($payment_type == 3)
                                {
                                    $amount_paidd = $amount_paidd;
                                    $x++;
                                    ?>
                                    <tr>
                                    <td><?php echo $x;?></td>
                                    <td><?php echo $service_associate;?></td>

                                    <td>Ksh. (<?php echo $amount_paidd;?>)</td>
                                    </tr>
                                    <?php
                                }
                            endforeach;
                        }
                        $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                        ?>
                        <tr>
                          <td colspan="2">TOTAL :</td>
                          <td> Ksh <?php echo number_format($total_amount,2);?></td>
                        </tr>
                        <?php
                    }
                    else{
                       ?>
                        <tr>
                          <td colspan="3"> No Charges</td>
                        </tr>
                        <?php
                    }
                    ?>
                     <tr>
                      <td colspan="3"><strong> PAYMENTS </strong> </td>
                    </tr>
                    <?php

                    $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                    $payments_rs = $this->accounts_model->payments($visit_id);
                    $total_payments = 0;
                    
                    if(count($payments_rs) > 0)
                    {
                        $x=0;
                        
                        foreach ($payments_rs as $key_items):
                            $x++;
                            $payment_type = $key_items->payment_type;
                            $payment_status = $key_items->payment_status;
                            
                            if($payment_type == 1 && $payment_status == 1)
                            {
                                $payment_id = $key_items->payment_id;
                                $amount_paid = $key_items->amount_paid;
                            
                                $total_payments = $total_payments + $amount_paid;
                                
                                if($payment_id == $receipt_payment_id)
                                {
                                    $payment_method = $key_items->payment_method;
                                    $transaction_code = $key_items->transaction_code;
                                    $payment_service_id = $key_items->payment_service_id;
                                    $service_name = $payment_method.' '.$transaction_code;
                                    
                                    if(count($service_rs) > 0)
                                    {
                                        foreach($service_rs as $serv)
                                        {
                                            $service_id = $serv->service_id;
                                            if($payment_service_id == $service_id)
                                            {
                                                $service_name = $serv->service_name;
                                                break;
                                            }
                                        }
                                    }
                                                    
                                    //display DN & CN services
                                    if((count($payments_rs) > 0) && ($service_name == ''))
                                    {
                                        foreach ($payments_rs as $key_items):
                                            $payment_type = $key_items->payment_type;
                                            
                                            if(($payment_type == 2) || ($payment_type == 3))
                                            {
                                                $payment_service_id2 = $key_items->payment_service_id;
                                                
                                                if($payment_service_id2 == $payment_service_id)
                                                {
                                                    $service_name = $this->accounts_model->get_service_detail($payment_service_id);
                                                    break;
                                                }
                                            }
                                            
                                        endforeach;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2"> <?php echo strtoupper($payment_method);?> <?php echo strtoupper($transaction_code);?></td>
                                        <td>Ksh. <?php echo number_format($amount_paid, 2);?></td>
                                    </tr>
                                    <?php
                                }
                            }


                        endforeach;
                        
                    }
                    $invoice_total = $this->accounts_model->total_invoice($visit_id);

                    ?>

                    <tr class="receipt_bottom_border" style="margin-bottom: 20px; ">


                        <td colspan="2">BALANCE</td>
                        <td> Ksh. <?php echo number_format($invoice_total - $paid_current - $paid_before, 2);?></td>
                    </tr>
                      
                  </tbody>
                </table>
            </div>
        </div>
        
        <div class="" style="font-style:italic; ">
            <div style="float:left; margin:0 10px 0 10px;">
                Served by: <?php echo $served_by; ?><br>
                <?php echo $today; ?> Thank you
            </div>
        </div>
    </body>
    
</html>