<?php
$patient_items = '';
$visit_type_id = null;
$close_card = 3;
if($query->num_rows() > 0)
{
    foreach ($query->result() as $key => $res) 
    {
        # code...

        $visit_date = date('D M d Y',strtotime($res->visit_date)); 
        $insurance_number = $res->insurance_number;
        $insurance_description = $res->insurance_description;
        $insurance_limit = $res->insurance_limit;
        $visit_type = $res->visit_type;
        $patient_id = $res->patient_id;
        $visit_id = $res->visit_id;
        $visit_type_id = $res->visit_type;
         $close_card = $res->close_card;
        

    }

}
?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
           <div class="col-md-12">
           		<div class="col-md-7">
           			<div class="col-md-12" style="height: 75vh; overflow-y: scroll;">
	           			<div class="row" id="invoice-div" style="padding:10px;">
			                <input type="text" name="search_procedures" id="search_procedures" class="form-control" onkeyup="get_procedures_done(<?php echo $patient_id;?>,<?php echo $visit_id;?>,<?php echo $visit_type_id;?>,<?php echo $visit_invoice_id;?>)" placeholder="Search procedures.... root canal,extraction">
			                    
			            </div>
			            <input type="hidden" name="visit_type_id" id="visit_type_id" value="<?php echo $visit_type_id?>">
			             <!-- <input type="hidden" name="visit_invoice" id="visit_invoice" value="<?php echo $visit_invoice_id?>"> -->
			            <div class="col-md-12" style="padding:10px;" id="charges-div">
			                    <ul  id="searched-procedures" style="list-style: none;">

			                    </ul>
			            </div>
			        </div>
           	
          		 </div>
          		 <div class="col-md-5">
          		 	<div class="col-md-12" style="height: 75vh; overflow-y: scroll;">
		                  <div id="items-div" style="display: none;">
		                  		<?php echo form_open("accounts/add_service_item", array("class" => "form-horizontal","id"=>"add-bill-inpatient"));?>
			                  		<h4 id="service-name"></h4>
			                  		<!-- <input type="hidden" name=""> -->
			                  		<br>
			                  		<div class="form-group">
										<label class="col-md-4 control-label">Units:</label>
										
										<div class="col-md-8">
							                <input type="number" class="form-control" name="units_charged" id="units_charged" value="1" required>
										</div>
									</div>
			                  		<div class="form-group">
										<label class="col-lg-4 control-label">Consultant: </label>
										
										<div class="col-lg-8">
											 <select name="personnel_id" id="personnel_id" class="form-control">
											 
												<option value="0">----Select a Consultant----</option>
												<?php
																		
													if(count($doctor) > 0){
														foreach($doctor as $row):
															$fname = $row->personnel_fname;
															$onames = $row->personnel_onames;
															$personnel_id = $row->personnel_id;
															
															if($personnel_id == set_value('personnel_id'))
															{
																echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
															}
															
															else
															{
																echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
															}
														endforeach;
													}
												?>
											</select>
										</div>
									</div>
			                  		<div class="form-group">
						                <label class="col-md-4 control-label">Date: </label>
						                <div class="col-md-8">
						                	<div class="input-group">
							                	<span class="input-group-addon">
							                        <i class="fa fa-calendar"></i>
							                    </span>
							                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" id="date_to_invoice" name="date_to_invoice" placeholder="date" value="<?php echo date('Y-m-d');?>">
							                </div>
						                </div>
						                
						            </div>
						           <div class="center-align" style="margin-top:10px;">
										<input type="submit" value="Add charge to bill" class="btn btn-info btn-sm" />
									</div>


						        <?php echo form_close();?>

		                  </div>
		            </div>
           	
          		 </div>
           </div>
            
              
           
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">
        
</script>
        