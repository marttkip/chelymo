<?php
$preauth_date  = '';
$rebate_status = 0;
$open_status = 0;
$relation  = '';
$principal_member  = '';
$claim_number  = '';
if($visit_invoice_id > 0)
{

	// var_dump($visit_invoice_id);die();
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = date('d/m/Y',strtotime($value->preauth_date));
			$invoice_date = date('d/m/Y',strtotime($value->created));
			$preauth_amount = $value->preauth_amount;

			$authorising_officer = $value->authorising_officer;
			$insurance_limit = $value->insurance_limit;
			$insurance_number = $value->member_number;
			$insurance_description = $value->scheme_name;
			$bill_to  = $value->bill_to;
			$authorising_officer  = $value->authorising_officer;
			$preauth_status  = $value->preauth_status;
			$rebate_status  = $value->rebate_status;
			$relation  = $value->relation;
			$principal_member  = $value->principal_member;
			$claim_number  = $value->claim_number;
			$open_status  = $value->open_status;



		}
	}


}



$days = $this->accounts_model->days_billed($visit_invoice_id);

if($rebate_status == 0)
{
	$items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="0" checked="checked" >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="1">
                        Yes
                    </label>
                </div>
            </div>';

}
else if($rebate_status == 1)
{
	$items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="0"  >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="1" checked="checked">
                        Yes
                    </label>
                </div>
            </div>';

}




if($open_status == 0)
{
	$open_items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="open_status" value="0" checked="checked" >
                        Interim
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="open_status" value="1">
                        Invoiced
                    </label>
                </div>
            </div>';

}
else if($open_status == 1)
{
	$open_items = '<div class="col-lg-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" name="open_status" value="0"  >
		                        Interim
		                    </label>
		                </div>
		            </div>
		            
		            <div class="col-lg-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" name="open_status" value="1" checked="checked">
		                        Completed
		                    </label>
		                </div>
		            </div>';

}



$total = 0;
$item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_tree($visit_id);
    // var_dump($item_invoiced_rs->result());die();
$total_amount= 0; 
$days = 0;
 $service_items = '';
 $bills_items  ='';
 $total_waiver = 0;
if($item_invoiced_rs->num_rows() > 0)
{
    foreach ($item_invoiced_rs->result() as  $value) {
        # code...
        $service_id= $value->service_id;
        $service_name = $value->service_name;
        $visit_invoice_discount = $value->visit_invoice_discount;
        $visit_credit_note_id = $value->visit_credit_note_id;

        if(empty($visit_invoice_discount))
        {
        	$visit_invoice_discount = 0;
        }

        $bills_items .="<tr><th colspan='5'>".$service_name."</th></tr>";	
        $rs2 = $this->accounts_model->get_visit_procedure_charges_per_service_inpatient($visit_id,$service_id); 
        $sub_total= 0; 
        $personnel_query = $this->personnel_model->retrieve_personnel();


            
        if(count($rs2) >0){
            $count = 0;
            $visit_date_day = '';
            
            foreach ($rs2 as $key1):
                $v_procedure_id = $key1->visit_charge_id;
                $procedure_id = $key1->service_charge_id;
                $date = $key1->date;
                $time = $key1->time;
                $visit_charge_timestamp = $key1->visit_charge_timestamp;
                $visit_charge_amount = $key1->visit_charge_amount;
                $units = $key1->visit_charge_units;
                $procedure_name = $key1->service_charge_name;
                $service_id = $key1->service_id;
                $provider_id = $key1->provider_id;
            
                
                $visit_date = date('l d F Y',strtotime($date));
                $visit_time = date('H:i A',strtotime($visit_charge_timestamp));
                if($visit_date_day != $visit_date)
                {
                    
                    $visit_date_day = $visit_date;
                }
                else
                {
                    $visit_date_day = '';
                }

                

                if($personnel_query->num_rows() > 0)
                {
                    $personnel_result = $personnel_query->result();
                    
                    foreach($personnel_result as $adm)
                    {
                        $personnel_id = $adm->personnel_id;
                        
                        
                            if($personnel_id == $provider_id)
                            {
                                $provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

                                $procedure_name = $procedure_name.$provider_id;
                            }
                        
                        

                        
                        
                        
                    }

                }
                
                else
                {
                    $provider_id = '';
                    
                }
                if($procedure_name == 'Bed Charges')
                {
                    $days++;
                }



                $bills_items .="<tr> 
									<td>".$date." ".$visit_time."</td>	
									<td >".$procedure_name."</td>
									<td align='center'>
										".$units."
									</td>
									<td align='center'>".$visit_charge_amount."</div>
									</td>
									
									<td >".number_format($visit_charge_amount*$units)."</td>
									".$buttons."
								</tr>";	
					$count++;
               
                    $sub_total= $sub_total +($units * $visit_charge_amount);
                  
                
                $visit_date_day = $visit_date;
             
                endforeach;
                

        }
        $bills_items .="<tr><th colspan='4'>Sub Total ".$service_name."</th>
        				<th>".number_format($sub_total,2)."</th></tr>";	
       	$percentage_discount = 0;
        if($visit_invoice_discount > 0)
        {	
        	$percentage_discount = ($visit_invoice_discount * 100)/$sub_total;
        	$total_waiver += $visit_invoice_discount;
        	$total_bill =  $sub_total - $visit_invoice_discount; 
        }
        else
        {
        	 $total_bill = $sub_total;
        }
        $service_items .="
				            <tr >
				                <th colspan='1'>".strtoupper($service_name)."</th>
				                <td colspan='1' align='center'><strong>".number_format($sub_total,2)." </strong></td>
				                <td colspan='1' align='center'><strong><input type='text'   value=".$visit_invoice_discount." id='visit_invoice_discount".$service_id."' class='form-control'  /> </strong></td>
				                <td colspan='1' align='center'><strong> ".number_format($percentage_discount,2)."</strong></td>
				                <td colspan='1' align='center'><strong> ".number_format($total_bill,2)."</strong></td>
				                <td><a class='btn btn-xs btn-success' onclick='update_waiver_discount(".$visit_id.",".$visit_invoice_id.",".$service_id.",".$patient_id.")'><i class='fa fa-pencil'></i></a></td>
				            </tr>
			         
			            ";
        $total_amount = $total_amount + $sub_total;

    }

    $bills_items .="<tr><th colspan='4'>GRAND TOTAL</th>
        				<th>".number_format($total_amount,2)."</th>
        			</tr>";	

     $service_items .="
            <tr >
                <th colspan='1'>GRAND BILL</th>
                <td colspan='1' align='center' style='border-top:2px solid #000'><strong>".number_format($total_amount,2)." </strong></td>
                <td colspan='2' align='center' style='border-top:2px solid #000'><strong>BILL AFTER WAIVER</td>
                 <td colspan='1' align='center' style='border-top:2px solid #000'><strong>".number_format($total_amount-$total_waiver,2)." </strong></td>
            </tr>
         
            ";
     
}
					
$payment_item_rs = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id,1,$patient_id);
// var_dump($patient_id);die();



$x = 0;
$total_payment = 0;
$discount_date = date('Y-m-d');
$visit_cr_note_comment = '';
if($payment_item_rs->num_rows() > 0)
{

	foreach ($payment_item_rs->result() as $key => $value) {
		# code...
		$visit_invoice_number = $value->visit_invoice_number;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_credit_note_id = $value->visit_credit_note_id;
		$discount_date = $value->created;
		$payment_method_idd = $value->payment_method_id;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_cr_note_number = $value->visit_cr_note_number;
		$visit_cr_note_comment = $value->visit_cr_note_comment;
		
		// $patient_id = $value->patient_id;
		$payment_item_amount = $value->total_amount;



	

	
		$x++;
		
		$total_payment += $payment_item_amount;
	}

	
}
 

?>
<div class='col-md-12' >
	<div class='col-md-7'>
		<div style="height: 70vh;overflow-y: scroll;">
			<table class="table table-hover table-bordered">
				<thead>
					<th style='width:25%'>Date</th>
					<th style='width:40%'>Services/Items</th>
					<th style='width:10%'>Units</th>
					<th style='width:15%'>Unit Cost (Ksh)</th>
					<th style='width:10%'> Total</th>
				</thead>
				<tbody>
					<?php echo $bills_items;?>
				</tbody>
			</table>
			
		</div>
		

	</div>
	<div class='col-md-5'>


		 <?php echo form_open("accounts/confirm_waiver", array("class" => "form-horizontal","id"=>"waiver-form"));?>
			<div class="table">
				<table class="table table-hover table-bordered">
					<thead>
						<th>
							Service/Department
						</th>
						<th>
							Total Bill
						</th>
						<th>
							Disc
						</th>
						<th>
							Disc (%)
						</th>
						<th>
							Balance
						</th>
						<th>
							
						</th>
					</thead>
					<tbody>
						
						<?php echo $service_items?>
					</tbody>
				</table>
			</div>

			
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="form-group">
			            <label class="col-md-5">TOTAL WAIVER: </label>
			            <div class="col-md-12">
			            	KES. <?php echo number_format($total_waiver,2)?>
			            </div>
			            
			        </div>
				</div>
				<div class="col-md-6">
					
					<div class="form-group">
			            <label class="col-md-12">Discount Date: </label>
			            <div class="col-md-12">
			            	<div class="input-group">
			                	<span class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </span>
			                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" id="discount_date" name="discount_date" placeholder="date" value="<?php echo $discount_date;?>" required>
			                </div>
			            </div>
			            
			        </div>
				</div>
				<div class="col-md-12">

					<div class="form-group">
			            <label class="col-md-12 "> DISCOUNT NOTE </label>
			            <div class="col-md-12">
			            	<textarea class="form-control cleditor" name="discount_note" id="discount_note" required> <?php echo $visit_cr_note_comment?> </textarea>
			            	
			            </div>
			            
			        </div>
					
				</div>
				
		  		
	
				
			</div>
		
	  	
		 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
	  	 <input type="hidden" name="billing_visit_id" id="billing_visit_id" value="<?php echo $visit_id?>">
	  	 <input type="hidden" name="billing_patient_id" id="billing_patient_id"  value="<?php echo $visit_id?>">
	  	 <input type="hidden" name="visit_invoice_id" id="visit_invoice_id"  value="<?php echo $visit_invoice_id?>">
	  	  <input type="hidden" name="total_waiver" id="total_waiver"  value="<?php echo $total_waiver?>">


      		

	  	<div class='col-md-12 center-align' style="margin-top:15px;">
			<input type="submit" class="btn btn-info btn-sm"  value="Confirm Waiver Detail" />
		</div>
	</div>
	<?php echo form_close();?>
</div>