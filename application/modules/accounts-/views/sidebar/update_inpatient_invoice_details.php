<?php
$preauth_date  = '';
$rebate_status = 0;
$open_status = 0;
$relation  = '';
$principal_member  = '';
$claim_number  = '';
if($visit_invoice_id > 0)
{

	// var_dump($visit_invoice_id);die();
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = date('d/m/Y',strtotime($value->preauth_date));
			$invoice_date = date('d/m/Y',strtotime($value->created));
			$preauth_amount = $value->preauth_amount;

			$authorising_officer = $value->authorising_officer;
			$insurance_limit = $value->insurance_limit;
			$insurance_number = $value->member_number;
			//$insurance_description = $value->scheme_name;
			$bill_to  = $value->bill_to;
			$authorising_officer  = $value->authorising_officer;
			$preauth_status  = $value->preauth_status;
			$rebate_status  = $value->rebate_status;
			$relation  = $value->relation;
			$principal_member  = $value->principal_member;
			$claim_number  = $value->claim_number;
			$open_status  = $value->open_status;
			$copay  = $value->copay;
			$copay_status  = $value->copay_status;





		}
	}


}

$visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 0;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$close_card = $value->close_card;
		$visit_type_id = $value->visit_type;
		$invoice_number = $value->invoice_number;
		$visit_time_out = $value->visit_time_out;
		$parent_visit = $value->parent_visit;
		$insurance_description = $value->insurance_description;
		$insurance_number = $value->insurance_number;
		$patient_insurance_number = $value->patient_insurance_number;
		$insurance_description = $value->insurance_description;
		$insurance_limit = $value->insurance_limit;
		$admission_date = $value->admission_date;
		$discharge_date = $value->discharge_date;
		$copay_percentage = 0;//$value->copay_percentage;
		
		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
	}
}



$days = $this->accounts_model->days_billed($visit_invoice_id);

if($rebate_status == 0)
{
	$items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="0" checked="checked" >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="1">
                        Yes
                    </label>
                </div>
            </div>';

}
else if($rebate_status == 1)
{
	$items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="0"  >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="1" checked="checked">
                        Yes
                    </label>
                </div>
            </div>';

}


if($copay_status == 0)
{
	$copay_items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="copay_status" value="0" checked="checked" >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="copay_status" value="1">
                        Yes
                    </label>
                </div>
            </div>';

}
else if($copay_status == 1)
{
	$copay_items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="copay_status" value="0"  >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="copay_status" value="1" checked="checked">
                        Yes
                    </label>
                </div>
            </div>';

}




if($open_status == 0)
{
	$open_items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="open_status" value="0" checked="checked" >
                        Interim / Open
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="open_status" value="1">
                        Invoiced / Closed
                    </label>
                </div>
            </div>';

}
else if($open_status == 1)
{
	$open_items = '<div class="col-lg-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" name="open_status" value="0"  >
		                        Interim / Open
		                    </label>
		                </div>
		            </div>
		            
		            <div class="col-lg-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" name="open_status" value="1" checked="checked">
		                        Invoice / Closed 
		                    </label>
		                </div>
		            </div>';

}





?>

<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i>UPDATE PATIENT BILLING</h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd" style="height: 80vh;overflow-y: scroll;">
        	 <?php echo form_open("accounts/change_patient_visit", array("class" => "form-horizontal","id"=>"visit_type_change"));?>
			<div class='row'>
				<div class='col-md-6'>
					<div class="form-group">
						<label class="col-lg-4 control-label">Type: </label>
					  
						<div class="col-lg-8">
							<select id='visit_type_id' name='visit_type_id' class='form-control' >
			                 
			                  <?php
															
								if($visit_types_rs->num_rows() > 0){

									foreach($visit_types_rs->result() as $row):
										$visit_type_name = $row->visit_type_name;
										$visit_type_id = $row->visit_type_id;

										if($visit_type_id == $bill_to)
										{
											echo "<option value='".$visit_type_id."' selected='selected'>".$visit_type_name."</option>";
										}
										
										else
										{
											echo "<option value='".$visit_type_id."'>".$visit_type_name."</option>";
										}
									endforeach;
								}
							?>
			                  
			                </select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Scheme: </label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="insurance_description" value="<?php echo $insurance_description?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Member Number: </label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="patient_insurance_number" value="<?php echo $patient_insurance_number?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Insurance Limit:</label>
						
						<div class="col-md-8">
			                <input type="number" class="form-control" name="insurance_limit" value="<?php echo $insurance_limit?>">
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label">Claim Number: </label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="claim_number" value="<?php echo $claim_number?>">
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-4 control-label">Principal: </label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="principal_member" value="<?php echo $principal_member?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Relation: </label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="relation" value="<?php echo $relation?>">
						</div>
					</div>
						<div class="form-group">
						<label class="col-lg-4 control-label">Copay  </label>
			            <?php echo $copay_items?>
					</div>
						<div class="form-group">
						<label class="col-md-4 control-label">UPDATE NHIF COPAY : </label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="copay" value="<?php echo $copay?>">
						</div>
					</div>


				</div>
				<div class='col-md-6'>
					<div class="form-group" style="display:none;">
						<label class="col-lg-4 control-label">Include NHIF Rebate? </label>
			            <?php echo $items?>
					</div>

				


					<div class="form-group">
						<label class="col-lg-4 control-label">Invoice Status </label>
			            <?php echo $open_items?>
					</div>
					<div class="alert alert-xs alert-danger" style="display:none;">No of days <?php echo $days?> days.</div>
					<div class="form-group">
						<label class="col-lg-4 control-label">Invoice Date: </label>
						<div class="col-lg-8">
			                <div class="input-group">
			                    <span class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </span>
			                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" id="datepicker" placeholder="Invoice Date" value="<?php echo date('Y-m-d');?>">
			                </div>
						</div>
					</div>
				</div>
			  	

				 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
			  	 <input type="hidden" name="billing_visit_id" id="billing_visit_id" value="<?php echo $visit_id?>">
			  	 <input type="hidden" name="billing_patient_id" id="billing_patient_id"  value="<?php echo $visit_id?>">
			  	 <input type="hidden" name="visit_invoice_id" id="visit_invoice_id"  value="<?php echo $visit_invoice_id?>">
			</div>
			<div class='row center-align' style="margin-top:15px;">
				<button type="submit" class="btn btn-info btn-sm" type="submit" onclick="return confirm('Are you sure you want to update the patient details ?')" >Update billing</button>
			</div>
			 <?php echo form_close();?>
		</div>
	</div>
</section>
<div class="col-md-12">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>
</div>