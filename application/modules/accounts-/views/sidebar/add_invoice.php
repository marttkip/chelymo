<?php
$patient_items = '';
$visit_type_id = null;
$close_card = 3;
$visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 0;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$close_card = $value->close_card;
		$visit_type_id = $value->visit_type;
		$invoice_number = $value->invoice_number;
		$visit_time_out = $value->visit_time_out;
		$parent_visit = $value->parent_visit;
		$insurance_description = $value->insurance_description;
		$insurance_number = $value->insurance_number;
		$admission_date = $value->admission_date;
		$visit_date = $value->visit_date;
		$discharge_date = $value->discharge_date;
		$personnel_id = $value->personnel_id;
		$copay_percentage = 0;//$value->copay_percentage;
		
		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
	}
}


if(empty($admission_date) )
{
	$admission_date = $visit_date;
}


if(empty($discharge_date))
{
	$discharge_date = date('Y-m-d');
}
?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title.' '.$visit_date?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
           <div class="col-md-12">
           		<div class="col-md-3">
           			
           	
          		 </div>
          		 <div class="col-md-6">
          		 	<div class="col-md-12" style="height: 75vh; overflow-y: scroll;">
		                  <div class="col-md-12">
		                  		<?php echo form_open("accounts/add_service_item", array("class" => "form-horizontal","id"=>"create_new_invoice"));?>
			                  		<!-- <h4 > </h4> -->
			                  		<!-- <input type="hidden" name=""> -->
			                  		<br>
			                  		 
			                  		 <input type="hidden" class="form-control" name="visit_id" id="visit_id" value="<?php echo $visit_id?>" required>
			                  		 <input type="hidden" class="form-control" name="patient_id" id="patient_id" value="<?php echo $patient_id?>" required>
			                  		  <input type="hidden" class="form-control" name="doctor_id" id="doctor_id" value="<?php echo $personnel_id?>" required>

			                  		<div class="form-group">
										<label class="col-lg-4 control-label">Account Type: </label>
										
										<div class="col-lg-8">
											<select name="visit_type_id" id="visit_type_id2" class="form-control" required>
												<option value="">----Select a visit type----</option>
												<?php
																		
													if($visit_types->num_rows() > 0){

														foreach($visit_types->result() as $row):
															$visit_type_name = $row->visit_type_name;
															$visit_type_id = $row->visit_type_id;

														
															echo "<option value='".$visit_type_id."'>".$visit_type_name."</option>";
															
														endforeach;
													}
												?>
											</select>
										</div>
									</div>
		                      		<div id="insured_company" style="display:none">
		                                
										<div class="form-group" style="margin-bottom: 15px;">
											<label class="col-lg-4 control-label">Member Number: </label>
											<div class="col-lg-8">
												<input type="text" name="insurance_number" id="member_number" class="form-control member_number">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 15px;">
											<label class="col-lg-4 control-label">Scheme Name: </label>
											<div class="col-lg-8">
												<input type="text" name="scheme_name" id="scheme_name" class="form-control scheme_name">
											</div>
										</div>
		                                
										<div class="form-group" style="margin-bottom: 15px;">
											<label class="col-lg-4 control-label">Insurance Limit: </label>
											<div class="col-lg-8">
												<input type="text" name="insurance_limit" class="form-control">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 15px;">
											<label class="col-lg-4 control-label">Principal Member: </label>
											<div class="col-lg-8">
												<input type="text" name="principal_member" class="form-control principal_member">
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 15px;">
											<label class="col-lg-4 control-label">Relation: </label>
											<div class="col-lg-8">
												<input type="text" name="relation" class="form-control relation">
											</div>
										</div>
									</div>
						           <div class="center-align" style="margin-top:10px;">
										<input type="submit" value="ADD INVOICE" class="btn btn-info btn-sm" />
									</div>


						        <?php echo form_close();?>

		                  </div>
		            </div>
           	
          		 </div>

          		 <div class="col-md-3">
           			
           	
          		 </div>
           </div>
            
              
           
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">

        
</script>
        