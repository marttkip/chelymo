

<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i>UPDATE PATIENT BILLING</h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd" style="height: 80vh;overflow-y: scroll;">
        	
			<div id="discounts-page"></div>
			
			 
		</div>
	</div>
</section>
<div class="col-md-12">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>
</div>
<script type="text/javascript">
	

function update_waiver_discount(visit_id,visit_invoice_id,service_id,patient_id)
{

	var config_url = $('#config_url').val();
	var discount_given = $('#visit_invoice_discount'+service_id).val();
	var url = "<?php echo base_url();?>accounts/update_waiver_amount";		
	
	$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: visit_id,visit_invoice_id: visit_invoice_id,service_id:service_id,patient_id:patient_id,discount_given: discount_given},
			dataType: 'text',
		success:function(data){
			// close_side_bar();
		 	// display_patient_bill(v_id);
		 	get_waiver_info_details(visit_id,visit_invoice_id,patient_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			
		}
	});
	return false;
}


$(document).on("submit","form#waiver-form",function(e)
{
	// alert("changed");
	e.preventDefault();	

	var form_data = new FormData(this);

	var res = confirm('Are you sure you want to confirm this waiver ?');

	if(res)
	{
		var config_url = $('#config_url').val();
		// var bill_visit = $('#bill_visit').val();
		var url = "<?php echo base_url();?>accounts/confirm_waiver";		
		
		$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
				close_side_bar();
			 	// display_patient_bill(v_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				
			}
		});

		var patient_id = window.localStorage.getItem('patient_id');
		var visit_id = window.localStorage.getItem('visit_id');
		 get_all_visits_div(patient_id);
		 display_patient_bill(visit_id);
		return false;

	}
	
});

</script>