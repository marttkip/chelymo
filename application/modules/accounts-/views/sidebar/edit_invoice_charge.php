<?php
$patient_items = '';
$visit_type_id = null;
$close_card = 3;
if($query->num_rows() > 0)
{
    foreach ($query->result() as $key => $res) 
    {
        # code...

       
        $service_charge_name = $res->service_charge_name;
        $visit_charge_amount = $res->visit_charge_amount;
        $visit_charge_units = $res->visit_charge_units;
        $reason = $res->reason;
        $service_charge_id = $res->service_charge_id;
        $personnel_idd = $res->doctor_id;
        $date = date('Y-m-d',strtotime($res->date));
        
        

    }

}
?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
           <div class="col-md-12">
           		<div class="col-md-3">
           			
           	
          		 </div>
          		 <div class="col-md-6">
          		 	<div class="col-md-12" style="height: 75vh; overflow-y: scroll;">
		                  <div class="col-md-12">
		                  		<?php echo form_open("accounts/add_service_item", array("class" => "form-horizontal","id"=>"update-bill-inpatient"));?>
			                  		<h4 > <?php echo strtolower($service_charge_name);?> Unit Price KES. <?php echo $visit_charge_amount;?></h4>
			                  		<!-- <input type="hidden" name=""> -->
			                  		<br>
			                  		 <input type="hidden" class="form-control" name="visit_charge_id" id="visit_charge_id" value="<?php echo $visit_charge_id?>" required>
			                  		 <input type="hidden" class="form-control" name="service_charge_id" id="service_charge_id" value="<?php echo $service_charge_id?>" required>
			                  		 <input type="hidden" class="form-control" name="visit_id" id="visit_id" value="<?php echo $visit_id?>" required>
			                  		 <input type="hidden" class="form-control" name="patient_id" id="patient_id" value="<?php echo $patient_id?>" required>
			                  		 <input type="hidden" class="form-control" name="service_charge_name" id="service_charge_name" value="<?php echo $service_charge_name?>" required>
			                  		 <div class="form-group">
										<label class="col-md-4 control-label">Reason:</label>
										
										<div class="col-md-8">
							                <input type="text" class="form-control" name="reason" id="reason" value="<?php echo $reason?>" required>
										</div>
									</div>
			                  		<div class="form-group">
										<label class="col-md-4 control-label">Units:</label>
										
										<div class="col-md-8">
							                <input type="number" class="form-control" name="units_charged" id="units_charged" value="<?php echo $visit_charge_units?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Unit Charge:</label>
										
										<div class="col-md-8">
							                <input type="text" class="form-control" name="visit_charge_amount" id="visit_charge_amount" value="<?php echo $visit_charge_amount?>">
										</div>
									</div>
			                  		<div class="form-group">
										<label class="col-lg-4 control-label">Consultant: <?php echo $personnel_idd;?></label>
										
										<div class="col-lg-8">
											 <select name="provider_id" id="provider_id" class="form-control">
												<?php
													
																		
													if(count($doctor) > 0){
														foreach($doctor as $row):
															$fname = $row->personnel_fname;
															$onames = $row->personnel_onames;
															$personnel_id = $row->personnel_id;
															
															if($personnel_id == $personnel_idd)
															{
																echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
															}
															
															else
															{
																echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
															}
														endforeach;
														echo "<option value='0'>----Select a Consultant----</option>";
													}
												?>
											</select>
										</div>
									</div>
			                  	<div class="form-group">
						                <label class="col-md-4 control-label">Date: </label>
						                <div class="col-md-8">
						                	<div class="input-group">
							                	<span class="input-group-addon">
							                        <i class="fa fa-calendar"></i>
							                    </span>
							            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" id="date" name="date" placeholder="date" value="<?php echo $date;?>">
							                </div>
						                </div>
						                
						            </div>
						           <div class="center-align" style="margin-top:10px;">
										<input type="submit" value="Update bill" class="btn btn-info btn-sm" />
									</div>


						        <?php echo form_close();?>

		                  </div>
		            </div>
           	
          		 </div>

          		 <div class="col-md-3">
           			
           	
          		 </div>
           </div>
            
              
           
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">
        
</script>
        