<?php

$payment_item_rs = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id,1,$patient_id);
// var_dump($patient_id);die();

$result = "

			<table  class='table table-striped table-bordered table-condensed'>
			<tr>
				<th></th>
				<th>DATE</th>
				<th>NUMBER</th>
				<th>CR NUMBER</th>
				<th>AMOUNT</th>
				<th></th>

			</tr>
		";

$x = 0;
$total_payment = 0;
$payment_date = date('Y-m-d');
if($payment_item_rs->num_rows() > 0)
{

	foreach ($payment_item_rs->result() as $key => $value) {
		# code...
		$visit_invoice_number = $value->visit_invoice_number;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_credit_note_id = $value->visit_credit_note_id;
		$created = $value->created;
		$payment_method_idd = $value->payment_method_id;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_cr_note_number = $value->visit_cr_note_number;
		
		// $patient_id = $value->patient_id;
		$payment_item_amount = $value->total_amount;



	


		
		$checked = "<td>
						<a class='btn btn-xs btn-warning' href='".site_url().'print-credit-note/'.$visit_credit_note_id.'/'.$visit_invoice_id.'/'.$patient_id."' target='_blank'><i class='fa fa-print'></i></a>
					</td>";
	
		$x++;
		$result .= '<tr>
						<td >'.$x.'</td>
						<td>'.date('jS M Y',strtotime($created)).'</td>
						<td>'.$visit_invoice_number.'</td>
						<td>'.$visit_cr_note_number.'</td>
						<td>'.number_format($payment_item_amount,2).'</td>
						'.$checked.'
					</tr>';
		$total_payment += $payment_item_amount;
	}

	$result .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total</td>
						<td>'.number_format($total_payment,2).'</td>
					</tr>';
}
  $result .='</tbody>
                      </table>';


 

?>
<h4>Waivers <a class="btn btn-xs btn-danger pull-right"  onclick="open_waiver_details(<?php echo $visit_id;?>,<?php echo $visit_invoice_id;?>,<?php echo $patient_id;?>)"><i class="fa fa-pencil"></i> Add Waiver</a> </h4>
<div class="col-md-12">
	<?php echo $result;?>
</div>
