<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
// $visit_id = $patient['visit_id'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor

// var_dump($visit_id);die();
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
// $paid_before = $this->accounts_model->get_payment_before($payment_id,$visit_id);
// $paid_current = $this->accounts_model->get_payment_current($payment_id,$visit_id);
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));

$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

$doctor = '';
$credit_note = 0;
$previous_payment = 0;
$refunds = 0;
if($visit_invoice_detail->num_rows() > 0)
{
    foreach ($visit_invoice_detail->result() as $key => $value) {
        # code...
        $visit_invoice_number = $value->visit_invoice_number;
        $preauth_date = date('d/m/Y',strtotime($value->preauth_date));
        $invoice_date = date('d/m/Y',strtotime($value->created));
        $preauth_amount = $value->preauth_amount;

        $authorising_officer = $value->authorising_officer;
        $insurance_limit = $value->insurance_limit;
        $insurance_number = $value->member_number;
        $insurance_description = $value->scheme_name;
        $visit_id  = $value->visit_id;
        $authorising_officer  = $value->authorising_officer;
        $patient_id  = $value->patient_id;



    }
    $doctor = $this->accounts_model->get_att_doctor($visit_id);

    // var_dump($)

    $credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
    $total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);

}
?>

<!DOCTYPE html>
<html lang="en">

   <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .col-md-6 {
                width: 50%;
             }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
            {
                 padding: 4px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            body.receipt .sheet { width: 80mm !important;letter-spacing:1px; } /* change height as you like */
            @media print { body.receipt { width: 80mm !important;letter-spacing:1px;} } /* this line is needed for fixing Chrome's bug */
        </style>
    </head>
    <body class="receipt">
        <div class="row">
            <div class="col-MD-12">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 center-align receipt_bottom_border" style="margin-bottom: 20px; ">
                <strong>
                    <?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?><br/>
                </strong>
            </div>
        </div>
        
        <div class="row" >
            <div class="col-md-12 center-align">
                <h2><strong>RECEIPT</strong></h2>
            </div>
        </div>
     
        
        <!-- Patient Details -->
        <div class="row receipt_bottom_border" style="margin-bottom: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">
            <div class="col-md-12">
                <div class="pull-left " >
                    <div class="row">
                        <div class="col-md-12">
                            <div >RECEIPT#: <strong><?php echo 'OTC-INV-00'.$payment_id; ?></strong> </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div >PATIENT: <strong><?php echo $patient_surname.' '.$patient_othernames; ?></strong> </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div >FILE NO: <strong><?php echo $patient_number; ?></strong> </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div >DATE SEEN: <strong><?php echo $visit_date; ?></strong> </div> 
                        </div>
                    </div>

                   
                </div>
            </div>
            
        </div>
        
        
        <div class="row receipt_bottom_border" style="margin-bottom: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; padding-right: 5px;">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped">
                  <tbody>
                    
                    <?php
                        $total = 0;
                        $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_tree($visit_id);
                            // var_dump($item_invoiced_rs->result());die();
                        $total_amount= 0; 
                        $days = 0;
                        if($item_invoiced_rs->num_rows() > 0)
                        {
                            foreach ($item_invoiced_rs->result() as  $value) {
                                # code...
                                $service_id= $value->service_id;
                                $service_name = $value->service_name;

                              

                                $rs2 = $this->accounts_model->get_visit_procedure_charges_per_service_inpatient($visit_id,$service_id); 

                                
                                
                                $sub_total= 0; 
                                $personnel_query = $this->personnel_model->retrieve_personnel();
                                    
                                if(count($rs2) >0){
                                    $count = 0;
                                    $visit_date_day = '';
                                    
                                    foreach ($rs2 as $key1):
                                        $v_procedure_id = $key1->visit_charge_id;
                                        $procedure_id = $key1->service_charge_id;
                                        $date = $key1->date;
                                        $time = $key1->time;
                                        $visit_charge_timestamp = $key1->visit_charge_timestamp;
                                        $visit_charge_amount = $key1->visit_charge_amount;
                                        $units = $key1->visit_charge_units;
                                        $procedure_name = $key1->service_charge_name;
                                        $service_id = $key1->service_id;
                                        $provider_id = $key1->provider_id;
                                    
                                        
                                        $visit_date = date('l d F Y',strtotime($date));
                                        $visit_time = date('H:i A',strtotime($visit_charge_timestamp));
                                        if($visit_date_day != $visit_date)
                                        {
                                            
                                            $visit_date_day = $visit_date;
                                        }
                                        else
                                        {
                                            $visit_date_day = '';
                                        }

                                        

                                        if($personnel_query->num_rows() > 0)
                                        {
                                            $personnel_result = $personnel_query->result();
                                            
                                            foreach($personnel_result as $adm)
                                            {
                                                $personnel_id = $adm->personnel_id;
                                                
                                                
                                                    if($personnel_id == $provider_id)
                                                    {
                                                        $provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

                                                        $procedure_name = $procedure_name.$provider_id;
                                                    }
                                                
                                                

                                                
                                                
                                                
                                            }

                                        }
                                        
                                        else
                                        {
                                            $provider_id = '';
                                            
                                        }
                                        if($procedure_name == 'Bed Charges')
                                        {
                                            $days++;
                                        }




                                        $count++;
                                       
                                            $sub_total= $sub_total +($units * $visit_charge_amount);
                                          
                                        
                                        $visit_date_day = $visit_date;
                                     
                                        endforeach;
                                        

                                }
                                echo"
                                    <tr >
                                        <td colspan='4'>".strtoupper($service_name)." TOTAL</td>
                                        <td colspan='1' align='center'><strong>".number_format($sub_total,2)." </strong></td>
                                    </tr>
                                 
                                    ";
                                $total_amount = $total_amount + $sub_total;

                            }

                             echo"
                                    <tr >
                                        <th colspan='4'>GRAND BILL</th>
                                        <td colspan='1' align='center' style='border-top:2px solid #000'><strong>".number_format($total_amount,2)." </strong></td>
                                    </tr>
                                 
                                    ";
                             echo"
                                    <tr >
                                        <th colspan='5'></th>
                                    </tr>
                                 
                                    ";
                             echo"
                                    <tr >
                                        <th colspan='5'></th>
                                    </tr>
                                 
                                    ";
                        }
                    ?>
                    <tr>
                      <td colspan="5"><strong>RECEIPT SUMMARY </strong> </td>
                    </tr>
                    <tr class="receipt_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                        <td colspan="4">TOTAL INVOICE</td>
                        <td>Kes. <?php echo number_format($total_amount, 2);?></td>
                    </tr>
                    <tr class="receipt_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                        <td colspan="4">CREDIT NOTE</td>
                        <td>Kes. (<?php echo number_format($credit_note, 2);?>)</td>
                    </tr>
                    
                    <?php

                    // $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                    $payments_rs = $this->accounts_model->inpatient_payments($visit_id,$visit_invoice_id);

                  
                    $total_payments = 0;
                    
                    if(count($payments_rs) > 0)
                    {
                        $x=0;
                        
                        foreach ($payments_rs as $key_items):
                            $x++;
                            $payment_type = $key_items->payment_type;
                            $payment_status = $key_items->payment_status;
                            
                            if($payment_type == 1 && $payment_status == 1)
                            {
                                $payment_id = $key_items->payment_id;
                                $amount_paid = $key_items->amount_paid;
                            
                                $total_payments = $total_payments + $amount_paid;
                                
                                // if($payment_id == $receipt_payment_id)
                                // {
                                    $payment_method = $key_items->payment_method;
                                    $transaction_code = $key_items->transaction_code;
                                    $payment_service_id = $key_items->payment_service_id;
                                    $service_name = $payment_method.' '.$transaction_code;
                                    
                                    if(count($service_rs) > 0)
                                    {
                                        foreach($service_rs as $serv)
                                        {
                                            $service_id = $serv->service_id;
                                            if($payment_service_id == $service_id)
                                            {
                                                $service_name = $serv->service_name;
                                                break;
                                            }
                                        }
                                    }
                                                    
                                    //display DN & CN services
                                    if((count($payments_rs) > 0) && ($service_name == ''))
                                    {
                                        foreach ($payments_rs as $key_items):
                                            $payment_type = $key_items->payment_type;
                                            
                                            if(($payment_type == 2) || ($payment_type == 3))
                                            {
                                                $payment_service_id2 = $key_items->payment_service_id;
                                                
                                                if($payment_service_id2 == $payment_service_id)
                                                {
                                                    $service_name = $this->accounts_model->get_service_detail($payment_service_id);
                                                    break;
                                                }
                                            }
                                            
                                        endforeach;
                                    }
                                 
                                    ?>
                                    <tr style="margin-bottom: 20px; font-weight: bold;">
                                        <td colspan="4" > <?php echo strtoupper($payment_method);?> <?php echo strtoupper($transaction_code);?></td>
                                        <td>Ksh. ( <?php echo number_format($amount_paid, 2);?> )</td>
                                    </tr>
                                    <?php
                                // }
                            }


                        endforeach;
                        
                    }
                    // $invoice_total = $this->accounts_model->total_invoice_inpatient($visit_id);

                    ?>
                    
                    <tr class="receipt_bottom_border" style="margin-bottom: 20px; font-weight: bold;">


                        <td colspan="4">BALANCE</td>

                        <td> Ksh. <?php echo number_format($total_amount - ($total_payments+$credit_note+$refunds), 2);?></td>
                    </tr>
                      
                  </tbody>
                </table>
            </div>
        </div>
        
        <div class="" style="font-style:italic; ">
            <div style="float:left; margin:0 10px 0 10px;">
                Served by: <?php echo $served_by; ?><br>
                <?php echo $today; ?> Thank you
            </div>
        </div>
    </body>
    
</html>