<?php

$visit_invoices_rs = $this->accounts_model->get_visit_invoices_list($patient_id,$visit_id);
$charges_result = '';

if($visit_invoices_rs->num_rows() > 0)
{
	foreach ($visit_invoices_rs->result() as $key => $value) {
		// code...
		$invoice_bill = $value->invoice_bill;
		$invoice_balance = $value->invoice_balance;
		$invoice_credit_note = $value->invoice_credit_note;
		$visit_invoice_number = $value->visit_invoice_number;
		$invoice_payments = $value->invoice_payments;
		$visit_invoice_id = $value->visit_invoice_id;




		// $total = 0;
		// $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_tree($visit_id,$visit_invoice_id);
		// $total_amount= 0; 
		// $days = 0;
		// if($item_invoiced_rs->num_rows() > 0)
		// {
		// 	foreach ($item_invoiced_rs->result() as  $value) {
		// 		# code...
		// 		$service_id= $value->service_id;
		// 		$service_name = $value->service_name;

		$charges_result .="
							<tr > 
								<td colspan='9'><strong>".$visit_invoice_number."</strong></td>
							</tr> 
						";

			$rs2 = $this->accounts_model->get_visit_procedure_charges_per_service($visit_id,null,$visit_invoice_id); 

				// if($service_name == "Bed charge")
				// {
				// 	$days = count($rs2);
				// }
				
				// var_dump($service_name); die();
				$sub_total= 0; 
				$personnel_query = $this->personnel_model->retrieve_personnel();
					
				if(count($rs2) >0){
					$count = 0;
					$visit_date_day = '';
					
					foreach ($rs2 as $key1):
						$v_procedure_id = $key1->visit_charge_id;
						$procedure_id = $key1->service_charge_id;
						$date = $key1->date;
						$time = $key1->time;
						$visit_charge_timestamp = $key1->visit_charge_timestamp;
						$visit_charge_amount = $key1->visit_charge_amount;
						$units = $key1->visit_charge_units;
						$procedure_name = $key1->service_charge_name;
						$service_id = $key1->service_id;
						$provider_id = $key1->provider_id;
					
						
						$visit_date = date('l d F Y',strtotime($date));
						$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
						if($visit_date_day != $visit_date)
						{
							
							$visit_date_day = $visit_date;
						}
						else
						{
							$visit_date_day = '';
						}

						

						if($personnel_query->num_rows() > 0)
						{
							$personnel_result = $personnel_query->result();
							
							foreach($personnel_result as $adm)
							{
								$personnel_id = $adm->personnel_id;
								
								
									if($personnel_id == $provider_id)
									{
										$provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

										$procedure_name = $procedure_name.$provider_id;
									}
								
								

								
								
								
							}

						}
						
						else
						{
							$provider_id = '';
							
						}
						if($procedure_name == 'Bed Charges')
						{
							$days++;
						}




						$count++;
						if($procedure_id != 7769)
						{
							$sub_total= $sub_total +($units * $visit_charge_amount);
							$charges_result .="
													<tr> 
														<td >".$count."</td>
														<td >".$visit_date_day." ".$visit_time."</td>
														<td >".$procedure_name."</td>
														<td align='right'>".$units."</td>
														<td align='right'>".number_format($visit_charge_amount,2)."</td>
														<td align='right'>".number_format($units * $visit_charge_amount,2)."</td>
														
													</tr>	
											";
						
						$visit_date_day = $visit_date;
						}
						endforeach;
						

				}
				$charges_result .="
									<tr >
										<td colspan='5'><strong>INVOICE TOTAL:</strong></td>
										<td colspan='1' align='right'><strong>".number_format($invoice_bill,2)." </strong></td>
									</tr>
									";
				$total_amount = $total_amount + $sub_total;

	}
	$charges_result .="
									<tr >
										<td colspan='5'><strong> CREDIT NOTES :</strong></td>
										<td colspan='1' align='right'><strong> (".number_format($invoice_credit_note,2).") </strong></td>
									</tr>
									";
	$charges_result .="
									<tr >
										<td colspan='5'><strong> PAYMENTS :</strong></td>
										<td colspan='1' align='right'><strong> (".number_format($invoice_payments,2).") </strong></td>
									</tr>
									";
	$charges_result .="
									<tr >
										<td colspan='5'><strong> BALANCE : ".$visit_invoice_number."</strong></td>
										<td colspan='1' align='right'><strong> ".number_format($invoice_balance,2)." </strong></td>
									</tr>
									";
}




$open_bills = '';

$rs3 = $this->accounts_model->get_visit_procedure_charges_per_service($visit_id,null,0); 

$sub_total_open= 0; 
$personnel_query = $this->personnel_model->retrieve_personnel();
	
if(count($rs3) >0){
	$count = 0;
	$visit_date_day = '';
	
	foreach ($rs3 as $key1):
		$v_procedure_id = $key1->visit_charge_id;
		$procedure_id = $key1->service_charge_id;
		$date = $key1->date;
		$time = $key1->time;
		$visit_charge_timestamp = $key1->visit_charge_timestamp;
		$visit_charge_amount = $key1->visit_charge_amount;
		$units = $key1->visit_charge_units;
		$procedure_name = $key1->service_charge_name;
		$service_id = $key1->service_id;
		$provider_id = $key1->provider_id;
	
		
		$visit_date = date('l d F Y',strtotime($date));
		$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
		if($visit_date_day != $visit_date)
		{
			
			$visit_date_day = $visit_date;
		}
		else
		{
			$visit_date_day = '';
		}

		

		if($personnel_query->num_rows() > 0)
		{
			$personnel_result = $personnel_query->result();
			
			foreach($personnel_result as $adm)
			{
				$personnel_id = $adm->personnel_id;
				
				
					if($personnel_id == $provider_id)
					{
						$provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

						$procedure_name = $procedure_name.$provider_id;
					}
				
				

				
				
				
			}

		}
		
		else
		{
			$provider_id = '';
			
		}
		if($procedure_name == 'Bed Charges')
		{
			$days++;
		}




		$count++;
		if($procedure_id != 7769)
		{
			$sub_total_open= $sub_total_open +($units * $visit_charge_amount);
			$open_bills .="
									<tr> 
										<td >".$count."</td>
										<td >".$visit_date_day." ".$visit_time."</td>
										<td >".$procedure_name."</td>
										<td align='right'>".$units."</td>
										<td align='right'>".number_format($visit_charge_amount,2)."</td>
										<td align='right'>".number_format($units * $visit_charge_amount,2)."</td>
										
									</tr>	
							";
		
		$visit_date_day = $visit_date;
		}
		endforeach;
		

}
$open_bills .="
					<tr >
						<td colspan='5'><strong>OPEN BILLS:</strong></td>
						<td colspan='1' align='right'><strong>".number_format($sub_total_open,2)." </strong></td>
					</tr>
					";

?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
            <div class="col-md-12">
            	<div class="col-md-6" style="height: 75vh; overflow-y: scroll;">
            		<h4>Visit Invoices</h4>
            		<table class="table table-hover table-bordered table-striped">
            			<thead>
            				<th></th>
            				<th>Billing Date</th>
            				<th>Procedure</th>
            				<th>Units</th>
            				<th>Amount</th>
            				<th>Total Amount</th>
            			</thead>
            			<tbody>
            				<?php echo $charges_result?>
            	
            			</tbody>
            		</table>
	                    
	            </div>
	            <div class="col-md-6" style="height: 75vh; overflow-y: scroll;">
	            	<h4>Visit Open Bills</h4>
	            	<table class="table table-hover table-bordered table-striped">
            			<thead>
            				<th></th>
            				<th>Billing Date</th>
            				<th>Procedure</th>
            				<th>Units</th>
            				<th>Amount</th>
            				<th>Total Amount</th>
            			</thead>
            			<tbody>
            				<?php echo $open_bills?>
            	
            			</tbody>
            		</table>
	                    
            	</div>
            	
            </div>
           
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
<script type="text/javascript">
        
</script>
        