<?php

$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');


$preauth_date  = '';
if($visit_invoice_id > 0)
{

	// var_dump($visit_invoice_id);die();
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = date('d/m/Y',strtotime($value->preauth_date));
			$invoice_date = date('d/m/Y',strtotime($value->created));
			$preauth_amount = $value->preauth_amount;

			$authorising_officer = $value->authorising_officer;
			$insurance_limit = $value->insurance_limit;
			$insurance_number = $value->member_number;
			//$insurance_description = $value->insurance_description;
			$bill_to  = $value->bill_to;
			$authorising_officer  = $value->authorising_officer;
			$preauth_status  = $value->preauth_status;
			$rebate_status  = $value->rebate_status;
			$claim_number  = $value->claim_number;
			$principal_member  = $value->principal_member;
			$relation  = $value->relation;
			$open_status  = $value->open_status;



		}
	}




	$payments_rs = $this->accounts_model->get_cash_invoice_payments_id($visit_invoice_id);
	$total_payments = 0;
	$s = 0;
	$total_amount = 0;

	if($payments_rs->num_rows() > 0)
	{
		foreach($payments_rs->result() as $row)
		{

			$total_payments = $row->total_payments;
		}
	}



}

// $visit_rs = $this->accounts_model->get_visit_details($visit_id);
// $visit_type_id = 0;
// if($visit_rs->num_rows() > 0)
// {
// 	foreach ($visit_rs->result() as $key => $value) {
// 		# code...
// 		$close_card = $value->close_card;
// 		$visit_type_id = $value->visit_type;
// 		$invoice_number = $value->invoice_number;
// 		$visit_time_out = $value->visit_time_out;
// 		$parent_visit = $value->parent_visit;
// 		$insurance_description = $value->insurance_description;
// 		$insurance_number = $value->insurance_number;
// 		$admission_date = $value->admission_date;
// 		$visit_date = $value->visit_date;
// 		$discharge_date = $value->discharge_date;
// 		$copay_percentage = 0;//$value->copay_percentage;
		
// 		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
// 	}
// }


// if(empty($admission_date))
// {
// 	$admission_date = $visit_date;
// }


// if(empty($discharge_date))
// {
// 	$discharge_date = date('Y-m-d');
// }

?>
<div class="row" >
		<header class="panel-heading">						
			<a class='btn btn-sm btn-warning ' data-toggle='modal' data-target='#add_assessment' ><i class="fa fa-plus"></i> Add Service</a>
			<?php
			if(!empty($visit_id))
			{
				?>
				<a   class="btn btn-sm btn-danger" onclick="view_patient_trail(<?php echo $visit_id;?>)"><i class="fa fa-print"></i> View Trail </a>
				<?php
			}
			?>
			<span style="text-transform: uppercase;"><strong>Patient Name: <?php echo $title;?></strong></span>
			<a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Queue</a>
		</header>	
		

</div>
	
<div class="row" >
	<div class="col-md-3" style="background: #fff;height:80vh !important;overflow-y: scroll;border-right: grey 2px solid;">
		<div class="row">
			<header class="panel-heading">						
				<h2 class="panel-title">Vists</h2>
			</header>
			<div id="visits_div"></div>

		</div>
	</div>
	<div class="col-md-9" style="background: #fff;height:80vh !important;overflow-y: scroll;border-right: grey 2px solid;">
		<div class="row">
			<header class="panel-heading">						
				<div id="page_header"></div>
				
			</header>
		</div>
		<div class="row">
			 <input type="hidden" name="visit_invoice_id" id="visit_invoice_id">
			 
			 <br>
			<div class="col-md-12">
					<div class="col-md-12" style="display:none;" id="search-invoice">
						
			
						<div class="col-md-6" >
						 	<div class="form-group">
				                <label class="col-md-4 control-label" style="color:red;">Service name:* </label>
				                
				                <div class="col-md-8">
				                    <input type="text" class="form-control" name="service_charge_name_search" id="service_charge_name_search" placeholder="Service Name" value="<?php echo set_value('service_charge_name');?>"  onkeyup="search_visit_invoice_records()">
				                </div>
				            </div>
						 </div>
						 <div class="col-md-2" >
						 	<?php
						 	if($open_status == 0)
							{
								?>
								
						  		<a class="btn btn-xs btn-warning pull-right"  onclick="invoice_details_view()" ><i class="fa fa-plus"></i> Bill Accoount</a>  
						  		<?php

					  		}
						 	?>
						 </div>
						 <div class="col-md-4">
						 	<a class="btn btn-xs btn-success pull-right"  data-toggle="modal" data-target="#add_payment_modal" ><i class="fa fa-plus"></i> Add Payment</a>  
						 </div>
					</div>
					<div class="col-md-12">
						<div class="col-lg-8" style="height: 67vh;overflow-y: scroll;">
							<div style="padding: 5px !important;">
								<div id="patient_bill"></div>
							</div>
							
						</div>

						<div class="col-lg-4">
							 <div  style="height: 25vh;overflow-y: scroll;">
							 	 <div id="payments-made"></div>

							 </div>
							  <div  style="height: 20vh;overflow-y: scroll;">
							  	<div id="waiver-made"></div>
							  </div>

							  <div  style="height: 20vh;overflow-y: scroll;">
							  	<div id="doctors-note"></div>
							  </div>
					         
						</div>
					</div>
					
					
			 
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="add_assessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Service</h4>
            </div>
            <?php echo form_open("accounts/add_service_item", array("class" => "form-horizontal"));?>
            <div class="modal-body">
            	<div class="row">
                	<div class='col-md-12'>
                      	<div class="form-group">
							<label class="col-lg-4 control-label">Service Name: </label>
						  
							<div class="col-lg-8">
								<select id='parent_service_id' name='parent_service_id' class='form-control custom-select ' >
			                      <option value=''>None - Please Select a service</option>
			                       <?php echo $services_items;?>
			                    </select>
							</div>
						</div>
						 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                      	<div class="form-group">
							<label class="col-lg-4 control-label">Charge Name: </label>
						  
							<div class="col-lg-5">
								<input type="text" class="form-control" name="service_charge_item" placeholder="" autocomplete="off">
							</div>
						</div>				
                      	<div class="form-group">
							<label class="col-lg-4 control-label">Service Amount: </label>
						  
							<div class="col-lg-5">
								<input type="number" class="form-control" name="service_amount" placeholder="" autocomplete="off" >
							</div>
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Service</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="add_invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Invoice</h4>
                </div>
                 <?php echo form_open("", array("class" => "form-horizontal",'id'=>"create_new_invoice"));?>
                <div class="modal-body">
                	<div class="row">
                    	<div class='col-md-10'>
                    		 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
	                        
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Invoice</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
                <?php echo form_close();?>
            </div>
        </div>
</div>
<div class="modal fade bs-example-modal-lg" id="add_provider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Provider</h4>
                </div>
                 <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal"));?>
                <div class="modal-body">
                	<div class="row">
                    	<div class='col-md-12'>
                          	<div class="form-group">
								<label class="col-lg-4 control-label">First Name: </label>
							  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
								<div class="col-lg-5">
									<input type="text" class="form-control" name="personnel_fname" placeholder="" autocomplete="off">
								</div>
							</div>
                          	<div class="form-group">
								<label class="col-lg-4 control-label">Other Names: </label>
							  
								<div class="col-lg-5">
									<input type="text" class="form-control" name="personnel_onames" placeholder="" autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-4 control-label">Phone Number: </label>
							  
								<div class="col-lg-5">
									<input type="text" class="form-control" name="personnel_phone" placeholder="" autocomplete="off">
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Provider</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
                <?php echo form_close();?>
            </div>
        </div>
</div>
<div class="modal fade bs-example-modal-lg" id="add_to_bill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add to Bill</h4>
            </div>
             <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal","id"=>"add_bill"));?>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-2 ">
            		</div>
		            	<div class="col-md-10 ">
		                    <div class="col-md-12" style="margin-bottom: 10px">
			                  <div class="form-group">
			                  <label class="col-md-2 control-label">Service: </label>
			                  	<div class="col-md-10">
				                    <select id='service_id_item' name='service_id' class='form-control custom-select ' >
				                      <option value=''>None - Please Select a service</option>
				                       <?php echo $services_list;?>
				                    </select>

				                    <input type="hidden" name="visit_id_checked" id="visit_id_checked">
			                    </div>
			                  </div>
			                </div>
			                <br>
			                <div class="col-md-12">
				                <div class="form-group">
									<label class="col-lg-2 control-label">Date: </label>
									
									<div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_date" id="visit_date_date" placeholder="Admission Date" value="<?php echo date('Y-m-d');?>">
                                        </div>
									</div>
								</div>
							</div>
			            </div>
			            <div class="col-md-12" style="margin:20px;">
			            	<div class="center-align">
								
							</div>
			            </div>
			        </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit' onclick="return confirm('Are you sure you want to add this procedure to bill ? ')" >Add to Bill</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="add_payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Payment to bill Balance <strong><span id="visit_balance"></span></strong></h4>
            </div>
             <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal","id"=>"add_payment"));?>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-2 ">
            		</div>
		            	<div class="col-md-10 ">
		            		<div class="form-group" style="display: none">
								<div class="col-lg-4">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="1"  onclick="getservices(1)" > 
                                            Normal
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="2"> 
                                            Debit Note
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="3"  onclick="getservices(1)"> 
                                            Waiver
                                        </label>
                                    </div>
								</div>
							</div>
							 
                           	<!-- <input type="hidden" name="type_payment" value="1"> -->
                           	<!-- <input type="hidden" name="service_id" id="service_id" value="0"> -->
                           	<div id="normal_div">
                           		<div id="service_div" class="form-group"  style="display: none;">
	                                <label class="col-lg-2 control-label"> Services: </label>
	                                
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="service_id" >
	                                    	<option value="">--Select a service--</option>
											<?php
	                                        $service_rs = $this->accounts_model->get_all_service();
	                                        $service_num_rows = count($service_rs);
	                                        if($service_num_rows > 0)
	                                        {
												foreach($service_rs as $service_res)
												{
													$service_id = $service_res->service_id;
													$service_name = $service_res->service_name;
													
													echo '<option value="'.$service_id.'">'.$service_name.'</option>';
												}
	                                        }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>

	                        <div class="col-md-12" style="margin-bottom: 10px">
	                        	<input type="hidden" name="provider_id" id="provider_id_item" value="0">

			                  
			                </div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Amount: </label>
								  
									<div class="col-lg-8">
										<input type="text" class="form-control" name="amount_paid" id="amount_paid" placeholder="" autocomplete="off"  onkeyup="get_change()">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-lg-2 control-label">Payment Method: </label>
									  
									<div class="col-lg-8">
										<select class="form-control" name="payment_method" id="payment_method" onchange="check_payment_type(this.value)">
											<option value="0">Select a group</option>
	                                    	<?php
											  $method_rs = $this->accounts_model->get_payment_methods();
											  $num_rows = count($method_rs);
											 if($num_rows > 0)
											  {
												
												foreach($method_rs as $res)
												{
												  $payment_method_id = $res->payment_method_id;
												  $payment_method = $res->payment_method;
												  
													echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
												  
												}
											  }
										  ?>
										</select>
									  </div>
								</div>
                           		
                           	</div>
                           	<div id="waiver_div" style="display: none;">

                           		<div id="service_div" class="form-group" >
	                                <label class="col-lg-2 control-label"> Services: </label>
	                                
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="waiver_service_id" >
	                                    	<option value="">--Select a service--</option>
											<?php
	                                        $service_rs = $this->accounts_model->get_all_service();
	                                        $service_num_rows = count($service_rs);
	                                        if($service_num_rows > 0)
	                                        {
												foreach($service_rs as $service_res)
												{
													$service_id = $service_res->service_id;
													$service_name = $service_res->service_name;
													
													echo '<option value="'.$service_id.'">'.$service_name.'</option>';
												}
	                                        }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>

	                         <div class="col-md-12" style="margin-bottom: 10px">
			                  <div class="form-group " >
			                  <label class="col-md-2 control-label">Provider: </label>
			                  	<div class="col-md-10">
				                    <select id='provider_id_item' name='provider_id' class='form-control custom-select ' >
				                      <option value=''>None - Please Select a provider</option>
				                      <?php
									
											if(count($doctor) > 0){
												foreach($doctor as $row):
													$fname = $row->personnel_fname;
													$onames = $row->personnel_onames;
													$personnel_id = $row->personnel_id;
													
													if($personnel_id == set_value('personnel_id'))
													{
														echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
													}
													
													else
													{
														echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
													}
												endforeach;
											}
										?>
				                    </select>
				                </div>
			                  </div>
			                </div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Amount: </label>
								  
									<div class="col-lg-8">
										<input type="text" class="form-control" name="waiver_amount" id="waiver_amount" placeholder="" autocomplete="off"  onkeyup="get_change()">
									</div>
								</div>
								
                           		
                           	</div>                          							
	                        

							<input type="hidden" class="form-control" name="change_payment" id="change_payment" placeholder="" autocomplete="off" >
							<div id="mpesa_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Mpesa TX Code: </label>

								<div class="col-lg-8">
									<input type="text" class="form-control" name="mpesa_code" id="mpesa_code" placeholder="">
								</div>
							</div>
						  
							<div id="insuarance_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Credit Card Detail: </label>
								<div class="col-lg-8">
									<input type="text" class="form-control" name="insuarance_number" id="insuarance_number" placeholder="">
								</div>
							</div>
						  
							<div id="cheque_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Cheque Number: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="cheque_number" id="cheque_number" placeholder="">
								</div>
							</div>
							<div id="bank_deposit_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Deposit Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="deposit_detail" id="deposit_detail" placeholder="">
								</div>
							</div>
							<div id="debit_card_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Debit Card Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="debit_card_detail" id="debit_card_detail" placeholder="">
								</div>
							</div>
						  
							<div id="username_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Username: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="username" id="username" placeholder="">
								</div>
							</div>
						  
							<div id="password_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Password: </label>
							  
								<div class="col-lg-8">
									<input type="password" class="form-control" name="password" id="password" placeholder="">
								</div>
							</div>
			            </div>
			           <input type="hidden" name="visit_id_payments" id="visit_id_payments">
			        </div>
            </div>
            <div class="modal-footer">
            	<h4 class="pull-left" > Change : <span id="change_item"></span></h4>
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Payment</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>


	
<input type="hidden" name="visit_discharge_visit" id="visit_discharge_visit">
 <div class="modal fade bs-example-modal-lg" id="change_patient_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Patient Invoice</h4>
            </div>
           
            <div class="modal-body">
            	<div class="row" id="update-invoice-view">
                	
                </div>
                 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                 <input type="hidden" name="visit_id_visit" id="visit_id_visit">
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Change Type</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
           
        </div>
    </div>
</div>
<script type="text/javascript">
	function get_visit_detail(visit_id,visit_invoice_id)
	{
		// alert(visit_id);
		
		document.getElementById("visit_id_checked").value = visit_id;
		document.getElementById("visit_id_payments").value = visit_id;
		document.getElementById("visit_id_visit").value = visit_id;
		document.getElementById("visit_discharge_visit").value = visit_id;
		document.getElementById("visit_invoice_id").value = visit_invoice_id;

		window.localStorage.setItem('visit_invoice_id',visit_invoice_id);

		display_patient_bill(visit_id);


	}
	function get_next_page(page,visit_id)
	{
		// alert()
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_visits_div/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visits_div").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}

	function get_next_invoice_page(page,visit_id)
	{
		// alert(page);
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}

	function remove_charged_item(visit_charge_id,visit_id)
	{

		// var visit_charge_id = $('#v_procedure_id').val();

		// alert(visit_charge_id);	
		var remove_description = $('#remove_description'+visit_charge_id).val();
		// alert(remove_description);	
		// var visit_id = $('#visit_to_charge').val();
		var url = "<?php echo base_url();?>accounts/cancel_charge/"+visit_charge_id+"/"+visit_id;	

		// alert(url);	
		$.ajax({
		type:'POST',
		url: url,
		data:{remove_description: remove_description},
		dataType: 'json',
		success:function(data){
			
			alert(data.message);
			if(data.status == "success")
			{
				$('#remove_charge'+visit_charge_id).modal('toggle');
		 		display_patient_bill(visit_id);
			}
		 	
		},
		error: function(xhr, status, error) {
		// alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		// return false;
	};

	function get_next_payments_page(page,visit_id)
	{
		// alert(page);
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_receipt/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}


	function get_page_header(visit_id,visit_invoice_id=0)
	{
		// alert()
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      window.localStorage.setItem('visit_invoice_id',visit_invoice_id);
      var patient_id = window.localStorage.getItem('visit_invoice_id');
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_details_header/"+visit_id+"/"+visit_invoice_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("page_header").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

      get_invoice_item_update(patient_id,visit_id,visit_invoice_id);

	}
</script>

<script type="text/javascript">

  $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   		// alert(<?php echo $num_pages;?>)

   		window.localStorage.setItem('patient_id',<?php echo $patient_id?>);

   		get_all_visits_div(<?php echo $patient_id;?>);
   		// display_patient_bill(<?php echo $visit_id;?>);
   });
     
  function getservices(id){

		var type_payment =  $("input[name='type_payment']:checked").val();

        // var myTarget1 = document.getElementById("service_div");
        var myTarget5 = document.getElementById("normal_div");
        var myTarget6 = document.getElementById("waiver_div");
		// alert(id);
        if(type_payment == 1)
        {
          myTarget6.style.display = 'none';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget6.style.display = 'block';
          myTarget5.style.display = 'none';
        }
        
  }



  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget4 = document.getElementById("debit_card_div");

    var myTarget5 = document.getElementById("bank_deposit_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 7)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'block';
    }
    else if(payment_type_id == 8)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'block';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 6)
    {
       myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';  
    }

  }

   function display_patient_bill(visit_id){

   	// alert(visit_id);
      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');

       window.localStorage.setItem('visit_id',visit_id);
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id+"/"+visit_invoice_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;

                 get_services_offered(visit_id);
                 get_patient_receipt(visit_id);
      			 get_page_header(visit_id,visit_invoice_id);
      			  get_visit_balance(visit_id);
      			  get_patient_visit_invoice_credits(visit_id);
      			  get_patient_visit_invoice_doctors_notes(visit_id);

      			var myTarget3 = document.getElementById("search-invoice");
				myTarget3.style.display = 'block';
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

      
  }
  function get_services_offered(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_services_billed/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("billed_services").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
      get_page_header(visit_id,visit_invoice_id);
  }
  function get_visit_balance(visit_id){

  		var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
		var url = "<?php echo base_url();?>accounts/get_visit_balance/"+visit_id+"/"+visit_invoice_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: visit_id},
		dataType: 'json',
		success:function(data){
			var balance = data.balance;

			$('#visit_balance').html("Kes."+data.balance);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			
		}
		});
		return false;
  }

  function get_patient_receipt(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_receipt/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
       var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
      get_page_header(visit_id,visit_invoice_id);
  }
  function get_all_visits_div(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_visits_div/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visits_div").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	     
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  

	    grand_total(id, units, billed_amount, v_id);
	}
	function grand_total(procedure_id, units, amount, v_id){
    	var config_url = document.getElementById("config_url").value;
    	var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: v_id},
		dataType: 'json',
		success:function(data){
			alert(data.message);
			display_patient_bill(v_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(v_id);
		}
		});
		return false;

	    
	   
	}
	function delete_service(id, visit_id){

		var res = confirm('Are you sure you want to delete this charge?');
     
	    if(res)
	    {

	    	var config_url = document.getElementById("config_url").value;
	    	var url = config_url+"accounts/delete_service_billed/"+id+"/"+visit_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{visit_id: visit_id,id: id},
			dataType: 'json',
			success:function(data){
				alert(data.message);
				display_patient_bill(visit_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				display_patient_bill(visit_id);
			}
			});
			return false;
		    var XMLHttpRequestObject = false;
		        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"accounts/delete_service_billed/"+id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                display_patient_bill(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
		
		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}

	$(document).on("submit","form#add_bill",function(e)
	{
		e.preventDefault();	

		var service_id = $('#service_id_item').val();
		var provider_id = $('#provider_id_item').val();
		var visit_date = $('#visit_date_date').val();
		var visit_id = $('#visit_id_checked').val();
		var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id+"/"+visit_invoice_id;
		
		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'json',
		success:function(data){

			alert(data.message);
		 	$('#add_to_bill').modal('toggle');
			display_patient_bill(visit_id);
			var patient_id = window.localStorage.getItem('patient_id');
			get_all_visits_div(patient_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	});

	$(document).on("submit","form#visit_type_change",function(e)
	{
		e.preventDefault();	

		var form_data = new FormData(this);


		// var visit_type_id = $('#visit_type_id').val();
		var visit_id = $('#billing_patient_id').val();
		// var visit_id = window.localStorage.getItem('visit_id');
		// alert(visit_id);
		var url = "<?php echo base_url();?>accounts/change_patient_visit/"+visit_id;
		
		$.ajax({
		type:'POST',
		url: url,
		data:form_data,
		dataType: 'text',
		processData: false,
		contentType: false,
		success:function(data){
			// alert("You have successfully changed patient type");
		 	// $('#change_patient_type').modal('toggle');
		 	close_side_bar();
		 	var patient_id = window.localStorage.getItem('patient_id');
		 	get_all_visits_div(patient_id);
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	});

	$(document).on("submit","form#discharge-patient",function(e)
	{
		e.preventDefault();	

		var res = confirm('Are you sure you want to close the visits ?');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		
			// var visit_date_charged = $('#visit_date_charged').val();
			var visit_id = $('#visit_id').val();
			var admission_date = $('#admission_date').val();
			var discharge_date = $('#discharge_date').val();
			// var visit_id = $('#visit_discharge_visit').val();
			var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
			var patient_id = window.localStorage.getItem('patient_id');
			var url = "<?php echo base_url();?>accounts/discharge_patient/"+visit_id+"/"+visit_invoice_id;
			
			$.ajax({
			type:'POST',
			url: url,
			data:{admission_date: admission_date,discharge_date:discharge_date},
			dataType: 'text',
			success:function(data){
				alert("Visit successfully closed");
				close_side_bar();
			 	// $('#end_visit_date').modal('toggle');
			 	var patient_id = window.localStorage.getItem('patient_id');
				get_all_visits_div(patient_id);
				display_patient_bill(visit_id);
				window.location.href = config_url+"accounts/payments/"+patient_id;
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				display_patient_bill(visit_id);
			}
			});
			return false;
		}
	});

	$(document).on("submit","form#payments-paid-form",function(e)
	{
		// alert("changed");
		e.preventDefault();	

		var cancel_action_id = $('#cancel_action_id').val();
		var cancel_description = $('#cancel_description').val();
		var visit_id = $('#visit_id').val();
		var payment_id = $('#payment_id').val();
		var url = "<?php echo base_url();?>accounts/cancel_payment/"+payment_id+"/"+visit_id;		
		$.ajax({
		type:'POST',
		url: url,
		data:{cancel_description: cancel_description, cancel_action_id: cancel_action_id},
		dataType: 'text',
		success:function(data){
			alert("You have successfully cancelled a payment");
		 	$('#refund_payment'+visit_id).modal('toggle');
		 	 var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
		 	get_page_header(visit_id,visit_invoice_id);
			get_patient_receipt(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			get_page_header(visit_id,visit_invoice_id);
			get_patient_receipt(visit_id);
		}
		});
		return false;
	});


	$(document).on("submit","form#add_payment",function(e)
	{
		e.preventDefault();	

		var payment_method = $('#payment_method').val();
		var amount_paid = $('#amount_paid').val();
		var type_payment =  $("input[name='type_payment']:checked").val(); //$('#type_payment').val();
		// alert(amount_paid); die();
		var service_id = $('#service_id').val();
		var waiver_amount = $('#waiver_amount').val();
		var waiver_service_id = $('#waiver_service_id').val();		
		var cheque_number = $('#cheque_number').val();
		var insuarance_number = $('#insuarance_number').val();
		var mpesa_code = $('#mpesa_code').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var change_payment = $('#change_payment').val();

		var debit_card_detail = $('#debit_card_detail').val();
		var deposit_detail = $('#deposit_detail').val();
		var password = $('#password').val();


		var visit_id = $('#visit_id_payments').val();

		var payment_service_id = $('#payment_service_id').val();

		
		var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
		var patient_id = window.localStorage.getItem('patient_id');
		var url = "<?php echo base_url();?>accounts/make_payments/"+visit_id+"/"+visit_invoice_id;
		// alert(patient_id);

		$.ajax({
		type:'POST',
		url: url,
		data:{payment_method: payment_method, amount_paid: amount_paid, type_payment: type_payment,service_id: service_id, cheque_number: cheque_number, insuarance_number: insuarance_number, mpesa_code: mpesa_code,username: username,password: password, payment_service_id: payment_service_id,debit_card_detail: debit_card_detail,deposit_detail: deposit_detail,change_payment:change_payment,waiver_amount: waiver_amount, waiver_service_id,visit_invoice_id:visit_invoice_id,patient_id:patient_id},
		dataType: 'json',
		success:function(data){

			if(data.result == 'success')
        	{
				alert(data.message);

			 	$('#add_payment_modal').modal('toggle');
			 	 var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
			 	get_page_header(visit_id,visit_invoice_id);
      			get_patient_receipt(visit_id,null);
      			get_all_visits_div(patient_id);
				 
			}
			else
			{
				alert(data.message);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	});
	function close_visit(visit_id)
	{
		var res = confirm('Are you sure you want to put the Discharge In?');
     
	    if(res)
	    {
	    	var url = "<?php echo base_url();?>accounts/close_visit/"+visit_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{visit_id: visit_id},
			dataType: 'json',
			success:function(data){
				alert(data.message);
				setTimeout(function() {
					send_message(visit_id);
				  }, 2000);
				display_patient_bill(visit_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				display_patient_bill(visit_id);
			}
			});
			return false;

	    }
	}
	function send_message(visit_id)
	{
		var url = "<?php echo base_url();?>accounts/send_message/"+visit_id;
		// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{visit_id: visit_id},
			dataType: 'json',
			success:function(data){

				window.location.href = "<?php echo base_url();?>queues/inpatient-queue";
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				
			}
			});
			return false;
	}
	function get_change()
	{

		var visit_id = $('#visit_id_payments').val();
	
		var amount_paid = $('#amount_paid').val();
		var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
		var url = "<?php echo base_url();?>accounts/get_change/"+visit_id+"/"+visit_invoice_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: visit_id, amount_paid: amount_paid},
		dataType: 'json',
		success:function(data){
			var change = data.change;

			document.getElementById("change_payment").value = change;
			$('#change_item').html("Kes."+data.change);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}
	// $(document).on("submit","form#discharge-patient",function(e)
	// {
	// 	e.preventDefault();	

	// 	var visit_date_charged = $('#visit_date_charged').val();
	// 	var visit_id = $('#visit_discharge_visit').val();
	// 	var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
	// 	var url = "<?php echo base_url();?>accounts/discharge_patient/"+visit_id+"/"+visit_invoice_id;
		
	// 	$.ajax({
	// 	type:'POST',
	// 	url: url,
	// 	data:{visit_date_charged: visit_date_charged},
	// 	dataType: 'json',
	// 	success:function(data){
	// 		alert(data.message);
	// 	 	$('#end_visit_date').modal('toggle');
	// 	 	var patient_id = window.localStorage.getItem('patient_id');
	// 		get_all_visits_div(patient_id);
	// 		display_patient_bill(visit_id);
	// 	},
	// 	error: function(xhr, status, error) {
	// 	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	// 		display_patient_bill(visit_id);
	// 	}
	// 	});
	// 	return false;
	// });

	function get_invoice_item_update(patient_id,visit_id,visit_invoice_id)
	{


		var config_url = $('#config_url').val();
	 	var url = config_url+"accounts/update_inpatient_invoice/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#update-invoice-view").html(data.result);
				 $('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}





function invoice_details_view()
{
	var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
	var visit_id = window.localStorage.getItem('visit_id');
	var patient_id = window.localStorage.getItem('patient_id');
	// document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 
	open_sidebar();
	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}


function get_procedures_done(patient_id,visit_id,visit_type_id,visit_invoice_id =null)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/search_inpatient_procedures/"+patient_id+"/"+visit_id+"/"+visit_type_id+"/"+visit_invoice_id;
	// window.alert(data_url);
	$('#charges-div').css('display', 'block');

	$('#items-div').css('display', 'none');

	window.localStorage.setItem('patient_id',patient_id);
	window.localStorage.setItem('visit_id',visit_id);
	window.localStorage.setItem('visit_type_id',visit_type_id);
	window.localStorage.setItem('visit_invoice_id',visit_invoice_id);

	var lab_test = $('#search_procedures').val();
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : lab_test},
	dataType: 'text',
	success:function(data){
		$("#searched-procedures").html(data);
	},
	error: function(xhr, status, error) {
		alert(error);
	}

	});
}

function add_inpatient_bill(service_charge_id,visit_id,patient_id,visit_type_id)
{

	$('#items-div').css('display', 'block');

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_service_charge_details/"+service_charge_id;
	// window.alert(data_url);
	

	window.localStorage.setItem('service_charge_id',service_charge_id);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);
		
		$("#service-name").html(data.service_charge_name);
		$('#charges-div').css('display', 'block');
		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
	},
	error: function(xhr, status, error) {
		alert(error);
	}

	});
	

}


$(document).on("submit","form#add-bill-inpatient",function(e)
{
	e.preventDefault();	

	var patient_id = window.localStorage.getItem('patient_id');
	var visit_id  = window.localStorage.getItem('visit_id');
	var visit_type_id  = window.localStorage.getItem('visit_type_id');
	var visit_invoice_id  = window.localStorage.getItem('visit_invoice_id');
	var service_charge_id =   window.localStorage.getItem('service_charge_id');


	var service_id = service_charge_id;
	var provider_id = $('#personnel_id').val();
	var visit_date = $('#date_to_invoice').val();
	var units_charged = $('#units_charged').val();
	var visit_id = visit_id;



	var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
	var url = "<?php echo base_url();?>accounts/add_inpatient_bill/"+visit_id+"/"+visit_invoice_id;
	
	$.ajax({
	type:'POST',
	url: url,
	data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date,visit_type_id: visit_type_id,units_charged:units_charged},
	dataType: 'json',
	success:function(data){

		alert(data.message);
		close_side_bar();
		display_patient_bill(visit_id);
		get_all_visits_div(patient_id);
	},
	error: function(xhr, status, error) {
	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		display_patient_bill(visit_id);
	}
	});
	return false;
});


function open_billing_details(visit_id,visit_invoice_id,patient_id)
{
	// close_side_bar();
	open_sidebar();

	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/update_inpatient_invoice/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);

		document.getElementById("current-sidebar-div").style.display = "block"; 


		$("#current-sidebar-div").html(data.result);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}


function open_waiver_details(visit_id,visit_invoice_id,patient_id)
{
	open_sidebar();

	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/update_waiver_details/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	document.getElementById("current-sidebar-div").style.display = "none"; 
	$("#current-sidebar-div").html('');
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);

		document.getElementById("current-sidebar-div").style.display = "block"; 


		$("#current-sidebar-div").html(data.result);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		get_waiver_info_details(visit_id,visit_invoice_id,patient_id);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}

 
function add_visit_invoice_discount(visit_id,visit_invoice_id,service_id,patient_id)
{

	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/update_waiver_details/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);

		document.getElementById("current-sidebar-div").style.display = "block"; 


		$("#current-sidebar-div").html(data.result);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });

		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function search_visit_invoice_records()
{
	var config_url = $('#config_url').val();

	var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
	var visit_id = window.localStorage.getItem('visit_id');

    var data_url = config_url+"accounts/view_patient_bill/"+visit_id+"/"+visit_invoice_id;




	var lab_test = $('#service_charge_name_search').val();
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : lab_test},
	dataType: 'text',
	success:function(data){
		$("#patient_bill").html(data);
	},
	error: function(xhr, status, error) {
		alert(error);
	}

	});
}



function assign_to_invoice(patient_id,visit_id,visit_charge_id)
{

		open_sidebar();


		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/allocate_to_invoice/"+patient_id+"/"+visit_id+"/"+visit_charge_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#sidebar-div").html(data);

			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


}

function assign_charge_to_invoice(visit_invoice_id,visit_charge_id,patient_id)
{

	var res = confirm('Are you sure you want to confirm this charge ?');

	var visit_id = window.localStorage.getItem('visit_id');
	if(res)
	{

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/assign_charge_to_invoice/"+visit_invoice_id+"/"+visit_charge_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			close_side_bar();
			display_patient_bill(visit_id);
			get_all_visits_div(patient_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

	}
	
}




// edit invoice

function edit_charge_invoice(patient_id,visit_charge_id,service_charge_id,visit_id)
{

		open_sidebar();


		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/edit_charge_invoice/"+patient_id+"/"+visit_id+"/"+visit_charge_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#sidebar-div").html(data);

			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


}

$(document).on("submit","form#update-bill-inpatient",function(e)
{
	// alert("changed");
	e.preventDefault();	

	var form_data = new FormData(this);

	var res = confirm('Are you sure you want to update this item ?');

	if(res)
	{
		var config_url = $('#config_url').val();
		// var bill_visit = $('#bill_visit').val();
		var url = "<?php echo base_url();?>accounts/update_accounts_billing";		
		
		$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
				close_side_bar();
			 	// display_patient_bill(v_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				
			}
		});

		var patient_id = window.localStorage.getItem('patient_id');
		var visit_id = window.localStorage.getItem('visit_id');
		 get_all_visits_div(patient_id);
		 display_patient_bill(visit_id);
		return false;

	}
	
});



function discharge_patient(visit_id,visit_invoice_id,patient_id)
{
	// close_side_bar();
	open_sidebar();

	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/discharge_inpatient/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#sidebar-div").html(data);

			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
}


function add_new_invoice(visit_id,patient_id)
{
	// close_side_bar();
	open_sidebar();

	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/add_new_invoice/"+patient_id+"/"+visit_id;

	$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#sidebar-div").html(data);

			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
}



$(document).on("change","select#visit_type_id2",function(e)
{
    var visit_type_id = $(this).val();

    // alert("sasa");

    if(visit_type_id == '1')
    {
        $('#insured_company').css('display', 'none');
    }
    else
    {
        // alert('sas');
        $('#insured_company').css('display', 'block');
    }
    

    var patient_id = window.localStorage.getItem('patient_id');
    

    
    
    
    var config_url = $('#config_url').val();

        
    var url = config_url+"reception/get_patient_accounts/"+patient_id+"/"+visit_type_id;

    $.ajax({
    type:'POST',
    url: url,
    data:{patient_id: patient_id,visit_type_id: visit_type_id},
    dataType: 'text',
    success:function(data){
      var data = jQuery.parseJSON(data);
        
         $(".scheme_name").val(data.scheme_name);
         $(".member_number").val(data.member_number);
         $(".principal_member").val(data.principal_member);
         $(".relation").val(data.relation);
     

    },
    error: function(xhr, status, error) {
    alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

    }
    });

});


$(document).on("submit","form#create_new_invoice",function(e)
{
	// alert("changed");
	e.preventDefault();	

	var form_data = new FormData(this);

	var res = confirm('Are you sure you want to add a new invoice ?');

	if(res)
	{
		var config_url = $('#config_url').val();
		// var bill_visit = $('#bill_visit').val();
		var url = "<?php echo base_url();?>accounts/create_new_invoice";		
		
		$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
				// close_side_bar();
			 	// display_patient_bill(v_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				
			}
		});

		var patient_id = window.localStorage.getItem('patient_id');
		var visit_id = window.localStorage.getItem('visit_id');
		 get_all_visits_div(patient_id);
		 // display_patient_bill(visit_id);
		return false;

	}


});


function get_waiver_info_details(visit_id,visit_invoice_id,patient_id)
{
	tinymce.remove();
	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/get_waiver_details/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);

		$("#discounts-page").html(data.result);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}

 function get_patient_visit_invoice_credits(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
       var patient_id = window.localStorage.getItem('patient_id');
       var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
       // var visit_id = window.localStorage.getItem('visit_id');
      var url = config_url+"accounts/get_all_credit_notes/"+patient_id+"/"+visit_invoice_id+"/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("waiver-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
       var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
      get_page_header(visit_id,visit_invoice_id);
  }


  function delete_credit_note(visit_credit_note_id, patient_id, visit_invoice_id) {
    var res = confirm('Are you sure you want to complete deleting this credit note ?');
    var config_url = $('#config_url').val();
    var url = config_url + "accounts/delete_credit_note/" + visit_credit_note_id + "/" + patient_id + "/" +visit_invoice_id;
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            visit_credit_note_id: visit_credit_note_id
        },
        dataType: 'text',
        success: function(data) {
            var data = jQuery.parseJSON(data);
            if (data.message == "success") {
                close_side_bar();
                get_patient_statement(patient_id);
            } else {
                alert('Sorry could not delete this credit note please try again');
            }
        },
        error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" +
                error);
        }
    });
    var patient_id = window.localStorage.getItem('patient_id');
	var visit_id = window.localStorage.getItem('visit_id');
    get_all_visits_div(patient_id);
	display_patient_bill(visit_id);
}


function get_patient_visit_invoice_doctors_notes(visit_id)
{

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
       var patient_id = window.localStorage.getItem('patient_id');
       var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
      var url = config_url+"accounts/get_all_doctors_summary_notes/"+patient_id+"/"+visit_invoice_id+"/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("doctors-note").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      //  var visit_invoice_id = window.localStorage.getItem('visit_invoice_id');
      // get_page_header(visit_id,visit_invoice_id);
      // get_all_notes_trail(visit_id,visit_invoice_id,patient_id);
  }



function open_doctors_notes_details(visit_id,visit_invoice_id,patient_id)
{
	open_sidebar();

	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/update_invoice_doctors_notes/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	document.getElementById("current-sidebar-div").style.display = "none"; 
	$("#current-sidebar-div").html('');
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);

		document.getElementById("current-sidebar-div").style.display = "block"; 


		$("#current-sidebar-div").html(data.result);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		get_all_notes_trail(visit_id,visit_invoice_id,patient_id);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}

function get_all_notes_trail(visit_id,visit_invoice_id,patient_id)
{
	tinymce.remove();
	var config_url = $('#config_url').val();
	// var data_url = config_url+"accounts/get_visit_invoices_inpatient/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	var data_url = config_url+"accounts/get_all_notes_trail/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;

	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	processData: false,
	contentType: false,
	success:function(data){
		var data = jQuery.parseJSON(data);

		$("#trail-page").html(data.result);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}








</script>

