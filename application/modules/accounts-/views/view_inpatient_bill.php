<?php

$visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 0;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$close_card = $value->close_card;
		$visit_type_id = $value->visit_type;
		$invoice_number = $value->invoice_number;
		$visit_time_out = $value->visit_time_out;
		$parent_visit = $value->parent_visit;
		$patient_id = $value->patient_id;
		$visit_type_id = $value->visit_type_id;
		$insurance_description = $value->insurance_description;
		$insurance_number = $value->insurance_number;
		$copay_percentage = 0;//$value->copay_percentage;
		
		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
	}
}
?>

<div class="col-md-12" style="margin-top: 10px;">
	<div class="row">
		<div class="col-md-12"> 
			<a  class="btn btn-info btn-sm pull-right"  onclick="invoice_details_view(<?php echo $visit_invoice_id;?>,<?php echo $visit_id;?>,<?php echo $patient_id;?>)"> Add Item to Bill </a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">   	    
			<?php

		echo "
		<table align='center' class='table table-striped table-hover table-condensed table-linked'>
			<tr>
				<th>#</th>
				<th style='width:10%'>Date</th>
				<th style='width:10%'>Time</th>
				<th style='width:10%'>Department</th>
				<th style='width:20%'>Services/Items</th>
				<th style='width:10%'>Units</th>
				<th style='width:15%'>Unit Cost (Ksh)</th>
				<th style='width:10%'> Total</th>
				<th></th>
				<th></th>
			</tr>		
		"; 

		$total= 0; 
		$count = 0;
		

		$num_pages = $total_rows/$per_page;

		if($num_pages < 1)
		{
			$num_pages = 0;
		}
		$num_pages = round($num_pages);

		if($page==0)
		{
			$counted = 0;
		}
		else if($page > 0)
		{
			$counted = $per_page*$page;
		}
		// var_dump($invoice_items->num_rows()); die();
		$sub_total= 0; 
		$personnel_query = $this->personnel_model->retrieve_personnel();
			
		if($invoice_items->num_rows() >0){
			
			$visit_date_day = '';
			foreach ($invoice_items->result() as $value => $key1):
				$v_procedure_id = $key1->visit_charge_id;
				$procedure_id = $key1->service_charge_id;
				$service_name = $key1->service_name;
				$date = $key1->date;
				$time = $key1->time;
				$visit_charge_timestamp = $key1->visit_charge_timestamp;
				$visit_charge_amount = $key1->visit_charge_amount;
				$units = $key1->visit_charge_units;
				$procedure_name = $key1->service_charge_name;
				$service_id = $key1->service_id;
				$provider_id = $key1->provider_id;
			
				$sub_total= $sub_total +($units * $visit_charge_amount);
				$visit_date = date('l d F Y',strtotime($date));
				$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
				
				if($visit_date_day != $visit_date)
				{
					
					$visit_date_day = $visit_date;
				}
				else
				{
					$visit_date_day = '';
				}

				// echo 'asdadsa'.$visit_date_day;

				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id = $adm->personnel_id;
						

						if($personnel_id == $provider_id)
						{
							$provider_id = '[ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';


							$procedure_name = $procedure_name.$provider_id;
						}
						
						
						
					}
				}
				
				else
				{
					$provider_id = '';
				}


				$can_change = $this->accounts_model->check_if_can_change();


                            if($can_change)
                            {
                                $buttons = "
                                            <td>
                                                <button type='button' class='btn btn-sm btn-default' data-toggle='modal' data-target='#remove_charge".$v_procedure_id."'><i class='fa fa-times'></i></button>
                                                <div class='modal fade' id='remove_charge".$v_procedure_id."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                                                    <div class='modal-dialog' role='document'>
                                                        <div class='modal-content'>
                                                            <div class='modal-header'>
                                                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                                                <h4 class='modal-title' id='myModalLabel'>Remove Charge</h4>
                                                            </div>
                                                            <div class='modal-body'>
                                                                
                                                                <p>Charge Name : ".$procedure_name." </p>   
                                                                <p>Charge Amount : KSH. ".$visit_charge_amount." </p>                                                         
                                                                <div class='form-group'>
                                                                    <label class='col-md-4 control-label'>Description: </label>
                                                                    
                                                                    <div class='col-md-8'>
                                                                        <textarea class='form-control' id='remove_description".$v_procedure_id."' name='remove_description' ></textarea>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class='row'>
                                                                    <div class='col-md-8 col-md-offset-4'>
                                                                        <div class='center-align'>
                                                                            <button type='submit' class='btn btn-primary' onclick='remove_charged_item(".$v_procedure_id.",".$visit_id.",".$visit_invoice_id.")'>Remove Charge</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class='modal-footer'>
                                                                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>

                                            ";
                                
                            }
                            else
                            {
                                $buttons = '';
                            }





				$counted++;
				echo"
						<tr> 
							<td>".$counted."</td>
							<td>".$date."</td>
							<td >".$visit_time."</td>										
							<td >".$service_name."</td>
							<td >".$procedure_name."</td>
							<td align='center'>
								<input type='text' id='units".$v_procedure_id."' class='form-control' value='".$units."' size='3' />
							</td>
							<td align='center'><input type='text' id='billed_amount".$v_procedure_id."' class='form-control'  size='5' value='".$visit_charge_amount."'></div>
							</td>
							
							<td >".number_format($visit_charge_amount*$units)."</td>
							<td>
							<a class='btn btn-sm btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
							</td>
							<td>
								<a class='btn btn-sm btn-danger'  onclick='delete_service(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
							</td>
							".$buttons."
						</tr>	
				";

				$visit_date_day = $visit_date;
				endforeach;
				

		}		
		echo"
		 </table>
		";
		?>
	
		</div>
	</div>
</div>


	