<?php
class Accounts_model extends CI_Model 
{
	public function payments2($visit_id)
	{
		$table = "payments";
		$where = "payments.visit_id =". $visit_id;
		$items = "payments.amount_paid,payments.payment_type";
		$order = "amount_paid";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		$total = 0;
		
		if(count($result) > 0){
			foreach ($result as $row2):
				$payment_type = $row2->payment_type;
				if($payment_type == 1)
				{
					$amount_paid = $row2->amount_paid;
					$total = $total + $amount_paid;
				}
			endforeach;
		}
		
		else{
			$total = 0;
		}
		
		$value = $total;
		
		return $value;
	}
	public function total_invoice_inpatient($visit_id)
	{
		 $item_invoiced_rs = $this->get_patient_visit_charge_items_old($visit_id);
         $credit_note_amount = $this->get_sum_credit_notes($visit_id);
         $debit_note_amount = $this->get_sum_debit_notes($visit_id);
         $total = 0;
          $total_amount =  0;
          if(count($item_invoiced_rs) > 0){
            $s=0;
            
            foreach ($item_invoiced_rs as $key_items):
              $s++;
			  $visit_total = 0;
			  // $service_id = $key_items->service_id;
			  $service_charge_id = $key_items->service_charge_id;
              $service_charge_name = $key_items->service_charge_name;
              $visit_charge_amount = $key_items->visit_charge_amount;
              // $service_name = $key_items->service_name;
              $units = $key_items->visit_charge_units;
			  
			  //If pharmacy
			 //  	if($service_id == 4)
				// {
				// 	if($this->accounts_model->in_pres($service_charge_id, $visit_id))
				// 	{
				// 		$visit_total = $visit_charge_amount * $units;
				// 	}
				// }
				
				// else
				// {
					$visit_total = $visit_charge_amount * $units;
				// }
             // $visit_total = $visit_charge_amount * $units;
              $total = $total + $visit_total;
            endforeach;
            $total_amount = $total;
          }
          else
          {
          	$total_amount = 0;
          }
          $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
          return $total_amount;
	}
	public function total_invoice($visit_id)
	{
		 $item_invoiced_rs = $this->get_patient_visit_charge_items($visit_id);
         $credit_note_amount = $this->get_sum_credit_notes($visit_id);
         $debit_note_amount = $this->get_sum_debit_notes($visit_id);
         $total = 0;
          $total_amount =  0;
          if(count($item_invoiced_rs) > 0){
            $s=0;
            
            foreach ($item_invoiced_rs as $key_items):
              $s++;
			  $visit_total = 0;
			  $service_id = $key_items->service_id;
			  $service_charge_id = $key_items->service_charge_id;
              $service_charge_name = $key_items->service_charge_name;
              $visit_charge_amount = $key_items->visit_charge_amount;
              $service_name = $key_items->service_name;
              $units = $key_items->visit_charge_units;
			  
			  //If pharmacy
			  	
					$visit_total = $visit_charge_amount * $units;
		
             // $visit_total = $visit_charge_amount * $units;
              $total = $total + $visit_total;
            endforeach;
            $total_amount = $total;
          }
          else
          {
          	$total_amount = 0;
          }
          $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
          return $total_amount;
	}

	public function total_payments_inpatient($visit_id)
	{
	      $payments_rs = $this->accounts_model->payments_inpatient($visit_id);
	      $total_payments = 0;
	      
	      if(count($payments_rs) > 0)
	      {
	        $x=0;
	        
	          foreach ($payments_rs as $key_items):
	            $x++;
	                $payment_type = $key_items->payment_type;
	                $payment_status = $key_items->payment_status;
	                if($payment_type == 1 && $payment_status ==1)
	                {
	                  $payment_method = $key_items->payment_method;
	                  $amount_paid = $key_items->amount_paid;
	                  
	                  $total_payments = $total_payments + $amount_paid;
	                }
	          endforeach;
	                    
	      }
	      else
	      {
	      	$total_payments = 0;
	      }
	      return $total_payments;
	}

		public function payments_inpatient($visit_id){
		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =". $visit_id;
		$items = "*";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function total_payments($visit_id)
	{
	      $payments_rs = $this->accounts_model->payments($visit_id);
	      $total_payments = 0;
	      
	      if(count($payments_rs) > 0)
	      {
	        $x=0;
	        
	          foreach ($payments_rs as $key_items):
	            $x++;
	                $payment_type = $key_items->payment_type;
	                $payment_status = $key_items->payment_status;
	                if($payment_type == 1 && $payment_status ==1)
	                {
	                  $payment_method = $key_items->payment_method;
	                  $amount_paid = $key_items->amount_paid;
	                  
	                  $total_payments = $total_payments + $amount_paid;
	                }
	          endforeach;
	                    
	      }
	      else
	      {
	      	$total_payments = 0;
	      }
	      return $total_payments;
	}

	public function visit_payments($visit_id)
	{
		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id  AND payments.visit_id =". $visit_id;
		$items = "SUM(amount_paid) AS total_amount";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$amount_paid = $value->total_amount;
			}
		}
		return $amount_paid;
	}
	
	public function total($visit_id)
	{
	 	$total=""; 
	 	$temp="";
		
		//identify patient/visit type
		$visit_type_rs = $this->nurse_model->get_visit_type($visit_id);
		foreach ($visit_type_rs as $key):
			$visit_t = $key->visit_type;
		endforeach;
		//  get patient id 
		$patient_id = $this->nurse_model->get_patient_id($visit_id);
	
		//  get the visit type details
		$type_details_rs = $this->visit_type_details($visit_t);
		$num_type = count($type_details_rs);
		if($num_type > 0){
			foreach ($type_details_rs as $key_details):
				$visit_type_name = $key_details->visit_type_name;
			endforeach;
		}
		if ($visit_type_name=="Insurance")
		{
			//  get insuarance amounts 
			$insurance_rs = $this->get_service_charges_amounts($visit_id);
		    $num_rows = count($insurance_rs);
			foreach ($insurance_rs as $key_values):
				$service_id1  = $key_values->service_id;
				$visit_charge_amount  = $key_values->visit_charge_amount;
				$visit_charge_units  = $key_values->visit_charge_units;
				$discounted_value="";
				
				$dicount_rs = $this->get_dicountend_values($patient_id,$service_id1);
				foreach ($dicount_rs as $key_disounts):
					$percentage = $key_disounts->percentage;
					$amount = $key_disounts->amount;
				endforeach;
					$penn=((100-$percentage)/100);
					$discounted_value="";	
					if($percentage==0){
						$discounted_value=$amount;	
						$sum = $visit_charge_amount -$discounted_value;			
				
					}
					else if($amount==0){
						$discounted_value=$percentage;
						$sum = $visit_charge_amount *((100-$discounted_value)/100);
						$penn=((100-$discounted_value)/100);
					}
					else if(($amount==0)&&($percentage==0)){
						$sum=$visit_charge_amount;
					}
						
				$total=($sum*$visit_charge_units)+$temp;	$temp=$total;
						
			endforeach;
			return $total;
		}
		else
		{
			$amount_rs = $this->get_service_charges_amounts($visit_id);
		    $num_rows = count($amount_rs);
			foreach ($amount_rs as $key_values):
				$service_id1  = $key_values->service_id;
				$visit_charge_amount  = $key_values->visit_charge_amount;
				$visit_charge_units  = $key_values->visit_charge_units;
				$amount=$visit_charge_amount*$visit_charge_units;
				$total = $total + $amount;
						
			endforeach;
			return $total;
		}
	
	}
	function visit_type_details($visit_type_id){
		$table = "visit_type";
		$where = "visit_type.visit_type_id =". $visit_type_id;
		$items = "*";
		$order = "visit_type_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	
	function get_service_charges_amounts($visit_id)
	{
		$table = "visit_charge, service_charge";
		$where = "service_charge.service_charge_id = visit_charge.service_charge_id
		AND visit_charge.visit_id =". $visit_id;
		$items = "visit_charge.visit_charge_amount,visit_charge.visit_charge_units,visit_charge.service_charge_id,service_charge.service_id";
		$order = "visit_charge.service_charge_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
		
	}
	function get_dicountend_values($patient_id,$service_id)
	{
		$table = "insurance_discounts";
		$where = "insurance_id = (SELECT company_insurance_id FROM `patient_insurance` where patient_id = ". $patient_id .") and service_id = ". $service_id;
		$items = "*";
		$order = "insurance_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	function get_payment_methods()
	{
		$table = "payment_method";
		$where = "payment_method_id > 0";
		$items = "*";
		$order = "payment_method";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function balance($payments, $invoice_total)
	{
		
		$value = $payments - $invoice_total;
		if($value > 0){
			$value= '(-'.$value.')';
		}
		else{
			$value= -(1) * ($value);
		}
	
		return $value;
	}

	public function get_patient_visit_charge_items_tree_inpatient($visit_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_id =". $visit_id;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);
		$this->db->group_by('service.service_name');
		$this->db->order_by('visit_charge.date');
		$result = $this->db->get($table);
		
		return $result;
	}

	function get_visit_procedure_charges_as_services_inpatient($v_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = $v_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id ";
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "visit_charge.date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}
	public function get_patient_visit_charge_items_old($visit_id)
	{
		$table = "visit_charge,service_charge";
		$where = "visit_charge.visit_charge_units <> 0 AND visit_charge.visit_charge_delete = 0 AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_id =". $visit_id;
		$items = "service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id";
		$order = "service_charge.service_charge_name";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	public function get_patient_visit_charge_items($visit_id=NULL,$visit_invoice_id=NULL)
	{
		$add = '';
		if(!empty($visit_id))
		{
			$add .= ' AND  visit_charge.visit_id ='.$visit_id;
		}

		if(!empty($visit_invoice_id))
		{
			$add .= ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
		}


		$table = "visit_charge, service_charge, service,visit,visit_invoice";
		$where = "visit_charge.visit_charge_units <> 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_charge.service_charge_id = service_charge.service_charge_id".$add;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id";
		$order = "service.service_name";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}


	public function get_patient_visit_charge_items_receipt($visit_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0  AND visit_charge.service_charge_id = service_charge.service_charge_id  AND visit_charge.visit_id =". $visit_id;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id";
		$order = "service.service_name";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_patient_visit_charge($visit_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_id =". $visit_id;
		$items = "DISTINCT(service_charge.service_id) AS service_id, service.service_name,";
		$order = "service_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function total_debit_note_per_service($service_id,$visit_id){
		$table = "payments,payment_method";
		$where = "payment_method.payment_method_id = payments.payment_method_id AND payments.payment_type = 3  AND payments.visit_id =". $visit_id;
		$items = "SUM(amount_paid) AS total_debit";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$total_debit = 0;
		 if(count($result) > 0){
		 	foreach ($result as $key_items):
		 		$total_debit = $key_items->total_debit;
		    endforeach;
		 }
		 else
		 {
		 	$total_debit = 0;
		 }
		 return $total_debit;
	}
	public function total_credit_note_per_service($service_id,$visit_id){
		$table = "payments,payment_method";
		$where = "payment_method.payment_method_id = payments.payment_method_id AND payments.payment_type = 2  AND payments.visit_id =". $visit_id;
		$items = "SUM(amount_paid) AS total_credit";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$total_credit = 0;
		 if(count($result) > 0){
		 	foreach ($result as $key_items):
		 		$total_credit = $key_items->total_credit;
		    endforeach;
		 }
		 else
		 {
		 	$total_credit = 0;
		 }
		 return $total_credit;
	}

	public function payments($visit_id){

		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =". $visit_id;
		$items = "*";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	public function inpatient_payments($visit_id,$visit_invoice_id){

		$table = "payments,payment_item, payment_method";
		$where = "payments.cancel = 0 AND payment_item.payment_id = payments.payment_id AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =". $visit_id;
		$items = "*";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	public function paymentz($visit_id){

		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =". $visit_id;
		$items = "*";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}


	public function get_all_visit_transactions($visit_id){
		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =". $visit_id;
		$items = "*";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function get_sum_credit_notes($visit_id)
	{
		$table = "payments";
		$where = "payments.payment_type = 2 AND payments.cancel = 0 AND payments.visit_id =". $visit_id;
		$items = "SUM(payments.amount_paid) AS amount_paid";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			foreach ($result as $key):
				# code...
				$amount = $key->amount_paid;
				if(!is_numeric($amount))
				{
					return 0;
				}
				else
				{
					return $amount;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
	}
	public function get_sum_debit_notes($visit_id)
	{
		$table = "payments";
		$where = "payments.payment_type = 3 AND payments.cancel = 0 AND payments.visit_id =". $visit_id;
		$items = "SUM(payments.amount_paid) AS amount_paid";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			foreach ($result as $key):
				# code...
				$amount = $key->amount_paid;
				if(!is_numeric($amount))
				{
					return 0;
				}
				else
				{
					return $amount;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
	}
	public function  get_payment_peronnel($payment_id)
	{
		$table = "payments";
		$where = "payment_id =". $payment_id;
		$items = "payment_created_by";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			foreach ($result as $key):
				# code...
				$payment_created_by = $key->payment_created_by;
				if(!is_numeric($payment_created_by))
				{
					return 0;
				}
				else
				{
					return $payment_created_by;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
	}
	public function receipt_payment($visit_id,$visit_invoice_id){
		
		$payment_method=$this->input->post('payment_method');
		$type_payment=1;//$this->input->post('type_payment');
		$payment_service_id=$this->input->post('payment_service_id');
		
		if($type_payment == 1)
		{
			$payment_service_id=1;//$this->input->post('payment_service_id');
			$amount = $this->input->post('amount_paid');
		}
		
		else
		{
			$payment_service_id=$this->input->post('waiver_service_id');
			$amount = $this->input->post('waiver_amount');
		}
		
		if($payment_method == 1)
		{
			// check for cheque number if inserted
			
			$transaction_code = $this->input->post('cheque_number');
		}
		else if($payment_method == 6)
		{
			// check for insuarance number if inserted
			$transaction_code = $this->input->post('debit_card_detail');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else if($payment_method == 7)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('deposit_detail');
		}
		else if($payment_method == 8)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('debit_card_detail');
		}
		else
		{
			$transaction_code = '';
		}

		$amount = str_replace(',', '', $amount);
		$change = $this->input->post('change_payment');

		$reason = $this->input->post('reason');
		
		$patient_id = $this->input->post('patient_id');

		// $patient_id = 0;
		if($visit_id > 0)
		{


			$rs = $this->nurse_model->check_visit_type($visit_id);
			if(count($rs)>0){
			  foreach ($rs as $rs1) {
			    # code...
			      $patient_id = $rs1->patient_id;
			  }
			}
		}



		// $payments_value = $this->accounts_model->total_payments_inpatient($visit_id);

		// $invoice_total = $this->accounts_model->total_invoice_inpatient($visit_id);

		// $balance = $this->accounts_model->balance($payments_value,$invoice_total);
		// if($change > 0 AND $payment_method == 2 AND $balance > 0)
		// {
		// 	$amount = $amount - $change;
		// }
		// else
		// {
		// 	$change = 0;
		// 	$amount = $amount;
		// }
		$visit_invoice_id = $this->input->post('visit_invoice_id');
		$data = array(
			'visit_id' => $visit_id,
			'payment_method_id'=>$payment_method,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'payment_type'=>$type_payment,
			'transaction_code'=>$transaction_code,
			'reason'=>$reason,
			'payment_service_id'=>$payment_service_id,
			'change'=>$change,
			'payment_created'=>date("Y-m-d"),
			'payment_date'=>date("Y-m-d"),
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$this->session->userdata("personnel_id"),'date_approved'=>date('Y-m-d')
		);
		if($type_payment == 1)
		{
			$prefix= $suffix = $this->create_receipt_number();
			// var_dump($prefix);die();
			$visit_rs = $this->accounts_model->get_visit_details($visit_id);
			$branch_code = 'RS';
			if($visit_rs->num_rows() > 0)
			{
				foreach ($visit_rs->result() as $key => $value) {
					# code...
					$branch_code = $value->branch_code;

					// $visit_time_out = date('jS F Y',strtotime($visit_time_out));
				}
			}
			if($prefix < 10)
			{
				$number = '00000'.$prefix;
			}
			else if($prefix < 100 AND $prefix >= 10)
			{
				$number = '0000'.$prefix;
			}
			else if($prefix < 1000 AND $prefix >= 100)
			{
				$number = '000'.$prefix;
			}
			else if($prefix < 10000 AND $prefix >= 1000)
			{
				$number = '00'.$prefix;
			}
			else if($prefix < 100000 AND $prefix >= 10000)
			{
				$number = '0'.$prefix;
			}

			$invoice_number = $branch_code.$number;
			$data['confirm_number'] = $invoice_number;
			$data['suffix'] = $suffix;
			$data['prefix'] = $suffix;
		}
		// var_dump($data);die();
		if($this->db->insert('payments', $data))
		{

			$payment_id = $this->db->insert_id();

			 $service = array(
	              'visit_invoice_id'=>$visit_invoice_id,
	              'invoice_type'=>1,
	              'patient_id' => $patient_id,
	              'visit_id'=>$visit_id,
	              'created_by' => $this->session->userdata('personnel_id'),
	              'created' => date('Y-m-d'),
	              'payment_item_amount'=>$amount,
	              // 'payment_id'=>NULL
	            );
	            // var_dump($payment_id);die();

	    if($payment_id > 0)
	    {
	      $service['payment_id'] = $payment_id;
	    }
	    else
	    {
	    	$service['payment_id'] = 0;
	    }

	    $this->db->insert('payment_item',$service);

	    if(!empty($visit_invoice_id))
	    {
	    	$this->update_invoice_totals($visit_invoice_id);

	    }
			return $payment_id;
		}
		else{
			return FALSE;
		}
	}
	public function check_admin_person($username,$password)
	{
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
		
		if($authorize_invoice_changes != 1)
		{
			$password = md5($password);
			$table = "personnel,personnel_department";
			$where = "personnel.personnel_username = '$username' AND personnel.personnel_password = '$password'  AND personnel.personnel_id = personnel_department.personnel_id AND personnel_department.department_id = 3";
			$items = "personnel.personnel_id";
			$order = "personnel.personnel_id";
			
			$result = $this->database->select_entries_where($table, $where, $items, $order);
			
			if(count($result) > 0)
			{
				foreach ($result as $row2):
					$personnel_id = $row2->personnel_id;
				endforeach;
				return $personnel_id;	
			}
			else{
				return FALSE;
			}
		}
		
		else
		{
			$personnel_id = $this->session->userdata('personnel_id');
			
			return $personnel_id;
		}
	}
	public function add_billing($visit_id)
	{
		$billing_method_id = $this->input->post('billing_method_id');
		$data = array('bill_to_id' => $billing_method_id);
		
		$this->db->where('visit_id', $visit_id);
		if($this->db->update('visit', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_service_item()
	{
		$parent_service_id = $this->input->post('parent_service_id');
		$service_charge_item = $this->input->post('service_charge_item');
		$service_amount = $this->input->post('service_amount');
		

		$this->db->where('visit_type_status', 1);
		$visit_type_query = $this->db->get('visit_type');

		if($visit_type_query->num_rows() > 0)
		{
			// foreach ($visit_type_query->result() as $key) {
			
				// $visit_type_id = $key->visit_type_id;
				// service charge entry
				$service_charge_insert = array(
								"service_charge_name" => $service_charge_item,
								"service_id" => $parent_service_id,
								"visit_type_id" => 1,
								"service_charge_amount" => $service_amount,
								'service_charge_status' => 1,
							);
				
				if($this->service_charge_exists($service_charge_item, 1))
				{
					$this->db->where(array('service_charge_name' => $service_charge_item, 'visit_type_id' => $visit_type_id));
					if($this->db->update('service_charge', $service_charge_insert))
					{
						
					}
					
					else
					{
					}
				}
				
				else
				{
					$service_charge_insert['created'] = date('Y-m-d H:i:s');
					$service_charge_insert['created_by'] = $this->session->userdata('personnel_id');
					$service_charge_insert['modified_by'] = $this->session->userdata('personnel_id');
					// var_dump($service_charge_insert); die();
					if($this->db->insert('service_charge', $service_charge_insert))
					{
						
					}
					
					else
					{
					}
				}
			// }
			return TRUE;
		}
		else
		{
			return FALSE;
		}

		
	}

	public function service_charge_exists($service_charge_name, $visit_type_id)
	{
		$this->db->where(array('service_charge_name' => $service_charge_name, 'visit_type_id' => $visit_type_id, 'service_charge_delete' => 0));
		$query = $this->db->get('service_charge');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	
	public function get_att_doctor($visit_id)
	{
		$this->db->select('personnel.personnel_fname, personnel.personnel_onames');
		$this->db->from('personnel, visit');
		$this->db->where('personnel.personnel_id = visit.personnel_id AND visit.visit_id = '.$visit_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$doctor = $row->personnel_fname;
		}
		
		else
		{
			$doctor = '-';
		}
		
		return $doctor;
	}
	
	public function get_personnel($personnel_id)
	{
		if(empty($personnel_id))
		{
			//redirect('login');
			$personnel = '-';
		}
		
		else
		{
			$this->db->select('personnel.personnel_fname, personnel.personnel_onames');
			$this->db->from('personnel');
			$this->db->where('personnel.personnel_id = '.$personnel_id);
			
			$query = $this->db->get();
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$personnel = $row->personnel_onames.' '.$row->personnel_fname;
			}
			
			else
			{
				$personnel = '-';
			}
			
			return $personnel;
		}
	}
	
	public function get_visit_date($visit_id)
	{
		$this->db->select('visit_date');
		$this->db->from('visit');
		$this->db->where('visit_id = '.$visit_id);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$visit_date = $row->visit_date;
		}
		
		else
		{
			$visit_date = '-';
		}
		
		return $visit_date;
	}
	public function get_visit_details($visit_id)
	{
		$this->db->select('*');
		$this->db->from('visit,visit_type,patients');
		$this->db->where('visit.visit_type = visit_type.visit_type_id AND visit.patient_id = patients.patient_id AND visit_id = '.$visit_id);
		
		$query = $this->db->get();
		
		
		return $query;
	}
	
	public function end_visit($visit_id)
	{
		$data = array(
        	"close_card" => 1,
        	"visit_time_out" => date('Y-m-d H:i:s')
    	);
		
		$this->db->where('visit_id', $visit_id);
		
		if($this->db->update('visit', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}


	public function close_visit($visit_id)
	{
		// $data = array(
  //       	"close_card" => 0,
  //       	"visit_time_out" => date('Y-m-d H:i:s')
  //   	);
		
		$this->db->where('visit_id', $visit_id);
		
		if($this->db->delete('visit_bill', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

	public function end_visit_with_status($visit_id,$status)
	{
		$data = array(
        	"close_card" => $status
    	);
		
		$this->db->where('visit_id', $visit_id);
		
		if($this->db->update('visit', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

	public function discharge_visit_with_status($visit_id,$status,$visit_date,$visit_invoice_id)
	{
		$data = array(
        	"close_card" => $status,
        	"branch_id"=>3,
        	"visit_time_out" => $visit_date.' '.date('H:i:s')
    	);
		
		$this->db->where('visit_id', $visit_id);
		
		if($this->db->update('visit', $data))
		{
			
				$visit_update['open_status'] = 1;
				$this->db->where('visit_invoice_id',$visit_invoice_id);
				$this->db->update('visit_invoice',$visit_update);

			
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function get_billing_methods()
	{
		$this->db->order_by('bill_to_name');
		$query = $this->db->get('bill_to');
		
		return $query;
	}
	
	public function get_bill_to($visit_id)
	{
		$this->db->where('visit_id', $visit_id);
		$query = $this->db->get('visit');
		$row = $query->row();
		return $row->bill_to_id;
	}

	public function get_billing_info($visit_id)
	{
		$this->db->where('visit_id', $visit_id);
		$query = $this->db->get('visit');
		$row = $query->row();
		return $row->payment_info;
	}
	public function get_all_service($patient_id=null)
	{


		$table = "service";
		$where = "service_delete = 0";
		$items = "*";
		$order = "service_id";
		
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function get_service_detail($service_id)
	{
		$table = "service";
		$where = "service_id = ".$service_id;
		$items = "*";
		$order = "service_id";
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			foreach ($result as $key):
				# code...
				$service_name = $key->service_name;
			endforeach;
		}
		else
		{
			$service_name = "";
		}
		return  $service_name;
	}
	public function get_all_notes($visit_id)
	{
		$table = "payments, service";
		$where = "payments.payment_service_id = service.service_id AND (payments.payment_type = 2 OR payments.payment_type = 3) AND payments.visit_id = ". $visit_id;
		
		$this->db->select('service.service_name, payments.payment_service_id, payments.amount_paid, payments.payment_type');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function in_pres($service_charge_id, $visit_id)
	{
		$table = "pres, visit_charge";
		//$where = "pres.service_charge_id = visit_charge.service_charge_id AND pres.service_charge_id = ". $service_charge_id." AND pres.visit_id = ". $visit_id." AND visit_charge.visit_id = ". $visit_id;
		$where = "pres.service_charge_id = visit_charge.service_charge_id AND pres.visit_id = visit_charge.visit_id AND pres.service_charge_id = ". $service_charge_id." AND pres.visit_id = ". $visit_id." AND visit_charge.visit_id = ". $visit_id;
		
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function get_going_to($visit_id)
	{
		$this->db->select('departments.department_name, visit_department.accounts, departments.department_id');
		$this->db->where('visit_department.department_id = departments.department_id AND visit_department.visit_department_status = 1 AND visit_department.visit_id = '.$visit_id);
		$query = $this->db->get('visit_department, departments');
		
		return $query;
	}
	
	public function get_last_department($visit_id)
	{
		$this->db->select('departments.department_name, a.accounts, departments.department_id');
		$this->db->where('a.created = (
						SELECT MAX(created)
						FROM visit_department AS b
						WHERE b.visit_department_status = 0 AND b.visit_id = '.$visit_id.')
						AND a.department_id = departments.department_id AND a.visit_id = '.$visit_id);
		$query = $this->db->get('visit_department AS a, departments');
		
		return $query;
	}
	
	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');
		
		return $this->db->get('cancel_action');
	}

	public function cancel_payment($payment_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->input->post('cancel_action_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1
		);
		
		$this->db->where('payment_id', $payment_id);
		if($this->db->update('payments', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}


	public function cancel_payment_olding($payment_id,$visit_id=0)
	{


		$visit_invoice_id = $this->input->post('visit_invoice_id');
		$payment_id = $this->input->post('payment_id');
		$patient_id = $this->input->post('patient_id');
		$visit_id = $this->input->post('visit_id');
		$parent_payment_id = $this->input->post('payment_id');
		$amount_refunded = $this->input->post('amount_refunded');
		$reason = $this->input->post('cancel_description');
		$refund_date = $this->input->post('refund_date');
		$account_from_id = $this->input->post('account_from_id');

		$data = array(
						'patient_id' => $patient_id,
						'payment_method_id'=>$account_from_id,
						'amount_paid'=>-$amount_refunded,
						'personnel_id'=>$this->session->userdata("personnel_id"),
						'payment_type'=>$parent_payment_id,
						'transaction_code'=>'',
						'reason'=>$reason,
						'payment_service_id'=>1,
						'change'=>0,
						'payment_date'=>$this->input->post('refund_date'),
						'payment_created_by'=>$this->session->userdata("personnel_id"),
						'approved_by'=>$this->session->userdata("personnel_id"),'date_approved'=>date('Y-m-d')
					);
		$data['payment_created'] = date("Y-m-d");
		$prefix = $suffix = $this->create_receipt_number();
	
		$branch_code = $this->session->userdata('branch_code');
		$branch_id = $this->session->userdata('branch_id');

		// $branch_code = str_replace("RS", "CT", $branch_code);

		// if($branch_code == "CT")
		// {
			$branch_code = "RF";
		// }
	
	// var_dump($data);die();

		
		if($prefix < 10)
		{
			$number = '00000'.$prefix;
		}
		else if($prefix < 100 AND $prefix >= 10)
		{
			$number = '0000'.$prefix;
		}
		else if($prefix < 1000 AND $prefix >= 100)
		{
			$number = '000'.$prefix;
		}
		else if($prefix < 10000 AND $prefix >= 1000)
		{
			$number = '00'.$prefix;
		}
		else if($prefix < 100000 AND $prefix >= 10000)
		{
			$number = '0'.$prefix;
		}

		$invoice_number = $branch_code.$number;
		$data['confirm_number'] = $invoice_number;
		$data['suffix'] = $suffix;
		$data['branch_id'] = $branch_id;


		if($this->db->insert('payments', $data))
		{
			$payment_id =  $this->db->insert_id();


				$service = array(
			              'visit_invoice_id'=>$visit_invoice_id,
			              'invoice_type'=>1,
			              'patient_id' => $patient_id,
			              'created_by' => $this->session->userdata('personnel_id'),
			              'created' => date('Y-m-d'),
			              'payment_item_amount'=>-$amount_refunded
			            );
			   
			  $service['payment_id'] = $payment_id;
			  

			 	$this->db->insert('payment_item',$service);


		    return TRUE;
		}
		else
		{
			return FALSE;
		}



	}
	public function cancel_payment_old($payment_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->input->post('cancel_action_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1
		);
		
		$this->db->where('payment_id', $payment_id);
		if($this->db->update('payments', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	function get_visit_procedure_charges($v_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = $v_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id ";
		$items = "*";
		$order = "visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	public function get_patient_visit_charge_items_tree_old($visit_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND service.service_name <> 'Others' AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_id =". $visit_id;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);
		$this->db->group_by('service.service_name');
		$this->db->order_by('visit_charge.date');
		$result = $this->db->get($table);
		
		return $result;
	}



	function get_visit_procedure_charges_as_services($v_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = $v_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id ";
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "visit_charge.visit_charge_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}
	function get_visit_procedure_charges_per_service_old($v_id,$service_id,$visit_type_id = null)
	{
		$adding = '';
		if(!empty($visit_type_id))
		{
			$adding = ' AND visit_charge.charge_to = '.$visit_type_id;
		}
		// var_dump($visit_type_id)
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = $v_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_id = $service_id  ".$adding;
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "visit_charge.visit_charge_id,service.service_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	function get_visit_procedure_charges_per_service($v_id,$service_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = $v_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_id = $service_id ";
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "visit_charge.date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	function get_visit_procedure_charges_per_service_checked($v_id,$service_id,$visit_invoice_id = NULL)
	{
		if(!empty($visit_invoice_id))
		{
			$add = ' AND visit_charge.visit_invoice_id = '.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_id = '.$v_id;
		}
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0  AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND visit_charge.plan_status <= 2  ".$add;
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "service.service_id,visit_charge.visit_charge_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	function get_visit_procedure_charges_per_date_inpatient($v_id,$billing_date,$visit_invoice_id = NULL)
	{
		if(!empty($visit_invoice_id))
		{
			$add = ' AND visit_charge.visit_invoice_id = '.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_id = '.$v_id;
		}
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1  AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND DATE(visit_charge.date) = '".$billing_date."' ".$add;
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "service.view_order";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	function get_visit_procedure_charges_per_service_inpatient($v_id,$service_id,$visit_invoice_id = NULL)
	{
		if(!empty($visit_invoice_id))
		{
			$add = ' AND visit_charge.visit_invoice_id = '.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_id = '.$v_id;
		}
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_id = $v_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_id = $service_id ";
		$items = "visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*";
		$order = "visit_charge.date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	public function get_service_charge_detail($service_charge_id)
	{
		$table = "service_charge";
		$where = "service_charge_id = ". $service_charge_id;
		$items = "*";
		$order = "service_charge_name";
		$this->db->where($where);
		$this->db->select($items);
		$result = $this->db->get($table);
		$service_charge_amount = 0;
		if($result->num_rows() > 0)
		{
			foreach ($result->result() as $value) {
				# code...
				$service_charge_amount = $value->service_charge_amount;
			}
		}
			
		return $service_charge_amount;
	}

	public function add_personnel()
	{
		$data = array(
			'personnel_onames'=>ucwords(strtolower($this->input->post('personnel_onames'))),
			'personnel_fname'=>ucwords(strtolower($this->input->post('personnel_fname'))),
			'branch_id'=>2,
			'personnel_phone'=>$this->input->post('personnel_phone'),
			'title_id'=>4,
			'personnel_type_id'=>6,
		);
		
		if($this->db->insert('personnel', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_all_visits_parent($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		// $this->db->group_by('visit.patient_id','');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_visits_invoice_items($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*');
		$this->db->where($where);
		// $this->db->order_by('service.service_name,visit_charge.date','DESC');
		$this->db->order_by('visit_charge.visit_charge_id','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_visits_invoice_items_walkin($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit_charge.created_by AS charge_creator, visit_charge.*,service_charge.*,service.*,pres.*');
		$this->db->where($where);
		$this->db->order_by('service.service_name,visit_charge.date','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_visits_payments_items($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users

		
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('payments.payment_id','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	/*
	*	Retrieve all patients
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_patients_accounts($table, $where, $per_page, $page, $items = '*')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select($items);
		$this->db->where($where);
		$this->db->order_by('last_visit','desc');
		// $this->db->group('last_visit','desc');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_personnel()
	{
		$this->db->select('*');
		$query = $this->db->get('personnel');
		
		return $query;
	}

	public function check_if_visit_active($visit_id)
	{
		$this->db->where('close_card = 0 OR close_card = 2 AND visit_id ='.$visit_id);
		$query = $this->db->get('visit');

		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}


	public function receipt_invoice_payment($visit_id,$personnel_id = NULL)
	{
		$amount = $this->input->post('amount_paid');
		
		$change = $this->input->post('change_payment');
		$visit_invoice_id = $this->input->post('visit_invoice_id');

		// $payments_value = $this->accounts_model->total_payments($visit_id);

		// $invoice_total = $this->accounts_model->total_invoice($visit_id);

		// $balance = $this->accounts_model->balance($payments_value,$invoice_total);
		// if($change > 0 AND $payment_method == 2 AND $balance > 0)
		// {
		// 	$amount = $amount - $change;
		// }
		// else
		// {
		// 	$change = 0;
		// 	$amount = $amount;
		// }





		$data = array(
			'visit_id' => $visit_id,
			'payment_method_id'=>$payment_method,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'payment_type'=>$type_payment,
			'transaction_code'=>$transaction_code,
			'payment_service_id'=>$payment_service_id,
			'change'=>$change,
			'payment_created'=>date("Y-m-d"),
			'payment_date'=>date("Y-m-d"),
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		var_dump($data);die();
		if($this->db->insert('payments', $data))
		{
			$payment_id = $this->db->insert_id();

			if($visit_invoice_id > 0)
			{


				$service = array(
		              'visit_invoice_id'=>$visit_invoice_id,
		              'invoice_type'=>1,
		              'patient_id' => $patient_id,
		              'visit_id' => $visit_id,
		              'created_by' => $this->session->userdata('personnel_id'),
		              'created' => date('Y-m-d'),
		              'payment_item_amount'=>$amount,
		              'payment_id'=>$payment_id
		            );
		            

		    $this->db->insert('payment_item',$service);
		  }

	    return $payment_id;
		}
		else{
			return FALSE;
		}
	}

	public function previous_payment($visit_id,$date_today){
		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND payments.visit_id =".$visit_id." AND payment_created <  '".$date_today."' ";
		$items = "SUM(amount_paid) AS total_amount";
		$order = "payments.payment_id";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$total_amount = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$total_amount = $value->total_amount;
		}
		
		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}


	public function get_cash_payments($visit_id){
		$table = "payments";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method_id < 9 AND payments.visit_id =".$visit_id."";
		$items = "SUM(amount_paid) AS total_amount";
		$order = "payments.payment_id";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$total_amount = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$total_amount = $value->total_amount;
		}
		
		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}

	public function get_insurance_payments($visit_id){
		$table = "payments";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method_id = 9 AND payments.visit_id =".$visit_id."";
		$items = "SUM(amount_paid) AS total_amount";
		$order = "payments.payment_id";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$total_amount = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$total_amount = $value->total_amount;
		}
		
		if(empty($total_amount))
		{
			$total_amount = 0;
		}
		return $total_amount;
	}
	public function payment_detail($payment_id){
		$table = "payments, payment_method";
		$where = "payments.cancel = 0 AND payment_method.payment_method_id = payments.payment_method_id AND payments.payment_id =". $payment_id;
		$items = "*";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function update_rejected_reasons($visit_id)
    {
    	// recreate new visit 
		$rs_rejection = $this->dental_model->get_rejection_info($visit_id);
		$rejected_amount = '';
		$rejected_reason ='';
		$close_card = 0;
		if(count($rs_rejection) >0){
			foreach ($rs_rejection as $r2):
			    # code...
			    $rejected_amount = $r2->rejected_amount;
			    $rejected_date = $r2->rejected_date;
			    $rejected_reason = $r2->rejected_reason;
			    $visit_type = $r2->visit_type;
			endforeach;


		    $data = array(
					            "visit_bill_amount" => $this->input->post('rejected_amount'),
					            "visit_type_id" => $this->input->post('visit_type_id'),
					            "visit_id" => $visit_id,
					            "visit_parent" => $visit_id,
					            'visit_bill_reason'=> $this->input->post('rejected_reason'),
					            "visit_parent_visit_type_id" => $visit_type,
					        );
		       
		     $this->db->insert('visit_bill', $data);
		     return TRUE;

	

		}    	


       
    }

    public function get_visit_charges($visit_id)
	{
		$this->db->where('visit_id', $visit_id);
		return $this->db->get('visit_charge');

	}
	


    public function total_payments_today($todays_date,$visit_type_id)
	{
	    $table = "payments, payment_method,visit";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND visit.visit_date = '".$todays_date."' AND visit.visit_type =".$visit_type_id;
		$items = "SUM(amount_paid) AS total_payments";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);

		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		
		return $total_payments;
	}


    public function total_invoice_today($todays_date,$visit_type_id)
	{
	    $table = "visit_charge,visit";
		$where = "visit_charge.visit_charge_delete = 0 AND visit.visit_id = visit_charge.visit_id AND visit.visit_delete = 0 AND visit.visit_date = '".$todays_date."'  AND visit.visit_type =".$visit_type_id;
		$items = "SUM(visit_charge_amount*visit_charge_units) AS total_invoice";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);

		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}


		// $table = "visit";
		// $where = "visit.visit_delete = 0 AND visit.visit_date = '".$todays_date."'";
		// $items = "SUM(rejected_amount) AS total_rejected_amount";
		// $order = "payments.payment_id";
		// $this->db->where($where);
		// $query_rejected = $this->db->get($table);

		// $total_rejected_amount = 0;
		// if($query_rejected->num_rows() > 0)
		// {
		// 	foreach ($query_rejected->result() as $key => $value2) {
		// 		# code...
		// 		$total_rejected_amount = $value2->total_rejected_amount;
		// 	}
		// }


		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 2 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND visit.visit_date = '".$todays_date."' AND visit.visit_type =".$visit_type_id;
		$items = "SUM(amount_paid) AS total_waivers";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_waivers = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_waivers = $wiver_value->total_waivers;
			}
		}
		
		$invoice_total = $total_invoice - $total_waivers;
		return $invoice_total;
	}

	public function get_visit_total_invoice($visit_id)
	{
	    $table = "visit_charge,visit";
		$where = "visit_charge.visit_charge_delete = 0 AND (visit.parent_visit IS NULL OR visit.parent_visit = 0)AND visit.visit_id = visit_charge.visit_id AND visit.visit_delete = 0 AND visit_charge.charged = 1 AND visit.visit_id = '".$visit_id."'";
		$items = "SUM(visit_charge_amount*visit_charge_units) AS total_invoice";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);

		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}

		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 2 AND (visit.parent_visit IS NULL OR visit.parent_visit = 0) AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND visit.visit_id = '".$visit_id."' ";
		$items = "SUM(amount_paid) AS total_waivers";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_waivers = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_waivers = $wiver_value->total_waivers;
			}
		}
		
		$invoice_total = $total_invoice - $total_waivers;
		return $invoice_total;
	}
	public function get_child_amount_payable($visit_id)
	{
		# code...

		$table = "visit,visit_bill,visit_type";
		$where = "visit_bill.visit_id = '$visit_id' AND visit.visit_delete = 0 AND visit.visit_id = visit_bill.visit_id AND visit_type.visit_type_id = visit.visit_type";
		$items = "SUM(visit_bill_amount) AS total_bill";
		$order = "visit.visit_id";

		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);
		$total_bill = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_bill = $wiver_value->total_bill;
			}
		}

		return $total_bill;

	}
	public function get_visit_waiver($visit_id)
	{
		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 2 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND visit.visit_id = '".$visit_id."' ";
		$items = "SUM(amount_paid) AS total_waivers";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_waivers = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_waivers = $wiver_value->total_waivers;
			}
		}
		return $total_waivers;
	}

	public function get_cash_balance($patient_id)
	{
		$this->db->where('visit.patient_id = '.$patient_id.' AND visit.visit_type = 1 AND visit.visit_delete = 0  AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_id = visit_charge.visit_id AND visit_charge.visit_charge_delete = 0');
		$this->db->select('sum(visit_charge.visit_charge_amount * visit_charge.visit_charge_units) AS total_amount');

		$query_invoice = $this->db->get('visit,visit_charge');

		$total_amount = 0;
		if($query_invoice->num_rows() > 0)
		{
			foreach ($query_invoice->result() as $key => $wiver_value) {
				# code...
				$total_amount = $wiver_value->total_amount;
			}
		}


		// insurance rejections

		$this->db->where('visit.patient_id = '.$patient_id.' AND visit.visit_type <> 1 AND visit.visit_delete = 0  AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_id = visit_charge.visit_id AND visit_charge.visit_charge_delete = 0');
		$this->db->select('sum(rejected_amount) AS total_amount');

		$query_rejection = $this->db->get('visit,visit_charge');

		$total_rejection = 0;
		if($query_rejection->num_rows() > 0)
		{
			foreach ($query_rejection->result() as $key => $wiver_value) {
				# code...
				$total_rejection = $wiver_value->total_amount;
			}
		}


		$table = "visit_bill,visit";
		$where = "visit_parent = visit.parent_visit  AND visit.visit_delete = 0 AND visit.patient_id =".$patient_id;
		$items = "SUM(visit_bill_amount) AS total_rejected";
		$order = "visit.visit_id";


		$this->db->where($where);
		$this->db->select($items);

		$query_rejection = $this->db->get($table);
		$total_rejected = 0;
		if($query_rejection->num_rows() > 0)
		{
			foreach ($query_rejection->result() as $key => $wiver_value) {
				# code...
				$total_rejected = $wiver_value->total_rejected;
			}
		}
		$total_rejection += $total_rejected;
		// var_dump($total_rejection); die();


		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method_id < 9 AND visit.visit_type = 1 AND payments.visit_id =visit.visit_id AND visit.patient_id =".$patient_id;
		$items = "SUM(amount_paid) AS cash_payments";
		$order = "payments.payment_id";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$cash_payments = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$cash_payments = $value->cash_payments;
		}
		
		if(empty($cash_payments))
		{
			$cash_payments = 0;
		}



		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 2 AND visit.visit_id = payments.visit_id AND visit.visit_type = 1 AND visit.visit_delete = 0 AND visit.patient_id = '".$patient_id."' ";
		$items = "SUM(amount_paid) AS total_waivers";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_waivers = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_waivers = $wiver_value->total_waivers;
			}
		}




		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 3 AND visit.visit_id = payments.visit_id AND visit.visit_type = 1 AND visit.visit_delete = 0 AND visit.patient_id = '".$patient_id."' ";
		$items = "SUM(amount_paid) AS total_debits";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_debits = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_debits = $wiver_value->total_debits;
			}
		}
		// var_dump($total_rejection); die();
		return ($total_rejection + $total_amount + $total_debits) - ($cash_payments + $total_waivers);
	}



	public function get_insurance_balance($patient_id)
	{
		$this->db->where('visit.patient_id = '.$patient_id.' AND visit.visit_type <> 1 AND visit.visit_delete = 0  AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_id = visit_charge.visit_id AND visit_charge.visit_charge_delete = 0 ');
		$this->db->select('sum(visit_charge.visit_charge_amount * visit_charge.visit_charge_units) AS total_amount');

		$query_invoice = $this->db->get('visit,visit_charge');

		$total_amount = 0;
		if($query_invoice->num_rows() > 0)
		{
			foreach ($query_invoice->result() as $key => $wiver_value) {
				# code...
				$total_amount = $wiver_value->total_amount;
			}
		}


		$this->db->where('visit.patient_id = '.$patient_id.' AND visit.visit_type <> 1 AND visit.visit_delete = 0  AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_id = visit_charge.visit_id AND visit_charge.visit_charge_delete = 0');
		$this->db->select('sum(rejected_amount) AS total_amount');

		$query_rejection = $this->db->get('visit,visit_charge');

		$total_rejection = 0;
		if($query_rejection->num_rows() > 0)
		{
			foreach ($query_rejection->result() as $key => $wiver_value) {
				# code...
				$total_rejection = $wiver_value->total_amount;
			}
		}

		$table = "visit_bill,visit";
		$where = "visit_parent = visit.parent_visit  AND visit.visit_delete = 0 AND visit.patient_id =".$patient_id;
		$items = "SUM(visit_bill_amount) AS total_rejected";
		$order = "visit.visit_id";


		$this->db->where($where);
		$this->db->select($items);

		$query_rejection = $this->db->get($table);
		$total_rejected = 0;
		if($query_rejection->num_rows() > 0)
		{
			foreach ($query_rejection->result() as $key => $wiver_value) {
				# code...
				$total_rejected = $wiver_value->total_rejected;
			}
		}
		$total_rejection += $total_rejected;
		
		// var_dump($total_rejection); die();

		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method_id = 9 AND payments.visit_id =visit.visit_id AND visit.patient_id =".$patient_id;
		$items = "SUM(amount_paid) AS cash_payments";
		$order = "payments.payment_id";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$cash_payments = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$cash_payments = $value->cash_payments;
		}
		
		


		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 2 AND visit.visit_id = payments.visit_id AND visit.visit_type <> 1  AND visit.visit_delete = 0 AND visit.patient_id = '".$patient_id."' ";
		$items = "SUM(amount_paid) AS total_waivers";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_waivers = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_waivers = $wiver_value->total_waivers;
			}
		}


		$table = "payments,visit";
		$where = "payments.cancel = 0 AND payment_type = 3 AND visit.visit_id = payments.visit_id AND visit.visit_type <> 1 AND visit.visit_delete = 0 AND visit.patient_id = '".$patient_id."' ";
		$items = "SUM(amount_paid) AS total_debits";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_debits = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_debits = $wiver_value->total_debits;
			}
		}
		// var_dump($total_rejection); die();
		
		return ($total_amount + $total_debits) - ($cash_payments + $total_waivers + $total_rejection);
	}


	public function approve_invoice($visit_id,$visit_invoice_id)
	{
		$approved_amount = $this->input->post('approved_amount');
		$data = array(
        	"preauth_status" => 2,
        	"approved_amount"=>$approved_amount
    	);	

    	// var_dump($data);die();
		$this->db->where('visit_invoice_id', $visit_invoice_id);
		if($this->db->update('visit_invoice', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	function get_service_charge($procedure_id){
		$table = "service_charge";
		$where = "service_charge_id = '$procedure_id'";
		$items = "*";
		$order = "service_charge_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}




	public function get_patient_visit_quote_items_tree($visit_id)
	{
		$table = "visit_quotation, service_charge, service";
		$where = "visit_quotation.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_quotation.visit_charge_delete = 0 AND visit_quotation.charged = 1 AND service.service_name <> 'Others' AND visit_quotation.service_charge_id = service_charge.service_charge_id AND visit_quotation.visit_id =". $visit_id;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_quotation.service_charge_id,visit_quotation.visit_charge_units, visit_quotation.visit_charge_amount, visit_quotation.visit_charge_timestamp,visit_quotation.visit_charge_id,visit_quotation.created_by, visit_quotation.personnel_id";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);
		$this->db->group_by('service.service_name');
		$this->db->order_by('visit_quotation.date');
		$result = $this->db->get($table);
		
		return $result;
	}

	function get_visit_quote_charges_per_service($v_id,$service_id,$visit_type_id = null)
	{
		$adding = '';
		if(!empty($visit_type_id))
		{
			$adding = ' AND visit_quotation.charge_to = '.$visit_type_id;
		}
		// var_dump($visit_type_id)
		$table = "visit_quotation, service_charge, service";
		$where = "visit_quotation.visit_charge_delete = 0 AND visit_quotation.visit_id = $v_id AND visit_quotation.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_id = $service_id AND visit_quotation.visit_charge_amount > 0 ".$adding;
		$items = "visit_quotation.created_by AS charge_creator, visit_quotation.*,service_charge.*,service.*";
		$order = "visit_quotation.date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;
	}

	public function get_visit_quote_amount($visit_id)
	{

		$this->db->where('visit.visit_id = visit_quotation.visit_id AND visit_quotation.visit_charge_delete = 0 and charged = 1 AND visit_quotation.visit_id ='.$visit_id);
		$this->db->select('sum(visit_quotation.visit_charge_amount * visit_quotation.visit_charge_units) AS total_amount');

		$query_invoice = $this->db->get('visit,visit_quotation');

		$total_amount = 0;
		if($query_invoice->num_rows() > 0)
		{
			foreach ($query_invoice->result() as $key => $wiver_value) {
				# code...
				$total_amount = $wiver_value->total_amount;
			}
		}

		return $total_amount;
	}

	public function create_receipt_number()
	{
		//select product code
		$this->db->where('payment_type = 1 ');
		$this->db->from('payments');
		$this->db->select('MAX(prefix) AS number');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number
			// if($number == 1){
			// 	$number = 390;
			// }
			// var_dump($number);die();
			
			if($number == 1)
			{
				$number = 1;
			}
			
		}
		else{//start generating receipt numbers
			$number = 1;
		}


		return $number;
	}



	// get new cash balance

	public function get_patient_cash_balance($patient_id)
	{
		$this->db->where('visit.patient_id = '.$patient_id.' AND visit.visit_type = 1 AND visit.visit_delete = 0  AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_id = visit_charge.visit_id AND visit_charge.visit_charge_delete = 0');
		$this->db->select('sum(visit_charge.visit_charge_amount * visit_charge.visit_charge_units) AS total_amount');

		$query_invoice = $this->db->get('visit,visit_charge');

		$total_amount = 0;
		if($query_invoice->num_rows() > 0)
		{
			foreach ($query_invoice->result() as $key => $wiver_value) {
				# code...
				$total_amount = $wiver_value->total_amount;
			}
		}


		// insurance rejections

		$this->db->where('visit.patient_id = '.$patient_id.' AND visit.visit_type <> 1 AND visit.visit_delete = 0  AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL) AND visit.visit_id = visit_charge.visit_id AND visit_charge.visit_charge_delete = 0');
		$this->db->select('sum(rejected_amount) AS total_amount');

		$query_rejection = $this->db->get('visit,visit_charge');

		$total_rejection = 0;
		if($query_rejection->num_rows() > 0)
		{
			foreach ($query_rejection->result() as $key => $wiver_value) {
				# code...
				$total_rejection = $wiver_value->total_amount;
			}
		}


		$table = "visit_bill,visit";
		$where = "visit_parent = visit.parent_visit  AND visit.visit_delete = 0 AND visit.patient_id =".$patient_id;
		$items = "SUM(visit_bill_amount) AS total_rejected";
		$order = "visit.visit_id";


		$this->db->where($where);
		$this->db->select($items);

		$query_rejection = $this->db->get($table);
		$total_rejected = 0;
		if($query_rejection->num_rows() > 0)
		{
			foreach ($query_rejection->result() as $key => $wiver_value) {
				# code...
				$total_rejected = $wiver_value->total_rejected;
			}
		}
		$total_rejection += $total_rejected;
		// var_dump($total_rejection); die();


		$table = "payments";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method_id < 9  AND payments.patient_id =".$patient_id;
		$items = "SUM(amount_paid) AS cash_payments";
		$order = "payments.payment_id";
		
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		$cash_payments = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$cash_payments = $value->cash_payments;
		}
		
		if(empty($cash_payments))
		{
			$cash_payments = 0;
		}



		$table = "payments";
		$where = "payments.cancel = 0 AND payment_type = 2  AND payments.patient_id = '".$patient_id."' ";
		$items = "SUM(amount_paid) AS total_waivers";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_waivers = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_waivers = $wiver_value->total_waivers;
			}
		}




		$table = "payments";
		$where = "payments.cancel = 0 AND payment_type = 3 AND payments.patient_id = '".$patient_id."' ";
		$items = "SUM(amount_paid) AS total_debits";
		$order = "payments.payment_id";
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		$total_debits = 0;
		if($query_waiver->num_rows() > 0)
		{
			foreach ($query_waiver->result() as $key => $wiver_value) {
				# code...
				$total_debits = $wiver_value->total_debits;
			}
		}
		// var_dump($total_rejection); die();
		return ($total_rejection + $total_amount + $total_debits) - ($cash_payments + $total_waivers);
	}

	public function get_patient_all_invoices_statement($patient_id,$type=NULL,$preauth_status=0)
	{
		if($type ==1)
		{
			$add = ' visit_invoice.patient_id = patients.patient_id ';
			// $table_add = ',visit_invoice,visit_type';
		}
		else
		{
			$table_add='';
			$add = '';
		}


		// if($preauth_status == 1 )
		// {
		// 	$add .= ' AND (v_transactions_by_date.preauth_status = 1 OR v_transactions_by_date.preauth_status = 2)';
		// }
		// else
		// {
		// 	$add .= ' AND (v_transactions_by_date.preauth_status = 0 OR v_transactions_by_date.preauth_status = 3)';
		// }
		$table = "visit_invoice,patients";
		$where = "visit_invoice.patient_id = '".$patient_id."' ".$add;
		$items = "*";
	
		$this->db->where($where);
		$this->db->select($items);
		$query_waiver = $this->db->get($table);

		
		return $query_waiver;
	}

	public function get_patient_statement($patient_id,$type=NULL,$preauth_status=0)
	{
		if($type ==1)
		{
			$add = ' AND v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id AND visit_invoice.bill_to = visit_type.visit_type_id ';
			$table_add = ',visit_invoice,visit_type';
		}
		else
		{
			$table_add='';
			$add = '';
		}


		if($preauth_status == 1 )
		{
			$add .= ' AND (v_transactions_by_date.preauth_status = 1 OR v_transactions_by_date.preauth_status = 2)';
		}
		else
		{
			$add .= ' AND (v_transactions_by_date.preauth_status = 0 OR v_transactions_by_date.preauth_status = 3)';
		}
		$table = "v_transactions_by_date".$table_add;
		$where = "v_transactions_by_date.patient_id = '".$patient_id."' ".$add;

		if($preauth_status == 0)
		{
			$items = "*";
		}
		else
		{
			$items = "*";
		}
		
	
		$this->db->where($where);
		$this->db->select($items);
		if($preauth_status == 0)
		{
			$this->db->group_by('visit_invoice.visit_invoice_id');
		}
		
		$query_waiver = $this->db->get($table);

		
		return $query_waiver;
	}
	public function get_visit_invoice_details($visit_invoice_id)
	{
		$this->db->from('visit_invoice,visit_type');
		$this->db->select('visit_invoice.*,visit_type_name');
		$this->db->where('visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_id = '.$visit_invoice_id);
		$query = $this->db->get('');
		
		return $query;
	}
	function get_visit_charges_charged($visit_id,$visit_invoice_id = NULL)
	{
		if($visit_invoice_id > 0)
		{
			$add = ' AND visit_charge.visit_invoice_id = '.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_invoice_id = 0';
		}
		$this->db->select('*');
		$this->db->where('visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit_charge.visit_id = '.$visit_id.$add);
		$query = $this->db->get('visit_charge,service_charge');
		
		return $query;
	}
	public function get_patient_invoices($patient_id)
	{
		  $select_statement = "
                        SELECT
                          data.visit_invoice_id AS visit_invoice_id,
                          data.visit_invoice_number AS visit_invoice_number,
                          data.invoice_date AS visit_invoice_date,
                          data.visit_type_name AS visit_type_name,
                          COALESCE (SUM(data.dr_amount),0) AS dr_amount,
                          COALESCE (SUM(data.cr_amount),0) AS cr_amount,
                          COALESCE (SUM(data.dr_amount),0) - COALESCE (SUM(data.cr_amount),0) AS balance
                        FROM 
                        (     
                            SELECT
                            	visit_invoice.visit_invoice_id as visit_invoice_id,
	                            visit_invoice.visit_invoice_number as visit_invoice_number,
	                            visit_invoice.visit_id as visit_id,
	                            visit_invoice.patient_id as patient_id,
	                            visit.visit_date as invoice_date,
	                            'Invoice' AS creditor_invoice_type,
	                            COALESCE (SUM((`visit_charge`.`visit_charge_amount` * `visit_charge`.`visit_charge_units`)),0) AS dr_amount,
	                            0 AS cr_amount,
	                            visit_type.visit_type_name AS visit_type_name
								FROM
									visit_charge,visit_invoice,visit,visit_type
									WHERE visit.visit_delete = 0 AND visit_charge.charged = 1 
									AND visit_charge.visit_charge_delete = 0 
									AND visit_type.visit_type_id = visit_invoice.bill_to
									AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
									AND visit.visit_id = visit_charge.visit_id
									AND visit_invoice.visit_invoice_delete = 0
								GROUP BY visit_charge.visit_invoice_id

                            

                          

                          ) AS data WHERE data.patient_id = ".$patient_id."  GROUP BY data.visit_invoice_number ORDER BY data.invoice_date ASC ";
                          $query = $this->db->query($select_statement);


       //                    UNION ALL 

                           
							// SELECT
       //                      	visit_invoice.visit_invoice_id as visit_invoice_id,
	      //                       visit_invoice.visit_invoice_number as visit_invoice_number,
	      //                       visit_invoice.visit_id as visit_id,
	      //                       visit_invoice.patient_id as patient_id,
	      //                       visit.visit_date as invoice_date,
	      //                       'Invoice' AS creditor_invoice_type,
	      //                       COALESCE (SUM((`visit_charge`.`visit_charge_amount` * `visit_charge`.`visit_charge_units`)),0) AS dr_amount,
	      //                       ((SELECT COALESCE (SUM(`payment_item`.`payment_item_amount`),0) FROM payment_item,payments WHERE `payment_item`.`visit_invoice_id` = `visit_invoice`.`visit_invoice_id` 
							// 		AND `payment_item`.`payment_id` = `payments`.`payment_id` AND payments.cancel = 0) + (SELECT COALESCE (SUM(`visit_credit_note_item`.`visit_cr_note_amount`* visit_credit_note_item.visit_cr_note_units),0) FROM visit_credit_note_item,visit_credit_note WHERE `visit_credit_note`.`visit_invoice_id` = `visit_invoice`.`visit_invoice_id` 
							// 		AND `visit_credit_note_item`.`visit_credit_note_id` = `visit_credit_note`.`visit_credit_note_id` AND visit_credit_note.visit_cr_note_delete = 0)) AS cr_amount,
	      //                       visit_type.visit_type_name AS visit_type_name
							// 	FROM
							// 		visit_charge,visit_invoice,visit,visit_type
							// 		WHERE visit.visit_delete = 0 AND visit_charge.charged = 1 
							// 		AND visit_charge.visit_charge_delete = 0 
							// 		AND visit_type.visit_type_id = visit_invoice.bill_to
							// 		AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
							// 		AND visit.visit_id = visit_charge.visit_id
							// 		AND visit_invoice.visit_invoice_delete = 0
							// 	GROUP BY visit_charge.visit_invoice_id


                  return $query;
	}
	public function get_payment_items($patient_id,$payment_id = NULL)
	{
		if($payment_id > 0)
		{
			$add = ' AND payment_item.payment_id = '.$payment_id;
		}
		else
		{
			$add = ' AND payment_item.payment_id = 0';
		}
		$this->db->where('payment_item.patient_id = '.$patient_id.$add);
		$this->db->join('visit_invoice','visit_invoice.visit_invoice_id = payment_item.visit_invoice_id','LEFT');
		$this->db->join('payments','payments.payment_id = payment_item.payment_id AND payments.cancel = 0','LEFT');
		$query = $this->db->get('payment_item');
		return $query;
	}

	public function add_payment_item($patient_id,$payment_id)
	{

	    $amount = $this->input->post('amount');
	    $creditor_invoice_id = $this->input->post('invoice_detail');



	    // if(empty($creditor_invoice_id))
	    // {
	    //   $invoice_type = 2;
	    // }
	    // else
	    // {
	      $exploded = explode('.', $creditor_invoice_id);
	      $invoice_id = $exploded[0];
	      $invoice_number = $exploded[1];
	      $invoice_type = $exploded[2];

	    // }

	    $service = array(
	              'visit_invoice_id'=>$invoice_id,
	              'invoice_type'=>$invoice_type,
	              'patient_id' => $patient_id,
	              'created_by' => $this->session->userdata('personnel_id'),
	              'created' => date('Y-m-d'),
	              'payment_item_amount'=>$amount,
	              // 'payment_id'=>NULL
	            );
	            // var_dump($payment_id);die();

	    if($payment_id > 0)
	    {
	      $service['payment_id'] = $payment_id;
	    }
	    else
	    {
	    	$service['payment_id'] = 0;
	    }

	    $this->db->insert('payment_item',$service);
	    return TRUE;

	}

	public function confirm_payment($patient_id,$payment_id=null)
	{
		
		$payment_method=$this->input->post('payment_method');
		$type_payment=$this->input->post('type_payment');
		$payment_service_id=$this->input->post('payment_service_id');
		
		if($type_payment == 1)
		{
			$payment_service_id=$this->input->post('payment_service_id');
			$amount = $this->input->post('amount_paid');
		}
		
		else
		{
			$payment_service_id=$this->input->post('waiver_service_id');
			$amount = $this->input->post('waiver_amount');
		}
		
		if($payment_method == 1)
		{
			// check for cheque number if inserted
			
			$transaction_code = $this->input->post('cheque_number');
		}
		else if($payment_method == 6)
		{
			// check for insuarance number if inserted
			$transaction_code = $this->input->post('debit_card_detail');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else if($payment_method == 7)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('deposit_detail');
		}
		else if($payment_method == 8)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('debit_card_detail');
		}
		else
		{
			$transaction_code = '';
		}
		$change = $this->input->post('change_payment');

		$reason = $this->input->post('reason');

		$total_amount = $this->input->post('total_amount');
		$branch_id = $this->session->userdata('branch_id');
		$data = array(
						'patient_id' => $patient_id,
						'payment_method_id'=>$payment_method,
						'amount_paid'=>$total_amount,
						'personnel_id'=>$this->session->userdata("personnel_id"),
						'payment_type'=>$type_payment,
						'transaction_code'=>$transaction_code,
						'reason'=>$reason,
						'payment_service_id'=>$payment_service_id,
						'change'=>0,
						'branch_id'=>$branch_id,
						'payment_date'=>$this->input->post('payment_date'),
						'payment_created_by'=>$this->session->userdata("personnel_id"),
						'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
					);

		if($payment_id > 0)
		{
			$this->db->where('payment_id',$payment_id);
			if($this->db->update('payments', $data))
			{
				 $total_visits = sizeof($_POST['visit_payment_items']);
		          //check if any checkboxes have been ticked
		          if($total_visits > 0)
		          {
		            for($r = 0; $r < $total_visits; $r++)
		            {
		              $visit = $_POST['visit_payment_items'];
		              $payment_item_id = $visit[$r];
		              //check if card is held
		              $service = array(
		                        'payment_id'=>$payment_id,
		                        // 'charged'=>1,
		                      );
		              $this->db->where('payment_item_id',$payment_item_id);
		              $this->db->update('payment_item',$service);
		            }
		          }

		          // checnk the paid items



		          $this->update_visit_invoice_data($payment_id);

		          return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else
		{
			$data['payment_created'] = date("Y-m-d");
			$prefix = $suffix = $this->create_receipt_number();
			// var_dump($prefix);die();
			$branch_code = $this->session->userdata('branch_code');
			$branch_id = $this->session->userdata('branch_id');

			// $branch_code = str_replace("RS", "CT", $branch_code);

			// if($branch_code == "CT")
			// {
				$branch_code = "CTR";
			// }



			
			if($prefix < 10)
			{
				$number = '0000'.$prefix;
			}
			else if($prefix < 100 AND $prefix >= 10)
			{
				$number = '000'.$prefix;
			}
			else if($prefix < 1000 AND $prefix >= 100)
			{
				$number = '00'.$prefix;
			}
			else if($prefix < 10000 AND $prefix >= 1000)
			{
				$number = '0'.$prefix;
			}
			else if($prefix < 100000 AND $prefix >= 10000)
			{
				$number = $prefix;
			}

			$branch_id = $this->session->userdata('branch_id');
			$invoice_number = $branch_code.$number;
			$data['confirm_number'] = $invoice_number;
			$data['suffix'] = $suffix;
			$data['prefix'] = $prefix;
			$data['branch_id'] = $branch_id;
			$data['branch_id'] = $branch_id;


			if($this->db->insert('payments', $data))
			{
				$payment_id =  $this->db->insert_id();

				 $total_visits = sizeof($_POST['visit_payment_items']);
		          //check if any checkboxes have been ticked
		          if($total_visits > 0)
		          {
		            for($r = 0; $r < $total_visits; $r++)
		            {
		              $visit = $_POST['visit_payment_items'];
		              $payment_item_id = $visit[$r];
		              //check if card is held
		              $service = array(
		                        'payment_id'=>$payment_id,
		                        // 'charged'=>1,
		                      );
		              $this->db->where('payment_item_id',$payment_item_id);
		              $this->db->update('payment_item',$service);
		            }
		          }
		         $this->update_visit_invoice_data($payment_id);
		          return TRUE;
			}
			else{
				return FALSE;
			}
		
		}
	}
	
	function convert_number($number) {
		if (($number < 0) || ($number > 999999999)) {
			throw new Exception("Number is out of range");
		}

		$Gn = floor($number / 1000000);
		/* Millions (giga) */
		$number -= $Gn * 1000000;
		$kn = floor($number / 1000);
		/* Thousands (kilo) */
		$number -= $kn * 1000;
		$Hn = floor($number / 100);
		/* Hundreds (hecto) */
		$number -= $Hn * 100;
		$Dn = floor($number / 10);
		/* Tens (deca) */
		$n = $number % 10;
		/* Ones */

		$res = "";

		if ($Gn) {
			$res .= $this->convert_number($Gn) .  "Million";
		}

		if ($kn) {
			$res .= (empty($res) ? "" : " ") .$this->convert_number($kn) . " Thousand";
		}

		if ($Hn) {
			$res .= (empty($res) ? "" : " ") .$this->convert_number($Hn) . " Hundred";
		}

		$ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
		$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

		if ($Dn || $n) {
			if (!empty($res)) {
				$res .= " and ";
			}

			if ($Dn < 2) {
				$res .= $ones[$Dn * 10 + $n];
			} else {
				$res .= $tens[$Dn];

				if ($n) {
					$res .= " " . $ones[$n];
				}
			}
		}

		if (empty($res)) {
			$res = "zero";
		}

		return $res;
	}


	public function get_payment_details($payment_id)
	{
		$table = "payments,payment_method,payment_item";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND payment_item.payment_id = payments.payment_id AND payments.payment_id =".$payment_id;
		$items = "payments.*,payment_item.*,visit_invoice.visit_invoice_number,payment_method.payment_method";
		$order = "payments.payment_id";

		$this->db->where($where);
		$this->db->select($items);
		$this->db->join('visit_invoice','visit_invoice.visit_invoice_id = payment_item.visit_invoice_id','LEFT');
		$query = $this->db->get($table);
		
		
		return $query;
	}


	public function get_cash_invoice_payments_id($visit_invoice_id)
	{
		$table = "payments,payment_method,payment_item";
		$where = "payments.cancel = 0 AND payment_type = 1 AND payment_method.payment_method_id = payments.payment_method_id AND payment_item.payment_id = payments.payment_id AND payment_item.visit_invoice_id =".$visit_invoice_id;
		$items = "payments.*,payment_item.*,visit_invoice.visit_invoice_number,payment_method.payment_method";
		$order = "payments.payment_id";

		$this->db->where($where);
		$this->db->select('sum(payment_item_amount) AS total_payments');
		$this->db->join('visit_invoice','visit_invoice.visit_invoice_id = payment_item.visit_invoice_id','LEFT');
		$query = $this->db->get($table);
		
		
		return $query;
	}

	function get_months()
	{

		return $this->db->get('month');
	}
	function get_bank_accounts($status = null)
	{
		if(!empty($status))
		{
			$add = ' AND paying_account = 1';
		}
		else
		{
			$add = '';
		}
		$this->db->where('parent_account = 1'.$add);
		return $this->db->get('account');
	}

	function get_incomplete_invoices($patient_id,$visit_id,$visit_date= NULL,$visit_invoice_id=NULL,$group_by=NULL)
	{
		if(!empty($visit_date))
		{
			// $date_add = ' AND visit_charge.date = "'.$visit_date.'"';
			$date_add = '';
		}
		else
		{
			$date_add ='';
		}


		if(!empty($visit_invoice_id))
		{
			$date_add = ' AND visit_charge.visit_invoice_id = "'.$visit_invoice_id.'"';
			// $date_add = '';
		}
		else
		{
			$date_add =' AND visit_charge.visit_invoice_id = 0';
		}


		
		$date_add .= ' AND visit_charge.visit_id = "'.$visit_id.'"';
			
	
		$this->db->select('*,visit_charge.created_by AS personnel_creator');
		$this->db->where('visit.visit_id = visit_charge.visit_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit.visit_delete = 0   AND visit.patient_id = '.$patient_id.$date_add);
		$this->db->order_by('visit_charge.date','DESC');

		if(!empty($group_by))
		{
			$this->db->group_by('visit_charge.date');
		}
		return $this->db->get('visit_charge,visit,service_charge');
	}

	public function confirm_invoice_charges($visit_id,$patient_id,$visit_invoice_id = NULL)
	{
		$amount = $this->input->post('amount');

		$preauth_date = $this->input->post('preauth_date');
		$invoice_date = $this->input->post('invoice_date');
		$preauth_amount = $this->input->post('preauth_amount');
		$amount = $this->input->post('amount');
		$scheme_name = $this->input->post('scheme_name');
		$visit_type_id = $this->input->post('visit_type_id');
		$member_number = $this->input->post('member_number');
		$insurance_limit = $this->input->post('insurance_limit');
		$visit_invoice_id = $this->input->post('visit_invoice_id');
		$dentist_id = $this->input->post('dentist_id');
		$preauth = $this->input->post('preauth');
		$approved_amount = $this->input->post('approved_amount');



		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];

		if(empty($preauth_date))
		{
			$preauth_date = $invoice_date;
		}

		
		// var_dump($amount);die();
		// var_dump($preauth_date); die();

		
		$insertarray['patient_id'] = $patient_id;
		
		$insertarray['created_by'] = $this->session->userdata('personnel_id');		
		// $insertarray['visitamount'] = $amount;

		
	    	$branch_id = $this->session->userdata('branch_id');

	    if($visit_invoice_id > 0)
	    {
	    	$insertarray['preauth_date'] = $preauth_date;
	    	if(empty($preauth_amount))
	    	{
	    		$preauth_amount = 0;
	    	}
	    	if(empty($approved_amount))
	    	{
	    		$approved_amount = 0;
	    	}	

	    	$visit_invoice_number = $this->input->post('visit_invoice_number');

	    	$insertarray['visit_invoice_number'] = $visit_invoice_number;

	    	$preauth_amount = str_replace(",", "", $preauth_amount);
					$preauth_amount = str_replace(".00", "", $preauth_amount);
	      	$insertarray['preauth_amount'] = $preauth_amount;
	      	$insertarray['created'] = $invoice_date;
	      	$insertarray['member_number'] = $member_number;
	      	$insertarray['scheme_name'] = $scheme_name;
	      	$insertarray['bill_to'] = $visit_type_id;
	      	$insertarray['insurance_limit'] = $insurance_limit;
	      	$insertarray['branch_id'] = $branch_id;
	      	$insertarray['preauth_status'] = $preauth;
	      	$insertarray['approved_amount'] = $approved_amount;
	        $this->db->where('visit_invoice_id',$visit_invoice_id);
	        if($this->db->update('visit_invoice', $insertarray))
	        {

	          $total_visits = sizeof($_POST['visit_invoice_items']);
	          //check if any checkboxes have been ticked

	          // var_dump($total_visits);die();
	          if($total_visits > 0)
	          {
	            for($r = 0; $r < $total_visits; $r++)
	            {
	              $visit = $_POST['visit_invoice_items'];
	              $visit_charge_id = $visit[$r];
	              //check if card is held
	              $service = array(
	                        'visit_invoice_id'=>$visit_invoice_id,
	                        // 'charged'=>1,
	                      );
	              $this->db->where('visit_charge_id',$visit_charge_id);
	              $this->db->update('visit_charge',$service);
	               // $this->update_product_details($visit_charge_id);
	            }
	          }

	        
	        }


	         if($preauth_amount < $amount)
	        {
	        	$visit_bill_reason = $this->input->post('visit_bill_reason');
	        	// add visit charge
	        	$balance = $amount - $preauth_amount;
	        	$array['service_charge_id'] = 1435;
						$array['visit_id'] = $visit_id;
						$array['visit_charge_units'] = 1;
						$array['visit_charge_notes'] = $visit_bill_reason;
						$array['visit_charge_comment'] = $visit_bill_reason;
						$array['visit_charge_amount'] = -$balance;
						$array['date']= $preauth_date;
						$array['charge_to']= $visit_type_id;
						$array['charged']= 1;
						$array['patient_id']= $patient_id;
						$array['visit_invoice_id']= $visit_invoice_id;

						// var_dump($array);die();
						$this->db->insert('visit_charge',$array);





						// create another visit invoice
						$prefix2 = $this->create_invoice_number();

					  	$invoice_number = $prefix2;


						$insert_array_two['created_by'] = $this->session->userdata('personnel_id');		
						$insert_array_two['visit_invoice_number'] = $invoice_number;
						$insert_array_two['created'] = $invoice_date;
						$insert_array_two['patient_id'] = $patient_id;
						$insert_array_two['visit_id'] = $visit_id;
						$insert_array_two['prefix'] = $branch_code;
						$insert_array_two['suffix'] = $prefix2;
						$insert_array_two['bill_to'] = 1;
						$insert_array_two['scheme_name'] = '';
						$insert_array_two['member_number'] = '';
						$insert_array_two['branch_id'] = $branch_id;
						$insert_array_two['dentist_id'] = $dentist_id;
						$insert_array_two['parent_visit_invoice_id'] = $visit_invoice_id;
						
						$this->db->insert('visit_invoice',$insert_array_two);
						$visit_invoice_idd = $this->db->insert_id();


			      $array_six['service_charge_id'] = 1435;
						$array_six['visit_id'] = $visit_id;
						$array_six['visit_charge_units'] = 1;
						$array_six['visit_charge_notes'] = $visit_bill_reason;
						$array_six['visit_charge_comment'] = $visit_bill_reason;
						$array_six['visit_charge_amount'] = $balance;
						$array_six['date']= $preauth_date;
						$array_six['charge_to']= 1;
						$array_six['charged']= 1;
						$array_six['patient_id']= $patient_id;
						$array_six['visit_invoice_id']= $visit_invoice_idd;
						// var_dump($array_six);die();
						$this->db->insert('visit_charge',$array_six);





	        }
	        $this->update_invoice_totals($visit_invoice_id);
	          return TRUE;
	    }
	    else
	    {
	    	$prefix = $this->create_invoice_number();
		
		  $invoice_number = $prefix;
		
		  $insertarray['visit_id'] = $visit_id;
	   	  $document_number = $invoice_number;
	      $insertarray['visit_invoice_number'] = $document_number;
	      $insertarray['prefix'] = $branch_code;
	      $insertarray['suffix'] = $prefix;
	      $insertarray['preauth_date'] = $preauth_date;
	      $insertarray['created'] = $invoice_date;
	      if(empty($preauth_amount))
	    	{
	    		$preauth_amount = 0;
	    	}


	    	if(empty($approved_amount))
	    	{
	    		$approved_amount = 0;
	    	}

	    	$preauth_amount = str_replace(",", "", $preauth_amount);
			$preauth_amount = str_replace(".00", "", $preauth_amount);
			$insertarray['preauth_amount'] = $preauth_amount;

			$insertarray['member_number'] = $member_number;
	      	$insertarray['scheme_name'] = $scheme_name;
	      	$insertarray['bill_to'] = $visit_type_id;
	      	$insertarray['insurance_limit'] = $insurance_limit;
	      	$insertarray['dentist_id'] = $dentist_id;
	      	$insertarray['branch_id'] = $branch_id;
	      	$insertarray['preauth_status'] = $preauth;
	      	$insertarray['approved_amount'] = $approved_amount;
	      	$insertarray['authorising_officer'] = $this->input->post('authorising_officer');
			// $insertarray['created'] = date('Y-m-d');

	      // var_dump($insertarray);die();
	      if($this->db->insert('visit_invoice', $insertarray))
	      {

	        $visit_invoice_id = $this->db->insert_id();
	        $total_visits = sizeof($_POST['visit_invoice_items']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['visit_invoice_items'];
	            $visit_charge_id = $visit[$r];
	            //check if card is held
	            $service = array(
	                      'visit_invoice_id'=>$visit_invoice_id,
	                      // 'charged' =>1,
	                    );
	            $this->db->where('visit_charge_id',$visit_charge_id);
	            $this->db->update('visit_charge',$service);

	            // update product details

	            // $this->update_product_details($visit_charge_id);
	          }
	        }

	        if($preauth_amount < $amount)
	        {
	        	$visit_bill_reason = $this->input->post('visit_bill_reason');
	        	// add visit charge
	        	$balance = $amount - $preauth_amount;
	        	$array['service_charge_id'] = 1435;
						$array['visit_id'] = $visit_id;
						$array['visit_charge_units'] = 1;
						$array['visit_charge_notes'] = $visit_bill_reason;
						$array['visit_charge_comment'] = $visit_bill_reason;
						$array['visit_charge_amount'] = -$balance;
						$array['date']= $preauth_date;
						$array['charge_to']= $visit_type_id;
						$array['charged']= 1;
						$array['patient_id']= $patient_id;
						$array['visit_invoice_id']= $visit_invoice_id;

						// var_dump($array);die();
						$this->db->insert('visit_charge',$array);





						// create another visit invoice
						$prefix2 = $this->create_invoice_number();

					  	$invoice_number = $prefix2;


						$insert_array_two['created_by'] = $this->session->userdata('personnel_id');		
						$insert_array_two['visit_invoice_number'] = $invoice_number;
						$insert_array_two['created'] = $invoice_date;
						$insert_array_two['patient_id'] = $patient_id;
						$insert_array_two['visit_id'] = $visit_id;
						$insert_array_two['prefix'] = $branch_code;
						$insert_array_two['suffix'] = $prefix2;
						$insert_array_two['bill_to'] = 1;
						$insert_array_two['scheme_name'] = '';
						$insert_array_two['member_number'] = '';
						$insert_array_two['branch_id'] = $branch_id;
						$insert_array_two['dentist_id'] = $dentist_id;
						$insert_array_two['parent_visit_invoice_id'] = $visit_invoice_id;
						
						$this->db->insert('visit_invoice',$insert_array_two);
						$visit_invoice_idd = $this->db->insert_id();


			      $array_six['service_charge_id'] = 1435;
						$array_six['visit_id'] = $visit_id;
						$array_six['visit_charge_units'] = 1;
						$array_six['visit_charge_notes'] = $visit_bill_reason;
						$array_six['visit_charge_comment'] = $visit_bill_reason;
						$array_six['visit_charge_amount'] = $balance;
						$array_six['date']= $preauth_date;
						$array_six['charge_to']= 1;
						$array_six['charged']= 1;
						$array_six['patient_id']= $patient_id;
						$array_six['visit_invoice_id']= $visit_invoice_idd;
						// var_dump($array_six);die();
						$this->db->insert('visit_charge',$array_six);





	        }
	        $this->update_invoice_totals($visit_invoice_id);
	        return TRUE;
	      }
	    }
			

	}

	public function update_product_details($visit_charge_id)
	{
		// update all the

		$this->db->where('visit_charge_id',$visit_charge_id);
		$query = $this->db->get('visit_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;
				$store_id = $value->store_id;

			}


			$stock_level = $this->inventory_management_model->get_all_stock_per_store($product_id,$store_id);

      $purchases = $stock_level['dr_quantity'];
      $deductions = $stock_level['cr_quantity'];
     
      $in_stock = $purchases - $deductions;

      if($in_stock > 0)
      {
          $inventory_status = 1;
      }
      else if($in_stock < 0)
      {
          $inventory_status = 0;
      }
      else
      {
          $inventory_status = 2;
      }
      // if($store_id == 5)
      // {
      // 	$array_update['stock_amount'] = $in_stock;
      // }
      // else
      // {
      // 	 $array_update['stock_amount'] = $in_stock;
      // }
     

      // $array_update['inventory_status'] = $inventory_status;




      // $this->db->where('product_id',$product_id);
      // $this->db->update('product',$array_update);

      // update the store

      $store_update['store_balance'] = $in_stock;

      $this->db->where('owning_store_id = '.$store_id.' AND product_id = '.$product_id);
      $this->db->update('store_product',$store_update);

		}
	}
	

	public function get_patient_visit_charge_items_time_tree($visit_id,$visit_invoice_id=NULL)
	{
		if(!empty($visit_invoice_id))
		{
			$add = ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_id = '.$visit_id;
		}
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1  AND visit_charge.service_charge_id = service_charge.service_charge_id ". $add;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id,service.department_id,visit_charge.date AS billing_date";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);

		// $this->db->group_by('service.service_name');
		$this->db->group_by('visit_charge.date');
		$this->db->order_by('visit_charge.date');
		$result = $this->db->get($table);
		
		return $result;
	}


	public function get_patient_visit_charge_items_tree_olding($visit_id,$visit_invoice_id=NULL)
	{
		if(!empty($visit_invoice_id))
		{
			$add = ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_id = '.$visit_id;
		}
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_charge.plan_status <= 2 AND service.service_name <> 'Others' AND visit_charge.service_charge_id = service_charge.service_charge_id ". $add;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id,service.department_id";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);

		// $this->db->group_by('service.service_name');
		$this->db->group_by('service_charge.service_id');
		$this->db->order_by('visit_charge.date');
		$result = $this->db->get($table);
		
		return $result;
	}

		public function get_patient_visit_charge_items_tree($visit_id,$visit_invoice_id=NULL)
	{
		if(!empty($visit_invoice_id))
		{
			$add = ' AND visit_charge.visit_invoice_id ='.$visit_invoice_id;
		}
		else
		{
			$add = ' AND visit_charge.visit_id = '.$visit_id;
		}
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_charge.plan_status <= 2 AND service.service_name <> 'Others' AND visit_charge.service_charge_id = service_charge.service_charge_id ". $add;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id,service.department_id,visit_invoice_discounts.visit_invoice_discount,visit_invoice_discounts.visit_credit_note_id";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);

		$this->db->join('visit_invoice_discounts','visit_invoice_discounts.visit_invoice_id = visit_charge.visit_invoice_id AND visit_invoice_discounts.service_id = service.service_id','LEFT');
		$this->db->group_by('service_charge.service_id');
		$this->db->order_by('service.service_name');
		$result = $this->db->get($table);
		
		return $result;
	}


	public function get_patient_visit_charge_items_tree_departmnts($visit_id,$visit_invoice_id=NULL)
	{
		
		$add = ' AND visit_charge.visit_id = '.$visit_id;
		
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_invoice_id = 0 AND visit_charge.visit_charge_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_charge.plan_status <= 2 AND service.service_name <> 'Others' AND visit_charge.service_charge_id = service_charge.service_charge_id ". $add;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id,service.department_id";
		$order = "service.service_name";
		$this->db->where($where);
		$this->db->select($items);

		// $this->db->group_by('service.service_name');
		$this->db->group_by('service.department_id');
		$this->db->order_by('visit_charge.date');
		$result = $this->db->get($table);
		
		return $result;
	}

	public function get_visit_invoice_payments($visit_invoice_id,$type=NULL,$patient_id=NULL)
	{
		if($type == 1)
		{
			if($patient_id > 0 and $visit_invoice_id == 'null')
			{
				$this->db->select('SUM(payment_item_amount) AS total_amount,payment_method.payment_method,payments.payment_date,payments.confirm_number,payments.payment_id,payment_item.visit_invoice_id,payments.visit_id');
				$this->db->where('payments.cancel = 0 AND payments.payment_method_id = payment_method.payment_method_id AND payment_item.payment_item_deleted = 0 AND payments.payment_type >= 1 AND payments.payment_id = payment_item.payment_id AND payments.patient_id ='.$patient_id);

			}
			else
			{
				$this->db->select('SUM(payment_item_amount) AS total_amount,payment_method.payment_method,payments.payment_date,payments.confirm_number,payments.payment_id,payment_item.visit_invoice_id,payments.visit_id');
				$this->db->where('payments.cancel = 0 AND payments.payment_type >= 1  AND payments.payment_method_id = payment_method.payment_method_id AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
			}
		}
		else
		{
			$this->db->select('SUM(payment_item_amount) AS total_amount');
			$this->db->where('payments.cancel = 0 AND payments.payment_type >= 1  AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		}
		
		
		if($type == 1)
		{

			$this->db->group_by('payment_item.payment_id');
			$query = $this->db->get('payments,payment_item,payment_method');
		}
		else
		{
			$query = $this->db->get('payments,payment_item');
		}
		


		if($type == 1)
		{
			return $query;

		}
		else
		{

			$total_amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$total_amount = $value->total_amount;
				}
			}

			if(empty($total_amount))
			{
				$total_amount = 0;
			}

			return $total_amount;

		}
		
	}


	public function get_visit_refunds_payments($visit_invoice_id,$type=NULL,$patient_id=NULL)
	{
		if($type == 1)
		{
			if($patient_id > 0 and $visit_invoice_id == 'null')
			{
				$this->db->select('SUM(payment_item_amount) AS total_amount,account.account_name,payments.payment_date,payments.confirm_number,payments.payment_id,payment_item.visit_invoice_id');
				$this->db->where('payments.cancel = 0 AND payments.payment_method_id = account.account_id AND payment_item.payment_item_deleted = 0 AND payments.payment_type > 1 AND payments.payment_id = payment_item.payment_id AND payments.patient_id ='.$patient_id);

			}
			else
			{
				$this->db->select('SUM(payment_item_amount) AS total_amount,account.account_name,payments.payment_date,payments.confirm_number,payments.payment_id,payment_item.visit_invoice_id');
				$this->db->where('payments.cancel = 0 AND payments.payment_type > 1  AND payments.payment_method_id = account.account_id AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
			}
		}
		else
		{
			$this->db->select('SUM(payment_item_amount) AS total_amount');
			$this->db->where('payments.cancel = 0 AND payments.payment_type > 1  AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		}
		
		
		if($type == 1)
		{

			$this->db->group_by('payment_item.payment_id');
			$query = $this->db->get('payments,payment_item,account');
		}
		else
		{
			$query = $this->db->get('payments,payment_item');
		}
		


		if($type == 1)
		{
			return $query;

		}
		else
		{

			$total_amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$total_amount = $value->total_amount;
				}
			}

			if(empty($total_amount))
			{
				$total_amount = 0;
			}

			return $total_amount;

		}
		
	}


	public function get_visit_invoice_credit_notes($visit_invoice_id,$type=NULL,$patient_id=NULL)
	{
		if($type == 1)
		{
			// var_dump($visit_invoice_id);die();
			if($patient_id > 0 AND $visit_invoice_id == 'null')
			{
				$this->db->select('SUM(visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units) AS total_amount,visit_credit_note.*,visit_invoice.visit_invoice_number');
				$this->db->where('visit_credit_note.visit_cr_note_delete = 0  AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id AND visit_credit_note.patient_id ='.$patient_id);
			}
			else
			{
				$this->db->select('SUM(visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units) AS total_amount,visit_credit_note.*,visit_invoice.visit_invoice_number');
				$this->db->where('visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id  AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id AND visit_credit_note.visit_invoice_id ='.$visit_invoice_id);
			}
		}
		else
		{
			$this->db->select('SUM(visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units) AS total_amount,visit_credit_note.*');
			$this->db->where('visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id  AND visit_credit_note.visit_invoice_id ='.$visit_invoice_id);
		}
		
		
		if($type == 1)
		{
			$this->db->group_by('visit_credit_note_item.visit_credit_note_id');
			$query = $this->db->get('visit_credit_note_item,visit_credit_note,visit_invoice');
		}
		else
		{
			$query = $this->db->get('visit_credit_note_item,visit_credit_note');
		}
		


		if($type == 1)
		{
			return $query;

		}
		else
		{

			$total_amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$total_amount = $value->total_amount;
				}
			}

			if(empty($total_amount))
			{
				$total_amount = 0;
			}

			return $total_amount;

		}
		
	}

	public function get_visit_invoice_cash_payments($visit_invoice_id)
	{
		$this->db->select('SUM(payment_item_amount) AS total_amount');
		$this->db->where('payments.cancel = 0 AND payment_item.payment_item_deleted = 0 AND payments.payment_method_id < 9  AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('payments,payment_item');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;
	}

	public function get_visit_invoice_insurance_payments($visit_invoice_id)
	{
		$this->db->select('SUM(payment_item_amount) AS total_amount');
		$this->db->where('payments.cancel = 0 AND payment_item.payment_item_deleted = 0 AND payments.payment_method_id = 9 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('payments,payment_item');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;
	}

	public function get_visit_invoice_total($visit_invoice_id)
	{
		$this->db->select('SUM(visit_charge.visit_charge_units * visit_charge.visit_charge_amount) AS total_amount');
		$this->db->where('visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('visit_charge,visit_invoice');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;
	}

	public function visit_procedure_done($v_id,$procedure_id,$suck)
	{


		$service_charge_rs = $this->get_service_charge_item($procedure_id);

		foreach ($service_charge_rs as $key) :
			# code...
			$visit_charge_amount = $key->service_charge_amount;
		endforeach;
		// $rs = $this->check_visit_type($v_id);
		// if(count($rs)>0){
		//   foreach ($rs as $rs1) {
		// 	# code...
		// 	  $visit_t = $rs1->visit_type;
		//   }
		// }

		$visit_data = array('service_charge_id'=>$procedure_id,'visit_id'=>$v_id,'visit_charge_amount'=>$visit_charge_amount,'visit_charge_units'=>1,'charge_to'=>1,'created_by'=>$this->session->userdata("personnel_id"),'date'=>date("Y-m-d"),'charged'=>1,'visit_invoice_id'=>0,'visit_charge_qty'=>1,'visit_charge_comment'=>"");
		if($this->db->insert('patient_visit_procedure', $visit_data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	public function get_service_charge_item($procedure_id){
		$table = "service_charge";
		$where = "service_charge_id = '$procedure_id'";
		$items = "*";
		$order = "service_charge_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function visit_charge_insert($v_id,$procedure_id,$suck,$visit_invoice_id = NULL,$patient_id=NULL)
	{


		$service_charge_rs = $this->get_service_charge_item($procedure_id);

		foreach ($service_charge_rs as $key) :
			# code...
			$visit_charge_amount = $key->service_charge_amount;
			$lab_test_id = $key->lab_test_id;
			$service_charge_id = $key->service_charge_id;
		endforeach;
	
		$visit_id = $v_id;

			// check if the service charge has children 

			$children_rs = $this->get_service_charge_children($service_charge_id);

					// var_dump($children_rs);die();
			if($children_rs->num_rows() > 0)
			{

					$visit_data = array('service_charge_id'=>$procedure_id,'visit_id'=>$v_id,'visit_charge_amount'=>$visit_charge_amount,'visit_charge_units'=>1,'charge_to'=>1,'patient_id'=>$patient_id,'time'=>date('H:i:s'),'created_by'=>$this->session->userdata("personnel_id"),'date'=>date("Y-m-d"),'charged'=>1);

					if($visit_invoice_id > 0)
					{
						$visit_data['visit_invoice_id'] = $visit_invoice_id;
					}
					else
					{
						$visit_data['visit_invoice_id'] = 0;
					}
					// var_dump($visit_data);die();
					$this->db->insert('visit_charge', $visit_data);
					$visit_charge_id = $this->db->insert_id();

				foreach ($children_rs->result() as $key => $value) {
					// code...
					$service_charge_id = $value->service_charge_id;
					$lab_test_id = $value->lab_test_id;
					$visit_charge_amount = $value->service_charge_amount;

						// var_dump($lab_test_id);die();
					if($lab_test_id > 0)
					{

					
						//get lab test formats
						$get_lab_visit_rs = $this->lab_model->get_visits_lab_result($visit_id, $lab_test_id);
						$num_rows = count($get_lab_visit_rs);

						// var_dump($num_rows);die();
						
						if ($num_rows == 0 ){//if no formats
							$rs = $this->lab_model->get_lab_visit($visit_id, $service_charge_id);
							$num_visit = count($rs);
							
							//save lab test into the lab billing table(may be multiple)
							$visit_lab_test_id = $this->lab_model->save_lab_visit_trail($visit_id, $service_charge_id);
							
							if($visit_lab_test_id != FALSE)
							{
								foreach ($get_lab_visit_rs as $key2 ): 		
									$lab_format_id = $key2->lab_test_format_id;
								
									$this->lab_model->save_lab_visit_format($visit_id, $service_charge_id, $lab_format_id, $visit_lab_test_id);
								endforeach;
								$this->lab_model->update_lab_test_charge_to_visit_charge_group($visit_id,$visit_lab_test_id,date('Y-m-d'),$status,$visit_charge_id);
							}
						}
						
						else{//if there are formats

							$rs = $this->lab_model->get_lab_visit($visit_id, $service_charge_id);
							$num_visit = count($rs);
							
							//save lab test into the lab billing table(may be multiple)
							$visit_lab_test_id = $this->lab_model->save_lab_visit_trail($visit_id, $service_charge_id);
							//echo $num_visit;
							if($visit_lab_test_id != FALSE)
							{
								foreach ($get_lab_visit_rs as $key2 ): 		
									$lab_format_id = $key2->lab_test_format_id;
								
									$this->lab_model->save_lab_visit_format($visit_id, $service_charge_id, $lab_format_id, $visit_lab_test_id);
								endforeach;
								$this->lab_model->update_lab_test_charge_to_visit_charge_group($visit_id,$visit_lab_test_id,date('Y-m-d'),$status,$visit_charge_id);
							}
						}

							// return TRUE;
					}
					else
					{



							$visit_data = array('service_charge_id'=>$procedure_id,'visit_id'=>$v_id,'visit_charge_amount'=>$visit_charge_amount,'visit_charge_units'=>1,'charge_to'=>1,'patient_id'=>$patient_id,'time'=>date('H:i:s'),'created_by'=>$this->session->userdata("personnel_id"),'date'=>date("Y-m-d"),'charged'=>1);

							if($visit_invoice_id > 0)
							{
								$visit_data['visit_invoice_id'] = $visit_invoice_id;
							}
							else
							{
								$visit_data['visit_invoice_id'] = 0;
							}
							// var_dump($visit_data);die();
							if($this->db->insert('visit_charge', $visit_data))
							{


								return TRUE;
							}
							else
							{
								return FALSE;
							}
					}

				}
			}
			else
			{

				if($lab_test_id > 0)
				{

				
					//get lab test formats
					$get_lab_visit_rs = $this->lab_model->get_visits_lab_result($visit_id, $lab_test_id);
					$num_rows = count($get_lab_visit_rs);

					// var_dump($num_rows);die();
					
					if ($num_rows == 0 ){//if no formats
						$rs = $this->lab_model->get_lab_visit($visit_id, $service_charge_id);
						$num_visit = count($rs);
						
						//save lab test into the lab billing table(may be multiple)
						$visit_lab_test_id = $this->lab_model->save_lab_visit_trail($visit_id, $service_charge_id);
						
						if($visit_lab_test_id != FALSE)
						{
							foreach ($get_lab_visit_rs as $key2 ): 		
								$lab_format_id = $key2->lab_test_format_id;
							
								$this->lab_model->save_lab_visit_format($visit_id, $service_charge_id, $lab_format_id, $visit_lab_test_id);
							endforeach;
							$this->lab_model->update_lab_test_charge_to_visit_charge($visit_id,$visit_lab_test_id,date('Y-m-d'),$status,$patient_id);
						}
					}
					
					else{//if there are formats

						$rs = $this->lab_model->get_lab_visit($visit_id, $service_charge_id);
						$num_visit = count($rs);
						
						//save lab test into the lab billing table(may be multiple)
						$visit_lab_test_id = $this->lab_model->save_lab_visit_trail($visit_id, $service_charge_id);
						//echo $num_visit;
						if($visit_lab_test_id != FALSE)
						{
							foreach ($get_lab_visit_rs as $key2 ): 		
								$lab_format_id = $key2->lab_test_format_id;
							
								$this->lab_model->save_lab_visit_format($visit_id, $service_charge_id, $lab_format_id, $visit_lab_test_id);
							endforeach;
							$this->lab_model->update_lab_test_charge_to_visit_charge($visit_id,$visit_lab_test_id,date('Y-m-d'),$status,$patient_id);
						}
					}

						// return TRUE;
				}
				else
				{



						$visit_data = array('service_charge_id'=>$procedure_id,'visit_id'=>$v_id,'visit_charge_amount'=>$visit_charge_amount,'visit_charge_units'=>1,'charge_to'=>1,'patient_id'=>$patient_id,'time'=>date('H:i:s'),'created_by'=>$this->session->userdata("personnel_id"),'date'=>date("Y-m-d"),'charged'=>1);

						if($visit_invoice_id > 0)
						{
							$visit_data['visit_invoice_id'] = $visit_invoice_id;
						}
						else
						{
							$visit_data['visit_invoice_id'] = 0;
						}
						// var_dump($visit_data);die();
						if($this->db->insert('visit_charge', $visit_data))
						{


							return TRUE;
						}
						else
						{
							return FALSE;
						}
				}

			}

			
	}

	public function get_visit_type($visit_type_id)
	{
		$this->db->select('*');
		$this->db->from('visit_type');
		$this->db->where('visit_type_id = '.$visit_type_id);
		
		$query = $this->db->get();
		
		
		return $query;
	}
	public function create_invoice_number()
	{
		//select product code
		$this->db->where('visit_invoice_delete = 0');
		$this->db->from('visit_invoice');
		$this->db->select('suffix AS number');
		$this->db->order_by('suffix','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query->result()); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;


			$number++;//go to the next number
			$number = $number;
		}
		else{//start generating receipt numbers
			
			$number= 1;
		}
		return $number;
	}

	public function get_visit_credit_note($visit_id,$visit_credit_note_id = NULL,$patient_id=NULL)
	{
		if($visit_credit_note_id > 0)
		{
			$add = ' AND visit_credit_note_item.visit_credit_note_id = '.$visit_credit_note_id;
		}
		else
		{
			$add = ' AND visit_credit_note_item.visit_credit_note_id = 0';
		}
		$this->db->select('*');
		$this->db->where('visit_credit_note_item.service_charge_id = service_charge.service_charge_id AND visit_credit_note_item.visit_cr_note_item_delete = 0 AND visit_credit_note_item.patient_id = '.$patient_id.$add);
		$query = $this->db->get('visit_credit_note_item,service_charge');
		
		return $query;
	}


	public function get_visit_credit_note_details($visit_credit_note_id)
	{
		$this->db->from('visit_credit_note,visit_invoice');
		$this->db->select('visit_credit_note.*,visit_invoice.visit_invoice_number');
		$this->db->where('visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id AND visit_credit_note_id = '.$visit_credit_note_id);
		$query = $this->db->get('');
		
		return $query;
	}
	public function visit_cr_note_insert($v_id,$procedure_id,$suck,$visit_credit_note_id = NULL,$patient_id=NULL)
	{


		$service_charge_rs = $this->get_service_charge_item($procedure_id);

		foreach ($service_charge_rs as $key) :
			# code...
			$visit_charge_amount = $key->service_charge_amount;
		endforeach;
		
		if($v_id > 0)
		{
			$v_id = $v_id;
		}
		else
		{
			$v_id = 0;
		}

		if($patient_id > 0)
		{
			$patient_id = $patient_id;
		}
		else
		{
			$patient_id = 0;
		}

		$visit_data = array(
							'service_charge_id'=>$procedure_id,
							'visit_id'=>$v_id,
							'patient_id'=>$patient_id,
							'visit_cr_note_amount'=>$visit_charge_amount,
							'visit_cr_note_units'=>1,
							'created_by'=>$this->session->userdata("personnel_id"),
							'date'=>date("Y-m-d")
							);

		if($visit_credit_note_id > 0)
		{
			$visit_data['visit_credit_note_id'] = $visit_credit_note_id;
		}
		else
		{
			$visit_data['visit_credit_note_id'] = 0;
		}

		if($this->db->insert('visit_credit_note_item', $visit_data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function confirm_credit_note($visit_id,$patient_id,$visit_credit_note_id = NULL)
	{
		$amount = $this->input->post('amount');

		$preauth_amount = $this->input->post('preauth_amount');
		$visit_cr_note_number = $this->input->post('visit_cr_note_number');
		$invoice_date = $this->input->post('credit_note_date');
		// $visit_credit_note_id = $this->input->post('visit_credit_note_id');
		$visit_invoice_id = $this->input->post('visit_invoice_id');
	


		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];
		

		if($insurance_limit > 0)
		{
			$insurance_limit = $insurance_limit;
		}
		else

		{
			$insurance_limit = 0;
		}

		if(empty($authorising_officer))
		{
			$authorising_officer = 'SELF';
		}

		// var_dump($checked); die();
		$insertarray['patient_id'] = $patient_id;

		if($visit_id > 0)
		{
			$insertarray['visit_id'] = $visit_id;
		}
		
		
		$insertarray['created_by'] = $this->session->userdata('personnel_id');		
		// $insertarray['visitamount'] = $amount;

	    if($visit_credit_note_id > 0)
	    {
	    	$insertarray['created'] = $invoice_date;
	    	if(empty($preauth_amount))
	    	{
	    		$preauth_amount = 0;
	    	}

	    	$preauth_amount = str_replace(",", "", $preauth_amount);
			$preauth_amount = str_replace(".00", "", $preauth_amount);
	      	$insertarray['visit_cr_note_amount'] = $preauth_amount;
	      	$insertarray['visit_invoice_id'] = $visit_invoice_id;

	      	
	        $this->db->where('visit_credit_note_id',$visit_credit_note_id);
	        if($this->db->update('visit_credit_note', $insertarray))
	        {

	          $total_visits = sizeof($_POST['visit_invoice_items']);
	          //check if any checkboxes have been ticked
	          if($total_visits > 0)
	          {
	            for($r = 0; $r < $total_visits; $r++)
	            {
	              $visit = $_POST['visit_invoice_items'];
	              $visit_credit_note_item_id = $visit[$r];
	              //check if card is held
	              $service = array(
	                        'visit_credit_note_id'=>$visit_credit_note_id,
	                        // 'charged'=>1,
	                      );
	              $this->db->where('visit_credit_note_item_id',$visit_credit_note_item_id);
	              $this->db->update('visit_credit_note_item',$service);
	            }
	          }
	           $this->update_visit_invoice_data_credit($visit_invoice_id);
	          return TRUE;
	        }
	    }
	    else
	    {
			if(empty($visit_cr_note_number))
			{
				$document_number = $this->create_visit_credit_note_number();
			}
			else
			{
				$document_number = $visit_cr_note_number;
			}
			  
	      $insertarray['visit_cr_note_number'] = $document_number;
	     
	      if(empty($preauth_amount))
	    	{
	    		$preauth_amount = 0;
	    	}

	    	$preauth_amount = str_replace(",", "", $preauth_amount);
			$preauth_amount = str_replace(".00", "", $preauth_amount);
	      $insertarray['visit_cr_note_amount'] = $preauth_amount;
	      $insertarray['created'] = $invoice_date;
	      $insertarray['visit_invoice_id'] = $visit_invoice_id;
	      // var_dump($insertarray);die();
	      if($this->db->insert('visit_credit_note', $insertarray))
	      {

	        $visit_credit_note_id = $this->db->insert_id();
	        $total_visits = sizeof($_POST['visit_invoice_items']);
	        //check if any checkboxes have been ticked
	        if($total_visits > 0)
	        {
	          for($r = 0; $r < $total_visits; $r++)
	          {
	            $visit = $_POST['visit_invoice_items'];
	            $visit_credit_note_item_id = $visit[$r];
	            //check if card is held
	            $service = array(
	                      'visit_credit_note_id'=>$visit_credit_note_id,
	                      // 'charged' =>1,
	                    );
	            $this->db->where('visit_credit_note_item_id',$visit_credit_note_item_id);
	            $this->db->update('visit_credit_note_item',$service);
	          }
	        }

	         $this->update_visit_invoice_data_credit($visit_invoice_id);

	        return TRUE;
	      }
	    }
	   
	
		


	}

	public function create_visit_credit_note_number()
	{
		//select product code
		$this->db->where('visit_cr_note_number > 0');
		$this->db->from('visit_credit_note');
		$this->db->select('visit_cr_note_number AS number');
		$this->db->order_by('visit_cr_note_number','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query->result()); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;


			$number++;//go to the next number
			$number = $number;
		}
		else{//start generating receipt numbers
			
			$number= 1;
		}
		return $number;
	}

	public function update_visit_invoice_data($payment_id)
	{
		 $this->db->where('payment_id',$payment_id);
		$this->db->group_by('visit_invoice_id');
		$query = $this->db->get('payment_item');


		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$visit_invoice_id = $value->visit_invoice_id;


				if($visit_invoice_id > 0)
				{
					$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
					$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

					$journal = $this->accounts_model->get_visit_journal_total($visit_invoice_id);

					$balance = $total_invoice_amount - ($total_payments+$credit_note +$journal);
					// update visit invoice


					if($balance > 0)
					{
						$update_array['visit_invoice_status'] = 0;
					}
					else if($balance == 0)
					{
						$update_array['visit_invoice_status'] = 1;
					}
					else if($balance < 0)
					{
						$update_array['visit_invoice_status'] = 2;
					}
					else
					{
						$update_array['visit_invoice_status'] = 3;
					}


					$update_array['invoice_bill'] = $total_invoice_amount;
					$update_array['invoice_credit_note'] = $credit_note;
					$update_array['invoice_payments'] = $total_payments;
					$update_array['invoice_balance'] = $balance;
					$update_array['invoice_journal'] = $journal;
					$update_array['sync_status'] = 1;


					$this->db->where('visit_invoice_id',$visit_invoice_id);
					$this->db->update('visit_invoice',$update_array);
				}
			}
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		          
	}
	public function update_visit_invoice_data_credit($visit_invoice_id)
	{
		$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
		$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
		$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

		$journal = $this->accounts_model->get_visit_journal_total($visit_invoice_id);

		$balance = $total_invoice_amount - ($total_payments+$credit_note +$journal);
		// update visit invoice


		if($balance > 0)
		{
			$update_array['visit_invoice_status'] = 0;
		}
		else if($balance == 0)
		{
			$update_array['visit_invoice_status'] = 1;
		}
		else if($balance < 0)
		{
			$update_array['visit_invoice_status'] = 2;
		}
		else
		{
			$update_array['visit_invoice_status'] = 3;
		}


		$update_array['invoice_bill'] = $total_invoice_amount;
		$update_array['invoice_credit_note'] = $credit_note;
		$update_array['invoice_payments'] = $total_payments;
		$update_array['invoice_balance'] = $balance;
		$update_array['invoice_journal'] = $journal;
		$update_array['sync_status'] = 1;


		$this->db->where('visit_invoice_id',$visit_invoice_id);
		$this->db->update('visit_invoice',$update_array);
	}


	public function get_previous_payments($payment_id,$visit_invoice_id)
	{
		
		
		$this->db->select('SUM(payment_item_amount) AS total_amount');
		$this->db->where('payments.cancel = 0 AND payments.payment_type = 1 AND payments.payment_id < '.$payment_id.'  AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		
		
		
		
		$query = $this->db->get('payments,payment_item');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;

		
		
	}

	public function get_previous_refunds($payment_id,$visit_invoice_id)
	{
		
		
		$this->db->select('SUM(payment_item_amount) AS total_amount');
		$this->db->where('payments.cancel = 0 AND payments.payment_type > 1   AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		
		
		
		
		$query = $this->db->get('payments,payment_item');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;

		
		
	}

	public function get_initial_visit_billing($visit_id)
	{
		$this->db->select('SUM(visit_charge.visit_charge_units * visit_charge.visit_charge_amount) AS total_amount');
		$this->db->where('visit_charge.charged = 1 AND visit_charge.visit_id ='.$visit_id);
		$query = $this->db->get('visit_charge');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;
	}

	public function get_patient_visit_today($patient_id,$visit_id)
	{


		$this->db->select('*');
		$this->db->where('a.patient_id = '.$patient_id.' AND a.visit_id ='.$visit_id.' AND a.patient_id  IN (SELECT b.patient_id FROM visit AS b WHERE a.visit_id > b.visit_id AND b.visit_date = "'.date('Y-m-d').'")');
		$query = $this->db->get('visit AS a');

		return $query->num_rows();
	}
	public function check_invoiced($visit_id)
	{
		$this->db->select('SUM(visit_charge.visit_charge_units * visit_charge.visit_charge_amount) AS total_amount,visit_invoice.visit_invoice_id');
		$this->db->where('visit_charge.charged = 1 AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.visit_id ='.$visit_id);
		$this->db->group_by('visit_charge.visit_invoice_id');
		$query = $this->db->get('visit_charge,visit_invoice');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;

				$visit_invoice_id = $value->visit_invoice_id;


				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
				$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);


				$balance = ($total_amount - $credit_note) - $total_payments;

				$total_balance += $balance;

			}

			if($total_balance <= 0)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

		else
		{
			return FALSE;
		}

		
	}

		public function get_payment_before($payment_id,$visit_id)
	{
		$table = "payments";
		$where = "payments.payment_type = 1 AND payments.visit_id =". $visit_id.' AND cancel = 0 AND payment_id <'.$payment_id;
		$items = "SUM(payments.amount_paid) AS amount_paid";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			foreach ($result as $key):
				# code...
				$amount = $key->amount_paid;
				if(!is_numeric($amount))
				{
					return 0;
				}
				else
				{
					return $amount;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
	}
	public function get_payment_current($payment_id,$visit_id)
	{
		$table = "payments";
		$where = "payments.payment_type = 1 AND payments.visit_id =". $visit_id.' AND cancel = 0 AND payment_id  = '.$payment_id;
		$items = "SUM(payments.amount_paid) AS amount_paid";
		$order = "payments.payment_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		if(count($result) > 0)
		{
			foreach ($result as $key):
				# code...
				$amount = $key->amount_paid;
				if(!is_numeric($amount))
				{
					return 0;
				}
				else
				{
					return $amount;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
	}

	public function get_patient_visit_charge_items_inpatient($visit_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "visit_charge.visit_charge_units <> 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.service_charge_id = service_charge.service_charge_id AND charged = 1 AND visit_charge.visit_id =". $visit_id;
		$items = "service.service_id,service.service_name,service_charge.service_charge_name,visit_charge.service_charge_id,visit_charge.visit_charge_units, visit_charge.visit_charge_amount, visit_charge.visit_charge_timestamp,visit_charge.visit_charge_id,visit_charge.created_by, visit_charge.personnel_id";
		$order = "service.service_name";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}
	public function get_patient_visit_charge_inpatient($visit_id)
	{
		$table = "visit_charge, service_charge, service";
		$where = "service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_id =". $visit_id;
		$items = "DISTINCT(service_charge.service_id) AS service_id, service.service_name,";
		$order = "service_id";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	public function get_service_charge_children($service_charge_id)
	{
			$data['charge_group_status'] = 0;
			$this->db->where('charge_group_status = 0 AND service_charge_group.service_charge_id = service_charge.service_charge_id AND service_charge_group.parent_service_charge_id = '.$service_charge_id);
			$this->db->order_by('charge_group_id');
			
			return $this->db->get('service_charge_group,service_charge');
	}

	public function get_all_visits_inpatient_accounts($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_invoice.*,visit_type.visit_type_name');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
			$this->db->join('visit_type','visit_invoice.bill_to = visit_type.visit_type_id','Left');
		// $this->db->join('visit_invoice','visit_invoice.visit_id = visit.visit_id','Left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function update_all_other_charges($patient_id)
	{

		$this->db->where('inpatient = 1 AND visit.visit_id = visit_invoice.visit_id AND visit.patient_id = '.$patient_id);
		$query = $this->db->get('visit,visit_invoice');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$visit_id = $value->visit_id;
				$visit_invoice_id = $value->visit_invoice_id;

				$update_item['visit_invoice_id'] = $visit_invoice_id;
				$this->db->where('visit_id = '.$visit_id.' AND visit_invoice_id = 0 and charged = 1');
				$this->db->update('visit_charge',$update_item);



			}
		}


	}

	public function update_rebate_items($visit_invoice_id,$patient_id,$visit_id,$rebate_status)
	{

			$days = $this->days_billed($visit_invoice_id);	

			// var_dump($days);die();

		 if($rebate_status == 1 and $days > 0)
		 {

		 		$balance = $days * -1800;
		 		// check if billed
		 		$visit_bill_reason  = 'Rebate Deduction for '.$days.' ';

		 		$this->db->where('service_charge_id = 3959 AND visit_invoice_id ='.$visit_invoice_id); 
		 		$query = $this->db->get('visit_charge');

		 		if($query->num_rows() == 0)
		 		{
		 			// add

			      $array_six['service_charge_id'] = 3959;
						$array_six['visit_id'] = $visit_id;
						$array_six['visit_charge_units'] = $days;
						$array_six['visit_charge_qty'] = $days;
						$array_six['visit_charge_notes'] = $visit_bill_reason;
						$array_six['visit_charge_comment'] = $visit_bill_reason;
						$array_six['visit_charge_amount'] = $balance;
						$array_six['date']= $this->input->post('invoice_date');
						$array_six['charge_to']= 1;
						$array_six['charged']= 1;
						$array_six['patient_id']= $patient_id;
						$array_six['visit_charge_delete']= 0;
						$array_six['visit_invoice_id']= $visit_invoice_id;
						// var_dump($array_six);die();
						$this->db->insert('visit_charge',$array_six);

		 		}
		 		else
		 		{
		 			// update the with the correct quantity
		 				$balance = $days * -1800;

			      $array_six['service_charge_id'] = 3959;
						$array_six['visit_id'] = $visit_id;
						$array_six['visit_charge_units'] = $days;
						$array_six['visit_charge_qty'] = $days;
						$array_six['visit_charge_notes'] = $visit_bill_reason;
						$array_six['visit_charge_comment'] = $visit_bill_reason;
						$array_six['visit_charge_amount'] = $balance;
						$array_six['date']= $this->input->post('invoice_date');
						$array_six['charge_to']= 1;
						$array_six['charged']= 1;
						$array_six['patient_id']= $patient_id;
						$array_six['visit_charge_delete']= 0;
						$array_six['visit_invoice_id']= $visit_invoice_id;
						// var_dump($array_six);die();
						$this->db->where('service_charge_id = 3959 AND  visit_invoice_id ='.$visit_invoice_id);
						$this->db->update('visit_charge',$array_six);

		 		}



		 }
		 else if($rebate_status == 0)
		 {
		 		$this->db->where('service_charge_id = 3959 AND visit_invoice_id ='.$visit_invoice_id); 
		 		$query = $this->db->get('visit_charge');

		 		if($query->num_rows() > 0)
		 		{

		 				$array_six['visit_charge_delete']= 1;
						// var_dump($array_six);die();
						$this->db->where('service_charge_id = 3959 AND visit_invoice_id ='.$visit_invoice_id);
						$this->db->update('visit_charge',$array_six);

		 		}


		 }

	}

	public function days_billed($visit_invoice_id)
	{
		$this->db->where('visit_charge.service_charge_id = service_charge.service_charge_id AND service_charge.service_id = service.service_id AND service.service_name = "Bed charge" AND visit_charge.visit_charge_delete = 0 AND visit_charge.visit_invoice_id = '.$visit_invoice_id);
		$this->db->select('*');
		$query = $this->db->get('visit_charge,service_charge,service');

		return $query->num_rows();
	}

	public function update_invoice_totals($visit_invoice_id)
		{

			if($visit_invoice_id > 0)
			{
					$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
					$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
					$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

					$journal =  $this->accounts_model->get_visit_journal_total($visit_invoice_id);

					$balance = $total_invoice_amount - ($total_payments+$credit_note ) + $journal;
					// update visit invoice

									// var_dump($balance);die();
					if($balance > 0)
					{
						$update_array['visit_invoice_status'] = 0;
					}
					else if($balance == 0)
					{
						$update_array['visit_invoice_status'] = 1;
					}
					else if($balance < 0)
					{
						$update_array['visit_invoice_status'] = 2;
					}
					else
					{
						$update_array['visit_invoice_status'] = 3;
					}

					$update_array['invoice_bill'] = $total_invoice_amount;
					$update_array['invoice_credit_note'] = $credit_note;
					$update_array['invoice_payments'] = $total_payments;
					$update_array['invoice_balance'] = $balance;
					$update_array['invoice_journal'] = $journal;
					$update_array['sync_status'] = 1;

					$this->db->where('visit_invoice_id',$visit_invoice_id);
					$this->db->update('visit_invoice',$update_array);
				}


		}

		public function create_visit_receipt_number($prefix=null)
	{

		if(!empty($prefix))
		{
			$add = ' AND prefix = "'.$prefix.'"';
		}
		else
		{
			$prefix = "PAY";
			$add = ' AND prefix = "PAY"';
		}

		if($prefix == "PAY")
		{

			$add .= ' AND payments.payment_id > 1178 AND suffix >= 69914 AND payments.cancel = 0';
		}
		//select product code
		$this->db->where('suffix > 0'.$add);
		$this->db->from('payments');
		$this->db->select('MAX(suffix) AS number');
		$this->db->order_by('suffix','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
	
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			if(empty($number))
			{
				$number = 69915;
			}
			else
			{
					$number++;//go to the next number
					$number = $number;
			}
			
		}
		else{//start generating receipt numbers
			
			$number = 69915;
		}
			// var_dump($number); die();
		return $number;
	}
	public function get_visit_charge_details($visit_charge_id)
	{
		$this->db->where('visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_charge_id = '.$visit_charge_id);
		$this->db->select('visit_charge.*,service_charge.service_charge_name');
		$query = $this->db->get('service_charge,visit_charge');


		return $query;


	}
	public function get_patient_invoices_list($patient_id)
	{
		$this->db->where('visit_invoice.visit_invoice_delete = 0 AND visit_type.visit_type_id = visit_invoice.bill_to AND patient_id = '.$patient_id);
		$this->db->select('visit_invoice.*,visit_type.visit_type_name');
		$query = $this->db->get('visit_invoice,visit_type');

		return $query;
	}


	
	public function get_visit_balance($visit_id)
	{
		 $table = "visit_invoice";
		$where = "visit_id =".$visit_id;
		$items = "SUM(invoice_balance) AS total_payments";
		$order = "visit_invoice.visit_invoice_id";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);

		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		
		return $total_payments;
	}

	public function get_open_charges($visit_id)
	{
		 $table = "visit_charge";
		$where = "visit_charge.visit_invoice_id = 0 AND charged = 1 AND visit_id =".$visit_id;
		$items = "SUM(visit_charge_amount*visit_charge_units) AS total_payments";
		$order = "visit_charge.visit_charge_id";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);

		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		
		return $total_payments;
	}

	public function patient_balance($patient_id)
	{
		// code...
		$table = "visit_invoice";
		$where = "visit_invoice.visit_invoice_delete = 0 AND visit_invoice.patient_id =".$patient_id;
		$items = "SUM(invoice_balance) AS total_payments";
		$order = "visit_invoice.visit_invoice_id";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);

		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		
		return $total_payments;
	}

	public function get_visit_invoices_list($patient_id,$visit_id)
	{

		$table = "visit_invoice";
		$where = "visit_invoice.patient_id = ".$patient_id." AND visit_id =".$visit_id;
		$items = "*";
		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);


		return $query;
	}

	public function update_visit_totals($patient_id)
	{
		$this->db->where('patient_id',$patient_id);
		$query = $this->db->get('visit');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$visit_id = $value->visit_id;
				$visit_type = $value->visit_type;
				$visit_date = $value->visit_date;
				// $personnel_id = $value->personnel_id;

				$this->db->where('visit_id',$visit_id);
				$query_two = $this->db->get('visit_invoice');

				// var_dump($query_two);die();
				
				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value2) {
						// code...
						$visit_invoice_id = $value2->visit_invoice_id;
					}
				}
				else
				{
					  $array_invoice['patient_id'] = $patient_id;
					  $array_invoice['bill_to'] = $visit_type;
					  $array_invoice['created'] = $visit_date;
					  $array_invoice['visit_id'] = $visit_id;

						$array_invoice['prefix'] = $this->session->userdata('branch_code');
						$array_invoice['suffix'] = $visit_id;
						$array_invoice['visit_invoice_number'] = $visit_id;
						$this->db->insert('visit_invoice',$array_invoice);
						$visit_invoice_id = $this->db->insert_id();

				}


				if($visit_invoice_id > 0 AND $visit_id > 0)
				{
					// update visit charge
					$array_update['charged'] = 1;
					$array_update['visit_invoice_id'] = $visit_invoice_id;
					$array_update['patient_id'] = $patient_id;


					$this->db->where('visit_charge_delete = 0 AND visit_id = '.$visit_id);
					$this->db->update('visit_charge',$array_update);




					$this->db->where('payments.visit_id = visit.visit_id AND payments.payment_type = 1  AND visit.visit_id = visit_invoice.visit_id AND payments.visit_id ='.$visit_id.' AND payments.payment_id NOT IN (SELECT payment_id FROM payment_item)');
					$this->db->select('payments.*,visit.patient_id AS patient,visit_invoice.visit_invoice_id,visit.visit_id,payments.approved_by,payments.payment_id');
					$this->db->order_by('payments.payment_id','ASC');
					$query = $this->db->get('payments,visit,visit_invoice');

					// var_dump($query);die();
					if($query->num_rows() > 0)
					{
						foreach ($query->result() as $key => $value) {
							# code...
							$payment_created = $value->payment_created;
							$amount_paid = $value->amount_paid;
							$patient_id = $value->patient;
							$payment_created = $value->payment_created;
							$visit_invoice_id = $value->visit_invoice_id;
							$visit_id = $value->visit_id;
							$created_by = $value->approved_by;
							$payment_id = $value->payment_id;


							$array['patient_id'] = $patient_id;
							$array['payment_id'] = $payment_id;
							$array['visit_invoice_id'] = $visit_invoice_id;
							$array['visit_id'] = $visit_id;
							$array['payment_item_amount'] = $amount_paid;
							$array['created_by'] = $created_by;
							$array['created'] = $payment_created;
							$array['invoice_type'] = 1;
							$array['patient_id'] = $patient_id;
							$array['payment_item_deleted'] = 0;

							$this->db->insert('payment_item',$array);

							$array_two['payment_date'] = $payment_created;
							$array_two['patient_id'] = $patient_id;
							$array_two['confirm_number'] = $payment_id;
							$this->db->where('payment_id',$payment_id);
							$this->db->update('payments',$array_two);
							
						}
					}


				}



			}
		}
	}

	public function get_visit_journal_total($visit_invoice_id)
	{
		$this->db->select('SUM(journal_amount) AS total_amount');
		$this->db->where('patients_journals.visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('patients_journals');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;
	}

	public function mark_bills_as_charged($visit_id)
	{
		$this->db->where('visit_id',$visit_id);
		$query = $this->db->get('visit');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$visit_id = $value->visit_id;
				$visit_type = $value->visit_type;
				$visit_date = $value->visit_date;
				$patient_id = $value->patient_id;
				// $personnel_id = $value->personnel_id;

				$this->db->where('visit_id',$visit_id);
				$query_two = $this->db->get('visit_invoice');

				// var_dump($query_two);die();
				
				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value2) {
						// code...
						$visit_invoice_id = $value2->visit_invoice_id;
					}
				}
				else
				{
					  $array_invoice['patient_id'] = $patient_id;
					  $array_invoice['bill_to'] = $visit_type;
					  $array_invoice['created'] = $visit_date;
					  $array_invoice['visit_id'] = $visit_id;

						$array_invoice['prefix'] = $this->session->userdata('branch_code');
						$array_invoice['suffix'] = $visit_id;
						$array_invoice['visit_invoice_number'] = $visit_id;
						$this->db->insert('visit_invoice',$array_invoice);
						$visit_invoice_id = $this->db->insert_id();

				}


				if($visit_invoice_id > 0 AND $visit_id > 0)
				{
					// update visit charge
					$array_update['charged'] = 1;
					$array_update['visit_invoice_id'] = $visit_invoice_id;
					$array_update['patient_id'] = $patient_id;


					$this->db->where('visit_charge_delete = 0 AND visit_id = '.$visit_id);
					$this->db->update('visit_charge',$array_update);


				}



			}
		}
	}



	public function get_current_visit_invoice_doctors_summary($visit_invoice_id,$type=NULL,$patient_id=NULL)
	{

		$this->db->where('visit_invoice_id',$visit_invoice_id);
		$query = $this->db->get('visit_invoice');

		return $query;

		
	}


	public function get_all_invoice_notes_summary($visit_invoice_id,$type=NULL,$patient_id=NULL)
	{

		$this->db->select('*');
		$this->db->where('visit_invoice_id',$visit_invoice_id);
		$this->db->join('personnel','personnel.personnel_id = visit_invoice_notes.created_by','LEFT');
		$this->db->order_by('visit_invoice_notes.visit_invoice_note_id','DESC');
		$query = $this->db->get('visit_invoice_notes');

		return $query;

		
	}
	

}
?>