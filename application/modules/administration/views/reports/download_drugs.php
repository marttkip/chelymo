<?php
$drug_result = '';
if($query->num_rows() > 0)
{
	$count = 0;
	$drug_result .='
			<table class="table table-hover table-bordered table-striped table-responsive col-md-12" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Visit Date</th>
						<th>Patient No</th>
						<th>Patient Name</th>
						<th>PRN No</th>
						<th>Description</th>
						<th>QTY</th>
						<th>Amount</th>
						<th>Pharm</th>
						<th>Time</th>
					</tr>
				</thead>
				<tbody>
			';
	foreach($query->result() as $visit_drug_result)
	{
		$service_charge = $visit_drug_result->service_charge_name;
		$service_amount = $visit_drug_result->service_charge_amount;
		$patient_surname = $visit_drug_result->patient_surname;
		$patient_othernames = $visit_drug_result->patient_othernames;
		$visit_id = $visit_drug_result->visit_id;
		$time = $visit_drug_result->time;
		$visit_charge_units = $visit_drug_result->visit_charge_units;
		$patient_type = $visit_drug_result->patient_type;
		$visit_charge_amount = $visit_drug_result->visit_charge_amount;
		$patient_number = $visit_drug_result->patient_number;
		$personnel_fname = $visit_drug_result->personnel_fname;
		$product_code = $visit_drug_result->product_code;
		$visit_date = date('jS M Y',strtotime($visit_drug_result->visit_date));

		

		// get drugs sold to a patient 
		$drug_result .='
					<tr>
						<td colspan="10"><strong>'.$patient_number.'<strong></td>
					</tr>';
		// GET ALL GRUGS BY THIS VISIT 

		$query_two = $this->reports_model->get_all_drugs_given($visit_id);

		if($query_two->num_rows() > 0)
		{
			$total_amount = 0;
			foreach ($query_two->result() as $key => $value) {
				# code...
				$service_charge = $value->service_charge_name;
				$service_amount = $value->service_charge_amount;
				$patient_surname = $value->patient_surname;
				$patient_othernames = $value->patient_othernames;
				$time = $value->time;
				$visit_charge_units = $value->visit_charge_units;
				$patient_type = $value->patient_type;
				$visit_charge_amount = $value->visit_charge_amount;
				$patient_number = $value->patient_number;
				$personnel_fname = $value->personnel_fname;
				$product_code = $value->product_code;
				$visit_date = date('jS M Y',strtotime($value->visit_date));
				$count++;
				$drug_result .='
								<tr>
									<td>'.$count.'</td>
									<td>'.$visit_date.'</td>
									<td>'.$patient_number.'</td>
									<td>'.$patient_surname.' '.$patient_othernames.'</td>
									<td>'.$product_code.'</td>
									<td>'.$service_charge.'</td>
									<td>'.$visit_charge_units.'</td>
									<td>'.number_format(($visit_charge_units * $visit_charge_amount),2).'</td>
									<td>'.$personnel_fname.'</td>
									<td>'.$time.'</td>
								</tr>';
				$total_amount += $visit_charge_units * $visit_charge_amount;

			}

			$drug_result .='
					<tr>
						<td colspan="7"><strong>Total<strong></td>
						<td><strong>'.number_format($total_amount,2).'<strong></td>
						<td colspan="2"></td>
					</tr>';
		}
		
		
		
	}
	$drug_result.='
				</tbody>
			</table>';
}
else
{
	$drug_result.= 'No drugs have been dispensed';
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Drugs Dispensed</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
			@media print
			{
				#page-break
				{
					page-break-after: always;
					page-break-inside : avoid;
				}
				.print-no-display
				{
					display: none !important;
				}
			}
        </style>
    </head>
    <body class="receipt_spacing">
    	
       <div class="row receipt_bottom_border">
            <div class="col-md-12">
               	<div class="row">
		        	<div class="col-xs-12">
		            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
		            </div>
		        </div>
		    	<div class="row">
		        	<div class="col-md-12 center-align receipt_bottom_border">
		            	<strong>
		                	<?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
		                </strong>
		            </div>
		        </div>
		        <div class="row receipt_bottom_border" style="padding-top: 5px;padding-bottom: 5px;">
		        	<div class="col-md-12 center-align">
		            	<h5 class="panel-title"><?php echo $title;?></h5>
		            </div>							
		        </div>
		        
                <!-- <div class="row"> -->
                	<div class="col-md-12">
                		<?php echo $drug_result;?>
                	</div>
                <!-- </div> -->
					
            </div>
        </div>

    </body>
</html>
            