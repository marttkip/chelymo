<?php

class Moh_reports_model extends CI_Model
{
	public function get_moh_morbidity_report($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('diseases.*');
		$this->db->where($where);
		$this->db->order_by('diseases.diseases_id','ASC');
		$query = $this->db->get('', $per_page, $page);

		return $query;

	}

	public function get_moh_705_report($table, $where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('moh_diseases.*');
		$this->db->where($where);
		$this->db->order_by('moh_diseases.diseases_name','ASC');
		$query = $this->db->get('');

		return $query;

	}


	public function get_moh_705_diagnosis_report($table, $where)
	{
		//retrieve all users
		$sql ="SELECT disease_id, disease_name, DAY(diagnosis_date) AS month_day, COUNT(patient_id) AS num_instances
				FROM visit_diagnosis
				WHERE $where
				GROUP BY disease_id, month_day
				ORDER BY disease_name, month_day";
		$query = $this->db->query($sql);

		return $query;

	}

	public function get_moh_705_diagnosis_report_old($table, $where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('moh_diagnosis_report.disease_name,disease_id,SUM(01) AS one,SUM(02) AS two,SUM(03) AS three,SUM(04) AS four,SUM(05) AS five,SUM(06) AS six,SUM(07) AS seven,SUM(08) AS eight,SUM(09) AS nine,SUM(10) AS ten,SUM(11) AS eleven,SUM(12) AS twelve,SUM(13) AS thirteen,SUM(14) AS fourteen,SUM(15) AS fifteen,SUM(16) AS sixteen,SUM(17) AS seventeen,SUM(18) AS eighteen,SUM(19) AS nineteen,SUM(20) AS twenty,SUM(21) AS twenty_one,SUM(22) AS twenty_two,SUM(23) AS twenty_three,SUM(24) AS twenty_four,SUM(25) AS twenty_five,SUM(26) AS twenty_six,SUM(27) AS twenty_seven,SUM(28) AS twenty_eight,SUM(29) AS twenty_nine,SUM(30) AS thirty,SUM(31) AS thirty_one');
		$this->db->where($where);
		$this->db->order_by('moh_diagnosis_report.disease_name','ASC');
		$this->db->group_by('disease_id');
		$query = $this->db->get('');

		return $query;

	}

	
	public function get_counters_per_disease($diseases_id,$visit_date)
	{
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_id = visit_diagnosis.visit_id AND moh_diseases.diseases_id = visit_diagnosis.disease_id AND visit_diagnosis.disease_id = '.$diseases_id.' AND visit.visit_date = "'.$visit_date.'"';

		// if($visit_date == "2017-03-10")
		// 	{
		// 			var_dump($where); die();
		// 	}
		// var_dump($where); die();
		$visit_report_search = $this->session->userdata('morbidity_705_search');

		if(!empty($visit_report_search))
		{
			// var_dump($visit_report_search); die();
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' ';
		}
		$this->db->from('moh_diseases,visit_diagnosis,visit,patients');
		$this->db->where($where);

		return $this->db->count_all_results();
	}
	public function get_per_disease_total($visit_date, $patient_id)
	{
		$no_of_visit_per_month = 0;
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_id = visit_diagnosis.visit_id AND moh_diseases.diseases_id = visit_diagnosis.disease_id AND visit.visit_date < "'.$visit_date.'" AND patients.patient_id = '.$patient_id;

		$visit_report_search = $this->session->userdata('morbidity_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' ';
		}
		//var_dump($where);die();
		$this->db->select('COUNT(visit.visit_id) as no_of_visit');
		$this->db->where($where);
		$query = $this->db->get('moh_diseases,visit_diagnosis,visit,patients');

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			$no_of_visit_per_month = $result->no_of_visit;
		}
		return $no_of_visit_per_month;
	}


	public function get_per_disease_new_encounters_total($visit_date, $patient_id,$revisit)
	{
		$no_of_visit_per_month = 0;
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_id = visit_diagnosis.visit_id AND moh_diseases.diseases_id = visit_diagnosis.disease_id AND revisit = '.$revisit.' AND visit.visit_date = "'.$visit_date.'" AND patients.patient_id = '.$patient_id;

		$visit_report_search = $this->session->userdata('morbidity_705_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' ';
		}
		//var_dump($where);die();
		$this->db->select('COUNT(visit.visit_id) as no_of_visit');
		$this->db->where($where);
		$query = $this->db->get('moh_diseases,visit_diagnosis,visit,patients');

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			$no_of_visit_per_month = $result->no_of_visit;
		}
		return $no_of_visit_per_month;
	}
	public function get_all_visit_all($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,service.*');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		$this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}



	public function get_all_visit_all_content($table, $where,$per_page, $page, $order_by = 'visit.visit_date', $order = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,lab_test.*');
		$this->db->where($where);
		$this->db->order_by($order_by,$order);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}
	public function get_lab_result_content($table, $where,$per_page=null, $page=null, $order_by = 'visit.visit_date', $order = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,lab_test.*');
		$this->db->where($where);
		$this->db->order_by($order_by,$order);
		$query = $this->db->get('');

		return $query;
	}

		public function get_ultra_result_content($table, $where,$per_page=null, $page=null, $order_by = 'visit.visit_date', $order = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge');
		$this->db->where($where);
		$this->db->order_by($order_by,$order);
		$query = $this->db->get('');

		return $query;
	}
	
	public function calculate_age($patient_date_of_birth)
	{
		$value = $this->dateDiff(date('y-m-d  h:i'), $patient_date_of_birth." 00:00", 'year');

		return $value;
	}
	public function dateDiff($time1, $time2, $interval)
	{
	    // If not numeric then convert texts to unix timestamps
	    if (!is_int($time1)) {
	      $time1 = strtotime($time1);
	    }
	    if (!is_int($time2)) {
	      $time2 = strtotime($time2);
	    }

	    // If time1 is bigger than time2
	    // Then swap time1 and time2
	    if ($time1 > $time2) {
	      $ttime = $time1;
	      $time1 = $time2;
	      $time2 = $ttime;
	    }

	    // Set up intervals and diffs arrays
	    $intervals = array('year','month','day','hour','minute','second');
	    if (!in_array($interval, $intervals)) {
	      return false;
	    }

	    $diff = 0;
	    // Create temp time from time1 and interval
	    $ttime = strtotime("+1 " . $interval, $time1);
	    // Loop until temp time is smaller than time2
	    while ($time2 >= $ttime) {
	      $time1 = $ttime;
	      $diff++;
	      // Create new temp time from time1 and interval
	      $ttime = strtotime("+1 " . $interval, $time1);
	    }

	    return $diff;
  	}

  	public function get_moh_item_list($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('departments.*');
		$this->db->where($where);
		$this->db->order_by('departments.department_id','ASC');
		$query = $this->db->get('', $per_page, $page);

		return $query;

	}

	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL,$group_by=null,$order_by = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		if($group_by != NULL)
		{
			$this->db->group_by($group_by);
		}
		if($order_by != NULL)
		{
			$this->db->order_by($order_by,'DESC');
		}
		return $this->db->count_all_results();
	}

	public function get_all_items($table,$where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();

		return $query;
	}
	public function get_all_visits($date, $end)
	{
		$this->db->select('patient_id, visit_date');
		$this->db->where('visit_delete = 0 AND visit.visit_date >= "'.$date.'" AND visit.visit_date <= "'.$end.'"');
		$query = $this->db->get('visit');

		return $query;
	}


	function get_tests_result_done($lab_test_id,$test_status=null,$visit_id)
	{
		if($visit_id > 0)
		{
			$test_item = ' AND visit_lab_test.visit_id = '.$visit_id;
		}
		else
		{
			$test_item = '';
		}

		$where = 'service_charge.service_charge_id = visit_lab_test.service_charge_id AND service_charge.lab_test_id = \''.$lab_test_id.'\' '.$test_item;
		$this->db->where($where);
		$this->db->from('service_charge, visit_lab_test');
		$this->db->select('test_status AS total_tests');
		$query = $this->db->get();

		$total_tests = 0;

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$total_tests = $row->total_tests;
		}



		return $total_tests;
	}


	public function export_lab_tests_report()
	{
		$this->load->library('excel');
		
		
		$where = 'lab_test.lab_test_id = service_charge.lab_test_id  AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1';
		$table = 'service_charge,visit_charge,visit,patients,lab_test';


		$visit_report_search = $this->session->userdata('laboratory_test_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}



		$visits_query = $this->moh_reports_model->get_lab_result_content($table, $where,null,null,'lab_test.lab_test_name' ,'ASC');
		
		$title = 'Lab Tests Report';

		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient';
			$report[$row_count][3] = 'Patient Number';
			$report[$row_count][4] = 'Age';
			$report[$row_count][5] = 'Gender';
			$report[$row_count][6] = 'Procedure';
			$report[$row_count][7] = 'Result';
			
			//get & display all services
			$personnel_query = $this->personnel_model->get_all_personnel();
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
			      $visit_time = date('H:i a',strtotime($row->visit_time));
			      if($row->visit_time_out != '0000-00-00 00:00:00')
			      {
			        $visit_time_out = date('H:i a',strtotime($row->visit_time_out));
			      }
			      else
			      {
			        $visit_time_out = '-';
			      }

			      $visit_id = $row->visit_id;
			      $patient_id = $row->patient_id;
			      $visit_date_days = $row->visit_date;
			      $patient_number = $row->patient_number;
			      $strath_no = $row->strath_no;
			      $personnel_id = $row->personnel_id;
			      $dependant_id = $row->dependant_id;
			      $strath_no = $row->strath_no;
			      $visit_type_id = $row->visit_type_id;
			      $gender_id = $row->gender_id;
			      $patient_othernames = $row->patient_othernames;
			      $patient_surname = $row->patient_surname;
			      $patient_date_of_birth = $row->patient_date_of_birth;
			      $last_visit = $row->last_visit;
			      $service_charge_name = $row->service_charge_name;
			      $service_charge_name = $row->service_charge_name;
			      $lab_positive_status = $row->lab_positive_status;
			      $lab_test_id = $row->lab_test_id;
			      $names = $patient_surname.' '.$patient_othernames;


			      if($gender_id == 1)
			      {
			        $gender = 'Male';
			      }
			      else
			      {
			        $gender = 'Female';
			      }

			      // this is to check for any credit note or debit notes
			      $age = $this->moh_reports_model->calculate_age($patient_date_of_birth);
			      // end of the debit and credit notes


			      //creators and editors
			      if($personnel_query->num_rows() > 0)
			      {
			        $personnel_result = $personnel_query->result();

			        foreach($personnel_result as $adm)
			        {
			          $personnel_id2 = $adm->personnel_id;

			          if($personnel_id == $personnel_id2)
			          {
			            $doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
			            break;
			          }

			          else
			          {
			            $doctor = '-';
			          }
			        }
			      }

			      else
			      {
			        $doctor = '-';
			      }
			      if($lab_positive_status)
			      {
			        $result_status = $this->moh_reports_model->get_tests_result_done($lab_test_id,NULL,$visit_id);
			        if($result_status == 1)
			        {
			          $result_status = 'NEGATIVE';
			        }
			        else if($result_status == 2)
			        {
			          $result_status = 'POSITIVE';
			        }
			        else {
			          $result_status = 'NOT CAPTURED';
			        }
			      }
			      else
			      {

			        $result_status = '-';
			      }

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_number;
				$report[$row_count][3] = $names;
				$report[$row_count][4] = $patient_number;
				$report[$row_count][5] = $age;
				$report[$row_count][6] = $service_charge_name;
				$report[$row_count][7] = $result_status;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	
	}
}

?>
