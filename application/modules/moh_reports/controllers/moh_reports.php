<?php   
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);

class Moh_reports extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('moh_reports/moh_reports_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/database');
		$this->load->model('administration/personnel_model');
	}

	public function morbidity_report()
	{
		$where = 'diseases.diseases_id > 0 AND diseases.diseases_name != "Suspected Malaria"';
		$table = 'diseases';

		$morbidity_diseases_search = $this->session->userdata('morbidity_diseases_search');


		if(!empty($morbidity_diseases_search))
		{
			$where .= $morbidity_diseases_search;
		}
		else
		{
			$where .= ' ';
		}
		
		

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/morbidity-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 1000;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		
		$query = $this->moh_reports_model->get_moh_morbidity_report($table, $where, $config["per_page"], $page, 'ASC');

		$page_title = 'IC10 MOH Reports';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;




		$data['content'] = $this->load->view('mobidity_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function moh_705_report()
	{
		$where = 'visit_diagnosis.disease_id > 0';
		$table = 'visit_diagnosis';

		$moh_705_diseases_search = $this->session->userdata('morbidity_705_search');


		if(!empty($moh_705_diseases_search))
		{
			$where .= $moh_705_diseases_search;
		}
		else
		{
			$month = (int)date('m');
			
			$where .= ' AND MONTH(diagnosis_date) = '.$month.' AND YEAR(diagnosis_date) = '.date('Y').'';
			$visit_search_title = 'REPORT FOR '.date('M').' '.date('Y').' ';

			$this->session->set_userdata('moh_year', date('Y'));
			$this->session->set_userdata('moh_month', $month);
			$this->session->set_userdata('mobidity_type',0);


			$this->session->set_userdata('moh_705_title_search', $visit_search_title);
		}

		
		$query = $this->moh_reports_model->get_moh_705_diagnosis_report($table, $where);


		$page_title = 'MOH 705 Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;


		$data['content'] = $this->load->view('moh_diagnosis', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function print_morbidity_report()
	{
		$where = 'diseases.diseases_id > 0';
		$table = 'diseases';

		$morbidity_diseases_search = $this->session->userdata('morbidity_diseases_search');

		if(!empty($morbidity_diseases_search))
		{
			$where .= $morbidity_diseases_search;
		}
		else
		{
			$where .= ' ';
		}
		$this->db->where($where);
		$this->db->order_by('diseases.diseases_id','ASC');
		$query = $this->db->get($table);

		$page_title = 'Morbidity Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$this->load->view('mobidity_report_print', $v_data);

	}

	public function print_moh_705_report()
	{
		$where = 'visit_diagnosis.disease_id > 0';
		$table = 'visit_diagnosis';

		$moh_705_diseases_search = $this->session->userdata('moh_705_diseases_search');

		if(!empty($moh_705_diseases_search))
		{
			$where .= $moh_705_diseases_search;
		}
		else
		{	
			$visit_search_title = 'REPORT FOR "'.date('M').'" "'.date('Y').'" ';
			$where .= ' AND MONTH(diagnosis_date) = "'.date('m').'" AND YEAR(diagnosis_date) = "'.date('Y').'"';
			$this->session->set_userdata('moh_705_title_search', $visit_search_title);
		}
		
		$query = $this->moh_reports_model->get_moh_705_diagnosis_report($table, $where);


		$page_title = 'MOH 705 Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$this->load->view('moh_705_report_print', $v_data);

	}
	public function search_morbidity()
	{
		$morbidity_type = $this->input->post('morbidity_type');
		$morbidity_year = $year1 = $moh_year = $this->input->post('morbidity_year');
		$morbidity_month = $moh_month = $this->input->post('morbidity_month');
		$morbidity_diseases_id  = $this->input->post('morbidity_diseases_id');
		$visit_search_title ='';

		if(!empty($morbidity_year))
		{
			$searched_morbidity_year = ' AND YEAR(visit.visit_date) = \''.$morbidity_year.'\'';
			// $visit_search_title = ' From '.$morbidity_year.' ';
		}
		else
		{
			$searched_morbidity_year = '';

		}

		if(!empty($morbidity_month))
		{
			$searched_morbidity_month = ' AND MONTH(visit.visit_date) = \''.$morbidity_month.'\'';
			// $visit_search_title = 'Sick Off To '.$visit_date_to.'';
		}
		else
		{
			$searched_morbidity_month = '';

		}



		if(!empty($morbidity_diseases_id))
		{
			$morbidity_diseases_id = ' AND diseases_id = '.$morbidity_diseases_id;
		}
		else
		{
			$morbidity_diseases_id = '';

		}

		if(!empty($mobidity_type) && !empty($moh_year))
		{
			if($mobidity_type == 1)
			{
				$age = $year1 - 5;
				$add = ' AND YEAR(patients.patient_date_of_birth) <= '.$age;
				$visit_search_title = ' Over 5 Years ';
			}
			else
			{
				$age = $year1 - 5;
				$add = ' AND YEAR(patients.patient_date_of_birth) >= '.$age.' AND YEAR(patients.patient_date_of_birth) <= '.date('Y');
				$visit_search_title = ' Below 5 Years ';
			}

			$mobidity_type = $add;

		}

		else
		{
			$mobidity_type = '';
		}

		$search = $searched_morbidity_year.$searched_morbidity_month.$mobidity_type;
		$morbidity_diseases_search = $morbidity_diseases_id;

		$this->session->set_userdata('morbidity_report_search', $search);
		$this->session->set_userdata('moh_year', $moh_year);
		$this->session->set_userdata('moh_month', $moh_month);
		$this->session->set_userdata('morbidity_diseases_search', $morbidity_diseases_search);
		$this->session->set_userdata('morbidity_title_search', $visit_search_title);

		redirect('medical-reports/morbidity-report');
	}

	public function search_moh_705()
	{
		$mobidity_type = $this->input->post('mobidity_type');
		$year = $year1 = $moh_year = $this->input->post('year');
		$month = $moh_month = $this->input->post('month');
		$diseases_id  = $this->input->post('diseases_id');
		$visit_search_title ='';

		if(!empty($year))
		{
			$year = ' AND YEAR(diagnosis_date) = '.$year.'';
			$visit_search_title = ' From '.$year.' ';
		}
		else
		{
			$year = '';

		}

		if(!empty($month))
		{
			$month = (int)$month;
			$month = ' AND MONTH(diagnosis_date) = '.$month.'';
			$visit_search_title = 'MOH 705 To '.$visit_date_to.'';
		}
		else
		{
			$month = '';

		}



		if(!empty($diseases_id))
		{
			$diseases_id = ' AND disease_id = '.$diseases_id;
		}
		else
		{
			$diseases_id = '';

		}

		if(!empty($mobidity_type) && !empty($moh_year))
		{
			if($mobidity_type == 1)
			{
				$age = $year1 - 5;
				// $add = ' AND YEAR(patients.patient_date_of_birth) <= '.$age;
				$add = ' AND age_bracket = 2';
				$visit_search_title = ' Over 5 Years ';
			}
			else
			{
				$age = $year1 - 5;
				// $add = ' AND YEAR(patients.patient_date_of_birth) >= '.$age.' AND YEAR(patients.patient_date_of_birth) <= '.date('Y');
				$add = ' AND age_bracket < 2';
				$visit_search_title = ' Below 5 Years ';
			}

			$mobidity_type = $add;

		}

		else
		{
			$mobidity_type = '';
		}

		$search = $year.$month.$mobidity_type;

		// var_dump($search);die();
		$moh_705_diseases_search = $diseases_id;

		$this->session->set_userdata('morbidity_705_search', $search);
		$this->session->set_userdata('moh_year', $moh_year);
		$this->session->set_userdata('moh_month', $moh_month);
		$this->session->set_userdata('mobidity_type', $mobidity_type);
		$this->session->set_userdata('moh_705_diseases_search', $moh_705_diseases_search);
		$this->session->set_userdata('moh_705_title_search', $visit_search_title);

		redirect('moh-report/moh-705');
	}

	public function close_search_morbidity()
	{
		$this->session->unset_userdata('morbidity_report_search');
		$this->session->unset_userdata('moh_year');
		$this->session->unset_userdata('moh_month');
		$this->session->unset_userdata('morbidity_title_search');
		$this->session->unset_userdata('morbidity_diseases_search');

		redirect('medical-reports/morbidity-report');
	}

	public function close_search_moh_705()
	{
		$this->session->unset_userdata('morbidity_705_search');
		$this->session->unset_userdata('moh_year');
		$this->session->unset_userdata('moh_month');
		$this->session->unset_userdata('moh_705_title_search');
		$this->session->unset_userdata('moh_705_diseases_search');

		redirect('moh-report/moh-705');
	}

	public function procedures_report()
	{
		$where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Procedures" AND service.service_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0';
		$table = 'service_charge,service,visit_charge,visit,patients';


		$visit_report_search = $this->session->userdata('procedures_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/procedures-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->moh_reports_model->get_all_visit_all($table, $where, $config["per_page"], $page, 'ASC');


		$page_title = 'Procedures Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$data['content'] = $this->load->view('procedures_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	
	public function print_procedures_report()
	{

		$where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Procedures" AND service.service_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0';
		$table = 'service_charge,service,visit_charge,visit,patients';


		$visit_report_search = $this->session->userdata('procedures_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}



		$query = $this->moh_reports_model->get_all_visit_all_content($table, $where,'sick_off.from_date' ,'ASC');


		$page_title = 'Procedures Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('procedures_report_print', $v_data);

	}



	public function tests_report()
	{
		$where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Laboratory" AND service.service_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0';
		$table = 'service_charge,service,visit_charge,visit,patients';


		$visit_report_search = $this->session->userdata('laboratory_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/tests-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->moh_reports_model->get_all_visit_all($table, $where, $config["per_page"], $page, 'ASC');


		$page_title = 'Laboratory Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$data['content'] = $this->load->view('tests_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function ultra_report()
	{
		$where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Ultrasound" AND service.service_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0';
		$table = 'service_charge,service,visit_charge,visit,patients';


		$visit_report_search = $this->session->userdata('ultrasound_test_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/ultra-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 100;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->moh_reports_model->get_all_visit_all($table, $where, $config["per_page"], $page, 'ASC');


		$page_title = 'Radiology';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$data['content'] = $this->load->view('ultra_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function print_tests_report()
	{

		$where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Laboratory" AND service.service_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0';
		$table = 'service_charge,service,visit_charge,visit,patients';


		$visit_report_search = $this->session->userdata('laboratory_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}



		$query = $this->moh_reports_model->get_all_visit_all_content($table, $where,'sick_off.from_date' ,'ASC');


		$page_title = 'Tests Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('tests_report_print', $v_data);

	}




	public function workload_report()
	{

		$where = 'report_item = 1';
		$table = 'departments';

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/work-load-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->moh_reports_model->get_moh_item_list($table, $where, $config["per_page"], $page, 'ASC');

		$page_title = 'Work Load Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$data['content'] = $this->load->view('workload_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_workload()
	{
		$year = $this->input->post('year');
		$month = $this->input->post('month');
		$visit_search_title ='';


		if(!empty($year))
		{
			$search_year = $year;
			$year = ' AND YEAR(visit.visit_date) = \''.$year.'\'';

			$visit_search_title = ' From '.$year.' ';
		}
		else
		{
			$search_year = '';
			$year = '';

		}

		if(!empty($month))
		{
			$search_month = $month;
			$month = ' AND MONTH(visit.visit_date) = \''.$month.'\'';
			$visit_search_title = 'Sick Off To '.$visit_date_to.'';
		}
		else
		{
			$month = '';

		}


		$search = $year.$month;

		$this->session->set_userdata('workload_report_search', $search);
		$this->session->set_userdata('workload_year_search', $search_year);
		$this->session->set_userdata('workload_month_search', $search_month);
		$this->session->set_userdata('workload_title_search', $visit_search_title);

		redirect('medical-reports/work-load-report');
	}

	public function close_search_workload()
	{
		$this->session->unset_userdata('workload_report_search');
		$this->session->unset_userdata('workload_year_search');
		$this->session->unset_userdata('workload_month_search');
		$this->session->unset_userdata('workload_title_search');

		redirect('medical-reports/work-load-report');
	}

	public function search_procedures_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$search_title = "";

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit dates between '.$visit_date_from.' AND '.$visit_date_to.' ';
		}

		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';

			$search_title = 'Procedures search for visit date '.$visit_date_from.' ';
		}

		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit date to '.$visit_date_to.' ';
		}

		else
		{
			$visit_date = '';
		}

		$search = $visit_date;

		$this->session->set_userdata('procedures_report_search', $search);
		$this->session->set_userdata('procedures_title_search', $search_title);

		redirect('medical-reports/procedures-report');
	}

	public function close_search_procedures()
	{
		$this->session->unset_userdata('procedures_report_search');
		$this->session->unset_userdata('procedures_title_search');

		redirect('medical-reports/procedures-report');
	}

	public function search_tests_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$lab_test_age_type = $this->input->post('lab_test_age_type');
		$visit_year = $year1 = $visit_year = $this->input->post('visit_year');
		$surname = $this->input->post('surname');
		$patient_number = $this->input->post('patient_number');
		$search_title = "";

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit dates between '.$visit_date_from.' AND '.$visit_date_to.' ';
		}

		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';

			$search_title = 'Procedures search for visit date '.$visit_date_from.' ';
		}

		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit date to '.$visit_date_to.' ';
		}

		else
		{
			$visit_date = '';
		}


		if(!empty($patient_number))
		{
			$search_title .= ' patient number <strong>'.$patient_number.'</strong>';
			$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\'';
		}


		$year1 = date(Y);

		// var_dump($lab_test_age_type); die();
		
		if(!empty($lab_test_age_type))
		{

			if($lab_test_age_type == 1)
			{
				$age = $year1 - 5;
				$add = ' AND YEAR(patients.patient_date_of_birth) <= '.$age;
				$search_title = ' Over 5 Years ';
			}
			else
			{
				$age = $year1 - 5;
				$add = ' AND YEAR(patients.patient_date_of_birth) >= '.$age.' AND YEAR(patients.patient_date_of_birth) <= '.date('Y');
				$search_title = ' Below 5 Years ';
			}

			$lab_test_age_type_searched = $add;

		}

		else
		{
			$lab_test_age_type_searched = '';
		}

		

		//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);

			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\')';
				}

				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}

		else
		{
			$surname = '';
		}

		$search = $visit_date.$patient_number.$surname.$lab_test_age_type_searched;

		$this->session->set_userdata('laboratory_report_search', $search);
		$this->session->set_userdata('laboratory_title_search', $search_title);

		redirect('medical-reports/tests-report');
	}

	public function close_search_tests()
	{
		$this->session->unset_userdata('laboratory_report_search');
		$this->session->unset_userdata('laboratory_title_search');

		redirect('medical-reports/tests-report');
	}




	public function lab_tests_report()
	{
		$where = 'lab_test.lab_test_id = service_charge.lab_test_id  AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1';
		$table = 'service_charge,visit_charge,visit,patients,lab_test';


		$visit_report_search = $this->session->userdata('laboratory_test_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'medical-reports/lab-tests-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->moh_reports_model->get_all_visit_all_content($table, $where, $config["per_page"], $page,'service_charge.service_charge_id', 'ASC');


		$page_title = 'Laboratory Test Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;


		$data['content'] = $this->load->view('lab_tests_report', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}




	public function search_lab_tests_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$surname = $this->input->post('surname');
		$lab_test_name = $this->input->post('lab_test_name');
		$gender_id = $this->input->post('gender_id');
		$age_group = $this->input->post('age_group');
		$search_title = "";

		// var_dump($age_group);die();

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit dates between '.$visit_date_from.' AND '.$visit_date_to.' ';
		}

		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';

			$search_title = 'Procedures search for visit date '.$visit_date_from.' ';
		}

		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit date to '.$visit_date_to.' ';
		}

		else
		{
			$visit_date = '';
		}


		if(!empty($lab_test_name))
		{
			$search_title .= ' Lab Test Name <strong>'.$lab_test_name.'</strong>';
			$lab_test_name = ' AND lab_test.lab_test_name LIKE \'%'.$lab_test_name.'%\'';
		}

		if(!empty($gender_id))
		{
			if($gender_id == 1)
			{
				$gender = 'Male';
			}
			else if($gender_id == 2)
			{
				$gender = 'Female';
			}
			$search_title .= ' Gender <strong>'.$gender.'</strong>';
			$gender_id = ' AND patients.gender_id = '.$gender_id.'';
		}
		$year1 = date('Y');
		if($age_group == 1)
		{
			$age = $year1 - 5;
			$add = ' AND YEAR(patients.patient_date_of_birth) <= '.$age;
			$search_title = ' Over 5 Years ';
		}
		else
		{
			$age = $year1 - 5;
			$add = ' AND YEAR(patients.patient_date_of_birth) >= '.$age.' AND YEAR(patients.patient_date_of_birth) <= '.date('Y');
			$search_title = ' Below 5 Years ';
		}

		//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);

			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\')';
				}

				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}

		else
		{
			$surname = '';
		}

		$search = $visit_date.$lab_test_name.$surname.$add.$gender_id;

		$this->session->set_userdata('laboratory_test_report_search', $search);
		$this->session->set_userdata('laboratory_title_search', $search_title);

		redirect('medical-reports/lab-tests-report');
	}

	public function close_search_lab_tests()
	{
		$this->session->unset_userdata('laboratory_test_report_search');
		$this->session->unset_userdata('laboratory_title_search');

		redirect('medical-reports/lab-tests-report');
	}


	public function print_lab_tests_report()
	{

		$where = 'lab_test.lab_test_id = service_charge.lab_test_id  AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1';
		$table = 'service_charge,visit_charge,visit,patients,lab_test';


		$visit_report_search = $this->session->userdata('laboratory_test_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->moh_reports_model->get_lab_result_content($table, $where,null,null,'lab_test.lab_test_name' ,'ASC');


		$page_title = 'Tests Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('lab_tests_report_print', $v_data);

	}

	public function print_lab_ultra_report()
	{

	    $where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Ultrasound" AND service.service_delete = 0 AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1';
		$table = 'service_charge,service,visit_charge,visit,patients';


		$visit_report_search = $this->session->userdata('ultrasound_test_report_search');

		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->moh_reports_model->get_all_visit_all($table, $where, $config["per_page"], $page, 'ASC');


		$page_title = 'Ultrasound Report';
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('print_ultra', $v_data);

	}



	public function export_lab_tests_report()
	{
		$this->moh_reports_model->export_lab_tests_report();
	}


	public function search_ultrasound_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$surname = $this->input->post('surname');
		$lab_test_name = $this->input->post('lab_test_name');
		$gender_id = $this->input->post('gender_id');
		$age_group = $this->input->post('age_group');
		$search_title = "";

		// var_dump($age_group);die();

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit dates between '.$visit_date_from.' AND '.$visit_date_to.' ';
		}

		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';

			$search_title = 'Procedures search for visit date '.$visit_date_from.' ';
		}

		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$search_title = 'Procedures search for visit date to '.$visit_date_to.' ';
		}

		else
		{
			$visit_date = '';
		}


		if(!empty($lab_test_name))
		{
			$search_title .= ' Lab Test Name <strong>'.$lab_test_name.'</strong>';
			$lab_test_name = ' AND lab_test.lab_test_name LIKE \'%'.$lab_test_name.'%\'';
		}

		if(!empty($gender_id))
		{
			if($gender_id == 1)
			{
				$gender = 'Male';
			}
			else if($gender_id == 2)
			{
				$gender = 'Female';
			}
			$search_title .= ' Gender <strong>'.$gender.'</strong>';
			$gender_id = ' AND patients.gender_id = '.$gender_id.'';
		}
		// $year1 = date('Y');
		// if($age_group == 1)
		// {
		// 	$age = $year1 - 5;
		// 	$add = ' AND YEAR(patients.patient_date_of_birth) <= '.$age;
		// 	$search_title = ' Over 5 Years ';
		// }
		// else
		// {
		// 	$age = $year1 - 5;
		// 	$add = ' AND YEAR(patients.patient_date_of_birth) >= '.$age.' AND YEAR(patients.patient_date_of_birth) <= '.date('Y');
		// 	$search_title = ' Below 5 Years ';
		// }

		//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' first name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);

			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\')';
				}

				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}

		else
		{
			$surname = '';
		}

		$search = $visit_date.$lab_test_name.$surname.$add.$gender_id;

		$this->session->set_userdata('ultrasound_test_report_search', $search);
		$this->session->set_userdata('ultrasound_title_search', $search_title);

		redirect('medical-reports/ultra-report');
	}

	public function close_search_ultrasound()
	{
		$this->session->unset_userdata('ultrasound_test_report_search');
		$this->session->unset_userdata('ultrasound_title_search');

		redirect('medical-reports/ultra-report');
	}

}
?>
