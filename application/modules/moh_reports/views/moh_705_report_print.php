<?php
		
$result = '';
$year_search = $this->session->userdata('moh_year');
$month_search = $this->session->userdata('moh_month');
if(!empty($year_search) AND !empty($month_search))
{
	$date = $year_search.'-'.$month_search.'-01';
	$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
}
else if(!empty($year_search) AND empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date = $year_search.'-'.date('m-').'01';
	$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
else if(empty($year_search) AND !empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date = date('Y-').$month_search.'-01';
	$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
}
else
{
	$date = date('Y-m-').'01';
	$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}

$search = $this->session->userdata('morbidity_title_search');
if(!empty($search))
{
	$search = $search;
}
else
{
	$search = '';
}
$title_content = 'Showing diagnosis for '.date('M Y', strtotime($date)).' '.$search;

$items_list = '';
while(strtotime($date) <= strtotime($end)) {
	$day_num = date('d', strtotime($date));
	$day_name = date('l', strtotime($date));
	$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
	$items_list .= "<th>$day_num</th>";
}
  
 

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed" id ="testTable">
		<thead>
			<tr>
				<th>#</th>
				<th>Disease Name</th>
				'.$items_list.'
				<th>Total</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	//get all administrators
	$personnel_query = $this->personnel_model->get_all_personnel();
	
	
	foreach ($query->result() as $row)
	{
		$diseases_name = $row->diseases_name;
		$diseases_id = $row->diseases_id;

		$items_lists = '';
		//  get the quantities of diseases detected
		$year_search = $this->session->userdata('moh_year');
		$month_search = $this->session->userdata('moh_month');
		if(!empty($year_search) AND !empty($month_search))
		{
			$date = $year_search.'-'.$month_search.'-01';
			$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
		}
		else if(!empty($year_search) AND empty($month_search))
		{
			// $date = $year_search.'-01-01';
			// $end = $year_search.'-12-' . date('t', strtotime($date));
			$date = $year_search.'-'.date('m-').'01';
			$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
		}
		else if(empty($year_search) AND !empty($month_search))
		{
			// $date = $year_search.'-01-01';
			// $end = $year_search.'-12-' . date('t', strtotime($date));
			$date = date('Y-').$month_search.'-01';
			$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
		}
		else
		{
			$date = date('Y-m-').'01';
			$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
		}

		// var_dump($end); die();
		$total_row = $total_new_counters = $new_visit_total = $old_visit_total = 0;
		$counters = 0;
		//no.of first time visits(65) reattendances (66)
		if(($diseases_id == 65) || ($diseases_id == 66))
		{
			$all_visits = $this->moh_reports_model->get_all_visits($date, $end);
			
			while(strtotime($date) <= strtotime($end)) 
			{
				$new_counters = $old_counters = 0;
				if($all_visits->num_rows() > 0)
				{
					foreach($all_visits->result() as $visit)
					{
						$patient_id = $visit->patient_id;
						$visit_date = $visit->visit_date;
						
						if($visit_date == $date)
						{
							$no_of_visits = $this->moh_reports_model->get_per_disease_new_encounters_total($date, $patient_id,0);
							$no_of_revisits = $this->moh_reports_model->get_per_disease_new_encounters_total($date, $patient_id,1);
							$total_row += $no_of_visits;
							if($no_of_visits > 0)
							{
								if($diseases_id == 65)
								{
									$new_counters = $new_counters + 1;
									
								}
							}
							elseif($no_of_revisits > 0)
							{
								if($diseases_id == 66)
								{
									if($date == '2017-05-09')
									{
									echo $patient_id.' ';
									}
									$old_counters = $old_counters + 1;
								}
							}
						}
					}
				}
				if($diseases_id == 65)
				{
					$new_visit_total += $new_counters;
					$items_lists .= "<td>".$new_counters."</td>";
					$total_row = $new_visit_total;
				}
				else if($diseases_id == 66)
				{
					$old_visit_total += $old_counters;
					$items_lists .= "<td>".$old_counters."</td>";
					$total_row = $old_visit_total;
				}
				$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
			}
		}
		else
		{
			while(strtotime($date) <= strtotime($end)) 
			{
				$day_num = date('d', strtotime($date));
				$day_name = date('l', strtotime($date));
					
				$counters = $this->moh_reports_model->get_counters_per_disease($diseases_id,$date);
				$total_row += $counters;
				// if($date == "2017-03-10")
				// {
				// 		var_dump($counters); die();
				// }
				$items_lists .= "<td>".$counters."</td>";
	
				$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
			}
		}
		// end of getting the diseases detected

		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$diseases_name.'</td>
				'.$items_lists.'
				<td>'.$total_row.'</td>
			</tr> 
		';
	}
	
	$result .= 
	'
	  </tbody>
	</table>
	';
}

else
{
	$result .= "There are no diagnosis";
}
?>

<?php
$title_ext = $title_content.' '.$this->session->userdata('moh_705_title_search');

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $title;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
			@media print
			{
				#page-break
				{
					page-break-after: always;
					page-break-inside : avoid;
				}
				.print-no-display
				{
					display: none !important;
				}
			}
        </style>
    </head>
    <body class="receipt_spacing" >        
        <!-- Widget content -->
        <div id="excel-export">
        	<table class="table table-condensed">
                <tr>
                    <th><?php echo $title_ext;?></th>
                </tr>
            </table>
        	<?php echo $result;?>
        </div>
		 <a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
     </body>
 </html>

 <script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>

