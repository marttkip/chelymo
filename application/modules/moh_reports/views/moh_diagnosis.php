<?php echo $this->load->view('search/moh_705_search.php', '', TRUE);?>
<?php
		
$result = '';
$year_search = $this->session->userdata('moh_year');
$month_search = $this->session->userdata('moh_month');
if(!empty($year_search) AND !empty($month_search))
{
	$date = $start_date = $year_search.'-'.$month_search.'-01';
	$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
}
else if(!empty($year_search) AND empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date  = $start_date = $year_search.'-'.date('m-').'01';
	$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
else if(empty($year_search) AND !empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date  = $start_date = date('Y-').$month_search.'-01';
	$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
}
else
{
	$date  = $start_date = date('Y-m-').'01';
	$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
$search = $this->session->userdata('moh_705_title_search');
if(!empty($search))
{
	$search = $search;
}
else
{
	$search = '';
}
$title_content = 'Showing diagnosis for '.date('M Y', strtotime($date)).' '.$search;

$items_list = '';
while(strtotime($date) <= strtotime($end)) {
	$day_num = date('d', strtotime($date));
	$day_name = date('l', strtotime($date));
	
	$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
	$items_list .= "<th>$day_num</th>";
}
  
 

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed" >
		<thead>
			<tr>
				<th>#</th>
				<th>Disease Name</th>
				'.$items_list.'
				<th>Total</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	//get all administrators
	$personnel_query = $this->personnel_model->get_all_personnel();

	$date = $start_date;
	$year = date('Y', strtotime($date));
	$month = intval(date('m', strtotime($date)));
	$max_day = "$year-".($month < 10 ? "0":"") . $month . "-01";
	$max_day = new DateTime($max_day);
	$max_day = $max_day->format("t");
	$curr_disease = 0;
	$curr_day = 0;
	$str_disease = "";
	$disease_total = 0;
	$arrDays = array();

	foreach ($query->result() as $row)
	{
		$disease_name = $row->disease_name;
		$disease_id = $row->disease_id;
		
		
		if($disease_id != $curr_disease){
			if($curr_disease > 0){
				while($curr_day <= $max_day){
					
					$str_disease .= "<td style='color:lightgray;text-align:center'>0</td>";
					if(!array_key_exists($curr_day, $arrDays))
						$arrDays[$curr_day] =0;

					$curr_day++;
				}
				$str_disease .= "<td style='text-align:center'>".$disease_total."</td>";
				$str_disease .= "</tr>";
			}
			$count++;
			$str_disease .= "<tr>
				<td>".$count."</td>
				<td>".$disease_name."</td>";
			$curr_day = 1;
			$curr_disease = $disease_id;
			$disease_total = 0;
			

		}

		if(!array_key_exists($curr_day, $arrDays))
			$arrDays[$curr_day] =0;
		$month_day = $row->month_day;
		while($curr_day < $month_day){
			$str_disease .= "<td style='color:lightgray;text-align:center'>0</td>";
			$curr_day++;
			if(!array_key_exists($curr_day, $arrDays))
				$arrDays[$curr_day] =0;
		}

		$num_instances = $row->num_instances;
		$disease_total += $num_instances;

		$arrDays[$curr_day] += $num_instances;

		$str_disease .= "<td style='text-align:center'>".$num_instances."</td>";
		$curr_day++;
	
		// end of getting the diseases detected
		
		
	}

	if($curr_disease > 0){
		while($curr_day <= $max_day){
			$curr_day++;
			$str_disease .= "<td style='color:lightgray;text-align:center'>0</td>";
		}
		$str_disease .= "<td style='text-align:center'>".$disease_total."</td>";
		$str_disease .= "</tr>";
	}
	
	//Add the days' totals

	$str_disease .= "<tr style='font-weight:bold'><td colspan=2>Totals</td>";
	$grand = 0;
	foreach($arrDays as $day_total){
		$str_disease .= "<td style='text-align:center; ".($day_total == 0 ? "color:lightgray" :"")."'>$day_total</td>";
		$grand += $day_total;
	}
	$str_disease .= "<td style='text-align:center; ".($grand == 0 ? "color:lightgray" :"")."'>$grand</td></tr>";
	unset($arrDays);

	$str_disease .= 
	'
	  </tbody>
	</table>
	';

	$result .= $str_disease;
}

else
{
	// $result .= "There are no types";
}
?>

<?php
	$title_ext = $title_content;




?>

<section class="panel">
	 <header class="panel-heading">
    	 <h2 class="panel-title"><?php echo $title;?></h2>
    	 <a href="<?php echo site_url();?>print-moh-705-report" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print MOH 705 Report</a>
    </header>   
	<div class="panel-body">
    	
        <?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			$this->session->unset_userdata('error_message');
		}

		$search = $this->session->userdata('moh_705_diseases_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'moh_reports/close_search_moh_705" class="btn btn-sm btn-warning">Close Search</a>';
		}
		?>
		<div class="center-align"><?php echo $title_ext;?></div>
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    
    <div class="panel-foot">
        
		<?php if(isset($links)){echo $links;}?>
    
        <div class="clearfix"></div> 
    
    </div>
</section>