       <?php
            $reach = '';
            $where = 'moh_diseases.diseases_id > 0 AND moh_diseases.diseases_name != "Suspected Malaria"';
            $table = 'moh_diseases';
            $this->db->where($where);
            $diseases_list = $this->db->get($table);

            foreach ($diseases_list->result() as $row)
            {
                $diseases_name = $row->diseases_name;
                $diseases_id = $row->diseases_id;
                $diseases_code = $row->diseases_code;
                $reach .='<option value="'.$diseases_id.'">'.$diseases_name.' '.$diseases_code.' </option>';
            }

            $year = 2018;
            $moh_year =  $this->session->userdata('moh_year');
            $moh_month = $this->session->userdata('moh_month');
            $moh_mobidity = $this->session->userdata('mobidity_type');
            $option = '';
            for ($i=$year; $i <= date('Y') ; $i++) { 
                // code...
                if($i == $moh_year)
                {
                      $option .= '<option value="'.$i.'" selected>'.$i.'</option>';
                }
                else
                {
                      $option .= '<option value="'.$i.'">'.$i.'</option>';
                }
              
            }

            $month_options = '';
            for ($z=1; $z <= 12 ; $z++) { 
                // code...

                $monthNum  = $z;
                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                $monthName = $dateObj->format('F'); // March

                if($z == $moh_month)
                {
                      $month_options .= '<option value="'.$z.'" selected>'.$monthName.'</option>';
                }
                else
                {
                      $month_options .= '<option value="'.$z.'">'.$monthName.'</option>';
                }
              
            }

            $types = '';
            if($moh_mobidity == 1){
                $types .= '<option value="1" selected>705A</option>';
            }
            else if($moh_mobidity == 2){
                $types .= '<option value="2">705B</option>';
            }
            else
            {
                 $types .= '<option value="0">---ALL TYPE---</option>
                            <option value="1"> > 705A</option>
                            <option value="2"> < 705B </option>';
            }


       ?>
       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-moh-705", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	<div class="col-md-2">

            		<div class="form-group">
                        <label class="col-md-4 control-label">Range: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="mobidity_type">
                            	<?php echo $types;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Year: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                        		<select class="form-control" name="year" required>
                        			<?php echo $option?>
                        		</select>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Month: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <select class="form-control" name="month" required>
                                	<option value="">---Select Month---</option>
                                    <?php echo $month_options;?>
                        			
                        		</select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        
                        <div class="col-md-12">
                            <select id='diseases_id' name='diseases_id' class='form-control custom-select ' >
                                <option value="">---Select ---</option>
                                <?php echo $reach?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search Report</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>
<script text="javascript">
$(function() {
    $("#diseases_id").customselect();
});
$(document).ready(function(){

    $(function() {
        $("#diseases_id").customselect();
    });
});
</script>