<?php echo $this->load->view('search/ultra_search', '', TRUE);?>
<?php
$search_title = $this->session->userdata('ultrasound_title_search');
if(!empty($search_title))
{
	$title_ext = $search_title;
}
else
{
	$title_ext = 'Ultrasound Report for '.date('Y-m-d');
}

?>
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            	 <a href="<?php echo site_url();?>moh-reports/print-ultra-report" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print List</a>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $title_ext;?></h5>
          <br>
<?php
		$result = '';
		$search = $this->session->userdata('laboratory_report_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'moh_reports/close_search_ultrasound" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Visit Date</th>
						  <th>Patient</th>
						  <th>Patient Number</th>
						  <th>Patient Gender </th>
						  <th>Procedure Name</th>
						  </tr>
					  </thead>
					  <tbody>
						  
				';
				
		
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$visit_date_days = $row->visit_date;
				$patient_number = $row->patient_number;
				$strath_no = $row->strath_no;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$gender_id = $row->gender_id;
				$patient_othernames = $row->patient_othernames;
				$service_charge_name = $row->service_charge_name;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$last_visit = $row->last_visit;
				$service_charge_name = $row->service_charge_name;
				$names = $patient_surname.' '.$patient_othernames;
				

				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}

				// this is to check for any credit note or debit notes
				$age = $this->moh_reports_model->calculate_age($patient_date_of_birth);
				// end of the debit and credit notes


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}
				
				$count++;
				
				
					$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$visit_date.'</td>
								<td>'.$names.'</td>
								<td>'.$patient_number.'</td>
								<td>'.$gender.'</td>
								<td>'.$service_charge_name.'</td>
									<td><a href="'.site_url().'radiology/xray/print_result_u/'.$patient_id.'/'.$visit_id.'/'.$visit_charge_id.'" target="_blank" class="btn btn-xs btn-success">Print Ultra Results</a></td>

						';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visit procedures done";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>