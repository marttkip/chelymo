<?php
    $result = '';
    $search = $this->session->userdata('laboratory_report_search');
    if(!empty($search))
    {
      echo '<a href="'.site_url().'close-ultra-search" class="btn btn-sm btn-warning">Close Search</a>';
    }
    
    //if users exist display them
    if ($query->num_rows() > 0)
    {
      $count = $page;
      
      $result .= 
        '
          <table class="table table-hover table-bordered table-striped table-responsive col-md-12">
            <thead>
            <tr>
              <th>#</th>
              <th>Visit Date</th>
              <th>Patient</th>
              <th>Patient Number</th>
              <th>Patient Gender </th>
              <th>Procedure Name</th>
              </tr>
            </thead>
            <tbody>
              
        ';
        
    
      
      $personnel_query = $this->personnel_model->get_all_personnel();
      
      foreach ($query->result() as $row)
      {
        $total_invoiced = 0;
        $visit_date = date('jS M Y',strtotime($row->visit_date));
        $visit_time = date('H:i a',strtotime($row->visit_time));
        if($row->visit_time_out != '0000-00-00 00:00:00')
        {
          $visit_time_out = date('H:i a',strtotime($row->visit_time_out));
        }
        else
        {
          $visit_time_out = '-';
        }
        
        $visit_id = $row->visit_id;
        $patient_id = $row->patient_id;
        $visit_date_days = $row->visit_date;
        $patient_number = $row->patient_number;
        $strath_no = $row->strath_no;
        $personnel_id = $row->personnel_id;
        $dependant_id = $row->dependant_id;
        $strath_no = $row->strath_no;
        $visit_type_id = $row->visit_type_id;
        $gender_id = $row->gender_id;
        $patient_othernames = $row->patient_othernames;
        $patient_surname = $row->patient_surname;
        $patient_date_of_birth = $row->patient_date_of_birth;
        $last_visit = $row->last_visit;
        $service_charge_name = $row->service_charge_name;
        $names = $patient_surname.' '.$patient_othernames;
        

        if($gender_id == 1)
        {
          $gender = 'Male';
        }
        else
        {
          $gender = 'Female';
        }

        // this is to check for any credit note or debit notes
        $age = $this->moh_reports_model->calculate_age($patient_date_of_birth);
        // end of the debit and credit notes


        //creators and editors
        if($personnel_query->num_rows() > 0)
        {
          $personnel_result = $personnel_query->result();
          
          foreach($personnel_result as $adm)
          {
            $personnel_id2 = $adm->personnel_id;
            
            if($personnel_id == $personnel_id2)
            {
              $doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
              break;
            }
            
            else
            {
              $doctor = '-';
            }
          }
        }
        
        else
        {
          $doctor = '-';
        }
        
        $count++;
        
        
          $result .= 
            '
              <tr>
                <td>'.$count.'</td>
                <td>'.$visit_date.'</td>
                <td>'.$names.'</td>
                <td>'.$patient_number.'</td>
                <td>'.$gender.'</td>
                <td>'.$service_charge_name.'</td>

            ';
      }
      
      $result .= 
      '
              </tbody>
            </table>
      ';
    }
    
    else
    {
      $result .= "There are no visit procedures done";
    }
    
    
    //echo $result;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Ultrasound Report | <?php echo $title_ext;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong>RADIOLOGY REPORT (<?php echo $title_ext;?>)</strong>
            </div>
        </div>
        <div class="row receipt_bottom_border" style="margin-bottom: 10px;">
        	<div class="col-md-12">
        		<?php echo $result;?>
        	</div>
        </div>
    </body>

</html>
