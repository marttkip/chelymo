<?php echo $this->load->view('search/moh_705_search.php', '', TRUE);?>
<?php
		
$result = '';
$year_search = $this->session->userdata('moh_year');
$month_search = $this->session->userdata('moh_month');
if(!empty($year_search) AND !empty($month_search))
{
	$date = $year_search.'-'.$month_search.'-01';
	$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
}
else if(!empty($year_search) AND empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date = $year_search.'-'.date('m-').'01';
	$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
else if(empty($year_search) AND !empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date = date('Y-').$month_search.'-01';
	$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
}
else
{
	$date = date('Y-m-').'01';
	$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
$search = $this->session->userdata('moh_705_title_search');
if(!empty($search))
{
	$search = $search;
}
else
{
	$search = '';
}
$title_content = 'Showing diagnosis for '.date('M Y', strtotime($date)).' '.$search;

$items_list = '';
while(strtotime($date) <= strtotime($end)) {
	$day_num = date('d', strtotime($date));
	$day_name = date('l', strtotime($date));
	$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
	$items_list .= "<th>$day_num</th>";
}
  
 

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed" >
		<thead>
			<tr>
				<th>#</th>
				<th>Disease Name</th>
				'.$items_list.'
				<th>Total</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	//get all administrators
	$personnel_query = $this->personnel_model->get_all_personnel();
	
	
	foreach ($query->result() as $row)
	{
		$diseases_name = $row->diseases_name;
		$diseases_id = $row->diseases_id;
		

		$items_lists = '';
		//  get the quantities of diseases detected
		if(!empty($year_search) AND !empty($month_search))
		{
			$date = $year_search.'-'.$month_search.'-01';
			$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
		}
		else if(!empty($year_search) AND empty($month_search))
		{
			// $date = $year_search.'-01-01';
			// $end = $year_search.'-12-' . date('t', strtotime($date));
			$date = $year_search.'-'.date('m-').'01';
			$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
		}
		else if(empty($year_search) AND !empty($month_search))
		{
			// $date = $year_search.'-01-01';
			// $end = $year_search.'-12-' . date('t', strtotime($date));
			$date = date('Y-').$month_search.'-01';
			$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
		}
		else
		{
			$date = date('Y-m-').'01';
			$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
		}

		// var_dump($end); die();
		$total_row = $total_new_counters = $new_visit_total = $old_visit_total = 0;
		$counters = 0;
		
		//no.of first time visits(65) reattendances (66)
		if(($diseases_id == 65) || ($diseases_id == 66))
		{
			$all_visits = $this->moh_reports_model->get_all_visits($date, $end);
			
			while(strtotime($date) <= strtotime($end)) 
			{
				$new_counters = $old_counters = 0;
				if($all_visits->num_rows() > 0)
				{
					foreach($all_visits->result() as $visit)
					{
						$patient_id = $visit->patient_id;
						$visit_date = $visit->visit_date;
						
						if($visit_date == $date)
						{
							$no_of_visits = $this->moh_reports_model->get_per_disease_new_encounters_total($date, $patient_id,0);
							$no_of_revisits = $this->moh_reports_model->get_per_disease_new_encounters_total($date, $patient_id,1);
							$total_row += $no_of_visits;
							if($no_of_visits > 0)
							{
								if($diseases_id == 65)
								{
									$new_counters = $new_counters + 1;
									
								}
							}
							elseif($no_of_revisits > 0)
							{
								if($diseases_id == 66)
								{
									// if($date == '2017-05-09')
									// {
									// echo $patient_id.' ';
									// }
									$old_counters = $old_counters + 1;
								}
							}
						}
					}
				}
				if($diseases_id == 65)
				{
					$new_visit_total += $new_counters;
					$items_lists .= "<td>".$new_counters."</td>";
					$total_row = $new_visit_total;
				}
				else if($diseases_id == 66)
				{
					$old_visit_total += $old_counters;
					$items_lists .= "<td>".$old_counters."</td>";
					$total_row = $old_visit_total;
				}
				$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
			}
		}
		else
		{
			while(strtotime($date) <= strtotime($end)) 
			{
				$day_num = date('d', strtotime($date));
				$day_name = date('l', strtotime($date));
					
				$counters = $this->moh_reports_model->get_counters_per_disease($diseases_id,$date);
				$total_row += $counters;
				// if($date == "2017-03-10")
				// {
				// 		var_dump($counters); die();
				// }
				$items_lists .= "<td>".$counters."</td>";
	
				$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
			}
		}
		// end of getting the diseases detected
		
		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$diseases_name.'</td>
				'.$items_lists.'
				<td>'.$total_row.'</td>
			</tr> 
		';
	}
	
	$result .= 
	'
	  </tbody>
	</table>
	';
}

else
{
	$result .= "There are no types";
}
?>

<?php
	$title_ext = $title_content;




?>

<section class="panel">
	 <header class="panel-heading">
    	 <h2 class="panel-title"><?php echo $title;?></h2>
    	 <a href="<?php echo site_url();?>print-moh-705-report" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print MOH 705 Report</a>
    </header>   
	<div class="panel-body">
    	
        <?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			$this->session->unset_userdata('error_message');
		}

		$search = $this->session->userdata('moh_705_diseases_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'moh_reports/close_search_moh_705" class="btn btn-sm btn-warning">Close Search</a>';
		}
		?>
		<div class="center-align"><?php echo $title_ext;?></div>
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    
    <div class="panel-foot">
        
		<?php if(isset($links)){echo $links;}?>
    
        <div class="clearfix"></div> 
    
    </div>
</section>