<?php
	
$dental = 0;

 $data['visit_id'] = $visit_id;
 $data['lab_test'] = 100;


$visit_rs = $this->accounts_model->get_visit_details($visit_id);

if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$visit_date = $value->visit_date;
		$admission_date = $value->admission_date;
		$discharge_date = $value->discharge_date;
	}
}





 ?>

<div class="row">
	<div class="col-md-12">
	  <!-- Widget -->
	  <section class="panel panel-featured panel-featured-info">
	 
	        <div class="panel-body">
	          <div class="padd">
	             <!-- vitals from java script -->
	             <div class="col-md-12">
	             	<div class="col-md-6">
	             		<div class="form-group">
                           <label class="col-md-4 control-label">Admission Date: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="admission_date" placeholder="Admission date" value=""  autocomplete="off" id="admission_date<?php echo $visit_id?>" value="<?php echo $admission_date?>">
                               </div>
                           </div>
                       </div>

	             		
	             	</div>
	             	<div class="col-md-6">
	             		<div class="form-group">
                           <label class="col-md-4 control-label">Discharge Date: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="discharge_date" id="discharge_date<?php echo $visit_id?>" placeholder="Discharge date" value=""  autocomplete="off" value="<?php echo $discharge_date?>">
                               </div>
                           </div>
                       </div>
	             	</div>
	             	
	             </div>
	             <br>
	             <?php

					$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
					$num_rows2 = count($rs2);

					$v_data['signature_location'] = base_url().'assets/signatures/';
					$v_data['query'] =  $query_data = $this->nurse_model->get_notes(11, $visit_id);

					if($query_data->num_rows() > 0)
					{
						foreach ($query_data->result() as $key => $value_two) {
							# code...
							$summary = $value_two->notes_name;
						}
						
					}
					else
					{
						$summary = '<strong>Complaints : </strong> 
	                            	<p></p>
	                            	<strong>Diagnosis : </strong>
	                            	<p></p>
	                            	<strong>Investigation : </strong> 
	                            	<p></p>
	                            	<strong>Treatment Plan: </strong>
	                            	<p></p>
	                            	<strong>Condition On Discharge : </strong>
	                            	<p></p>';
					}
					if(!isset($mobile_personnel_id))
					{
						$mobile_personnel_id = NULL;
					}
					// var_dump($summary); die();
					$v_data['mobile_personnel_id'] = $mobile_personnel_id;

					$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);

					?>

					<!-- <div id="discharge_summary_notes"></div> -->

					<div class="row">
                    	<div class='col-md-12'>
                        	<input type="hidden" name="date" value="<?php echo date('Y-m-d');?>" />
                        	<input type="hidden" name="time" value="<?php echo date('H:i');?>" />
                            <textarea class='cleditor' id='discharge_note<?php echo $visit_id;?>' >
                            	<?php  echo $summary;?>
                            </textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                    	<div class='col-md-12 center-align'>
                    		<a class='btn btn-info btn-sm' type='submit' onclick='save_discharge_notes(<?php echo $visit_id;?>)'> Save Discharge Summary</a>
                    		<a class='btn btn-warning btn-sm' href="<?php echo site_url()?>print-discharge-summary/<?php echo $visit_id;?>"> Print Discharge Summary</a>
                    	</div>
                    </div>
					
	             <!-- end of vitals data -->
	          </div>
	        </div>
	    </section>
  	</div>
</div>
