<!-- search -->
<?php 
	// this is it
	
	
	
	?>
<!-- end search -->
<section class="panel panel-featured ">
    <header class="panel-heading">
        <h2 class="panel-title"><strong>Names </strong> <?php echo $patient_surname.' '.$patient_othernames?>  <strong>Age </strong> <?php echo $age;?></h2>
    </header>
  	<div class="panel-body">
		<div class="padd">
			<?php
				$search = $this->session->userdata('visit_search');
				
				// if(!empty($search))
				// {
				//     echo '<a href="'.site_url().'nurse/close_queue_search" class="btn btn-warning">Close Search</a>';
				// }
				$result = '';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
				    $count = $page;
				    
				    $result .= 
				        '
				            <table class="table table-hover table-bordered ">
				              <thead>
				                <tr>
				                  <th>#</th>
				                  <th>Visit Date</th>
				                  <th>Visit Type</th>
				                  <th>Assessment</th>
				                  <th>Diagnosis</th>
				                  <th>Plan</th>
				                  <th>Waiting time</th>
				                  <th>Doctor</th>
				                  <th colspan="4">Actions</th>
				                </tr>
				              </thead>
				              <tbody>
				        ';
				    
				    $personnel_query = $this->personnel_model->get_all_personnel();
				    $count = 1;
				    $doctor_id =$this->session->userdata('personnel_id');
				    foreach ($query->result() as $row)
				    {
				        $visit_date = date('jS M Y',strtotime($row->visit_date));
				        $visit_time = date('H:i a',strtotime($row->visit_time));
				
				        $visit_id1 = $row->visit_id;
				        $waiting_time = $this->nurse_model->waiting_time($visit_id1);
				        $patient_id = $row->patient_id;
				        $personnel_id = $row->personnel_id;
				        $dependant_id = $row->dependant_id;
				        $strath_no = $row->strath_no;
				        $created_by = $row->created_by;
				        $modified_by = $row->modified_by;
				        $visit_type_id = $row->visit_type_id;
				        $visit_type = $row->visit_type;
				        $created = $row->patient_date;
				        $last_modified = $row->last_modified;
				        $last_visit = $row->last_visit;
				        $inpatient = $row->inpatient;

				        // var_dump($inpatient);die();
				        
				        $patient_type = $this->reception_model->get_patient_type($visit_type_id);
				        
				        $visit_type = 'General';
				        
				        $patient_othernames = $row->patient_othernames;
				        $patient_surname = $row->patient_surname;
				        $title_id = $row->title_id;
				        $patient_date_of_birth = $row->patient_date_of_birth;
				        $civil_status_id = $row->civil_status_id;
				        $patient_address = $row->patient_address;
				        $patient_post_code = $row->patient_postalcode;
				        $patient_town = $row->patient_town;
				        $patient_phone1 = $row->patient_phone1;
				        $patient_phone2 = $row->patient_phone2;
				        $patient_email = $row->patient_email;
				        $patient_national_id = $row->patient_national_id;
				        $religion_id = $row->religion_id;
				        $gender_id = $row->gender_id;
				        $patient_kin_othernames = $row->patient_kin_othernames;
				        $patient_kin_sname = $row->patient_kin_sname;
				        $relationship_id = $row->relationship_id;
				        
				        //creators and editors
				        if($personnel_query->num_rows() > 0)
				        {
				            $personnel_result = $personnel_query->result();
				            
				            foreach($personnel_result as $adm)
				            {
				                $personnel_id2 = $adm->personnel_id;
				                
				                if($personnel_id == $personnel_id2)
				                {
				                    $doctor = $adm->personnel_fname;
				                    break;
				                }
				                
				                else
				                {
				                    $doctor = '-';
				                }
				            }
				        }
				        
				        else
				        {
				            $doctor = '-';
				        }
				
				        //  get the visit assessment
				
				        $assesment_rs = $this->nurse_model->get_assessment($visit_id1);
				         $assesment_num_rows = count($assesment_rs);
				        $assessment = '';
				        if($assesment_num_rows > 0){
				            foreach ($assesment_rs as $key_assessment):
				            $visit_assessment = $key_assessment->visit_assessment;
				            endforeach;
				            $assessment .="".$visit_assessment."";
				            
				        }
				        else
				        {
				            $assessment .='-';
				        }
				        // end of the visit assessment
				
				
				        //  start of plan
				        $plan_rs = $this->nurse_model->get_plan($visit_id1);
				        $plan_num_rows = count($plan_rs);
				        $plan = "";
				        if($plan_num_rows > 0){
				            foreach ($plan_rs as $key_plan):
				                $visit_plan = $key_plan->visit_plan;
				            endforeach;
				            $plan .="".$visit_plan."";
				            
				        }
				        else
				        {
				            $plan .="-";
				        }
				        // end of plan
				
				        // start of diagnosis
				        $diagnosis_rs = $this->nurse_model->get_diagnosis($visit_id1);
				        $diagnosis_num_rows = count($diagnosis_rs);
				        //echo $num_rows;
				        $patient_diagnosis = "";
				        if($diagnosis_num_rows > 0){
				            foreach ($diagnosis_rs as $diagnosis_key):
				                $diagnosis_id = $diagnosis_key->diagnosis_id;
				                $diagnosis_name = $diagnosis_key->diseases_name;
				                $diagnosis_code = $diagnosis_key->diseases_code;
				                $patient_diagnosis .="".$diagnosis_code."-".$diagnosis_name."<br>";
				                
				            endforeach;
				        }
				        else
				        {
				            $patient_diagnosis .= "-";
				        }

				        if($inpatient == 1)
				        {
				        	$type = 'Admission';
				        }
				        else
				        {
				        	$type = 'Clinic Visit';
				        }
				        // <td><a href="'.site_url().'nurse/patient_card/'.$visit_id1.'/a/1" class="btn btn-xs btn-info" target="_blank">Outpatient Card</a></td>

				  //       if($doctor_id == 10 OR $doctor_id == 12)
						// {
							$fiels = '<td><a href="'.site_url().'nurse/patient_card/'.$visit_id1.'/a/1" class="btn btn-xs btn-info" >Outpatient Card</a></td>
									<td><a href="'.site_url().'patient-uploads/'.$patient_id.'" class="btn btn-xs btn-primary">Uploads</a></td>';

							if($inpatient == 1)
							{
								$fiels .= '<td><a href="'.site_url().'nurse/inpatient_card/'.$visit_id1.'/a/0" class="btn btn-xs btn-info">Inpatient Card</a></td>';
							}
						// }
						// else
						// {
						// 	$fiels = '';
						// }
						
				        $result .= 
				            '
				                <tr>
				                    <td>'.$count.'</td>
				                    <td>'.$visit_date.'</td>
				                    <td>'.$type.'</td>
				                    <td>'.$assessment.'</td>
				                    <td>'.$patient_diagnosis.'</td>
				                    <td>'.$plan.'</td>
				                    <td>'.$waiting_time.'</td>
				                    <td>'.$doctor.'</td>
				                   
									'.$fiels.'
				                </tr> 
				            ';
				            $count++;
				    }
				    
				    $result .= 
				    '
				                  </tbody>
				                </table>
				    ';
				}
				
				else
				{
				    $result .= "There is no history";
				}
				
				echo $result;
				?>
		</div>
		<div class="widget-foot">
			<?php if(isset($links)){echo $links;}?>
			<div class="clearfix"></div>
		</div>
	</div>
</section>