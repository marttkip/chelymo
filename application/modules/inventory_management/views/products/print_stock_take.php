	<?php

	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));

						$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						$search_result ='';
						$search_result2  ='';
						if(!empty($error))
						{
							$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
							$this->session->unset_userdata('error_message');
						}

						if(!empty($success))
						{
							$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
							$this->session->unset_userdata('success_message');
						}

						$search = $this->session->userdata('product_inventory_search');

						if(!empty($search))
						{
							$search_result = '<a href="'.site_url().'inventory/close-product-search" class="btn btn-success btn-sm">Close Search</a>';
						}

						$inventory_search_start_date = $this->session->userdata('inventory_search_start_date');
						if(!empty($inventory_search_start_date))
						{
							$search_start_date = $inventory_search_start_date;
							if($search_start_date == '')
							{
								$search_start_date = NULL;
							}
						}
						else
						{
							$search_start_date = NULL;
						}

						$inventory_search_end_date = $this->session->userdata('inventory_search_end_date');
						if(!empty($inventory_search_start_date))
						{
							$search_end_date = $inventory_search_end_date;
							if($search_end_date == '')
							{
								$search_end_date = NULL;
							}
						}
						else
						{
							$search_end_date = NULL;
						}


						$result = '';
						$result .= ''.$search_result2.'';
						$result .= '
								';

						//if users exist display them
						if ($query->num_rows() > 0)
						{
							$count = $page;

							if($type == 1)
							{
								$cols_items ='
		                                <th>O</th>
		                                <th>T</th>
		                                <th>S</th>
		                                <th>R</th>';
								$colspan = 2;
							}
							else
							{
								$cols_items =
										'
										<!--<th>Unit Price</th>
		                                <th>33% MU</th>
		                                <th>O</th>
		                                <th>P</th>
		                                <th>D</th>-->
		                                ';
		                         $colspan = 7;
							}
							$result .=
							'
							<div class="row">
							<div class="col-md-12 table-responsive">
								<table class="table table-bordered">

								  <thead>
		                                <th>#</th>
		                                <th>Code</th>
		                                <th>Store</th>
		                                <th>Name</th>
		                                <th>Category</th>
		                                <th>Vatable</th>
		                                '.$cols_items.'
		                                <th>B.P</th>
		                                <th>S.P</th>
		                                <th>OPENING</th>
		                            
		                            </thead>
								  <tbody>
							';

							//get all administrators
							// $personnel_query = $this->personnel_model->get_all_personnel();

							foreach ($query->result() as $row)
							{//var_dump($query);die();

								$product_id = $row->product_id;
								$product_name = $row->product_name;
								$product_code = $row->product_code;
								$product_status = $row->product_status;
								$product_description = $row->product_description;
								$store_name = $row->store_name;
								$category_id = $row->category_id;
								$created = $row->created;
								$created_by = $row->created_by;
								$last_modified = $row->last_modified;
								$modified_by = $row->modified_by;
								$category_name = $row->category_name;
								$store_id = $row->store_id;
								$reorder_level = $row->reorder_level;
								$parent_store = $row->store_parent;
								$vatable = $row->vatable;
								$owning_store_id = $row->owning_store_id;
								$regenerate_id = $row->regenerate_id;
								$stock_take = $row->stock_take;
								$product_packsize = $row->product_packsize;
								$buying_unit_price = $row->product_buying_price;
								$preferred_price = $row->preferred_price;
								$store_balance = $row->store_balance;
								$opening_stocking = $row->store_quantity;
								$in_stock_whole_sale = $row->whole_sale_stock;


								if($parent_store == 0)
								{
									$quantity = $row->quantity;
								}
								else
								{
									$quantity = 0;
								}

								$quantity = $row->store_quantity;
								// var_dump($quantity); die();
								$product_unitprice = $row->product_unitprice;
		                        $product_deleted = $row->product_deleted;
		                        $stock_qty = $row->stock_qty;
		                        $in_service_charge_status = $row->in_service_charge_status;
		                        // var_dump($owning_store_id);

								//status
								if($product_status == 1)
								{
									$status = 'Active';
								}
								else
								{
									$status = 'Disabled';
								}


								if($vatable == 1)
								{
									$vatable_status = 'Yes';
								}
								else
								{
									$vatable_status = 'No';
								}

								if($stock_take == 0 OR empty($stock_take))
								{
									$regenerate = 'info';
									$closed = 'readonly';
								}
								else
								{
									$regenerate = 'primary';
									$closed = '';
								}



								$button = '';
								//create deactivated status display
								if($product_status == 0)
								{
									$status = '<span class="label label-danger">Deactivated</span>';
									if($parent_store == 1)
									{
										$button .= '<a class="btn btn-info btn-sm" href="'.site_url().'inventory/activate-product/'.$product_id.'" onclick="return confirm(\'Do you want to activate '.$product_name.'?\');">Activate</a>';
									}
								}
								//create activated status display
								else if($product_status == 1)
								{
									$status = '<span class="label label-success">Active</span>';
									if($parent_store == 1)
									{
										$button .= '<a class="btn btn-default btn-sm" href="'.site_url().'inventory/deactivate-product/'.$product_id.'" onclick="return confirm(\'Do you want to deactivate '.$product_name.'?\');">Deactivate</a>';
									}
								}



									$store_id = $owning_store_id;


                                    $purchases = 0;
                                    $sales = 0;
                                    $purchases = 0;
                                    $deductions = 0;
                                    // $in_stock = 0;
                                    $pending_procurement = 0;
                                    $opening_quantity = 0;
                                    $total_store_deductions = 0;
                                    $child_stock = 0;

                                    // $stock_level = $this->inventory_management_model->get_all_stock($product_id);

                                    // $purchases = $stock_level['dr_quantity'];
                                    // $deductions = $stock_level['cr_quantity'];
                                   
                                    // $in_stock = $purchases - $deductions;

                                    
			                        	// var_dump($store_requests); die();
									
									$other_items =
										'
		                                <td>'.$purchases.'</td>
		                                <td>'.$deductions.'</td>

										';



										if($parent_store == 0 )
										{
											
													// <td><a href="'.site_url().'search-store-ded/'.$product_id.'" class="btn btn-sm btn-info fa fa-database"></a></td>
												
												$button_two_sub ='

													<td><a href="'.site_url().'inventory/drug-trail/'.$product_id.'/'.$owning_store_id.'" class="btn btn-sm btn-primary fa fa-file"></a></td>
													<td><a class="btn btn-default btn-sm fa fa-money" href="'.site_url().'inventory/product-sales/'.$product_id.'" ></a></td>
													<td><a href="'.site_url().'inventory/deduction-product/'.$product_id.'/'.$store_id.'" class="btn btn-sm btn-danger fa fa-minus" tooltip="Reduce Stock"></a></td>
													 <td><a href="'.site_url().'inventory/product-purchases/'.$product_id.'/'.$store_id.'" class="btn btn-sm btn-success fa fa-plus"></a></td>
													 
													';
												// $personnel_role = $this->session->userdata('personnel_role');

												$personnel_role = $this->session->userdata('personnel_role');
												$regenerate_button = '';

												// var_dump($personnel_role);die();
												if($personnel_role == 'Administrator' OR $personnel_role == 'Accounts' OR $personnel_role == 'CEO')
												{
													$regenerate_button = '<td><a href="'.site_url().'regenerate-product/'.$product_id.'" class="btn btn-sm btn-danger fa fa-recycle" onclick="return confirm(\'Are you sure you want to regenerate this product?\');"></a></td>
													<td><a href="'.site_url().'delete-product/'.$product_id.'" class="btn btn-sm btn-danger fa fa-trash" tooltip="Delete" onclick="return confirm(\'Are you sure you want to delete this record ? \')"></a></td>';
												}
												else
												{
													$regenerate_button = '';
												}

												
											
												$button_two = ' <td><a href="'.site_url().'inventory/edit-product/'.$product_id.'" class="btn btn-sm btn-primary fa fa-pencil"> </a></td>
					                           	'.$button_two_sub.'
					                            '.$regenerate_button.'


					                              ';

										}
										else
										{
											
											$personnel_role = $this->session->userdata('personnel_role');
											$delete_button = '';
											if($personnel_role == 'Administrator' OR $personnel_role == 'Accounts' OR $personnel_role == 'CEO')
											{
												$delete_button = '<td><a href="'.site_url().'inventory/deduction-product/'.$product_id.'/'.$store_id.'" class="btn btn-sm btn-danger fa fa-minus" tooltip="Reduce Stock"></a></td>
													';
											}
											else
											{
												$delete_button = '<td><a href="'.site_url().'inventory/deduction-product/'.$product_id.'/'.$store_id.'" class="btn btn-sm btn-danger fa fa-minus" tooltip="Reduce Stock"></a></td>';
											}
											$button_two = ' <td><a href="'.site_url().'inventory/edit-product/'.$product_id.'" class="btn btn-sm btn-primary fa fa-pencil"> </a></td>
														<td><a href="'.site_url().'inventory/drug-trail/'.$product_id.'/'.$owning_store_id.'" class="btn btn-sm btn-primary fa fa-file"></a></td>
															<td><a class="btn btn-default btn-sm fa fa-money" href="'.site_url().'inventory/product-sales/'.$product_id.'" ></a></td>
													 <td><a href="'.site_url().'search-s11/'.$product_id.'" class="btn btn-sm btn-success fa fa-search"></a></td>
													  '.$delete_button.'';
											
										}


								// }
							  	// var_dump($department_id); die();
										if( ($personnel_id_main==0))
										{

											// $button_update = '<td><input type="text" name="amount" class="form-control" value="'.$quantity.'" '.$closed.'/></td>
											// 	 <td><button type="submit" class="btn btn-sm btn-warning" >Update </button></td>';
										}
										else
										{
											if(($department_id == 1 AND ($is_admin OR $personnel_id_main == 0) AND $store_id > 5))
											{
												$button_update = ' <td><a href="'.site_url().'inventory/return-product/'.$product_id.'/'.$store_id.'" class="btn btn-sm btn-warning fa fa-arrow-left"></a></td>';
											}
											else
											{
												$button_update = '';
											}

										}

										if(!empty($buying_unit_price) AND !empty($product_unitprice))
										{
											// mark up 

											$mark_up = number_format((($product_unitprice - $buying_unit_price)*100)/$buying_unit_price,2);
										}
										else
										{
											$mark_up = '';
										}

										if($preferred_price == 0)
										{
											$preferred_price = $product_unitprice;
										}

								// $button_update = '<td>'.$product_unitprice.'</td>
								// 			<td><input type="text" name="preferred_price" class="form-control" value="'.$preferred_price.'" /></td>
								// 			<td><button type="submit" class="btn btn-sm btn-warning" >Update </button></td>';
									// $categories = '';
								 //    if($all_categories->num_rows() > 0)
         //                          	{
         //                                $result_new = $all_categories->result();
                                        
         //                                foreach($result_new as $res_cap)
         //                                {
         //                                    if($res_cap->category_id == $category_id)
         //                                    {
         //                                         $categories .= '<option value="'.$res_cap->category_id.'" selected>'.$res_cap->category_name.'</option>';
         //                                    }
         //                                    else
         //                                    {
         //                                         $categories .= '<option value="'.$res_cap->category_id.'">'.$res_cap->category_name.'</option>';
         //                                    }
         //                                }
         //                            }
								// $button_update = '
								// 				<td><input type="text" name="product_packsize" class="form-control" value="'.$product_packsize.'" /></td>
								// 				<td>
													
	       //                                                  <select name="category_id" id="category_id" class="form-control" required="required">
	       //                                                  	'.$categories.'                  
	       //                                                  </select>
	                                                   
        //                                         </td>
								// 				<td><input type="text" name="stock_qty" class="form-control" value="'.$stock_qty.'" /></td>
								// 				<td><button type="submit" class="btn btn-sm btn-warning" >Update</button></td>';

								// <td><input type="text" name="product" class="form-control" value="" required="required"/></td> under $button_update

                                    $authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
                                    if($stock_take == 1)
                                    {
                                    	$button_update = '
												
												
												<td><input type="text" name="stock_qty" class="form-control" value="'.$stock_qty.'" /></td>
												<td><button type="submit" class="btn btn-sm btn-warning" onclick="return confirm(\'You are about to merge to this drug. Do you want to proceed ? \')" >Update</button></td>';
                                    }
                                    else
                                    {
                                    	$button_update = '<td></td><td></td>';

                                    	if($owning_store_id == 5)
                                    	{
                                    		$button_update = '<td></td><td><a href="'.site_url().'regenerate-product/'.$product_id.'" class="btn btn-sm btn-danger " tooltip="Re Count" onclick="return confirm(\'Are you sure you want to re count this record ? \')"><i class="fa fa-recycle"></i></a></td>';
                                    	}
                                    }
                                $regenerate_button = '';
                                if($authorize_invoice_changes OR $personnel_id == 0)
                                {
                                	$regenerate_button = '
													<td><a href="'.site_url().'recalculate-product/'.$product_id.'/'.$store_id.'" class="btn btn-sm btn-warning fa fa-refresh" tooltip="Recalculate" onclick="return confirm(\'Are you sure you want to recalculate this record ? \')"></a></td>


													<td><a href="'.site_url().'delete-product/'.$product_id.'" class="btn btn-sm btn-danger fa fa-trash" tooltip="Delete" onclick="return confirm(\'Are you sure you want to delete this record ? \')"></a></td>
													';
                                }
                                   
								

								// $child_store_stock = $this->inventory_management_model->child_store_stock($inventory_start_date, $product_id,6);
								$count++;

								$stock_status = $this->session->userdata('stock_status');

								// $result.= form_open("update-drug-prices/".$product_id."/".$owning_store_id, array("class" => "form-horizontal"));
								$result.= form_open("update-current-stock/".$product_id."/".$owning_store_id, array("class" => "form-horizontal"));
								// $result.= form_open("merge-drugs/".$product_id."/".$owning_store_id, array("class" => "form-horizontal"));

								if($owning_store_id == 5)
								{
									$store_quantity = $store_balance;
								}
								else
								{
									$store_quantity = $store_balance;
								}

								if($store_quantity<=$reorder_level)
								{
		                        	$class = "class = 'danger'";
		                        }
								else{
		                        	$class = "";

		                        }

									$result .=
									'
										<tr >
											<input type="hidden" name="category_id" class="form-control" value="'.$category_id.'" />
											<td>'.$count.'</td>
											<td class="'.$regenerate.'">'.$product_code.'</td>
											<td>'.$store_name.'</td>
											<td '.$class.'>'.strtoupper(strtolower($product_name)).'</td>
											<td>'.$category_name.'</td>
											<td>'.$vatable_status.'</td>
											<td>'.number_format($buying_unit_price,2).'</td>
											<td>'.number_format($product_unitprice,2).'</td>
			                            
			                      
			                                 <td class="info">'.$opening_stocking.'</td>
			                       
			                             

			                      


										</tr>
									';
									 $result .= form_close();


							}

							$result .=
							'
										  </tbody>
										</table>
										</div>
									</div>
							';
						}

						else
						{
							$result .= '';
						}

						$result .= '</div>';
					//	echo $result;
				?>
<!DOCTYPE html>
<html lang="en">

    <head>
    <!--       <title> Reports</title> -->
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
           .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
            {
                 padding: 2px;
            }
            .title-img{float:left; padding-left:10px;}
            img.logo{ margin:0 auto;}
           .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:0px;}
            img.logo{height:130px; margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            h4, .h4 {
                font-size: 24px;
                font-weight: bolder;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">

        <div class="row receipt_bottom_border" >
            <div class="col-md-12">
                <div class="pull-left" style="margin-bottom: 8px;">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="height: 100px; width: 100px;"/>
                </div>
     

                <div class="pull-left">
                    <strong>
                        <h4  style="font-size:14px">
                        <?php echo $contacts['company_name'];?><br/>
                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                        <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                        E-mail: <?php echo $contacts['email'];?>.<br/>
                         Tel : <?php echo $contacts['phone'];?><br/>
                     </h4>
                    </strong>
                </div>
                <div class="pull-right" style="margin-right:30px">
                    <strong>
                    <!--     <h4  style="font-size:18px">
                        	<?php echo $requisition_number; ?>
                    
                  
                     </h4> -->
                    </strong>
                </div>
            </div>
        </div>
        <div class="row receipt_bottom_border" >
            <div class="col-md-12 center-align">
                <h4>OPENING STOCK FORM</h4>
            </div>
        </div>
      
        
        <!-- <div class="padd"> -->
            <div class="col-print-12">
                <?php echo $result;?>
            </div>
          
              <!-- <div class="pull-left"> -->
            <div class="col-md-12" style="font-style:bold;font-weight: bold;">
                <div class="col-print-12" style="margin-bottom: 30px; margin-top: 20px;">
                    <div class="col-print-4">
                        Prepared by : <?php echo $served_by; ?>
                    </div>
                    <div class="col-print-4">
                        Designation : ......................................................
                    </div>
                    <div class="col-print-4">
                        Sign : ...........................................................
                    </div>
                </div>
       
                <div class="col-print-12" style="margin-bottom: 30px;">
                    <div class="col-print-4">
                        Checked by : ......................................................
                    </div>
                    <div class="col-print-4">
                        Designation : ......................................................
                    </div>
                    <div class="col-print-4">
                        Sign : ............................................................
                    </div>
                </div>
                <div class="col-print-12" style="margin-bottom: 30px;">
                    <div class="col-print-4">
                        Approved by : ......................................................
                    </div>
                    <div class="col-print-4">
                        Designation : ......................................................
                    </div>
                    <div class="col-print-4">
                        Sign : .............................................................
                    </div>
                </div>
                 <div class="col-print-12" style="margin-bottom: 30px;">
                    <div class="col-print-4">
                        Authorised by : ......................................................
                    </div>
                    <div class="col-print-4">
                        Designation : .........................................................
                    </div>
                    <div class="col-print-4">
                        Sign : ................................................................
                    </div>
                </div>
            </div>
        <!-- </div> -->
        <!-- </div> -->
       
    </body>
    
</html> 