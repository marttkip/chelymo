<?php

class Requisition_model extends CI_Model 
{
	/*
	*	Retrieve all orders
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_requisitions($table, $where, $per_page, $page)
	{
		//retrieve all orders
		$this->db->from($table);
		$this->db->select('requisition.*,personnel.personnel_fname,personnel.personnel_onames,store.*,requisition.created AS requisition_date,requisition.created_by as personnel_created');
		$this->db->where($where);
		$this->db->order_by('requisition.created','DESC');
		$this->db->join('personnel', 'personnel.personnel_id = requisition.created_by','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	 public function get_requisition_order($requisition_id)
    {
         //retrieve all users
        $this->db->from('requisition');
        $this->db->select('requisition.*, personnel.personnel_fname AS added_by');
        $this->db->where('requisition_id ='.$requisition_id);
        $this->db->join('personnel','personnel.personnel_id = requisition.created_by','LEFT');
        $query = $this->db->get();
        
        return $query;

    }

	 public function get_product_deductions($table, $where, $per_page, $page)
    {
         //retrieve all users
    	$this->db->from($table);
        $this->db->select('product_deductions.*,personnel.personnel_fname,personnel.personnel_onames,store.*,product_deductions.requested_by as personnel_created');
		$this->db->where($where);
		$this->db->order_by('product_deductions.product_deductions_date','DESC');
		$this->db->join('personnel', 'personnel.personnel_id = product_deductions.requested_by','left');
		$query = $this->db->get('', $per_page, $page);
		//$this->db->group_by('store.store_id');
        
        return $query;

    }


  //   public function get_product_deductions($table, $where, $per_page, $page)
  //   {
  //        //retrieve all users
  //   	$this->db->from($table);
  //       $this->db->select('*');
		// $this->db->where($where);
		// $query = $this->db->get('', $per_page, $page);
		// $this->db->group_by('store.store_id');
        
  //       return $query;

  //   }

    	public function get_store()
	{
		$this->db->where('store_id > 0');
		$query = $this->db->get('store');
		return $query;
	}


	public function get_store_type($store_name)
	{
		$this->db->where('product_deductions.product_id = product.product_id AND product_deductions.store_id = store.store_id AND store.store_name = "'.$store_name.'" AND product_deductions.product_deductions_quantity > 0');
		$query = $this->db->get('product_deductions,store,product');


		return $query;
	}


       public function get_store_product_order($store_product_id)
    {
         //retrieve all users
        $this->db->from('store_product');
        $this->db->select('*');
        $this->db->where('store_product_id ='.$store_product_id);
        $query = $this->db->get();
        
        return $query;

    }

	public function get_all_requisitions_orders($table, $where, $per_page, $page)
	{
		//retrieve all orders
		$this->db->from($table);
		$this->db->select('requisition.*,personnel.personnel_fname,personnel.personnel_onames,store.*,orders.*,orders.store_id AS store');
		$this->db->where($where);
		$this->db->order_by('requisition.created','DESC');
		$this->db->join('personnel', 'personnel.personnel_id = requisition.created_by','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function create_supplier_requisition()
	{
		$store_id = $this->input->post('store_id');
		// $store_from_id = $this->input->post('store_from_id');
		//check if store is parent store
		// $order_type = $this->is_store_parent($store_id);
		$prefix = $this->requisition_number();

		$requisition_number = 'CMC-REQ'.$prefix;

		$array = array(
			'created'=>date('Y-m-d'),
			'store_id'=>$store_id,
			'requisition_number'=>$requisition_number,
			'created'=>date('Y-m-d'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'requisition_type_id'=>2,
			'suffix'=>$prefix
		);
		if($this->db->insert('requisition', $array))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}


	public function create_requisition()
	{
		$store_id = $this->input->post('store_id');
		// $store_from_id = $this->input->post('store_from_id');
		//check if store is parent store
		// $order_type = $this->is_store_parent($store_id);
		$prefix = $this->requisition_number();

		$requisition_number = 'CMC-'.$prefix;

		$array = array(
			'created'=>date('Y-m-d'),
			'store_id'=>$store_id,
			'requisition_number'=>$requisition_number,
			'created'=>date('Y-m-d'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'requisition_type_id'=>1,
			'suffix'=>$prefix
		);
		if($this->db->insert('requisition', $array))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function requisition_number()
	{

		$this->db->where('requisition_id > 0 ');
		$this->db->from('requisition');
		$this->db->select('MAX(suffix) AS number');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			
		$number++;//go to the next number
			if($number == 1){
				$number = "00001";
			}
			if($number == 1)
			{
				$number = "00001";
			}

		}
		else{

			$number = 00001;
		}
		return $number;
	}

	public function get_store_balance($store_id,$product_id)
	{
		$this->db->where('product_id = '.$product_id.' AND owning_store_id = '.$store_id.'' );
		$store_query = $this->db->get('store_product');
		$store_balance = 0;
		if($store_query->num_rows() > 0)
		{
			foreach ($store_query->result() as $key => $value) {
				// code...
				$store_balance = $value->store_balance;
			}
		}

		if(empty($store_balance))
		{
			$store_balance = 0;
		}

		return $store_balance;
	}


	public function get_all_requisition_orders()
	{

		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$invoice_number = $this->input->post('invoice_number');
		$order_approval_status = $this->input->post('order_approval_status');
		$creditor_id = $this->input->post('creditor_id');
		$lpo_number = $this->input->post('lpo_number');

		$add ='';
		if(!empty($order_approval_status))
		{
			$add .= ' AND orders.order_approval_status = '.$order_approval_status;
		}


		if(!empty($creditor_id))
		{
			$add .= ' AND orders.supplier_id = '.$creditor_id;
		}

		if(!empty($creditor_id))
		{
			$add .= ' AND requisition.requisition_number LIKE "%'.$invoice_number.'%"';
		}


		if(!empty($date_from) AND !empty($date_to))
		{
			$add .= ' AND DATE(requisition.created) BETWEEN "'.$date_from.'" AND "'.$date_to.'"';
		}
		else if(empty($date_from) AND !empty($date_to))
		{
			$add .= ' AND DATE(requisition.created) = "'.$date_to.'"';
		}

		else if(!empty($date_from) AND empty($date_to))
		{
			$add .= ' AND DATE(requisition.created) = "'.$date_from.'"';
		}
		if(!empty($lpo_number))
		{
			$add .= ' AND orders.lpo_number LIKE   "%'.$lpo_number.'%"';
		}



		$this->db->where('requisition.requisition_id = orders.requisition_id AND orders.order_approval_status >= 3 AND orders.supplier_id = creditor.creditor_id  AND requisition.requisition_id > 1 AND requisition.requisition_delete = 0'.$add);
		$this->db->select('requisition.*,requisition.created AS requisition_date,creditor.creditor_name,creditor.creditor_id,orders.order_number,orders.order_approval_status,orders.order_id,orders.lpo_number,orders.billing_status,orders.created_by,orders.approved_by');
		$query = $this->db->get('requisition,creditor,orders');

		return $query;
	}

	 public function get_items_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*,order_supplier.discount AS discount');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}
	public function get_invoice_detail($creditor_invoice_id)
	{
		//retrieve all users
		$this->db->from('creditor_invoice');
		$this->db->select('*');
		$this->db->where('creditor_invoice_id = '.$creditor_invoice_id);
		$query = $this->db->get();
		
		return $query;
	}

	public function get_order_detail($order_id)
	{
		//retrieve all users
		$this->db->from('orders');
		$this->db->select('*');
		$this->db->where('order_id = '.$order_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_supplier_order_details($supplier_order_id,$creditor_id)
	{
		$this->db->where('creditor.creditor_id = orders.supplier_id AND orders.order_id = '.$supplier_order_id.' AND creditor.creditor_id = '.$creditor_id);
		$this->db->select('creditor.*,orders.*,requisition.requisition_number,orders.approved_by AS approvee');
		$this->db->join('requisition','requisition.requisition_id = orders.requisition_id','LEFT');
		$query = $this->db->get('creditor,orders');
		
		return $query;
	}
	public function get_order_items($order_id)
	{
		$this->db->select('product.product_name, order_item.*');
		$this->db->where('product.product_id = order_item.product_id AND order_item.order_id = '.$order_id);
		$query = $this->db->get('order_item, product');
		
		return $query;
	}

	public function get_order_items_supplier($order_id,$creditor_id)
	{
		$this->db->select('order_supplier.quantity AS supplying,order_supplier.unit_price AS single_price, product.*,order_supplier.*,order_item.*');
		$this->db->where('order_supplier.supplier_id = '.$creditor_id.' AND order_item.order_id = '.$order_id.'  AND order_item.order_item_id = order_supplier.order_item_id AND order_item.product_id = product.product_id');
		$this->db->order_by('order_supplier_id');
		$query = $this->db->get('order_supplier,order_item,product');
		
		return $query;

	}

	public function get_personnel_name($personnel_id)
	{
		//retrieve all users


		$this->db->from('personnel');
		$this->db->select('*');
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get();

		$names = '';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$personnel_fname = $value->personnel_fname;
				$personnel_onames = $value->personnel_onames;
				$names = $personnel_fname.' '.$personnel_onames;
			}
		}

		if($personnel_id == 0)
		{
			$names = 'Administrator';
		}
		
		return $names;
	}

    public function confirm_user()
    {
        //select the personnel by username from the database

    	   $personnel_id = $this->session->userdata('personnel_id');
        $this->db->select('*');
        $this->db->where(
            array(
                'personnel_username' => $this->input->post('personnel_username'), 
                'personnel_status' => 1, 
                'personnel_password' => md5($this->input->post('personnel_password')),
                'personnel_id'=>$personnel_id
            )
        );
        $query = $this->db->get('personnel');
        
        //if personnel exists
        if ($query->num_rows() > 0)
        {
            $result = $query->result();

            $personnel_id = $result[0]->personnel_id;
          

            return $personnel_id;
        }
        
        //if personnel doesn't exist
        else
        {
            return FALSE;
        }
    }


    /*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	// public function delete_requisition($requisition_id)
	// {
	// 	//delete parent
	// 	if($this->db->delete('requisition', array('requisition_id' => 1)))
	// 	{
	// 		return TRUE;
	// 	}
	// 	else{
	// 		return FALSE;
	// 	}
	// }


	public function delete_requisition($requisition_id)
	{
		$data = array(
        	"requisition_delete" => 1,
        	
    	);
		
		$this->db->where('requisition_id', $requisition_id);
		
		if($this->db->update('requisition', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}


	public function reversal($requisition_id)
	{
		$data = array(
        	"requisition_status" => 1,
        	
    	);
		
		$this->db->where('requisition_id', $requisition_id);
		
		if($this->db->update('requisition', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}




	public function correct($requisition_id)
	{
		$data = array(
        	"requisition_status" => 0,
        	
    	);
		
		$this->db->where('requisition_id', $requisition_id);
		
		if($this->db->update('requisition', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	



}
?>