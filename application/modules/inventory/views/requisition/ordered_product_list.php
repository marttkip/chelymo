<?php

$result = '';

$result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>PRODUCT NAME</th>
                  <th>RECEVING STORE</th>

                  <th>SELLING UNIT PRICE</th>
                  <th>REQUESTED UNITS</th>
                  <th>REQUESTED VALUE</th>
                  <th>UNITS TO AWARD</th>
                  <th>AWARDED UNITS</th>
                  <th>AWARDED VALUE</th>
                   <th>TOTAL ITEMS</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$product_deductions_id = $value->product_deductions_id;
			$product_name = $value->product_name;
      $product_unitprice = $value->product_unitprice;
			$store_name = $value->store_name;
			$quantity_given = $value->quantity_given;
      $product_buying_price = $value->product_buying_price;
			$units = $value->product_deductions_quantity;

              if($product_buying_price == 0)
              {
                $product_buying_price = 1;
              }

                $requested_value = $units * $product_unitprice;

              $awarded_value = 0;
              if($quantity_given > 0)
              {
                $awarded_value = $quantity_given * $product_unitprice;
              }

			if(empty($units))
			{
				$units = 0;
			}

        $store_id = 5;
        $child_store_stock = $this->inventory_management_model->get_store_available_units($product_id,$store_id);


			 $result .= 
                '
                    <tr >
                        <td>'.$product_name.'</td>
                        <td>'.$store_name.'</td>

                         <td>'.$product_unitprice.'</td>
                        <td>'.$units.'</td>
                        <td>'.number_format($requested_value,2).'</td>
                        <td><input type="text" name="units'.$product_deductions_id.'" id="units'.$product_deductions_id.'" class="form-control" value="'.$quantity_given.'" ></td>
                         <td>'.$quantity_given.'</td>
                         <td>'.number_format($awarded_value,2).'</td>
                        <td><a class="btn btn-xs btn-success" onclick="award_item('.$product_deductions_id.','.$requisition_id.','.$order_id.','.$product_id.')"><i class="fa fa-pencil"></i></a></td>
                    </tr> 
                ';
			
		}
	}
}
 $result .= 
                '
                    <tr >
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th>'.$total.'</th>
                    </tr> 
                ';
        '
          </tbody>
        </table>
        ';
echo $result;


?>