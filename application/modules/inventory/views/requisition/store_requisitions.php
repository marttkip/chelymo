<?php echo $this->load->view('store_requisition_search', '', TRUE);?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
         	<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#orders_modal">
            	Create New Store Requisition
            </button>
            	
          </div>

         
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">


                            
        <div class="modal fade" id="orders_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create New Requisition</h4>
                    </div>
                    <?php echo form_open('inventory/requisition/create_store_requisition', array('class' => 'form-horizontal'));?>
                        	
                    <div class="modal-body">
                    	
                    	
                        	<div class="form-group">
                                <label for="exampleInputEmail1" class="col-md-5">Receiving Store</label>
                                <div class="col-md-7">
                                	<select name="store_id" id="store_id" class="form-control" required="required">
                                    <option value="0">--Select Store--</option>
                                	 <?php
										if($store_priviledges->num_rows() > 0)
										{
											foreach ($store_priviledges->result() as $key)
											{
												# code...
												$store_parent = $key->store_parent;
												$store_id = $key->store_id;
												$store_name = $key->store_name;
												
												?>
												<option value="<?php echo $store_id;?>"><?php echo $store_name;?></option>
												<?php
											}
										}
									?>
                                    </select>
                                </div>
                            </div>  
                           
                          <!--   <div class="form-group">
                            	<div class="col-sm-offset-2 col-sm-10">
                            		
                                </div>
                            </div> -->
                      
                   

                    </div>
                    <div class="modal-footer">
                    	<button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to create this requisition ? ')">Create Requisition</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                      <?php echo form_close();?>
                </div>
            </div>
        </div>
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				$search_result ='';
				$search_result2  ='';
				if(!empty($error))
				{
					$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
						
				$search = $this->session->userdata('orders_search');
				
				if(!empty($search))
				{
					$search_result = '<a href="'.site_url().'inventory/orders/close_search_orders" class="btn btn-danger">Close Search</a>';
				}


				$result = '<div class="padd">';	
				$result .= ''.$search_result2.'';
				$result .= '
						';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<div class="row">
						<div class="col-md-12">
							<table class="example table-autosort:0 table-stripeclass:alternate table table-hover table-bordered " id="TABLE_2">
							  <thead>
								<tr>
								  <th >#</th>
							
								  <th class="table-sortable:default table-sortable" title="Click to sort">Time</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Date Sent</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Requisition Number</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Requesting Store</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Created By</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Closed By</th>
								  <th colspan="2">Actions</th>
								</tr>
							  </thead>
							  <tbody>
							';
					
								//get all administrators
								$personnel_query = $this->personnel_model->get_all_personnel();
								
								foreach ($query->result() as $row)
								{
									$requisition_id = $row->requisition_id;
									$requisition_number = $row->requisition_number;
									$requisition_date = $row->requisition_date;
									$store_id = $row->store_id;
									$status = $row->requisition_status;
									$modified_by = $row->created_by;
									$store_name = $row->store_name;
									$personnel_fname = $row->personnel_fname;
									$personnel_onames = $row->personnel_onames;
									$date_sent = $row->date_sent;
									
									$created_by = $personnel_fname.' '.$personnel_onames;

									if(!empty($date_sent))
									{
										$sent = '<td>'.date('Y-m-d H:i',strtotime($date_sent)).'</td>';	
									}

									else
									{
										$sent = '<td>-</td>';
									}


									
									// var_dump($order_approval_status); die();

									// $order_details = $this->orders_model->get_order_items($order_id);
									$total_price = 0;
									$total_items = 0;
									//creators & editors
									
									$button = '';

									

									if($status == 0)
									{
										$add = '<td><a href="'.site_url().'view-store-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-folder">OPEN DETAIL</a></td>';
										$status_name = 'Open Requisition';
									}
									else if($status == 1)
									{
										$add = '<td><a href="'.site_url().'print-store-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-warning  btn-sm fa fa-print" target="_blank"> PRINT</a></td>';
										$status_name = 'Waiting to be awarded';
									}

									// just to mark for the next two stages
								

									$count++;
									$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.date('H:i',strtotime($requisition_date)).'</td>
											'.$sent.'
											<td>'.$requisition_number.'</td>
											<td>'.$store_name.'</td>
											<td>'.$created_by.'</td>
											<td>'.$status_name.'</td>
											
											'.$add.'
											
											
										</tr> 
									';
									// }
								}
					
						$result .= 
						'
								  </tbody>
								</table>
							</div>
						</div>
						';
				}
				
				else
				{
					$result .= "There are no orders";
				}
				$result .= '</div>';
				echo $result;
			?>

    </div>
</section>