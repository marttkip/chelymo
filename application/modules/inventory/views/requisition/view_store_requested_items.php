
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">

			<input type="text" class="form-control" id="requisition_list" onkeyup="get_all_req_items(<?php echo $requisition_id ?>,<?php echo $order_id ?>)" > 

			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				
				<div id="products-list"></div>
			</div>
			<div class="center-align">
				<a  href="<?php echo site_url()?>complete-store-awards/<?php echo $requisition_id?>/<?php echo $order_id?>" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to complete this requisition order. Note that once you complete this you will not be able to make any adjustments ? ')"/> Complete Awarding Request</a>
			</div>
		</div>
		
		
	</div>
	
</div>

<script type="text/javascript">
	$(document).ready(function(){
  	

      	get_all_ordered_products(<?php echo $requisition_id?>,<?php echo $order_id?>);
	 });


	function get_all_ordered_products(requisition_id,order_id)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/store_ordered_items/"+requisition_id+"/"+order_id;
			// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);
			    	// alert(data);
			  if(data.message == 'success')	 
			  {

			  		$('#products-list').html(data.results);
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


  	function get_selected_list(requisition_id=0,store_id)
  	{
  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/requisition_items/"+requisition_id+"/"+store_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#selected-list').html(data.results);
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  

  	function award_item(product_deductions_id,requisition_id,order_id,product_id)
  	{

  		var units = $('#units'+product_deductions_id).val();

  		var res = confirm('Are you sure you want to award '+units+' to the requesting store ?');


  		if(res)
  		{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/update_quantity_given/"+product_deductions_id+"/"+requisition_id+"/"+order_id+"/"+product_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: units},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	alert(data.results);
			  }
			 
			  get_all_ordered_products(requisition_id,order_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});

  		}


  		
  	}


	function remove_items(product_deductions_id,requisition_id,store_id)
  	{

  		var res = confirm('Are you sure you want to remove this product to the list ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/delete_deduction_item/"+product_deductions_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id,store_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function confirm_requisition(requisition_id)
  	{

  		var res = confirm('Are you sure you want to confirm this request of requisition ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/confirm_requisition/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	window.location.href = 	config_url+'procurement/requisitions';
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

	function get_all_req_items(requisition_id=0,order_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/store_ordered_items/"+requisition_id+"/"+order_id;
			 var name = $('#requisition_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, drug : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#products-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}

	  function get_all_prod_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/requisition_items/"+requisition_id+"/"+store_id;
			var name = $('#product_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, product_name : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}
  	function check_other_stores(product_id,requisition_id,store_id)
  	{

  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/stores_quantity/"+product_id+"/"+requisition_id+"/"+store_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);

			if(data.message == 'success')	 
			{

				$('#stores-list').html(data.results);
				
				
			}
			else
			{
				// alert(data.result);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

  	}


  	function create_order(product_id,requisition_id,ordering_store_id,store_id)
  	{

  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/add_requisition_orders/"+product_id+"/"+requisition_id+"/"+ordering_store_id+"/"+store_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);

			if(data.message == 'success')	 
			{

				// $('#selected-list').html(data.results);
				get_selected_list(requisition_id,ordering_store_id);
				
				
			}
			else
			{
				// alert(data.result);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

  	}
	  
	

</script>
