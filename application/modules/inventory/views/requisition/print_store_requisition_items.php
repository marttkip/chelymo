<?php

$created_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));

$order_details = $this->requisition_model->get_requisition_order($requisition_id);


$requisition_number = '';
$added_by = '';
$created = '';

if($order_details->num_rows() > 0)
{
	foreach ($order_details->result() as $key => $value) {
		# code...
		$requisition_number = $value->requisition_number;
        $created = $value->created;
		$added_by = $value->added_by;
	
	}
}



$result = '';

$result ='<table class="table table-hover table-condensed table-bordered "  id ="testTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>PRODUCT NAME</th>
                  <th>STORE FROM</th>
                   <th>UNITS REQUESTED</th>
                   <th>UNITS AWARDED</th>
                   <th>UNIT PRICE</th>
                   <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$product_deductions_id = $value->product_deductions_id;
            $product_code = $value->product_code;
			$product_name = $value->product_name;
			$store_name = $value->store_name;
            $product_unitprice = $value->product_unitprice;
			$ordering_store_id = $value->ordering_store_id;
			$orders_date = $value->orders_date;
            $quantity_given = $value->quantity_given;
			$units = $value->quantity_requested;
			

            $total =  $product_unitprice * $quantity_given;

            $total_units += $total;
			//$requisition_number = $value->requisition_number;

                    if($ordering_store_id == 6)
						{
							$store = 'OPD Store';
						}
						else if($ordering_store_id == 9)
						{
							$store = 'IPD Store';
						}
						else if($ordering_store_id == 10)
						{
							$store = 'Catering Store';
						}
						else if($ordering_store_id == 12)
						{
							$store = 'Theatre Store ';
						}
							else if($ordering_store_id == 13)
						{
							$store = 'Nurse Store';
						}
							else if($ordering_store_id == 14)
						{
							$store = 'Laboratory Store';
						}



			if(empty($units))
			{
				$units = 0;
			}


			 $result .= 
                '
                    <tr >
                        <td>'.$product_code.'</td>
                        <td>'.$product_name.'</td>
                        <td>'.$store_name.'</td>
                        <td>'.$units.'</td>
                        <td>'.$quantity_given.'</td>
                        <td>'.$product_unitprice.'</td>
                        <td>'.$total.'</td>
                        
                    </tr> 
                ';
			
		}
	}
}
 $result .= 
        '
                <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <td>'.$total_units.'</td>
                        
                    </tr> 
          </tbody>
        </table>
        ';
//echo $result;


?>
<!DOCTYPE html>
<html lang="en">

    <head>
    <!--       <title> Reports</title> -->
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:15px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 15px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 60%;
             }
        
            h3
            {
                font-size: 10px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 10px;}
            .title-img{float:left; padding-left:10px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {  
                            width:20%; float:right; 
                            /*background-color: green; */
                            color:black;
                            height: 73px !important;
                            padding: 20px;
                            word-spacing: 3px;
                            margin-top: 5px;
                            /*margin: 1px;*/
                            border-radius: 5px;
                        }
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:8px;
                margin-top:8px;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            /*th, td {
                border: 2px solid #000 !important;
                padding: 0.5em 1em !important;
            }
            thead tr:first-child th:first-child {
                border-radius: 20px 0 0 0 !important;
            }
            thead tr:first-child th:last-child {
                border-radius: 0 20px 0 0 !important;
            }
            tbody tr:last-child td:first-child {
                border-radius: 0 0 0 20px !important;
            }
            tbody tr:last-child td:last-child {
                border-radius: 0 0 20px 20px !important;
            }
            tbody tr:first-child td:first-child {
                border-radius: 20px 20px 0 0 !important;
                border-bottom: 0px solid #000 !important;
            }*/
            .padd
            {
                padding:50px;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">

        <div class="row receipt_bottom_border" >
            <div class="col-md-12">
                <div class="pull-left" style="margin-bottom: 8px;">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="height: 100px; width: 100px;"/>
                </div>
     

                <div class="pull-left">
                    <strong>
                        <h4  style="font-size:18px">
                        <?php echo $contacts['company_name'];?><br/>
                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                        <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                        E-mail: <?php echo $contacts['email'];?>.<br/>
                         Tel : <?php echo $contacts['phone'];?><br/>
                     </h4>
                    </strong>
                </div>
                <div class="pull-right" style="margin-right:30px">
                    <strong>
                        <h4  style="font-size:18px">
                        	FROM: <?php echo $store_name; ?>
                      <br/>                     
                        	To: <?php echo $store; ?>
                      <br/>
                        	Date: <?php echo $orders_date; ?>
                      <br/>
                      No: <?php echo $requisition_number; ?>
                        <br/>
                     </h4>
                    </strong>
                </div>
            </div>
        </div>
        <!-- <div class="padd"> -->
            <div class="col-print-12">
                <?php echo $result;?>
            </div>
          
              <div class="pull-left">
            <div class="col-md-12">
                <div class="col-md-12" style="margin-bottom: 30px; margin-top: 20px;">
                    <div class="col-md-4 pull-left">
                        Prepared by : <?php echo $added_by; ?>
                    </div>
                    <div class="col-md-4 pull-left">
                        Signature : ......................................................
                    </div>
                    <div class="col-md-4 pull-left">
                        Date : <?php echo date('jS M Y',strtotime($created)); ?>
                    </div>
                </div>
       
                <div class="col-md-12" style="margin-bottom: 30px;">
                    <div class="col-md-4 pull-left">
                        Received by : ......................................................
                    </div>
                    <div class="col-md-4 pull-left">
                        Signature : ......................................................
                    </div>
                    <div class="col-md-4 pull-left">
                        Date : ......................................................
                    </div>
                </div>
            </div>
        </div>
        <h4><a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a></h4>
        <!-- </div> -->
       
    </body>
    
</html> 

<script type="text/javascript">
    var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>