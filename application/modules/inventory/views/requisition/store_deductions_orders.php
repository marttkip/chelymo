<?php echo $this->load->view('store_orders_search', '', TRUE);?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
         	
            	
          </div>

          <div class="clearfix"></div>
    </header>
    <div class="panel-body">


			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				$search_result ='';
				$search_result2  ='';
				if(!empty($error))
				{
					$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
						
				$search = $this->session->userdata('orders_search');
				
				if(!empty($search))
				{
					$search_result = '<a href="'.site_url().'inventory/orders/close_search_orders" class="btn btn-danger">Close Search</a>';
				}


				$result = '<div class="padd">';	
				$result .= ''.$search_result2.'';
				$result .= '
						';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<div class="row">
						<div class="col-md-12">
							<table class="example table-autosort:0 table-stripeclass:alternate table table-hover table-bordered " id="TABLE_2">
							  <thead>
								<tr>
								  <th >#</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Date Created</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Time</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Date Sent</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Requisition Number</th>

								  <th class="table-sortable:default table-sortable" title="Click to sort">Issuing Store</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Ordering Store</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Created By</th>

								  <th class="table-sortable:default table-sortable" title="Click to sort">Status</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Closed Date</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Closed By</th>
								  <th colspan="2">Actions</th>
								</tr>
							  </thead>
							  <tbody>
							';
					
								//get all administrators
								$personnel_query = $this->personnel_model->get_all_personnel();
								
								foreach ($query->result() as $row)
								{
									$requisition_id = $row->requisition_id;
									$requisition_number = $row->requisition_number;
									$requisition_date = $row->created;
									$store_id = $row->store_id;
									$order_id = $row->order_id;
									$status = $row->requisition_status;
									$modified_by = $row->created_by;
									$store_name = $row->store_name;
									$personnel_fname = $row->personnel_fname;
									$personnel_onames = $row->personnel_onames;
									$date_sent = $row->date_sent;
									$created_by = $personnel_fname.' '.$personnel_onames;

									$closed_date = $row->closed_date;
									$closed_by = $row->closed_by;
									$order_approval_status = $row->order_approval_status;
									$ordering_store_id = $row->ordering_store_id;


					                if($ordering_store_id == 6)
									{
									 $store = 'OPD Store';
									}
									else if($ordering_store_id == 9)
									{
									$store = 'IPD Store';
									}
									else if($ordering_store_id == 10)
									{
									$store = 'Catering Store';
									}
									else if($ordering_store_id == 12)
									{
									$store = 'Theatre Store ';
									}
										else if($ordering_store_id == 13)
									{
										$store = 'Nurse Store';
									}
										else if($ordering_store_id == 14)
									{
										$store = 'Laboratory Store';
									}



									if(!empty($closed_date))
									{
										$closed_date_view = '<td>'.date('Y-m-d H:i',strtotime($closed_date)).'</td>';	
									}

									else
									{
										$closed_date_view = '<td>-</td>';
									}


									if($personnel_query->num_rows() > 0)
									{
										$personnel_result = $personnel_query->result();
										
										foreach($personnel_result as $adm)
										{
											$personnel_id = $adm->personnel_id;
											
											if($personnel_id == $closed_by)
											{
												$closed = $adm->personnel_fname;
											}

										}
									}
									
									else
									{
										$closed = '-';
									}
														
									



									if(!empty($date_sent))
									{
										$sent = '<td>'.date('Y-m-d H:i',strtotime($date_sent)).'</td>';	
									}

									else
									{
										$sent = '<td>-</td>';
									}
									// var_dump($order_approval_status); die();

									// $order_details = $this->orders_model->get_order_items($order_id);
									$total_price = 0;
									$total_items = 0;
									//creators & editors
									
									
									
									$button = '';

									

									if($order_approval_status == 6 OR $order_approval_status == 0)
									{
										$add = '<td><a href="'.site_url().'award-store-requisition/'.$requisition_id.'/'.$order_id.'" class="btn btn-xs btn-success  btn-sm fa fa-folder">OPEN DETAIL</a></td>';
										$status_name = 'Waiting for disbursment';
									}
									else if($order_approval_status == 7)
									{
										$add = '<td><a href="'.site_url().'print-store-requisition/'.$requisition_id.'/'.$order_id.'" class="btn btn-xs btn-warning  btn-sm fa fa-print" target="_blank"> PRINT</a></td>
										<td><a href="'.site_url().'award-store-requisition/'.$requisition_id.'/'.$order_id.'" class="btn btn-xs btn-success  btn-sm fa fa-folder">OPEN DETAIL</a></td>';
										$status_name = 'Requisition Closed';
									}
									
									// just to mark for the next two stages
								

									$count++;
									$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.date('jS M Y H:i',strtotime($requisition_date)).'</td>
											<td>'.date('H:i',strtotime($requisition_date)).'</td>
											'.$sent.'
											<td>'.$requisition_number.'</td>
											<td>'.$store_name.'</td>
											<td>'.$store.'</td>
											<td>'.$created_by.'</td>
											<td>'.$status_name.'</td>
											'.$closed_date_view.'
											<td>'.$closed.'</td>
											'.$add.'
											
											
										</tr> 
									';
									// }
								}
					
						$result .= 
						'
								  </tbody>
								</table>
							</div>
						</div>
						';
				}
				
				else
				{
					$result .= "There are no orders";
				}
				$result .= '</div>';
				echo $result;
			?>

    </div>
</section>