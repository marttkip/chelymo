<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
            	<a href="<?php echo base_url();?>procurement/general-orders" class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> Back to orders</a>
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
            ?>
            
            <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
     		<div class="row">
     			<div class="col-md-12">

                    <input type="hidden" name="store_id" value="5">
     				 <!-- brand Name -->
                     
                    <div class="col-md-9">
			            <div class="form-group">
			                <label class="col-lg-3 control-label">Order Instructions</label>
			                <div class="col-lg-9">
			                	<textarea class="form-control" name="order_instructions" rows="5" required><?php echo set_value('order_instructions');?></textarea>
			                </div>
			            </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-actions center-align">
                            <button class="submit btn btn-primary btn-sm" type="submit" onclick="return confirm('Are you sure you want to add this new procurement order ?')">
                                Create New Order
                            </button>
                        </div>
                    </div>
     			</div>
     		</div>
     		
            <br />
            <?php echo form_close();?>
    </div>
</section>