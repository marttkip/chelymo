<?php echo $this->load->view('requisition_search', '', TRUE);?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
         	<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#orders_modal">
            	Create New Store Requisition
            </button>
            	
          </div>

         
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">


                            
        <div class="modal fade" id="orders_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create New Requisition</h4>
                    </div>
                    <?php echo form_open('inventory/requisition/create_supplier_requisition', array('class' => 'form-horizontal'));?>
                        	
                    <div class="modal-body">
                    	
                    	
                        	<div class="form-group">
                                <label for="exampleInputEmail1" class="col-md-5">Receiving Store</label>
                                <div class="col-md-7">
                                	<select name="store_id" id="store_id" class="form-control" required="required">
                                    <option value="0">--Select Store--</option>
                                	 <?php
										if($store_priviledges->num_rows() > 0)
										{
											foreach ($store_priviledges->result() as $key)
											{
												# code...
												$store_parent = $key->store_parent;
												$store_id = $key->store_id;
												$store_name = $key->store_name;
												
												?>
												<option value="<?php echo $store_id;?>"><?php echo $store_name;?></option>
												<?php
											}
										}
									?>
                                    </select>
                                </div>
                            </div>  
                           
                          <!--   <div class="form-group">
                            	<div class="col-sm-offset-2 col-sm-10">
                            		
                                </div>
                            </div> -->
                      
                   

                    </div>
                    <div class="modal-footer">
                    	<button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to create this requisition ? ')">Create Requisition</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                      <?php echo form_close();?>
                </div>
            </div>
        </div>
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				$search_result ='';
				$search_result2  ='';
				if(!empty($error))
				{
					$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
						
				$search = $this->session->userdata('orders_search');
				
				if(!empty($search))
				{
					$search_result = '<a href="'.site_url().'inventory/orders/close_search_orders" class="btn btn-danger">Close Search</a>';
				}


				$result = '<div class="padd">';	
				$result .= ''.$search_result2.'';
				$result .= '
						';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<div class="row">
						<div class="col-md-12">
							<table class="example table-autosort:0 table-stripeclass:alternate table table-hover table-bordered " id="TABLE_2">
							  <thead>
								<tr>
								  <th >#</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">TIME</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">DATE SENT</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">REQUISITION NUMBER</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">ORDERING STORE</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">CREATED BY</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">PROCUREMENT STATUS</th>
								  <th colspan="2"></th>
								</tr>
							  </thead>
							  <tbody>
							';
							
								//get all administrators
								$personnel_query = $this->personnel_model->get_all_personnel();
								
								foreach ($query->result() as $row)
								{
									$requisition_id = $row->requisition_id;
									$requisition_number = $row->requisition_number;
									$requisition_date = $row->requisition_date;
									$store_id = $row->store_id;
									$status = $row->requisition_status;
									$modified_by = $row->created_by;
									$personnel_created = $row->personnel_created;
									$store_name = $row->store_name;
									$personnel_fname = $row->personnel_fname;
									$personnel_onames = $row->personnel_onames;
									$date_sent = $row->date_sent;
									
									$created_by = $personnel_fname.' '.$personnel_onames;

									if(!empty($date_sent))
									{
										$sent = '<td>'.date('Y-m-d H:i',strtotime($date_sent)).'</td>';	
									}

									else
									{
										$sent = '<td>-</td>';
									}

									$total_price = 0;
									$total_items = 0;
									
									$button = '';

									$next_order_status = $status;

									$personnel_id = $this->session->userdata('personnel_id');


									$status_name = $this->orders_model->get_next_approval_status_name($next_order_status);
									$check_level_approval = FALSE;
									if($personnel_id > 0)
									{
										$next_order_status++;
										$check_level_approval = $this->orders_model->check_assigned_next_approval($next_order_status);
									}


									$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

									if($status == 0 AND ($authorize_invoice_changes OR $check_level_approval))
									{

										
										$add = '<td><a href="'.site_url().'view-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-folder">ADD REQUISITION ITEMS</a></td>';
										
										
										$status_name = 'DEPARTMENT ORDER';
									}
									else if($status == 1  AND ($authorize_invoice_changes OR $check_level_approval))
									{
										$add = '<td><a href="'.site_url().'print-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-info  btn-sm fa fa-print" target="_blank"> PRINT REQUISITION FORM</a></td>';
										$add .= '<td><a href="'.site_url().'open-supplier-view/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-pencil"> VIEW REQUISITION</a></td>';
										$status_name = 'PROCUREMENT OFFFICE';
									}
									else if($status == 2  AND ($authorize_invoice_changes OR $check_level_approval))
									{
										$add = '<td><a href="'.site_url().'print-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-info  btn-sm fa fa-print" target="_blank"> PRINT REQUISITION FORM</a></td>';
										// $add = '<td><a href="'.site_url().'view-orders-view/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-pencil"> VIEW ORDERS</a></td>';
										$add = '';
										$status_name = 'COMPLETED';
									}
									else
									{
										$add = '<td><a href="'.site_url().'print-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-info  btn-sm fa fa-print" target="_blank"> PRINT REQUISITION FORM</a></td>';
										// $add = '<td><a href="'.site_url().'print-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-info  btn-sm fa fa-print" target="_blank"> PRINT REQUISITION FORM</a></td>';
										$add = '';
										$status_name = 'COMPLETED';
									}
									// just to mark for the next two stages
								

									$count++;
									$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.date('H:i',strtotime($date_sent)).'</td>
											'.$sent.'
											<td>'.$requisition_number.'</td>
											<td>'.$store_name.'</td>
											<td>'.$created_by.'</td>
											<td>'.$status_name.'</td>
											
											'.$add.'
											
											
										</tr> 
									';
									// }
								}
					
						$result .= 
						'
								  </tbody>
								</table>
							</div>
						</div>
						';
				}
				
				else
				{
					$result .= "There are no orders";
				}
				$result .= '</div>';
				echo $result;
			?>

    </div>
</section>