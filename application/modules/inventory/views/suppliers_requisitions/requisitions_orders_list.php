<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>LPO NUMBER </th>
					    <th>DATE</th>
						<th>VENDOR</th>
						<th>CREATED BY</th>
						<th>APPROVED BY</th>
						<th>REQUISITION NUMBER </th>
                        <th>STATUS</th>
					</tr>
				</thead>
				 <tbody>
				  
			';
			
			
			
			foreach ($query->result() as $key)
			{
				    $creditor_name = $key->creditor_name;
				    $creditor_id = $key->creditor_id;
	                $requisition_number = $key->requisition_number;
	                $created_by = $key->created_by;
	                $approved_by = $key->approved_by;
	                $creditor_id = $key->creditor_id;
	                $requisition_id = $key->requisition_id;
	                $order_id = $key->order_id;
	                $order_approval_status = $key->order_approval_status;
	                $requisition_date = $key->requisition_date;
	                $lpo_number = $key->lpo_number;
	                $order_number = $key->order_number;
	                $billing_status = $key->billing_status;
	                $requisition_status = $key->requisition_status;


	                $created = '';
					if($created_by)
					{
					$created = $this->requisition_model->get_personnel_name($created_by);
					}
					// $approved_by = '';
					// if($lpo_generated_by)
					// {
					// 	// $approved_by = $this->requisition_model->get_personnel_name($lpo_generated_by);
					// }

					$approvee = '';
					if($approved_by)
					{
					$approvee = $this->requisition_model->get_personnel_name($approved_by);
					}

					$authorise = '';
					if($authorised_by)
					{
					$authorise = $this->requisition_model->get_personnel_name($authorised_by);
					}

                     if($requisition_status == 1)
	                {
	                	$status = 'Reversed';
	                	$color_code = '';
	                }
	                else if($requisition_status >= 1)
	                {
	                	$status = 'Completed';
	                	$color_code = 'warning';
	                }



	             

	                if($order_approval_status == 3)
	                {
	                	$billing_status = 'Waiting for LPO Generation';
	                	$color_code = '';
	                }
	                else if($order_approval_status == 4)
	                {
	                	$billing_status = 'Waiting for LPO Approval';
	                	$color_code = 'warning';
	                }
	                else if($order_approval_status == 5)
	                {

	                	if($billing_status == 1)
	                	{
	                		$billing_status = 'Authorised';
	                		$color_code = 'info';
	                	}
	                	else
	                	{
	                		$billing_status = 'Waiting For Authorisation';
	                		$color_code = 'danger';
	                	}
	                	
	                }
	                else if($order_approval_status == 6)
	                {
	                	$billing_status = 'Waiting for Delivery';
	                	$color_code = 'primary';
	                }
	                else if($order_approval_status == 7){
	                	$billing_status = 'Delivered';
	                	$color_code = 'success';
	                }

	                
				
				$count++;
				$result .= 
							'

								<tr onclick="view_invoice_details('.$requisition_id.','.$order_id.','.$creditor_id.')">
									<td class="'.$color_code.'">'.$count.' </td>
									<td class="'.$color_code.'">'.$lpo_number.'</td>
									<td class="'.$color_code.'">'.date('jS M Y',strtotime($requisition_date)).'</td>
									<td class="'.$color_code.'">'.$creditor_name.'</td>
									<td class="'.$color_code.'">'.$created.'</td>
									<td class="'.$color_code.'">'.$approvee.'</td>
									<td class="'.$color_code.'">'.$requisition_number.'</td>
									<td class="'.$color_code.'">'.$billing_status.'</td>
									<td class="'.$color_code.'">'.$status.'</td>

									<td>
								
								   <a href="'.site_url().'procurement/reversal-requisition/'.$requisition_id.'" class="btn btn-sm btn-success"<i class="fa fa-trash"> Reversal</i></a>
							       </td>

							      <td> 	<a href="'.site_url().'procurement/delete-requisition/'.$requisition_id.'" class="btn btn-sm btn-danger"<i class="fa fa-trash"> Delete</i></a> </td>
								</tr> 
							';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There Are No Invoices";
		}

		echo $result;
?>