
<div class="row">
	<div class="col-md-12">
		<div class="col-md-7">
			<section class="panel panel-success">
			        <div class="panel-body">
						<?php
						$this->session->set_userdata('supplier_invoice_search');
						echo form_open("accounting/creditors/search_suppler_invoices", array("class" => "form-horizontal","id"=>"supplier-form"));
			            ?>
			            <!-- <input type="hidden" id="requisition_id" value="<?php echo $requisition_id?>" name="requisition_id"> -->
			            <div class="row">
			                <div class="col-md-6">
			                    <div class="form-group">
			                        <label class="col-md-2 control-label">Supplier: </label>
			                        
			                        <div class="col-md-10">
			                            <select class="form-control  custom-select" name="creditor_id" id="creditor_id">
			                            	<option value="">---Select Supplier---</option>
			                                <?php

			                                	$all_creditors = $this->creditors_model->get_active_creditors();

			                                	if($all_creditors->num_rows() > 0)
			                                	{
			                                		foreach ($all_creditors->result() as $key => $value) {
			                                			// code...

			                                			$creditor_name = $value->creditor_name;
			                                			$creditor_id = $value->creditor_id;
			                                			$creditor_name = $value->creditor_name;
			                                		?><option value="<?php echo $creditor_id; ?>" ><?php echo $creditor_name ?></option>
			                                        <?php	
			                                		}
			                                	}
			                                   
			                                ?>
			                            </select>
			                        </div>
			                    </div>
			                    
			                    <div class="form-group">
			                        <label class="col-md-2 control-label">Requisition N.: </label>
			                        
			                        <div class="col-md-10">
			                            <input type="text" class="form-control" name="invoice_number" placeholder="Requisition Number">
			                        </div>
			                    </div>
			                    
			                    <div class="form-group">
			                    	<div class="col-md-2">
			                    		Status
			                    	</div>
			                        <div class="col-md-2">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="order_approval_status" value="0"  checked>
			                                    ALL
			                                </label>
			                            </div>
			                        </div>
			                        
			                        <div class="col-md-2">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="order_approval_status" value="5" >
			                                    Approved LPO's
			                                </label>
			                            </div>
			                        </div>
			                        <div class="col-md-2">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="order_approval_status" value="6" >
			                                    Authorised LPO's
			                                </label>
			                            </div>
			                        </div>
			                        <div class="col-md-2">
			                            <div class="radio">
			                                <label>
			                                    <input id="optionsRadios2" type="radio" name="order_approval_status" value="7" >
			                                   	Delivered
			                                </label>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="col-md-6">
			                                
			                    <div class="form-group">
	                                <label class="col-md-2 control-label">From: </label>
	                                <div class="col-md-10">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-calendar"></i>
	                                        </span>
	                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From">
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-md-2 control-label">To: </label>
	                                <div class="col-md-10">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="fa fa-calendar"></i>
	                                        </span>
	                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To">
	                                    </div>
	                                </div>
	                            </div>
	                             <div class="form-group">
			                        <label class="col-md-2 control-label">LPO NO.: </label>
			                        
			                        <div class="col-md-10">
			                            <input type="text" class="form-control" name="lpo_number" placeholder="LPO Number">
			                        </div>
			                    </div>
	                            <!-- </div> -->

	                            <div class="form-group">
		                            <div class="col-md-6 col-md-offset-3">
		                                <div class="center-align">
		                                    <button type="submit" class="btn btn-info">Search</button>
		                                </div>
		                            </div>
		                        </div>
			                </div>
			            </div>
			            <?php
			            echo form_close();
			            ?>
			    	</div>
			</section>

			<div class="panel-body" style="height:60vh !important;overflow-y:scroll;">
				<div id="invoices-list"></div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel-body" style="margin-bottom:5px !important;" >
				<div id="invoice-header"></div>
			</div>
		
			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				<div id="invoice-detail"></div>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">

	$(document).ready(function(){
  	
		$("#creditor_id").customselect();
      	get_all_orders();
      	// get_selected_list();
	 });

	function search_LPO_number() {
	  // Declare variables
	  var input, filter, table, tr, td, i, txtValue;
	  input = document.getElementById("lpo_number");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("requisition_orders_list");
	  tr = table.getElementsByTagName("tr");

	  // Loop through all table rows, and hide those who don't match the search query
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[3];
	    if (td) {
	      txtValue = td.textContent || td.innerText;
	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    }
	  }
	}


	function get_all_orders()
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/get_all_orders";
			// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);
			    	// alert(data);
			  if(data.message == 'success')	 
			  {

			  		$('#invoices-list').html(data.results);
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


  	function view_invoice_details(requisition_id,order_id,creditor_id)
  	{
  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"inventory/requisition/view_requisition_details/"+requisition_id+"/"+order_id+"/"+creditor_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#invoice-detail').html(data.results);
		  	$('#invoice-header').html(data.header);

		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function generate_lpo(order_id,requisition_id,creditor_id)
  	{

  		var res = confirm('Are you sure you want to generate this LPO ?');


  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"inventory/requisition/generate_lpo/"+order_id+"/"+requisition_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);


			   get_all_orders();

			  	view_invoice_details(requisition_id,order_id,creditor_id);
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

  	function approve_lpo_financials(order_id,requisition_id,creditor_id)
  	{
  		open_sidebar();
  		



  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"auth/login/authenticate_user/0";
		window.localStorage.setItem('order_id',order_id);
		window.localStorage.setItem('requisition_id',requisition_id);
		window.localStorage.setItem('creditor_id',creditor_id);

		$.ajax({
		type:'POST',
		url: url,
		data:{order_id: order_id,requisition_id:requisition_id,creditor_id:creditor_id},
		dataType: 'text',
		success:function(data){
		  


		    document.getElementById("current-sidebar-div").style.display = "block"; 
   			$("#current-sidebar-div").html(data);
		 
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		
  	}



	$(document).on("submit","form#confirm-authenitcation",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);

		var res = confirm('Are you sure you want authenticate this action ?');

		var order_id = window.localStorage.getItem('order_id');
		var requisition_id = window.localStorage.getItem('requisition_id');
		var creditor_id = window.localStorage.getItem('creditor_id');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"inventory/requisition/approve_lpo_financials/"+order_id+"/"+requisition_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
	       	contentType: false,
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
				{
	    			close_side_bar();
					get_all_orders();

			  		view_invoice_details(requisition_id,order_id,creditor_id);
				}
				else
				{
					alert(data.result);
				}
			 
			  	  

			  	// window.location.hre
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
		
		
		
	   
		
	});




  	function approve_lpo_financials_submit(order_id,requisition_id,creditor_id)
  	{

  		var res = confirm('Are you sure you want to send to next level ?');


  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"inventory/requisition/approve_lpo_financials/"+order_id+"/"+requisition_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			 
			  	  get_all_orders();

			  	view_invoice_details(requisition_id,order_id,creditor_id);

			  	// window.location.hre
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

  	function delete_procedure(id, requisition_id){
	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"requisition/delete_charged_item/"+id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {


	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
      // display_patient_bill(visit_id);
       get_visit_procedures_done(requisition_id);
       
	}


  	function authorise_lpo(order_id,requisition_id,creditor_id)
  	{
  		open_sidebar();
  		



  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"auth/login/authenticate_user/1";
		window.localStorage.setItem('order_id',order_id);
		window.localStorage.setItem('requisition_id',requisition_id);
		window.localStorage.setItem('creditor_id',creditor_id);

		$.ajax({
		type:'POST',
		url: url,
		data:{order_id: order_id,requisition_id:requisition_id,creditor_id:creditor_id},
		dataType: 'text',
		success:function(data){
		  


		    document.getElementById("current-sidebar-div").style.display = "block"; 
   			$("#current-sidebar-div").html(data);
		 
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		
  	}

  	$(document).on("submit","form#confirm-authenitcation-authorise",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);

		var res = confirm('Are you sure you want authenticate this action ?');

		var order_id = window.localStorage.getItem('order_id');
		var requisition_id = window.localStorage.getItem('requisition_id');
		var creditor_id = window.localStorage.getItem('creditor_id');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"inventory/requisition/authorise_lpo/"+order_id+"/"+requisition_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:form_data,
			dataType: 'text',
			processData: false,
	       	contentType: false,
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
				{
	    			close_side_bar();
					 get_all_orders();

			  		view_invoice_details(requisition_id,order_id,creditor_id);
				}
				else
				{
					alert(data.result);
				}
			 
			  	  

			  	// window.location.hre
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
		
		
		
	   
		
	});

	function authorise_lpo_old(order_id,requisition_id,creditor_id)
  	{

  		var res = confirm('Are you sure you want to authorise this LPO ?');


  		if(res)
  		{



	  		var config_url = $('#config_url').val();

	  		// alert(config_url);
		 	var url = config_url+"inventory/requisition/authorise_lpo/"+order_id+"/"+requisition_id+"/"+creditor_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			 
			  	  get_all_orders();

			  	view_invoice_details(requisition_id,order_id,creditor_id);

			  	// window.location.hre
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function decline_invoice(invoice_id,invoice_type_id,creditor_id)
  	{

  		var config_url = $('#config_url').val();

  		// alert(config_url);
	 	var url = config_url+"accounting/creditors/decline_invoice/"+invoice_id+"/"+invoice_type_id+"/"+creditor_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	get_all_invoices();
		  }
		   

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function add_to_list(product_id,requisition_id)
  	{
  		var res = confirm('Are you sure you want to add this product to the list ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_item_to_list/"+product_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});

  		}
  		get_selected_list(requisition_id);
  		
  	}

  	function update_item(requisition_item_id,requisition_id)
  	{


  		var config_url = $('#config_url').val();
  		var units = $('#units'+requisition_item_id).val();
	 	var url = config_url+"inventory/requisition/update_items/"+requisition_item_id+"/"+requisition_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{units: units},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 
		  get_selected_list(requisition_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


	function remove_items(requisition_item_id,requisition_id)
  	{

  		var res = confirm('Are you sure you want to remove this product to the list ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/delete_requisition_item/"+requisition_item_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function confirm_requisition(requisition_id)
  	{

  		var res = confirm('Are you sure you want to confirm this request of requisition ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/confirm_requisition/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	window.location.href = 	config_url+'procurement/requisitions';
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

	function get_all_req_items(requisition_id=0)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/product_list/"+requisition_id;
			 var name = $('#requisition_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, drug : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#products-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}

	  function get_all_prod_items(requisition_id=0)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/selected_products/"+requisition_id;
			var name = $('#product_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, product_name : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


$(document).on("submit","form#supplier-form",function(e)
{
	e.preventDefault();
	var form_data = new FormData(this);
	var config_url = $('#config_url').val();	
	var requisition_id = $('#requisition_id').val();	
	var url = config_url+"inventory/requisition/get_all_orders/"+requisition_id;
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          var data = jQuery.parseJSON(data);
          if(data.message == 'success')	 
		  {
		  		$('#invoices-list').html(data.results);
		  }
		  else
		  {
		  	// alert(data.result);
		  }
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       }
       });
});
</script>
