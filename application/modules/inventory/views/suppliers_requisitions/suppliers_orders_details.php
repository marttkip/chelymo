
<div class="row">
	<div class="col-md-12">
		<div class="col-md-4">
			<div class="col-md-12">
		
				<div class="col-md-8">
					<input type="text" class="form-control" id="requisition_list" onkeyup="get_all_req_items(<?php echo $requisition_id ?>,<?php echo $store_id ?>)" > 
				</div>
				<div class="col-md-4">
					<a   class="btn btn-sm btn-success" /> Add New Item</a>
				</div>
			</div>
			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				<div id="selected-list"></div>
			</div>
		</div>
		<div class="col-md-2">
			<input type="text" class="form-control" id="supplier_search" onkeyup="search_supplier()" > 
			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				<div id="stores-list"></div>
			</div>
		</div>
		<div class="col-md-6">
			<input type="text" class="form-control" id="product_list" onkeyup="get_all_prod_items(<?php echo $requisition_id ?>,<?php echo $store_id ?>)" > 

			<div class="panel-body" style="height:75vh !important;overflow-y:scroll;margin-bottom: 10px;">
				<div id="orders-list-items"></div>
			</div>
			<div class="center-align">
				<a  href="<?php echo site_url()?>procurement/correct-requisition/<?php echo $requisition_id?>" class="btn btn-sm btn-info" onclick="return confirm('Are you sure you want to make corection on  this requisition order ')"/> Send Back for Correction</a>
				<a  href="<?php echo site_url()?>complete-procurement-requisition/<?php echo $requisition_id?>/<?php echo $store_id?>" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to complete this requisition order. Note that once you complete this you will not be able to make adjustments ? ')"/> Send For LPO Approval</a>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	$(document).ready(function(){
      	get_all_products(<?php echo $requisition_id?>,<?php echo $store_id?>);
      	get_selected_list(<?php echo $requisition_id?>,<?php echo $store_id?>);
      	get_orders_list(<?php echo $requisition_id?>,<?php echo $store_id?>);
	 });

	// search_supplier
	function search_supplier() {
	  // Declare variables
	  var input, filter, table, tr, td, i, txtValue;
	  input = document.getElementById("supplier_search");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("suppliers_list");
	  tr = table.getElementsByTagName("tr");

	  // Loop through all table rows, and hide those who don't match the search query
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[0];
	    if (td) {
	      txtValue = td.textContent || td.innerText;
	      if (txtValue.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    }
	  }
	}
	
	function get_all_products(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/supplier_product_list/"+requisition_id+"/"+store_id;
			// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);
			    	// alert(data);
			  if(data.message == 'success')	 
			  {

			  		$('#products-list').html(data.results);
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


  	function get_selected_list(requisition_id=0,store_id)
  	{
  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/requisition_items_to_supply/"+requisition_id+"/"+store_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#selected-list').html(data.results);
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function add_to_list(product_id,requisition_id)
  	{
  		var res = confirm('Are you sure you want to add this product to the list ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_item_to_list/"+product_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});

  		}
  		get_selected_list(requisition_id);
  		
  	}

  	function add_supplier(requisition_id,store_id,order_item_id)
  	{

  		var config_url = $('#config_url').val();
  		var units = $('#units'+order_item_id).val();
  		
	 	var url = config_url+"inventory/requisition/update_supplier_order_items/"+order_item_id+"/"+requisition_id;
		// alert(url);
		$.ajax({
		type:'POST',
		url: url,
		data:{units: units},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 
		  get_selected_list(requisition_id,store_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


	function remove_items(order_item_id,requisition_id,store_id)
  	{

  		var res = confirm('Are you sure you want to remove this product to the list ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/delete_item_to_supply/"+order_item_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id,store_id);
			  get_orders_list(requisition_id,store_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function confirm_requisition(requisition_id)
  	{

  		var res = confirm('Are you sure you want to confirm this request of requisition ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/confirm_requisition/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	window.location.href = 	config_url+'procurement/requisitions';
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

	function get_all_req_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/requisition_items_to_supply/"+requisition_id+"/"+store_id;
			 var name = $('#requisition_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, drug : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}

	  function get_all_prod_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/requisition_items/"+requisition_id+"/"+store_id;
			var name = $('#product_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, product_name : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}
  	function check_other_stores(requisition_id,store_id,order_item_id)
  	{

  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/suppliers_list/"+order_item_id+"/"+requisition_id+"/"+store_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);



			if(data.message == 'success')	 
			{

				$('#stores-list').html(data.results);
				
				
			}
			else
			{
				// alert(data.result);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

		window.localStorage.setItem('requisition_id',requisition_id);
  		window.localStorage.setItem('store_id',store_id);
  		window.localStorage.setItem('order_item_id',order_item_id);

  	}

  	function supplier_search()
  	{

  		var requisition_id = window.localStorage.getItem('requisition_id');
  		var store_id = window.localStorage.getItem('store_id');
  		var order_item_id = window.localStorage.getItem('order_item_id');

  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/suppliers_list/"+order_item_id+"/"+requisition_id+"/"+store_id;
	 	var supplier_search = $('#supplier_search').val();
		$.ajax({
		type:'POST',
		url: url,
		data:{supplier_search: supplier_search},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);

			if(data.message == 'success')	 
			{

				$('#stores-list').html(data.results);
				
				
			}
			else
			{
				// alert(data.result);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


  	function create_suppliers_order(product_id,requisition_id,ordering_store_id)
  	{

  		var res = confirm('Are you sure you want to add to your requisition ? ');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_requisition_supplier_orders/"+product_id+"/"+requisition_id+"/"+ordering_store_id;

			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					// $('#selected-list').html(data.results);
					get_selected_list(requisition_id,ordering_store_id);
			  		get_orders_list(requisition_id,ordering_store_id);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}

  	}

  	function get_orders_list(requisition_id=0,store_id)
  	{
  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/view_items_to_supply/"+requisition_id+"/"+store_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#orders-list-items').html(data.results);
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function add_item_to_supplier(product_id,requisition_id,ordering_store_id,supplier_id,order_item_id)
  	{

  		var res = confirm('Are you sure you want to add to your requisition ? ');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_requisition_supplier_orders_to_supplier/"+product_id+"/"+requisition_id+"/"+ordering_store_id+"/"+supplier_id+"/"+order_item_id;

			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					// $('#selected-list').html(data.results);
					get_orders_list(requisition_id,ordering_store_id);
					get_selected_list(requisition_id,ordering_store_id);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}

  	}

  	function update_item_amount(order_item_id,requisition_id,store_id)
  	{
  		var config_url = $('#config_url').val();
  		var units = $('#units'+order_item_id).val();
  		var discount = $('#discount'+order_item_id).val();
  		var vat = $('#vat'+order_item_id).val();
  		var supplier_price = $('#supplier_price'+order_item_id).val();
  		
	 	var url = config_url+"inventory/requisition/update_ordered_item/"+order_item_id+"/"+requisition_id+"/"+store_id;
		// alert(url);
		$.ajax({
		type:'POST',
		url: url,
		data:{units: units,supplier_price:supplier_price,discount:discount,vat:vat},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 
		  get_orders_list(requisition_id,store_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

	  
	

</script>
