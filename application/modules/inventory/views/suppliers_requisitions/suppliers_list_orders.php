
<div class="row">
	<div class="col-md-12">
		<div class="col-md-3">

			<input type="text" class="form-control" id="requisition_list" onkeyup="get_all_req_items(<?php echo $requisition_id ?>,<?php echo $store_id ?>)" > 

			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				
				<div id="products-list"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				<div id="stores-list"></div>
			</div>
		</div>
		<div class="col-md-6">
			<input type="text" class="form-control" id="product_list" onkeyup="get_all_prod_items(<?php echo $requisition_id ?>,<?php echo $store_id ?>)" > 

			<div class="panel-body" style="height:75vh !important;overflow-y:scroll;margin-bottom: 10px;">
				<div id="selected-list"></div>
			</div>
			<div class="center-align">
				<a  href="<?php echo site_url()?>complete-requisition/<?php echo $requisition_id?>/<?php echo $store_id?>" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to complete this requisition order. Note that once you complete this you will not be able to make adjustments ? ')"/> Complete store requisiton</a>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	$(document).ready(function(){
  	

      	get_all_products(<?php echo $requisition_id?>,<?php echo $store_id?>);
      	get_selected_list(<?php echo $requisition_id?>,<?php echo $store_id?>);
	 });


	function get_all_products(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/supplier_product_list/"+requisition_id+"/"+store_id;
			// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);
			    	// alert(data);
			  if(data.message == 'success')	 
			  {

			  		$('#products-list').html(data.results);
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


  	function get_selected_list(requisition_id=0,store_id)
  	{
  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/supplier_requisition_items/"+requisition_id+"/"+store_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#selected-list').html(data.results);
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function add_to_list(product_id,requisition_id)
  	{
  		var res = confirm('Are you sure you want to add this product to the list ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_item_to_list/"+product_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});

  		}
  		get_selected_list(requisition_id);
  		
  	}

  	function update_item(product_deductions_id,requisition_id,store_id)
  	{


  		var config_url = $('#config_url').val();
  		var units = $('#units'+product_deductions_id).val();
	 	var url = config_url+"inventory/requisition/update_store_order_items/"+product_deductions_id+"/"+requisition_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{units: units},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 
		  get_selected_list(requisition_id,store_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


	function remove_items(product_deductions_id,requisition_id,store_id)
  	{

  		var res = confirm('Are you sure you want to remove this product to the list ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/delete_deduction_item/"+product_deductions_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id,store_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function confirm_requisition(requisition_id)
  	{

  		var res = confirm('Are you sure you want to confirm this request of requisition ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/confirm_requisition/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	window.location.href = 	config_url+'procurement/requisitions';
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

	function get_all_req_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/supplier_product_list/"+requisition_id+"/"+store_id;
			 var name = $('#requisition_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, drug : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#products-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}

	  function get_all_prod_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/requisition_items/"+requisition_id+"/"+store_id;
			var name = $('#product_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, product_name : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}
  	function check_other_stores(product_id,requisition_id,store_id)
  	{

  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/suppliers_list/"+product_id+"/"+requisition_id+"/"+store_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);

			if(data.message == 'success')	 
			{

				$('#stores-list').html(data.results);
				
				
			}
			else
			{
				// alert(data.result);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

  	}


  	function create_suppliers_order(product_id,requisition_id,ordering_store_id)
  	{

  		var res = confirm('Are you sure you want to add to your requisition ? ');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_requisition_supplier_orders/"+product_id+"/"+requisition_id+"/"+ordering_store_id;

			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					// $('#selected-list').html(data.results);
					get_selected_list(requisition_id,ordering_store_id);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}

  	}
	  
	

</script>
