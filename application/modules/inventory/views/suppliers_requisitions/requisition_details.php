
<div class="row">
	<div class="col-md-12">
		<div class="col-md-4">

			<input type="text" class="form-control" id="requisition_list" onkeyup="get_all_req_items(<?php echo $requisition_id ?>,<?php echo $store_id ?>)" > 

			<div class="panel-body" style="height:80vh !important;overflow-y:scroll;">
				
				<div id="products-list"></div>
			</div>
		</div>
		
		<div class="col-md-8">
			<input type="text" class="form-control" id="product_list" onkeyup="get_all_prod_items(<?php echo $requisition_id ?>,<?php echo $store_id ?>)" > 

			<div class="panel-body" style="height:75vh !important;overflow-y:scroll;margin-bottom: 10px;">
				<div id="selected-list"></div>
			</div>
			<div class="center-align">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#orders_modal">
            	Create Store Requisition
            </button>
			</div>
		</div>
	</div>
	
</div>


 <div class="modal fade" id="orders_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Complete Store Requisition</h4>
            </div>
            <?php echo form_open(site_url().'complete-store-requisition/'.$requisition_id.'/'.$store_id, array('class' => 'form-horizontal'));?>
                	
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-3">
            		
            		</div>
            		<div class="col-md-6">

	                	<div class="form-group">
	                        <label for="exampleInputEmail1" class="col-md-12">Reason why the items are required</label>
	                        <div class="col-md-12">
	                        	<textarea class="form-control" name="reason" id="reason" rows="6" required> </textarea>
	                        </div>
	                    </div>
            		
            		</div>
            		<div class="col-md-3">
            		
            		</div>
            	</div>
            	  

            </div>
            <div class="modal-footer">
            	
            	<button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to complete this requisition order. Note that once you complete this you will not be able to make adjustments ? ')">Complete Requisition</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
  	

      	get_all_products(<?php echo $requisition_id?>,<?php echo $store_id?>);
      	get_selected_list(<?php echo $requisition_id?>,<?php echo $store_id?>);
	 });


	function get_all_products(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/supplier_product_list/"+requisition_id+"/"+store_id;
			// alert(url);
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);
			    	// alert(data);
			  if(data.message == 'success')	 
			  {

			  		$('#products-list').html(data.results);
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}


  	function get_selected_list(requisition_id=0,store_id)
  	{
  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/supplier_requisition_items/"+requisition_id+"/"+store_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	$('#selected-list').html(data.results);
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}

  	function add_to_list(product_id,requisition_id)
  	{
  		var res = confirm('Are you sure you want to add this product to the list ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_item_to_list/"+product_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});

  		}
  		get_selected_list(requisition_id);
  		
  	}

  	function update_item_checked(order_item_id,requisition_id,store_id)
  	{

  		var config_url = $('#config_url').val();
  		var units = $('#units'+order_item_id).val();
  		var order_item_instruction = $('#order_item_instruction'+order_item_id).val();
  		var current_stock = $('#current_stock'+order_item_id).val();

  		
	 	var url = config_url+"inventory/requisition/update_supplier_order_items/"+order_item_id+"/"+requisition_id;
		// alert(url);
		$.ajax({
		type:'POST',
		url: url,
		data:{units: units,order_item_instruction:order_item_instruction,current_stock:current_stock},
		dataType: 'text',
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == 'success')	 
		  {
		  	
		  }
		  else
		  {
		  	// alert(data.result);
		  }
		 
		  get_selected_list(requisition_id,store_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
  	}


	function remove_items(order_item_id,requisition_id,store_id)
  	{

  		var res = confirm('Are you sure you want to remove this product to the list ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/delete_order_item/"+order_item_id+"/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  get_selected_list(requisition_id,store_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}


  	function confirm_requisition(requisition_id)
  	{

  		var res = confirm('Are you sure you want to confirm this request of requisition ?');

  		if(res)
  		{

	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/confirm_requisition/"+requisition_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{units: 0},
			dataType: 'text',
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  if(data.message == 'success')	 
			  {
			  	window.location.href = 	config_url+'procurement/requisitions';
			  }
			  else
			  {
			  	// alert(data.result);
			  }
			 
			  
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}
  	}

	function get_all_req_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/supplier_product_list/"+requisition_id+"/"+store_id;
			 var name = $('#requisition_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, drug : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#products-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}

	  function get_all_prod_items(requisition_id=0,store_id=5)
  	{
  			var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/supplier_requisition_items/"+requisition_id+"/"+store_id;
			var name = $('#product_list').val();
			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0, product_name : name},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					$('#selected-list').html(data.results);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
  	}
  	function check_other_stores(product_id,requisition_id,store_id)
  	{

  		var config_url = $('#config_url').val();
	 	var url = config_url+"inventory/requisition/suppliers_list/"+product_id+"/"+requisition_id+"/"+store_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{list_type: 0},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);

			if(data.message == 'success')	 
			{

				$('#stores-list').html(data.results);
				
				
			}
			else
			{
				// alert(data.result);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

  	}


  	function create_suppliers_order(product_id,requisition_id,ordering_store_id)
  	{

  		var res = confirm('Are you sure you want to add to your requisition ? ');

  		if(res)
  		{



	  		var config_url = $('#config_url').val();
		 	var url = config_url+"inventory/requisition/add_requisition_supplier_orders/"+product_id+"/"+requisition_id+"/"+ordering_store_id;

			$.ajax({
			type:'POST',
			url: url,
			data:{list_type: 0},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.message == 'success')	 
				{

					// $('#selected-list').html(data.results);
					get_selected_list(requisition_id,ordering_store_id);
					
					
				}
				else
				{
					// alert(data.result);
				}
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

			}
			});
		}

  	}
	  
	

</script>
