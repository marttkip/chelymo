<?php echo $this->load->view('procurement_search', '', TRUE);?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
         	
            	
          </div>

         
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">

			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				$search_result ='';
				$search_result2  ='';
				if(!empty($error))
				{
					$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
						
				$search = $this->session->userdata('orders_search');
				
				if(!empty($search))
				{
					$search_result = '<a href="'.site_url().'inventory/orders/close_search_orders" class="btn btn-danger">Close Search</a>';
				}


				$result = '<div class="padd">';	
				$result .= ''.$search_result2.'';
				$result .= '
						';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<div class="row">
						<div class="col-md-12">
							<table class="example table-autosort:0 table-stripeclass:alternate table table-hover table-bordered " id="TABLE_2">
							  <thead>
								<tr>
								  <th >#</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">DATE</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">TIME</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">DATE SENT</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">REQUISITION NUMBER</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">ORDERING STORE</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">CREATED BY</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">PROCUREMENT STATUS</th>
								  <th colspan="2">Actions</th>
								</tr>
							  </thead>
							  <tbody>
							';
							
								//get all administrators
								$personnel_query = $this->personnel_model->get_all_personnel();
								
								foreach ($query->result() as $row)
								{
									$requisition_id = $row->requisition_id;
									$requisition_number = $row->requisition_number;
									$requisition_date = $row->requisition_date;
									$store_id = $row->store_id;
									$status = $row->requisition_status;
									$modified_by = $row->created_by;
									$personnel_created = $row->personnel_created;
									$store_name = $row->store_name;
									$personnel_fname = $row->personnel_fname;
									$personnel_onames = $row->personnel_onames;
									$date_sent = $row->date_sent;
									
									$created_by = $personnel_fname.' '.$personnel_onames;

									if(!empty($date_sent))
									{
										$sent = '<td>'.date('Y-m-d H:i',strtotime($date_sent)).'</td>';	
									}

									else
									{
										$sent = '<td>-</td>';
									}

									$total_price = 0;
									$total_items = 0;
									
									$button = '';

									$next_order_status = $status;

									$personnel_id = $this->session->userdata('personnel_id');


									$status_name = $this->orders_model->get_next_approval_status_name($next_order_status);
									// $is_hod = $this->reception_model->check_if_admin($personnel_id,30);
									// $is_admin = $this->reception_model->check_if_admin($personnel_id,1);
									$check_level_approval = FALSE;
									if($personnel_id > 0)
									{
										$next_order_status++;
										$check_level_approval = $this->orders_model->check_assigned_next_approval($next_order_status);
									}

									// var_dump($check_level_approval);die();

									$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

									if($status == 0 AND ($authorize_invoice_changes OR $check_level_approval))
									{

										if($personnel_created == $personnel_id)
										{
											$add = '<td><a href="'.site_url().'view-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-folder">ADD REQUISITION ITEMS</a></td>';
										}
										else
										{
											$add = '';
										}
										
										$status_name = 'DEPARTMENT ORDER'.$personnel_created;
									}
									else if($status == 1  AND ($authorize_invoice_changes OR $check_level_approval))
									{
										$add = '<td><a href="'.site_url().'print-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-info  btn-sm fa fa-print" target="_blank"> PRINT REQUISITION FORM</a></td>';
										$add .= '<td><a href="'.site_url().'open-supplier-view/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-pencil"> VIEW REQUISITION</a></td>';
										$status_name = 'PROCUREMENT OFFFICE';
									}
									else if($status == 2  AND ($authorize_invoice_changes OR $check_level_approval))
									{
										
										$add = '<td><a href="'.site_url().'view-orders-view/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-success  btn-sm fa fa-pencil"> VIEW ORDERS</a></td>';
										$status_name = 'LPO GENERATION';
									}
									else
									{
										$add = '<td><a href="'.site_url().'print-supplier-requisition/'.$requisition_id.'/'.$store_id.'" class="btn btn-xs btn-info  btn-sm fa fa-print" target="_blank"> PRINT REQUISITION FORM</a></td>';
										$status_name = '';
									}
									// just to mark for the next two stages
								

									$count++;
									$result .= 
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$date_sent.'</td>
											<td>'.date('H:i',strtotime($date_sent)).'</td>
											'.$sent.'
											<td>'.$requisition_number.'</td>
											<td>'.$store_name.'</td>
											<td>'.$created_by.'</td>
											<td>'.$status_name.'</td>
											
											'.$add.'
											
											
										</tr> 
									';
									// }
								}
					
						$result .= 
						'
								  </tbody>
								</table>
							</div>
						</div>
						';
				}
				
				else
				{
					$result .= "There are no orders";
				}
				$result .= '</div>';
				echo $result;
			?>

    </div>
     <div class="widget-foot">
                                
		<?php if(isset($links)){echo $links;}?>
    
        <div class="clearfix"></div> 
    
    </div>
</section>