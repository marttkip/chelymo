 <section class="panel panel-success">
    <header class="panel-heading">
        <h3 class="panel-title">SEARCH <?php echo $title?> for <?php echo date('jS M Y',strtotime(date('Y-m-d'))); ?></h3>
    </header>
    <!-- Widget content -->
         <div class="panel-body">
    	<div class="padd">
			<?php
			
			
			echo form_open("inventory/requisition/search_procurement", array("class" => "form-horizontal"));
			
            
            ?>
            <div class="col-md-12">
                <div class="col-md-4">
                    
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Requisition No: </label>
                        
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="requisition_no" placeholder="Requisition No.">
                        </div>
                    </div>
                    
                  
                    
                </div>
               
                
                
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="form-group">
                            <!-- <label class="col-md-3 control-label">Schedule appointment? </label> -->
                            <div class="col-md-3">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="date_time" value="1" >
                                        Today
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="date_time" value="2" >
                                        Yesterday
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="date_time" value="3" >
                                        This Week
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="date_time" value="4" >
                                        This Month
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Date From: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Date To: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>
                <div class="col-md-2">
                  	<div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="center-align">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </div>
                        </div>
                 </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
</section>