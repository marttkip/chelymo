 <section class="panel panel-success">
    <header class="panel-heading">
        <h3 class="panel-title">SEARCH <?php echo $title?> for <?php echo date('jS M Y',strtotime(date('Y-m-d'))); ?></h3>
    </header>
    <!-- Widget content -->
         <div class="panel-body">
    	<div class="padd">
			<?php
			
			
			echo form_open("inventory/requisition/search_products", array("class" => "form-horizontal"));
			
            
            ?>
            <div class="col-md-12">
     
               
                
                
                <div class="col-md-10">
               <!--  -->
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Date From: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Date To: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>
                <div class="col-md-2">
                  	<div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="center-align">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </div>
                        </div>
                 </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
</section>