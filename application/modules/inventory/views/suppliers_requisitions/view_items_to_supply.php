<?php

$result = '';

$result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>PRODUCT</th>
                  <th>UNITs</th>
                  <th>UNIT PRICE</th>
                  <th>DISC%</th>
                  <th>VAT</th>
                  <th>TOTAL</th>
                  <th colspan="2">Actions</th>
                </tr>
              </thead>
              <tbody>
            ';
if($query != null)
{
	$total_amount = 0;
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$order_item_id = $value->order_item_id;
			$product_name = $value->product_name;
			$creditor_id = $value->creditor_id;
			// $supplier_name = $value->supplier_name;
			$product_name = $value->product_name;
			$units = $value->order_item_quantity;
			$supplier_unit_price = $selling_price = $value->buying_unit_price;
			$creditor_name = $value->creditor_name;
			$quantity_received = $value->quantity_received;
			$vat = $value->vat;
			$discount = $value->discount;
			$less_vat = $value->less_vat;

	
			
		

			



			$total_amount += $less_vat;


			 $result .= 
                '
                    <tr >
                        <td>'.$product_name.'
                        	<br>
                        	<strong>'.$creditor_name.'</strong>
                        </td>

                        <td><input type="text" name="units'.$order_item_id.'" id="units'.$order_item_id.'" class="form-control" value="'.$units.'" ></td>
                        <td><input type="text" name="supplier_price'.$order_item_id.'" id="supplier_price'.$order_item_id.'" class="form-control" value="'.$supplier_unit_price.'" ></td>
                        <td><input type="text" name="discount'.$order_item_id.'" id="discount'.$order_item_id.'" class="form-control" value="'.$discount.'" ></td>
                        <td><input type="text" name="vat'.$order_item_id.'" id="vat'.$order_item_id.'" class="form-control" value="'.$vat.'" ></td>
                        <td>'.number_format($less_vat,2).'</td>
                        <td><a class="btn btn-xs btn-success" onclick="update_item_amount('.$order_item_id.','.$requisition_id.','.$store_id.')"><i class="fa fa-pencil"></i></a></td>
                        <td><a class="btn btn-xs btn-danger" onclick="remove_items('.$order_item_id.','.$requisition_id.','.$store_id.')"><i class="fa fa-trash"></i></a></td>
                    </tr> 
                ';
			
		}

		 $result .= 
                '
                    <tr >
                        <th>TOTAL AMOUNT </th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>'.number_format($total_amount,2).'</th>
                        <th></th>
                         <th></th>
                    </tr> 
                ';

	}
}
 $result .= 
        '
          </tbody>
        </table>
        ';
echo $result;
if($requisition_id == 0)
{
	?>
	<div class="row">
		<div class="center-align">

			<a class="btn btn-xs btn-success" onclick="confirm_requisition(<?php echo $requisition_id?>)"> Confirm Requisition Items </a>
		</div>
	</div>
	<?php
}

?>