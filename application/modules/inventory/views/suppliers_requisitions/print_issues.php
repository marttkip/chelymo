<?php


 
if($query != null)
{
 	$query = $this->requisition_model->get_store();
	if($query->num_rows() > 0)
	{

		
   $result ='<table class="table table-hover table-condensed table-bordered ">
              <thead>
                <tr>
                  <th>DATE</th>
                  <th>DESCRIPTION</th>
                  <th>QUANTITY</th>
                  <th>PRICE</th>
                   <th>TOTAL</th>
                </tr>
              </thead>
              <tbody>
            ';


		foreach ($query->result() as $key => $value) {
			# code...
			$product_id = $value->product_id;
			$product_deductions_id = $value->product_deductions_id;
			$product_name = $value->product_name;
			//$product_unitprice = $value->product_unitprice;
			$store_name = $value->store_name;
			$units = $value->product_deductions_quantity;
			$product_deductions_date = $value->product_deductions_date;


			if(empty($units))
			{
				$units = 0;
			}


			 $result .= 
                '
                    <tr >
            <th class="text-left btn-danger"  colspan="5"  > '.strtoupper($store_name).'</th>
                    </tr> 
   
                ';

       $store = $this->requisition_model->get_store_type($store_name);

       $product_name ="" ;
				if($store->num_rows() > 0)
				{
					foreach ($store->result() as $key => $value) {
						# code...
						
					$product_id = $value->product_id;
					$product_deductions_id = $value->product_deductions_id;
					$product_name = $value->product_name;
					$store_name = $value->store_name;
					$units = $value->product_deductions_quantity;
					$product_unitprice = $value->product_unitprice;
		            $product_deductions_date = $value->product_deductions_date;
		            $product_unitprice = $value->product_unitprice;


		            $total = $product_unitprice * $units;
		            $total_amount += $total;

		            $total_units += $units;


                	 $result .= 
                '
                    <tr >
                  <tr>
									
								        <td>'.$product_deductions_date.'</td>
                        <td>'.$product_name.'</td>
                        <td>'.$units.'</td>
                        <td>'.$product_unitprice.'</td>
                        <td>'.$total.'</td>
									</tr>
                ';



    // $store = $this->requisition_model->get_store_type($account_type_name)
			
		}
	}


 $result .= 
                '
                <tr style ="color:blue;">
                <td><strong>TOTAL</strong></td>			
                <th>'.$store_name.'s</th>
                 <td>'.$total_units.'</td>
                 <td></td>
                 <td>'.$total_amount.'</td>
				</tr>
                ';



	}
}
 $result .= 
        '
          </tbody>
        </table>
        ';
//echo $result;
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
    <!--       <title> Reports</title> -->
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
           .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
            {
                 padding: 2px;
            }
            .title-img{float:left; padding-left:10px;}
            img.logo{ margin:0 auto;}
           .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:0px;}
            img.logo{height:130px; margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            h4, .h4 {
                font-size: 24px;
                font-weight: bolder;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">

        <div class="row receipt_bottom_border" >
            <div class="col-md-12">
                <div class="pull-left" style="margin-bottom: 8px;">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="height: 100px; width: 100px;"/>
                </div>
     

                <div class="pull-left">
                    <strong>
                        <h4  style="font-size:14px">
                        <?php echo $contacts['company_name'];?><br/>
                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                        <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                        E-mail: <?php echo $contacts['email'];?>.<br/>
                         Tel : <?php echo $contacts['phone'];?><br/>
                     </h4>
                    </strong>
                </div>
                <div class="pull-right" style="margin-right:30px">
                    <strong>
                    <!--     <h4  style="font-size:18px">
                        	<?php echo $requisition_number; ?>
                    
                  
                     </h4> -->
                    </strong>
                </div>
            </div>
        </div>
        <div class="row receipt_bottom_border" >
            <div class="col-md-12 center-align">
                <h4>STOCK ISSUES</h4>
            </div>
        </div>
      
        
        <!-- <div class="padd"> -->
            <div class="col-print-12">
                <?php echo $result;?>
            </div>
          
              <!-- <div class="pull-left"> -->
            <div class="col-md-12" style="font-style:bold;font-weight: bold;">
                <div class="col-print-12" style="margin-bottom: 30px; margin-top: 20px;">
                    <div class="col-print-4">
                        Prepared by : <?php echo $served_by; ?>
                    </div>
                    <div class="col-print-4">
                        Designation : ......................................................
                    </div>
                    <div class="col-print-4">
                        Sign : ...........................................................
                    </div>
                </div>
       
                <div class="col-print-12" style="margin-bottom: 30px;">
                    <div class="col-print-4">
                        Checked by : ......................................................
                    </div>
                    <div class="col-print-4">
                        Designation : ......................................................
                    </div>
                    <div class="col-print-4">
                        Sign : ............................................................
                    </div>
                </div>
                <div class="col-print-12" style="margin-bottom: 30px;">
                    <div class="col-print-4">
                        Approved by : ......................................................
                    </div>
                    <div class="col-print-4">
                        Designation : ......................................................
                    </div>
                    <div class="col-print-4">
                        Sign : .............................................................
                    </div>
                </div>
                 <div class="col-print-12" style="margin-bottom: 30px;">
                    <div class="col-print-4">
                        Authorised by : ......................................................
                    </div>
                    <div class="col-print-4">
                        Designation : .........................................................
                    </div>
                    <div class="col-print-4">
                        Sign : ................................................................
                    </div>
                </div>
            </div>
        <!-- </div> -->
        <!-- </div> -->
       
    </body>
    
</html> 