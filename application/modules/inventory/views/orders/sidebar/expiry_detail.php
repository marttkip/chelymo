<?php

$this->db->where('order_supplier_id ='.$order_supplier_id);
$query = $this->db->get('order_supplier');


$expiry_date = date('Y-m-d');
$store_id = 5;
if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		// code...
		$expiry_date = $value->expiry_date;
		$store_id = $value->store_id;
		$quantity_received = $value->quantity_received;
	}
}


$this->db->where('product_id ='.$product_id);
$query = $this->db->get('product');


$product_name = '';
if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		// code...
		$product_name = $value->product_name;
	}
}

if($expiry_date == "0000-00-00")
{
	$expiry_date = date('Y-m-d');
}

?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo  $product_name.' Expiry Update';?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd" style="height:80vh;overflow-y: scroll;">
            
           	<?php
			
			
			echo form_open("reception/search_general_queue", array("class" => "form-horizontal","id"=>'expiry-order-form'));
			
            
            ?>

            <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id;?>">
            <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id;?>">
            <input type="hidden" name="quantity_received" id="quantity_received" value="<?php echo $quantity_received;?>">
            <input type="hidden" name="store_id" id="store_id" value="<?php echo $store_id;?>">
             <input type="hidden" name="order_supplier_id" id="order_supplier_id" value="<?php echo $order_supplier_id;?>">
            <div class="row">
                <div class="col-md-6">                   
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Expiry Date: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="expiry_date" placeholder="" value="<?php echo $expiry_date;?>">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                            <div class="center-align">
                                <button type="submit" class="btn btn-info">Update Exipiry</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>