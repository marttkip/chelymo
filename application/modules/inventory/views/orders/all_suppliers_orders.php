<div class="row">
	

	<?php echo $this->load->view('search_suppliers_orders', '', TRUE);?>
	<section class="panel panel-featured panel-featured-info">
	    <header class="panel-heading">
	         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
	         <div class="widget-icons pull-right">
	         		<a class="btn btn-warning btn-sm" data-toggle='modal' data-target='#lpo_add_items'>ADD NEW GOODS RECEIVED NOTE </a>
	            	<a class="btn btn-success btn-sm" data-toggle='modal' data-target='#add_provider_items'>ADD NEW SUPPLIER INVOICE</a>
	          </div>
	          <div class="clearfix"></div>
	    </header>
	    <div class="panel-body">
	    	<div class="padd">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				$search_result ='';
				$search_result2  ='';
				if(!empty($error))
				{
					$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
				$search = $this->session->userdata('supplier_order_search');
				
				if(!empty($search))
				{
					$search_result2 = '<a href="'.site_url().'inventory/orders/close_supplier_order_search" class="btn btn-danger">Close Search</a>';
				}


				$result = '<div class="padd">';	
				$result .= ''.$search_result2.'';
				$result .= '
						';
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<div class="row">
						<div class="col-md-12">
							<table class="example table-autosort:0 table-stripeclass:alternate table  table-bordered " id="TABLE_2">
							  <thead>
								<tr>
								  <th >#</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Created Date</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Invoice Date</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">S.Invoice Number</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Supplier</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Ordering Store</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Type</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Created By</th>
								  <th class="table-sortable:default table-sortable" title="Click to sort">Status</th>
								  <th >Total Amount</th>
								  <th colspan="2">Actions</th>
								</tr>
							  </thead>
							  <tbody>
							';
					
								//get all administrators
								$personnel_query = $this->personnel_model->get_all_personnel();
								
								foreach ($query->result() as $row)
								{
									$order_id = $row->order_id;
									$order_number = $row->order_number;
									$order_status = $row->order_status_id;
									$order_instructions = $row->order_instructions;
									$order_status_name = $row->order_status_name;
									$created_by = $row->created_by;
									$created = $row->created;
									$modified_by = $row->modified_by;
									$creditor_name = $row->creditor_name;
									$suffix = $row->suffix;
									$store_id = $row->store_id;
									$creditor_id = $row->creditor_id;
									$store_name = $row->store_name;
									$last_modified = $row->last_modified;
									$dr_amount = $row->dr_amount;
									$order_category = $row->order_category;
									$order_approval_status = $row->order_approval_status;
									$supplier_invoice_date = $row->supplier_invoice_date;
									$supplier_invoice_number = $row->supplier_invoice_number;

									if(!empty($supplier_invoice_date))
									{
										$invoice_date = ''.date('jS M Y',strtotime($supplier_invoice_date)).'';
									}
									else
									{
										$invoice_date = '-';
									}


									if(!empty($created))
									{
										$created_date = ''.date('jS M Y',strtotime($created)).'';
									}
									else
									{
										$created_date = '-';
									}

									// var_dump($order_approval_status); die();

									$order_details = $this->orders_model->get_order_items($order_id);
									$total_price = 0;
									$total_items = 0;
									//creators & editors
									
									if($personnel_query->num_rows() > 0)
									{
										$personnel_result = $personnel_query->result();
										
										foreach($personnel_result as $adm)
										{
											$personnel_id2 = $adm->personnel_id;
											
											if($created_by == $personnel_id2 ||  $modified_by == $personnel_id2 )
											{
												$created_by = $adm->personnel_fname;
												break;
											}
											
											else
											{
												$created_by = '-';
											}
										}
									}
									
									else
									{
										$created_by = '-';
									}

									
									$button = '';

									


									// $approval_levels = $this->orders_model->check_if_can_access($order_approval_status,$order_id);

									// $personnel_id = $this->session->userdata('personnel_id');

									// $is_hod = $this->reception_model->check_if_admin($personnel_id,30);
									// $is_admin = $this->reception_model->check_if_admin($personnel_id,1);


									
									// if($approval_levels == TRUE OR $personnel_id == 0 )
									// {	

										$next_order_status = $order_approval_status+1;

										// $status_name = $this->orders_model->get_next_approval_status_name($next_order_status);
										// $is_hod = $this->reception_model->check_if_admin($personnel_id,30);
										// $is_admin = $this->reception_model->check_if_admin($personnel_id,1);

									
										//pending order
										if($order_approval_status == 7 )
										{
											$status = '<span class="label label-success">Order has been closed</span>';
											$button = '';
											$button2 = '';
										}
										else
										{
											$status = '<span class="label label-default">Order is open</span>';
											$button = '';
											$button2 = '';
										}

										if($order_category == 0)
										{
											$category = 'Local';
										}
										else
										{
											$category = 'Import';
										}
										

										// just to mark for the next two stages
										// $dr_amount = $this->orders_model->get_creditor_invoice_total($supplier_id,$order_id);

										$count++;

										


										$result .= 
										'
											<tr>
												<td>'.$count.'</td>
												<td>'.$created_date.'</td>
												<td>'.$invoice_date.'</td>
												<td>'.$supplier_invoice_number.'</td>
												<td>'.$creditor_name.'</td>
												<td>'.$store_name.'</td>
												<td>'.$category.'</td>
												<td>'.$created_by.'</td>
												<td>'.$status.'</td>
												<td>'.number_format($dr_amount,2).'</td>
												<td><a href="'.site_url().'accounts-payables/supplier-invoice-detail/'.$order_id.'" class="btn btn-info  btn-sm fa fa-eye"> VIEW INVOICE </a></td>
												<td><a href="'.site_url().'inventory/orders/goods_received_notes/'.$order_id.'" target="_blank" class="btn btn-warning  btn-sm fa fa-print"> PRINT </a></td>
												
												
												
											</tr> 
										';
										
									// }
								}
					
						$result .= 
						'
								  </tbody>
								</table>
							</div>
						</div>
						';
				}
				
				else
				{
					$result .= "There are no orders";
				}
				$result .= '</div>';
				echo $result;
			?>
		</div>

	         <div class="widget-foot">
	                                
					<?php if(isset($links)){echo $links;}?>
	            
	                <div class="clearfix"></div> 
	            
	            </div>
	        </div>

	   

	     <div class="modal fade bs-example-modal-lg" id="lpo_add_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog modal-lg" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">RECEIVE SUPPLIED GOODS</h4>
	                </div>
	                <?php echo form_open("add-goods-recieved-notes", array("class" => "form-horizontal", "role" => "form"));?>
	                <div class="modal-body">
				        <div class="row">
				        	<div class="col-md-10">
				        	
				             	<div class="col-md-12" style="margin-top: 20px;">
				              		<div class="form-group">
					                	<label class="col-lg-2 control-label">Order</label>
					                    <div class="col-lg-10">
					                    	<select class="form-control" name="order_id" id="order_id" required="required">
					                    		<option value=""> ------- Select an order ----- </option>
					                    		<?php
					                    		if($lpo_items->num_rows() > 0)
					                    		{
					                    			foreach ($lpo_items->result() as $lpo_items ) {
					                    				# code...
					                    				$order_id = $lpo_items->order_id;
					                    				$creditor_name = $lpo_items->creditor_name;
					                    				$lpo_number = $lpo_items->lpo_number;

					                    				echo '<option value="'.$order_id.'">'.$creditor_name.' - ( '.$lpo_number.' )</option>';
					                    			}
					                    		}
					                    		?>
					                    	</select>
					                    </div>
					                </div>
					            </div>
					           
					            <div class="col-md-12" style="margin-top: 20px;">
				                  	<div class="form-group">
				                        <label class="col-lg-2 control-label">Invoice No: </label>	                        
				                        <div class="col-lg-10">
				                            <input type="text" class="form-control" name="supplier_invoice_number" placeholder="Invoice Number" autocomplete="off" required="required">
				                        </div>
				                    </div>
				                </div>
				                <div class="col-md-12" style="margin-top: 20px;">
					                <div class="form-group">
				                        <label class="col-lg-2 control-label">Invoice Date: </label>
				                        
				                        <div class="col-lg-10">
				                        	<div class="input-group">
				                                <span class="input-group-addon">
				                                    <i class="fa fa-calendar"></i>
				                                </span>
				                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="supplier_invoice_date" placeholder="Date" value="<?php echo date('Y-m-d')?>" required="required" >
				                            </div>
				                        </div>
				                    </div>
				                 </div>
			      			</div>
				        </div>
	                </div>
	                <div class="modal-footer">
	                	<button type="submit" class='btn btn-info btn-sm' type='submit' >ADD ORDER </button>
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                    
	                </div>
	                <?php echo form_close();?>
	            </div>
	        </div>
	     </div>
	     <div class="modal fade bs-example-modal-lg" id="add_provider_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog modal-lg" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="myModalLabel">Add New Item</h4>
	                </div>
	                <?php echo form_open("add-supplier-invoice", array("class" => "form-horizontal", "role" => "form"));?>
	                <div class="modal-body">
				        <div class="row">
				        	<div class="col-md-12">
				                <div class="form-group">
				                	<label class="col-lg-2 control-label">Ordering Store</label>
				                    <div class="col-lg-10">
				                    	 <select name="store_id" id="store_id" class="form-control" required="required">
		                                        <?php
		                                        $personnel_id = $this->session->userdata('personnel_id');
		                                        $all_stores = $this->stores_model->get_parent_stores($personnel_id);
		                                        echo '<option value="">No Store</option>';
		                                        if($all_stores->num_rows() > 0)
		                                        {
		                                            $result = $all_stores->result();
		                                            
		                                            foreach($result as $res)
		                                            {
		                                                if($res->store_id == set_value('store_id'))
		                                                {
		                                                    echo '<option value="'.$res->store_id.'" selected>'.$res->store_name.'</option>';
		                                                }
		                                                else
		                                                {
		                                                    echo '<option value="'.$res->store_id.'">'.$res->store_name.'</option>';
		                                                }
		                                            }
		                                        }
		                                        ?>
		                                </select>
				                       
				                    </div>
				                </div>
				            </div>
			             	<div class="col-md-12" style="margin-top: 20px;">
			              		<div class="form-group">
				                	<label class="col-lg-2 control-label">Suppliers</label>
				                    <div class="col-lg-10">
				                    	<select class="form-control" name="supplier_id" id="supplier_id" required="required">
				                    		<option value=""> ------- Select a supplier ----- </option>
				                    		<?php
				                    		if($suppliers_query->num_rows() > 0)
				                    		{
				                    			foreach ($suppliers_query->result() as $key_supplier_items ) {
				                    				# code...
				                    				$creditor_id = $key_supplier_items->creditor_id;
				                    				$creditor_name = $key_supplier_items->creditor_name;
				                    				$creditor_type_name = $key_supplier_items->creditor_type_name;

				                    				echo '<option value="'.$creditor_id.'">'.$creditor_name.' - ( '.$creditor_type_name.' )</option>';
				                    			}
				                    		}
				                    		?>
				                    	</select>
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-12" style="margin-top: 20px;">
				            	<div class="form-group">
									<label class="col-lg-2 control-label">Import / Local </label>
						            <div class="col-lg-5">
						                <div class="radio">
						                    <label>
						                        <input id="optionsRadios2" type="radio" name="order_category" value="0" checked="checked" >
						                        Local
						                    </label>
						                </div>
						            </div>
						            
						            <div class="col-lg-5">
						                <div class="radio">
						                    <label>
						                        <input id="optionsRadios2" type="radio" name="order_category" value="1">
						                        Import
						                    </label>
						                </div>
						            </div>
								</div>
				            </div>
				            <div class="col-md-12" style="margin-top: 20px;">
			                  	<div class="form-group">
			                        <label class="col-lg-2 control-label">Invoice No: </label>	                        
			                        <div class="col-lg-10">
			                            <input type="text" class="form-control" name="supplier_invoice_number" placeholder="Invoice Number" autocomplete="off" required="required">
			                        </div>
			                    </div>
			                </div>
			                 <div class="col-md-12" style="margin-top: 20px;">
				                <div class="form-group">
			                        <label class="col-lg-2 control-label">Invoice Date: </label>
			                        
			                        <div class="col-lg-10">
			                        	<div class="input-group">
			                                <span class="input-group-addon">
			                                    <i class="fa fa-calendar"></i>
			                                </span>
			                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="supplier_invoice_date" placeholder="Date" value="<?php echo date('Y-m-d')?>" required readonly>
			                            </div>
			                        </div>
			                    </div>
			                 </div>
			       <!--             <div class="form-group">
	                                    <label class="col-md-4 control-label">Payment Date: </label>
	                                    
	                                    <div class="col-md-8">
	                                        <div class="input-group">
	                                            <span class="input-group-addon">
	                                                <i class="fa fa-calendar"></i>
	                                            </span>
	                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment date" value="<?php echo date('Y-m-d')?>" required="required">
	                                        </div>
	                                    </div>
	                                </div> -->
				          <!--   <div class="col-md-12" style="margin-top: 20px;">
				            	<div class="form-group">
					                <label class="col-lg-2 control-label">Order Instructions</label>
					                <div class="col-lg-10">
					                	<textarea class="form-control" name="order_instructions"><?php echo set_value('order_instructions');?></textarea>
					                </div>
					            </div>
				            </div> -->
				        </div>
	                </div>
	                <div class="modal-footer">
	                	<button type="submit" class='btn btn-info btn-sm' onclick="return confirm('Are you sure you want to add this supplier invoice ?')" type='submit' >Add Order</button>
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                    
	                </div>
	                <?php echo form_close();?>
	            </div>
	        </div>
	     </div>
	</section>
</div>
<script>
$(document).ready(function(){
    $("#store_id").customselect();
    $("#supplier_id").customselect();
});
</script>