 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title pull-right"></h2>

        <h2 class="panel-title">Search Orders</h2>

    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php

            echo form_open("inventory/orders/search_supplier_orders", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	<!-- <div class="col-md-12"> -->
	                <div class="col-md-2">
	                	<div class="form-group" style="margin:0 auto;">
	                        <!-- <label class="col-lg-4 control-label">SUPPLIER </label> -->
                            <div class="col-lg-10">
                                <select id="supplier_id" name="supplier_id" class="form-control custom-select">
                                   <option value="0">SELECT A SUPPLIER</option>
		                    		<?php
		                    		if($suppliers_query->num_rows() > 0)
		                    		{
		                    			foreach ($suppliers_query->result() as $key_supplier_items ) {
		                    				# code...
		                    				$creditor_id = $key_supplier_items->creditor_id;
		                    				$creditor_name = $key_supplier_items->creditor_name;

		                    				echo '<option value="'.$creditor_id.'">'.$creditor_name.'</option>';
		                    			}
		                    		}
		                    		?>
                                </select>
                            </div>
	                    </div>
	                </div>
                    <div class="col-md-2">
                        <div class="form-group" style="margin:0 auto;">
                            <!-- <label class="col-lg-4 control-label">SELECT A STORE</label> -->
                            <div class="col-lg-10">
                                <select name="store_id" id="store_id" class="form-control custom-select" required="required">
                                        <?php
                                        $personnel_id = $this->session->userdata('personnel_id');
                                        $all_stores = $this->stores_model->get_parent_stores($personnel_id);
                                        echo '<option value="">No Store</option>';
                                        if($all_stores->num_rows() > 0)
                                        {
                                            $result = $all_stores->result();
                                            
                                            foreach($result as $res)
                                            {
                                                if($res->store_id == set_value('store_id'))
                                                {
                                                    echo '<option value="'.$res->store_id.'" selected>'.$res->store_name.'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$res->store_id.'">'.$res->store_name.'</option>';
                                                }
                                            }
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" style="margin:0 auto;">
                            <div class="col-lg-2 "></div> 
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="invoice_number" placeholder="Invoice Number">
                            </div>
                        </div>
                    </div>      
                    <div class="col-md-3">
                        <div class="form-group" style="margin:0 auto;">
                            <!-- <label class="col-lg-4 control-label">DATE: </label> -->
                            
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="supplier_invoice_date" placeholder="Invoice Date" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info btn-sm">Search</button>
                        </div>
                    </div>
                </div>
            <!-- </div> -->

            <br/>
            
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>