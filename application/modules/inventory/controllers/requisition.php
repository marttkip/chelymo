<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Requisition extends MX_Controller
{

	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('inventory_management/products_model');
		$this->load->model('orders_model');
		$this->load->model('suppliers_model');
		$this->load->model('categories_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/creditors_model');
		$this->load->model('inventory/stores_model');
		$this->load->model('reception/database');
		$this->load->model('reception/reception_model');
		$this->load->model('inventory/requisition_model');
		$this->load->model('inventory/orders_model');
		$this->load->model('inventory_management/inventory_management_model');
	}

	/*
	*
	*	Default action is to show all the orders
	*
	*/
	public function index()
	{
		// get my approval roles

		$personnel_id = $this->session->userdata('personnel_id');
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

		if($personnel_id == 0 OR $authorize_invoice_changes)
		{
			$where = 'requisition_id > 0 AND requisition_type_id = 2 AND store.store_id = requisition.store_id';
		}
		else
		{
			$where = 'requisition_id > 0 AND requisition_type_id = 2 AND store.store_id = requisition.store_id AND requisition.created_by = '.$personnel_id;


		}
		
		$table = 'requisition,store';

	

		$search = $this->session->userdata('main_requisition_search');


		if(!empty($search))
		{
			$where .= $search;
		}
		else
		{
			$where .= ' AND DATE(requisition.created) = "'.date('Y-m-d').'" ';
		}
        

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/requisitions';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 40;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_all_requisitions($table, $where, $config["per_page"], $page);

		$store_priviledges = $this->inventory_management_model->get_main_stores();
		$v_data['store_priviledges'] =  $store_priviledges;


		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "SUPPLIERS REQUISITIONS";
		$data['content'] = $this->load->view('suppliers_requisitions/supplier_requisitions', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_requisition()
	{
	
		$requisition_number = $this->input->post('requisition_number');
		$date_time = $this->input->post('date_time');
		$visit_date_from = $this->input->post('date_from');
		$visit_date_to = $this->input->post('date_to');
		

		$search_title = '';
		if(!empty($requisition_number))
		{
			$search_title .= ' Number :  '.$requisition_number;
			$requisition_number = ' AND requisition.requisition_number LIKE \'%'.$requisition_number.'%\'';
		}
		

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$providers_date_from = $visit_date_from;
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from) && empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_from.'\'';
			$providers_date_from = $visit_date_from;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to) && empty($visit_date_from))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_to.'\'';
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date_checked = '';
			$providers_date_from = '';
			$providers_date_to = '';
		}
		// var_dump($visit_date_checked);die();
		
		$list_view = '';

		// var_dump($date_time);die();
		if(!empty($date_time))
		{

			if($date_time == 1)
			{
				$list_view = ' AND DATE(requisition.created) = \''.date('Y-m-d').'\' ';
				$search_title .= ' Today ';
			}
			else if($date_time == 2)
			{
				$visit_date = date('Y-m-d');
				$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
				$list_view = ' AND DATE(requisition.created) = \''.$visit_date.'\'';
				$search_title .= ' Yesterday ';

			}
			else if($date_time == 3)
			{		
				$visit_date = date('Y-m-d');
				$firstday = date('Y-m-d', strtotime("this week")); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$firstday.'\' AND \''.$visit_date.'\'  ';
				$search_title .= ' This Week ';
			}
			else if($date_time == 4)
			{
				

				$visit_date = date('Y-m-01');
				$last_day = date('Y-m-t'); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$visit_date.'\' AND \''.$last_day.'\'  ';
				$search_title .= ' This Month ';
			}
			
		}
		
		$search = $requisition_number.$list_view.$visit_date_checked;
		$this->session->set_userdata('main_requisition_search_title', $search_title);
		$this->session->set_userdata('main_requisition_search', $search);
		
		redirect('procurement/request');
	}

		/*
	*
	*	Default action is to show all the orders
	*
	*/
	public function stock_level()
	{
		// get my approval roles

		$personnel_id = $this->session->userdata('personnel_id');
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

		$where = 'product_deductions.product_id = product.product_id  AND orders.order_id = product_deductions.order_id AND store.store_id = orders.store_id AND product_deductions.product_deductions_status = 0';
		$table = 'product,orders,product_deductions,store';

	

		$search = $this->session->userdata('main_stock_search');


		if(!empty($search))
		{
			$where .= $search;
		}
		else
		{
			$where .= ' AND product_deductions.search_date = "'.$search_date.'"';
		}
        

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'inventory/stock-issues';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 40;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_product_deductions($table, $where, $config["per_page"], $page);

		$store_priviledges = $this->inventory_management_model->get_main_stores();
		$v_data['store_priviledges'] =  $store_priviledges;


		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "STOCK PER STORE";
		$data['content'] = $this->load->view('suppliers_requisitions/store_requisition_stock', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}


		public function print_issues()
	{

		
		$where = 'product_deductions.product_id = product.product_id  AND orders.order_id = product_deductions.order_id AND store.store_id = orders.store_id AND product_deductions.product_deductions_status = 0';
		$table = 'product,orders,product_deductions,store';


		
	
		$search = $this->session->userdata('main_stock_search');


		if(!empty($search))
		{
			$where .= $search;
		}
		// else
		// {
		// 	$where .= ' AND product_deductions.search_date = "'.$search_date.'"';
		// }

		$query = $this->requisition_model->get_product_deductions($table, $where);
		// $v_data['store_id'] = $store_id;

		$data['title'] = 'Stickers';
		$v_data['title'] = $data['title'];
		$v_data['contacts'] = $this->site_model->get_contacts();

		$v_data['query'] = $query;
		// $v_data['branches'] = $branches;

		$this->load->view('suppliers_requisitions/print_issues', $v_data);

	}



	public function search_products()
	{
	
	
		$date_time = $this->input->post('date_time');
		$visit_date_from = $this->input->post('date_from');
		$visit_date_to = $this->input->post('date_to');

		

		$search_title = '';
	

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(product_deductions.product_deductions_date) BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$providers_date_from = $visit_date_from;
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from) && empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(product_deductions.product_deductions_date) = \''.$visit_date_from.'\'';
			$providers_date_from = $visit_date_from;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to) && empty($visit_date_from))
		{
			$visit_date_checked = ' AND DATE(product_deductions.product_deductions_date) = \''.$visit_date_to.'\'';
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date_checked = '';
			$providers_date_from = '';
			$providers_date_to = '';
		}
		// var_dump($visit_date_checked);die();

		
		$search = $visit_date_checked;
		$this->session->set_userdata('main_stock_search_title', $search_title);
		$this->session->set_userdata('main_stock_search', $search);
		
		redirect('inventory/stock-issues');
	}

	public function close_general_queue_search()
	{
		$this->session->unset_userdata('main_stock_search');
		$this->session->unset_userdata('main_stock_search_title');
		// $this->general_queue($page_name);
		redirect('inventory/stock-issues');
	}




	public function create_supplier_requisition()
	{
		// $this->form_validation->set_rules('orders_date', 'Order Date', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'numeric|xss_clean');

		//if form conatins valid data
		if ($this->form_validation->run())
		{

			if($this->requisition_model->create_supplier_requisition())
			{
				$this->session->userdata('success_message', 'Product has been added successfully');

			}

			else
			{
				$this->session->userdata('error_message', 'Unable to add product. Please try again');
			}
		}

		else
		{
			$v_data['validation_errors'] = validation_errors();
		}
		redirect('procurement/requisitions');
	}
	public function requition_supplier_details($requisition_id,$store_id)
	{
	

		$v_data['requisition_id'] = $requisition_id;
		$v_data['store_id'] = $store_id;

		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('suppliers_requisitions/requisition_details', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);

	
	}


	public function supplier_product_list($requisition_id,$store_id)
	{

		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$drug_name = $this->input->post('drug');
		$query = null;
		if(!empty($drug_name))
		{

			$ultra_sound_where = 'product_deleted = 0';


			$ultra_sound_table = 'product';

			$ultra_sound_where .= ' AND product_name LIKE \'%'.$drug_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(50);
			// $this->db->join('store_product','store_product.product_id = product.product_id','LEFT');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0';
			$ultra_sound_table = 'product';
	

			$this->db->where($ultra_sound_where);
			$this->db->limit(50);
			// $this->db->join('store_product','store_product.product_id = product.product_id','LEFT');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('suppliers_requisitions/supplier_product_list',$data,true);
		$response['message'] = 'success';
		$response['results'] = $page;

		
		

		echo json_encode($response);

	}
	public function requisition_add($requisition_id = 0)
	{
		$v_data['requisition_id'] = $requisition_id;

		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('requisition/requisition_view', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);

	}

	public function product_list($requisition_id=0)
	{
		$data = array('requisition_id'=>$requisition_id);


		$drug_name = $this->input->post('drug');
		$query = null;
		if(!empty($drug_name))
		{

			$ultra_sound_where = 'product_deleted = 0';


			$ultra_sound_table = 'product';

			$ultra_sound_where .= ' AND product_name LIKE \'%'.$drug_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0';
			$ultra_sound_table = 'product';
	

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('requisition/product_list',$data,true);
		$response['message'] = 'success';
		$response['results'] = $page;

		
		

		echo json_encode($response);
	}	

	public function selected_products($requisition_id= 0)
	{
		

		$data = array('requisition_id'=>$requisition_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = requisition_item.product_id';
			$ultra_sound_table = 'product,requisition_item';
			$ultra_sound_where .= ' AND product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = requisition_item.product_id AND requisition_item.requisition_item_delete = 0';
			$ultra_sound_table = 'product,requisition_item';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('requisition/requisition_items',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}

	public function add_item_to_list($product_id,$requisition_id)
	{
		$requisition_add['requisition_id'] = $requisition_id;
		$requisition_add['product_id'] = $product_id;
		$requisition_add['created_on'] = date('Y-m-d');
		$requisition_add['created_by'] = $this->session->userdata('personnel_id');
		$requisition_add['current_stock'] = 0;
		

		$this->db->where('product_id = '.$product_id.' AND requisition_id = '.$requisition_id);
		$requisition_items = $this->db->get('requisition_item');

		if($requisition_items->num_rows() == 0)
		{
			$this->db->insert('requisition_item',$requisition_add);
		}
		else
		{

		}

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function update_items($requisition_item_id,$requisition_id)
	{

		$units = $this->input->post('units');
		$requisition_add['units'] = $units;
		

		$this->db->where('requisition_item_id = '.$requisition_item_id);
		$this->db->update('requisition_item',$requisition_add);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function delete_requisition_item($requisition_item_id,$requisition_id)
	{

	
		$requisition_add['requisition_item_delete'] = 1;
		

		$this->db->where('requisition_item_id = '.$requisition_item_id);
		$this->db->update('requisition_item',$requisition_add);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function confirm_requisition($requisition_id)
	{

	
		$requisition_add['created'] = date('Y-m-d');
		$requisition_add['created_by'] = $this->session->userdata('personnel_id');
		$requisition_add['requisition_status'] = 0;
		
		$this->db->insert('requisition',$requisition_add);
		$requisition_idd = $this->db->insert_id();

		$requisition_update['requisition_id'] = $requisition_idd;
		$this->db->where('requisition_id = '.$requisition_id.' AND requisition_item_delete = 0 AND created_by = '.$this->session->userdata('personnel_id'));
		$this->db->update('requisition_item',$requisition_update);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function store_requisitions()
	{

		// $config['base_url'] = base_url().'procurement/store-orders';
		// get my approval roles
		$personnel_id = $this->session->userdata('personnel_id');
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

		if($personnel_id == 0 OR $authorize_invoice_changes)
		{
			$where = 'requisition_id > 0 AND requisition_type_id = 1 AND store.store_id = requisition.store_id';
		}
		else
		{
			$where = 'requisition_id > 0 AND requisition_type_id = 1 AND store.store_id = requisition.store_id AND requisition.created_by = '.$personnel_id;
		}
		
		$table = 'requisition,store';

	

		$search = $this->session->userdata('orders_requisition_search');


		if(!empty($search))
		{
			$where .= $search;
		}
		else
		{
			$where .= ' AND DATE(requisition.created) = "'.date('Y-m-d').'" ';
		}
        

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/store-orders';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 40;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_all_requisitions($table, $where, $config["per_page"], $page);

		$store_priviledges = $this->inventory_management_model->get_assigned_stores();
		$v_data['store_priviledges'] =  $store_priviledges;


		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "My Requisitions";
		$data['content'] = $this->load->view('requisition/store_requisitions', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function create_store_requisition()
	{
		// $this->form_validation->set_rules('orders_date', 'Order Date', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'numeric|xss_clean');

		//if form conatins valid data
		if ($this->form_validation->run())
		{

			if($this->requisition_model->create_requisition())
			{
				$this->session->userdata('success_message', 'Product has been added successfully');

			}

			else
			{
				$this->session->userdata('error_message', 'Unable to add product. Please try again');
			}
		}

		else
		{
			$v_data['validation_errors'] = validation_errors();
		}
		redirect('procurement/store-orders');
	}

	public function requition_details($requisition_id,$store_id)
	{

		$v_data['requisition_id'] = $requisition_id;
		$v_data['store_id'] = $store_id;

		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('requisition/requisition_details', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);

	}
	public function store_product_list($requisition_id,$store_id)
	{

		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$drug_name = $this->input->post('drug');
		$query = null;
		if(!empty($drug_name))
		{

			$ultra_sound_where = 'product_deleted = 0';


			$ultra_sound_table = 'product';

			$ultra_sound_where .= ' AND product_name LIKE \'%'.$drug_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(50);
			// $this->db->join('store_product','store_product.product_id = product.product_id','LEFT');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0';
			$ultra_sound_table = 'product';
	

			$this->db->where($ultra_sound_where);
			$this->db->limit(50);
			// $this->db->join('store_product','store_product.product_id = product.product_id','LEFT');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('requisition/store_product_list',$data,true);
		$response['message'] = 'success';
		$response['results'] = $page;

		
		

		echo json_encode($response);

	}

	public function add_requisition_supplier_orders($product_id,$requisition_id,$requesting_store_id)
	{	
		$ordering_store_id = $requesting_store_id;
		if($product_id > 0 AND $requisition_id > 0 AND $requesting_store_id > 0)
		{

			$this->db->where('product_id = '.$product_id.' AND order_item.order_item_delete = 0 AND requisition_id = '.$requisition_id.'');
			$query = $this->db->get('order_item');

			if($query->num_rows() == 0)
			{

				$data = array(
								'order_id'=>0,
								'product_id'=>$product_id,
								'requisition_id'=>$requisition_id,
								'order_item_quantity'=>0,
								'in_stock'=>0
							);
						
				if($this->db->insert('order_item', $data))
				{

					$order_item_id = $this->db->insert_id();

					$creditor_id = 0;
					$quantity = 0;
					$unit_price = 0;
					$order_id = 0;
					$created = date('Y-m-d');

					$data = array(
							'supplier_id'=>0,
							'order_item_id'=>$order_item_id,
							'order_id'=>0,
							'requisition_id'=>$requisition_id,
							'product_id'=>$product_id,
							'store_id'=>$requesting_store_id,
							'created'=>date('Y-m-d')				
						);
					$data['quantity']=0;
					$data['unit_price']=0;

					$this->db->insert('order_supplier',$data);
				}
					

			}
		
			


			$response['message'] = 'success';
		
		}
		else
		{

			$response['message'] = 'success';
		}
		echo json_encode($response);
	}

	public function delete_order_item($order_item_id,$requisition_id)
	{

	
		$requisition_add['order_item_delete'] = 1;
		$requisition_add['order_item_quantity'] = 0;
		$requisition_add['purchase_quantity'] = 0;
		

		$this->db->where('order_item_id = '.$order_item_id);
		$this->db->update('order_item',$requisition_add);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}
	public function update_supplier_order_items($order_item_id,$requisition_id)
	{

		$units = $this->input->post('units');
		$order_item_instruction = $this->input->post('order_item_instruction');
	    $current_stock = $this->input->post('current_stock');
		$requisition_add['order_item_quantity'] = $units;
		$requisition_add['order_item_instruction'] = $order_item_instruction;
		$requisition_add['current_stock'] = $current_stock;
		$requisition_add['purchase_quantity'] = 0;
		

		$this->db->where('order_item_id = '.$order_item_id);
		$this->db->update('order_item',$requisition_add);
		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}
	public function complete_store_requisition($requisition_id,$store_id)
	{

	
		$requisition_add['requisition_status'] = 1;
		$requisition_add['reason'] = $this->input->post('reason');
		$requisition_add['date_sent'] = date('Y-m-d H:i:s');
		$requisition_add['sent_by'] = $this->session->userdata('personnel_id');
		$this->db->where('requisition_id = '.$requisition_id);
		$this->db->update('requisition',$requisition_add);


		// $requisition_order['order_approval_status'] = 2;
		// $this->db->where('requisition_id = '.$requisition_id);
		// $this->db->update('orders',$requisition_order);

		redirect('procurement/requisitions');
	}

	

	
	public function supplier_requisition_items($requisition_id,$store_id)
	{


		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0';
			$ultra_sound_table = 'product,order_item';
			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0';
			$ultra_sound_table = 'product,order_item';

			$this->db->where($ultra_sound_where);
			$this->db->limit(100);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('suppliers_requisitions/supplier_requisition_items',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}


	public function stores_quantity($product_id,$requisition_id,$requesting_store_id)
	{
		

		$data = array('requisition_id'=>$requisition_id,'product_id'=>$product_id,'requesting_store_id'=>$requesting_store_id);


		$ultra_sound_where = 'product.product_id = store_product.product_id AND store.store_id = store_product.owning_store_id AND store_product.product_id = '.$product_id.' AND store_product.owning_store_id <> '.$requesting_store_id;
			$ultra_sound_table = 'product,store_product,store';

		$this->db->where($ultra_sound_where);
		$this->db->limit(1000);
		$query = $this->db->get($ultra_sound_table);


		$data['query'] = $query;
		$page = $this->load->view('requisition/store_quantities',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}

	public function requisition_items($requisition_id,$store_id)
	{


		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = product_deductions.product_id AND orders.requisition_id = '.$requisition_id.' AND orders.order_id = product_deductions.order_id AND store.store_id = orders.store_id AND product_deductions.product_deductions_status = 0';
			$ultra_sound_table = 'product,orders,product_deductions,store';
			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = product_deductions.product_id AND orders.requisition_id = '.$requisition_id.' AND orders.order_id = product_deductions.order_id AND store.store_id = orders.store_id AND product_deductions.product_deductions_status = 0';
			$ultra_sound_table = 'product,orders,product_deductions,store';

			$this->db->where($ultra_sound_where);
			$this->db->limit(100);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('requisition/store_requisition_items',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}


	public function print_requisition_items($requisition_id,$store_id)
	{


		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = product_deductions.product_id AND orders.requisition_id = '.$requisition_id.' AND orders.order_id = product_deductions.order_id AND store.store_id = orders.store_id AND product_deductions.product_deductions_status = 0';
			$ultra_sound_table = 'product,orders,product_deductions,store,';
			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = product_deductions.product_id AND orders.requisition_id = '.$requisition_id.' AND orders.order_id = product_deductions.order_id AND store.store_id = orders.store_id AND product_deductions.product_deductions_status = 0 ';
			$ultra_sound_table = 'product,orders,product_deductions,store';

			$this->db->where($ultra_sound_where);
			$this->db->limit(100);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}


		$data['query'] = $query;

		$data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('requisition/print_store_requisition_items', $data);

		//$response['message'] = 'success';
		//$response['results'] = $page;
		//echo json_encode($response);
	}

	public function print_supplier_requisition_items($requisition_id,$store_id)
	{


		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0';
			$ultra_sound_table = 'product,order_item';
			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0';
			$ultra_sound_table = 'product,order_item';

			$this->db->where($ultra_sound_where);
			$this->db->limit(100);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$data['contacts'] = $this->site_model->get_contacts();
		//$page = $this->load->view('suppliers_requisitions/print_supplier_requisition_items',$data,true);

		$this->load->view('suppliers_requisitions/print_supplier_requisition_items', $data);

		//$response['message'] = 'success';
		//$response['results'] = $page;
		//echo json_encode($response);
	}


// public function print_product_stickers()
// 	{

		
// 		$where = 'product.category_id = category.category_id AND product.product_deleted = 0 AND brand.brand_id = product.brand_id AND product.product_id = store_product.product_id';
// 		$table = 'product, category, brand,store_product';

// 		//echo $where;die();

// 		$product_inventory_search = $this->session->userdata('product_inventory_search');

// 		if(!empty($product_inventory_search))
// 		{
// 			$where .= $product_inventory_search;
// 		}
// 		$query = $this->products_model->get_product_items($table, $where);

// 		// $v_data['store_id'] = $store_id;

// 		$data['title'] = 'Stickers';
// 		$v_data['title'] = $data['title'];
// 		$v_data['query'] = $query;
// 		// $v_data['branches'] = $branches;

// 		$this->load->view('order/product_stickers', $v_data);

	


	public function add_requisition_orders($product_id,$requisition_id,$requesting_store_id,$store_id)
	{	

		$ultra_sound_where = 'product_deleted = 0';
		$ultra_sound_table = 'product';
		$ultra_sound_where .= ' AND product.product_id = '.$product_id;

		$this->db->where($ultra_sound_where);
		$this->db->limit(1000);
		// $this->db->order_by('product.product_name','ASC');
		$query = $this->db->get($ultra_sound_table);

		$product_buying_price = 0;
		$product_unitprice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_buying_price = $value->product_buying_price;
				$product_unitprice = $value->product_unitprice;
			}
		}

		if(empty($product_buying_price))
		{
			$product_buying_price = 0;
		}

		if(empty($product_unitprice))
		{
			$product_unitprice = 0;
		}


		$ordering_store_id = $requesting_store_id;
		if($product_id > 0 AND $requisition_id > 0 AND $requesting_store_id > 0 AND $store_id > 0)
		{

			$this->db->where('requisition_id ='.$requisition_id.' AND store_id = '.$store_id);
			$checked_order = $this->db->get('orders');


			if($checked_order->num_rows() > 0)
			{
				foreach ($checked_order->result() as $key => $value_checked) {
					// code...
					$order_id = $value_checked->order_id;
				}

				if($order_id > 0)
				{
					$this->db->where('product_id = '.$product_id.' AND owning_store_id = '.$ordering_store_id.'');
					$query = $this->db->get('store_product');

					if($query->num_rows() > 0)
					{

					}
					else
					{
						$array_ten['product_id'] = $product_id;
						$array_ten['owning_store_id'] = $ordering_store_id;
						$array_ten['created'] = date('Y-m-d');
						$array_ten['store_balance'] = 0;
						$array_ten['created_by'] = $this->session->userdata('personnel_id');
						$this->db->insert('store_product',$array_ten);

					}


					$this->db->where('product_id = '.$product_id.' AND owning_store_id = '.$ordering_store_id.'');
					$query = $this->db->get('store_product');

					if($query->num_rows() > 0)
					{

					}

					// check if it exisits
					$data = array('store_id' => $ordering_store_id, 'order_id' => $order_id, 'product_id'=> $product_id,'date_requested'=>date('Y-m-d H:i:s'),'search_date'=>date('Y-m-d'),'requested_by'=>$this->session->userdata('personnel_id'),'product_deductions_status'=>0,'product_deductions_pack_size'=>1,'product_deductions_quantity'=>0,'quantity_requested'=>0,'product_buying_price'=>$product_buying_price,'selling_price'=>$product_unitprice);

					$this->db->insert('product_deductions', $data);
				}

			}
			else
			{

			

				$order_type = $this->inventory_management_model->is_store_parent($store_id);

				$array = array(
					'orders_date'=>$this->input->post('orders_date'),
					'store_id'=>$store_id,
					'order_number'=>$this->inventory_management_model->create_order_number(),
					'created'=>date('Y-m-d H:i:s'),
					'supplier_id'=>0,
					'created_by'=>$this->session->userdata('personnel_id'),
					'modified_by'=>$this->session->userdata('personnel_id'),
					'order_type_id'=>$order_type,
					'ordering_store_id'=>$requesting_store_id,
					'is_store'=>1,
					'requisition_id'=>$requisition_id
				);
				if($this->db->insert('orders', $array))
				{
					$order_id = $this->db->insert_id();
					
					


					$this->db->where('product_id = '.$product_id.' AND owning_store_id = '.$ordering_store_id.'');
					$query = $this->db->get('store_product');

					if($query->num_rows() > 0)
					{

					}
					else
					{
						$array_tenss['product_id'] = $product_id;
						$array_tenss['owning_store_id'] = $ordering_store_id;
						$array_tenss['created'] = date('Y-m-d');
						$array_tenss['store_balance'] = 0;
						$array_tenss['created_by'] = $this->session->userdata('personnel_id');
						$this->db->insert('store_product',$array_tenss);

					}

					$data = array('store_id' => $ordering_store_id, 'order_id' => $order_id, 'product_id'=> $product_id,'date_requested'=>date('Y-m-d H:i:s'),'search_date'=>date('Y-m-d'),'requested_by'=>$this->session->userdata('personnel_id'),'product_deductions_status'=>0,'product_deductions_pack_size'=>1,'product_deductions_quantity'=>0,'quantity_requested'=>0,'product_buying_price'=>$product_buying_price,'selling_price'=>$product_unitprice);

					$this->db->insert('product_deductions', $data);
					
				}
			}
				
			


			$response['message'] = 'success';
		
		}
		else
		{

			$response['message'] = 'success';
		}
		echo json_encode($response);
	}

	public function update_store_order_items($product_deductions_id,$requisition_id)
	{

		$units = $this->input->post('units');
		$requisition_add['quantity_requested'] = $units;
		$requisition_add['product_deductions_quantity'] = $units;
		

		$this->db->where('product_deductions_id = '.$product_deductions_id);
		$this->db->update('product_deductions',$requisition_add);
		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function delete_deduction_item($product_deductions_id,$requisition_id)
	{

	
		$requisition_add['product_deductions_status'] = 1;
		$requisition_add['quantity_requested'] = 0;
		$requisition_add['product_deductions_quantity'] = 0;
		

		$this->db->where('product_deductions_id = '.$product_deductions_id);
		$this->db->update('product_deductions',$requisition_add);

		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}

	public function update_requisition_detail($requisition_id,$store_id)
	{

	
		$requisition_add['requisition_status'] = 1;
		$requisition_add['date_sent'] = date('Y-m-d H:i:s');
		$requisition_add['sent_by'] = $this->session->userdata('personnel_id');
		$this->db->where('requisition_id = '.$requisition_id);
		$this->db->update('requisition',$requisition_add);


		$requisition_order['order_approval_status'] = 6;
		$this->db->where('requisition_id = '.$requisition_id);
		$this->db->update('orders',$requisition_order);

		redirect('procurement/store-orders');
	}

	public function print_requisition_order($requisition_id,$store_id)
	{

	
		$requisition_add['requisition_status'] = 1;
		$requisition_add['date_sent'] = date('Y-m-d H:i:s');
		$requisition_add['sent_by'] = $this->session->userdata('personnel_id');
		$this->db->where('requisition_id = '.$requisition_id);
		$this->db->update('requisition',$requisition_add);


		$requisition_order['order_approval_status'] = 7;
		$this->db->where('requisition_id = '.$requisition_id);
		$this->db->update('orders',$requisition_order);

		redirect('procurement/store-orders');
	}

	public function store_deductions()
	{

		$data['title'] = 'Store Management';
		$store_priviledges = $this->inventory_management_model->get_assigned_stores();
		$v_data['store_priviledges'] =  $store_priviledges;

		$store_id = 0;
		$addition = '';

		$constant  = '';
		$table = '';
		$personnel_id = $this->session->userdata('personnel_id');
		if($store_priviledges->num_rows() > 0 )
		{
			$count = 0;
			$number_rows = $store_priviledges->num_rows();
			$constant .= ' AND (';
			if($number_rows > 1)
			{
				$v_data['type'] = 3;
			}

			else
			{
				$v_data['type'] = 2;
			}

			foreach ($store_priviledges->result() as $key)
			{
				# code...
				$store_parent = $key->store_parent;
				$store_id = $key->store_id;
				$constant .= ' requisition.owning_store_id ='.$store_id;
				$count++;
				if($count < $number_rows)
				{
					$constant .= ' OR ';
				}
			}
			$constant .= ' )';
		}
		else
		{
			$v_data['type'] = 4;
			$table = '';
			$constant = ' AND  store_product.owning_store_id = store.store_id  AND store.store_status = 1 AND store.store_deleted = 0';
			$addition = '';
		}



		
		$where = 'orders.store_id = store.store_id AND requisition.requisition_id = orders.requisition_id AND requisition.requisition_type_id = 1 AND requisition.requisition_status > 0 ';
		$table = 'requisition,orders,store';
		
		$search = $this->session->userdata('store_requisition_search');


		if(!empty($search))
		{
			$where .= $search;
		}
		else
		{
			$where .= ' AND DATE(requisition.created) = "'.date('Y-m-d').'" ';
		}
        

		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'inventory/store-deductions';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_all_requisitions_orders($table, $where, $config["per_page"], $page);


		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$v_data['title'] =  'Store Management';

		$data['content'] = $this->load->view('requisition/store_deductions_orders', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}


	public function view_all_product_deductions($requisition_id,$order_id)
	{
		$segment = 3;

		
		$table = 'product_deductions,store,product,orders';

		$order = 'product_deductions.product_deductions_date';
		$where = 'product_deductions.store_id = store.store_id AND product.product_deleted = 0 AND product_deductions.quantity_requested > 0 AND product_deductions.product_id = product.product_id AND product_deductions.order_id = orders.order_id AND orders.order_id = product_deductions.order_id AND orders.order_id = '.$order_id;



		$v_data['requisition_id'] = $requisition_id;
		$v_data['order_id'] = $order_id;

		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('requisition/view_store_requested_items', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);




    }

    public function store_ordered_items($requisition_id,$order_id)
    {

    	$ultra_sound_table = 'product_deductions,store,product,orders';

		$order = 'product_deductions.product_deductions_date';
		$ultra_sound_where = 'product_deductions.store_id = store.store_id AND product.product_deleted = 0 AND product_deductions.quantity_requested > 0 AND product_deductions.product_id = product.product_id AND product_deductions.order_id = orders.order_id AND orders.order_id = product_deductions.order_id AND orders.order_id = '.$order_id;

		$drug_name = $this->input->post('drug');
		$query = null;
		if(!empty($drug_name))
		{

			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$drug_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$data['requisition_id'] = $requisition_id;
		$data['order_id'] = $order_id;
		$page = $this->load->view('requisition/ordered_product_list',$data,true);
		$response['message'] = 'success';
		$response['results'] = $page;

		
		

		echo json_encode($response);
    }

    public function update_quantity_given($product_deductions_id,$requisition_id,$order_id,$product_id)
	{

		$this->db->where('order_id',$order_id);
		$orders_query = $this->db->get('orders');
		if($orders_query->num_rows() > 0)
		{
			foreach ($orders_query->result() as $key => $value) {
				// code...
				$store_id = $value->store_id;
			}
		}
		
		$requisition_add['quantity_given'] = 0;
		$this->db->where('product_deductions_id = '.$product_deductions_id);
		$this->db->update('product_deductions',$requisition_add);
		$this->inventory_management_model->update_product_counts_items($product_id);

		$units = $this->input->post('units');
		$available_units = $this->inventory_management_model->get_store_available_units($product_id,$store_id);

		if($units <= $available_units)
		{
			$requisition_add['quantity_given'] = $units;
			$this->db->where('product_deductions_id = '.$product_deductions_id);
			$this->db->update('product_deductions',$requisition_add);

			// $this->inventory_management_model->update_store_items($product_id,$store_id);
			$this->inventory_management_model->update_product_counts_items($product_id);

			$response['message'] = 'success';
			$response['results'] = 'You have successfully awarded the units';

		}
		else
		{
			$response['message'] = 'fail';
			$response['results'] = 'Sorry the total available units is '.$available_units;
		}

		
		
		echo json_encode($response);

	}



	public function complate_store_requisition($requisition_id,$order_id)
	{

		$requisition_order['closed_date'] = date('Y-m-d H:i:s');
		$requisition_order['closed_by'] = $this->session->userdata('personnel_id');
		$requisition_order['order_approval_status'] = 7;
		
		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders',$requisition_order);

		redirect('inventory/store-deductions');
	}


	public function supplier_order_details($requisition_id,$store_id)
	{
		$v_data['requisition_id'] = $requisition_id;
		$v_data['store_id'] = $store_id;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('suppliers_requisitions/suppliers_orders_details', $v_data, true);
		$data['title'] = 'All orders';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function requisition_items_to_supply($requisition_id,$store_id)
	{


		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$product_name = $this->input->post('drug');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0 AND order_item.order_id = 0';
			$ultra_sound_table = 'product,order_item';
			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0 AND order_item.order_id = 0';
			$ultra_sound_table = 'product,order_item';

			$this->db->where($ultra_sound_where);
			$this->db->limit(100);
			// $this->db->order_by('product.product_name','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('suppliers_requisitions/supplier_requisitions_orders',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}

	public function suppliers_list($order_item_id,$requisition_id,$requesting_store_id)
	{
	
		$this->db->where('order_item_id',$order_item_id);
		$query_item = $this->db->get('order_item');

		if($query_item->num_rows() > 0)
		{
			foreach ($query_item->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;
			}
		}	

		$data = array('requisition_id'=>$requisition_id,'order_item_id'=>$order_item_id,'requesting_store_id'=>$requesting_store_id,'product_id'=>$product_id);
		$product_name = $this->input->post('supplier_search
');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'creditor_status = 0 ';
			$ultra_sound_table = 'creditor';
			$ultra_sound_where .= ' AND creditor_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			// $this->db->order_by('creditor_name','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{

			$ultra_sound_where = 'creditor_status = 0 ';
			$ultra_sound_table = 'creditor';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			$query = $this->db->get($ultra_sound_table);
		}


		$data['query'] = $query;
		$page = $this->load->view('suppliers_requisitions/suppliers_list',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}
	public function add_requisition_supplier_orders_to_supplier($product_id,$requisition_id,$requesting_store_id,$supplier_id,$order_item_id)
	{	
		$ordering_store_id = $requesting_store_id;
		if($product_id > 0 AND $requisition_id > 0 AND $requesting_store_id > 0)
		{

			$this->db->where('requisition_id ='.$requisition_id.' AND store_id = '.$requesting_store_id.' AND supplier_id = '.$supplier_id);
			$checked_order = $this->db->get('orders');


			if($checked_order->num_rows() > 0)
			{
				foreach ($checked_order->result() as $key => $value_checked) {
					// code...
					$order_id = $value_checked->order_id;
				}

				if($order_id > 0)
				{
					

				

						$data = array(
									'order_id'=>$order_id
								);
						$this->db->where('order_item_id',$order_item_id);
						if($this->db->update('order_item', $data))
						{

							

							$creditor_id = $supplier_id;
							$quantity = 0;
							$unit_price = 0;
							$order_id = $order_id;
							$created = date('Y-m-d');

							$data_order = array(
											'supplier_id'=>$supplier_id,
											'order_id'=>$order_id				
										);
							$this->db->where('product_id = '.$product_id.' AND order_item_id ='.$order_item_id);
							$this->db->update('order_supplier',$data_order);
						}
							

					

					// check if it exisits

				}

			}
			else
			{

			

				$order_type = $this->inventory_management_model->is_store_parent($ordering_store_id);

				$array = array(
					'orders_date'=>date('Y-m-d'),
					'store_id'=>$ordering_store_id,
					'order_number'=>$this->inventory_management_model->create_order_number(),
					'created'=>date('Y-m-d H:i:s'),
					'supplier_id'=>$supplier_id,
					'created_by'=>$this->session->userdata('personnel_id'),
					'modified_by'=>$this->session->userdata('personnel_id'),
					'order_type_id'=>$order_type,
					'ordering_store_id'=>$ordering_store_id,
					'is_store'=>0,
					'requisition_id'=>$requisition_id
				);
				if($this->db->insert('orders', $array))
				{
					$order_id = $this->db->insert_id();
					
					


					$data = array(
									'order_id'=>$order_id
								);
					$this->db->where('order_item_id',$order_item_id);
					if($this->db->update('order_item', $data))
					{

						$creditor_id = $supplier_id;
						$quantity = 0;
						$unit_price = 0;
						$order_id = $order_id;
						$created = date('Y-m-d');

						$data_order = array(
										'supplier_id'=>$supplier_id,
										'order_id'=>$order_id				
									);
						$this->db->where('product_id = '.$product_id.' AND order_item_id ='.$order_item_id);
						$this->db->update('order_supplier',$data_order);
					}
					
				}
			}
			$response['message'] = 'success';
		
		}
		else
		{

			$response['message'] = 'success';
		}
		echo json_encode($response);
	}

	public function view_items_to_supply($requisition_id,$store_id)
	{
		$data = array('requisition_id'=>$requisition_id,'store_id'=>$store_id);


		$product_name = $this->input->post('product_name');
		$query = null;
		if(!empty($product_name))
		{

			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0 AND orders.order_id = order_item.order_id AND creditor.creditor_id = orders.supplier_id AND order_item.order_item_id = order_supplier.order_item_id';
			$ultra_sound_table = 'product,order_item,orders,creditor,order_supplier';
			$ultra_sound_where .= ' AND product.product_name LIKE \'%'.$product_name.'%\'';

			$this->db->where($ultra_sound_where);
			$this->db->limit(1000);
			$this->db->order_by('orders.supplier_id','ASC');
			$query = $this->db->get($ultra_sound_table);

		}
		else
		{
			$ultra_sound_where = 'product_deleted = 0 AND product.product_id = order_item.product_id AND order_item.requisition_id = '.$requisition_id.'   AND order_item.order_item_delete = 0 AND orders.order_id = order_item.order_id AND creditor.creditor_id = orders.supplier_id AND order_item.order_item_id = order_supplier.order_item_id';
			$ultra_sound_table = 'product,order_item,orders,creditor,order_supplier';

			$this->db->where($ultra_sound_where);
			$this->db->limit(100);
			$this->db->order_by('orders.supplier_id','ASC');
			$query = $this->db->get($ultra_sound_table);
		}
		$data['query'] = $query;
		$page = $this->load->view('suppliers_requisitions/view_items_to_supply',$data,true);

		$response['message'] = 'success';
		$response['results'] = $page;
		echo json_encode($response);
	}

	public function update_ordered_item($order_item_id,$requisition_id)
	{

		$units = $this->input->post('units');
		$supplier_unit_price = $this->input->post('supplier_price');
		$discount = $this->input->post('discount');
		$vat = $this->input->post('vat');

		$requisition_add['order_item_quantity'] = $units;
		$requisition_add['supplier_unit_price'] = $supplier_unit_price;
		$requisition_add['purchase_quantity'] = $units;
		

		$this->db->where('order_item_id = '.$order_item_id);
		$this->db->update('order_item',$requisition_add);


		$gross_amount = $supplier_unit_price * ($units);

		if($discount > 0)
		{
			$less_vat = $gross_amount * ((100 - $discount)/100);
		}
		else
		{
			$discount = 0;
			$less_vat  = $gross_amount;
		}

		if($vat > 0)
		{
			$less_vat = $less_vat * ((100+$vat)/100);
		}
		else
		{
			$vat = 0;
			// $less_vat = $gross_amount;
		}
		// var_dump($discount);die();
		$gross_amount = $less_vat * ((100+$vat)/100);


		$order_supplied['quantity'] = $units;
		$order_supplied['buying_unit_price'] = $supplier_unit_price;
		$order_supplied['quantity_received'] = $units;
		$order_supplied['pack_size'] = 1;
		$order_supplied['discount'] = $discount;
		$order_supplied['vat'] = $vat;
		$order_supplied['less_vat'] = $less_vat;
		$order_supplied['unit_price'] = $supplier_unit_price;
		$order_supplied['total_amount'] = $less_vat;

		// var_dump($order_supplied);die();

		$this->db->where('order_item_id = '.$order_item_id.' AND requisition_id ='.$requisition_id);
		$this->db->update('order_supplier',$order_supplied);



		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}
	public function delete_item_to_supply($order_item_id,$requisition_id)
	{
		// $requisition_add['order_item_delete'] = 1;
		// $requisition_add['supplier_id'] = 0;
		$requisition_add['order_id'] = 0;
		$requisition_add['supplier_unit_price'] = 0;

		$this->db->where('order_item_id = '.$order_item_id);
		$this->db->update('order_item',$requisition_add);



		$order_supplied['quantity'] = 0;
		$order_supplied['unit_price'] = 0;
		$order_supplied['quantity_received'] = 0;
		$order_supplied['pack_size'] = 0;
		$order_supplied['discount'] = 0;
		$order_supplied['vat'] = 0;
		$order_supplied['less_vat'] = 0;
		$order_supplied['unit_price'] = 0;
		$order_supplied['total_amount'] = 0;

		$this->db->where('order_item_id = '.$order_item_id.' AND requisition_id ='.$requisition_id);
		$this->db->update('order_supplier',$order_supplied);
		

		$response['message'] = 'success';
		$response['results'] = '';
		echo json_encode($response);
	}
	public function complete_procurement_requisition($requisition_id,$store_id)
	{

		$this->db->where('orders.requisition_id ='.$requisition_id);
		$order_query = $this->db->get('orders');
		if($order_query->num_rows() > 0)
		{
			foreach ($order_query->result() as $key => $value) {
				// code...
				$order_id = $value->order_id;

				$this->db->where('order_id = '.$order_id.' AND requisition_id = '.$requisition_id);
				$query = $this->db->get('order_item');

				if($query->num_rows() > 0)
				{
					$requisition_order['order_sent_time'] = date('Y-m-d H:i:s');
					$requisition_order['order_sent_by'] = $this->session->userdata('personnel_id');
					$requisition_order['order_approval_status'] = 3;
					
					$this->db->where('requisition_id = '.$requisition_id);
					$this->db->update('orders',$requisition_order);

				}
			}
			$requisition_add['requisition_status'] = 2;
			$this->db->where('requisition_id',$requisition_id);
			$this->db->update('requisition',$requisition_add);
		}

		

		redirect('procurement/requisitions');
	}


	public function view_requisition_orders()
	{
		$data['title'] = 'REQUISITION ORDERS';
		$v_data['title'] = $data['title'];
		// $v_data['requisition_id'] = $requisition_id;
		$this->session->set_userdata('supplier_invoice_search');
		$check_level_approval = FALSE;
		$personnel_id = $this->session->userdata('personnel_id');
		if($personnel_id > 0)
		{
			// $check_level_approval = $this->orders_model->check_assigned_next_approval($order_approval_status);
		}
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');		
		$v_data['query'] = '';
		$v_data['page'] = 0;
		$data['content'] = $this->load->view('suppliers_requisitions/requisitions_orders', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function get_all_orders()
	{
		$query = $this->requisition_model->get_all_requisition_orders();
    	$data['query'] = $query;
    	// $data['requisition_id'] = $requisition_id;
		$page = $this->load->view('suppliers_requisitions/requisitions_orders_list',$data,true);
		$data['results'] = $page;
		$data['message'] = 'success';
		echo json_encode($data);
	}

		function delete_charged_item($requisition_id)
	{
		$visit_data = array('requisition_delete'=>1);

		$this->db->where(array("requisition_id"=>$requisition_id));
		$this->db->update('requisition', $visit_data);
		$response['message'] = 'success';
		$response['result'] = 'youve successfully deleted the LPO';
		echo json_encode($response);
	}

		/*
	*
	*	Delete an existing Requsition Item
	*	@param int $requisition_id
	*
	*/
	public function delete_requisition($requisition_id)
	{
		if($this->requisition_model->delete_requisition($requisition_id))
		{
			$this->session->set_userdata('success_message', 'Personnel has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Personnel could not deleted');
		}
		redirect('procurement/local-purchase-orders');
	}

		/*
	*
	*	Reversal 
	*	@param int $requisition_id
	*
	*/
	public function reverse_order($requisition_id)
	{
		if($this->requisition_model->reversal($requisition_id))
		{
			$this->session->set_userdata('success_message', 'Reversal Completed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Reversal Completed');
		}
		redirect('procurement/local-purchase-orders');
	}

			/*
	*
	*	Correction 
	*	@param int $requisition_id
	*
	*/
	public function correct_order($requisition_id)
	{
		if($this->requisition_model->correct($requisition_id))
		{
			$this->session->set_userdata('success_message', 'Correct the requisition');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Correct the requisition');
		}
		redirect('procurement/request');
	}


	public function view_requisition_details($requisition_id,$order_id,$creditor_id)
	{
		$data['order_id'] = $order_id;
		$page = $this->load->view('suppliers_requisitions/order_items_list',$data,true);

		$invoice = $this->requisition_model->get_order_detail($order_id);
		$row = $invoice->row();
		$order_number = $row->order_number;
		$order_approval_status = $row->order_approval_status;
		$lpo_number = $row->lpo_number;
		$authorised_by = $row->authorised_by;
		$billing_status = $row->billing_status;


		$creditor = $this->requisition_model->get_creditor($creditor_id);
		$row = $creditor->row();
		$creditor_name = $row->creditor_name;
		$personnel_id = $this->session->userdata('personnel_id');
		$add = '';
		if($order_approval_status == 3)
		{
			$add = '<a href="'.site_url().'print-order-requisition/'.$order_id.'" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-print"></i> Print</a>
					<a onclick="generate_lpo('.$order_id.','.$requisition_id.','.$creditor_id.')" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-correct"></i>GENERATE LPO</a>';
		}
		else if($order_approval_status == 4)
		{
			$add = '<strong>LPO NO.:</strong> <span style="font-weight: normal;">'.$lpo_number.' </span> 
					<a href="'.site_url().'print-lpo/'.$order_id.'/'.$creditor_id.'" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-correct"></i>PRINT PROVISIONAL LPO</a>
				   <a onclick="approve_lpo_financials('.$order_id.','.$requisition_id.','.$creditor_id.')" class="btn btn-xs btn-info" target="_blank"><i class="fa fa-correct"></i>APPROVE LPO</a>';
		}
		else if($order_approval_status == 5)
		{
			if($authorised_by > 0 AND $billing_status == 1)
			{

				$add = '<strong>LPO NO.:</strong> <span style="font-weight: normal;">'.$lpo_number.' </span> 
					<a href="'.site_url().'print-lpo/'.$order_id.'/'.$creditor_id.'" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-correct"></i>PRINT LPO</a>';

			}
			else
			{

				if($personnel_id == 1281)
				{
					$add = '<strong>LPO NO.:</strong> <span style="font-weight: normal;">'.$lpo_number.' </span> 
						<a href="'.site_url().'print-lpo/'.$order_id.'/'.$creditor_id.'" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-correct"></i>PRINT LPO</a>
						<a onclick="authorise_lpo('.$order_id.','.$requisition_id.','.$creditor_id.')" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-correct"></i>AUTHORISE  '.$authorised_by.'</a>';
				}
				else
				{
					$add = '<strong>LPO NO.:</strong> <span style="font-weight: normal;">'.$lpo_number.' </span> 
						<a href="'.site_url().'print-lpo/'.$order_id.'/'.$creditor_id.'" class="btn btn-xs btn-success" target="_blank"><i class="fa fa-correct"></i>PRINT LPO</a>';
				}
				
			}
			
		}
    	
    	$data['header'] = '<strong>Name:</strong> <span style="font-weight: normal;">'.$creditor_name.' <strong>Order No.:</strong> <span style="font-weight: normal;">'.$order_number.' </span> 
    	
    	'.$add.'';

		$data['results'] = $page;
		$data['message'] = 'success';
		echo json_encode($data);
	}

	public function generate_lpo($order_id,$requisition_id,$creditor_id)
	{
		$prefix = 'CMC';
		$suffix = $this->inventory_management_model->create_lpo_number($prefix);

		$lpo_number = $prefix.'.'.$suffix;

		$requisition_order['lpo_number'] = $lpo_number;
		$requisition_order['lpo_generated_date'] = date('Y-m-d H:i:s');
		$requisition_order['lpo_generated_by'] = $this->session->userdata('personnel_id');
		$requisition_order['order_approval_status'] = 4;
		$requisition_order['lpo_prefix'] = $prefix;
		$requisition_order['lpo_suffix'] = $suffix;
		
		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders',$requisition_order);

		$data['message'] = 'success';
		echo json_encode($data);
	}


	public function approve_lpo_financials($order_id,$requisition_id,$creditor_id)
	{

		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{

			$validate = $this->requisition_model->confirm_user();
			if($validate)
			{
				$requisition_order['approved_by'] = $this->session->userdata('personnel_id');
				$requisition_order['approved_date'] = date('Y-m-d H:i:s');
				$requisition_order['order_approval_status'] = 5;
				$this->db->where('order_id = '.$order_id);
				$this->db->update('orders',$requisition_order);

				$data['message'] = 'success';

			}
			else
			{
				$data['message'] = 'fail';
				$data['fail'] = 'Sorry could not authenticate the user.';
			}
			
			
		}
		else
		{
			$validation_errors = validation_errors();
		
		
			$data['message'] = 'fail';
			$data['result'] = strip_tags($validation_errors);
			
		}
		echo json_encode($data);
	}

	public function authorise_lpo($order_id,$requisition_id,$creditor_id)
	{

		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[personnel.personnel_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{

			$validate = $this->requisition_model->confirm_user();
			if($validate)
			{
				$requisition_order['authorised_by'] = $this->session->userdata('personnel_id');
				$requisition_order['authorised_date'] = date('Y-m-d H:i:s');
				$requisition_order['order_approval_status'] = 5;
				$requisition_order['billing_status'] = 1;
				$this->db->where('order_id = '.$order_id);
				$this->db->update('orders',$requisition_order);
				$data['message'] = 'success';

			}
			else
			{
				$data['message'] = 'fail';
				$data['fail'] = 'Sorry could not authenticate the user.';
			}
			
			
		}
		else
		{
			$validation_errors = validation_errors();
		
		
			$data['message'] = 'fail';
			$data['result'] = strip_tags($validation_errors);
			
		}
		echo json_encode($data);

	
	}

	public function print_lpo_new($order_id,$creditor_id)
	{
		$data = array('supplier_order_id'=>$order_id,'creditor_id'=>$creditor_id);

		$data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('suppliers_requisitions/lpo_print', $data);

	}

	public function procurement_list()
	{
		// get my approval roles

		$personnel_id = $this->session->userdata('personnel_id');
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

		if($personnel_id == 0 OR $authorize_invoice_changes)
		{
			$where = 'requisition_id > 0 AND requisition_type_id = 2  AND requisition_status >= 1  AND store.store_id = requisition.store_id';
		}
		else
		{
			$where = 'requisition_id > 0 AND requisition_type_id = 2 AND requisition_status >= 1 AND store.store_id = requisition.store_id';

			
		}
		
		$table = 'requisition,store';

	

		$search = $this->session->userdata('procurement_requisition_search');


		if(!empty($search))
		{
		$where .= $search;
		}

		else
		{
			$where .= ' AND DATE(requisition.created) = "'.date('Y-m-d').'" ';
		}
        
        

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/procurement-requisitions';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 3;
		$config['per_page'] = 40;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_all_requisitions($table, $where, $config["per_page"], $page);

		$store_priviledges = $this->inventory_management_model->get_assigned_stores();
		$v_data['store_priviledges'] =  $store_priviledges;


		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "PROCUMENT REQUISITIONS";
		$data['content'] = $this->load->view('suppliers_requisitions/procurement_requisitions', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}


	public function search_store_orders()
	{
	
		$requisition_number = $this->input->post('requisition_number');
		$date_time = $this->input->post('date_time');
		$visit_date_from = $this->input->post('date_from');
		$visit_date_to = $this->input->post('date_to');
		

		$search_title = '';
		if(!empty($requisition_number))
		{
			$search_title .= ' Number :  '.$requisition_number;
			$requisition_number = ' AND requisition.requisition_number LIKE \'%'.$requisition_number.'%\'';
		}
		

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$providers_date_from = $visit_date_from;
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from) && empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_from.'\'';
			$providers_date_from = $visit_date_from;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to) && empty($visit_date_from))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_to.'\'';
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date_checked = '';
			$providers_date_from = '';
			$providers_date_to = '';
		}
		// var_dump($visit_date_checked);die();
		
		$list_view = '';

		// var_dump($date_time);die();
		if(!empty($date_time))
		{

			if($date_time == 1)
			{
				$list_view = ' AND DATE(requisition.created) = \''.date('Y-m-d').'\' ';
				$search_title .= ' Today ';
			}
			else if($date_time == 2)
			{
				$visit_date = date('Y-m-d');
				$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
				$list_view = ' AND DATE(requisition.created) = \''.$visit_date.'\'';
				$search_title .= ' Yesterday ';

			}
			else if($date_time == 3)
			{		
				$visit_date = date('Y-m-d');
				$firstday = date('Y-m-d', strtotime("this week")); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$firstday.'\' AND \''.$visit_date.'\'  ';
				$search_title .= ' This Week ';
			}
			else if($date_time == 4)
			{
				

				$visit_date = date('Y-m-01');
				$last_day = date('Y-m-t'); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$visit_date.'\' AND \''.$last_day.'\'  ';
				$search_title .= ' This Month ';
			}
			
		}
		
		$search = $requisition_number.$list_view.$visit_date_checked;
		$this->session->set_userdata('store_requisition_search_title', $search_title);
		$this->session->set_userdata('store_requisition_search', $search);
		
		redirect('inventory/store-deductions');
	}

	public function search_requisition_orders()
	{

		$requisition_number = $this->input->post('requisition_number');
		$date_time = $this->input->post('date_time');
		$visit_date_from = $this->input->post('date_from');
		$visit_date_to = $this->input->post('date_to');
		

		$search_title = '';
		if(!empty($requisition_number))
		{
			$search_title .= ' Number :  '.$requisition_number;
			$requisition_number = ' AND requisition.requisition_number LIKE \'%'.$requisition_number.'%\'';
		}
		

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$providers_date_from = $visit_date_from;
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from) && empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_from.'\'';
			$providers_date_from = $visit_date_from;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to) && empty($visit_date_from))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_to.'\'';
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date_checked = '';
			$providers_date_from = '';
			$providers_date_to = '';
		}
		// var_dump($visit_date_checked);die();
		
		$list_view = '';

		// var_dump($date_time);die();
		if(!empty($date_time))
		{

			if($date_time == 1)
			{
				$list_view = ' AND DATE(requisition.created) = \''.date('Y-m-d').'\' ';
				$search_title .= ' Today ';
			}
			else if($date_time == 2)
			{
				$visit_date = date('Y-m-d');
				$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
				$list_view = ' AND DATE(requisition.created) = \''.$visit_date.'\'';
				$search_title .= ' Yesterday ';

			}
			else if($date_time == 3)
			{		
				$visit_date = date('Y-m-d');
				$firstday = date('Y-m-d', strtotime("this week")); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$firstday.'\' AND \''.$visit_date.'\'  ';
				$search_title .= ' This Week ';
			}
			else if($date_time == 4)
			{
				

				$visit_date = date('Y-m-01');
				$last_day = date('Y-m-t'); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$visit_date.'\' AND \''.$last_day.'\'  ';
				$search_title .= ' This Month ';
			}
			
		}
		
		$search = $requisition_number.$list_view.$visit_date_checked;
		$this->session->set_userdata('orders_requisition_search_title', $search_title);
		$this->session->set_userdata('orders_requisition_search', $search);
		
		redirect('procurement/store-orders');

	}


	public function search_procurement()
	{

		$requisition_number = $this->input->post('requisition_number');
		$date_time = $this->input->post('date_time');
		$visit_date_from = $this->input->post('date_from');
		$visit_date_to = $this->input->post('date_to');
		

		$search_title = '';
		if(!empty($requisition_number))
		{
			$search_title .= ' Number :  '.$requisition_number;
			$requisition_number = ' AND requisition.requisition_number LIKE \'%'.$requisition_number.'%\'';
		}
		

		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$providers_date_from = $visit_date_from;
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from) && empty($visit_date_to))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_from.'\'';
			$providers_date_from = $visit_date_from;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to) && empty($visit_date_from))
		{
			$visit_date_checked = ' AND DATE(requisition.created) = \''.$visit_date_to.'\'';
			$providers_date_to = $visit_date_to;
			$search_title .= ': '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date_checked = '';
			$providers_date_from = '';
			$providers_date_to = '';
		}
		
		$list_view = '';

		if(!empty($date_time))
		{

			if($date_time == 1)
			{
				$list_view = ' AND DATE(requisition.created) = \''.date('Y-m-d').'\' ';
				$search_title .= ' Today ';
			}
			else if($date_time == 2)
			{
				$visit_date = date('Y-m-d');
				$visit_date = date("Y-m-d", strtotime("-1 day", strtotime($visit_date)));
				$list_view = ' AND DATE(requisition.created) = \''.$visit_date.'\'';
				$search_title .= ' Yesterday ';

			}
			else if($date_time == 3)
			{		
				$visit_date = date('Y-m-d');
				$firstday = date('Y-m-d', strtotime("this week")); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$firstday.'\' AND \''.$visit_date.'\'  ';
				$search_title .= ' This Week ';
			}
			else if($date_time == 4)
			{
				

				$visit_date = date('Y-m-01');
				$last_day = date('Y-m-t'); 
				$list_view = ' AND DATE(requisition.created) BETWEEN \''.$visit_date.'\' AND \''.$last_day.'\'  ';
				$search_title .= ' This Month ';
			}
			
		}
		
		$search = $requisition_number.$list_view.$visit_date_checked;
		$this->session->set_userdata('procurement_requisition_search_title', $search_title);
		$this->session->set_userdata('procurement_requisition_search', $search);
		
		redirect('procurement/procurement-requisitions');

	}

	public function update_all_requisition_deductions()
	{
		$this->db->where('product_id > 0 AND quantity_requested > 0 AND product_buying_price = 0');
		$query = $this->db->get('product_deductions');


		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_deductions_id = $value->product_deductions_id;
				$product_id = $value->product_id;


				$ultra_sound_where = 'product_deleted = 0';
				$ultra_sound_table = 'product';
				$ultra_sound_where .= ' AND product.product_id = '.$product_id;

				$this->db->where($ultra_sound_where);
				$this->db->limit(1000);
				// $this->db->order_by('product.product_name','ASC');
				$query = $this->db->get($ultra_sound_table);

				$product_buying_price = 0;
				$product_unitprice = 0;
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...
						$product_buying_price = $value->product_buying_price;
						$product_unitprice = $value->product_unitprice;
					}
				}

				if(empty($product_buying_price))
				{
					$product_buying_price = 0;
				}

				if(empty($product_unitprice))
				{
					$product_unitprice = 0;
				}


				$array['product_buying_price'] = $product_buying_price;
				$array['selling_price'] = $product_unitprice;

				$this->db->where('product_deductions_id',$product_deductions_id);
				$this->db->update('product_deductions',$array);
			}
		}

	}
}

?>