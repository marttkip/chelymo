<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Orders extends MX_Controller
{

	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('inventory_management/products_model');
		$this->load->model('orders_model');
		$this->load->model('suppliers_model');
		$this->load->model('categories_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/creditors_model');
		$this->load->model('inventory/stores_model');
		$this->load->model('reception/database');
		$this->load->model('reception/reception_model');
		$this->load->model('inventory_management/inventory_management_model');
	}

	/*
	*
	*	Default action is to show all the orders
	*
	*/
	public function index()
	{
		// get my approval roles

		$where = 'orders.order_status_id = order_status.order_status_id';
		$table = 'orders, order_status';

		// $search = $this->session->userdata('search_orders');
		// $where .= $search;

		// $search = $this->session->userdata('orders_search');

		// //  var_dump('jhvjhv');die();
		// if(!empty($search))
		// {
		// $where .= $search;

		// }
		// var_dump('sjdbs');die();


		$search = $this->session->userdata('orders_search');
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND patient_delete = '.$delete;
		// $where = 'patient_type = 0 AND category_id = 2 AND patient_delete = '.$delete;


		if(!empty($search))
		{
			$where .= $search;
		}
		
		else
		{
			// $where .= ' AND patients.branch_code = \''.$this->session->userdata('branch_code').'\'';
		}

		// var_dump('sjdbs');die();
        

		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'inventory/orders';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_orders($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('orders/all_orders', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new order
	*
	*/
	public function add_order()
	{
		//form validation rules
		$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			$prefix = 'INV';
			$order_id = $this->orders_model->add_order($prefix);
			//update order
			if($order_id > 0)
			{
				redirect('inventory/orders');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}

		}

		// $store_priviledges = $this->inventory_management_model->get_assigned_stores();
		// $v_data['store_priviledges'] =  $store_priviledges;
		//open the add new order
		$data['title'] = 'Add Order';
		$v_data['title'] = 'Add Order';
		$v_data['order_status_query'] = $this->orders_model->get_order_status();

		$data['content'] = $this->load->view('orders/add_order', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}


    public function add_order_item($order_id,$order_number)
    {

		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('in_stock', 'In Stock', 'required|numeric|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_order_item($order_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		$order_details = $this->orders_model->get_order_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		// var_dump($order_id);die();
		$data['content'] = $this->load->view('orders/order_item', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
    }
    public function add_supplier_items()
    {
    	$this->form_validation->set_rules('creditor_id', 'creditor', 'required|xss_clean');
		$this->form_validation->set_rules('order_product_id', 'Product', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('quantity_to_deliver', 'QTY', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('unit_price_supplier', 'Unit Price', 'required|numeric|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_supplier_items())
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

    }

    public function print_lpo_new($order_id,$creditor_id)
	{
		$data = array('supplier_order_id'=>$order_id,'creditor_id'=>$creditor_id);

		$data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('orders/views/lpo_print', $data);

	}
	public function print_rfq_new($order_id,$supplier_id,$order_number)
	{
		$data = array('order_id'=>$order_id,'supplier_id'=>$supplier_id,'order_number'=>$order_number);

		$data['contacts'] = $this->site_model->get_contacts();

		$this->load->view('orders/views/request_for_quotation', $data);

	}

    public function update_order_item($order_id,$order_number,$order_item_id)
    {
    	$this->form_validation->set_rules('quantity', 'Quantity', 'numeric|required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
	    	if($this->orders_model->update_order_item($order_id,$order_item_id))
			{
				$this->session->set_userdata('success_message', 'Order Item updated successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Order Item was not updated');
			}
		}
		else
		{
			$this->session->set_userdata('success_message', 'Sorry, Please enter a number in the field');
		}
		redirect('inventory/add-order-item/'.$order_id.'/'.$order_number.'');

    }
    public function update_supplier_prices($order_id,$order_number,$order_item_id)
    {
    	$this->form_validation->set_rules('unit_price', 'Unit Price', 'numeric|required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
	    	if($this->orders_model->update_order_item_price($order_id,$order_item_id))
			{
				$this->session->set_userdata('success_message', 'Order Item updated successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Order Item was not updated');
			}
		}
		else
		{
			$this->session->set_userdata('success_message', 'Sorry, Please enter a number in the field');
		}
		redirect('inventory/add-order-item/'.$order_id.'/'.$order_number.'');

    }
    public function submit_supplier($order_id,$order_number)
    {
    	$this->form_validation->set_rules('supplier_id', 'Quantity', 'numeric|required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_supplier_to_order($order_id))
			{
				$this->session->set_userdata('success_message', 'Order Item updated successfully');
			}
			else
			{
				$this->session->set_userdata('success_message', 'Order Item updated successfully');
			}
		}
		else
		{
			$this->session->set_userdata('success_message', 'Order Item updated successfully');
		}
		redirect('inventory/add-general-order-item/'.$order_id.'/'.$order_number.'');
    }


    public function remove_supplier($order_id,$order_number,$supplier_order_id)
    {

    	
    	
		if($this->orders_model->remove_supplier($supplier_order_id,$order_id))
		{
			$this->session->set_userdata('success_message', 'Supplier to order has been successfully updated');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
		}
	
		redirect('inventory/add-general-order-item/'.$order_id.'/'.$order_number.'');
    }


     public function approve_supplier($order_id,$order_number,$supplier_order_id,$supplier_id)
    {

    	
		if($this->orders_model->approve_supplier($supplier_order_id,$order_id,$supplier_id))
		{
			$this->session->set_userdata('success_message', 'Supplier to order has been successfully updated');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
		}
	
		redirect('inventory/add-general-order-item/'.$order_id.'/'.$order_number.'');
    }
	/*
	*
	*	Edit an existing order
	*	@param int $order_id
	*
	*/
	public function edit_order($order_id)
	{
		//form validation rules
		$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('user_id', 'Customer', 'required|xss_clean');
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update order
			if($this->orders_model->update_order($order_id))
			{
				$this->session->set_userdata('success_message', 'Order updated successfully');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}
		}

		//open the add new order
		$data['title'] = 'Edit Order';

		//select the order from the database
		$query = $this->orders_model->get_order($order_id);

		if ($query->num_rows() > 0)
		{
			$v_data['order'] = $query->row();
			$query = $this->products_model->all_products();
			$v_data['products'] = $query->result();#
			$v_data['payment_methods'] = $this->orders_model->get_payment_methods();

			$data['content'] = $this->load->view('orders/edit_order', $v_data, true);
		}

		else
		{
			$data['content'] = 'Order does not exist';
		}

		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add products to an order
	*	@param int $order_id
	*	@param int $product_id
	*	@param int $quantity
	*
	*/
	public function add_product($order_id, $product_id, $quantity, $price)
	{
		if($this->orders_model->add_product($order_id, $product_id, $quantity, $price))
		{
			redirect('edit-order/'.$order_id);
		}
	}

	/*
	*
	*	Add products to an order
	*	@param int $order_id
	*	@param int $order_item_id
	*	@param int $quantity
	*
	*/
	public function update_cart($order_id, $order_item_id, $quantity)
	{
		if($this->orders_model->update_cart($order_item_id, $quantity))
		{
			redirect('edit-order/'.$order_id);
		}
	}

	/*
	*
	*	Delete an existing order
	*	@param int $order_id
	*
	*/
	public function delete_order($order_id)
	{
		//delete order
		$this->db->delete('orders', array('order_id' => $order_id));
		$this->db->delete('order_item', array('order_item_id' => $order_id));
		redirect('accounts-payables/supplier-orders');
	}

	public function remove_supplier_order($order_id,$order_number,$order_supplier_id)
	{
		$this->db->delete('order_supplier', array('order_supplier_id' => $order_supplier_id));
		redirect('inventory/add-order-item/'.$order_id.'/'.$order_number);
	}

	/*
	*
	*	Add products to an order
	*	@param int $order_item_id
	*
	*/
	public function delete_order_item($order_id, $order_item_id)
	{
		if($this->orders_model->delete_order_item($order_item_id))
		{
			redirect('edit-order/'.$order_id);
		}
	}

	public function delete_supplier_order_item($order_item_id, $order_supplier_id,$order_id)
	{
		if($this->db->delete('order_item', array('order_item_id' => $order_item_id)))
		{
			if($this->db->delete('order_supplier', array('order_supplier_id' => $order_supplier_id)))
			{

			}
			else{

			}
		}
		else{

		}

		redirect('accounts-payables/supplier-invoice-detail/'.$order_id);
	}

	/*
	*
	*	Complete an order
	*	@param int $order_id
	*
	*/
	public function finish_order($order_id)
	{

		$this->db->where('order_id',$order_id);
		$query = $this->db->get('supplier_order');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$supplier_id = $value->supplier_id;
			}
		}
		$data = array(
					'order_status_id'=>1,
					'order_approval_status'=>7,
					'is_store'=>0,
					'account_id'=>$this->config->item('creditor_invoice'),
					'supplier_id'=>$supplier_id
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		
		$this->db->where('order_item.order_id = orders.order_id AND order_item.order_id ='.$order_id);
		$query = $this->db->get('order_item,orders');

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$product_id  = $value->product_id;
				$store_id  = $value->store_id;


				$this->db->where('order_item.product_id = '.$product_id.' AND order_item.order_item_id = order_supplier.order_item_id AND orders.order_id = order_item.order_id AND orders.order_approval_status = 7 AND orders.store_id = '.$store_id);
				$this->db->limit(1);
				$this->db->select('order_supplier.expiry_date,order_supplier.quantity_received');
				$this->db->order_by('orders.supplier_invoice_date','DESC');
				$query_new = $this->db->get('orders,order_item,order_supplier');


			
				if($query_new->num_rows() > 0)
				{
					foreach($query_new->result() as $key => $value2)
					{
						$expiry_date = $value2->expiry_date;
						$quantity_received = $value2->quantity_received;
					}
				
				}
		
				$stock_level = $this->inventory_management_model->get_all_stock_per_store($product_id,$store_id);

	            $purchases = $stock_level['dr_quantity'];
	            $deductions = $stock_level['cr_quantity'];
	           
	            $in_stock = $purchases - $deductions;

	            if($in_stock > 0)
	            {
	            	$inventory_status = 1;
	            }
	            else if($in_stock < 0)
	            {
	            	$inventory_status = 0;
	            }
	            else
	            {
	            	$inventory_status = 2;
	            }


	            if(empty($in_stock) OR $in_stock == 0)
	            {
	            	$store_quantity = 0;
	            }
	            else
	            {
	            	$store_quantity = $in_stock;
	            }

	          
	           	$array_update['store_balance'] = $store_quantity;
	            
	           		// var_dump($array_update);die();
				// update products table
				$this->db->where('product_id = '.$product_id.' AND owning_store_id ='.$store_id);
				$this->db->update('store_product',$array_update);

                // update the store


               
			}
		}


		redirect('accounts-payables/supplier-orders');
	}

	public function finish_supplier_order($order_id)
	{
		$data = array(
					'order_status_id'=>2,
					'order_approval_status'=>7,
					'account_id'=>$this->config->item('credit_note_supplies'),
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		$this->db->where('order_item.order_id = orders.order_id AND order_item.order_id ='.$order_id);
		$query = $this->db->get('order_item,orders');

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$product_id  = $value->product_id;
				$store_id  = $value->store_id;


				$this->db->where('order_item.product_id = '.$product_id.' AND order_item.order_item_id = order_supplier.order_item_id AND orders.order_id = order_item.order_id AND orders.order_approval_status = 7 AND orders.store_id = '.$store_id);
				$this->db->limit(1);
				$this->db->select('order_supplier.expiry_date,order_supplier.quantity_received');
				$this->db->order_by('orders.supplier_invoice_date','DESC');
				$query_new = $this->db->get('orders,order_item,order_supplier');


				// var_dump($query_new);die();
				if($query_new->num_rows() > 0)
				{
					foreach($query_new->result() as $key => $value2)
					{
						$expiry_date = $value2->expiry_date;
						$quantity_received = $value2->quantity_received;
					}
				
				}
		
				$stock_level = $this->inventory_management_model->get_all_stock_per_store($product_id,$store_id);

	            $purchases = $stock_level['dr_quantity'];
	            $deductions = $stock_level['cr_quantity'];
	           
	            $in_stock = $purchases - $deductions;

	            if($in_stock > 0)
	            {
	            	$inventory_status = 1;
	            }
	            else if($in_stock < 0)
	            {
	            	$inventory_status = 0;
	            }
	            else
	            {
	            	$inventory_status = 2;
	            }


	            if(empty($in_stock) OR $in_stock == 0)
	            {
	            	$store_quantity = 0;
	            }
	            else
	            {
	            	$store_quantity = $in_stock;
	            }

	          
	           	$array_update['store_balance'] = $store_quantity;
	            

				// update products table
				$this->db->where('product_id = '.$product_id.' AND owning_store_id ='.$store_id);
				$this->db->update('store_product',$array_update);

                // update the store


               
			}
		}

		redirect('accounts-payables/suppliers-invoices');
	}
	public function open_supplier_order($order_id,$level=NULL)
	{
		$data = array(
					'order_status_id'=>2,
					'order_approval_status'=>6
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		$this->db->where('order_item.order_id = orders.order_id AND order_item.order_id ='.$order_id);
		$query = $this->db->get('order_item,orders');

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$product_id  = $value->product_id;
				$store_id  = $value->store_id;

				$stock_level = $this->inventory_management_model->get_all_stock_per_store($product_id,$store_id);

	            $purchases = $stock_level['dr_quantity'];
	            $deductions = $stock_level['cr_quantity'];
	           
	            $in_stock = $purchases - $deductions;

	            if($in_stock > 0)
	            {
	            	$inventory_status = 1;
	            }
	            else if($in_stock < 0)
	            {
	            	$inventory_status = 0;
	            }
	            else
	            {
	            	$inventory_status = 2;
	            }


	            if(empty($in_stock) OR $in_stock == 0)
	            {
	            	$store_quantity = 0;
	            }
	            else
	            {
	            	$store_quantity = $in_stock;
	            }

	          
	           	$array_update['store_balance'] = $store_quantity;
	            

				// update products table
				$this->db->where('product_id = '.$product_id.' AND owning_store_id ='.$store_id);
				$this->db->update('store_product',$array_update);

			}
		}


		if(empty($level))
		{
			redirect('accounts-payables/supplier-invoice-detail/'.$order_id);
			// redirect('accounts-payables/suppliers-invoices');
		}
		else if($level == 1)
		{
			redirect('procurement/credit-note-detail/'.$order_id);
		}
		else if($level == 2)
		{
			redirect('procurement/credit-note-detail/'.$order_id);
		}
		
	}

	public function open_transfer_order($order_id)
	{
		$data = array(
					'order_status_id'=>2,
					'order_approval_status'=>0
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		redirect('procurement/order-invoice-detail/'.$order_id);
	}
	public function finish_credit_note($order_id)
	{
		$data = array(
					'order_status_id'=>2,
					'order_approval_status'=>7
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		$this->db->where('order_item.order_id = orders.order_id AND order_item.order_id ='.$order_id);
		$query = $this->db->get('order_item,orders');

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$product_id  = $value->product_id;
				$store_id  = $value->store_id;


				$stock_level = $this->inventory_management_model->get_all_stock_per_store($product_id,$store_id);

	            $purchases = $stock_level['dr_quantity'];
	            $deductions = $stock_level['cr_quantity'];
	           
	            $in_stock = $purchases - $deductions;

	            if($in_stock > 0)
	            {
	            	$inventory_status = 1;
	            }
	            else if($in_stock < 0)
	            {
	            	$inventory_status = 0;
	            }
	            else
	            {
	            	$inventory_status = 2;
	            }


	            if(empty($in_stock) OR $in_stock == 0)
	            {
	            	$store_quantity = 0;
	            }
	            else
	            {
	            	$store_quantity = $in_stock;
	            }

	          
	           	$array_update['store_balance'] = $store_quantity;
	            

				// update products table
				$this->db->where('product_id = '.$product_id.' AND owning_store_id ='.$store_id);
				$this->db->update('store_product',$array_update);
			}
		}





		redirect('procurement/suppliers-credit-note');
	}
	public function finish_returned_note($order_id)
	{
		$data = array(
					'order_status_id'=>2,
					'order_approval_status'=>7
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		redirect('procurement/goods-returned-note');
	}
	public function send_order_for_correction($order_id)
	{

    	$data = array(
					'order_approval_status'=>0,
					'order_status_id'=>1
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		redirect('accounts-payables/supplier-orders');
	}

    public function send_order_for_approval($order_id,$order_status= NULL)
    {
    	if($order_status == NULL)
    	{
    		$order_status = 1;
    	}
    	else
    	{
    		$order_status = $order_status;
    	}


    	if($order_status == 4)
    	{
    		// update all items to the order supplier table 
    		$this->orders_model->update_orders_items_to_supplier_items($order_id);

    	}
		$this->orders_model->update_order_status($order_id,$order_status);


		redirect('procurement/general-orders');
    }
	/*
	*
	*	Cancel an order
	*	@param int $order_id
	*
	*/
	public function cancel_order($order_id)
	{
		$data = array(
					'order_status'=>3
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		redirect('all-orders');
	}

	/*
	*
	*	Deactivate an order
	*	@param int $order_id
	*
	*/
	public function deactivate_order($order_id)
	{
		$data = array(
					'order_status'=>1
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		redirect('all-orders');
	}
	public function update_invoice_charges()
	{
		// var_dump($_POST); die();

		$this->form_validation->set_rules('invoice_number', 'Invoice Number', 'xss_clean');
		$this->form_validation->set_rules('mark_up', 'Mark up', 'xss_clean');
		$this->form_validation->set_rules('discount', 'Dicount', 'xss_clean');
		$this->form_validation->set_rules('vat', 'vat', 'xss_clean');
		$this->form_validation->set_rules('quantity_received', 'Quantity Received', 'required|xss_clean');
		$this->form_validation->set_rules('order_supplier_id', 'Order Supplier Id', 'required|xss_clean');
		$this->form_validation->set_rules('product_name', 'Product Name', 'required|xss_clean');
		$this->form_validation->set_rules('total_amount', 'Total Amount', 'required|xss_clean');
		// $this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required|xss_clean');
		//$this->form_validation->set_rules('pack_size', 'Pack Size', 'required|xss_clean');

		$form_id = $this->input->post('form_id');

		if(!empty($form_id))
		{
			// $this->form_validation->set_rules('buying_unit_price', 'Buying Price', 'required|xss_clean');
		}

		//if form has been submitted
		if ($this->form_validation->run())
		{

			// var_dump($_POST); die();
	    	if($this->orders_model->update_invoice_charges())
			{
				$this->session->set_userdata('success_message', 'You have successfully updated the charges');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry, please try again later');
			}
		}
		else
		{
			// $this->session->set_userdata('success_message', 'Sorry, Please enter a number in the field');
			$this->session->set_userdata('error_message', validation_errors());
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);


	}

	public function suppliers_invoices()
	{

		
		$personnel_id = $this->session->userdata('personnel_id');
		
		$where = 'orders.order_approval_status >= 6  AND orders.supplier_id = creditor.creditor_id  AND orders.is_store <> 3';
		$table = 'orders, creditor';
		//pagination


        $search = $this->session->userdata('supplier_order_search');
        if(isset($search))
        {
        	$where .= $search;
        }

		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts-payables/suppliers-invoices';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_orders_suppliers($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$v_data['lpo_items'] = $this->suppliers_model->get_all_orders_requisitions();
		$v_data['title'] = "Suppliers Invoices";

			// var_dump($table);die();

		$data['content'] = $this->load->view('orders/all_suppliers_orders', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);

	}


	public function add_supplier_invoice()
	{
		$this->form_validation->set_rules('store_id', 'Store', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_id', 'Supplier', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_invoice_date', 'Supplier', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_invoice_number', 'Supplier', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			$supplier_id = $this->input->post('supplier_id');
			$supplier_invoice_number = $this->input->post('supplier_invoice_number');

			$this->db->where('supplier_id = '.$supplier_id.' AND supplier_invoice_number = "'.$supplier_invoice_number.'"');
			$query_checked = $this->db->get('orders');

			if($query_checked->num_rows() == 0)
			{


				$store_id = $this->input->post('store_id');
				$supplier_id = $this->input->post('supplier_id');
				// var_dump($store_id); die();
				if($store_id > 0 AND $supplier_id > 0)
				{
					$prefix = 'NHINV';
					$order_id = $this->orders_model->add_supplier_order($prefix);
					//update order
					if($order_id > 0)
					{
						$this->session->set_userdata('success_message', 'You have successfully added an order');
					}

					else
					{
						$this->session->set_userdata('error_message', 'Could not update order. Please try again');
					}
				}
				else
				{
					$this->session->set_userdata('error_message', 'Could not create order. Ensure that all fields are selected');
				}

			}
			else
			{
				$this->session->set_userdata('error_message', 'Seems like there is an invoice already entered as you have indicated. Please check before proceeding');
			}

		}
		else
		{
			$this->session->set_userdata('error_message', 'To Create an order endure you have added all the item required');
		}

		redirect('procurement/suppliers-invoices');
	}


	public function add_goods_received_note()
	{
	
		$this->form_validation->set_rules('supplier_invoice_date', 'Supplier', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_invoice_number', 'Supplier', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{

			
			$order_id = $this->input->post('order_id');

			$this->db->where('order_id = '.$order_id.'');
			$query_order = $this->db->get('orders');

			if($query_order->num_rows() > 0)
			{
				foreach ($query_order->result() as $key => $value) {
					// code...
					$supplier_id = $value->supplier_id;
				}
			}



			// $supplier_id = $this->input->post('supplier_id');
			$supplier_invoice_number = $this->input->post('supplier_invoice_number');

			$this->db->where('supplier_id = '.$supplier_id.' AND supplier_invoice_number = "'.$supplier_invoice_number.'"');
			$query_checked = $this->db->get('orders');

			if($query_checked->num_rows() == 0)
			{


				$supplier_invoice_date = $this->input->post('supplier_invoice_date');
				$supplier_invoice_number = $this->input->post('supplier_invoice_number');

				$grn_number = $this->inventory_management_model->create_grn_number();

				$branch_code = $this->session->userdata('branch_code');
				$order_number = $branch_code.'.'.$grn_number;
				// var_dump($store_id); die();
				$data_update = array(
								'supplier_invoice_date'=>$supplier_invoice_date,
								'supplier_invoice_number'=>$supplier_invoice_number,
								'order_approval_status'=>6,
								'billing_status'=>2,
								'order_number'=> $order_number,
								'suffix'=>$grn_number,
								'account_id'=>99
							);
				$this->db->where('order_id',$order_id);
				$this->db->update('orders', $data_update);

				$this->session->set_userdata('success_message', 'You have successfully added the order');

			
				$invoice_number = ' AND orders.order_id = '.$order_id.'';
				$search = $invoice_number;
				$this->session->set_userdata('supplier_order_search', $search);
				$this->session->set_userdata('sucess_message', 'Order has been added successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'To Create an order endure you have added all the item required');
			}

		}
		else
		{
			$this->session->set_userdata('error_message', 'To Create an order endure you have added all the item required');
		}

		redirect('procurement/suppliers-invoices');
	}

	public function suppliers_invoice_detail($order_id)
	{

		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		// $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('in_stock', 'In Stock', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('creditor_id', 'Supplier', 'required|numeric|xss_clean');

		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_order_item_supplier($order_id,$store_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		
		// var_dump($store_id);die();
		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products_orders();
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['store_id'] = $store_id;


		$where = 'product.product_id = order_item.product_id AND order_item.order_id = '.$order_id;
		$table = 'order_item, product';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts-payables/supplier-invoice-detail/'.$order_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_supplier_order_items($table, $where, $config["per_page"], $page);

		$v_data['order_item_query'] = $query;
		$v_data['page'] = $page;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$data['content'] = $this->load->view('orders/suppliers_order_items', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

		public function print_stickers($order_id)
	{

		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		// $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('in_stock', 'In Stock', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('creditor_id', 'Supplier', 'required|numeric|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_order_item_supplier($order_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;

		$v_data['store_id'] = $store_id;




		$where = 'product.product_id = order_item.product_id AND order_item.order_id = '.$order_id;
		$table = 'order_item, product';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts-payables/supplier-invoice-detail/'.$order_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_supplier_order_items($table, $where, $config["per_page"], $page);

		$v_data['order_item_query'] = $query;
		$v_data['page'] = $page;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$data['content'] = $this->load->view('orders/product_stickers', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

public function print_product_stickers()
	{

		
		$where = 'product.category_id = category.category_id AND product.product_deleted = 0 AND brand.brand_id = product.brand_id AND product.product_id = store_product.product_id';
		$table = 'product, category, brand,store_product';

		//echo $where;die();

		$product_inventory_search = $this->session->userdata('product_inventory_search');

		if(!empty($product_inventory_search))
		{
			$where .= $product_inventory_search;
		}
		$query = $this->products_model->get_product_items($table, $where);

		// $v_data['store_id'] = $store_id;

		$data['title'] = 'Stickers';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		// $v_data['branches'] = $branches;

		$this->load->view('order/product_stickers', $v_data);

	}



	public function update_orders_date($order_id)
	{
		$this->form_validation->set_rules('supplier_invoice_date', 'Date', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_invoice_number', 'Invoice number', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{

			$data = array(
					'supplier_invoice_date'=>$this->input->post('supplier_invoice_date'),
					'supplier_invoice_number'=>$this->input->post('supplier_invoice_number'),
					'discount_added'=>$this->input->post('discount_added'),
					'vat_added'=>$this->input->post('vat_added'),
					'transport_charge'=>$this->input->post('transport_charge')
				);

			$this->db->where('order_id = '.$order_id);
			if($this->db->update('orders', $data))
			{
				$this->session->set_userdata('success_message', 'Successfully updated invoice information');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong. please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Something went wrong. please try again');
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	public function get_stock_quantity($product_id,$store_id=5)
	{

		$this->db->where('product_id',$product_id);
		$query = $this->db->get('product');
		$buying_unit_price = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$buying_unit_price = $value->buying_unit_price;
			}
		}

		if(empty($buying_unit_price))
		{
			$buying_unit_price = 0;
		}
		$total_quantity = $this->input->post('quantity');
		$stock_level = $this->inventory_management_model->get_all_stock_per_store($product_id,$store_id);

        $purchases = $stock_level['dr_quantity'];
        $deductions = $stock_level['cr_quantity'];
       
        $in_stock = $purchases - $deductions;

        $response['message'] = 'success';
		$response['in_stock'] = $in_stock;
		$response['buying_unit_price'] = $buying_unit_price;
		$response['total_quantity'] = $total_quantity + $in_stock;

      echo json_encode($response);

	}

	public function delete_order_supply($order_id)

        {
        		//delete category image
        		$query = $this->assets_model->get_order_supply($order_id);

        		if ($query->num_rows() > 0)
        		{
        			$result = $query->result();

        		}
        		$this->assets_model->delete_order_supply($order_id);
        		$this->session->set_userdata('success_message', 'Supply has been deleted');
        		redirect('accounts-payables/suppliers-invoices');
        }


    public function product_supplies()
	{
		$where = "order_item.order_item_id = order_supplier.order_item_id AND order_item.product_id = product.product_id AND orders.order_id = order_item.order_id  AND product.product_deleted = 0 AND orders.supplier_id > 0";
		$table = 'order_item,order_supplier,product,orders';


		$supplier_search = $this->session->userdata('product_purchased_search');

		if(!empty($supplier_search))
		{
			$where .= $supplier_search;
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/product-supplies';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;


		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_supplied_items($table, $where, $config["per_page"], $page);
		$v_data['inventory_start_date'] = $this->inventory_management_model->get_inventory_start_date();
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] = 'All Supplies';
		//$v_data['child_suppliers'] = $this->suppliers_model->all_child_suppliers();
		$data['content'] = $this->load->view('orders/supplied_orders', $v_data, true);
		$data['title'] = 'All suppliers';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_inventory_product()
	{
		$stocked = $this->input->post('stocked');
		$product_code = $this->input->post('product_code');
		$product_name = $this->input->post('product_name');
		$brand_id = $this->input->post('brand_id');
		$generic_id = $this->input->post('generic_id');
		$category_id = $this->input->post('category_id');
		$store_id = $this->input->post('store_id');
		$start_date = $this->input->post('date_from');
		$end_date = $this->input->post('date_end');

		if($stocked == 1)
		{
			$stocked = ' AND product.quantity > 0 ';
		}
		else if($stocked == 2)
		{
			$stocked = ' AND (product.quantity = 0 OR product.quantity = \'\') ';
		}
		else
		{
			$stocked = '';
		}

		if(!empty($product_name))
		{
			$product_name = ' AND product.product_name LIKE \'%'.$product_name.'%\' ';
		}
		else
		{
			$product_name = '';
		}
		if(!empty($product_code))
		{
			$product_code = ' AND product.product_code = \''.$product_code.'\'';
		}
		else
		{
			$product_code = '';
		}
		if(!empty($generic_id))
		{
			$generic_id = ' AND product.generic_id = '.$generic_id;
		}
		else
		{
			$generic_id = '';
		}

		if(!empty($brand_id))
		{
			$brand_id = ' AND product.brand_id = '.$brand_id;
		}
		else
		{
			$brand_id = '';
		}

		if(!empty($category_id))
		{
			$category_id = ' AND product.category_id = '.$category_id;
		}
		else
		{
			$category_id = '';
		}
		if(!empty($store_id))
		{
			$store_id = ' AND store.store_id = '.$store_id;
		}
		else
		{
			$store_id = '';
		}

		$search = $product_name.$generic_id.$brand_id.$category_id.$store_id.$product_code.$stocked;
		$this->session->set_userdata('product_inventory_search', $search);
		$this->session->set_userdata('inventory_search_start_date',$start_date);
		$this->session->set_userdata('inventory_search_end_date',$end_date);

		$this->index();
	}

	public function goods_received_notes($order_id)
	{

		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_id'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();


		$where = 'product.product_id = order_item.product_id AND order_item.order_id = '.$order_id;
		$table = 'order_item, product';

		$v_data['query'] = $this->orders_model->get_creditors_detail_summary($where,$table);

		$this->load->view('suppliers/print_goods_received_note', $v_data);

	}
	public function print_credit_note($order_id)
	{

		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
				$deduction_type_id = $value->deduction_type_id;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_id'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['deduction_type_id'] = $deduction_type_id;

		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();





		$where = 'product.product_id = order_item.product_id AND order_item.order_id = '.$order_id;
		$table = 'order_item, product';

		$v_data['query'] = $this->orders_model->get_creditors_detail_summary($where,$table);

		$this->load->view('suppliers/print_credit_note', $v_data);

	}


	public function drug_transfers()
	{

		$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_id', 'Supplier', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			$prefix = 'APDT/'.date('Y');
			$order_id = $this->orders_model->add_transfer_order($prefix,2);
			//update order
			if($order_id > 0)
			{
				$this->session->set_userdata('success_message', 'You have successfully added an order');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}
		}

		$where = 'orders.order_status_id = order_status.order_status_id  AND orders.is_store = 2 AND deduction_type_id <> 1 ';
		$table = 'orders, order_status';


		 $search = $this->session->userdata('transfer_order_search');
        $where .= $search;
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/drug-transfers';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_orders_suppliers($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$v_data['title'] = "Suppliers Invoices";
		$data['content'] = $this->load->view('orders/drug_transfers', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);

	}


	public function order_invoice_detail($order_id)
	{

		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		// $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('in_stock', 'In Stock', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('creditor_id', 'Supplier', 'required|numeric|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
			if($this->orders_model->add_order_item_supplied($order_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$created = $value->created;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}
		

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['created'] = $created;


		$where = 'product.product_id = product_deductions.product_id AND product_deductions.order_id = orders.order_id AND orders.order_id = '.$order_id;
		$table = 'product_deductions, product,orders';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/order-invoice-detail/'.$order_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_order_order_items($table, $where, $config["per_page"], $page);

		$v_data['order_item_query'] = $query;
		$v_data['page'] = $page;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$data['content'] = $this->load->view('orders/order_item_views', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function award_order_products($product_deductions_id,$product_id,$order_id)
    {
    	// $parent_store_qty = $this->inventory_management_model->get_parent_store_inventory_quantity($parent_id,$product_id);

    	$this->form_validation->set_rules('quantity_given', 'Quantity', 'required|numeric|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{

				$quantity = $this->input->post('quantity_given');
				$pack_size = $this->input->post('pack_size');
				$quantity = $quantity;
		    	$inventory_start_date = $this->inventory_management_model->get_inventory_start_date();
		    	// $parent_store_qty = $this->inventory_management_model->parent_stock_store($inventory_start_date,$product_id,6);

		    	$stock_level = $this->inventory_management_model->get_stock_purchases_new($product_id,5);

                $purchases = $stock_level['dr_quantity'];
                $deductions = $stock_level['cr_quantity'];
               
                $parent_store_qty = $purchases - $deductions;

		    	if($parent_store_qty >= $quantity)
		    	{
		    		$data = array('quantity_given' => $quantity,'pack_size'=>$pack_size,'given_by'=>$this->session->userdata('personnel_id'),'product_deductions_status'=>1);
			    	$this->db->where('product_deductions_id ='.$product_deductions_id);
					$this->db->update('product_deductions',$data);
					$data['result']= "You've successfully awarded the store ".$quantity."";
					$this->session->set_userdata('success_message', "You've successfully awarded the store ".$quantity."");

		    	}
		    	else
		    	{
		    		$data['result']= "Sorry you cannot award the quantity you have placed ".$parent_store_qty;
		    		$this->session->set_userdata('error_message', "Sorry you cannot award the quantity you have placed".$parent_store_qty."");
		    	}

		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not award the product');
		}

		redirect('procurement/order-invoice-detail/'.$order_id);
    }

    public function delete_transfer_order_item($product_deductions_id,$order_id)
	{
		if($this->db->delete('product_deductions', array('product_deductions_id' => $product_deductions_id)))
		{
			$this->session->set_userdata('success_message', "You've successfully removed the item");
		}
		else
		{
			$this->session->set_userdata('success_message', "Sorry please try again");
		}

		redirect('procurement/order-invoice-detail/'.$order_id);
	}

	public function finish_transfer_order($order_id)
	{
		$data = array(
					'order_status_id'=>2,
					'order_approval_status'=>7,
					'account_id'=>99
				);

		$this->db->where('order_id = '.$order_id);
		$this->db->update('orders', $data);

		redirect('procurement/drug-transfers');
	}
	public function print_supplier_invoice($order_id)
	{

		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_id'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();


		$where = 'product.product_id = product_deductions.product_id AND product_deductions.order_id = '.$order_id;
		$table = 'product_deductions, product';

		$v_data['query'] = $this->orders_model->get_creditors_detail_summary($where,$table);

		$this->load->view('suppliers/print_goods_received_note', $v_data);

	}

	public function goods_transfered($order_id)
	{

		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_id'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();


		$where = 'product.product_id = product_deductions.product_id AND product_deductions.order_id = '.$order_id;
		$table = 'product_deductions, product';

		$v_data['query'] = $this->orders_model->get_creditors_detail_summary($where,$table);

		$this->load->view('suppliers/print_transfered_note', $v_data);

	}


	public function print_supplier_credit_note($order_id)
	{

		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_id'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();


		$where = 'product.product_id = product_deductions.product_id AND product_deductions.order_id = '.$order_id;
		$table = 'product_deductions, product';

		$v_data['query'] = $this->orders_model->get_creditors_detail_summary($where,$table);

		$this->load->view('suppliers/print_transfered_note', $v_data);

	}
	public function search_products_purchased()
	{
		$supplier_id = $this->input->post('supplier_id');
		$product_name = $this->input->post('product_name');
		$invoice_number = $this->input->post('invoice_number');

		// var_dump($_POST); die();
		if(!empty($product_name))
		{
			$product_name = ' AND product.product_name LIKE \'%'.$product_name.'%\' ';
		}
		else
		{
			$product_name = '';
		}
		if(!empty($invoice_number))
		{
			$invoice_number = ' AND order_supplier.invoice_number = \''.$invoice_number.'\'';
		}
		else
		{
			$invoice_number = '';
		}
		if(!empty($supplier_id))
		{
			$supplier_id = ' AND orders.supplier_id = '.$supplier_id;
		}
		else
		{
			$supplier_id = '';
		}



		$search = $product_name.$supplier_id.$invoice_number;
		$this->session->set_userdata('product_purchased_search', $search);

		redirect('procurement/product-supplies');
	}
	public function close_product_purchased_search()
	{
		$this->session->unset_userdata('product_purchased_search');
		redirect('procurement/product-supplies');
	}



	public function credit_notes()
	{

		//$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_id', 'Supplier', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			$prefix = 'APGR';
			$order_id = $this->orders_model->add_credit_note_order($prefix);
			//update order
			if($order_id > 0)
			{
				$this->session->set_userdata('success_message', 'You have successfully added an supplier credit note');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}
		}

		$where = 'orders.order_status_id = order_status.order_status_id  AND orders.is_store = 3  AND deduction_type_id <> 1';
		$table = 'orders, order_status';

		$search = $this->session->userdata('supplier_credit_note_search');
		 // var_dump($search);die();
		 if(!empty($search))
		 {
		 	$where .= $search;
		 }
        
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/suppliers-credit-note';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_orders_suppliers_credits($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$v_data['title'] = "Credit Notes";
		$v_data['deduction_type_id'] = 0;
		$data['content'] = $this->load->view('orders/credit_notes', $v_data, true);

		$data['title'] = 'All Good Return Notes';

		$this->load->view('admin/templates/general_page', $data);

	}

	public function credit_note_detail($order_id)
	{
		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		// $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
		// $this->form_validation->set_rules('in_stock', 'In Stock', 'xss_clean');
		$this->form_validation->set_rules('creditor_id', 'Supplier', 'required|numeric|xss_clean');
		// var_dump($_POST); die();
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_order_item_supplier($order_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		$order_details = $this->orders_model->get_order_supplier_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
				$creditor_name = $value->creditor_name;
				$creditor_email = $value->creditor_email;
				$creditor_phone = $value->creditor_phone;
				$created = $value->created;
				$creditor_id = $value->creditor_id;
				$creditor_location = $value->creditor_location;
				$supplier_invoice_number = $value->supplier_invoice_number;
				$supplier_invoice_date = $value->supplier_invoice_date;
				$reference_number = $value->reference_number;
				$deduction_type_id = $value->deduction_type_id;
				$is_store = $value->is_store;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['creditor_name'] = $creditor_name;
		$v_data['store_name'] = $store_name;
		$v_data['creditor_email'] = $creditor_email;
		$v_data['creditor_phone'] = $creditor_phone;
		$v_data['creditor_id_value'] = $creditor_id;
		$v_data['creditor_location'] = $creditor_location;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_number'] = $supplier_invoice_number;
		$v_data['supplier_invoice_date'] = $supplier_invoice_date;
		$v_data['reference_number'] = $reference_number;
		$v_data['deduction_type_id'] = $deduction_type_id;
		$v_data['created'] = $created;
		$v_data['is_store'] = $is_store;


		// $where = 'product.product_id = product_deductions.product_id AND product_deductions.order_id = orders.order_id AND orders.order_id = '.$order_id;
		// $table = 'product_deductions, product,orders';

		$where = 'product.product_id = order_item.product_id AND order_item.order_id = '.$order_id;
		$table = 'order_item, product';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/credit-note-detail/'.$order_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_supplier_order_items($table, $where, $config["per_page"], $page);
		// var_dump($query); die();
		$v_data['order_item_query'] = $query;
		$v_data['page'] = $page;

		$v_data['contacts'] = $this->site_model->get_contacts();

		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$data['content'] = $this->load->view('orders/credit_note_detail', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}



	public function update_notes_date($order_id)
	{
		$this->form_validation->set_rules('supplier_invoice_date', 'Date', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_invoice_number', 'Invoice number', 'required|xss_clean');
		$this->form_validation->set_rules('reference_number', 'Reference Number', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{

			$data = array(
					'supplier_invoice_date'=>$this->input->post('supplier_invoice_date'),
					'supplier_invoice_number'=>$this->input->post('supplier_invoice_number'),
					'reference_number'=>$this->input->post('reference_number'),
				);

			$this->db->where('order_id = '.$order_id);
			if($this->db->update('orders', $data))
			{
				$this->session->set_userdata('success_message', 'Successfully updated invoice information');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong. please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Something went wrong. please try again');
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	public function update_supplier_invoices()
	{
		$this->db->where('order_supplier_id > 0 AND less_vat IS NULL');
		$query = $this->db->get('order_supplier');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$order_supplier_id = $value->order_supplier_id;
				$unit_price = $value->unit_price;
				$pack_size = $value->pack_size;
				$quantity_received = $value->quantity_received;
				$discount = $value->discount;
				$vat = $value->vat;


				if(empty($pack_size))
				{
					$purchase_unit = 0;
				}
				else
				{
					$purchase_unit = ($unit_price/$pack_size);
				}

				$total_items_price = ($quantity_received) * $unit_price;
				if($discount > 0)
				 {

				 	$current_price = $total_items_price - (($discount/100)*$total_items_price);
				 }
				 else
				 {
				 	$current_price = $total_items_price;
				 }


				 if($vat > 0)
				 {

					$vat_capture = (0.16 *$current_price);
					// $current_price = $current_price+$vat_capture;
					// $total_vat += $vat_capture;
				 }

				 if($order_supplier_id == 9243)
				 {
				 	 // var_dump($current_price); die();
				 }


				$gross_amount = $purchase_unit * ($quantity_received*$pack_size);

				$less_vat = $current_price;//$gross_amount * ((100 - $discount)/100);
				$gross_amount = $less_vat * ((100+$vat)/100);
				// var_dump($gross_amount); die();

				$update_array['total_amount'] = $gross_amount;
				$update_array['less_vat'] = $less_vat;
				$this->db->where('order_supplier_id',$order_supplier_id);
				$this->db->update('order_supplier',$update_array);
			}
		}
	}

	public function search_supplier_orders()
	{

		$invoice_number = $this->input->post('invoice_number');
		$supplier_id = $this->input->post('supplier_id');
		$store_id = $this->input->post('store_id');
		$supplier_invoice_date = $this->input->post('supplier_invoice_date');

		if(!empty($invoice_number))
		{
			$invoice_number = ' AND orders.supplier_invoice_number LIKE \'%'.$invoice_number.'%\' ';
		}
		// var_dump($supplier_id); die();
	    if(!empty($supplier_id))
        {
            $supplier_id =' AND orders.supplier_id = '.$supplier_id;
        }
        else
        {
        	$supplier_id = '';
        }


        if(!empty($supplier_invoice_date))
		{
			$date = ' AND orders.supplier_invoice_date = \''.$supplier_invoice_date.'\'';
			$search_title .= 'Supplier invoice for '.date('jS M Y', strtotime($supplier_invoice_date)).' ';
		}

		else
		{
			$date = '';
		}


		if(!empty($store_id))
        {
            $store_id =' AND orders.store_id = '.$store_id;
        }
        else
        {
        	$store_id = '';
        }


		$search = $invoice_number.$supplier_id.$date.$store_id;

        // var_dump($search); die();
		$this->session->set_userdata('supplier_order_search', $search);

		redirect('accounts-payables/suppliers-invoices');

	}
	public function close_supplier_order_search()
	{
		$this->session->unset_userdata('supplier_order_search');
		redirect('accounts-payables/suppliers-invoices');
	}

	public function search_orders()
	{

		$order_number = $this->input->post('order_number');
		$order_status_id = $this->input->post('order_status_id');
		$order_created_date = $this->input->post('order_created_date');
		//$search_title = '';
		// var_dump('sjdbs');die();


		
		if(!empty($order_number))
		{
			$order_number = ' AND orders.order_number LIKE \'%'.$order_number.'%\'';
		}
		
		// var_dump($order_number); die();
	    if(!empty($order_status_id))
        {
            $order_status_id = ' AND orders.order_status_id = '.$order_status_id;
        }
		else
        {
        	$order_status_id = '';
        }
        


        if(!empty($order_created_date))
		{
			$date = ' AND orders.created LIKE \'%'.$order_created_date.'%\'';
			$search_title .= 'Order for '.date('jS M Y', strtotime($order_created_date)).' ';
		}
		


		$search = $order_number.$order_status_id.$date;

        // var_dump($search); die();
		$this->session->set_userdata('orders_search', $search);

		// var_dump($search); die();

		// var_dump('sjdbs');die();
		// redirect('procurement/general-orders');
		// // var_dump('sjdbs');die();
		redirect('inventory/orders/index');

	}

	public function close_search_orders()
	{
		$this->session->unset_userdata('orders_search');
		redirect('procurement/general-orders');
	}

		public function close_search_allan()
	{
		$this->session->unset_userdata('main_stock_search');
		redirect('inventory/stock-issues');
	}


	 public function search_transfer_orders()
	{

		$invoice_number = $this->input->post('invoice_number');
		$supplier_id = $this->input->post('supplier_id');


		if(!empty($invoice_number))
		{
			$invoice_number = ' AND orders.order_number LIKE \'%'.addslashes($invoice_number).'%\' ';
		}
		// var_dump($supplier_id); die();
	    if(!empty($supplier_id))
        {
            $supplier_id =' AND orders.supplier_id = '.$supplier_id;
        }
        else
        {
        	$supplier_id = '';
        }


		$search = $invoice_number.$supplier_id;

        // var_dump($search); die();
		$this->session->set_userdata('transfer_order_search', $search);

		redirect('procurement/drug-transfers');

	}
	public function close_transfer_order_search()
	{
		$this->session->unset_userdata('transfer_order_search');
		redirect('procurement/drug-transfers');
	}


	// general orders function

	public function general_orders()
	{
		// get my approval roles

		$where = 'orders.order_status_id = order_status.order_status_id AND orders.order_status_id = 1';
		$table = 'orders, order_status';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts-payables/supplier-orders';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 3;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_orders($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('general_orders/all_general_orders', $v_data, true);

		$data['title'] = 'All orders';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function add_general_order_item($order_id,$order_number)
    {

		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
	    // $this->form_validation->set_rules('in_stock', 'In Stock', 'numeric|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->orders_model->add_order_item($order_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		$order_details = $this->orders_model->get_order_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$store_parent = $this->inventory_management_model->get_parent_store($store_id);

		if($store_parent == 0)
		{
			$store_parent = $store_id;
		}
		$v_data['products_query'] = $this->products_model->all_products($store_parent);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['store_id'] = $store_id;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$data['content'] = $this->load->view('general_orders/general_order_item', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
    }

    /*
	*
	*	Add a new order
	*
	*/
	public function add_general_order()
	{
		//form validation rules
		$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			$prefix = 'APGO';
			$order_id = $this->orders_model->add_order($prefix);
			//update order
			if($order_id > 0)
			{
				redirect('accounts-payables/supplier-orders');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}
		}

		// $store_priviledges = $this->inventory_management_model->get_assigned_stores();
		// $v_data['store_priviledges'] =  $store_priviledges;
		//open the add new order
		$data['title'] = 'Add Order';
		$v_data['title'] = 'Add Order';
		$v_data['order_status_query'] = $this->orders_model->get_order_status();

		$data['content'] = $this->load->view('general_orders/add_general_order', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function delete_general_order_item($order_item_id,$order_id,$order_number)
	{
		if($this->db->delete('order_item', array('order_item_id' => $order_item_id)))
		{

		}
		else{

		}

		redirect('inventory/add-general-order-item/'.$order_id.'/'.$order_number);
	}

	public function update_general_order_item($order_id,$order_number,$order_item_id)
    {
    	$this->form_validation->set_rules('quantity', 'Quantity', 'numeric|required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
	    	if($this->orders_model->update_order_item($order_id,$order_item_id))
			{
				$this->session->set_userdata('success_message', 'Order Item updated successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Order Item was not updated');
			}
		}
		else
		{
			$this->session->set_userdata('success_message', 'Sorry, Please enter a number in the field');
		}
		redirect('inventory/add-general-order-item/'.$order_id.'/'.$order_number.'');

    }

    public function send_general_order_for_approval($order_id,$order_status= NULL)
    {
    	if($order_status == NULL)
    	{
    		$order_status = 1;
    	}
    	else
    	{
    		$order_status = $order_status;
    	}

		$this->orders_model->update_order_status($order_id,$order_status);


		redirect('procurement/general-orders');
    }

    public function print_general_order($order_id)
	{

		// var_dump($order_id); die();
		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->orders_model->get_order_details($order_id);
		$store_name = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$store_id = $value->store_id;
				$store_name = $value->store_name;
				$order_number = $value->order_number;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$order_number;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		$v_data['products_query'] = $this->products_model->all_products($store_id);
		$v_data['order_number'] = $order_number;
		$v_data['order_id'] = $order_id;
		$v_data['store_name'] = $store_name;
		$v_data['order_item_query'] = $this->orders_model->get_order_items($order_id);
		$v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$v_data['order_number'] = $order_number;


		$where = 'product.product_id = order_item.product_id AND order_item.order_id = '.$order_id;
		$table = 'order_item, product';

		$v_data['query'] = $this->orders_model->get_creditors_detail_summary($where,$table);

		$this->load->view('general_orders/print_order_items', $v_data);

	}

	public function goods_returned_note()
	{

		$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('store_id', 'Store', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_id', 'Supplier', 'required|xss_clean');
		$this->form_validation->set_rules('credit_note_number', 'Credit Note', 'required|xss_clean');
		$this->form_validation->set_rules('supplier_invoice_date', 'Date', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			$prefix = 'CMGRN/'.date('Y');
			$order_id = $this->orders_model->add_credit_note_order($prefix,1);
			//update order
			if($order_id > 0)
			{
				$this->session->set_userdata('success_message', 'You have successfully added an supplier credit note');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}
		}

		$where = 'orders.order_status_id = order_status.order_status_id  AND orders.is_store = 3 AND orders.deduction_type_id = 1 ';
		$table = 'orders, order_status';





		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'procurement/suppliers-credit-note';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->orders_model->get_all_orders_suppliers($table, $where, $config["per_page"], $page);

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->orders_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$v_data['title'] = "Goods Returned Note";
		$v_data['deduction_type_id'] = 1;
		$data['content'] = $this->load->view('orders/credit_notes', $v_data, true);

		$data['title'] = 'ALL Credit Notes';

		$this->load->view('admin/templates/general_page', $data);

	}

	public function get_creditors_invoices($creditor_id)
	{

		$options = $this->orders_model->get_creditors_invoices($creditor_id);
		echo '<option value="0"> SELECT AN INVOICE </option>';
		foreach($options->result() AS $key)
		{
			echo '<option value="'.$key->order_id.'.'.$key->supplier_invoice_number.'"> '.$key->supplier_invoice_number.' Date '.$key->supplier_invoice_date.'</option>';
		}
	}



	public function search_supplier_credit_notes()
	{
		$credit_note_number = $this->input->post('credit_note_number');
		$invoice_number = $this->input->post('invoice_number');
		$supplier_id = $this->input->post('supplier_id');


		if(!empty($invoice_number) )
		{
			$invoice_number = ' AND (orders.supplier_invoice_number LIKE \'%'.addslashes($invoice_number).'%\' OR orders.reference_number LIKE \'%'.addslashes($invoice_number).'%\')';
		}


		if(!empty($credit_note_number))
		{
			$credit_note_number = ' AND (orders.supplier_invoice_number LIKE \'%'.addslashes($credit_note_number).'%\' OR  orders.reference_number LIKE \'%'.addslashes($credit_note_number).'%\')';
		}
		// var_dump($supplier_id); die();
	    if(!empty($supplier_id))
        {
            $supplier_id =' AND orders.supplier_id = '.$supplier_id;
        }
        else
        {
        	$supplier_id = '';
        }


		$search = $invoice_number.$supplier_id.$credit_note_number;

        // var_dump($search); die();
		$this->session->set_userdata('supplier_credit_note_search', $search);

		redirect('procurement/suppliers-credit-note');

	}
	public function close_supplier_credit_notes_search()
	{
		$this->session->unset_userdata('supplier_credit_note_search');
		redirect('procurement/suppliers-credit-note');
	}


	public function update_drug_prices()
	{

		$this->db->where('service_charge.service_charge_amount = 0 and service_charge.product_id = product.product_id and product.product_deleted = 0');
		$this->db->group_by('service_charge.product_id');
		$query = $this->db->get('service_charge,product');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$product_id = $value->product_id;
				$product_unitprice = $value->product_unitprice;

				$update_array['service_charge_amount'] = $product_unitprice;
				$this->db->where('product_id',$product_id);
				$this->db->update('service_charge',$update_array);
			}
		}
	

	}

	public function update_prices()
	{
		$this->db->where('order_supplier.order_item_id = order_item.order_item_id');
		$query = $this->db->get('order_supplier,order_item');
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$order_supplier_id = $value->order_supplier_id;
				$buying_unit_price = $value->buying_unit_price;
				$selling_unit_price = $value->selling_unit_price;
				$product_id = $value->product_id;
				$mark_up = $value->mark_up;

				// var_dump($product_id);die();
				if($product_id > 0)
				{

					// use 100 

					$selling_price = ($buying_unit_price * (33/100)) + $buying_unit_price;
					$mark_up = 33;

					$array_product['product_unitprice'] = $selling_price;
					$this->db->where('product_id',$product_id);
					$this->db->update('product',$array_product);

					// $this->db->where('product_id',$product_id);
					// $array_charge['service_charge_amount'] = $selling_price;
					// $this->db->update('service_charge',$array_charge);


					$this->db->where('order_supplier_id',$order_supplier_id);
					$array_charge_old['selling_unit_price'] = $selling_price;
					$array_charge_old['mark_up'] = 33;

					// var_dump($selling_price);die();
					$this->db->update('order_supplier',$array_charge_old);
				}
			}
		}
	}






	public function update_prices_supplier()
	{
		$this->db->where('order_supplier.order_item_id = order_item.order_item_id AND (order_supplier.mark_up > 33 OR order_supplier.mark_up < 33) ');
		$query = $this->db->get('order_supplier,order_item');

		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$order_supplier_id = $value->order_supplier_id;
				$buying_unit_price = $value->buying_unit_price;
				$selling_unit_price = $value->selling_unit_price;
				$product_id = $value->product_id;
				$mark_up = $value->mark_up;

				// var_dump($product_id);die();
				if($product_id > 0)
				{

					// use 100 

					$selling_price = ($buying_unit_price * (33/100)) + $buying_unit_price;
					$mark_up = 33;

					$array_product['preferred_price'] = 0;
					$array_product['product_unitprice'] = $selling_price;
					$this->db->where('product_id',$product_id);
					$this->db->update('product',$array_product);

					// $this->db->where('product_id',$product_id);
					// $array_charge['service_charge_amount'] = $selling_price;
					// $this->db->update('service_charge',$array_charge);


					$this->db->where('order_supplier_id',$order_supplier_id);
					$array_charge_old['selling_unit_price'] = $selling_price;
					$array_charge_old['mark_up'] = 33;

					// var_dump($selling_price);die();
					$this->db->update('order_supplier',$array_charge_old);
				}
			}
		}
	}


	public function update_prices_product()
	{
		$this->db->where('product_deleted = 0 AND product_unitprice > 0 AND product_buying_price > 0 ');
		$query = $this->db->get('product');

		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$product_unitprice = $value->product_unitprice;
				$product_buying_price = $value->product_buying_price;
				$preferred_price = $value->preferred_price;
				$product_id = $value->product_id;

				// var_dump($product_id);die();
				if($product_id > 0)
				{

					// use 100 

					if(!empty($product_unitprice) AND !empty($product_buying_price))
					{
						$profit = $product_unitprice - $product_buying_price;

						$profit_percentage = $profit  * 100/ ($product_buying_price);


					}
					if($profit_percentage != 33)
					{

						$selling_price = ($product_buying_price * (33/100)) + $product_buying_price;
						$mark_up = 33;

						$array_product['preferred_price'] = $selling_price;
						$array_product['product_unitprice'] = $selling_price;
						$this->db->where('product_id',$product_id);
						$this->db->update('product',$array_product);


						$array_product_two['service_charge_amount'] = $selling_price;
						$this->db->where('product_id',$product_id);
						$this->db->update('service_charge',$array_product_two);

					}

					// var_dump($profit_percentage);die();

					

					// // $this->db->where('product_id',$product_id);
					// // $array_charge['service_charge_amount'] = $selling_price;
					// // $this->db->update('service_charge',$array_charge);


					// $this->db->where('order_supplier_id',$order_supplier_id);
					// $array_charge_old['selling_unit_price'] = $selling_price;
					// $array_charge_old['mark_up'] = 33;

					// // var_dump($selling_price);die();
					// $this->db->update('order_supplier',$array_charge_old);
				}
			}
		}
	}

	public function update_prices_product_names()
	{
		
		$query = $this->db->query('SELECT product_name
					FROM product
					WHERE product_deleted = 0
					GROUP BY product_name
					HAVING ( COUNT(product_name) > 1 )');

		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$product_name = $value->product_name;

				$query_two = $this->db->query('SELECT *
					FROM product WHERE product_name = "'.$product_name.'"');
				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value2) {

						$product_id = $value2->product_id;
						$category_id = $value2->category_id;
						$product_deleted = $value2->product_deleted;

						$query_three = $this->db->query('SELECT *
					FROM order_item WHERE product_id = '.$product_id);


						if($query_three->num_rows() > 0  AND $category_id == 0)
						{
							// update product
							$array['category_id'] = 2;
							$array['store_id'] = 5;
							$array['product_deleted'] = 0;
							$this->db->where('product_id',$product_id);
							$this->db->update('product',$array);

							$array2['owning_store_id'] = 5;
							$this->db->where('product_id',$product_id);
							$this->db->update('store_product',$array2);
						}
						else if($query_three->num_rows() == 0  AND $category_id > 0)
						{

							$array['product_deleted'] = 1;
							$this->db->where('product_id',$product_id);
							$this->db->update('product',$array);

							$array23['service_charge_delete'] = 1;
							$this->db->where('product_id',$product_id);
							$this->db->update('service_charge',$array23);
						}



					}
				}

			}
		}

	}
	public function delete_all_drugs()
	{

		$query = $this->db->query('SELECT *
					FROM product,store_product
					WHERE store_product.product_id = product.product_id AND product.product_deleted = 0 AND product.store_id = 0 AND store_product.owning_store_id = 0 ');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;

				$array['product_deleted'] = 1;
				$this->db->where('product_id',$product_id);
				$this->db->update('product',$array);

				$array23['service_charge_delete'] = 1;
				$this->db->where('product_id',$product_id);
				$this->db->update('service_charge',$array23);
			}
		}
	}

	public function update_orders()
	{

		$query = $this->db->query('SELECT *
					FROM order_supplier,order_item
					WHERE order_supplier.order_item_id = order_item.order_item_id AND order_supplier.vat > 0 GROUP BY order_item.product_id');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;

				$array23['vatable'] = 1;
				$this->db->where('product_id',$product_id);
				$this->db->update('service_charge',$array23);
			}
		}

	}
	public function update_chronic()
	{

		$query = $this->db->query('SELECT order_supplier.*,product.product_id,orders.store_id
					FROM order_supplier,order_item,product,orders
					WHERE order_supplier.order_item_id = order_item.order_item_id AND orders.order_id = order_item.order_id AND product.product_id = order_item.product_id AND product.category_id = 4 AND order_supplier.discount > 0  GROUP BY order_item.product_id,orders.store_id');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;
				$discount = $value->discount;
				$buying_unit_price = $value->buying_unit_price;
				$store_id = $value->store_id;
				$buying_unit_price = $buying_unit_price * ((100-$discount)/100);

				$selling_price =  $buying_unit_price * 1.21;





				if($store_id == 6)
				{
					$array_product['whole_sale_price'] = $selling_price;
				}
				else
				{
					$array_product['product_unitprice'] = $selling_price;
				}
				
				$this->db->where('product_id',$product_id);
				$this->db->update('product',$array_product);


				if($store_id == 6 AND $selling_price > 0)
				{
					$array_charge['whole_sale_price'] = $selling_price;
				}
				else
				{
					$array_charge['service_charge_amount'] = $selling_price;
				}


				$this->db->where('product_id',$product_id);
				$this->db->update('service_charge',$array_charge);
			}
		}

	}

	public function update_discounts()
	{

		$query = $this->db->query('SELECT order_supplier.*,product.product_id,orders.store_id
					FROM order_supplier,order_item,product,orders
					WHERE order_supplier.order_item_id = order_item.order_item_id AND orders.order_id = order_item.order_id AND product.product_id = order_item.product_id  AND order_supplier.discount > 0  GROUP BY order_item.product_id,orders.store_id');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;
				$discount = $value->discount;
				$buying_unit_price = $value->buying_unit_price;
				$store_id = $value->store_id;
				





				$array_product['discount'] = $discount;
				$this->db->where('product_id',$product_id);
				$this->db->update('product',$array_product);


			}
		}

	}



	public function update_expiries()
	{
		$where = 'product.stock_take = 1 AND product.product_deleted = 0';
		$table = 'product';


		$this->db->where($where);
		$query = $this->db->get($table);


		if($query->num_rows() > 0)
		{
			foreach($query->result() as $key => $value)
			{
				$stock_amount = $value->stock_amount;
				$product_id = $value->product_id;
				$whole_sale_stock = $value->whole_sale_stock;


				if($stock_amount > 0)
				{
					$this->db->where('order_item.product_id = '.$product_id.' AND order_item.order_item_id = order_supplier.order_item_id AND orders.order_id = order_item.order_id AND orders.order_approval_status = 7 AND orders.store_id = 5');
					$this->db->limit(1);
					$this->db->select('order_supplier.expiry_date,order_supplier.quantity_received');
					$this->db->order_by('orders.supplier_invoice_date','DESC');
					$query_new = $this->db->get('orders,order_item,order_supplier');


				// var_dump($query_new);die();
					if($query_new->num_rows() > 0)
					{
						foreach($query_new->result() as $key => $value2)
						{
							$expiry_date = $value2->expiry_date;
							$quantity_received = $value2->quantity_received;
						}
						$array_update['last_expiry_date_retail'] = $expiry_date;
						$array_update['last_quantity_update_retail'] = $quantity_received;
						$this->db->where('product_id',$product_id);
						$this->db->update('product',$array_update);
					}
				}


				if($whole_sale_stock > 0)
				{

					$this->db->where('order_item.product_id = '.$product_id.' AND order_item.order_item_id = order_supplier.order_item_id AND orders.order_id = order_item.order_id AND orders.order_approval_status = 7 AND orders.store_id = 6');
					$this->db->limit(1);
					$this->db->select('order_supplier.expiry_date,order_supplier.quantity_received');
					$this->db->order_by('orders.supplier_invoice_date','DESC');
					$query_new = $this->db->get('orders,order_item,order_supplier');

					if($query_new->num_rows() > 0)
					{
						foreach($query_new->result() as $key => $value2)
						{
							$expiry_date = $value2->expiry_date;
							$quantity_received = $value2->quantity_received;
						}

						$array_update['last_expiry_date_wholesale'] = $expiry_date;
						$array_update['last_quantity_update_wholesale'] = $quantity_received;
						$this->db->where('product_id',$product_id);
						$this->db->update('product',$array_update);
					}

					
				}
			}
		}
	}

	public function update_expiries_with_additions()
	{
		$where = 'product.stock_take = 1 AND product.product_deleted = 0 AND (last_expiry_date_wholesale IS NULL OR  last_expiry_date_retail IS NULL) AND product_purchase.product_id = product.product_id';
		$table = 'product,product_purchase';

		$this->db->select('product_purchase.*');
		$this->db->where($where);
		$query = $this->db->get($table);

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$product_id = $value->product_id;
				$store_id = $value->store_id;
				$expiry_date = $value->expiry_date;
				$purchase_pack_size = $value->purchase_pack_size;
				$purchase_quantity = $value->purchase_quantity;

				$quantity_received = $purchase_pack_size*$purchase_quantity;


				if($store_id == 6)
				{
					$array_update['last_expiry_date_wholesale'] = $expiry_date;
					$array_update['last_quantity_update_wholesale'] = $quantity_received;
					$this->db->where('product_id',$product_id);
					$this->db->update('product',$array_update);
				}
				else
				{
					$array_update['last_expiry_date_retail'] = $expiry_date;
					$array_update['last_quantity_update_retail'] = $quantity_received;
					$this->db->where('product_id',$product_id);
					$this->db->update('product',$array_update);
				}

				
			}
		}


		
	}


	public function expiry_update($order_supplier_id,$product_id,$order_id)
	{
		$data['product_id'] = $product_id;
		$data['order_supplier_id'] = $order_supplier_id;
		$data['order_id'] = $order_id;
		// var_dump($data);die();
			
		$page = $this->load->view('orders/sidebar/expiry_detail',$data);

		echo $page;
	}


	public function expiry_update_details()
	{
		$order_supplier_id = $this->input->post('order_supplier_id');
		$product_id = $this->input->post('product_id');
		$expiry_date = $this->input->post('expiry_date');
		$quantity_received = $this->input->post('quantity_received');
		$store_id = $this->input->post('store_id');

		$array_update['expiry_date'] = $expiry_date;
		$this->db->where('order_supplier_id',$order_supplier_id);
		$this->db->update('order_supplier',$array_update);


		if($store_id == 6)
		{
			$array_update_old['last_expiry_date_wholesale'] = $expiry_date;
			$array_update_old['last_quantity_update_wholesale'] = $quantity_received;
			$this->db->where('product_id',$product_id);
			$this->db->update('product',$array_update_old);
		}
		else
		{
			$array_update_new['last_expiry_date_retail'] = $expiry_date;
			$array_update_new['last_quantity_update_retail'] = $quantity_received;
			$this->db->where('product_id',$product_id);
			$this->db->update('product',$array_update_new);
		}

		$response['message'] = 'success';

		echo json_encode($response);
	}

	public function product_list_items($product_id,$store_id)
	{
	
		$this->inventory_management_model->update_store_items($product_id,$store_id);
	}


	public function get_updates()
	{
		$this->db->where('product_new.product_id > 0');
		$query = $this->db->get('product_new');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$product_id = $value->product_id;
				$product_unitprice = $value->product_unitprice;


				$array_update['product_unitprice'] = $product_unitprice;
				$this->db->where('product_id',$product_id);
				$this->db->update('product',$array_update);

			}
		}
	}


}

?>
