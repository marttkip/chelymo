<?php
$patient = $this->reception_model->patient_names2($patient_id);
$account_balance = $patient['account_balance'];

if($close_page == 2)
{
	$color = 'panel-warning';
}
else
{
	$color = '';
}


$rejected_amount += $total_rejected;

$department_id_to = '';
$department_name_to = '';
if(!empty($visit_id))
{
	$rs_pa = $this->nurse_model->get_prescription_notes_visit($visit_id);
	$visit_prescription = count($rs_pa);


	$visit_rs = $this->accounts_model->get_visit_details($visit_id);
	$visit_type_id = 0;
	if($visit_rs->num_rows() > 0)
	{
		foreach ($visit_rs->result() as $key => $value) {
			$department_id_to = $value->department_id;

				$department_name_to = $this->reception_model->get_department_name($department_id_to);
		}
	}

}
else
{
	$visit_prescription =0;
	$visit_sick_leave =0;
}



?>

<div class="col-md-2">
	<div id="sidebar-detail"></div>
</div>
<div class="col-md-10">
	<br>
		<div class="pull-right">

			<?php
			$personnel_id = $this->session->userdata('personnel_id');
			if(!empty($visit_id))
			{
				?>
				<a href="<?php echo site_url().'accounts/send_to_department/'.$visit_id.'/1';?>"  class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to send to department ?')"><i class="fa fa-plus"></i> Send to Department </a>


				 <a  class="btn btn-sm btn-primary" onclick="add_invoice(<?php echo $patient_id;?>,<?php echo $visit_id;?>)"><i class="fa fa-plus"></i> Add invoice </a>
				<?php
			}
			?>
			<a  class="btn btn-sm btn-info" onclick="add_credit_note(<?php echo $patient_id;?>,<?php echo $visit_id;?>)"><i class="fa fa-plus"></i> Add Credit Note </a>
			<!-- <a href="" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Add Credit Note </a> -->
			<a  class="btn btn-sm btn-success" onclick="add_payment(<?php echo $patient_id;?>)"><i class="fa fa-plus"></i> Add Payment </a>
			<a href="<?php echo site_url().'print-patient-statement/'.$patient_id?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Statement</a>

			 <a href="<?php echo site_url();?>accounts/send_notification/<?php echo $visit_id?>/<?php echo $patient_id?>" class="btn btn-default btn-sm  " onclick="return confirm('Do you want to send this notification ?')" ><i class="fa fa-folder"></i> Send notification </a>
			<?php
			if(!empty($visit_id))
			{
				?>
				 <a href="<?php echo site_url();?>accounts/end_visit/<?php echo $visit_id?>" class="btn btn-danger btn-sm  " onclick="return confirm('Do you want to close this visit ?')" ><i class="fa fa-folder"></i> Close this visit </a>
				<?php
			}
			?>
					 
			<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm  " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
		</div>
		 
	<br>
	<br>
	

		<div class="panel-body" style="height: 78vh;overflow-y: scroll;">
	
			<div class="col-md-12">
				<?php
					$error = $this->session->userdata('error_message');
					$success = $this->session->userdata('success_message');

					if(!empty($error))
					{
						echo '<div class="alert alert-danger">'.$error.'</div>';
						$this->session->unset_userdata('error_message');
					}

					if(!empty($success))
					{
						echo '<div class="alert alert-success">'.$success.'</div>';
						$this->session->unset_userdata('success_message');
					}


				 ?>
			</div>
				
			<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id?>">
			<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
		
				<h4 style="border-bottom: 1px solid #000;"><i class="fa fa-star"></i> INCOMPLETE INVOICES</h4>
				<br>
				<div id="incomplete-invoices"></div>
				<h4 style="border-bottom: 1px solid #000;"><i class="fa fa-money"></i> COMPLETED INVOICES</h4>
				<br>
				<div id="patient-statement"></div>



		</div>

		<div class="row" style="margin-top:10px;">
			<div class="center-align">


				
						

				<?php

				$items = '';
				if($visit_id > 0)
				{
					
					$all_departments_rs = $this->reception_model->get_patient_departments($visit_id,1);
				
					if($all_departments_rs->num_rows() > 0)
					{
						foreach ($all_departments_rs->result() as $key => $value) {
							$department_name = $value->department_name;
							$department_id = $value->department_id;
							$created = $value->created;
							$picked = $value->picked;
							$personnel_onames = $value->personnel_onames;
							?>
							<div class="col-md-2">
								<?php echo form_open("accounts/send_to_department/".$visit_id."/".$department_id, array("class" => "form-horizontal"));?>
									<input type="hidden" name="visit_department_id" value="<?php echo $close_page;?>">
									<input type="submit" class="btn btn-sm btn-success center-align" value="Send to <?php echo $department_name;?> " onclick="return confirm('Are you sure you want to send to <?php echo $department_name;?> ?');"/>
								<?php echo form_close();?>
							</div>
							<?php
						}
					}
					else
					{

						// check if there is a visit that was prior

						$checked_number = $this->accounts_model->get_patient_visit_today($patient_id,$visit_id);
						?>
						<?php

						if($checked_number > 0)
						{
							?>

							<div class="col-md-2">
								<?php echo form_open("accounts/send_to_doctor_department/".$visit_id."/".$department_id_to, array("class" => "form-horizontal"));?>
									<input type="hidden" name="visit_department_id" value="<?php echo $close_page;?>">
									<input type="submit" class="btn btn-sm btn-success center-align" value="Send to <?php echo $department_name_to;?> " onclick="return confirm('Are you sure you want to send to <?php echo $department_name_to;?> ?');"/>
								<?php echo form_close();?>
							</div>


							<?php
						}
						else
						{

							if($department_id_to == 5 OR $department_id_to == 6 OR $department_id_to == 12)
							{
								?>
								<div class="col-md-2">
									<div class="center-align">

											<?php echo form_open("nurse/send_to_walkin/".$visit_id."/".$department_id_to, array("class" => "form-horizontal"));?>
												<input type="hidden" name="visit_department_id" value="<?php echo $close_page;?>">
										
												<input type="submit" class="btn btn-sm btn-success center-align" value="Send To <?php echo $department_name_to?>" onclick="return confirm('Send to <?php echo $department_name_to?>?');"/>
											<?php echo form_close();?>
										</div>
									</div>

								<?php
							}
							else
							{
								?>
								<div class="col-md-2">
									<div class="center-align">

											<?php echo form_open("nurse/send_to_nurse/".$visit_id."/8", array("class" => "form-horizontal"));?>
												<input type="hidden" name="visit_department_id" value="<?php echo $close_page;?>">
												<input type="submit" class="btn btn-sm btn-success center-align" value="Send To Triage" onclick="return confirm('Send to Triage?');"/>
											<?php echo form_close();?>
										</div>
								</div>
								<?php
							}
							?>
							
							<?php
						}
						?>
						
						
						<div class="col-md-2 pull-right">
							<div class="center-align">
								<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
							</div>
						</div>


						<?php
					}
				}
					?>
				
			</div>
	
			
		</div>
		
</div>

  <!-- END OF ROW -->
<script type="text/javascript">


   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   		// display_procedure(<?php echo $visit_id;?>);
   		// alert(<?php echo $patient_id;?>);
   		get_patient_statement(<?php echo $patient_id;?>);
   		get_patient_incomplete_invoices(<?php echo $patient_id;?>);
   		get_sidebar_details(<?php echo $patient_id;?>,<?php echo $visit_id;?>);
   });



  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");

        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }

  }
  function check_payment_type(payment_type_id){


    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check

      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;

      if (window.XMLHttpRequest) {

          XMLHttpRequestObject = new XMLHttpRequest();
      }

      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      var config_url = document.getElementById("config_url").value;
      var close_page = document.getElementById("close_page").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id+"/1";
      // alert(url);
      if(XMLHttpRequestObject) {

          XMLHttpRequestObject.open("GET", url);

          XMLHttpRequestObject.onreadystatechange = function(){

              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }

          XMLHttpRequestObject.send(null);
      }


  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id=null,visit_invoice_id=null){
		// alert(id);
	    var units = document.getElementById('units'+id).value;
	    var billed_amount = document.getElementById('billed_amount'+id).value;

	    grand_total(id, units, billed_amount, v_id,visit_invoice_id);

	}

  function grand_total(procedure_id, units, amount, v_id=null,visit_invoice_id=null)
  {

		 var config_url = document.getElementById("config_url").value;
		 var patient_id = document.getElementById('charge_patient_id').value;
	     var data_url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;

	     var visit_comment = document.getElementById('visit_comment'+procedure_id).value;

	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id,notes: null,visit_comment: visit_comment,patient_id: patient_id,amount: amount},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         // display_patient_bill(v_id);

	         alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        // display_billing(v_id);
	    alert(error);
	    }

	    });

	    var patient_id = document.getElementById("patient_id").value;
		var visit_id = document.getElementById("visit_id").value;
		get_visit_charges(visit_id,patient_id,visit_invoice_id);


	   //  var XMLHttpRequestObject = false;

	   //  if (window.XMLHttpRequest) {

	   //      XMLHttpRequestObject = new XMLHttpRequest();
	   //  }

	   //  else if (window.ActiveXObject) {
	   //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	   //  }
	   //  var config_url = document.getElementById("config_url").value;

	   //  var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   //  // alert(url);
	   //  if(XMLHttpRequestObject) {

	   //      XMLHttpRequestObject.open("GET", url);

	   //      XMLHttpRequestObject.onreadystatechange = function(){

	   //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
				// {
	   //  			// display_patient_bill(v_id);
	   //  			display_billing(v_id);
	   //          }
	   //      }

	   //      XMLHttpRequestObject.send(null);
	   //  }
	}


	function delete_service(id, visit_id){

		var res = confirm('Do you want to remove this charge ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;

		    if (window.XMLHttpRequest) {

		        XMLHttpRequestObject = new XMLHttpRequest();
		    }

		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"accounts/delete_service_billed/"+id;

		    if(XMLHttpRequestObject) {

		        XMLHttpRequestObject.open("GET", url);

		        XMLHttpRequestObject.onreadystatechange = function(){

		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                // display_patient_bill(visit_id);
		                display_procedure(visit_id);
		            }
		        }

		        XMLHttpRequestObject.send(null);
		    }
		}

	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}



	function parse_procedures(visit_id,suck)
    {
      var procedure_id = document.getElementById("service_id_item").value;
       procedures(procedure_id, visit_id, suck);

    }

	function procedures(id, v_id, suck){

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {

            XMLHttpRequestObject = new XMLHttpRequest();
        }

        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var url = "<?php echo site_url();?>accounts/accounts_update_bill/"+id+"/"+v_id+"/"+suck;

         if(XMLHttpRequestObject) {

            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function(){

                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    // document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                    display_patient_bill(v_id);
                }
            }

            XMLHttpRequestObject.send(null);
        }

    }
    function display_procedure(visit_id){

	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    // var url = config_url+"nurse/view_procedure/"+visit_id;
	    var url = config_url+"accounts/view_procedure/"+visit_id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_procedure(id, visit_id,visit_invoice_id=null){
	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_procedure/"+id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {


	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
      // display_patient_bill(visit_id);
      var patient_id = document.getElementById("patient_id").value;
      var visit_id = document.getElementById("visit_id").value;
      get_visit_charges(visit_id,patient_id,visit_invoice_id);
	}

	$(document).on("change","select#visit_type_id",function(e)
	{
		var visit_type_id = $(this).val();

		if(visit_type_id != '1')
		{
			$('#insured_company').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#insured_company').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}




	});

	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"accounts/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         display_patient_bill(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        display_patient_bill(v_id);
		    	alert(error);
		    }

		    });

		}

	}


	function get_patient_statement(patient_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"accounts/get_patient_statement/"+patient_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#patient-statement").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}

	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	function add_invoice(patient_id,visit_id=null)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 
		var visit_id = $('#visit_id').val();
		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_invoices/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			// alert(data);
				
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}


	function get_visit_invoices(patient_id)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_invoices/"+visit_id+"/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			document.getElementById("visit-invoice-div").style.display = "block"; 
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			$('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_procedures_done(patient_id,visit_id,visit_type_id,visit_invoice_id =null)
	{
		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;
		var data_url = config_url+"accounts/search_procedures/"+patient_id+"/"+visit_id+"/"+visit_type_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		var lab_test = $('#search_procedures').val();
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#searched-procedures").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function get_visit_charges(visit_id=null,patient_id,visit_invoice_id = null)
	{
		var config_url = $('#config_url').val();

		if(visit_id > 0)
		{

		}
		else
		{
			visit_id =null;
		}
		var data_url = config_url+"accounts/get_visit_charges/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#visit-charges").html(data);

			$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function add_payment(patient_id,visit_invoice_id=null)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/add_payment/"+patient_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			// alert(data);
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			get_visit_payments(patient_id,null);

			// if(visit_invoice_id > 0)
			// {
				get_visit_invoice_payments(patient_id,visit_invoice_id);
			// }
			// alert(data);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
		
	}


	function get_visit_payments(patient_id,payment_id=null)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_payments/"+patient_id+"/"+payment_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_visit_invoice_payments(patient_id,visit_invoice_id=null)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_invoice_payments/"+patient_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-invoice-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function add_payment_item(patient_id,payment_id= null)
	{
		var config_url = $('#config_url').val();

		 var amount_paid = document.getElementById("amount_paid").value;
		 var invoice_id = document.getElementById("invoice_id").value;

		 // window.alert(amount_paid);
		var data_url = config_url+"accounts/add_payment_item/"+patient_id+"/"+payment_id;
		
		// window.alert(invoice_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{invoice_detail: invoice_id,amount: amount_paid},
		dataType: 'text',
		success:function(data){
		
			get_visit_payments(patient_id,payment_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	$(document).on("submit","form#confirm-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		// var visit_id = $('#charge_visit_id').val();
		var patient_id = $('#payment_patient_id').val();
		var payment_id = $('#payment_payment_id').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/confirm_payment/"+patient_id+"/"+payment_id;
		// alert(url);
		
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function get_patient_incomplete_invoices(patient_id)
	{

		var config_url = $('#config_url').val();
		var visit_id = $('#visit_id').val();
	 	var url = config_url+"accounts/get_incomplete_invoices/"+patient_id+"/"+visit_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#incomplete-invoices").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}



$(document).on("submit","form#confirm-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var visit_id = $('#charge_visit_id').val();
	var patient_id = $('#charge_patient_id').val();
	var visit_invoice_id = $('#visit_invoice_id').val();
	var config_url = $('#config_url').val();	

	var url = config_url+"accounts/confirm_visit_charge/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	 
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    
      	if(data.message == "success")
		{
			
			
			close_side_bar();
			get_patient_incomplete_invoices(patient_id);
			get_patient_statement(patient_id);
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});

function invoice_details_view(visit_invoice_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_invoices/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_charges(visit_id,patient_id,visit_invoice_id);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}


function payments_details_view(payment_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_payments/"+patient_id+"/"+payment_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_payments(patient_id,payment_id);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function delete_payment(payment_item_id,patient_id,payment_id)
{
	var res = confirm('Are you sure you want to delete this payment item ?');

	if(res)
	{
		var XMLHttpRequestObject = false;
	                
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/delete_payment_item/"+payment_item_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	            	get_visit_payments(patient_id,payment_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	 }
}


$(document).on("change","select#visit_type_id",function(e)
{
	var visit_type_id = $(this).val();
	// alert(visit_type_id);
	if(visit_type_id != '1')
	{
		$('#insured_company').css('display', 'block');
		$('#insured_company2').css('display', 'block');
		// $('#consultation').css('display', 'block');
	}
	else
	{
		$('#insured_company').css('display', 'none');
		$('#insured_company2').css('display', 'none');
		// $('#consultation').css('display', 'block');
	}
	
	

});


function add_service_charge_test(service_charge_id,visit_id,patient_id,visit_type_id=null,visit_invoice_id =null)
{
	var res = confirm('Are you sure you want to charge ?');

	if(res)
	{	

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/add_visit_charge/"+service_charge_id+"/"+visit_id+"/"+patient_id+"/"+visit_type_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_invoice: visit_invoice_id},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById('search_procedures').value = '';
			$('#charges-div').css('display', 'none');

			if(visit_type_id == null)
			{
				get_visit_procedures_done(visit_id);
			}
			else
			{
				get_visit_charges(visit_id,patient_id,visit_invoice_id);
			}
			
			
			// $('#bottom-div').css('display', 'block');
			// tinymce.init({
   //              selector: ".cleditor",
   //             	height: "150"
	  //           });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

}

function delete_invoice(visit_invoice_id,patient_id)
{
	var res = confirm('Are you sure you want to delete this invoice ?');

	if(res)
	{	
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/delete_invoice/"+visit_invoice_id+"/"+patient_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{visit_invoice_id: visit_invoice_id},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	}

}
	

function delete_payment_receipt(payment_id,patient_id)
{
	var res = confirm('Are you sure you want to delete this payment ?');

	if(res)
	{	
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/delete_payment/"+payment_id+"/"+patient_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{payment_id: payment_id},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    	// alert(data);
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				// get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert('Sorry could not delete this payment please try again');
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	}

}

function update_authorised_amount()
{
	var preauth_amount = $('#preauth_amount').val();
	var total_invoice = $('#amount').val();

	if(preauth_amount > 0)
	{
		preauth_amount = preauth_amount;
	}
	else
	{
		preauth_amount = 0;
	}
	// alert(preauth_amount);
	// alert(total_invoice);

	var preauth_amount = parseInt(preauth_amount);
	var total_invoice = parseInt(total_invoice);
	
	if(preauth_amount > total_invoice)
	{
		$('#notification-div').css('display', 'block');
		$('#note-div').css('display', 'none');

		$("#message_warning").html('<div class="alert alert-danger">Kindly note that the amount you entered cannot be more than the total invoice amount.</div>');
		

	}

	else if(preauth_amount == total_invoice)
	{
		$('#notification-div').css('display', 'none');
		$('#note-div').css('display', 'none');
		
	}
	else
	{
		var balance = total_invoice - preauth_amount;
		$('#notification-div').css('display', 'block');
		$('#note-div').css('display', 'block');
		$("#message_warning").html('<div class="alert alert-info">Please note that this invoice will be credited with an amount of '+balance+' and on completing this invoice another Cash invoice of the same amount will be created to cover for difference between the Invoiced amount and insurance authorised amount.</div>');
	}

}



// credit note 



// credit NOTE


function add_credit_note(patient_id,visit_id=null,visit_invoice_id=null)
{
	// alert(visit_id);
	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_credit_notes/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_credit_note(visit_id,patient_id,visit_invoice_id);

		get_all_credit_notes(patient_id,visit_invoice_id);

		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}

function get_procedures_credit_note(patient_id,visit_id=null,visit_type_id,visit_invoice_id =null)
{
	var config_url = $('#config_url').val();
	// var visit_type_id = document.getElementById("visit_type_id").value;
	var data_url = config_url+"accounts/search_procedures/"+patient_id+"/"+visit_id+"/"+visit_type_id+"/"+visit_invoice_id+"/1";
	// window.alert(data_url);
	$('#charges-div').css('display', 'block');
	var lab_test = $('#search_procedures').val();
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : lab_test},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
	$("#searched-procedures").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function add_credit_note_charge(service_charge_id,visit_id,patient_id,visit_type_id=null,visit_credit_note_id =null)
{
	var res = confirm('Are you sure you want to this credit note item ?');

	if(res)
	{	

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/add_credit_note_charge/"+service_charge_id+"/"+visit_id+"/"+patient_id+"/"+visit_type_id+"/"+visit_credit_note_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_credit_note_id: visit_credit_note_id},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById('search_procedures').value = '';
			$('#charges-div').css('display', 'none');

			// if(visit_type_id == null)
			// {
			// 	get_visit_procedures_done(visit_id);
			// }
			// else
			// {
				get_visit_credit_note(visit_id,patient_id,visit_credit_note_id);
			// }
			

			
			// $('#bottom-div').css('display', 'block');
			// tinymce.init({
   //              selector: ".cleditor",
   //             	height: "150"
	  //           });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

}


function get_visit_credit_note(visit_id=null,patient_id,visit_invoice_id=null,visit_credit_note_id = null)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_credit_note/"+visit_id+"/"+patient_id+"/"+visit_invoice_id+"/"+visit_credit_note_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#visit-charges").html(data);

		
		$('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}



$(document).on("submit","form#confirm-credit-note",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var visit_id = $('#charge_visit_id').val();
	var patient_id = $('#charge_patient_id').val();
	var config_url = $('#config_url').val();	

	var url = config_url+"accounts/confirm_credit_note/"+visit_id+"/"+patient_id;
	 // alert(patient_id);
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    
      	if(data.message == "success")
		{
			
			
			close_side_bar();
			get_patient_statement(patient_id);
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});



function get_all_credit_notes(patient_id,visit_invoice_id=null,visit_credit_note_id = null)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_all_credit_notes/"+patient_id+"/"+visit_invoice_id+"/"+visit_credit_note_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#all-credit-notes").html(data);

		
		$('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}





$(document).on("submit","form#approve-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var patient_id = $('#patient_id').val();
	var visit_id = $('#visit_id').val();
	
	var config_url = $('#config_url').val();	

	var url = config_url+"accounts/approved_invoice/"+visit_id;
	 // alert(patient_id);
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    	// alert(data);
      	if(data.message == "success")
		{
			
			
			close_side_bar();
				get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});


function mark_as_invoice(visit_invoice_id)
{

	var res = confirm('Are you sure your want to mark this proforma invoice as invoice ?');


	if(res)
	{
		// alert(form_data);
		var patient_id = $('#patient_id').val();
		var visit_id = $('#visit_id').val();
		
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/mark_as_invoice/"+visit_id;
		 // alert(patient_id);
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{visit_invoice_id: visit_invoice_id},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    	// alert(data);
	      	if(data.message == "success")
			{
				
				close_side_bar();
				get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	}
}
function get_sidebar_details(patient_id,visit_id)
{

	var config_url = $('#config_url').val();
 	var url = config_url+"reception/get_sidebar_details/"+patient_id+"/"+visit_id;
 	// alert(url);
	$.ajax({
		type:'POST',
		url: url,
		data:{query: null},
		dataType: 'text',
		processData: false,
		contentType: false,
		success:function(data){
		var data = jQuery.parseJSON(data);
		  // alert(data.content);
		if(data.message == "success")
		{
			$("#sidebar-detail").html(data.result);
		}
		else
		{
			alert('Please ensure you have added included all the items');
		}

	},
	error: function(xhr, status, error) {
	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	}
	});

}
	

$(document).on("submit","form#payments-paid-form",function(e)
	{
		// alert("changed");
		e.preventDefault();	

		var cancel_action_id = 2;//$('#payment_cancel_action_id').val();
		var cancel_description = 'Money refunded to patient';//$('#payment_cancel_description').val();
		var visit_id = $('#payment_visit_id').val();
		var payment_id = $('#payment_id').val();
		var visit_invoice_id = $('#visit_invoice_id').val();
		var patient_id = $('#payment_patient_id').val();
		var amount_refunded = 16000;//$('#payment_amount_refunded').val();
		var refund_date = $('#refund_date').val();
		var account_from_id = 2;//$('#account_from_id').val();
		// var form_data = new FormData(this);
		// alert(cancel_description);

		
		var url = "<?php echo base_url();?>accounts/cancel_payment/"+payment_id+"/"+visit_id;		
		$.ajax({
		type:'POST',
		url: url,
		data:{cancel_description: cancel_description, cancel_action_id: cancel_action_id,patient_id: patient_id,visit_id: visit_id,payment_id: payment_id,visit_invoice_id: visit_invoice_id,amount_refunded:amount_refunded,refund_date:refund_date,account_from_id:account_from_id},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				alert("You have successfully added refund note");
			 	$('#refund_payment'+payment_id).modal('toggle');
			 	close_side_bar();
			 	get_patient_incomplete_invoices(patient_id);
					get_patient_statement(patient_id);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// get_page_header(visit_id);
			// get_patient_receipt(visit_id);
		}
		});
		return false;
	});


function assign_to_invoice(patient_id,visit_id,visit_charge_id)
{


		

		open_sidebar();
		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/allocate_to_invoice/"+patient_id+"/"+visit_id+"/"+visit_charge_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#sidebar-div").html(data);

			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


}

function assign_charge_to_invoice(visit_invoice_id,visit_charge_id,patient_id)
{

	var res = confirm('Are you sure you want to confirm this charge ?');


	if(res)
	{

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/assign_charge_to_invoice/"+visit_invoice_id+"/"+visit_charge_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			close_side_bar();
			get_patient_statement(patient_id);
			get_patient_incomplete_invoices(patient_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

	}
	
}




</script>
