<?php
$patient_id = 2;

$cancel_actions = $this->accounts_model->get_cancel_actions();
$visit_types_rs = $this->reception_model->get_visit_types();
$patient = $this->reception_model->get_patient_data($patient_id);
$patient = $patient->row();
$patient_othernames = $patient->patient_othernames;
$patient_surname = $patient->patient_surname;

$doctor = $this->reception_model->get_providers();


$title = $patient_othernames.' '.$patient_surname;




$order = 'service_charge.service_charge_name';
$where = 'service_charge.service_id = service.service_id AND service.service_name <> "Medical Services" AND service.service_delete = 0 AND service_charge.service_charge_status = 1 AND  service_charge.service_charge_delete = 0 AND service_charge.visit_type_id = visit_type.visit_type_id AND service_charge.visit_type_id = 1';

$table = 'service_charge,visit_type,service';
$config["per_page"] = 0;
$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

$rs9 = $procedure_query->result();
$procedures = '';
foreach ($rs9 as $rs10) :


$procedure_id = $rs10->service_charge_id;
$proced = $rs10->service_charge_name;
$visit_type = $rs10->visit_type_id;
$visit_type_name = $rs10->visit_type_name;

$stud = $rs10->service_charge_amount;

    $procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";

endforeach;

$services_list = $procedures;



$order = 'service.service_name';
$where = 'service.service_name <> "Pharmacy" AND service_status = 1';

$table = 'service';
$service_query = $this->nurse_model->get_other_procedures($table, $where, $order);

$rs9 = $service_query->result();
$services_items = '';
foreach ($rs9 as $rs11) :


	$service_id = $rs11->service_id;
	$service_name = $rs11->service_name;

	$services_items .="<option value='".$service_id."'>".$service_name."</option>";

endforeach;

$services_items = $services_items;
?>

	
<div class="row" >	
	<div class="col-md-12" style="background: #fff;min-height:500px;">
		<div class="row">
			<header class="panel-heading">						
				<div id="page_header"></div>
			</header>
		</div>
		<div class="row">
			<?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal","id"=>"add_bill"));?>
        	<div class="col-md-12 " style="margin-top: 10px">
                <div class="col-md-4" >
                  <div class="form-group">
                  <label class="col-md-2 control-label">Service: </label>
                  	<div class="col-md-10">
	                    <select id='service_id_item' name='service_id' class='form-control custom-select ' >
	                      <option value=''>None - Please Select a service</option>
	                       <?php echo $services_list;?>
	                    </select>

	                    <input type="hidden" name="visit_id_checked" id="visit_id_checked">

                    </div>
                  </div>
                </div>
                <div class="col-md-4" >
                  <div class="form-group " >
                  <label class="col-md-2 control-label">Provider: </label>
                  	<div class="col-md-10">
	                    <select id='provider_id_item' name='provider_id' class='form-control custom-select ' >
	                      <option value=''>None - Please Select a provider</option>
	                      <?php
						
								if(count($doctor) > 0){
									foreach($doctor as $row):
										$fname = $row->personnel_fname;
										$onames = $row->personnel_onames;
										$personnel_id = $row->personnel_id;
										
										if($personnel_id == set_value('personnel_id'))
										{
											echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
										}
										
										else
										{
											echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
										}
									endforeach;
								}
							?>
	                    </select>
	                </div>
                  </div>
                </div>
                <div class="col-md-4">
	                <div class="form-group">
						<label class="col-lg-2 control-label">Date: </label>
						
						<div class="col-lg-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_date" id="visit_date_date" placeholder="Admission Date" value="<?php echo date('Y-m-d');?>">
                            </div>
						</div>
					</div>
				</div>
            </div>
            <div class="col-md-12" style="margin:20px;">
            	<div class="center-align">
					<button type="submit" class='btn btn-info btn-sm' type='submit' >Add to Bill</button>
				</div>
            </div>
            <?php echo form_close();?>

        </div>
		<div class="row">
			<div id="patient_bill"></div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" id="add_assessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Service</h4>
            </div>
            <?php echo form_open("accounts/add_service_item", array("class" => "form-horizontal"));?>
            <div class="modal-body">
            	<div class="row">
                	<div class='col-md-12'>
                      	<div class="form-group">
							<label class="col-lg-4 control-label">Service Name: </label>
						  
							<div class="col-lg-8">
								<select id='parent_service_id' name='parent_service_id' class='form-control custom-select ' >
			                      <option value=''>None - Please Select a service</option>
			                       <?php echo $services_items;?>
			                    </select>
							</div>
						</div>
						 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                      	<div class="form-group">
							<label class="col-lg-4 control-label">Charge Name: </label>
						  
							<div class="col-lg-5">
								<input type="text" class="form-control" name="service_charge_item" placeholder="" autocomplete="off">
							</div>
						</div>				
                      	<div class="form-group">
							<label class="col-lg-4 control-label">Service Amount: </label>
						  
							<div class="col-lg-5">
								<input type="number" class="form-control" name="service_amount" placeholder="" autocomplete="off" >
							</div>
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Service</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="add_provider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Provider</h4>
                </div>
                 <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal"));?>
                <div class="modal-body">
                	<div class="row">
                    	<div class='col-md-12'>
                          	<div class="form-group">
								<label class="col-lg-4 control-label">First Name: </label>
							  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
								<div class="col-lg-5">
									<input type="text" class="form-control" name="personnel_fname" placeholder="" autocomplete="off">
								</div>
							</div>
                          	<div class="form-group">
								<label class="col-lg-4 control-label">Other Names: </label>
							  
								<div class="col-lg-5">
									<input type="text" class="form-control" name="personnel_onames" placeholder="" autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-4 control-label">Phone Number: </label>
							  
								<div class="col-lg-5">
									<input type="text" class="form-control" name="personnel_phone" placeholder="" autocomplete="off">
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Provider</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
                <?php echo form_close();?>
            </div>
        </div>
</div>
<input type="hidden" name="patient_id_item" id="patient_id_item" value="<?php echo $patient_id;?>">


<div class="modal fade bs-example-modal-lg" id="add_payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add to Bill</h4>
            </div>
             <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal","id"=>"add_payment"));?>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-2 ">
            		</div>
		            	<div class="col-md-10 ">
		            		<div class="form-group" >
								<div class="col-lg-4">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="1"  onclick="getservices(1)" > 
                                            Normal
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="2"> 
                                            Debit Note
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" >
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="3"  onclick="getservices(1)"> 
                                            Waiver
                                        </label>
                                    </div>
								</div>
							</div>
							 
                           	<!-- <input type="hidden" name="type_payment" value="1"> -->
                           	<!-- <input type="hidden" name="service_id" id="service_id" value="0"> -->
                           	<div id="normal_div" style="display: none;">
                           		<div id="service_div" class="form-group" >
	                                <label class="col-lg-2 control-label"> Services: </label>
	                                
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="service_id" >
	                                    	<option value="">--Select a service--</option>
											<?php
	                                        $service_rs = $this->accounts_model->get_all_service();
	                                        $service_num_rows = count($service_rs);
	                                        if($service_num_rows > 0)
	                                        {
												foreach($service_rs as $service_res)
												{
													$service_id = $service_res->service_id;
													$service_name = $service_res->service_name;
													
													echo '<option value="'.$service_id.'">'.$service_name.'</option>';
												}
	                                        }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>

	                        <div class="col-md-12" >

			                  <div class="form-group " >
			                  <label class="col-md-2 control-label">Provider: </label>
			                  	<div class="col-md-10">
				                    <select id='provider_id_item' name='provider_id' class='form-control custom-select ' >
				                      <option value=''>None - Please Select a provider</option>
				                      <?php
									
											if(count($doctor) > 0){
												foreach($doctor as $row):
													$fname = $row->personnel_fname;
													$onames = $row->personnel_onames;
													$personnel_id = $row->personnel_id;
													
													if($personnel_id == set_value('personnel_id'))
													{
														echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
													}
													
													else
													{
														echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
													}
												endforeach;
											}
										?>
				                    </select>
				                </div>
			                  </div>
			                </div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Amount: </label>
								  
									<div class="col-lg-8">
										<input type="text" class="form-control" name="amount_paid" id="amount_paid" placeholder="" autocomplete="off"  onkeyup="get_change()">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-lg-2 control-label">Payment Method: </label>
									  
									<div class="col-lg-8">
										<select class="form-control" name="payment_method" id="payment_method" onchange="check_payment_type(this.value)">
											<option value="0">Select a group</option>
	                                    	<?php
											  $method_rs = $this->accounts_model->get_payment_methods();
											  $num_rows = count($method_rs);
											 if($num_rows > 0)
											  {
												
												foreach($method_rs as $res)
												{
												  $payment_method_id = $res->payment_method_id;
												  $payment_method = $res->payment_method;
												  
													echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
												  
												}
											  }
										  ?>
										</select>
									  </div>
								</div>
                           		
                           	</div>
                           	<div id="waiver_div" style="display: none;">

                           		<div id="service_div" class="form-group" >
	                                <label class="col-lg-2 control-label"> Services: </label>
	                                
	                                <div class="col-lg-8">
	                                    <select class="form-control" name="waiver_service_id" >
	                                    	<option value="">--Select a service--</option>
											<?php
	                                        $service_rs = $this->accounts_model->get_all_service();
	                                        $service_num_rows = count($service_rs);
	                                        if($service_num_rows > 0)
	                                        {
												foreach($service_rs as $service_res)
												{
													$service_id = $service_res->service_id;
													$service_name = $service_res->service_name;
													
													echo '<option value="'.$service_id.'">'.$service_name.'</option>';
												}
	                                        }
	                                        ?>
	                                    </select>
	                                </div>
	                            </div>

	                         <div class="col-md-12" style="margin-bottom: 10px">
			                  <div class="form-group " >
			                  <label class="col-md-2 control-label">Provider: </label>
			                  	<div class="col-md-10">
				                    <select id='provider_id_item' name='provider_id' class='form-control custom-select ' >
				                      <option value=''>None - Please Select a provider</option>
				                      <?php
									
											if(count($doctor) > 0){
												foreach($doctor as $row):
													$fname = $row->personnel_fname;
													$onames = $row->personnel_onames;
													$personnel_id = $row->personnel_id;
													
													if($personnel_id == set_value('personnel_id'))
													{
														echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
													}
													
													else
													{
														echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
													}
												endforeach;
											}
										?>
				                    </select>
				                </div>
			                  </div>
			                </div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Amount: </label>
								  
									<div class="col-lg-8">
										<input type="text" class="form-control" name="waiver_amount" id="waiver_amount" placeholder="" autocomplete="off"  onkeyup="get_change()">
									</div>
								</div>
								
                           		
                           	</div>                          							
	                        

							<input type="hidden" class="form-control" name="change_payment" id="change_payment" placeholder="" autocomplete="off" >
							<div id="mpesa_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Mpesa TX Code: </label>

								<div class="col-lg-8">
									<input type="text" class="form-control" name="mpesa_code" id="mpesa_code" placeholder="">
								</div>
							</div>
						  
							<div id="insuarance_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Credit Card Detail: </label>
								<div class="col-lg-8">
									<input type="text" class="form-control" name="insuarance_number" id="insuarance_number" placeholder="">
								</div>
							</div>
						  
							<div id="cheque_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Cheque Number: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="cheque_number" id="cheque_number" placeholder="">
								</div>
							</div>
							<div id="bank_deposit_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Deposit Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="deposit_detail" id="deposit_detail" placeholder="">
								</div>
							</div>
							<div id="debit_card_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Debit Card Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="debit_card_detail" id="debit_card_detail" placeholder="">
								</div>
							</div>
						  
							<div id="username_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Username: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="username" id="username" placeholder="">
								</div>
							</div>
						  
							<div id="password_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Password: </label>
							  
								<div class="col-lg-8">
									<input type="password" class="form-control" name="password" id="password" placeholder="">
								</div>
							</div>
			            </div>
			           <input type="hidden" name="visit_id_payments" id="visit_id_payments">
			        </div>
            </div>
            <div class="modal-footer">
            	<h4 class="pull-left" > Change : <span id="change_item"></span></h4>
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Payment</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
 <div class="modal fade bs-example-modal-lg" id="end_visit_date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Discharge Patient</h4>
            </div>
            <?php echo form_open("", array("class" => "form-horizontal","id"=>"discharge-patient"));?>
            <div class="modal-body">
            	<div class="row">
                	<div class="col-md-12">
		                <div class="form-group">
							<label class="col-lg-2 control-label">Disharged Date: </label>
							
							<div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_charged" id="visit_date_charged" placeholder="Discharged Date" value="<?php echo date('Y-m-d');?>">
                                </div>
							</div>
						</div>
					</div>
                </div>
            </div>
			 <input type="hidden" name="visit_discharge_visit" id="visit_discharge_visit">
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Discharge Patient</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>

 <div class="modal fade bs-example-modal-lg" id="change_patient_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change Patient Visit Type</h4>
            </div>
            <?php echo form_open("accounts/change_patient_visit", array("class" => "form-horizontal","id"=>"visit_type_change"));?>
            <div class="modal-body">
            	<div class="row">
                	<div class='col-md-12'>
                      	<div class="form-group">
							<label class="col-lg-2 control-label">Type: </label>
						  
							<div class="col-lg-8">
								<select id='visit_type_id' name='visit_type_id' class='form-control' >
			                      <option value=''>None - Please Select a service</option>
			                      <?php
																
									if($visit_types_rs->num_rows() > 0){

										foreach($visit_types_rs->result() as $row):
											$visit_type_name = $row->visit_type_name;
											$visit_type_id = $row->visit_type_id;

											if($visit_type_id == $patient_type_id)
											{
												echo "<option value='".$visit_type_id."' selected='selected'>".$visit_type_name."</option>";
											}
											
											else
											{
												echo "<option value='".$visit_type_id."'>".$visit_type_name."</option>";
											}
										endforeach;
									}
								?>
			                      
			                    </select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-4 control-label">Visit date: </label>
							
							<div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" placeholder="Visit Date" value="<?php echo date('Y-m-d');?>">
                                </div>
							</div>
						</div>

						 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                      	 <input type="hidden" name="visit_id_visit" id="visit_id_visit">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Change Type</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
