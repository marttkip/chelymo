<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
// $patient_id = $patient['patient_id'];
$patient_insurance_number = $patient['patient_insurance_number'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

//doctor

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));

// var_dump($served_by);die();
$result = '';
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	
	$result .= 
	'
		<table class="table table-hover table-bordered ">
		  <thead>
			<tr>
			  <th style="text-align:center" rowspan=2>Invoice Date</th>
			  <th rowspan=2>Invoice Number</th>
			  <th rowspan=2>RX</th>
			  <th colspan=2 style="text-align:center;">Amount</th>
			
			</tr>
			<tr>
			  
			  <th style="text-align:center">Invoice</th>
			  <th style="text-align:center">Payment</th>
			  <th style="text-align:center">Total Balance</th>
			</tr>
		  </thead>
		  <tbody>
	';
	
	
	// $personnel_query = $this->personnel_model->get_all_personnel();
	$total_invoiced_amount = 0;
	$total_paid_amount = 0;
	$total_balance = 0;

	// var_dump($query->result());die();
	foreach ($query->result() as $row)
	{
		$visit_invoice_id = $row->visit_invoice_id;
		$visit_date = $row->created;
		$visit_id = $row->visit_id;
		$total_invoice = 0;//$this->accounts_model->total_invoice($visit_id);
		$total_payments = 0;//$this->accounts_model->total_payments($visit_id);

		// $total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
		$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
		// var_dump($visit_invoice_id);die();
		
		$invoice_number = $row->visit_invoice_number; //$visit_type_preffix.'-'.$month.'/'.$year.'-'.sprintf('%03d', $visit_id);
		


		$payments_rs = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id,1);

        $payments_made = '';
        if($payments_rs->num_rows() > 0)
        {
            foreach ($payments_rs->result() as $value => $key_items):
     
                $payment_type = $key_items->payment_type;
                 $payment_status = $key_items->payment_status;
                // if($payment_type == 1 && $payment_status == 1)
                // {
                    $payment_method = $key_items->payment_method;
                    $amount_paid = $key_items->total_amount;
                    $payment_created = $key_items->payment_date;
                    
                    $total_payments += $amount_paid;
                    $payments_made .='<tr>
										<td>'.$payment_created.'</td>
										<td>'.$payment_method.'</td>
										<td>'.number_format($amount_paid).'</td>
									</tr>';

									// var_dump($payments_made);die();
                // }


            endforeach;
            // var_dump($payments_rs->result());die();
        }
        else
        {
        	 $payments_made .='<tr>
									<td colspan=2>No Payments Done</td>
								</tr>';
        }




		$item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items($visit_id,$visit_invoice_id);
		$charged_services = '<table class="table">
							  <thead>
								<tr>
								  <th >Name</th>
								  <th >Units</th>
								  <th >Charge</th>
								  <th >Total</th>										
								</tr>
							  <tbody>';

		if(count($item_invoiced_rs) > 0){
			$s=0;
			$total_nhif_days = 0;
			$total = 0;
			
			foreach ($item_invoiced_rs as $key_items):
				$service_charge_id = $key_items->service_charge_id;
				$service_charge_name = $key_items->service_charge_name;
				$visit_charge_amount = $key_items->visit_charge_amount;
				$service_name = $key_items->service_name;
				$units = $key_items->visit_charge_units;
				$service_id = $key_items->service_id;
				$personnel_id = $key_items->personnel_id;
				$total += $units*$visit_charge_amount;

				

				$charged_services .=  '<tr>
											<td>'.$service_charge_name.'</td>
											<td>'.$units.'</td>
											<td>'.$visit_charge_amount.'</td>
											<td> '.number_format($units*$visit_charge_amount,2).'</td>
										</tr>';
				
			endforeach;
			$total -= $credit_note;
			$total_invoice += $total;

			$charged_services .=  '<tr>
											<th colspan=3>CREDIT NOTE</th>
											<th>('.number_format($credit_note,2).')</th>
										</tr>';
			$charged_services .=  '<tr>
											<th colspan=3>TOTAL INVOICE</th>
											<th> '.number_format($total,2).'</th>
										</tr>';
		}
		$charged_services .= '</tbody>
							</table>
							<p><strong>PAYMENTS</strong><p>';
		

		$charged_services .= '
							
							<table class="table">
							  <thead>
								<tr>
								  <th >Date</th>
								  <th >Method</th>
								  <th >Amount</th>										
								</tr>
								</thead>
							  <tbody>
							  	'.$payments_made.'
								</tbody>
							</table>';


		$count++;

		$total_paid_amount += $total_payments;
		$total_invoiced_amount += $total_invoice;
		if($total_invoice > 0)
		{
			$balance = $total_invoice - $total_payments;
					$total_balance += $balance;


			$result .= 
			'
				<tr>
					<td style="text-align:center">'.$visit_date.'</td>
					<td>'.$invoice_number.'</td>
					<td>'.$charged_services.'</td>
					<td style="text-align:center">'.number_format($total_invoice,2).'</td>
					<td style="text-align:center">'.number_format($total_payments,2).'</td>
					<td style="text-align:center">'.number_format($balance,2).'</td>
				</tr> 
			';
		}

		
		
	}
		$result .= 
			'
				<tr>
					<td></td>
					<td></td>
					<td style="text-align:center">Totals</td>
					<td style="text-align:center; font-weight:bold;"> '.number_format($total_invoiced_amount,2).'</td>
					<td style="text-align:center; font-weight:bold;">'.number_format($total_paid_amount,2).'</td>
					<td style="text-align:center; font-weight:bold;">'.number_format($total_balance,2).'</td>
				</tr> 
			';
		$Balance =  $total_invoiced_amount -$total_paid_amount;
			$result .= 
			'
				<tr>
					<td></td>
					<td></td>
					<td style="text-align:center; font-weight:bold;">Balance</td>
					<td colspan="3" style="text-align:center; font-weight:bold;">'.number_format($Balance,2).'</td>
				</tr> 
			';
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no items";
}



                   
?>

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url().'print-patient-statement/'.$patient_id;?>" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px;" target="_blank"> Print Statement</a>
            </header>
            
            <div class="panel-body">
                <div class="well well-sm info">
                    <h5 style="margin:0;">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>First name:</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $patient_surname;?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Other names:</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $patient_othernames;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </h5>
                </div>
			<?php
				echo $result;
			?>
          	</div>
          
          	
		</section>
    </div>
  </div>