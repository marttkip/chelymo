<table class="table table-bordered table-bordered col-md-12">			
	<tbody>
		<?php
		// var_dump()

		// $count_visit = $visit_list->num_rows();
		$num_pages = $total_rows/$per_page;

		if($num_pages < 1)
		{
			$num_pages = 0;
		}
		$num_pages = round($num_pages);

		if($page==0)
		{
			$counted = 0;
		}
		else if($page > 0)
		{
			$counted = $per_page*$page;
		}

		if($visit_list->num_rows() > 0)
		{
			foreach ($visit_list->result() as $key => $value) {
				# code...
				$visit_idd = $value->visit_id;
				$inpatient_visit = $value->inpatient;
				$visit_date_date = $value->visit_date;
				$open_status = $value->open_status;
				$visit_invoice_id = $value->visit_invoice_id;

				if($inpatient_visit == 1)
				{
					$visit_type = 'Admission Visit';
				}
				else
				{
					$visit_type = 'Clinic Visit';
				}
				if($open_status == 0)
				{
					$open_status_name = 'Open';
					$class = 'warning';
				}
				else
				{
					$open_status_name = 'Closed';
					$class = 'success';
				}

				$total_invoice_amount = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
				$total_payments = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);

				$balance = $total_invoice_amount - ($total_payments+$credit_note);

				$counted++;
				echo "<tr onclick='get_visit_detail(".$visit_idd.",".$visit_invoice_id.")'>
						<td>".$counted."</td>
						<td>".$visit_date_date."</td>
						<td class='".$class."'>".$open_status_name."</td>
						<td>".$visit_type."</td>
						<td>".number_format($balance,2)."</td>
					</tr>";
			}
		}
		?>
	</tbody>
</table>

