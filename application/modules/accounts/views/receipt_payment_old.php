<?php
$patient = $this->reception_model->patient_names2($patient_id);
$account_balance = $patient['account_balance'];

if($close_page == 2)
{
	$color = 'panel-warning';
}
else
{
	$color = '';
}
// $rs_rejection = $this->dental_model->get_rejection_info($visit_id);
// $rejected_amount = 0;
// $rejected_reason ='';
// $close_card = 0;
// $payment_info = '';
// if(count($rs_rejection) >0){
//   foreach ($rs_rejection as $r2):
//     # code...
//     $rejected_amount = $r2->rejected_amount;
//     $rejected_date = $r2->rejected_date;
//     $rejected_reason = $r2->rejected_reason;
//     $visit_type_id = $visit_type = $r2->visit_type;
//     $close_card = $r2->close_card;
//     $invoice_number = $r2->invoice_number;
//     $parent_visit = $r2->parent_visit;
//     $payment_info = $r2->payment_info;
//     $patient_id = $r2->patient_id;

//     // get the visit charge

//   endforeach;
// }
// echo $parent_visit; die();

// var_dump($invoice_number); die();

// $rs_rejection_rs = $this->dental_model->get_visit_rejected_updates($visit_id);

// $rejection = '<table class="table table-hover table-bordered col-md-12">
// 				<thead>
// 					<tr>
// 						<th>Visit Type</th>
// 						<th>Amount</th>
// 						<th colspan="1"></th>
// 					</tr>
// 				</thead>
// 				<tbody>';
// $total_rejected = 0;
// if(count($rs_rejection_rs) >0){
//   foreach ($rs_rejection_rs as $r3):
//     # code...
//     $visit_type_name2 = $r3->visit_type_name;
//     $visit_id_other = $r3->visit_id;
//     $invoice_number = $r3->invoice_number;
//     $visit_bill_amount = $r3->visit_bill_amount;
//     $total_rejected += $visit_bill_amount;

//     // get the visit charge
//     	$rejection .= '<tr>
//     						<td>'.$visit_type_name2.'</td>
//     						<td>'.number_format($visit_bill_amount,2).'</td>

//     						<td><a class="btn btn-danger btn-sm fa fa-trash" href="'.site_url().'accounts/remove_invoice/'.$visit_id.'/'.$invoice_number.'" onclick="return confirm(\' Do you want to remove this invoice\')"> </a></td>
//     					</tr>';

//   endforeach;
// }

// $rejection .='</tbody>
// 			</table>';

$rejected_amount += $total_rejected;

// var_dump($visit_id);die();
if(!empty($visit_id))
{
	$rs_pa = $this->nurse_model->get_prescription_notes_visit($visit_id);
	$visit_prescription = count($rs_pa);

	// $rs_pa = $this->nurse_model->get_sick_leave_notes_visit($visit_id);
	$visit_sick_leave =0;// count($rs_pa);
	// var_dump($visit_prescription);die();


}
else
{
	$visit_prescription =0;
	$visit_sick_leave =0;
}



?>




<div class="row">


 <section class="panel <?php echo $color?>">
	<header class="panel-heading">
		<div class="panel-title">
		<strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Visit: </strong><?php echo $visit_type_name;?>.

		<a href="<?php echo site_url();?>patient-statement/<?php echo $patient_id?>" class="btn btn-warning btn-sm " target="_blank" style="margin-top:0px"><i class="fa fa-print"></i> Statement </a>


		</div>
		<input type="hidden" name="close_page" id="close_page" value="<?php echo $close_page;?>">
		<div class="pull-right">
			<?php
			if($close_page == 2)
			{
				?>
				<a href="<?php echo site_url();?>accounts/patients-accounts" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to patients accounts</a>
				<?php
			}
			else {
			?>
				<a href="<?php echo site_url();?>queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Queue</a>
			<?php 
				}
			?>
		</div>
	</header>

	<!-- Widget content -->

	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				// $search = $this->session->userdata('patient_search');

				// if(!empty($search))
				// {
				// 	echo '
				// 	<a href="'.site_url().'reception/close_patient_search" class="btn btn-warning btn-sm ">Close Search</a>
				// 	';
				// }

				if($visit_prescription > 0)
				{
					echo '<a href="'.site_url().'print-prescription/'.$visit_id.'" target="_blank" class="btn btn-warning btn-sm ">Print Prescription</a>';
				}

				if($visit_sick_leave > 0)
				{
					echo '<a href="'.site_url().'print-sick-off/'.$visit_id.'" target="_blank" class="btn btn-info btn-sm " style="margin-left:10px;">Print Sick Leave</a>';
				}

			 ?>
			</div>
		</div>
		<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id?>">
		<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
		
		<div class="row " style="margin-bottom: 20px">
			<div class="col-md-12">
				<?php 
					// echo '<div class="alert alert-info">
					// 	Note:<br>
					// 	1. Only click on the <a  class="btn btn-xs btn-success" ><i class="fa fa-plus"></i> Add Payment </a> Button only if you have received money from a client. <br>
					// 	2. Kindly always click on <a  class="btn btn-danger btn-xs  " ><i class="fa fa-folder"></i> Close this visit </a> to mark the visit as complete everytime a patient leaves the clinic.
					// 	</div>';

				?>
			</div>
			
			<div class="pull-right" style="padding-right: 10px">
				<?php
				$personnel_id = $this->session->userdata('personnel_id');
				if(!empty($visit_id))
				{
					?>
					 <a  class="btn btn-sm btn-primary" onclick="add_invoice(<?php echo $patient_id;?>,<?php echo $visit_id;?>)"><i class="fa fa-plus"></i> Add invoice </a>
					<?php
				}
				?>
				<a  class="btn btn-sm btn-info" onclick="add_credit_note(<?php echo $patient_id;?>,<?php echo $visit_id;?>)"><i class="fa fa-plus"></i> Add Credit Note </a>
				<!-- <a href="" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Add Credit Note </a> -->
				<a  class="btn btn-sm btn-success" onclick="add_payment(<?php echo $patient_id;?>)"><i class="fa fa-plus"></i> Add Payment </a>
				<a href="<?php echo site_url().'print-patient-statement/'.$patient_id?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Statement</a>

				 <a href="<?php echo site_url();?>accounts/send_notification/<?php echo $visit_id?>/<?php echo $patient_id?>" class="btn btn-default btn-sm  " onclick="return confirm('Do you want to send this notification ?')" ><i class="fa fa-folder"></i> Send notification </a>
				<?php
				if(!empty($visit_id))
				{
					?>
					 <a href="<?php echo site_url();?>accounts/end_visit/<?php echo $visit_id?>" class="btn btn-danger btn-sm  " onclick="return confirm('Do you want to close this visit ?')" ><i class="fa fa-folder"></i> Close this visit </a>
					<?php
				}
				?>
				 
			</div>
		</div>
		<h4><i class="fa fa-star"></i> Incomplete Invoices</h4>
		<div id="incomplete-invoices"></div>
		<h4><i class="fa fa-money"></i> Patient Statement</h4>
		<div id="patient-statement"></div>


	</div>
</section>
</div>
  <!-- END OF ROW -->
<script type="text/javascript">


   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   		// display_procedure(<?php echo $visit_id;?>);
   		get_patient_statement(<?php echo $patient_id;?>);
   		get_patient_incomplete_invoices(<?php echo $patient_id;?>);
   });



  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");

        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }

  }
  function check_payment_type(payment_type_id){


    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check

      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;

      if (window.XMLHttpRequest) {

          XMLHttpRequestObject = new XMLHttpRequest();
      }

      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      var config_url = document.getElementById("config_url").value;
      var close_page = document.getElementById("close_page").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id+"/1";
      // alert(url);
      if(XMLHttpRequestObject) {

          XMLHttpRequestObject.open("GET", url);

          XMLHttpRequestObject.onreadystatechange = function(){

              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }

          XMLHttpRequestObject.send(null);
      }


  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id=null,visit_invoice_id=null){
		// alert(id);
	    var units = document.getElementById('units'+id).value;
	    var billed_amount = document.getElementById('billed_amount'+id).value;

	    grand_total(id, units, billed_amount, v_id,visit_invoice_id);

	}

  function grand_total(procedure_id, units, amount, v_id=null,visit_invoice_id=null)
  {

		 var config_url = document.getElementById("config_url").value;
		 var patient_id = document.getElementById('charge_patient_id').value;
	     var data_url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;

	     var visit_comment = document.getElementById('visit_comment'+procedure_id).value;

	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id,notes: null,visit_comment: visit_comment,patient_id: patient_id,amount: amount},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         // display_patient_bill(v_id);

	         alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        // display_billing(v_id);
	    alert(error);
	    }

	    });

	    var patient_id = document.getElementById("patient_id").value;
		var visit_id = document.getElementById("visit_id").value;
		get_visit_charges(visit_id,patient_id,visit_invoice_id);


	   //  var XMLHttpRequestObject = false;

	   //  if (window.XMLHttpRequest) {

	   //      XMLHttpRequestObject = new XMLHttpRequest();
	   //  }

	   //  else if (window.ActiveXObject) {
	   //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	   //  }
	   //  var config_url = document.getElementById("config_url").value;

	   //  var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   //  // alert(url);
	   //  if(XMLHttpRequestObject) {

	   //      XMLHttpRequestObject.open("GET", url);

	   //      XMLHttpRequestObject.onreadystatechange = function(){

	   //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
				// {
	   //  			// display_patient_bill(v_id);
	   //  			display_billing(v_id);
	   //          }
	   //      }

	   //      XMLHttpRequestObject.send(null);
	   //  }
	}


	function delete_service(id, visit_id){

		var res = confirm('Do you want to remove this charge ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;

		    if (window.XMLHttpRequest) {

		        XMLHttpRequestObject = new XMLHttpRequest();
		    }

		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"accounts/delete_service_billed/"+id;

		    if(XMLHttpRequestObject) {

		        XMLHttpRequestObject.open("GET", url);

		        XMLHttpRequestObject.onreadystatechange = function(){

		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                // display_patient_bill(visit_id);
		                display_procedure(visit_id);
		            }
		        }

		        XMLHttpRequestObject.send(null);
		    }
		}

	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}



	function parse_procedures(visit_id,suck)
    {
      var procedure_id = document.getElementById("service_id_item").value;
       procedures(procedure_id, visit_id, suck);

    }

	function procedures(id, v_id, suck){

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {

            XMLHttpRequestObject = new XMLHttpRequest();
        }

        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var url = "<?php echo site_url();?>accounts/accounts_update_bill/"+id+"/"+v_id+"/"+suck;

         if(XMLHttpRequestObject) {

            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function(){

                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    // document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                    display_patient_bill(v_id);
                }
            }

            XMLHttpRequestObject.send(null);
        }

    }
    function display_procedure(visit_id){

	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    // var url = config_url+"nurse/view_procedure/"+visit_id;
	    var url = config_url+"accounts/view_procedure/"+visit_id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_procedure(id, visit_id,visit_invoice_id=null){
	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_procedure/"+id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {


	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
      // display_patient_bill(visit_id);
      var patient_id = document.getElementById("patient_id").value;
      var visit_id = document.getElementById("visit_id").value;
      get_visit_charges(visit_id,patient_id,visit_invoice_id);
	}

	$(document).on("change","select#visit_type_id",function(e)
	{
		var visit_type_id = $(this).val();

		if(visit_type_id != '1')
		{
			$('#insured_company').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#insured_company').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}




	});

	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"accounts/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         display_patient_bill(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        display_patient_bill(v_id);
		    	alert(error);
		    }

		    });

		}

	}


	function get_patient_statement(patient_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"accounts/get_patient_statement/"+patient_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#patient-statement").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}

	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	function add_invoice(patient_id,visit_id=null)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 
		var visit_id = $('#visit_id').val();
		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_invoices/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			// alert(data);
				
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}


	function get_visit_invoices(patient_id)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_invoices/"+visit_id+"/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			document.getElementById("visit-invoice-div").style.display = "block"; 
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			$('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_procedures_done(patient_id,visit_id,visit_type_id,visit_invoice_id =null)
	{
		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;
		var data_url = config_url+"accounts/search_procedures/"+patient_id+"/"+visit_id+"/"+visit_type_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		var lab_test = $('#search_procedures').val();
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#searched-procedures").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function get_visit_charges(visit_id=null,patient_id,visit_invoice_id = null)
	{
		var config_url = $('#config_url').val();

		if(visit_id > 0)
		{

		}
		else
		{
			visit_id =null;
		}
		var data_url = config_url+"accounts/get_visit_charges/"+patient_id+"/"+visit_id+"/"+visit_invoice_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#visit-charges").html(data);

			$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function add_payment(patient_id,visit_invoice_id=null)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/add_payment/"+patient_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			// alert(data);
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			get_visit_payments(patient_id,null);

			// if(visit_invoice_id > 0)
			// {
				get_visit_invoice_payments(patient_id,visit_invoice_id);
			// }
			// alert(data);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
		
	}


	function get_visit_payments(patient_id,payment_id=null)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_payments/"+patient_id+"/"+payment_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_visit_invoice_payments(patient_id,visit_invoice_id=null)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_visit_invoice_payments/"+patient_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-invoice-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function add_payment_item(patient_id,payment_id= null)
	{
		var config_url = $('#config_url').val();

		 var amount_paid = document.getElementById("amount_paid").value;
		 var invoice_id = document.getElementById("invoice_id").value;

		 // window.alert(amount_paid);
		var data_url = config_url+"accounts/add_payment_item/"+patient_id+"/"+payment_id;
		
		// window.alert(invoice_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{invoice_detail: invoice_id,amount: amount_paid},
		dataType: 'text',
		success:function(data){
		
			get_visit_payments(patient_id,payment_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	$(document).on("submit","form#confirm-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		// var visit_id = $('#charge_visit_id').val();
		var patient_id = $('#payment_patient_id').val();
		var payment_id = $('#payment_payment_id').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/confirm_payment/"+patient_id+"/"+payment_id;
		// alert(url);
		
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function get_patient_incomplete_invoices(patient_id)
	{

		var config_url = $('#config_url').val();
		var visit_id = $('#visit_id').val();
	 	var url = config_url+"accounts/get_incomplete_invoices/"+patient_id+"/"+visit_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#incomplete-invoices").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}



$(document).on("submit","form#confirm-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var visit_id = $('#charge_visit_id').val();
	var patient_id = $('#charge_patient_id').val();
	var visit_invoice_id = $('#visit_invoice_id').val();
	var config_url = $('#config_url').val();	

	var url = config_url+"accounts/confirm_visit_charge/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	 
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    
      	if(data.message == "success")
		{
			
			
			close_side_bar();
			get_patient_incomplete_invoices(patient_id);
			get_patient_statement(patient_id);
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});

function invoice_details_view(visit_invoice_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_invoices/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_charges(visit_id,patient_id,visit_invoice_id);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}


function payments_details_view(payment_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_payments/"+patient_id+"/"+payment_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_payments(patient_id,payment_id);

		$('.datepicker').datepicker({
			    format: 'yyyy-mm-dd'
			});
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function delete_payment(payment_item_id,patient_id,payment_id)
{
	var res = confirm('Are you sure you want to delete this payment item ?');

	if(res)
	{
		var XMLHttpRequestObject = false;
	                
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/delete_payment_item/"+payment_item_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	            	get_visit_payments(patient_id,payment_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	 }
}


$(document).on("change","select#visit_type_id",function(e)
{
	var visit_type_id = $(this).val();
	// alert(visit_type_id);
	if(visit_type_id != '1')
	{
		$('#insured_company').css('display', 'block');
		$('#insured_company2').css('display', 'block');
		// $('#consultation').css('display', 'block');
	}
	else
	{
		$('#insured_company').css('display', 'none');
		$('#insured_company2').css('display', 'none');
		// $('#consultation').css('display', 'block');
	}
	
	

});


function add_service_charge_test(service_charge_id,visit_id,patient_id,visit_type_id=null,visit_invoice_id =null)
{
	var res = confirm('Are you sure you want to charge ?');

	if(res)
	{	

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/add_visit_charge/"+service_charge_id+"/"+visit_id+"/"+patient_id+"/"+visit_type_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_invoice: visit_invoice_id},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById('search_procedures').value = '';
			$('#charges-div').css('display', 'none');

			if(visit_type_id == null)
			{
				get_visit_procedures_done(visit_id);
			}
			else
			{
				get_visit_charges(visit_id,patient_id,visit_invoice_id);
			}
			
			
			// $('#bottom-div').css('display', 'block');
			// tinymce.init({
   //              selector: ".cleditor",
   //             	height: "150"
	  //           });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

}

function delete_invoice(visit_invoice_id,patient_id)
{
	var res = confirm('Are you sure you want to delete this invoice ?');

	if(res)
	{	
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/delete_invoice/"+visit_invoice_id+"/"+patient_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{visit_invoice_id: visit_invoice_id},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	}

}
	

function delete_payment_receipt(payment_id,patient_id)
{
	var res = confirm('Are you sure you want to delete this payment ?');

	if(res)
	{	
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/delete_payment/"+payment_id+"/"+patient_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{payment_id: payment_id},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    	// alert(data);
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				// get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert('Sorry could not delete this payment please try again');
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	}

}

function update_authorised_amount()
{
	var preauth_amount = $('#preauth_amount').val();
	var total_invoice = $('#amount').val();

	if(preauth_amount > total_invoice)
	{
		$('#notification-div').css('display', 'block');
		$('#note-div').css('display', 'none');

		$("#message_warning").html('<div class="alert alert-danger">Kindly note that the amount you entered cannot be more than the total invoice amount.</div>');
		

	}

	else if(preauth_amount == total_invoice)
	{
		$('#notification-div').css('display', 'none');
		$('#note-div').css('display', 'none');
		
	}
	else
	{
		var balance = total_invoice - preauth_amount;
		$('#notification-div').css('display', 'block');
		$('#note-div').css('display', 'block');
		$("#message_warning").html('<div class="alert alert-info">Please note that this invoice will be credited with an amount of '+balance+' and on completing this invoice another Cash invoice of the same amount will be created to cover for difference between the Invoiced amount and insurance authorised amount.</div>');
	}

}




// credit note 



// credit NOTE


function add_credit_note(patient_id,visit_id=null,visit_invoice_id=null)
{
	// alert(visit_id);
	document.getElementById("sidebar-right").style.display = "block"; 
	// document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_credit_notes/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_credit_note(visit_id,patient_id,visit_invoice_id);

		get_all_credit_notes(patient_id,visit_invoice_id);

		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});
}

function get_procedures_credit_note(patient_id,visit_id=null,visit_type_id,visit_invoice_id =null)
{
	var config_url = $('#config_url').val();
	// var visit_type_id = document.getElementById("visit_type_id").value;
	var data_url = config_url+"accounts/search_procedures/"+patient_id+"/"+visit_id+"/"+visit_type_id+"/"+visit_invoice_id+"/1";
	// window.alert(data_url);
	$('#charges-div').css('display', 'block');
	var lab_test = $('#search_procedures').val();
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : lab_test},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
	$("#searched-procedures").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function add_credit_note_charge(service_charge_id,visit_id,patient_id,visit_type_id=null,visit_credit_note_id =null)
{
	var res = confirm('Are you sure you want to this credit note item ?');

	if(res)
	{	

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/add_credit_note_charge/"+service_charge_id+"/"+visit_id+"/"+patient_id+"/"+visit_type_id+"/"+visit_credit_note_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_credit_note_id: visit_credit_note_id},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById('search_procedures').value = '';
			$('#charges-div').css('display', 'none');

			// if(visit_type_id == null)
			// {
			// 	get_visit_procedures_done(visit_id);
			// }
			// else
			// {
				get_visit_credit_note(visit_id,patient_id,visit_credit_note_id);
			// }
			

			
			// $('#bottom-div').css('display', 'block');
			// tinymce.init({
   //              selector: ".cleditor",
   //             	height: "150"
	  //           });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

}


function get_visit_credit_note(visit_id=null,patient_id,visit_invoice_id=null,visit_credit_note_id = null)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_visit_credit_note/"+visit_id+"/"+patient_id+"/"+visit_invoice_id+"/"+visit_credit_note_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#visit-charges").html(data);

		
		$('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}



$(document).on("submit","form#confirm-credit-note",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var visit_id = $('#charge_visit_id').val();
	var patient_id = $('#charge_patient_id').val();
	var config_url = $('#config_url').val();	

	var url = config_url+"accounts/confirm_credit_note/"+visit_id+"/"+patient_id;
	 // alert(patient_id);
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    
      	if(data.message == "success")
		{
			
			
			close_side_bar();
			get_patient_statement(patient_id);
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});



function get_all_credit_notes(patient_id,visit_invoice_id=null,visit_credit_note_id = null)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"accounts/get_all_credit_notes/"+patient_id+"/"+visit_invoice_id+"/"+visit_credit_note_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#all-credit-notes").html(data);

		
		$('.datepicker').datepicker({
				    format: 'yyyy-mm-dd'
				});
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}





$(document).on("submit","form#approve-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var patient_id = $('#patient_id').val();
	var visit_id = $('#visit_id').val();
	
	var config_url = $('#config_url').val();	

	var url = config_url+"accounts/approved_invoice/"+visit_id;
	 // alert(patient_id);
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    	// alert(data);
      	if(data.message == "success")
		{
			
			
			close_side_bar();
				get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});


function mark_as_invoice(visit_invoice_id)
{

	var res = confirm('Are you sure your want to mark this proforma invoice as invoice ?');


	if(res)
	{
		// alert(form_data);
		var patient_id = $('#patient_id').val();
		var visit_id = $('#visit_id').val();
		
		var config_url = $('#config_url').val();	

		var url = config_url+"accounts/mark_as_invoice/"+visit_id;
		 // alert(patient_id);
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{visit_invoice_id: visit_invoice_id},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    	// alert(data);
	      	if(data.message == "success")
			{
				
				close_side_bar();
				get_patient_incomplete_invoices(patient_id);
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
	}
}

	



</script>
