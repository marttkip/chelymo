<?php
$group_incomplete_rs = $this->accounts_model->get_incomplete_invoices($patient_id,1,null,null,$visit_id);
// var_dump($group_incomplete_rs);die();
$incomplete_result = '';
$date_visit = '';
if($group_incomplete_rs->num_rows() > 0)
{
	foreach ($group_incomplete_rs->result() as $key => $value2) {

		$visit_date = $value2->date;
		// $visit_id = $value2->visit_id;
		$incomplete_rs = $this->accounts_model->get_incomplete_invoices($patient_id,NULL,$visit_date,null,$visit_id);

		$incomplete_result .= '<tr class="primary">
									<td colspan="5">'.date('jS M Y',strtotime($visit_date)).'</td>
								</tr>';
		if($incomplete_rs->num_rows() > 0)
		{
			foreach ($incomplete_rs->result() as $key => $value) {
				# code...
				
				$visit_charge_amount = $value->visit_charge_amount;
				$visit_charge_qty = $value->visit_charge_qty;
				$visit_charge_units = $value->visit_charge_units;
				$visit_charge_id = $value->visit_charge_id;
				$service_charge_name = $value->service_charge_name;

				
				$total_visit_charge = $visit_charge_units*$visit_charge_amount;
				$incomplete_result .= '<tr>
											<td>'.$service_charge_name.'</td>
											<td>'.number_format($visit_charge_units).'</td>
											<td>'.number_format($visit_charge_amount,2).'</td>
											<td>'.number_format($total_visit_charge,2).'</td>
											<td>Unassigned</td>
										</tr>';
				

			}
		}
		$incomplete_result .= '<tr class="warning">
									<td colspan="5"></td>
								</tr>';
	}
}

?>
<h4>Unprocessed Bills</h4>
<table class="table table-bordered table-condensed">
	<thead>
		<th>Service Charge</th>
		<th>Units</th>
		<th>Amount</th>
		<th>Total</th>
		<th>Status</th>
	</thead>
	<tbody>
		<?php echo $incomplete_result;?>
	</tbody>
</table>

<?php
// $group_incomplete_rs = $this->accounts_model->get_incomplete_invoices($patient_id,1,null,null,null,$visit_id,1);

$completed = '';
$date_visit = '';
$complete_rs = $this->accounts_model->get_incomplete_invoices($patient_id,NULL,null,null,$visit_id,1);


if($complete_rs->num_rows() > 0)
{
	foreach ($complete_rs->result() as $key => $value) {
		# code...
		
		$visit_charge_amount = $value->visit_charge_amount;
		$visit_charge_qty = $value->visit_charge_qty;
		$visit_charge_units = $value->visit_charge_units;
		$visit_charge_id = $value->visit_charge_id;
		$service_charge_name = $value->service_charge_name;
		$visit_invoice_number = $value->visit_invoice_number;

		
		$total_visit_charge = $visit_charge_units*$visit_charge_amount;
		$completed .= '<tr>
									<td>'.$service_charge_name.'</td>
									<td>'.number_format($visit_charge_units).'</td>
									<td>'.number_format($visit_charge_amount,2).'</td>
									<td>'.number_format($total_visit_charge,2).'</td>
									<td>'.$visit_invoice_number.'</td>
								</tr>';
		

	}
}
$completed .= '<tr class="warning">
							<td colspan="5"></td>
						</tr>';

?>
<h4>Processed Bills</h4>
<table class="table table-bordered table-condensed">
	<thead>
		<th>Service Charge</th>
		<th>Units</th>
		<th>Amount</th>
		<th>Total</th>
		<th>Invoice</th>
	</thead>
	<tbody>
		<?php echo $completed;?>
	</tbody>
</table>