<?php
		
		$result = '';
		
		$total_unprocessed_bill = 0;
		$total_processed_bill = 0;
		$total_visit_balance = 0;
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed table-linked" id="PATIENTS VISITS">
				<thead>
					<tr>
						<th>#</th>
						<th>PATIENT NAME</th>
						<th>VISIT DATE</th>
                        <th>VISIT TYPE</th>
                        <th>VISIT STATUS</th>
                        <th>UNPROCESSED BILL</th>
                        <th>PROCESSED BILL</th>
                        <th>VISIT BALANCE</th>
					</tr>
				</thead>
				 <tbody>
				  
			';
			
			
			
			foreach ($query->result() as $key)
			{

				 

				    $patient_name = $key->patient_name;
				    $patient_id = $key->patient_id;
	                $invoice_number = $key->invoice_number;
	                $invoice_id = $key->invoice_id;
	                $invoice_date = $key->invoice_date;
	                $visit_type_name = $key->visit_type_name;
	                $invoice_status = $key->invoice_status;
	                $unprocessed_bill = $key->unprocessed_bill;
	                $processed_bill = $key->processed_bill;
	                $visit_balance = $key->visit_balance;
	                $open_status = $key->open_status;
	                $visit_type_name = $key->visit_type_name;
	                $visit_id = $key->visit_id;
	                $visit_closed = $key->visit_closed;
	                $invoice_status = $key->invoice_status;
	                $inpatient = $key->inpatient;
	                $visit_date = $key->visit_date;

	                if($unprocessed_bill > 0)
	                {
	                	$billing_status = 'Processed';
	                	$color_code = 'warning';
	                }
	                else if($unprocessed_bill == 0){
	                	$billing_status = 'Unprocessed';
	                	$color_code = 'default';
	                }
	                else{
	                	$billing_status = '';
	                	$color_code = '';
	                }

	                if($visit_closed == 1)
	                {
	                	$visit_status = 'Closed';
	                }
	               	else
	               	{
	               		$visit_status = 'Open';
	               	}
					

					if($inpatient == 1)
					{
						$type_name = 'I.P';
					}
					else
					{
						$type_name = 'O.P';
					}
				$count++;
				$total_unprocessed_bill += $unprocessed_bill;
				$total_processed_bill += $processed_bill;
				$total_visit_balance += $visit_balance;
				$result .= 
							'

								<tr onclick="view_invoice_details('.$visit_id.','.$patient_id.','.$inpatient.')">
									<td class="'.$color_code.'">'.$count.' </td>
									<td class="'.$color_code.'">'.$patient_name.'</td>
									<td class="'.$color_code.'">'.$visit_date.'</td>
									<td class="'.$color_code.'">'.$visit_type_name.'</td>
									<td class="'.$color_code.'">'.$visit_status.'</td>
									<td>'.number_format($unprocessed_bill,2).'</td>
									<td>'.number_format($processed_bill,2).'</td>
									<td>'.number_format($visit_balance,2).'</td>
								</tr> 
							';
			}


			$result .= 
			'
				</tbody>
				<tfoot>
					
						<th colspan="4"></th>
				  		<th>Total</th>
				  		<th>'.number_format($total_unprocessed_bill,2).'</th>
				  		<th>'.number_format($total_processed_bill,2).'</th>
				  		<th>'.number_format($total_visit_balance,2).'</th>
				  	
				</tfoot>
			  
			</table>
			';
		}
		
		else
		{
			$result .= "There Are No Invoices";
		}

		echo $result;
?>