<?php
		$result = '';

		$query = $this->accounts_model->get_patient_statement($patient_id,1,1);

		// var_dump($query);die();
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
				'
					<h4>Preauth Invoices</h4>
					<table class="table  table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Invoice Date</th>
						  <th>Invoice To</th>
						  <th>Invoice No.</th>
						  <th>Invoice Amount</th>
						  <th>Credit Note.</th>
						  <th>Payments.</th>
						  <th>Balance.</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			// $personnel_query = $this->accounting_model->get_all_personnel();
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$visit_type_name = $row->visit_type_name;
				$preauth_status = $row->preauth_status;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				
				$total_amount_credited = 0;
				$transaction_id = $row->transaction_id;
				$reference_id = $row->reference_id;
				$transaction_date = $row->transaction_date;
				$count++;
				$invoice_total = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
				$total_amount_credited = $credit_note + $payments_value;
			

				$balance  = $this->accounts_model->balance($total_amount_credited,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;
				$total_credit_notes += $credit_note;

				if($preauth_status == 1)
				{
					$color_code = 'warning';
				}
				else if($preauth_status == 2)
				{
					$color_code = 'success';
				}
				else
				{
					$color_code = '';
				}
				$result .= 
					'
						<tr>
							<td class="'.$color_code.'">'.$count.'</td>
							<td class="'.$color_code.'">'.$visit_date.'</td>
							<td class="'.$color_code.'">'.strtoupper($visit_type_name).'</td>
							<td class="'.$color_code.'">'.$visit_invoice_number.'</td>
							<td><a onclick="invoice_details_view('.$transaction_id.','.$reference_id.','.$patient_id.','.$transaction_date.')">'.number_format($invoice_total,2).'</a></td>
							<td>'.(number_format($credit_note,2)).'</td>
							<td>'.(number_format($payments_value,2)).'</td>
							<td>'.(number_format($balance,2)).'</td>
							<td><a href="'.site_url().'print-invoice/'.$visit_invoice_id.'/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Proforma Invoice</a></td>
						</tr> 
				';
				
			}

			$result .= 
					'
						<tr>
							<td colspan=4> Totals</td>
							<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
							<td><strong>'.number_format($total_credit_notes,2).'</strong></td>
							<td><strong>'.number_format($total_payments,2).'</strong></td>
							<td><strong>'.number_format($total_balance,2).'</strong></td>
						</tr> 
				';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			// $result .= "";
		}



		
		$query = $this->accounts_model->get_patient_statement($patient_id,1,0);
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
				'	
					<h4> Approved Invoices </h4>
					<table class="table  table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Invoice Date</th>
						  <th>Invoice To</th>
						  <th>Invoice No.</th>

						  
				';
				
			$result .= '
						
						  <th>Invoice Amount</th>
						  <th>Credit Note.</th>
						  <th>Payments.</th>
						  <th>Balance.</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			// $personnel_query = $this->accounting_model->get_all_personnel();
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				$parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;
				$visit_type_name = $row->visit_type_name;
				$inpatient = $row->inpatient;

				if(empty($rejected_amount))
				{
					$rejected_amount = 0;
				}
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);

				if($inpatient == "1")
				{
					if(empty($visit_invoice_number))
					{
						$visit_invoice_number = $visit_id;
					}
					$print_button = '<td><a href="'.site_url().'accounts/print_inpatient_invoice_new/'.$visit_id.'/'.$visit_invoice_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Invoice</a></td>';
				}
				else
				{
					$print_button = '<td><a href="'.site_url().'print-invoice/'.$visit_invoice_id.'/'.$visit_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Invoice</a></td>';
				}
				
				$total_amount_credited = 0;
				$transaction_id = $row->transaction_id;
				$reference_id = $row->reference_id;
				$transaction_date = $row->transaction_date;
				$count++;
				$invoice_total = $this->accounts_model->get_visit_invoice_total($visit_invoice_id);
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$credit_note = $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
				$total_amount_credited = $credit_note + $payments_value;
			

				$balance  = $this->accounts_model->balance($total_amount_credited,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;
				$total_credit_notes += $credit_note;
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$visit_date.'</td>
							<td>'.strtoupper($visit_type_name).'</td>
							<td>'.$visit_invoice_number.'</td>
							<td><a onclick="invoice_details_view('.$transaction_id.','.$reference_id.','.$patient_id.','.$transaction_date.')">'.number_format($invoice_total,2).'</a></td>
							<td>(<a onclick="add_credit_note('.$patient_id.','.$visit_id.','.$visit_invoice_id.')">'.(number_format($credit_note,2)).'</a>)</td>
							<td>(<a onclick="add_payment('.$patient_id.','.$visit_invoice_id.')">'.(number_format($payments_value,2)).'</a>)</td>
							<td>'.(number_format($balance,2)).'</td>
							'.$print_button.'
						</tr> 
				';
				
			}

			$result .= 
					'
						<tr>
							<td colspan=4> Totals</td>
							<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
							<td><strong>'.number_format($total_credit_notes,2).'</strong></td>
							<td><strong>'.number_format($total_payments,2).'</strong></td>
							<td><strong>'.number_format($total_balance,2).'</strong></td>
						</tr> 
				';
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visits";
		}
		
		echo $result;
?>