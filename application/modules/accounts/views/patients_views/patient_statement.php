<?php
$transactions = $this->accounts_model->get_patient_statement($patient_id);

$checked = '';
$total_invoice = 0;
$total_payment = 0;
$total_balance = 0;
if($transactions->num_rows() > 0)
{
	foreach ($transactions->result() as $key => $value) {
		# code...
		$transaction_date = $value->transaction_date;
		$transaction_id = $value->transaction_id;
		$transaction_description = $value->transaction_description;
		$transactionCategory = $value->transactionCategory;
		$reference_code = $value->reference_code;
		$dr_amount = $value->dr_amount;
		$cr_amount = $value->cr_amount;
		$reference_id = $value->reference_id;

		$balance = $dr_amount - $cr_amount;

		$total_invoice += $dr_amount;
		$total_payment += $cr_amount;
		$total_balance = $total_invoice - $total_payment;
		// <td><a href="'.site_url().'accounts/send-invoice/'.$transaction_id.'/'.$reference_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-folder"></i></a></td>
		if($transactionCategory == "Revenue")
		{
			$button = '<td><a href="'.site_url().'print-invoice/'.$transaction_id.'/'.$reference_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i></a></td>
						
			';

		}
		else if($transactionCategory == "Revenue Payment")
		{
			$button = '<td><a href="'.site_url().'print-receipt/'.$transaction_id.'/'.$reference_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i></a></td>';
		}

		if($dr_amount > 0 AND $transactionCategory == "Revenue")
		{
			$items_div = '<td><a onclick="invoice_details_view('.$transaction_id.','.$reference_id.','.$patient_id.','.$transaction_date.')">'.number_format($dr_amount,2).'</a></td>';
			$document_type = 'INVOICE';
		}

		else if($cr_amount > 0 AND $transactionCategory == "Revenue Payment")
		{
			$items_div = '<td><a onclick="payments_details_view('.$transaction_id.','.$reference_id.','.$patient_id.','.$transaction_date.')">('.number_format($cr_amount,2).')</a></td>';
			$document_type = 'PAYMENT';
		}
		else
		{
			$items_div = '<td>'.number_format(0,2).'</td>';
			$document_type = '';
		}

		$checked .='<tr>
						<td>'.date('jS M Y',strtotime($transaction_date)).'</td>
						<td>'.$document_type.'</td>
						<td>'.$transaction_description.'</td>
						<td>'.$reference_code.'</td>
						'.$items_div.'			
						<td>'.number_format($total_balance,2).'</td>
						'.$button.'
					</tr>';
	}

	$checked .='<tr>
						<th></th>
						<th></th>
						<th></th>

						<th>PATIENT BALANCE</th>			
						<th colspan="2" class="center-align">'.number_format($total_balance,2).'</th>
						
					</tr>';
}
?>
<div class="row ">
	<div class="col-md-12" style="margin-bottom: 20px;" >
		<table class="table table-bordered table-stripped table-condensed">
			<thead>
				<th style="width: 10%">DATE</th>
				<th style="width: 10%">TYPE</th>
				<th style="width: 40%">DESCRIPTION</th>
				<th style="width: 10%">DOCUMENT NO</th>
				<th style="width: 10%">AMOUNT</th>
				<th style="width: 10%">BALANCE</th>
			</thead>
			<tbody>
				<?php  echo $checked;?>
			</tbody>
		</table>
	</div>
</div>