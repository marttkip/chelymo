<?php
$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
?>
	<div class="tabs">
	    <ul class="nav nav-tabs nav-justified">
	        <li class="active">
	            <a class="text-center" data-toggle="tab" href="#outpatient">Invoiced Items</a>
	        </li>
	        <li>
	            <a class="text-center" data-toggle="tab" href="#inpatient">Payments/Recepts</a>
	        </li>
	    </ul>
	    <div class="tab-content">
	        <div class="tab-pane active" id="outpatient" style="height:70vh;overflow-y:scroll;">    
	        	<div class="row" style="margin-bottom: 5px;">
	        		<div class="col-md-12">
	        			<?php
	        			if($close_card != 2 OR $authorize_invoice_changes == TRUE)
	        			{
		        			?>
		        			
					  		<a class="btn btn-xs btn-warning pull-right"  data-toggle="modal" data-target="#add_to_bill" ><i class="fa fa-plus"></i> Bill Accoount</a>  
					  		<?php

				  			}
				  		?>

				  		<!-- <a class="btn btn-xs btn-warning pull-right"  onclick="add_bill(<?php echo $visit_id;?>)" ><i class="fa fa-plus"></i> Bill Accoount</a>   -->
	        		</div>
	        	</div>  
	        	<div class="row">
	        		<div class="col-md-12">   	    
	        			<?php

					echo "
					<table align='center' class='table table-striped table-hover table-condensed'>
						<tr>
							<th>#</th>
							<th style='width:10%'>Date</th>
							<th style='width:10%'>Time</th>
							<th style='width:10%'>Department</th>
							<th style='width:20%'>Services/Items</th>
							<th style='width:10%'>Units</th>
							<th style='width:15%'>Unit Cost (Ksh)</th>
							<th style='width:10%'> Total</th>
							<th></th>
							<th></th>
						</tr>		
					"; 

					$total= 0; 
					$count = 0;
					

					$num_pages = $total_rows/$per_page;

					if($num_pages < 1)
					{
						$num_pages = 0;
					}
					$num_pages = round($num_pages);

					if($page==0)
					{
						$counted = 0;
					}
					else if($page > 0)
					{
						$counted = $per_page*$page;
					}
					// var_dump($invoice_items->num_rows()); die();
					$sub_total= 0; 
					$personnel_query = $this->personnel_model->retrieve_personnel();
						
					if($invoice_items->num_rows() >0){
						
						$visit_date_day = '';
						foreach ($invoice_items->result() as $value => $key1):
							$v_procedure_id = $key1->visit_charge_id;
							$procedure_id = $key1->service_charge_id;
							$service_name = $key1->service_name;
							$date = $key1->date;
							$time = $key1->time;
							$visit_charge_timestamp = $key1->visit_charge_timestamp;
							$visit_charge_amount = $key1->visit_charge_amount;
							$units = $key1->visit_charge_units;
							$procedure_name = $key1->service_charge_name;
							$service_id = $key1->service_id;
							$provider_id = $key1->provider_id;
						
							$sub_total= $sub_total +($units * $visit_charge_amount);
							$visit_date = date('l d F Y',strtotime($date));
							$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
							
							if($visit_date_day != $visit_date)
							{
								
								$visit_date_day = $visit_date;
							}
							else
							{
								$visit_date_day = '';
							}

							// echo 'asdadsa'.$visit_date_day;

							if($personnel_query->num_rows() > 0)
							{
								$personnel_result = $personnel_query->result();
								
								foreach($personnel_result as $adm)
								{
									$personnel_id = $adm->personnel_id;
									

									if($personnel_id == $provider_id)
									{
										$provider_id = '[ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';


										$procedure_name = $procedure_name.$provider_id;
									}
									
									
									
								}
							}
							
							else
							{
								$provider_id = '';
							}

							if($close_card != 2 OR $authorize_invoice_changes == TRUE)
							{
								$buttons = "<td>
										<a class='btn btn-xs btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
										</td>
										<td>
											<a class='btn btn-xs btn-danger'  onclick='delete_service(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
										</td>";


							}
							else
							{
								$buttons = "<td colspan='2'> Restricted<td>";
							}




							$counted++;
							echo"
									<tr> 
										<td>".$counted."</td>
										<td>".$date."</td>
										<td >".$visit_time."</td>										
										<td >".$service_name."</td>
										<td >".$procedure_name."</td>
										<td align='center'>
											<input type='text' id='units".$v_procedure_id."' class='form-control' value='".$units."' size='3' />
										</td>
										<td align='center'><input type='text' id='billed_amount".$v_procedure_id."' class='form-control'  size='5' value='".$visit_charge_amount."'></div>
										</td>
										
										<td >".number_format($visit_charge_amount*$units)."</td>
										".$buttons."
									</tr>	
							";

							$visit_date_day = $visit_date;
							endforeach;
							

					}		
					echo"
					 </table>
					";
					?>
					
	        		</div>
	        	</div>
	        </div>
	        <div class="tab-pane" id="inpatient" style="height:70vh;overflow-y:scroll;">
	           <a class="btn btn-xs btn-success pull-right"  data-toggle="modal" data-target="#add_payment_modal" ><i class="fa fa-plus"></i> Add Payment</a>  
	           <a href="<?php echo site_url();?>accounts/print_receipt_all/<?php echo $visit_id;?>/<?php echo $visit_invoice_id;?>" target="_blank" class="btn btn-xs btn-warning pull-left"><i class="fa fa-print"></i> Print Receipt</a>
	           	<div id="payments-made"></div>
	           
	        </div>
	    </div>
	</div>
	