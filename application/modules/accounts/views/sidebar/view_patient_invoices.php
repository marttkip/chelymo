<?php

// var_dump($visit_charge_id);die();
$visit_charge_details = $this->accounts_model->get_visit_charge_details($visit_charge_id);

if($visit_charge_details->num_rows() > 0)
{
	foreach ($visit_charge_details->result() as $key => $value) {
		// code...
		$service_charge_name = $value->service_charge_name;
		$visit_charge_units = $value->visit_charge_units;
		$visit_charge_amount = $value->visit_charge_amount;

		$total_amount_charged = $visit_charge_units*$visit_charge_amount;
	}
}


$transactions = $this->accounts_model->get_patient_invoices_list($patient_id);

$checked = '';
$total_invoice = 0;
$total_payment = 0;
$total_balance = 0;
if($transactions->num_rows() > 0)
{
	foreach ($transactions->result() as $key => $value) {
		# code...
		$transaction_date = $value->created;
		$transaction_id = $value->visit_invoice_id;
		$transaction_description = $value->transaction_description;
		$transactionCategory = $value->transactionCategory;
		$reference_code = $value->visit_invoice_number;
		$dr_amount = $value->invoice_bill;
		$cr_amount = $value->invoice_balance;
;
		
	$total_balance += $cr_amount;
		$checked .='<tr onclick="assign_charge_to_invoice('.$transaction_id.','.$visit_charge_id.','.$patient_id.')">
						<td>'.date('jS M Y',strtotime($transaction_date)).'</td>
						<td>INVOICE '.$visit_charge_id.'</td>
						<td>'.$reference_code.'</td>
						<td>'.number_format($dr_amount,2).'</td>
						<td>'.number_format($cr_amount,2).'</td>
						'.$button.'
					</tr>';
	}

	$checked .='<tr>
						<th></th>
						<th></th>
						<th></th>

						<th>PATIENT BALANCE</th>			
						<th colspan="1" >'.number_format($total_balance,2).'</th>
						
					</tr>';
}
?>


<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="row ">
        	<div class="col-md-12" style="margin-bottom: 20px;" >
        		<h4>Add <?php echo $service_charge_name?> amount <?php echo number_format($total_amount_charged,2)?></h4>
        	</div>
			<div class="col-md-12" style="margin-bottom: 20px;" >
				<table class="table table-bordered table-stripped table-condensed">
					<thead>
						<th style="width: 10%">DATE</th>
						<th style="width: 10%">TYPE</th>
						<th style="width: 40%">INVOICE NUMBER</th>
						<th style="width: 15%">AMOUNT BILLED</th>
						<th style="width: 15%">BALANCE</th>
					</thead>
					<tbody>
						<?php  echo $checked;?>
					</tbody>
				</table>
			</div>
		</div>
    </div>
</section>
<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>
</div>
