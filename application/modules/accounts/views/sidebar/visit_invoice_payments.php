<?php

$payment_item_rs = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id,1,$patient_id);
// var_dump($patient_id);die();
$cancel_actions = $this->accounts_model->get_cancel_actions();

$result = "

			<table  class='table table-striped table-bordered table-condensed'>
			<tr>
				<th></th>
				<th>Transaction Date</th>
				<th>Payment Method</th>
				<th>Receipt Number</th>
				<th>Amount Paid</th>
				<th></th>

			</tr>
		";

$x = 0;
$total_payment = 0;
$payment_date = date('Y-m-d');
if($payment_item_rs->num_rows() > 0)
{

	foreach ($payment_item_rs->result() as $key => $value) {
		# code...
		$payment_id = $value->payment_id;
		$payment_item_id = $value->payment_item_id;
		$payment_method = $value->payment_method;
		$payment_method_idd = $value->payment_method_id;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_id = $value->visit_id;
		$payment_date = $value->payment_date;
		$confirm_number = $value->confirm_number;
		// $patient_id = $value->patient_id;
		$payment_item_amount = $value->total_amount;
		$payment_type = $value->payment_type;

		if($payment_type > 1)
		{
			$method = 'REFUND/CANCELLATION';
		}



		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
		$personnel_id = $this->session->userdata('personnel_id');
		$button ='';
		if($authorize_invoice_changes OR $personnel_id == 0)
		{
			$button = '<td>
									
										<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#refund_payment'.$payment_id.'"><i class="fa fa-times"></i></button>
										<div class="modal fade" id="refund_payment'.$payment_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								    <div class="modal-dialog" role="document">
								        <div class="modal-content">
								            <div class="modal-header">
								            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								            	<h4 class="modal-title" id="myModalLabel">ADD REFUND AMOUNT PAID '.$payment_item_amount.'</h4>
								            </div>
								            <div class="modal-body">
								            	'.form_open("accounts/cancel_payment/".$payment_id."/".$visit_id, array("class" => "form-horizontal","id"=>"payments-paid-form")).'
								                <div class="form-group">
								                    <label class="col-md-4 control-label">Action: </label>
								                    <input type="hidden" name="payment_id" id="payment_id" value="'.$payment_id.'">
								                    <input type="hidden" name="visit_id" id="payment_visit_id" value="'.$visit_id.'">
								                     <input type="hidden" name="patient_id" id="payment_patient_id" value="'.$patient_id.'">
								                    <input type="hidden" name="visit_invoice_id" id="visit_invoice_id" value="'.$visit_invoice_id.'">
								                    <div class="col-md-8">
								                        <select class="form-control" name="cancel_action_id" id="payment_cancel_action_id">
								                        	<option value="">-- Select action --</option>';
								                                if($cancel_actions->num_rows() > 0)
								                                {
								                                    foreach($cancel_actions->result() as $res)
								                                    {
								                                        $cancel_action_id = $res->cancel_action_id;
								                                        $cancel_action_name = $res->cancel_action_name;
								                                        
								                                        $button .= '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
								                                    }
								                                }
								                            $button .='
								                        </select>
								                    </div>
								                </div>

								                <div class="form-group">
								                    <label class="col-md-4 control-label">Amount: </label>
								                    
								                    <div class="col-md-8">
								                        <select id="payment_account_from_id" name="account_from_id" class="form-control" required>
													                <option value="0">--- Account ---</option>';
													                $accounts= $this->purchases_model->get_child_accounts("Bank");
													                if($accounts->num_rows() > 0)
													                {
													                 foreach($accounts->result() as $row):
													                                           
													                   $account_name = $row->account_name;
													                   $account_id = $row->account_id;

													                   $button .= '<option value="'.$account_id.'"> '.$account_name.'</option>';

													                 endforeach;
													               }
													                $button .= '
													            
													             </select>
								                    </div>
								                </div>
								                 <div class="form-group">
								                    <label class="col-md-4 control-label">Amount: </label>
								                    
								                    <div class="col-md-8">
								                        <input type="number" class="form-control" name="amount_refunded" id="payment_amount_refunded" value="" required="required" />
								                    </div>
								                </div>
								                
								                <div class="form-group">
								                    <label class="col-md-4 control-label">Description: </label>
								                    
								                    <div class="col-md-8">
								                        <textarea class="form-control" name="cancel_description" id="payment_cancel_description"></textarea>
								                    </div>
								                </div>
								                <div class="form-group">
					                        <label class="col-md-4 control-label">Cancelled Date: </label>
					                        
					                        <div class="col-md-8">
					                            <div class="input-group">
					                                <span class="input-group-addon">
					                                    <i class="fa fa-calendar"></i>
					                                </span>
					                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="refund_date" id="refund_date" placeholder="Payment Date" autocomplete="off" value="'.date('Y-m-d').'" required>
					                            </div>
					                        </div>
					                    </div>
								                <div class="row">
								                	<div class="col-md-8 col-md-offset-4">
								                    	<div class="center-align">
								                        	<button type="submit" class="btn btn-primary">Save action</button>
								                        </div>
								                    </div>
								                </div>
								               '.form_close().'
								            </div>
								            <div class="modal-footer">
								                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								            </div>
								        </div>
								    </div>
								</div>
									</td>
									<td>
										<a class="btn btn-xs btn-danger" onclick="delete_payment_receipt('.$payment_id.','.$patient_id.')" ><i class="fa fa-trash"></i> Delete</a>
									</td>';
		}
		
		$checked = "<td>
						<a class='btn btn-xs btn-warning' href='".site_url().'print-receipt/'.$payment_id.'/'.$visit_invoice_id."' target='_blank'><i class='fa fa-printer'></i> Print Receipt</a>
					</td>
					".$button;
	
		$x++;

		if($payment_type > 1)
		{
			$result .= '<tr>
						<td >'.$x.'</td>
						<td>'.date('jS M Y',strtotime($payment_date)).'</td>
						<td colspan="2">'.$method.'</td>
						<td>'.number_format($payment_item_amount,2).'</td>
						'.$checked.'
					</tr>';
		}else
		{
			$result .= '<tr>
				<td >'.$x.'</td>
				<td>'.date('jS M Y',strtotime($payment_date)).'</td>
				<td>'.$payment_method.'</td>
				<td>'.$confirm_number.'</td>
				<td>'.number_format($payment_item_amount,2).'</td>
				'.$checked.'
			</tr>';
		}
		
		$total_payment += $payment_item_amount;
	}

	$result .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total</td>
						<td>'.number_format($total_payment,2).'</td>
					</tr>';
}
  $result .='</tbody>
                      </table>';


 

?>




<?php

$payment_item_rs = $this->accounts_model->get_visit_refunds_payments($visit_invoice_id,1,$patient_id);
// var_dump($patient_id);die();
$cancel_actions = $this->accounts_model->get_cancel_actions();

$result .= "
			<h4>Refunds</h4>
			<table  class='table table-striped table-bordered table-condensed'>
			<tr>
				<th></th>
				<th>Transaction Date</th>
				<th>Payment Method</th>
				<th>Receipt Number</th>
				<th>Amount Paid</th>
				<th></th>

			</tr>
		";

$x = 0;
$total_payment = 0;
$payment_date = date('Y-m-d');
if($payment_item_rs->num_rows() > 0)
{

	foreach ($payment_item_rs->result() as $key => $value) {
		# code...
		$payment_id = $value->payment_id;
		$payment_item_id = $value->payment_item_id;
		$payment_method = $value->account_name;
		$payment_method_idd = $value->account_id;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_id = $value->visit_id;
		$payment_date = $value->payment_date;
		$confirm_number = $value->confirm_number;
		// $patient_id = $value->patient_id;
		$payment_item_amount = $value->total_amount;
		$payment_type = $value->payment_type;

		if($payment_type > 1)
		{
			$method = 'REFUND/CANCELLATION';
		}



		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
		$personnel_id = $this->session->userdata('personnel_id');
		$button ='';
		if($authorize_invoice_changes OR $personnel_id == 0)
		{
			$button = '
									<td>
										<a class="btn btn-xs btn-danger" onclick="delete_payment_receipt('.$payment_id.','.$patient_id.')" ><i class="fa fa-trash"></i> Delete</a>
									</td>';
		}
		
		$checked = "<td>
						<a class='btn btn-xs btn-warning' href='".site_url().'print-receipt/'.$payment_id.'/'.$visit_invoice_id."' target='_blank'><i class='fa fa-printer'></i> Print Refund</a>
					</td>
					".$button;
	
		$x++;

		if($payment_type > 1)
		{
			$result .= '<tr>
						<td >'.$x.'</td>
						<td>'.date('jS M Y',strtotime($payment_date)).'</td>
						<td colspan="2">'.$method.'</td>
						<td>'.number_format($payment_item_amount,2).'</td>
						'.$checked.'
					</tr>';
		}else
		{
			$result .= '<tr>
				<td >'.$x.'</td>
				<td>'.date('jS M Y',strtotime($payment_date)).'</td>
				<td>'.$payment_method.'</td>
				<td>'.$confirm_number.'</td>
				<td>'.number_format($payment_item_amount,2).'</td>
				'.$checked.'
			</tr>';
		}
		
		$total_payment += $payment_item_amount;
	}

	$result .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total</td>
						<td>'.number_format($total_payment,2).'</td>
					</tr>';
}
  $result .='</tbody>
                      </table>';


 

?>

<div class="row">
	<div class="col-md-12">
		<section class="panel">
		    <header class="panel-heading">
		            <h5 class="pull-left"><i class="icon-reorder"></i></h5>
		          <div class="widget-icons pull-right">
		              
		          </div>
		          <div class="clearfix"></div>
		    </header>
		    <div class="panel-body">
				<div class="padd">
					<?php echo $result;?>
				</div>
			</div>
		</section>
	</div>
</div>