<?php

$payment_item_rs = $this->accounts_model->get_payment_items($patient_id,$payment_id);
// var_dump($patient_id);die();
echo form_open("finance/creditors/confirm_payment/".$patient_id, array("class" => "form-horizontal","id" => "confirm-payment"));
$result = "

			<table  class='table table-striped table-bordered table-condensed'>
			<tr>
				<th></th>
				<th></th>
				<th>Type</th>
				<th>Invoice Number</th>
				<th>Amount Paid</th>
				<th></th>

			</tr>
		";

$x = 0;
$total_payment = 0;
$payment_date = date('Y-m-d');
if($payment_item_rs->num_rows() > 0)
{

	foreach ($payment_item_rs->result() as $key => $value) {
		# code...
		$payment_id = $value->payment_id;
		$payment_item_id = $value->payment_item_id;
		$payment_method_idd = $value->payment_method_id;
		$visit_invoice_id = $value->visit_invoice_id;
		$visit_id = $value->visit_id;
		$payment_date = $value->payment_date;
		// $patient_id = $value->patient_id;
		$payment_item_amount = $value->payment_item_amount;

		$visit_invoice_number = $value->visit_invoice_number;

		$invoice_type = $value->invoice_type;

		if($invoice_type == 1)
		{
			$type = 'Invoice';
		}
		else if($invoice_type == 3)
		{
			$type = 'On account';
		}
		$checkbox_data = array(
			                    'name'        => 'visit_payment_items[]',
			                    'id'          => 'checkbox'.$payment_item_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $payment_item_id
			                  );

		if($payment_id > 0)
		{
			$checked = '';
		}
		else
		{
			$checked = "<td>
						<a class='btn btn-xs btn-danger' onclick='delete_payment(".$payment_item_id.", ".$patient_id.",".$payment_id.")'><i class='fa fa-trash'></i></a>
					</td>";
		}
		$x++;
		$result .= '<tr>
						<td class="'.$text_color.'">'.form_checkbox($checkbox_data).'<label for="checkbox'.$payment_item_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td>'.$x.'</td>
						<td>'.$invoice_type.'</td>
						<td>'.$visit_invoice_number.'</td>
						<td>'.number_format($payment_item_amount,2).'</td>
						'.$checked.'
					</tr>';
		$total_payment += $payment_item_amount;
	}

	$result .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total</td>
						<td>'.number_format($total_payment,2).'</td>
					</tr>';
}
  $result .='</tbody>
                      </table>';


   if(empty($payment_date))
   {
   	$payment_date = date('Y-m-d');
   }

?>
<div class="row">
	<div class="col-md-12">
		<section class="panel">
		    <header class="panel-heading">
		            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
		          <div class="widget-icons pull-right">
		              
		          </div>
		          <div class="clearfix"></div>
		    </header>
		    <div class="panel-body">
				<div class="padd">
				
						<input type="hidden" name="type_payment" value="1">
					
			            <input type="hidden" name="payment_id" id="payment_payment_id" value="<?php echo $payment_id?>">
			             <input type="hidden" name="patient_id" id="payment_patient_id" value="<?php echo $patient_id;?>">
						
			            <div class="col-md-12">
			            	<?php echo $result;?>
			            </div>
			            <div class="col-md-12">
			            	<div class="col-md-6">
			            	</div>
							<div class="col-md-6">
								<div class="form-group" >
			                        <label class="col-lg-4 control-label">Method: </label>
			                          
			                        <div class="col-lg-8">
			                            <select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required="required">
			                                <option value="">---- select a payment method -------</option>
			                                <?php
			                                  $method_rs = $this->accounts_model->get_payment_methods();
			                                  $num_rows = count($method_rs);
			                                 if($num_rows > 0)
			                                  {
			                                    
			                                    foreach($method_rs as $res)
			                                    {
			                                      $payment_method_id = $res->payment_method_id;
			                                      $payment_method = $res->payment_method;


			                                      if($payment_method_idd == $payment_method_id)
			                                      {
			                                      	echo '<option value="'.$payment_method_id.'" selected>'.$payment_method.'</option>';
			                                      }
			                                      else
			                                      {
			                                      	echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
			                                      }
			                                      
			                                        
			                                      
			                                    }
			                                  }
			                              ?>
			                            </select>
			                          </div>
			                    </div>
			                    <div  class="form-group"  >
			                        <label class="col-lg-4 control-label"> Total Payment: </label>

			                        <div class="col-lg-8">
			                            <input type="text" class="form-control" name="total_amount" value="<?php echo $total_payment?>" readonly>
			                        </div>
			                    </div>
			                    <div id="mpesa_div" class="form-group" style="display:none;" >
			                        <label class="col-lg-4 control-label"> Mpesa TX Code: </label>

			                        <div class="col-lg-8">
			                            <input type="text" class="form-control" name="mpesa_code" placeholder="">
			                        </div>
			                    </div>
			                  
			                    <div id="insuarance_div" class="form-group" style="display:none;" >
			                        <label class="col-lg-4 control-label"> Reference : </label>
			                        <div class="col-lg-8">
			                            <input type="text" class="form-control" name="debit_card_detail" placeholder="">
			                        </div>
			                    </div>
			                  
			                    <div id="cheque_div" class="form-group" style="display:none;" >
			                        <label class="col-lg-4 control-label"> Cheque No.: </label>
			                      
			                        <div class="col-lg-8">
			                            <input type="text" class="form-control" name="cheque_number" placeholder="">
			                        </div>
			                    </div>
			                  
			                   
													<div class="form-group">
			                        <label class="col-md-4 control-label">Payment Date: </label>
			                        
			                        <div class="col-md-8">
			                            <div class="input-group">
			                                <span class="input-group-addon">
			                                    <i class="fa fa-calendar"></i>
			                                </span>
			                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="payment_date" id="payment_date" placeholder="Payment Date" autocomplete="off" value="<?php echo $payment_date;?>" required>
			                            </div>
			                        </div>
			                    </div>
			                    <div class="form-group">
			                    	<div class="center-align">
										<button class="btn btn-info btn-sm" type="submit" onclick="return confirm('Are you sure you want to comfirm this payment ? ')">Confirm Payment</button>
									</div>
			                    </div>
								
							</div>
						</div>

						<?php 
						$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
						$personnel_id = $this->session->userdata('personnel_id');


						if($payment_id > 0 AND ($authorize_invoice_changes OR $personnel_id == 0))
						{

							?>

								<div class="col-md-12">
									<a  class="btn btn-sm btn-danger pull-right" onclick="delete_payment_receipt(<?php echo $payment_id;?>,<?php echo $patient_id;?>)"> <i class="fa fa-trash"></i> DELETE PAYMENT</a>
								</div>
							<?php

						}
						?>
							
						
						
					
				</div>
			</div>
		</section>
	</div>
	<div class="col-md-12">                             
	    <div class="center-align">
	        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	    </div>  
	</div>
</div>
	<?php echo form_close();?>