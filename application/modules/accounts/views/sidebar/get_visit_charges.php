<?php

$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_insurance_number = $patient['patient_insurance_number'];
$account_balance = $patient['account_balance'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];
$visit_status = 0;

// var_dump($inpatient);die();

$visit_invoice_number ='';
$preauth_date = date('Y-m-d');
$created = date('Y-m-d');
$preauth_amount = '';
$bill_to = 0;
$preauth = 0;
$approved_amount = 0;




$visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 0;
$close_card = 3;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$close_card = $value->close_card;
		$visit_type_id = $value->visit_type;
		$visit_time_out = $value->visit_time_out;
		$parent_visit = $value->parent_visit;
		$close_card = $value->close_card;
		$visit_id = $value->visit_id;
		$dentist_id = $value->personnel_id;
		$visit_date = $value->visit_date;
		// $patient_id = $value->patient_id;
		$visit_type = $value->visit_type;
		$member_number = $value->patient_insurance_number;
		$insurance_limit = $value->insurance_limit;
		$insurance_description = $value->insurance_description;
		$visit_type = $value->visit_type;
		$visit_time_out = date('jS F Y',strtotime($visit_time_out));
	}
}


if(!empty($visit_invoice_id))
{
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = $value->preauth_date;
			$created = $value->created;
			$preauth_amount = $value->preauth_amount;
			$scheme_name = $value->scheme_name;
			$member_number = $value->member_number;
			$bill_to = $value->bill_to;
			$insurance_limit = $value->insurance_limit;
			$preauth_status = $value->preauth_status;
			$preauth = $value->preauth;
			$approved_amount = $value->approved_amount;
			

		}
	}
}


if($bill_to > 0)
{
	$visit_type = $bill_to;
}
else
{
	$scheme_name = $insurance_description;
	$member_number = $member_number;
	$insurance_limit = $insurance_limit;
}

if($visit_type == 1)
{
	$display = 'none';
}
else if($visit_type_id > 1)
{
	$display = 'block';
}

if(empty($approved_amount))
{
	$approved_amount = 0;
}

if(empty($visit_invoice_number) AND $inpatient == "1")
{
	$visit_invoice_number = $visit_id;
}

// var_dump($visit_invoice_number);die();
// get all the procedures

// var_dump($visit_invoice_id);die();

// $visit__rs1 = $this->accounts_model->get_visit_charges_charged($visit_id,$visit_invoice_id);
$visit__rs1  = $this->accounts_model->get_incomplete_invoices($patient_id,$visit_id,$visit_date,$visit_invoice_id);
// var_dump($visit_id)	;die();
echo form_open("finance/creditors/confirm_invoice_note/".$visit_id."/".$patient_id, array("class" => "form-horizontal","id" => "confirm-invoice"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th></th>
		<th>Invoice Item</th>
		<th>Description</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>

	</tr>
";
	$total= 0;  
	$number = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->visit_charge_id;
			$procedure_id = $value->service_charge_id;
			$visit_charge_amount = $value->visit_charge_amount;
			$units = $value->visit_charge_units;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$personnel_creator = $value->personnel_creator;
			$visit_invoice_id = $value->visit_invoice_id;
			$visit_comment = $value->visit_charge_comment;
			// $visit_type_id = 1;
			$total= $total +($units * $visit_charge_amount);

			if($visit_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;
			$personnel_id = $this->session->userdata('personnel_id');
			$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

			if($personnel_creator == $personnel_id OR $personnel_id == 0 OR $authorize_invoice_changes == TRUE)
			{
				$personnel_check = TRUE;
			}
			else
			{
				$personnel_check = FALSE;
			}
			
			if($personnel_check OR $personnel_id == 0)
			{
				$checked = "<td>
							<a class='btn btn-sm btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.",".$visit_invoice_id.")'><i class='fa fa-pencil'></i></a>
							</td>
							<td>
								<a class='btn btn-sm btn-danger'  onclick='delete_procedure(".$v_procedure_id.", ".$visit_id.",".$visit_invoice_id.")'><i class='fa fa-trash'></i></a>
							</td>";
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			
		$result .='
					<tr> 

						<td class="'.$text_color.'">'.form_checkbox($checkbox_data).'<label for="checkbox'.$v_procedure_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td class="'.$text_color.'" >'.$number.' '.$v_procedure_id.'</td>
						<td  class="'.$text_color.'" align="left">'.$procedure_name.'</td>
						<td  class="'.$text_color.'" align="left"><input class="form-control" id="visit_comment'.$v_procedure_id.'"  value="'.$visit_comment.'" placeholder="Comment"></td>
						<td  class="'.$text_color.'" align="center">
							<input type="text" id="units'.$v_procedure_id.'" class="form-control" value="'.$units.'" size="3" />

							<input type="hidden" id="visit_invoice_id'.$v_procedure_id.'" class="form-control" value="'.$visit_invoice_id.'" size="3" />
						</td>
						<td  class="'.$text_color.'" align="center"><input type="text" class="form-control" size="5" value="'.$visit_charge_amount.'" id="billed_amount'.$v_procedure_id.'"></div></td>
						<td  class="'.$text_color.'" >'.number_format($units*$visit_charge_amount,2).'</td>
						'.$checked.'
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		
		<td></td>
		<td colspan='4'></td>

		<th>Grand Total: </th>
		<th colspan='3' align='center'><div id='grand_total'>".number_format($total,2)."</div></th>
		
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	echo $result;
?>

<div class="row" style="margin-top: 10px;">
	<div class="col-md-12">
		 <input type="hidden" class="form-control" name="charge_visit_id" id="charge_visit_id" placeholder="Middle Name" value="<?php echo $visit_id;?>">
		 <input type="hidden" class="form-control" name="charge_patient_id" id="charge_patient_id" placeholder="Patient Id" value="<?php echo $patient_id;?>">
		 <input type="hidden" class="form-control" name="visit_invoice_id" id="visit_invoice_id" placeholder="Patient Id" value="<?php echo $visit_invoice_id;?>">
		 <input type="hidden" class="form-control" name="amount" id="amount" placeholder="Amount" value="<?php echo $total;?>">
		 <input type="hidden" class="form-control" name="dentist_id" id="dentist_id" placeholder="Amount" value="<?php echo $dentist_id;?>">

		<div class="col-md-4">
			<div class="form-group">
                <label class="col-md-4 control-label">Billing Details: <?php echo $inpatient;?></label>
                
                <div class="col-md-8">
                    <select name="visit_type_id" id="visit_type_id" class="form-control">
							<option value="">----Select an account----</option>
							<?php		
								$visit_types = $this->reception_model->get_visit_types();
								$types = '';
								if($visit_types->num_rows() > 0)
								{
									foreach ($visit_types->result() as $key => $value2) {
										# code...
										$visit_type_name = $value2->visit_type_name;
										$visit_type_id = $value2->visit_type_id;

										if($visit_type ===  $visit_type_id)
										{
											echo  '<option value="'.$visit_type_id.'" selected="selected">'.$visit_type_name.'</option>';
										}
										else
										{
											echo  '<option value="'.$visit_type_id.'">'.$visit_type_name.'</option>';
										}
									}
								}
							?>
					</select>
                </div>
            </div>

            <div id="insured_company" style="display: <?php echo $display?>;">
				<div class="form-group">
					<label class="col-md-4 control-label">Scheme: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="scheme_name" value="<?php echo $scheme_name;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Member Number: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="member_number" value="<?php echo $member_number;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Insurance Limit:</label>
					
					<div class="col-md-8">
		                <input type="number" class="form-control" name="insurance_limit" value="<?php echo $insurance_limit?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-lg-4 control-label">Normal Invoice or Preauth? </label>
		            <div class="col-lg-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" name="preauth" value="0" checked="checked" >
		                        Normal
		                    </label>
		                </div>
		            </div>
		            
		            <div class="col-lg-4">
		                <div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" name="preauth" value="1">
		                        Preauth
		                    </label>
		                </div>
		            </div>
				</div>

				
				
			</div>
		</div>
		<div class="col-md-8">
			<div id="insured_company2" style="display: <?php echo $display?>;">
				
			
				<div class="form-group" >
	                <label class="col-md-4 control-label">Preauth Date: </label>
	                
	                <div class="col-md-8">
	                    <div class="input-group">
	                        <span class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </span>
	                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="preauth_date" placeholder="Preauth Date" value="<?php echo $preauth_date;?>">
	                    </div>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-4 control-label">Authorised Amount: </label>
	                
	                <div class="col-md-8">
	                    <input type="number" class="form-control" name="preauth_amount" id="preauth_amount" placeholder="Authorised Amount" value="<?php echo $preauth_amount?>" onkeyup="update_authorised_amount()">
	                </div>

	            </div>
	            <div class="form-group">
	                <label class="col-md-4 control-label">Authorised By: </label>
	                
	                <div class="col-md-8">
	                    <input type="text" class="form-control" name="authorising_officer" placeholder="Authorised By" value="<?php echo $authorising_officer?>">
	                </div>

	            </div>
	        
	        </div>
	        <br>
	        <div id="notification-div" style="display: none;">
	        	<div class="form-group">
	                <label class="col-md-4 control-label">Note : </label>
	                
	                <div class="col-md-8">
			        	<div id="message_warning"></div>
			        </div>
		        </div>
	        </div>

	        <div id="note-div" style="display: none;">
	        	 <div class="form-group">
	                <label class="col-md-4 control-label">Reason: </label>
	                
	                <div class="col-md-8">
	                	<textarea name="visit_bill_reason" id="visit_bill_reason" class="form-control" placeholder="Please enter for the difference"></textarea>
	                   
	                </div>

	            </div>
	        </div>
	        <br>
	         <div class="form-group">
                <label class="col-md-4 control-label">Total Invoice Amount: </label>
                
                <div class="col-md-8">
                    <input type="text" class="form-control" name="total_invoice" id="total_invoice" placeholder="Preauth Amount" value="<?php echo number_format($total,2)?>" readonly>
                </div>

            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Invoice number: </label>
                
                <div class="col-md-8">
                    <input type="text" class="form-control" name="visit_invoice_number" placeholder="Invoice number" value="<?php echo $visit_invoice_number?>" >
                </div>
                
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Invoice Date: </label>
                
                <div class="col-md-8">
                	<div class="input-group">
	                	<span class="input-group-addon">
	                        <i class="fa fa-calendar"></i>
	                    </span>
	                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="invoice_date" placeholder="Invoice date" value="<?php echo $created;?>">
	                </div>
                </div>
                
            </div>

            <div class="form-group">
                <div class="center-align" style="margin-top:10px;">
					<button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to confirm this invoice ? ')"> COMPLETE INVOICE</button>
				</div>
	                
            </div>
			
		</div>
		
	</div>

	<?php 
	$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
	$personnel_id = $this->session->userdata('personnel_id');


	if($visit_invoice_id > 0 AND ($authorize_invoice_changes OR $personnel_id == 0))
	{

		?>

			<div class="col-md-12">
				<a  class="btn btn-sm btn-danger pull-right" onclick="delete_invoice(<?php echo $visit_invoice_id;?>,<?php echo $patient_id;?>)"> <i class="fa fa-trash"></i> DELETE INVOICE</a>
			</div>
		<?php

	}
	?>

</div>
 <?php echo form_close();?>



 <?php 

	if($preauth_status > 0 AND $preauth_status < 3)
	{
		?>
		<hr>
		<div class="col-md-12">
			<div class="col-md-10">
				<?php echo form_open("", array("class" => "form-horizontal","id" => "approve-invoice"));?>
					<input type="hidden" name="visit_invoice_id" id="visit_invoice_id" value="<?php echo $visit_invoice_id;?>">
					 <input type="hidden" class="form-control" name="total_invoice_amount" id="total_invoice_amount" placeholder="Authorised Amount" value="<?php echo $preauth_amount?>" onkeyup="update_authorised_amount()">
    				<div class="form-group" >
						<label class="col-lg-4 control-label"> Approved Amount: </label>

						<div class="col-lg-8">
							<input type="number" class="form-control" name="approved_amount" placeholder="" autocomplete="off" required="required" value="<?php echo $approved_amount;?>"  >
						</div>
					</div>

					<div  class="form-group" >
						<div class="center-align">

							<input type="submit" class="btn btn-sm- btn-warning" name="approved_amount" value="Mark invoice as approved" >
							
						</div>
					</div>
				</form>
			</div>
			<hr>
			<div class="col-md-12" style="margin-top: 10px; text-align: left;">
				<a class="btn btn-success btn-sm  " onclick="mark_as_invoice(<?php echo $visit_invoice_id;?>)" ><i class="fa fa-folder"></i> Make proforma invoice as an invoice </a>

			</div>
			
			<div class="col-md-12" style="margin-top: 10px; text-align: right;">
			<!-- 	<a href="<?php echo site_url();?>accounts/decline_invoice/<?php echo $visit_id?>" class="btn btn-danger btn-sm  " onclick="return confirm('Are you sure your want to mark this as not approved ?')" ><i class="fa fa-folder"></i> Mark proforma invoice as declined </a> -->

			</div>

		</div>
		<?php
	}
	?>