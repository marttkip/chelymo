<?php
$preauth_date  = '';
$rebate_status = 0;
if($visit_invoice_id > 0)
{

	// var_dump($visit_invoice_id);die();
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = date('d/m/Y',strtotime($value->preauth_date));
			$invoice_date = date('d/m/Y',strtotime($value->created));
			$preauth_amount = $value->preauth_amount;

			$authorising_officer = $value->authorising_officer;
			$insurance_limit = $value->insurance_limit;
			$insurance_number = $value->member_number;
			$insurance_description = $value->scheme_name;
			$bill_to  = $value->bill_to;
			$authorising_officer  = $value->authorising_officer;
			$preauth_status  = $value->preauth_status;
			$rebate_status  = $value->rebate_status;



		}
	}


}

$days = $this->accounts_model->days_billed($visit_invoice_id);

if($rebate_status == 0)
{
	$items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="0" checked="checked" >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="1">
                        Yes
                    </label>
                </div>
            </div>';

}
else if($rebate_status == 1)
{
	$items = '<div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="0"  >
                        No
                    </label>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" name="rebate_status" value="1" checked="checked">
                        Yes
                    </label>
                </div>
            </div>';

}


?>
<div class='col-md-12'>
	<div class='col-md-6'>
		<div class="form-group">
			<label class="col-lg-4 control-label">Type: </label>
		  
			<div class="col-lg-8">
				<select id='visit_type_id' name='visit_type_id' class='form-control' >
                 
                  <?php
												
					if($visit_types_rs->num_rows() > 0){

						foreach($visit_types_rs->result() as $row):
							$visit_type_name = $row->visit_type_name;
							$visit_type_id = $row->visit_type_id;

							if($visit_type_id == $bill_to)
							{
								echo "<option value='".$visit_type_id."' selected='selected'>".$visit_type_name."</option>";
							}
							
							else
							{
								echo "<option value='".$visit_type_id."'>".$visit_type_name."</option>";
							}
						endforeach;
					}
				?>
                  
                </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">Scheme: </label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="scheme_name" value="<?php echo $insurance_description?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">Member Number: </label>
			<div class="col-md-8">
				<input type="text" class="form-control" name="member_number" value="<?php echo $insurance_number?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label">Insurance Limit:</label>
			
			<div class="col-md-8">
                <input type="number" class="form-control" name="insurance_limit" value="<?php echo $insurance_limit?>">
			</div>
		</div>


	</div>
	<div class='col-md-6'>
		<div class="form-group">
			<label class="col-lg-4 control-label">Include NHIF Rebate? </label>
            <?php echo $items?>
		</div>
		<div class="alert alert-xs alert-danger">No of days <?php echo $days?> days.</div>
		<div class="form-group">
			<label class="col-lg-4 control-label">Invoice Date: </label>
			<div class="col-lg-8">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" id="datepicker" placeholder="Invoice Date" value="<?php echo date('Y-m-d');?>">
                </div>
			</div>
		</div>
	</div>
  	

	 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
  	 <input type="hidden" name="billing_visit_id" id="billing_visit_id" value="<?php echo $visit_id?>">
  	 <input type="hidden" name="billing_patient_id" id="billing_patient_id"  value="<?php echo $visit_id?>">
  	 <input type="hidden" name="visit_invoice_id" id="visit_invoice_id"  value="<?php echo $visit_invoice_id?>">
</div>