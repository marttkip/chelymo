<?php echo $this->load->view('search_monthly_sales', '', TRUE);?>
<div class="row">
	<div class="col-md-1">
	</div>
    <div class="col-md-10">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title">MONTHLY SALES TURNOVER</h2>
            	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
            	  	<a href="<?php echo site_url().'print-monthly-sales-report'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> PRINT MONTHLY SALES</a>
            	  
            	</div>
            </header>             

          <!-- Widget content -->
        	<div class="panel-body">

        		<?php

        		 $search = $this->session->userdata('monthly_turnover_searched');
				if(!empty($search))
				{
					$month_search = $this->session->userdata('monthly_turnover_month');
					$year_search = $this->session->userdata('monthly_turnover_year');


					$todays_date = $year_search.'-'.$month_search.'-01';

					$report_date= date('M Y',strtotime($todays_date));
				}
				else
				{
					$todays_date = date('Y').'-'.date('m').'-01';

					$report_date= date('M Y',strtotime($todays_date));
				}
				?>   
         	 <h5 class="center-align"><strong><?php echo strtoupper($report_date);?></strong></h5>
         	 <br>

         	 <?php
         	 $search = $this->session->userdata('monthly_turnover_searched');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'hospital_reports/close_monthly_turnover_search" class="btn btn-sm btn-warning">Close Search</a>';
			}
         	 ?>

         	 <?php
         	 $grand_invoices = 0;
         	 $grand_collections = 0;

         	 ?>
         




                <table class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                        <th style="width: 20%;">DOCTORS</th>
                        <th style="width: 10%;">CASH BILLINGS</th>
                        <th style="width: 10%;">INS. BILLINGS</th>
                        <th style="width: 10%;">TOTAL BILLINGS</th>
                        <th style="width: 10%;">TOTAL COLLECTIONS</th>
                        <th style="width: 20%;">PROF. FEES</th>
                        <th style="width: 10%;">WHT</th>
                        <th style="width: 10%;">NET PROF. FEES</th>
                    </thead>
                    <tbody>
                        <?php
                        $total_cash_breakdown = 0;
                        $total_cash_amount_invoiced = 0;
                        $total_cash_amount_paid = 0;

                        $total_insurance_amount_invoiced = 0;
                        $total_insurance_amount_paid = 0;

                         $total_professional_fees = 0;
                        $total_wht = 0;
                        $net_total = 0;
                        
                        
                        $doctors = $this->hospital_reports_model->get_doctor();
                         // var_dump($doctors->num_rows());die();
                         if($doctors->num_rows() > 0)
                         {
                            foreach($doctors->result() as $row):
                                $fname = $row->personnel_fname;
                                $onames = $row->personnel_onames;
                                $personnel_id = $row->personnel_id;


                                $professional_fees_percentage = $row->professional_fees_percentage;



			                  
                                $cash_amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id,null,1,1);
                                $cash_amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id,null,1,1);
                                $cash_amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id,null,1,1);
                                
                                $cash_amount_invoiced -= $cash_amount_credits;
                                $total_cash_amount_invoiced += $cash_amount_invoiced;
                                $total_cash_amount_paid += $cash_amount_paid;



                                $insurance_amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id,null,1,2);
                                $insurance_amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id,null,1,2);
                                $insurance_amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id,null,1,2);
                                
                                $insurance_amount_invoiced -= $insurance_amount_credits;
                                $total_insurance_amount_invoiced += $insurance_amount_invoiced;
                                // $total_insurance_amount_paid += $insurance_amount_paid;

                                $total_invoiced_amount = $cash_amount_invoiced + $insurance_amount_invoiced;

                                $total_paid_amount = $cash_amount_paid ;

			                    $amount = $total_invoiced_amount * ($professional_fees_percentage/100);
			                    $amount_vat =$amount - ($amount * 0.05);

                                $wht =($amount * 0.05);

                                $total_professional_fees += $amount;
                                $total_wht += $wht;
                                $net_total += $amount_vat;

			                    $grand_invoices += $total_invoiced_amount;
			                    $grand_collections += $total_paid_amount;

                                // if($amount_invoiced > 0 OR $amount_paid > 0)
                                // {
                                    echo 
                                        '
                                       <tr>
                                            <th>DR. '.strtoupper($onames.' '.$fname).' </th>
                                            <td>'.number_format($cash_amount_invoiced, 2).'</td>
                                            <td>'.number_format($insurance_amount_invoiced, 2).'</td>
                                            <td><a onclick="get_month_personnel_invoices('.$personnel_id.')">'.number_format($total_invoiced_amount, 2).'</a></td>
                                            <td><a onclick="get_month_personnel_payments('.$personnel_id.')">'.number_format($total_paid_amount, 2).'</a></td>
                                            <td>'.number_format($amount, 2).'</td>
                                            <td>'.number_format($wht, 2).'</td>
                                            <td>'.number_format($amount_vat, 2).'</td>
                                        </tr>
                                        ';
                                // }
                                
                                
                                
                                
                            endforeach;

                           
                            echo 
                                    '
                                     <tr>
                                        <th>TOTALS</th>
                                        <th>'.number_format($total_cash_amount_invoiced, 2).'</th>
                                        <th>'.number_format($total_insurance_amount_invoiced, 2).'</th>

                                        <th>'.number_format($grand_invoices, 2).'</th>
                                        <th>'.number_format($grand_collections, 2).'</th>


                                        <th>'.number_format($total_professional_fees, 2).'</th>
                                        <th>'.number_format($total_wht, 2).'</th>
                                         <th>'.number_format($net_total, 2).'</th>
                                    </tr>
                                    ';
                        }
                       
                        ?>
                    </tbody>
                </table>


                 <table class="table table-striped table-bordered">
	     			<thead>
	     				<tr>
	     					<th>TOTAL BILLINGS</th>
	     					<th><?php echo number_format($grand_invoices,2)?></th>
	     				</tr>
	     				<tr>
	     					<th>TOTAL COLLECTIONS</th>
	     					<th><?php echo number_format($grand_collections,2)?></th>
	     				</tr>
	     			</thead>
	     		</table>

            </div>
         
		</section>
    </div>
    <div class="col-md-1">
	</div>
  </div>


<script type="text/javascript">
    
    function get_month_personnel_invoices(personnel_id)
    {
        document.getElementById("sidebar-right").style.display = "block"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 

        var config_url = $('#config_url').val();
        $.ajax({
                type:'POST',
                url: config_url+"hospital_reports/get_personnel_invoices/"+personnel_id,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "text",
                success:function(data){
                  
                     document.getElementById("current-sidebar-div").style.display = "block"; 
                    $('#current-sidebar-div').html(data);
                    
                }
        });
    }

    function get_month_personnel_payments(personnel_id)
    {
        
        document.getElementById("sidebar-right").style.display = "block"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 

        var config_url = $('#config_url').val();
        $.ajax({
                type:'POST',
                url: config_url+"hospital_reports/get_personnel_payments/"+personnel_id,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "text",
                success:function(data){
                  
                     document.getElementById("current-sidebar-div").style.display = "block"; 
                    $('#current-sidebar-div').html(data);
                    
                }
        });
    }

    function close_side_bar()
    {
        document.getElementById("sidebar-right").style.display = "none"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 
    }
</script>