<!-- search -->
<?php echo $this->load->view('search_drb', '', TRUE);?>

<?php //echo $this->load->view('transaction_statistics', $v_data, TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title">DRB</h2>
            	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
            	  	<a href="<?php echo site_url().'print-drb'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print DRB</a>
            	  
            	</div>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
		<?php
				$result = '';
				$search = $this->session->userdata('drb_search');

				// var_dump($search);die();
				if(!empty($search))
				{
					echo '<a href="'.site_url().'hospital_reports/reports/close_drb_search" class="btn btn-sm btn-warning">Close Search</a>';
				}
				
				$total_invoiced = 0;
				$total_balance = 0;

				$query = $this->hospital_reports_model->get_patient_transactions();


				if(!empty($query))
				{





				$result .= '
							<table class="table table-hover table-bordered table-striped table-responsive col-md-12">

								<thead>
									<th>#</th>
									<th>DATE</th>
									<th>FILE NUMBER</th>
									<th>DOCTOR</th>
									<th>PATIENT NAME</th>
									<th>INVOICE NUMBER</th>
									<th>DEBITS</th>
									<th>CREDITS</th>
							  </thead>
							  <tbody>

							';
				$count = 0;
				$total_debits = 0;
				$total_credits = 0;
				

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...

						$patient_othernames = $value->patient_othernames;
						$patient_id = $value->patient_id;
						$patient_surname = $value->patient_surname;
						$visit_invoice_number = $value->visit_invoice_number;
						$transaction_date = $value->transaction_date;
						$dr_amount = $value->dr_amount;
						$cr_amount = $value->cr_amount;
						$reference_code = $value->reference_code;
						$personnel_fname = $value->personnel_fname;
						$personnel_onames = $value->personnel_onames;
						$patient_number = $value->patient_number;


						$total_debits += $dr_amount;
						$total_credits += $cr_amount;

						$count++;
						 $result .= '
						 			<tr>
						 				<td>'.$count.'</td>
						 				<td>'.date('jS M Y',strtotime($transaction_date)).'</td>
						 				<td>'.$patient_number.'</td>
						 				<td>'.$personnel_fname.' '.$personnel_onames.'</td>
						 				<td>'.$patient_surname.'</td>
						 				<td>'.$reference_code.'</td>
						 				<td>'.number_format($dr_amount,2).'</td>
						 				<td>'.number_format($cr_amount,2).'</td>
						 			</tr>';

					}
				}
									
				$result .= 
						'
						
								<tr>
								  <th colspan="5"></th>
								  <th>SUBTOTALS</th>
								  <th>'.number_format($total_debits,2).'</th>
								  <th>'.number_format($total_credits,2).'</th>
								</tr>
					';

				$result .= 
						'
						
								<tr>
								  <th colspan="5"></th>
								  <th>BALANCE</th>
								  <th colspan="2" class="center-align">'.number_format($total_debits-$total_credits,2).'</th>
							
								</tr>
					';
				$result .= '
						</tbody>
					</table>';
				
				echo $result;
				
		?>

			<div class="row">
	         	<div class="col-md-4">
	         		<table class="table table-striped table-bordered">
	         			<thead>
	         				<tr>
	         					<th>TOTAL BILLINGS</th>
	         					<th><?php echo number_format($total_debits,2)?></th>
	         				</tr>
	         				<tr>
	         					<th>TOTAL COLLECTIONS</th>
	         					<th><?php echo number_format($total_credits,2)?></th>
	         				</tr>
	         			</thead>
	         		</table>
	         	</div>
	         	
	        </div>
          </div>

          <?php
      	}
          ?>
          	
          
          
        
		</section>
    </div>
  </div>