
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | DAILY SALES RECORD</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:10px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 10px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}
            table td
            {
            	border: 2px solid grey !important;
            }
            table th
            {
            	border: 2px solid grey !important;
            }
          	@media print {

          		
          	}
            .padd
            {
                padding:10px;
            }
            
        </style>

        <style media="print">
		/*Specific CSS rules for the print version of the webpage */
			table td
	            {
	            	border: 2px solid grey !important;
	            }
	            table th
	            {
	            	border: 2px solid grey !important;
	            }
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                        <!-- <p style="margin-top: 20px !important;"> Dr. Lorna Sande | Dr. Kinoti M, Dental Surgeons</p> -->
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <!-- <strong>
                            Professor Nelson Awori Centre, Ground Floor<br/>
                            Suite A3 & A1, Ralph Bunche Rd No. 7<br/>
                            Telephone: +254 20 2430110 <br/>
                            +254 706 706 000 +254 700 033 337 <br/>
                            accounts@upperhilldentalcentre.com <br/>
                            P.O. Box 1998600202 Nairobi
                         
                        </strong> -->
                        <div  class="align-right">
			            	  <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
			            </div>
                    </div>
                    
                </div>
		       <div class="col-md-12">
		       		<div style="margin-top: 10px">
		       		<h4 class="left-align"><?php echo 'DAILY SALES RECORD';?></h4>
		       		<h4 class="left-align"><?php echo strtoupper($this->session->userdata('drb_search_title'));?></h4>
		       		<br>
		       		<?php
		       			
		       		$total_invoiced = 0;
					$total_balance = 0;

					$query = $this->hospital_reports_model->get_patient_transactions();


					if(!empty($query))
					{





					$result .= '
								<table class="table table-hover table-bordered table-striped table-responsive col-md-12">

									<thead>
										<th>#</th>
										<th>DATE</th>
										<th>FILE NUMBER</th>
										<th>DOCTOR</th>
										<th>PATIENT NAME</th>
										<th>INVOICE NUMBER</th>
										<th>DEBITS</th>
										<th>CREDITS</th>
								  </thead>
								  <tbody>

								';
					$count = 0;
					$total_debits = 0;
					$total_credits = 0;
					

					if($query->num_rows() > 0)
					{
						foreach ($query->result() as $key => $value) {
							// code...

							$patient_othernames = $value->patient_othernames;
							$patient_id = $value->patient_id;
							$patient_surname = $value->patient_surname;
							$visit_invoice_number = $value->visit_invoice_number;
							$transaction_date = $value->transaction_date;
							$dr_amount = $value->dr_amount;
							$cr_amount = $value->cr_amount;
							$reference_code = $value->reference_code;
							$personnel_fname = $value->personnel_fname;
							$personnel_onames = $value->personnel_onames;
							$patient_number = $value->patient_number;


							$total_debits += $dr_amount;
							$total_credits += $cr_amount;

							$count++;
							 $result .= '
							 			<tr>
							 				<td>'.$count.'</td>
							 				<td>'.date('jS M Y',strtotime($transaction_date)).'</td>
							 				<td>'.$patient_number.'</td>
							 				<td>'.$personnel_fname.' '.$personnel_onames.'</td>
							 				<td>'.$patient_surname.'</td>
							 				<td>'.$reference_code.'</td>
							 				<td>'.number_format($dr_amount,2).'</td>
							 				<td>'.number_format($cr_amount,2).'</td>
							 			</tr>';

						}
					}
										
					$result .= 
							'
							
									<tr>
									  <th colspan="5"></th>
									  <th>SUBTOTALS</th>
									  <th>'.number_format($total_debits,2).'</th>
									  <th>'.number_format($total_credits,2).'</th>
									</tr>
						';

					$result .= 
							'
							
									<tr>
									  <th colspan="5"></th>
									  <th>BALANCE</th>
									  <th colspan="2" class="center-align">'.number_format($total_debits-$total_credits,2).'</th>
								
									</tr>
						';
					$result .= '
							</tbody>
						</table>';
					
					echo $result;
					
			?>

				<div class="row">
		         	<div class="col-md-4">
		         		<table class="table table-striped table-bordered">
		         			<thead>
		         				<tr>
		         					<th>TOTAL BILLINGS</th>
		         					<th><?php echo number_format($total_debits,2)?></th>
		         				</tr>
		         				<tr>
		         					<th>TOTAL COLLECTIONS</th>
		         					<th><?php echo number_format($total_credits,2)?></th>
		         				</tr>
		         			</thead>
		         		</table>
		         	</div>
		         	<div class="col-md-8">
		         		&nbsp;
		         	</div>
		        </div>
	          </div>

	          <?php
	      	}
	          ?>
		       </div>
		    </div>
		</div>
	</body>
</html>
   		