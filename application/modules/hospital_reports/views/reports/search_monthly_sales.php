<?php

$month_searched = $this->session->userdata('monthly_turnover_month');
$year = $this->session->userdata('monthly_turnover_year');

$doctors_turnover_searched = $this->session->userdata('monthly_turnover_searched');



if($doctors_turnover_searched)
{
	$month_checked = $month_searched;
}
else
{
	$month_checked = date('M');
}
// (int)$month_checked;
// var_dump($month_checked);die();
?>
<div class="row">

    	<section class="panel">
            <header class="panel-heading">						
                <h2 class="panel-title">Search monthly sales summary</h2>
            </header>
            <div class="panel-body">
            	<?php 
				echo form_open('hospital-reports/search-monthly-turnover');
				?>
				<div class="col-md-4">
					<div class="form-group">
	                    <label class="col-lg-5 control-label">Year: </label>
	                    
	                    <div class="col-lg-7">
	                        <input type="text" name="year" class="form-control" size="54" value="<?php echo date("Y");?>" />
	                    </div>
	                </div>
					
				</div>
				<div class="col-md-4">
					<div class="form-group">
	                    <label class="col-lg-5 control-label">Month: </label>
	                    
	                    <div class="col-lg-7">
	                        <select name="month" class="form-control">
	                            <?php
	                                if($month->num_rows() > 0){
	                                    foreach ($month->result() as $row):
	                                        $mth = $row->month_name;
	                                        $mth_id = $row->month_id;
	                                        if($mth == $month_checked){
	                                            echo "<option value=".$mth_id." selected>".$row->month_name."</option>";
	                                        }
	                                        else{
	                                            echo "<option value=".$mth_id.">".$row->month_name."</option>";
	                                        }
	                                    endforeach;
	                                }
	                            ?>
	                        </select>
	                    </div>
	                </div>
				</div>
				<div class="col-md-4">
                
                
                
                
	          
	                    <div class="col-lg-7 col-lg-offset-5">
	                        <div class="form-actions center-align">
	                            <button class="submit btn btn-primary" type="submit">
	                                <i class='fa fa-search'></i> Search
	                            </button>
	                        </div>
	                    </div>
	                </div>
                <?php echo form_close();?>
            </div>
        </section>

    
	
</div>
