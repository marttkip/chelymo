
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 9px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

          
            .padd
            {
                padding:10px;
            }

            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 3px;
				line-height: 1.42857143;
				vertical-align: top;
				border-top: 2px solid #dddddd;
			}
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
             <div class="row">
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" width="25%"/>
                        <!-- <p style="margin-top: 20px !important;"> Dr. Lorna Sande | Dr. Kinoti M, Dental Surgeons</p> -->
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                            <!-- Professor Nelson Awori Centre, Ground Floor<br/>
                            Suite A3 & A1, Ralph Bunche Rd No. 7<br/>
                            Telephone: +254 20 2430110 <br/>
                            +254 706 706 000 +254 700 033 337 <br/>
                            accounts@upperhilldentalcentre.com <br/>
                            P.O. Box 1998600202 Nairobi
                          -->
                          <div  class="align-right">
			            	  <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
			            </div>
                        </strong>
                    </div>
                    
                </div>
		       <div class="col-md-12">

		       		<?php

		       		$month_search = $this->session->userdata('doctors_turnover_month');
		       		$year_search = $this->session->userdata('doctors_turnover_year');


		       		if(empty($year_search) OR empty($month_search))
		       		{
		       			$first_day_this_month = date('m-01-Y'); // hard-coded '01' for first day
						$last_day_this_month  = date('m-t-Y');
		       		}
		       		else
		       		{
		       				if($month_search < 10)
							{
								$month_search = '0'.$month_search;
							}
							$first_day_this_month = date('01-'.$month_search.'-'.$year_search); // hard-coded '01' for first day

							$last_date = date("Y-m-t", strtotime($first_day_this_month));

							$last_day_this_month  = date("m-t-Y", strtotime($last_date));
		       		}
		       		$last_day_date_explode = explode('-', $last_day_this_month);

					$count = $last_day_date_explode[1];
					$month = $last_day_date_explode[0];
					$year = $last_day_date_explode[2];
		       		$todays_date = $year.'-'.$month.'-01';

					$report_date= date('M Y',strtotime($todays_date));
		       		?>
		       		<div style="margin-top: 10px">
		       		<h4 class="left-align"><?php echo 'DOCTORS TURNOVER SUMMARY';?></h4>
		       		<h4 class="left-align"><?php echo strtoupper($report_date);?></h4>
		       	
		       		<div style="margin-top: 10px">
		       			<?php
						$result = '';
							$search = $this->session->userdata('doctors_turnover_search');
						



							

							$last_day_date_explode = explode('-', $last_day_this_month);

							$count = $last_day_date_explode[1];
							$month = $last_day_date_explode[0];
							$year = $last_day_date_explode[2];

							// var_dump($count);die();
							// var_dump($count);die();
					// var_dump($count);die();
			$result = '
						<h4 class="center-align"> '.$report_date.'</h4>
						<br>
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <tbody>
							';


            $doctors = $this->hospital_reports_model->get_doctor();
             // var_dump($doctors->num_rows());die();
            $list = '';
           	$list_two = '';
             if($doctors->num_rows() > 0)
             {
                foreach($doctors->result() as $row):
                    $fname = $row->personnel_fname;
                    $onames = $row->personnel_onames;
                    $personnel_id = $row->personnel_id;

                    $list .= '<th colspan="3">Dr. '.$fname.' '.$onames.'</th>';
                     $list_two .= '<th >Billings</th>
                     			   <th >Lab Work</th>
                     			   <th >Collections</th>';
                endforeach;
              }
              $list .= '<th colspan="3">ADP</th>';
              $list_two .= '<th >Billings</th>
              				<th >Lab Work</th>
                     		<th >Collections</th>';
			$result .= 
							'
							
									<tr>
									  <th>#</th>
									  '.$list.'
									</tr>
						';


			$result .= 
							'
							
									<tr>
									  <th></th>
									  '.$list_two.'
									</tr>
						';
						for ($i=1; $i <=  $count; $i++) { 
							# code...

							$date = $i.'.'.$month;


							if($i < 10)
							{
								$day = '0'.$i;
							}
							else
							{
								$day = $i;
							}
							$todays_date = $year.'-'.$month.'-'.$day;

							// var_dump($todays_date);die();
						
				           	$list_two = '';
				             if($doctors->num_rows() > 0)
				             {
				                foreach($doctors->result() as $row):
				                    $fname = $row->personnel_fname;
				                    $onames = $row->personnel_onames;
				                    $personnel_id = $row->personnel_id;


				                    $amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id,$todays_date);
				                    $lab_work = $this->hospital_reports_model->get_personnel_days_lab_work($personnel_id,$todays_date);
                                    $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id,$todays_date);
                                    $amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id,$todays_date);
                                     $amount_paid += $amount_credits;


                                    
                                    $amount_invoiced -= $amount_credits;


                                    $array['collection'.$personnel_id] += $amount_paid;
                                    $array['invoice'.$personnel_id] += $amount_invoiced;
                                    $array['lab_work'.$personnel_id] += $lab_work;
                                  

				                     $list_two .= '<td >'.number_format($amount_invoiced,2).'</td>
				                     			  <td >'.number_format($lab_work,2).'</td>
				                     			   <td >'.number_format($amount_paid,2).'</td>';



				                endforeach;
				              }


				              $amount_paid = $this->hospital_reports_model->get_personnel_days_payments(0,$todays_date);
                              $amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices(0,$todays_date);
                               $array['collection0'] += $amount_paid;
                                $array['invoice0'] += $amount_invoiced;

                              $list_two .= '<td >'.number_format($amount_invoiced,2).'</td>
                              				<td >'.number_format(0,2).'</td>
				                     		<td >'.number_format($amount_paid,2).'</td>';

							 $result .= 
										'
										
												<tr>
												  <th>'.$date.'</th>
												  '.$list_two.'
												</tr>
										';
							
						}

						$list_two = '';
						$grand_invoices = 0;
						$grand_lab_works = 0;
						$grand_collections = 0;

						$grand_director_collections = 0;
						$grand_director_invoices = 0;
						$grand_associates_invoices = 0;
						$grand_associates_collections = 0;


				             if($doctors->num_rows() > 0)
				             {
				                foreach($doctors->result() as $row):
				                    $fname = $row->personnel_fname;
				                    $onames = $row->personnel_onames;
				                    $personnel_id = $row->personnel_id;
				                    $personnel_type_id = $row->personnel_type_id;

				                    if($personnel_type_id == 4)
				                    {
				                    	$grand_director_invoices += $array['invoice'.$personnel_id];
				                    	$grand_director_collections += $array['collection'.$personnel_id];
				                    }
				                    else if($personnel_type_id == 2)
				                    {
				                    	$grand_associates_invoices += $array['invoice'.$personnel_id];
				                    	$grand_associates_collections += $array['collection'.$personnel_id];
				                    }
				                    $total_amount_collection = $array['collection'.$personnel_id];
									$total_amount_invoiced = $array['invoice'.$personnel_id];
									$total_amount_lab_work = $array['lab_work'.$personnel_id];

									$grand_invoices += $total_amount_invoiced;
									$grand_lab_works += $total_amount_lab_work;
									$grand_collections += $total_amount_collection;

									 $list_two .= '<th >'.number_format($total_amount_invoiced,2).'</th>
									 				<th >'.number_format($total_amount_lab_work,2).'</th>
				                     			   <th >'.number_format($total_amount_collection,2).'</th>';
				                 endforeach;
				             }
				            $total_amount_collection = $array['collection0'];
							$total_amount_invoiced = $array['invoice0'];

							$grand_invoices += $total_amount_invoiced;
							$grand_collections += $total_amount_collection;
				             $list_two .= '<th >'.number_format($total_amount_invoiced,2).'</th>
				            				 <th >'.number_format(0,2).'</th>
				                     			   <th >'.number_format($total_amount_collection,2).'</th>';
						 $result .= 
										'
										
												<tr>
												  <th>Total</th>
												  '.$list_two.'
												</tr>
										';


					$result .= 
				'
							  </tbody>
							</table>
						

				';
		
		echo $result;
		
?>

			<div class="col-md-12">
	         	<div class="col-md-4">
	         		<table class="table table-striped table-bordered">
	         			<thead>
	         				<tr>
	         					<th>TOTAL BILLINGS</th>
	         					<th><?php echo number_format($grand_invoices,2)?></th>
	         				</tr>
	         				<tr>
	         					<th>TOTAL LAB WORKS</th>
	         					<th><?php echo number_format($grand_lab_works,2)?></th>
	         				</tr>
	         				<tr>
	         					<th>TOTAL COLLECTIONS</th>
	         					<th><?php echo number_format($grand_collections,2)?></th>
	         				</tr>
	         			</thead>
	         		</table>
	         	</div>
	         	
	         </div>

	         <div class="row">
	         	<div class="col-md-12">
	         		<?php

	         			$result = '
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
						  <tbody>
							';


			       
			            $list = '';
			           	$list_two = '';
			           	$list_three = '';
			           	$list_four = '';
						if($doctors->num_rows() > 0)
						{
			                foreach($doctors->result() as $row):
			                    $fname = $row->personnel_fname;
			                    $onames = $row->personnel_onames;
			                    $personnel_id = $row->personnel_id;
			                    $professional_fees_percentage = $row->professional_fees_percentage;



			                    $total_amount_collection = $total_invoiced = $array['invoice'.$personnel_id];
			              		$total_amount_lab_work = $array['lab_work'.$personnel_id];

			              		$total_amount_collection = $total_amount_collection - $total_amount_lab_work;
			                    $amount = $total_amount_collection * ($professional_fees_percentage/100);

			                    if($amount >24000)
			                    {
			                    	$amount_vat = ($amount * 0.05);
			                    	$total_amount = $amount - ($amount * 0.05);
			                    }
			                    else
			                    {
			                    	$amount_vat = 0;
			                    	$total_amount = $amount;
			                    }
			                    


			                    $list .= '<th colspan="1">Dr. '.$fname.' '.$onames.'</th>
			                    		 ';
			                   	$list_two .= '<td colspan="1">'.number_format($amount,2).'</td>
			                    		 ';
			                    $list_three .= '<td colspan="1">'.number_format($amount_vat,2).'</td>
			                    		 ';
			                    $list_four .= '<td colspan="1">'.number_format($total_amount,2).'</td>
			                    		 ';
			                     $list_five .= '<td colspan="1">('.number_format($total_amount_lab_work,2).')</td>
			                    		 ';
			                   	$list_six .= '<td colspan="1">'.number_format($total_invoiced,2).'</td>
			                    		 ';
			                     
			                endforeach;
			              }
			               $list .= '<th colspan="1">ADP</th>';


			               $list_two .= '<td colspan="1">-</td>';
		                   $list_three .= '<td colspan="1">-</td>';

			              $result .= 
										'
										
												<tr>
												  <th></th>
												  '.$list.'
												</tr>
									';
						$result .= 
										'
										
												<tr>
												  <th>TOTAL BILLING</th>
												  '.$list_six.'
												</tr>
									';
						$result .= 
										'
										
												<tr>
												  <th>LAB WORK</th>
												  '.$list_five.'
												</tr>
									';
						$result .= 
										'
										
												<tr>
												  <th>PROF FEES</th>
												  '.$list_two.'
												</tr>
									';
						
						$result .= 
										'
										
												<tr>
												  <th>5% WHT</th>
												  '.$list_three.'
												</tr>
									';
						$result .= 
										'
										
												<tr>
												  <th>TOTAL AMOUNT</th>
												  '.$list_four.'
												</tr>
									';
						
						$result .= 
						'
									  </tbody>
									</table>
								

						';

						echo $result;

						$total_amount_collection = $array['collection0'];
						$total_amount_invoiced = $array['invoice0'];

						$totals_invoice = $grand_director_invoices+$grand_associates_invoices+$total_amount_invoiced;
						$totals_payments = $grand_director_collections+$grand_associates_collections+$total_amount_collection;
	         		?>

	         </div>
	     </div>

	     <div class="row">
	         	<div class="col-md-4">
	         		<table class="table table-striped table-bordered">
	         			<thead>
	         				<tr>
	         					<th></th>
	         					<th>Billings</th>
	         					<th>Collections</th>
	         				</tr>
	         				
	         				<tr>
	         					<th>DOCTORS</th>
	         					<td><?php echo number_format($grand_associates_invoices,2)?></td>
	         					<td><?php echo number_format($grand_associates_collections,2)?></td>
	         				</tr>
	         				<tr>
	         					<th>ADP</th>
	         					<td><?php echo number_format($total_amount_invoiced,2)?></td>
	         					<td><?php echo number_format($total_amount_collection,2)?></td>
	         				</tr>


	         				<tr>
	         					<th>TOTAL</th>
	         					<th><?php echo number_format($grand_invoices,2)?></th>
	         					<th><?php echo number_format($grand_collections,2)?></th>
	         				</tr>
	         			</thead>
	         		</table>
	         	</div>
	         	
	         </div>


          </div>


          </div>

				          </div>
		       		</div>
		       </div>
		    </div>
		</div>
	</body>
</html>
   		