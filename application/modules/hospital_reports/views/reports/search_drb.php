        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Active branch: <?php echo $branch_name;?></h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-drb-report", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                        <label class="col-lg-4 control-label">Doctor: </label>
                        
                          <div class="col-lg-8">
                            <select class="form-control" name="doctor_id" required="required">
                                <option value="0">---Select A Doctor---</option>
                                <?php
                                    if(count($doctors) > 0){
                                        foreach($doctors as $row):
                                            $personnel_fname = $row->personnel_fname;
                                            $personnel_onames = $row->personnel_onames;
                                            $personnel_id= $row->personnel_id;
                                                ?><option value="<?php echo $personnel_id; ?>" ><?php echo $personnel_fname ?> <?php echo $personnel_onames ?></option>
                                        <?php   
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>                

                </div>
                <div class="col-md-3">
                     <div class="form-group">
                        <label class="col-lg-4 control-label">Date From: </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From" autocomplete="off">
                            </div>
                        </div>
                    </div>                

                </div>
               
                 <div class="col-md-3">
                     <div class="form-group">
                        <label class="col-lg-4 control-label">Date To : </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To" autocomplete="off">
                            </div>
                        </div>
                    </div>                

                </div>
                                
                
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>