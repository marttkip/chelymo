
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 9px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

          
            .padd
            {
                padding:10px;
            }
            td{
            	border: 2px solid #000 !important;
            }
            tr
            {
            	border-top: 2px solid #000;
            }

            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 3px;
				line-height: 1.42857143;
				vertical-align: top;
				border: 2px solid #000;
				border-top: 2px solid #000;
			}
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
          
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                    
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                          
                          <div  class="align-right">
			            	  <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
			            </div>
                        </strong>
                    </div>
                    
                </div>
		       <div class="col-md-12">
		       		<?php

	        		 $search = $this->session->userdata('monthly_turnover_searched');
					if(!empty($search))
					{
						$month_search = $this->session->userdata('monthly_turnover_month');
						$year_search = $this->session->userdata('monthly_turnover_year');


						$todays_date = $year_search.'-'.$month_search.'-01';

						$report_date= date('M Y',strtotime($todays_date));
					}
					else
					{
						$todays_date = date('Y').'-'.date('m').'-01';

						$report_date= date('M Y',strtotime($todays_date));
					}
					?> 
			       	<div class="col-md-12" style="margin-top: 10px">
			       		<h4 class="left-align"><?php echo 'MONTHLY TURNOVER SUMMARY';?></h4>
			       		<h4 class="left-align"><?php echo strtoupper($report_date);?></h4>
			       	</div>
		       	
		       		<div class="col-md-12" style="margin-top: 10px">
		       			 <?php
			         	 $grand_invoices = 0;
			         	 $grand_collections = 0;

			         	 ?>
			         	 



			                <table class="table table-striped table-bordered table-hover table-condensed">
			                    <thead>
			                        <th style="width: 20%;">DOCTORS</th>
			                        <th style="width: 10%;">CASH BILLINGS</th>
			                        <th style="width: 10%;">INS. BILLINGS</th>
			                        <th style="width: 10%;">TOTAL BILLINGS</th>
			                        <th style="width: 10%;">TOTAL COLLECTIONS</th>
			                        <th style="width: 20%;">PROF. FEES</th>
			                        <th style="width: 10%;">WHT</th>
			                        <th style="width: 10%;">NET PROF. FEES</th>
			                    </thead>
			                    <tbody>
			                        <?php
			                        $total_cash_breakdown = 0;
			                        $total_cash_amount_invoiced = 0;
			                        $total_cash_amount_paid = 0;

			                        $total_insurance_amount_invoiced = 0;
			                        $total_insurance_amount_paid = 0;

			                         $total_professional_fees = 0;
			                        $total_wht = 0;
			                        $net_total = 0;
			                        
			                        
			                        $doctors = $this->hospital_reports_model->get_doctor();
			                         // var_dump($doctors->num_rows());die();
			                         if($doctors->num_rows() > 0)
			                         {
			                            foreach($doctors->result() as $row):
			                                $fname = $row->personnel_fname;
			                                $onames = $row->personnel_onames;
			                                $personnel_id = $row->personnel_id;


			                                $professional_fees_percentage = $row->professional_fees_percentage;



						                  
			                                $cash_amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id,null,1,1);
			                                $cash_amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id,null,1,1);
			                                $cash_amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id,null,1,1);
			                                
			                                $cash_amount_invoiced -= $cash_amount_credits;
			                                $total_cash_amount_invoiced += $cash_amount_invoiced;
			                                $total_cash_amount_paid += $cash_amount_paid;



			                                $insurance_amount_paid = $this->hospital_reports_model->get_personnel_days_payments($personnel_id,null,1,2);
			                                $insurance_amount_invoiced = $this->hospital_reports_model->get_personnel_days_invoices($personnel_id,null,1,2);
			                                $insurance_amount_credits = $this->hospital_reports_model->get_personnel_days_credits($personnel_id,null,1,2);
			                                
			                                $insurance_amount_invoiced -= $insurance_amount_credits;
			                                $total_insurance_amount_invoiced += $insurance_amount_invoiced;
			                                $total_insurance_amount_paid += $insurance_amount_paid;

			                                $total_invoiced_amount = $cash_amount_invoiced + $insurance_amount_invoiced;

			                                $total_paid_amount = $cash_amount_paid + $insurance_amount_paid;

						                    $amount = $total_invoiced_amount * ($professional_fees_percentage/100);
						                    $amount_vat =$amount - ($amount * 0.05);

			                                $wht =($amount * 0.05);

			                                $total_professional_fees += $amount;
			                                $total_wht += $wht;
			                                $net_total += $amount_vat;

						                    $grand_invoices += $total_invoiced_amount;
						                    $grand_collections += $total_paid_amount;

			                                // if($amount_invoiced > 0 OR $amount_paid > 0)
			                                // {
			                                    echo 
			                                        '
			                                       <tr>
			                                            <th>DR. '.strtoupper($onames.' '.$fname).' </th>
			                                            <td>'.number_format($cash_amount_invoiced, 2).'</td>
			                                            <td>'.number_format($insurance_amount_invoiced, 2).'</td>
			                                            <td>'.number_format($total_invoiced_amount, 2).'</td>
			                                            <td>'.number_format($total_paid_amount, 2).'</td>
			                                            <td>'.number_format($amount, 2).'</td>
			                                            <td>'.number_format($wht, 2).'</td>
			                                            <td>'.number_format($amount_vat, 2).'</td>
			                                        </tr>
			                                        ';
			                                // }
			                                
			                                
			                                
			                                
			                            endforeach;

			                           
			                            echo 
			                                    '
			                                     <tr>
			                                        <th>TOTALS</th>
			                                        <th>'.number_format($total_cash_amount_invoiced, 2).'</th>
			                                        <th>'.number_format($total_insurance_amount_invoiced, 2).'</th>

			                                        <th>'.number_format($grand_invoices, 2).'</th>
			                                        <th>'.number_format($grand_collections, 2).'</th>


			                                        <th>'.number_format($total_professional_fees, 2).'</th>
			                                        <th>'.number_format($total_wht, 2).'</th>
			                                         <th>'.number_format($net_total, 2).'</th>
			                                    </tr>
			                                    ';
			                        }
			                       
			                        ?>
			                    </tbody>
			                </table>


			                 <table class="table table-striped table-bordered">
				     			<thead>
				     				<tr>
				     					<th>TOTAL BILLINGS</th>
				     					<th><?php echo number_format($grand_invoices,2)?></th>
				     				</tr>
				     				<tr>
				     					<th>TOTAL COLLECTIONS</th>
				     					<th><?php echo number_format($grand_collections,2)?></th>
				     				</tr>
				     			</thead>
				     		</table>

		       		</div>

		    </div>
		   </div>
 </body>
</html>
