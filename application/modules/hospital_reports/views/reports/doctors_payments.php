<?php

$query = $this->hospital_reports_model->get_doctors_payments($personnel_id);
$period_payment =0;
$debt_payment = 0;
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
		'
			<table class="table table-bordered table-striped table-responsive col-md-12 table-linked">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Payment Type</th>
				  <th>Payment Date</th>
				  <th>Invoice Date</th>
				  <th>Patient</th>
				  <th>Payment Method</th>
				  <th>Type</th>
				  <th>Amount</th>
				  <th>Receipt No</th>
				  <th>Paid to</th>
				  <th>Branch</th>
				  <th>Recorded by</th>
				</tr>
			  </thead>
			  <tbody>
	';
	$total_payments = 0;
	foreach ($query->result() as $row)
	{
		$count++;
		$total_invoiced = 0;
		$payment_date = $row->transaction_date;
		$payment_created = date('jS M Y',strtotime($row->transaction_date));
		$time = date('H:i a',strtotime($row->created_at));
		$visit_id = $row->visit_id;
		$patient_id = $row->patient_id;
		$personnel_id = $row->personnel_id;
		$patient_othernames = $row->patient_othernames;
		$patient_surname = $row->patient_surname;
		$payment_method = $row->payment_method;
		$cr_amount = $row->cr_amount;
		$transaction_code = $row->transaction_code;
		$reference_code = $row->reference_code;
		$transaction_description = $row->transaction_description;
		$transactionClassification = $row->transactionClassification;
		$visit_invoice_number = $row->visit_invoice_number;
		$invoice_date = $row->invoice_date;
		$branch_code = $row->branch_code;
		$created_by = $row->personnel_fname.' '.$row->personnel_onames;

		if(!empty($invoice_date))
		{
			$invoice = date('jS M Y',strtotime($row->invoice_date));
		}
		else
		{
			$invoice ='';
		}
		

		if($payment_date == $invoice_date)
		{
			$type = 'Period Payment';
			$color = 'success';
			$period_payment += $cr_amount;
		}
		else
		{
			$type = 'Debt repayment';
			$color = 'info';
			$debt_payment += $cr_amount;
		}
		
		$total_payments += $cr_amount;

			$result .= 
				'
					<tr>
						<td class="'.$color.'">'.$count.'</td>
						<td class="'.$color.'">'.$type.'</td>
						<td>'.$payment_created.'</td>
						<td>'.$invoice.'</td>
						<td>'.ucwords(strtolower($patient_surname)).'</td>
						<td>'.$payment_method.'</td>
						<td>'.$transactionClassification.'</td>
						<td>'.number_format($cr_amount, 2).'</td>
						<td>'.$reference_code.'</td>
						<td>'.$visit_invoice_number.'</td>
						<td>'.$branch_code.'</td>
						<td>'.$created_by.'</td>
					</tr> 
			';

	}
	$result .= 
				'
				 </tbody>
				 <tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>

						<th></th>
						<th></th>
						<th></th>
						<th>Total</th>
						<th>'.number_format($total_payments, 2).'</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr> 
				 </tfoot>
			';
	
	$result .= 
	'
				 
				</table>
	';
}

else
{
	$result .= "There are no payments";
}
		
		

?>


<div class="row" style="margin-top: 5px;">
	<div class="col-md-12">
	
        <a href="<?php echo site_url().'print-doctors-payments-report'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> PRINT MONTHLY STATEMENT</a>
		
		<div class="panel-body" style="height:70vh;overflow-y:scroll;padding: 0px;">
			<?php echo $result;?>
			
		</div>
	</div>
</div>
<div class="row" style="margin-top: 5px;">
    <div class="col-md-12 center-align">
        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
       	
        	
    </div>
</div>