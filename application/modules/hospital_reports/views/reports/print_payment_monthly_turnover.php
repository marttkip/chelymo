
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 9px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

          
            .padd
            {
                padding:10px;
            }
            td{
            	border: 2px solid #000 !important;
            }
            tr
            {
            	border-top: 2px solid #000;
            }

            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 3px;
				line-height: 1.42857143;
				vertical-align: top;
				border: 2px solid #000;
				border-top: 2px solid #000;
			}
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
          
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                    
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                          
                          <div  class="align-right">
			            	  <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
			            </div>
                        </strong>
                    </div>
                    
                </div>
		       <div class="col-md-12">
		       		<?php

	        		 $search = $this->session->userdata('monthly_turnover_searched');
					if(!empty($search))
					{
						$month_search = $this->session->userdata('monthly_turnover_month');
						$year_search = $this->session->userdata('monthly_turnover_year');


						$todays_date = $year_search.'-'.$month_search.'-01';

						$report_date= date('M Y',strtotime($todays_date));
					}
					else
					{
						$todays_date = date('Y').'-'.date('m').'-01';

						$report_date= date('M Y',strtotime($todays_date));
					}
					?> 
			       	<div class="col-md-12" style="margin-top: 10px">
			       		<h4 class="left-align"><?php echo 'MONTHLY TURNOVER COLLECTIONS SUMMARY';?></h4>
			       		<h4 class="left-align"><?php echo strtoupper($report_date);?></h4>
			       	</div>
		       	
		       		<div class="col-md-12" style="margin-top: 10px">
		       			 <?php
						 $doctors = $this->hospital_reports_model->get_doctor();
						 // var_dump($doctors->num_rows());die();
						 if($doctors->num_rows() > 0)
						 {
							foreach($doctors->result() as $row):
								$fname = $row->personnel_fname;
								$onames = $row->personnel_onames;
								$personnel_id = $row->personnel_id;

								//  $personnel_id = $this->session->userdata($personnel_id);
								$query = $this->hospital_reports_model->get_doctors_payments($personnel_id);
								$period_payment =0;
								$debt_payment = 0;

							endforeach;
						}
						

			         	 ?>
			         	 



			                <table class="table table-striped table-bordered table-hover table-condensed">
			                    <thead>
			                        <th style="width: 5%;">#</th>
			                        <th>PAYMENT TYPE</th>
			                        <th>PAYMENT DATE</th>
			                        <th>INVOICE DATE</th>
			                        <th>PATIENT</th>
			                        <th>PAYMENT METHOD</th>
			                        <th>TYPE</th>
									<th>AMOUNT</th>
									<th>RECEIPT NO.</th>
			                        <th>PAID TO</th>
									<th>BRANCH</th>
									<th>RECORDED BY</th>
			                    </thead>
			                    <tbody>
			                        <?php

			                        $total_payments = 0;
									foreach ($query->result() as $row):
									
										$count++;
										$total_invoiced = 0;
										$payment_date = $row->transaction_date;
										$payment_created = date('jS M Y',strtotime($row->transaction_date));
										$time = date('H:i a',strtotime($row->created_at));
										$visit_id = $row->visit_id;
										$patient_id = $row->patient_id;
										$personnel_id = $row->personnel_id;
										$patient_othernames = $row->patient_othernames;
										$patient_surname = $row->patient_surname;
										$payment_method = $row->payment_method;
										$cr_amount = $row->cr_amount;
										$transaction_code = $row->transaction_code;
										$reference_code = $row->reference_code;
										$transaction_description = $row->transaction_description;
										$transactionClassification = $row->transactionClassification;
										$visit_invoice_number = $row->visit_invoice_number;
										$invoice_date = $row->invoice_date;
										$branch_code = $row->branch_code;
										$created_by = $row->personnel_fname.' '.$row->personnel_onames;
								
										if(!empty($invoice_date))
										{
											$invoice = date('jS M Y',strtotime($row->invoice_date));
										}
										else
										{
											$invoice ='';
										}
										
								
										if($payment_date == $invoice_date)
										{
											$type = 'Period Payment';
											$color = 'success';
											$period_payment += $cr_amount;
										}
										else
										{
											$type = 'Debt repayment';
											$color = 'info';
											$debt_payment += $cr_amount;
										}
										
										$total_payments += $cr_amount;

			                                // if($amount_invoiced > 0 OR $amount_paid > 0)
			                                // {
			                                    echo 
			                                        '
			                                       <tr>
			                                            <th>'.$count.'</th>
			                                            <td>'.$type.'</td>
			                                            <td>'.$payment_created.'</td>
			                                            <td>'.$invoice.'</td>
			                                            <td>'.ucwords(strtolower($patient_surname)).'</td>
			                                            <td>'.$payment_method.'</td>
			                                            <td>'.$transactionClassification.'</td>
			                                            <td>'.number_format($cr_amount, 2).'</td>
														<td>'.$reference_code.'</td>
														<td>'.$visit_invoice_number.'</td>
														<td>'.$branch_code.'</td>
														<td>'.$created_by.'</td>
			                                        </tr>
			                                        ';
			                                // }
			                                
			                                
			                                
			                                
			                            endforeach;

			                           
			                            echo 
			                                    '
			                                     <tr>
			                                        <th></th>
			                                        <th></th>
			                                        <th></th>

			                                        <th></th>
			                                        <th></th>
													<th></th>
			                                        <th>TOTAL</th>
			                                        <th>'.number_format($total_payments, 2).'</th>
													<th></th>
													<th></th>
			                                        <th></th>
			                                        <th></th>
			                                    </tr>
			                                    ';
			                        
			                       
			                        ?>
			                    </tbody>
			                </table>


			                 <!-- <table class="table table-striped table-bordered">
				     			<thead>
				     				<tr>
				     					<th>TOTAL BILLINGS</th>
				     					<th><?php echo number_format($grand_invoices,2)?></th>
				     				</tr>
				     				<tr>
				     					<th>TOTAL COLLECTIONS</th>
				     					<th><?php echo number_format($grand_collections,2)?></th>
				     				</tr>
				     			</thead>
				     		</table> -->

		       		</div>

		    </div>
		   </div>
 </body>
</html>
