<?php

$query = $this->hospital_reports_model->get_doctors_sales($personnel_id);

if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
		'
			<table class="table table-bordered table-striped table-responsive table-linked col-md-12">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Invoice Date</th>
				  <th>Patient No.</th>
				  <th>Patient</th>
				  <th>Category</th>
				  <th>Doctor</th>
				  <th>Preauth Status.</th>
				  <th>Invoice No.</th>
				  <th>Invoice Amount</th>
				  <th>Journal</th>
				  <th>Credit Note</th>
				  <th>Payments.</th>
				  <th>Balance.</th>
				</tr>
			  </thead>
			  <tbody>
	';
	
	// $personnel_query = $this->accounting_model->get_all_personnel();
	$total_waiver = 0;
	$total_payments = 0;
	$total_invoice = 0;
	$total_balance = 0;
	$total_rejected_amount = 0;
	$total_cash_balance = 0;
	$total_insurance_payments =0;
	$total_insurance_invoice =0;
	$total_payable_by_patient = 0;
	$total_payable_by_insurance = 0;
	$total_debit_notes = 0;
	$total_credit_notes= 0;
	$total_journals = 0;
	foreach ($query->result() as $row)
	{
		$total_invoiced = 0;
		$visit_date = date('jS M Y',strtotime($row->transaction_date));
		$visit_time = date('H:i a',strtotime($row->visit_time));
		if($row->visit_time_out != '0000-00-00 00:00:00')
		{
			$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
		}
		else
		{
			$visit_time_out = '-';
		}
		
		$visit_id = $row->visit_id;
		$patient_id = $row->patient_id;
		$personnel_id = $row->personnel_id;
		$dependant_id = $row->dependant_id;
		$strath_no = $row->strath_no;
		$visit_type_id = $row->visit_type;
		$patient_number = $row->patient_number;
		$visit_type = $row->visit_type;
		$visit_table_visit_type = $visit_type;
		$patient_table_visit_type = $visit_type_id;
		$rejected_amount = $row->amount_rejected;
		$visit_invoice_number = $row->reference_code;
		$visit_invoice_id = $row->visit_invoice_id;
		$parent_visit = $row->parent_visit;
		$branch_code = $row->branch_code;
		$preauth_status = $row->preauth_status;
		$invoice_date = $row->invoice_date;

		if(empty($rejected_amount))
		{
			$rejected_amount = 0;
		}
		// $coming_from = $this->reception_model->coming_from($visit_id);
		// $sent_to = $this->reception_model->going_to($visit_id);
		$visit_type_name = $row->payment_type_name;
		$patient_othernames = $row->patient_othernames;
		$patient_surname = $row->patient_surname;
		$patient_date_of_birth = $row->patient_date_of_birth;

		$doctor = $row->personnel_fname;
		$count++;
		$invoice_total = $row->invoice_bill; //$this->accounts_model->get_visit_invoice_total($visit_invoice_id);
		$payments_value = $row->invoice_payments; //$this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
		$credit_note = $row->invoice_credit_note;// $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
		$invoice_journal = $row->invoice_journal;
		// $invoice_total -= $credit_note;
		$balance  = $row->invoice_balance;

		$total_payable_by_patient += $invoice_total;
		$total_payments += $payments_value;
		$total_journals += $invoice_journal;
		$total_balance += $balance;
		$total_credit_notes += $credit_note;
		if(!empty($credit_note))
		{
			// var_dump($visit_invoice_number);die();
		}
		
		if($preauth_status == 1)
		{
			$status = 'Preauth';
			$color = 'warning';
		}
		else if($preauth_status == 2)
		{
			$status = 'Invoiced';
			$color = 'success';
		}
		else
		{
			$status = 'Invoiced';
			$color = '';
		}
		
		$result .= 
			'
				<tr>
					<td class="'.$color.'">'.$count.'</td>
					<td class="'.$color.'">'.$invoice_date.'</td>
					<td class="'.$color.'">'.$patient_number.'</td>
					<td class="'.$color.'">'.ucwords(strtolower($patient_surname).' '.strtolower($patient_othernames)).'</td>
					<td>'.$visit_type_name.'</td>
					<td>'.$doctor.'</td>
					<td>'.$status.'</td>
					<td>'.$visit_invoice_number.'</td>
					<td>'.number_format($invoice_total,2).'</td>
					<td>'.number_format($invoice_journal,2).'</td>
					<td>'.number_format($credit_note,2).'</td>
					<td>'.(number_format($payments_value,2)).'</td>
					<td>'.(number_format($balance,2)).'</td>
				</tr> 
		';
		
	}

	$result .= 
			'
			</tbody>
			<tfoot>
				<tr>
					<th colspan=8> Totals</th>
					<th><strong>'.number_format($total_payable_by_patient,2).'</strong></th>
					<th><strong>'.number_format($total_journals,2).'</strong></th>
					<th><strong>'.number_format($total_credit_notes,2).'</strong></th>
					<th><strong>'.number_format($total_payments,2).'</strong></th>
					<th><strong>'.number_format($total_balance,2).'</strong></td>
				</tr> 
			</tfoot>
		';
	
	$result .= 
	'
				  
				</table>
	';
}

else
{
	$result .= "There are no visits";
}

?>
<div class="row" style="margin-top: 5px;">
	<div class="col-md-12">
		<a href="<?php echo site_url().'print-individual-monthly-billing-report'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> PRINT MONTHLY STATEMENT</a>
		<div class="panel-body" style="height:75vh;overflow-y:scroll;padding:0px !important;">
			<?php echo $result;?>
			
		</div>
	</div>
</div>
<div class="row" style="margin-top: 5px;">
    <div class="col-md-12 center-align">
        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
       	
        	
    </div>
</div>
