
<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 9px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

          
            .padd
            {
                padding:10px;
            }
            td{
            	border: 2px solid #000 !important;
            }
            tr
            {
            	border-top: 2px solid #000;
            }

            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 3px;
				line-height: 1.42857143;
				vertical-align: top;
				border: 2px solid #000;
				border-top: 2px solid #000;
			}
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="padd">
          
                <div class="col-md-12">
                    <div class="col-print-6" style="text-align: left;">
                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                    
                    </div>
                    <div class="col-print-6 " style="text-align: right;">
                        <strong>
                          
                          <div  class="align-right">
			            	  <?php echo $contacts['company_name'];?><br/>
		                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
		                    <?php echo $contacts['location'];?><br/>
                         
			            </div>
                        </strong>
                    </div>
                    
                </div>
		        <div class="col-md-12">
		       		<?php

						$search = $this->session->userdata('monthly_turnover_searched');
						if(!empty($search))
						{
							$month_search = $this->session->userdata('monthly_turnover_month');
							$year_search = $this->session->userdata('monthly_turnover_year');


							$todays_date = $year_search.'-'.$month_search.'-01';

							$report_date= date('M Y',strtotime($todays_date));
						}
						else
						{
							$todays_date = date('Y').'-'.date('m').'-01';

							$report_date= date('M Y',strtotime($todays_date));
						}
					?> 
			       	<div class="col-md-12" style="margin-top: 10px">
			       		<h4 class="left-align"><?php echo 'MONTHLY TURNOVER BILLING SUMMARY';?></h4>
			       		<h4 class="left-align"><?php echo strtoupper($report_date);?></h4>
			       	</div>
		       	
		       		<div class="col-md-12" style="margin-top: 10px">
		       			 <?php
							$grand_invoices = 0;
							$grand_collections = 0;

			         	 ?>
			         	 <?php
			         	 $query = $this->hospital_reports_model->get_doctors_sales($personnel_id);

							if ($query->num_rows() > 0)
							{
								$count = 0;
								
								$result .= 
									'
										<table class="table table-bordered table-striped table-responsive col-md-12">
										  <thead>
											<tr>
											  <th>#</th>
											  <th>Invoice Date</th>
											  <th>Patient No.</th>
											  <th>Patient</th>
											  <th>Category</th>
											  <th>Doctor</th>
											  <th>Preauth Status.</th>
											  <th>Invoice No.</th>
											  <th>Invoice Amount</th>
											  <th>Journal</th>
											  <th>Credit Note</th>
											  <th>Payments.</th>
											  <th>Balance.</th>
											</tr>
										  </thead>
										  <tbody>
								';
								
								// $personnel_query = $this->accounting_model->get_all_personnel();
								$total_waiver = 0;
								$total_payments = 0;
								$total_invoice = 0;
								$total_balance = 0;
								$total_rejected_amount = 0;
								$total_cash_balance = 0;
								$total_insurance_payments =0;
								$total_insurance_invoice =0;
								$total_payable_by_patient = 0;
								$total_payable_by_insurance = 0;
								$total_debit_notes = 0;
								$total_credit_notes= 0;
								$total_journals = 0;
								foreach ($query->result() as $row)
								{
									$total_invoiced = 0;
									$visit_date = date('jS M Y',strtotime($row->transaction_date));
									$visit_time = date('H:i a',strtotime($row->visit_time));
									if($row->visit_time_out != '0000-00-00 00:00:00')
									{
										$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
									}
									else
									{
										$visit_time_out = '-';
									}
									
									$visit_id = $row->visit_id;
									$patient_id = $row->patient_id;
									$personnel_id = $row->personnel_id;
									$dependant_id = $row->dependant_id;
									$strath_no = $row->strath_no;
									$visit_type_id = $row->visit_type;
									$patient_number = $row->patient_number;
									$visit_type = $row->visit_type;
									$visit_table_visit_type = $visit_type;
									$patient_table_visit_type = $visit_type_id;
									$rejected_amount = $row->amount_rejected;
									$visit_invoice_number = $row->reference_code;
									$visit_invoice_id = $row->visit_invoice_id;
									$parent_visit = $row->parent_visit;
									$branch_code = $row->branch_code;
									$preauth_status = $row->preauth_status;
									$invoice_date = $row->invoice_date;

									if(empty($rejected_amount))
									{
										$rejected_amount = 0;
									}
									// $coming_from = $this->reception_model->coming_from($visit_id);
									// $sent_to = $this->reception_model->going_to($visit_id);
									$visit_type_name = $row->payment_type_name;
									$patient_othernames = $row->patient_othernames;
									$patient_surname = $row->patient_surname;
									$patient_date_of_birth = $row->patient_date_of_birth;

									$doctor = $row->personnel_fname;
									$count++;
									$invoice_total = $row->invoice_bill; //$this->accounts_model->get_visit_invoice_total($visit_invoice_id);
									$payments_value = $row->invoice_payments; //$this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
									$credit_note = $row->invoice_credit_note;// $this->accounts_model->get_visit_invoice_credit_notes($visit_invoice_id);
									$invoice_journal = $row->invoice_journal;
									// $invoice_total -= $credit_note;
									$balance  = $row->invoice_balance;

									$total_payable_by_patient += $invoice_total;
									$total_payments += $payments_value;
									$total_journals += $invoice_journal;
									$total_balance += $balance;
									$total_credit_notes += $credit_note;
									if(!empty($credit_note))
									{
										// var_dump($visit_invoice_number);die();
									}
									
										if($preauth_status == 1)
										{
											$status = 'Preauth';
											$color = 'warning';
										}
										else if($preauth_status == 2)
										{
											$status = 'Invoiced';
											$color = 'success';
										}
										else
										{
											$status = 'Invoiced';
											$color = '';
										}
									
									$result .= 
										'
											<tr>
												<td class="'.$color.'">'.$count.'</td>
												<td class="'.$color.'">'.$invoice_date.'</td>
												<td class="'.$color.'">'.$patient_number.'</td>
												<td class="'.$color.'">'.ucwords(strtolower($patient_surname).' '.strtolower($patient_othernames)).'</td>
												<td>'.$visit_type_name.'</td>
												<td>'.$doctor.'</td>
												<td>'.$preauth_status.'</td>
												<td>'.$visit_invoice_number.'</td>
												<td>'.number_format($invoice_total,2).'</td>
												<td>'.number_format($invoice_journal,2).'</td>
												<td>'.number_format($credit_note,2).'</td>
												<td>'.(number_format($payments_value,2)).'</td>
												<td>'.(number_format($balance,2)).'</td>
											</tr> 
									';
									
								}

								$result .= 
										'
											<tr>
												<td colspan=8> Totals</td>
												<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
												<td><strong>'.number_format($total_journals,2).'</strong></td>
												<td><strong>'.number_format($total_credit_notes,2).'</strong></td>
												<td><strong>'.number_format($total_payments,2).'</strong></td>
												<td><strong>'.number_format($total_balance,2).'</strong></td>
											</tr> 
									';
								
								$result .= 
								'
											  </tbody>
											</table>
								';
							}

							else
							{
								$result .= "There are no visits";
							}
							echo $result;
			         	 ?>

		       		</div>

		    	</div>
		</div>
 	</body>
</html>
