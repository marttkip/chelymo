
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Active branch: <?php echo $branch_name;?></h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-debtors-report", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <!-- <label class="col-lg-4 control-label">Visit Type: </label> -->
                        
                        <div class="col-lg-12">
                            <select class="form-control" name="visit_type_id">
                            	<option value="">---Select Visit Type---</option>
                                <?php
                                    if(count($type) > 0){
                                        foreach($type as $row):
                                            $type_name = $row->visit_type_name;
                                            $type_id= $row->visit_type_id;
                                                ?><option value="<?php echo $type_id; ?>" ><?php echo $type_name ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Type </label>
                        <div class="col-lg-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios1" type="radio" name="preauth_status" value="0"  >
                                    Normal
                                </label>
                            </div>
                        </div>
                        
                        <div class="col-lg-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios1" type="radio" name="preauth_status" value="1">
                                    Preauth
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Invoice Number: </label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="invoice_number" placeholder="Invoice">
                        </div>
                    </div>

                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Visit Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    
                   
                    
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Visit Date To: </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-md-1">
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>
