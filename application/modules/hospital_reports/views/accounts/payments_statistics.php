
    <div class="col-md-12 col-sm-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown</h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                
                    <table class="table table-striped table-hover table-condensed">
                        <tbody>
                            <?php
                            $total_cash_breakdown = 0;
                          
                            if($payment_methods->num_rows() > 0)
                            {
                                foreach($payment_methods->result() as $res)
                                {
                                    $method_name = $res->payment_method;
                                    $payment_method_id = $res->payment_method_id;
                                    $total = 0;
                                    
                                    if($normal_payments->num_rows() > 0)
                                    {
                                        foreach($normal_payments->result() as $res2)
                                        {
                                            $payment_method_id2 = $res2->payment_method_id;
                                           
                                        
                                            if($payment_method_id == $payment_method_id2)
                                            {
                                               
                                                $total += $res2->amount_paid;
                                            }
                                        }
                                    }
                                    
                                    $total_cash_breakdown += $total;
                                
                                    echo 
                                    '
                                    <tr>
                                        <th>'.$method_name.'</th>
                                        <td>'.number_format($total, 2).'</td>
                                    </tr>
                                    ';
                                }
                                
                                echo 
                                '
                                <tr>
                                    <th>Total</th>
                                    <td>'.number_format($total_cash_breakdown, 2).'</td>
                                </tr>
                                ';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
      
        </section>
    </div>
