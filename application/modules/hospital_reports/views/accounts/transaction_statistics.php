<div class="row statistics">
    
    <div class="col-md-12 col-sm-12">
    	 <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Transaction breakdown for <?php echo $total_visits;?> patients</h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <div class="row">
                   
                    <div class="col-md-3">
						<?php
                        	$total_invoices_revenue = $this->hospital_reports_model->get_visit_invoice_totals();

                            $total_invoices_revenue -= $this->hospital_reports_model->get_visit_credits_totals();
                            $total_payments_revenue = $this->hospital_reports_model->get_visit_payment_totals();
                      
                            $all_payments_period =  0;//$this->hospital_reports_model->all_payments_period();
                            $receivable = $all_payments_period - $total_payments_revenue;
                            $total_rejected_amounts = 0;// $this->hospital_reports_model->get_rejected_amounts();
                            // $total_invoices_revenue -= $total_waiver_revenue;


                            // get payments done today and visits not for today

                            // var_dump($total_invoices_revenue);die();


                            if($receivable < 0)
                            {
                                $receivable = ($receivable);
                            }
                            // var_dump($credit_note);die();
                        ?>
                        <?php
                        // var_dump($receivable);die();
                        $patients = $this->hospital_reports_model->get_patients_visits(1);

                        // $returning_patients = $this->accounting_model->get_patients_visits(0);
                        ?>
                        <h5>VISIT TYPE BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                 <tr>
                                    <th>TOTAL INVOICES</th>
                                    <td><?php echo number_format($total_invoices_revenue, 2);?></td>
                                </tr>
                                
                               <!-- <tr>
                                    <th>TOTAL DEBITS</th>
                                    <td><?php echo number_format($total_debits_revenue, 2);?></td>
                                </tr> -->
                                <tr>
                                    <th>PERIOD COLLECTION</th>
                                    <td>(<?php echo number_format($total_payments_revenue, 2);?>)</td>
                                </tr>
                                 <!-- <tr>
                                    <th>TOTAL WAIVERS</th>
                                    <td>(<?php echo number_format($total_waiver_revenue, 2);?>)</td>
                                </tr> -->

                                <tr>
                                    <th>DEBT BALANCE</th>
                                    <td><?php echo number_format(($total_invoices_revenue +$total_debits_revenue - $total_payments_revenue - $total_waiver_revenue), 2);?></td>
                                </tr>
                            </tbody>
                        </table>

                        <?php
                        $cash_invoice = $this->hospital_reports_model->get_visit_invoice_totals(1);
                        $insurance_invoice = $this->hospital_reports_model->get_visit_invoice_totals(2);
                        ?>
                        <h5>MODE BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                 <tr>
                                    <th>TOTAL CASH INVOICES</th>
                                    <td><?php echo number_format($cash_invoice, 2);?></td>
                                </tr>
                                
                               <!-- <tr>
                                    <th>TOTAL DEBITS</th>
                                    <td><?php echo number_format($total_debits_revenue, 2);?></td>
                                </tr> -->
                                <tr>
                                    <th>TOTAL INSURANCE BILLINGS</th>
                                    <td><?php echo number_format($insurance_invoice, 2);?></td>
                                </tr>
                                
                                <tr>
                                    <th>TOTAL BILLINGS</th>
                                    <td><?php echo number_format(($cash_invoice +$insurance_invoice), 2);?></td>
                                </tr>
                            </tbody>
                        </table>
                   
                    

                       
                        
                        <div class="clearfix"></div>
            		</div>
                    <!-- End Transaction Breakdown -->
                   
                   
                    <div class="col-md-2">
                       
                          <h5>VISIT  BREAKDOWN</h5>
                            <table class="table table-striped table-hover table-condensed">
                                <tbody>
                                    <tr>
                                        <th>NEW PATIENTS</th>
                                        <td><?php echo $patients['total_new'];?></td>
                                    </tr>
                                    <tr>
                                        <th>RETURNING PATIENTS</th>
                                        <td><?php echo $patients['total_old'];?></td>
                                    </tr>
                                     <tr>
                                        <th>TOTAL PATIENTS</th>
                                        <td><?php echo $patients['total_new'] + $patients['total_old'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                   
                   <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-4 center-align">
                                <h4>INVOICED</h4>
                                <h3>Ksh <?php echo number_format($total_invoices_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                 <h4>PERIOD PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                 <h4>BALANCE</h4>
                                <h3>Ksh <?php echo number_format($total_invoices_revenue  - $total_payments_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>
                      
                     
                    </div>
                </div>
          	</div>
		</section>
    </div>
</div>