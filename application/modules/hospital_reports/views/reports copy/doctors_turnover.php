<!-- search -->
<?php echo $this->load->view('search_doctors_turnover', '', TRUE);?>
<!-- end search -->

 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
            	  	<a href="<?php echo site_url().'print-doctors-turnover'?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Turnover</a>
            	  	<!-- <a href="<?php echo site_url().'export-doctors-turnover'?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-arrow-up"></i> Export Turnover</a> -->
            	</div>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '';
		$search = $this->session->userdata('doctors_turnover_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_reports/close_doctors_turnover_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		// var_dump($query);die();
		//if users exist display them
		//if users exist display them
		if(!empty($query))
		{
			if ($query->num_rows() > 0 )
			{
				$count = 1;
				
				$result .= '
							<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
							  <tbody>
							';
				
				// $personnel_query = $this->accounting_model->get_all_personnel();
				$total_waiver = 0;
				$total_payments = 0;
				$total_invoice = 0;
				$grand_total_balance = 0;
				$total_rejected_amount = 0;
				$total_cash_balance = 0;
				$total_insurance_payments =0;
				$total_insurance_invoice =0;
				$total_payable_by_patient = 0;
				$total_payable_by_insurance = 0;
				$db_personnel_id = 0;
				
				foreach ($query->result() as $value)
				{

					$personnel_fname = $value->personnel_fname;
					$personnel_onames = $value->personnel_onames;
					$personnel_id = $value->doctor;
					$personnel_percentage = $value->personnel_percentage;
					

					$result .= 
							'
								<tr>
									<th colspan="10">  '.$personnel_onames.' '.$personnel_fname.'</th>
									
								</tr> 
						';
					$result .= 
							'
							
									<tr>
									  <th>#</th>
									  <th>Date</th>
									  <th>Name</th>
									  <th>INV No</th>
									  <th>Item</th>
									  <th>Service</th>
									  <th>Qty</th>
									  <th>Sale Price</th>
									  <th>Amount</th>
									  <th>Balance</th>
									</tr>
						';


					// $personnel_id = $row->
						
					$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.visit_id = visit.visit_id AND visit.visit_delete = 0 AND visit.personnel_id = personnel.personnel_id AND service.service_id = service_charge.service_id AND visit.personnel_id  ='.$personnel_id;
					$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel,visit,service';
					$visit_search = $this->session->userdata('doctors_turnover_search');
					// var_dump($visit_search);die();
					if(!empty($visit_search))
					{
						$where .= $visit_search;
						
					}
					else
					{
						$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
						$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
						$visit_invoices = ' AND visit.visit_date = \''.date('Y-m-d').'\'';
						$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

						$this->session->set_userdata('visit_invoices', $visit_invoices);
						$this->session->set_userdata('visit_payments', $visit_payments);

						$where .= '';
					}
					
					
					$query_items = $this->hospital_reports_model->get_all_daily_sales($table, $where);
					$total_invoiced = 0;
					$total_balance = 0;
					$total_units = 0;
					$total_invoice_amount = 0;
					if($query_items->num_rows() > 0)
					{
						foreach ($query_items->result() as $row)
						{
							$visit_date = date('d.m.Y',strtotime($row->created));
							$visit_time = date('H:i a',strtotime($row->visit_time));
							if($row->visit_time_out != '0000-00-00 00:00:00')
							{
								$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
							}
							else
							{
								$visit_time_out = '-';
							}
							
							$visit_id = $row->visit_id;
							$patient_id = $row->patient_id;
							$personnel_id = $row->doctor;
							$dependant_id = $row->dependant_id;
							$strath_no = $row->strath_no;
							$visit_type_id = $row->visit_type;
							$patient_number = $row->patient_number;
							$visit_type = $row->visit_type;
							$visit_table_visit_type = $visit_type;
							$patient_table_visit_type = $visit_type_id;
							$rejected_amount = $row->amount_rejected;
							$invoice_number = $row->invoice_number;
							$personnel_onames = $row->personnel_onames;
							$personnel_fname = $row->personnel_fname;
							$parent_visit = $row->parent_visit;

							if(empty($rejected_amount))
							{
								$rejected_amount = 0;
							}


							$visit_type_name = $row->visit_type_name;
							$patient_othernames = $row->patient_othernames;
							$patient_surname = $row->patient_surname;
							$patient_first_name = $row->patient_first_name;
							$patient_date_of_birth = $row->patient_date_of_birth;

							$service_charge_name = $row->service_charge_name;
							$service_charge_code = $row->service_charge_code;
							$visit_charge_amount = $row->visit_charge_amount;
							$visit_charge_notes = $row->visit_charge_notes;
							$service_name = $row->service_name;

							$visit_charge_units = $row->visit_charge_units;
							$visit_invoice_number = $row->visit_invoice_number;

							if($visit_type_id == 1)
							{
								$type = 'Non Insurance';
							}
							else
							{
								$type = 'Insurance';
							}
							// $payments_value = $this->accounts_model->total_payments($visit_id);

							$invoice_amount = $visit_charge_amount * $visit_charge_units;

							$total_balance += $visit_charge_amount * $visit_charge_units;

							
							$total_invoiced += $visit_charge_amoun * $visit_charge_units;


							if(!empty($visit_charge_notes))
							{
								// var_dump($visit_charge_notes);die();
								$service_charge_name .= ' '.$visit_charge_notes;
							}
							$total_units += $visit_charge_units;
							$total_invoice_amount += $invoice_amount;
							$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$visit_date.'</td>
										<td>'.$patient_surname.' '.$patient_othernames.' '.$patient_first_name.'</td>
										<td>'.$visit_invoice_number.'</td>
										<td>'.$service_charge_name.'</td>
										<td>'.$service_name.'</td>
										<td>'.number_format($visit_charge_units,2).'</td>
										<td>'.number_format($visit_charge_amount,2).'</td>
										<td>'.number_format($invoice_amount,2).'</td>
										<td>'.number_format($total_balance,2).'</td>
									</tr> 
							';
						
							$count++;
							$db_personnel_id = $personnel_id;
					
						
						}
					}
					$grand_total_balance += $total_balance;
					$result .= 
							'
							
									<tr>
									  <th colspan="6">Total '.$personnel_fname.' '.$personnel_onames.' </th>
									  <th>'.number_format($total_units,2).'</th>
									  <th></th>
									  <th>'.number_format($total_balance,2).'</th>
									  <th>'.number_format($total_balance,2).'</th>
								
									</tr>
						';

				}
				
				$result .= 
							'
							
									<tr>
									  <th colspan="10"> </th>
								
									</tr>
						';
				if($personnel_percentage > 0)
				{
					$thirty_percent = $total_balance * ($personnel_percentage/100);
				}
				else
				{
					$thirty_percent = 0;
				}
				

				$withholding = 0;
				if($thirty_percent >= 24000 )
				{

					if($thirty_percent > 0)
					{
						$withholding = $thirty_percent * 0.05;
					}
					else
					{
						$withholding = 0;
					}
					
				}

				$net_pay = $thirty_percent - $withholding;
				$result .= 
							'
							
									<tr>
										<td></td>
										<td >100% Turnover</td>
										<td colspan="7"></td>
										<th >'.number_format($total_balance,2).'</th>
										<td ></td>
									</tr>
									<tr>
										<td></td>
										<td >'.$personnel_percentage.'% Professional fees</td>
										<td colspan="7"></td>
										<td >'.number_format($thirty_percent,2).'</td>
										<td ></td>
									</tr>
									<tr>
										<td></td>
										<td >5% Withholding tax</td>
										<td colspan="7"></td>
										<td >'.number_format($withholding,2).'</td>
										<td ></td>
									</tr>
									<tr>
										<td></td>
										<th >NET PAY</th>
										<td colspan="7"></td>
										<th >'.number_format($net_pay,2).'</th>
										<td ></td>
									</tr>
						';
				
				$result .= 
				'
							  </tbody>
							</table>
						

				';
			


			}
			
			else
			{
				$result .= "No results found";
			}
		}
		else
		{
			$result = "No results ";
		}
		
		echo $result;
		
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>
