        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Active branch: <?php echo $branch_name;?></h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-doctors-turnover-report", array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	<div class="col-md-3">
            		<div class="form-group">
						<label class="col-lg-4 control-label">Doctor: </label>
						
						<div class="col-lg-8">
							 <select name="doctor_id" class="form-control" id="doctor_id" required="required">
								<option value="">----Select a Doctor----</option>
								<?php
									$doctor = $this->reception_model->get_doctor();	
									if(count($doctor) > 0){
										foreach($doctor as $row):
											$fname = $row->personnel_fname;
											$onames = $row->personnel_onames;
											$personnel_id = $row->personnel_id;
											
											if($personnel_id == set_value('personnel_id'))
											{
												echo "<option value='".$personnel_id."' selected='selected'>".$onames." </option>";
											}
											
											else
											{
												echo "<option value='".$personnel_id."'>".$onames." </option>";
											}
										endforeach;
									}
								?>
							</select>
						</div>
					</div>
            	</div>
                <div class="col-md-4">
                     <div class="form-group">
                        <label class="col-lg-4 control-label">Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_from" placeholder="Visit Date From" autocomplete="off">
                            </div>
                        </div>
                    </div>                

                </div>
                 <div class="col-md-4">
                     <div class="form-group">
                        <label class="col-lg-4 control-label">Date To : </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date_to" placeholder="Visit Date To" autocomplete="off">
                            </div>
                        </div>
                    </div>                

                </div>
                                
                
                <div class="col-md-1">
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>