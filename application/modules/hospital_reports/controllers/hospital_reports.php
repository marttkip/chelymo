<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/accounts/controllers/accounts.php";
error_reporting(0);
class Hospital_Reports extends accounts
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('hospital_reports/hospital_reports_model');
		$this->load->model('accounts/accounts_model');
		// $this->load->model('nurse/nurse_model');
		// $this->load->model('pharmacy/pharmacy_model');
		// $this->load->model('admin/dashboard_model');
	}


	
	public function debtors()
	{
		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;
		
		// $where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.close_card <> 2 AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL)';
		// $table = 'visit, patients, visit_type';

		$where = 'visit_invoice.patient_id = patients.patient_id AND visit_invoice.visit_invoice_delete = 0 AND visit.visit_id = visit_invoice.visit_id';
		
		$table = 'visit_invoice,patients,visit';


		$visit_search = $this->session->userdata('debtors_search_query');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND payments.payment_date = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('visit_invoices', $visit_invoices);
			// $this->session->set_userdata('debtors_search_query', $visit_invoices);
			$this->session->set_userdata('visit_payments', $visit_payments);
		


			$where .= '';

		}

		$branch_id = $this->session->userdata('branch_id');

		if(!empty($branch_id))
		{
			$where .= ' AND visit.branch_id = '.$branch_id;
		}
		

		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital-reports/revenue-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 30;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();

		$query = $this->hospital_reports_model->get_all_patients_invoices($table, $where, $config["per_page"], $page, 'ASC');
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['search'] = $visit_search;
		$v_data['total_patients'] = $config['total_rows'];		
		
		$page_title = $this->session->userdata('page_title');
		if(empty($page_title))
		{
			$page_title = 'All transactions for '.date('Y-m-d');
		}
		// $v_data['normal_payments'] = $this->hospital_reports_model->get_normal_payments($where, $table, 'cash');
		// $v_data['payment_methods'] = $this->hospital_reports_model->get_payment_methods($where, $table, 'cash');
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['branches'] = $this->hospital_reports_model->get_all_active_branches();
		$v_data['total_visits'] = $config['total_rows'];


		
		$v_data['module'] = $module;

		// var_dump($v_data);die();
		
		$data['content'] = $this->load->view('accounts/invoices', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function search_debtors_report()
	{
		$visit_type_id = $visit_type_idd = $this->input->post('visit_type_id');
		$branch_id = $branch_idd= $this->input->post('branch_id');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$invoice_number = $this->input->post('invoice_number');
		// $patient_number = $this->input->post('patient_number');
		// $patient_name = $this->input->post('patient_name');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($visit_type_id))
		{
			$visit_type = $visit_type_id;
			$visit_type_id = ' AND visit_invoice.bill_to = '.$visit_type_id.' ';


			
			$this->db->where('visit_type_id', $visit_type_idd);
			$query = $this->db->get('visit_type');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->visit_type_name.' ';
			}
		}
		
		// if(!empty($patient_number))
		// {
		// 	$patient_number = ' AND patients.patient_number LIKE \'%'.$patient_number.'%\' ';
			
		// 	$search_title .= 'Patient number. '.$patient_number;
		// }
		
		if(!empty($branch_id))
		{
			$branch_id = ' AND visit.branch_id = '.$branch_id.' ';
			
			$this->db->where('branch_id', $branch_idd);
			$query = $this->db->get('branch');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->branch_name.' ';
			}
		}
		
		//date filter for cash report
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_payments = ' AND payments.payment_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit_invoice.created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'Visit date from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_payments = ' AND payments.payment_date = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.$visit_date_from.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_payments = ' AND payments.payment_date = \''.$visit_date_to.'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.$visit_date_to.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_payments = '';
			$visit_invoices = '';
		}

		$surname = '';

		// $visit_invoices = '';
		if(!empty($invoice_number))
		{
			$invoice_number = ' AND visit_invoice.visit_invoice_number LIKE \''.$invoice_number.'\'';
			$search_title .= 'Visit Invoice of '.$visit_invoice_number.' ';
		}

		
		
		$search = $visit_type_id.$visit_invoices.$branch_id.$invoice_number;
		
		// var_dump($search);die();
		$this->session->set_userdata('debtors_search_query', $search);
		$this->session->set_userdata('visit_invoices', $visit_invoices);
		$this->session->set_userdata('drb_payments_search', $visit_payments);
		$this->session->set_userdata('visit_type_id', $visit_type_id);
		$this->session->set_userdata('visit_type', $visit_type);
		$this->session->set_userdata('patient_number', $patient_number);
		$this->session->set_userdata('search_title', $search_title);
		
		redirect('hospital-reports/revenue-report');
	}

	public function close_reports_search()
	{
		$this->session->unset_userdata('debtors_search_query');
		$this->session->unset_userdata('visit_invoices');
		$this->session->unset_userdata('drb_payments_search');
		$this->session->unset_userdata('visit_type_id');
		$this->session->unset_userdata('visit_type');
		$this->session->unset_userdata('patient_number');
		$this->session->unset_userdata('search_title');

		redirect('hospital-reports/revenue-report');

	}
	
	public function export_debtors()
	{
		$this->hospital_reports_model->export_debtors();
	}






	// payments 


	public function payments_report()
	{

		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		
		$where = 'payments.payment_method_id = payment_method.payment_method_id AND visit_invoice.visit_id = visit.visit_id AND payments.payment_type = 1 AND visit.visit_delete = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id = visit_invoice.visit_invoice_id AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND payments.cancel = 0';
		
		$table = 'payments,payment_item,visit_invoice ,visit, patients, visit_type, payment_method';
		$visit_search = $this->session->userdata('cash_report_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		else
		{
			$where .=' AND payments.payment_created = "'.date('Y-m-d').'"';
		}



		$branch_id = $this->session->userdata('branch_id');

		if(!empty($branch_id))
		{
			$where .= ' AND visit.branch_id = '.$branch_id;
		}
		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital-reports/collections-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->hospital_reports_model->get_all_payments($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['search'] = $visit_search;
		$v_data['total_patients'] = $config['total_rows'];
		$v_data['total_payments'] = $this->hospital_reports_model->get_total_cash_collection($where, $table, 'cash');
		
		//all normal payments
		$where2 = $where.' AND payments.payment_type = 1';
		$v_data['normal_payments'] = $this->hospital_reports_model->get_normal_payments($where2, $table, 'cash');
		$v_data['payment_methods'] = $this->hospital_reports_model->get_payment_methods($where2, $table, 'cash');
		
		//normal payments
		$where2 = $where.' AND payments.payment_type = 1';
		$v_data['total_cash_collection'] = $this->hospital_reports_model->get_total_cash_collection($where2, $table, 'cash');

		$where4 = 'payments.payment_method_id = payment_method.payment_method_id AND payments.visit_id = visit.visit_id  AND visit.visit_delete = 0  AND visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND payments.cancel = 0 AND payments.payment_type = 3 AND payments.payment_created = "'.date('Y-m-d').'"';
		$v_data['total_waiver'] = $this->hospital_reports_model->get_total_cash_collection($where4, $table, 'cash');
		
		//count outpatient visits
		$where2 = $where.' AND patients.inpatient = 0';
		$v_data['outpatients'] = $this->reception_model->count_items($table, $where2);
		
		//count inpatient visits
		$where2 = $where.' AND patients.inpatient = 1';
		$v_data['inpatients'] = $this->reception_model->count_items($table, $where2);
		
		$page_title = $this->session->userdata('cash_search_title');
		
		if(empty($page_title))
		{
			$page_title = 'Cash report for '.date('Y-m-d');
		}



		
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		
		// $v_data['services_query'] = $this->hospital_reports_model->get_all_active_services();
		$v_data['branches'] = $this->hospital_reports_model->get_all_active_branches();
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('accounts/payments', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_cash_reports()
	 {
		$visit_type_id = $this->input->post('visit_type_id');
		$personnel_id = $this->input->post('personnel_id');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
			
			$this->db->where('visit_type_id', $visit_type_id);
			$query = $this->db->get('visit_type');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->visit_type_name.' ';
			}
		}
		
		if(!empty($personnel_id))
		{
			$personnel_id = ' AND visit.personnel_id = '.$personnel_id.' ';
			
			$this->db->where('personnel_id', $personnel_id);
			$query = $this->db->get('personnel');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->personnel_fname.' '.$row->personnel_onames.' ';
			}
		}

		$petty_cash_date ='';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND payments.payment_created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$petty_cash_date = ' AND petty_cash.petty_cash_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND payments.payment_created = \''.$visit_date_from.'\'';
			$petty_cash_date = ' AND petty_cash.petty_cash_date = \''.$visit_date_from.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND payments.payment_created = \''.$visit_date_to.'\'';
			$petty_cash_date = ' AND petty_cash.petty_cash_date = \''.$visit_date_to.'\'';
			$search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_date = '';
		}
		
		$search = $visit_type_id.$visit_date.$personnel_id;
		$this->session->unset_userdata('cash_report_search');
		
		$this->session->set_userdata('cash_report_search', $search);
		$this->session->set_userdata('today_cash_date_search', $visit_date);
		$this->session->set_userdata('petty_cash_date_search', $petty_cash_date);
		$this->session->set_userdata('cash_search_title', $search_title);
		
		redirect('hospital-reports/collections-report');
	}

	public function close_cash_search()
	{
		$this->session->unset_userdata('cash_report_search');
		$this->session->unset_userdata('petty_cash_date_search');
		$this->session->unset_userdata('today_cash_date_search');
		$this->session->unset_userdata('cash_search_title');
		
		redirect('hospital-reports/collections-report');
	}

	public function export_cash_report()
	{
		$this->hospital_reports_model->export_cash_report();
	}


	// general report


	public function general_report()
	{
		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;
		
		$branch_session = $this->session->userdata('branch_id');
		if($branch_session == 0)
		{
			$branch = '';
		}
		else
		{
			$branch = ' AND v_transactions_by_date.branch_id = '.$branch_session;
		}
		// $where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.close_card <> 2 AND (visit.parent_visit = 0 OR visit.parent_visit IS NULL)';
		// $table = 'visit, patients, visit_type';

		$where = '(v_transactions_by_date.transactionCategory = "Revenue" OR v_transactions_by_date.transactionCategory = "Revenue Payment") AND patients.patient_id = v_transactions_by_date.patient_id'.$branch;
		
		$table = 'v_transactions_by_date,patients';


		$visit_search = $this->session->userdata('general_report_search');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			
			
		}
		else
		{
			$where .= ' AND v_transactions_by_date.transaction_date = "'.date('Y-m-d').'" ';
		
			// $visit_payments = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			// $visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.date('Y-m-d').'\'';
			// $search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			// $this->session->set_userdata('general_report_search', $visit_invoices);
		


		}
		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital-reports/general-report';
		$config['total_rows'] = $this->reception_model->other_count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->hospital_reports_model->get_general_report_data($table, $where, $config["per_page"], $page, 'ASC');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['search'] = $visit_search;
		$v_data['total_patients'] = $config['total_rows'];		
		
		$page_title = $this->session->userdata('page_title');
		if(empty($page_title))
		{
			$page_title = 'General Report for '.date('Y-m-d');
		}
		$v_data['normal_payments'] = $this->hospital_reports_model->get_normal_payments($where, $table, 'cash');
		$v_data['payment_methods'] = $this->hospital_reports_model->get_payment_methods($where, $table, 'cash');
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['branches'] = $this->hospital_reports_model->get_all_active_branches();
		$v_data['total_visits'] = $config['total_rows'];

		
		$v_data['module'] = $module;
		
		$data['content'] = $this->load->view('accounts/general_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_general_report()
	{

		// $visit_type_id = $visit_type_idd = $this->input->post('visit_type_id');
		// $branch_id = $branch_idd= $this->input->post('branch_id');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		// $branch_code = $this->input->post('branch_code');
		// $patient_number = $this->input->post('patient_number');
		$patient_name = $this->input->post('patient_name');
		// $this->session->set_userdata('search_branch_code', $branch_code);
		
		$search_title = 'Showing reports for: ';
		
		
		
		
		//date filter for cash report
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_payments = ' AND v_transactions_by_date.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			$search_title .= 'Visit date from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_from.'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_from.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_from)).' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_payments = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_to.'\'';
			$visit_invoices = ' AND v_transactions_by_date.transaction_date = \''.$visit_date_to.'\'';
			$search_title .= 'Visit date of '.date('jS M Y', strtotime($visit_date_to)).' ';
		}
		
		else
		{
			$visit_payments = '';
			$visit_invoices = '';
		}

		if(!empty($patient_name))
		{
			$search_title .= ' Patient Name <strong>'.$patient_name.'</strong>';
			$surnames = explode(" ",$patient_name);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= 'patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\'';
				}
				
				else
				{
					$surname .= 'patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
			// var_dump($surname);die();
		}
		
		else
		{
			$surname = '';
		}
		

		
		// var_dump($visit_invoices);die();
		$search = $visit_invoices.$surname;
		
		$this->session->set_userdata('general_report_search', $search);
		$this->session->set_userdata('general_search_title', $search_title);
		
		redirect('hospital-reports/general-report');
	}
	public function close_general_reports_search()
	{
		$this->session->unset_userdata('general_report_search');
		$this->session->unset_userdata('general_search_title');

		redirect('hospital-reports/general-report');

	}
	public function export_general_report()
	{
		$this->accounting_model->export_general_report();

	}


	// doctors Turn over



	public function doctors_turnover()
	{


		$module = NULL;
		

		$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id';
		$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel';
		$visit_search = $this->session->userdata('doctors_turnover_search');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->hospital_reports_model->get_all_daily_sales($table, $where,1);
			// var_dump($query);die();
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('drb_search', $visit_invoices);
			$this->session->set_userdata('drb_payments_search', $visit_payments);
		
			$query = NULL;
			
		}
		
		
		
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		$v_data['total_patients'] = 1;		
	
		// $page_title = $this->session->userdata('page_title');
		// if(empty($page_title))
		// {
		// 	$page_title = 'Daily Records Report for '.date('Y-m-d');
		// }
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Doctor\'s Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		
		$v_data['module'] = $module;

		$v_data['month'] = $this->accounts_model->get_months();
		
		$data['content'] = $this->load->view('reports/doctors_turnover', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}
	public function search_doctors_turnover()
	{
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$doctor_id = $this->input->post('doctor_id');
		$personnel_id = $this->input->post('personnel_id');
		$search_title = '';
		
		
		if(!empty($month))
		{
			$month = $month;
		}
		else
		{
			$month = NULL;
		}

		if(!empty($year))
		{
			$year = $year;
		}
		else
		{
			$year = NULL;
		}

		if(!empty($personnel_id))
		{
			$personnel_id = ' AND visit.personnel_id = '.$personnel_id.' ';
			
			$this->db->where('personnel_id', $personnel_id);
			$query = $this->db->get('personnel');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->personnel_fname.' '.$row->personnel_onames.' ';
			}
		}
		
		
		

		// $search = $visit_invoices.$dentist_id;

		var_dump($month);die();
		$this->session->set_userdata('doctors_turnover_searched', TRUE);
		$this->session->set_userdata('doctors_turnover_month', $month);
		$this->session->set_userdata('doctors_turnover_year', $year);
		$this->session->set_userdata('doctors_turnover_personnel', $personnel_id);
		
		redirect('hospital-reports/doctors-turnover');
	}

	public function close_doctors_turnover_search()
	{
		$this->session->unset_userdata('doctors_turnover_searched');
		$this->session->unset_userdata('doctors_turnover_month');
		$this->session->unset_userdata('doctors_turnover_year');
		$this->session->unset_userdata('doctors_turnover_personnel');
		
		redirect('hospital-reports/doctors-turnover');
	}

	public function print_doctors_turnover_report()
	{

		$module = NULL;
		
		$v_data['branch_name'] = $branch_name;



		$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id';
		$table = 'patients, visit_type,visit_invoice,visit_charge,service_charge,personnel';
		$visit_search = $this->session->userdata('doctors_turnover_search');
		// var_dump($visit_search);die();
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->hospital_reports_model->get_all_daily_sales($table, $where,1);
			// var_dump($query);die();
			
		}
		else
		{
			$where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			$visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
			$visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			$search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			$this->session->set_userdata('drb_search', $visit_invoices);
			$this->session->set_userdata('drb_payments_search', $visit_payments);
		
			$query = NULL;
			
		}
		
		
		
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		$v_data['total_patients'] = 1;		
	
		// $page_title = $this->session->userdata('page_title');
		// if(empty($page_title))
		// {
		// 	$page_title = 'Daily Records Report for '.date('Y-m-d');
		// }
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Doctor\'s Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();

		$v_data['contacts'] = $this->site_model->get_contacts();


		$v_data['total_visits'] = 20;//$config['total_rows'];
		
		$v_data['module'] = $module;
		
		$this->load->view('reports/print_doctors_turnover', $v_data);

	}

	public function monthly_sales_summary()
	{


		$data['title'] = $v_data['title'] = 'Monthly Sales Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		
		// var_dump("sdakdha");die();

		$v_data['month'] = $this->accounts_model->get_months();
		
		$data['content'] = $this->load->view('reports/monthly_sales_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}



	public function search_monthly_turnover()
	{
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$doctor_id = $this->input->post('doctor_id');
		$search_title = '';
		
		
		if(!empty($month))
		{
			$month = $month;
		}
		else
		{
			$month = NULL;
		}

		if(!empty($year))
		{
			$year = $year;
		}
		else
		{
			$year = NULL;
		}
		
		
		

		// $search = $visit_invoices.$dentist_id;

		// var_dump($search);die();
		$this->session->set_userdata('monthly_turnover_searched', TRUE);
		$this->session->set_userdata('monthly_turnover_month', $month);
		$this->session->set_userdata('monthly_turnover_year', $year);
		
		redirect('hospital-reports/monthly-sales-summary');
	}

	public function close_monthly_turnover_search()
	{
		$this->session->unset_userdata('monthly_turnover_searched');
		$this->session->unset_userdata('monthly_turnover_month');
		$this->session->unset_userdata('monthly_turnover_year');
		
		redirect('hospital-reports/monthly-sales-summary');
	}

	public function print_monthly_sales()
	{

		$data['title'] = $v_data['title'] = 'Monthly Sales Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
			$v_data['contacts'] = $this->site_model->get_contacts();
		// var_dump("sdakdha");die();

		$v_data['month'] = $this->accounts_model->get_months();


		$this->load->view('reports/print_monthly_turnover', $v_data);
	}

	public function print_doctors_billing($personnel_id)
	{

		$data['title'] = $v_data['title'] = 'Monthly Billing Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		$v_data['personnel_id'] = $personnel_id;//$config['total_rows'];
			$v_data['contacts'] = $this->site_model->get_contacts();
		// var_dump("sdakdha");die();

		$v_data['month'] = $this->accounts_model->get_months();


		$this->load->view('reports/print_individual_monthly_turnover', $v_data);
	}

	public function print_doctors_payments()
	{

		$data['title'] = $v_data['title'] = 'Monthly Payments Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
			$v_data['contacts'] = $this->site_model->get_contacts();
		// var_dump("sdakdha");die();

		$v_data['month'] = $this->accounts_model->get_months();


		$this->load->view('reports/print_payment_monthly_turnover', $v_data);
	}



	public function get_personnel_invoices($personnel_id)
	{
		$data['personnel_id'] = $personnel_id;
		$results = $this->load->view('reports/doctors_sales', $data,true);

		echo $results;
	}
	public function get_personnel_payments($personnel_id)
	{
		$data['personnel_id'] = $personnel_id;
		$results = $this->load->view('reports/doctors_payments', $data,true);

		echo $results;
	}

	

	

	public function individual_doctors_turnover()
	{


		$module = NULL;
		

		$where = 'visit_type.visit_type_id = visit_invoice.bill_to AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND patients.patient_id = visit_invoice.patient_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit_invoice.dentist_id = personnel.personnel_id';
		$table = 'patients, visit_type, visit_invoice, visit_charge,service_charge, personnel';
		$visit_search = $this->session->userdata('doctors_turnover_individual_search');
		
		// var_dump($visit_search);die();

		if(!empty($visit_search))
		{
			$where .= $visit_search;
		
			$query = $this->hospital_reports_model->get_all_daily_sales($table, $where,1);
			// var_dump($where);die();
			
		}
		else
		{
			$where .= ' ';
			// $where .= ' AND visit_invoice.created = "'.date('Y-m-d').'" ';
		
			// $visit_payments = ' AND payments.payment_created = \''.date('Y-m-d').'\'';
			// $visit_invoices = ' AND visit_invoice.created = \''.date('Y-m-d').'\'';
			// $search_title = 'Visit date of '.date('jS M Y', strtotime(date('Y-m-d'))).' ';

			// $this->session->set_userdata('drb_search', $visit_invoices);
			// $this->session->set_userdata('drb_payments_search', $visit_payments);
		
			$query = NULL;
			
		}
		
		
		
		// var_dump($query);die();
		$v_data['query'] = $query;
		$v_data['page'] = 0;
		$v_data['total_patients'] = 1;		
	
		// $page_title = $this->session->userdata('page_title');
		// if(empty($page_title))
		// {
		// 	$page_title = 'Daily Records Report for '.date('Y-m-d');
		// }
		// var_dump($page_title);die();
		$data['title'] = $v_data['title'] = 'Doctor\'s Turnover';
		$v_data['debtors'] = $this->session->userdata('debtors');
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		$v_data['total_visits'] = 20;//$config['total_rows'];
		
		$v_data['module'] = $module;

		$v_data['month'] = $this->accounts_model->get_months();
		
		$data['content'] = $this->load->view('reports/individual_doctors_turnover', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_individual_doctors_turnover()
	{
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$doctor_id = $this->input->post('doctor_id');
		$personnel_id = $this->input->post('personnel_id');
		$search_title = '';
		
		
		if(!empty($month))
		{
			$month = $month;
		}
		else
		{
			$month = NULL;
		}

		if(!empty($year))
		{
			$year = $year;
		}
		else
		{
			$year = NULL;
		}

		if(!empty($personnel_id))
		{
			
			
			$this->db->where('personnel_id', $personnel_id);
			$query = $this->db->get('personnel');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->personnel_fname.' '.$row->personnel_onames.' ';
			}
		}
		
		

		

		// $search = $visit_invoices.$dentist_id;

		// var_dump($query);die();

		$this->session->set_userdata('doctors_turnover_individual_searched', TRUE);
		$this->session->set_userdata('doctors_turnover_individual_month', $month);
		$this->session->set_userdata('doctors_turnover_individual_year', $year);
		$this->session->set_userdata('doctors_turnover_individual_personnel', $personnel_id);
		
		redirect('hospital-reports/doctors-turnover-detail');
	}

	public function close_individual_doctors_turnover_search()
	{
		$this->session->unset_userdata('doctors_turnover_individual_searched');
		$this->session->unset_userdata('doctors_turnover_individual_month');
		$this->session->unset_userdata('doctors_turnover_individual_year');
		$this->session->unset_userdata('doctors_turnover_individual_personnel');
		
		redirect('hospital-reports/doctors-turnover-detail');
	}


	

	

	
}
?>