<?php


$amortization_rs = $this->assets_model->get_product_amortization($asset_id);

$result = '';
$total_interest = 0;
$actual_first_date = date('M Y', strtotime($first_date));

$current_year = date('Y');
$result .= '
					<table class="table table-condensed table-striped table-hover table-bordered">
						
						<tr>
							<th>#</th>
							<th>Application Date</th>
							<th>Begining Value</th>
							<th>Depreciation</th>
							<th>End bal.</th>
							<th></th>
							
						</tr>
						
					';

if($amortization_rs->num_rows() > 0)
{
	$count = 0;
	foreach ($amortization_rs->result() as $key => $value) {
		// code...

		$repayment = $value->repayment;
		$interest_amount = $value->interest_amount;
		$principal_amount = $value->principal_amount;
		$amortizationYear = $value->amortizationYear;
		$startBalance = $value->startBalance;
		$endBalance = $value->endBalance;
		$amortizationDate = $value->amortizationDate;
		$duration = $value->duration;
		$rate = $value->rate;
		$depriciation_type = $value->depriciation_type;
		$asset_amortization_id = $value->asset_amortization_id;

		$count++;

			if($current_year == $amortizationYear)
			{
				$result .= '
							<tr>
								<td>'.$count.'</td>
								<td>'.$amortizationYear.'</td>
								<td>'.number_format($startBalance, 2).'</td>
								<td><input type="text" class="form-control" id="interest_amount'.$asset_amortization_id.'" value="'.$interest_amount.'" >

									<input type="hidden" class="form-control" id="startBalance'.$asset_amortization_id.'" value="'.$startBalance.'" >
									<input type="hidden" class="form-control" id="startBalance'.$asset_amortization_id.'" value="'.$startBalance.'" >
								</td>
								<td>'.number_format($endBalance, 2).'</td>
								<td><button class="btn btn-xs btn-success" onclick="recalculate_amortization('.$amortizationYear.','.$asset_id.','.$asset_amortization_id.')"><i class="fa fa-pencil"></i></button></td>
								
							</tr>';
			}
			else
			{
				$result .= '
							<tr>
								<td>'.$count.'</td>
								<td>'.$amortizationYear.'</td>
								<td>'.number_format($startBalance, 2).'</td>
								<td>'.number_format($interest_amount, 2).'</td>
								<td>'.number_format($endBalance, 2).'</td>
								<td></td>
								
							</tr>';
			}


	}
}

	$result .= '</table>';



	
?>
<section class="panel">
    <header class="panel-heading">
         <strong>Amortization Table</strong>
    </header>
    <div class="panel-body">
    	<?php
        if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				//$this->session->unset_userdata('error_message');
			}
		?>
       <div class="table-responsive">	
			<?php echo $result;?>
	   </div>
     </div>
</section>


						
				