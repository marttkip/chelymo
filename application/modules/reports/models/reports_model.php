
<?php

class Reports_model extends CI_Model 
{
	public function get_queue_total($branch_code = 'OSE', $date = NULL, $where = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		if($where == NULL)
		{
			$where = 'visit.branch_code = \''.$branch_code.'\' AND visit.close_card = 0 AND visit.visit_date = \''.$date.'\' AND visit.visit_delete = 0';
		}
		
		else
		{
			$where .= ' AND visit.branch_code = \''.$branch_code.'\' AND visit.visit_delete = 0 AND visit.close_card = 0 AND visit.visit_date = \''.$date.'\' ';
		}
		
		$this->db->select('COUNT(visit.visit_id) AS queue_total');
		$this->db->where($where);
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->queue_total;
	}
	
	public function get_daily_balance($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		//select the user by email from the database
		$this->db->select('SUM(amount_paid) AS total_amount');
		$this->db->where('cancel = 0 AND payment_type = 1 AND payment_method_id = 2 AND payment_created = \''.$date.'\'');
		$this->db->from('payments');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_amount;
	}
	
	public function get_patients_total($branch_code = 'OSE', $date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where('visit.branch_code = \''.$branch_code.'\' AND visit_date = \''.$date.'\' AND visit.visit_delete = 0');
		$query = $this->db->get('visit');
		
		$result = $query->row();
		
		return $result->patients_total;
	}

	public function get_totals_items($where_item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 '.$where_item;
		$table = 'visit, patients, visit_type';


		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		$result = $query->row();
		
		return $result->patients_total;
	}


	public function calculate_distict($item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.inpatient = 0';
		$table = 'visit, patients, visit_type';


		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('visit.patient_id,rip_status');
		$this->db->where($where);
		if($item ==1)
		{
			$this->db->group_by('visit.patient_id');	
		}
		$query = $this->db->get($table);
		$response['total_count'] = $query->num_rows();
		$new_visit = 0;
		$repeat_visit = 0;
		$rip_number=0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$patient_id = $key->patient_id;
				$rip_status = $key->rip_status;

				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);

				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() == 1)
				{	

					
					$new_visit++;
				}
				
				else if($last_visit_rs->num_rows() > 1)
				{	
					$repeat_visit++;
					
				}

				if($rip_status ==1)
				{
					$rip_number++;
				}

				
			}


		}

		$response['new_visit'] = $new_visit;
		$response['repeat_visit'] = $repeat_visit;
		$response['rip_number'] = $rip_number;
		
	
		
		return $response;
	}


	public function get_totals_inpatient_items($where_item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 '.$where_item;
		$table = 'visit, patients, visit_type';


		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('COUNT(visit_id) AS patients_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		$result = $query->row();
		
		return $result->patients_total;
	}


	public function calculate_distict_inpatient($item = NULL)
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.inpatient = 1';
		$table = 'visit, patients, visit_type';


		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$this->db->select('visit.patient_id,rip_status');
		$this->db->where($where);
		if($item ==1)
		{
			$this->db->group_by('visit.patient_id');	
		}
		$query = $this->db->get($table);
		$response['total_count'] = $query->num_rows();
		$new_visit = 0;
		$repeat_visit = 0;
		$rip_number=0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$patient_id = $key->patient_id;
				$rip_status = $key->rip_status;

				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);

				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() == 1)
				{	

					
					$new_visit++;
				}
				
				else if($last_visit_rs->num_rows() > 1)
				{	
					$repeat_visit++;
					
				}

				if($rip_status ==1)
				{
					$rip_number++;
				}

				
			}


		}

		$response['new_visit'] = $new_visit;
		$response['repeat_visit'] = $repeat_visit;
		$response['rip_number'] = $rip_number;
		
	
		
		return $response;
	}

	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_visits($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name,departments.department_name');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		$this->db->join('departments', 'departments.department_id = visit.department_id', 'left');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/

	   
	 public function get_departments_l($department_id)
    {
         //retrieve all users
        $this->db->from('departments');
        $this->db->select('*');
        $this->db->where('department_id ='.$department_id);
        $query = $this->db->get();
        
        return $query;

    }


	public function get_all_patient_rip($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		//$this->db->order_by('patients.rip_date','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_patient_birth($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		//$this->db->order_by('patients.rip_date','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_visits_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name,departments.department_name');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		$this->db->join('departments', 'departments.department_id = visit.department_id', 'left');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->group_by('visit.visit_id');
		$query = $this->db->get('');
		
		return $query;
	}

	public function get_all_sick_off_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patient_leave.*,patient_leave.created_by AS personnel_id,patients.*, visit.department_name');
		$this->db->where($where);
		$this->db->order_by('patient_leave.start_date','DESC');
		$query = $this->db->get('');
		
		return $query;
	}

	public function get_all_visits_sick_offs($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('patient_leave.*, patient_leave.created_by AS personnel_id ,patients.*, visit.department_name, leave_type.leave_type_name');
		$this->db->where($where);
		$this->db->order_by('patient_leave.start_date','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function get_all_departments()
	{
		$this->db->distinct('department_name');
		$this->db->select('department_name');
		$this->db->where('department_name IS NOT NULL');
		$query = $this->db->get('visit');
		//var_dump($query); die();
		return $query;
	}
	public function get_all_patient_leave($table, $where, $per_page, $page, $order, $order_method)
	{
		$this->db->from($table);
		//$this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		$this->db->select('patient_leave.*, patients.*, visit.department_name, leave_type.leave_type_name');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function export_outpatient_report()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card >= 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name,departments.department_name');

		$this->db->join('departments', 'departments.department_id = visit.department_id', 'left');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Outpatient Report';

		$personnel_query = $this->personnel_model->get_all_personnel();
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient No';
			$report[$row_count][3] = 'Patient Name';
			$report[$row_count][4] = 'Gender';
			$report[$row_count][5] = 'Age';
			$report[$row_count][6] = 'Chemo / Review';
			$report[$row_count][7] = 'Visit';
			$report[$row_count][8] = 'Doctor';
			$report[$row_count][9] = 'Department';
			$report[$row_count][10] = 'D X';
			$report[$row_count][11] = 'RIP';
			$report[$row_count][12] = 'Patient Type';
			$report[$row_count][13] = 'HC Time In';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date =  date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}

				
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$patient_number = $row->patient_number;

				$strath_no = $row->strath_no;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$gender_id = $row->gender_id;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $first_visit_department = $this->reception_model->first_department($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$last_visit = $row->last_visit;
				$department_name = $row->department_name;
				$department_id = $row->department_id;
				$branch_code = $row->branch_code;
				$department = $row->department;
				$inpatient = $row->inpatient;
				// $relative_code = $row->relative_code;
				$referral_reason = $row->referral_reason;
				$rip_status = $row->rip_status;
				$rip_date = $row->rip_date;
				$visit_date1 = $row->visit_date;
				// var_dump($difference);
				if($rip_status == 1  AND $visit_date1 >= $rip_date)
				{
					$rip_status = 'RIP';
				}
				else
				{
					$rip_status = '';
				}
				
				//branch Code
				// if($branch_code =='OSE')
				// {
					$branch_code = 'Main HC';
				// }
				// else
				// {
				// 	$branch_code = 'Oserengoni';
				// }
				
				$close_card = $row->close_card;
				if($close_card == 1)
				{
					$visit_time_out = date('jS M Y H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);
				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() > 1)
				{
					$last_visit_name = 'Re Visit';
				}
				
				else
				{
					$last_visit_name = 'First Visit';
				}

				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}

				// this is to check for any credit note or debit notes
				$payments_value = $this->accounts_model->total_payments($visit_id);

				$invoice_total = $this->accounts_model->total_invoice($visit_id);

				$balance = $this->accounts_model->balance($payments_value,$invoice_total);
				// end of the debit and credit notes


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}


				if($inpatient == 0)
				{
					$patient_type = 'Outpatient';
				}
				else
				{
					$patient_type = 'Inpatient';
				}
				

				$age = $this->reception_model->calculate_age($patient_date_of_birth);


				$diagnosis_rs = $this->nurse_model->get_visit_diagnosis($visit_id);
				$diagnosis = '';
				if($diagnosis_rs->num_rows() > 0)
				{
					foreach ($diagnosis_rs->result() as $key_other) {
						# code...
						$diseases_name = $key_other->diseases_name;
						$diseases_code = $key_other->diseases_code;

						$diagnosis .= $diseases_name.'  '.$diseases_code.' ';
					}
				}


				$department_details = $this->reports_model->get_departments_l($department_id);
				$department_name = '';
				if($department_details->num_rows() > 0)
				{
				foreach ($department_details->result() as $key => $value) {
					# code...
					$department_name = $value->department_name;
				

				}
				}

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_number;
				$report[$row_count][3] = $patient_surname.' '.$patient_othernames;
				$report[$row_count][4] = $gender;
				$report[$row_count][5] = $age;
				$report[$row_count][6] = '-';
				$report[$row_count][7] = $last_visit_name;
				$report[$row_count][8] = $doctor;
				$report[$row_count][9] = $department_name;
				$report[$row_count][10] = $diagnosis;
				$report[$row_count][11] = $rip_status;
				$report[$row_count][12] = $patient_type;
				$report[$row_count][13] = $visit_time;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	
	}

	public function export_inpatient_report()
	{
		$this->load->library('excel');
		
		//get all transactions
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 1 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		
		$this->db->where($where);
		$this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		$this->db->select('visit.*, (visit.visit_time_out - visit.visit_time) AS waiting_time, patients.*, visit_type.visit_type_name');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Inpatient Report';

		$personnel_query = $this->personnel_model->get_all_personnel();
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Patient No';
			$report[$row_count][2] = 'Patient Name';
			$report[$row_count][3] = 'Gender';
			$report[$row_count][4] = 'Age';
			$report[$row_count][5] = 'Date of Admission';
			$report[$row_count][6] = 'Status';
			$report[$row_count][7] = 'D X';
			$report[$row_count][8] = 'RIP';
			$report[$row_count][9] = 'HC Time In';
			$report[$row_count][10] = 'HC Time Out';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$patient_number = $row->patient_number;

				$strath_no = $row->strath_no;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$gender_id = $row->gender_id;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $first_visit_department = $this->reception_model->first_department($visit_id);
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$last_visit = $row->last_visit;
				// $department_name = $row->department_name;
				$branch_code = $row->branch_code;
				$department = $row->department;
				$inpatient = $row->inpatient;
				$rip_status = $row->rip_status;
				// $relative_code = $row->relative_code;
				$referral_reason = $row->referral_reason;
				
				//branch Code
				// if($branch_code =='OSE')
				// {
					$branch_code = 'Main HC';
				// }
				// else
				// {
				// 	$branch_code = 'Oserengoni';
				// }
				
				$close_card = $row->close_card;
				if($close_card == 1)
				{
					$visit_time_out = date('jS M Y H:i a',strtotime($row->visit_time_out));
					$close_card_status = 'Discharged';
				}
				else if($close_card == 0)
				{
					$close_card_status = 'Patient Admitted';
					$visit_time_out = '-';
				}
				else 
				{
					$close_card_status = 'Discharged In';
					$visit_time_out = '-';
				}
				$last_visit_rs = $this->reception_model->get_if_patients_first_visit($patient_id);
				// var_dump($last_visit_rs); die();
				if($last_visit_rs->num_rows() > 1)
				{
					$last_visit_name = 'Re Visit';
				}
				
				else
				{
					$last_visit_name = 'First Visit';
				}

				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}


				if($gender_id == 1)
				{
					$gender = 'Male';
				}
				else
				{
					$gender = 'Female';
				}
				if($rip_status == 1)
				{
					$rip_status = 'RIP';
				}
				else
				{
					$rip_status = '';
				}

				// this is to check for any credit note or debit notes
				$payments_value = $this->accounts_model->total_payments($visit_id);

				$invoice_total = $this->accounts_model->total_invoice($visit_id);

				$balance = $this->accounts_model->balance($payments_value,$invoice_total);
				// end of the debit and credit notes


				//creators and editors
				if($personnel_query->num_rows() > 0)
				{
					$personnel_result = $personnel_query->result();
					
					foreach($personnel_result as $adm)
					{
						$personnel_id2 = $adm->personnel_id;
						
						if($personnel_id == $personnel_id2)
						{
							$doctor = $adm->personnel_onames.' '.$adm->personnel_fname;
							break;
						}
						
						else
						{
							$doctor = '-';
						}
					}
				}
				
				else
				{
					$doctor = '-';
				}


				if($inpatient == 0)
				{
					$patient_type = 'Outpatient';
				}
				else
				{
					$patient_type = 'Inpatient';
				}
				
				
				

				$age = $this->reception_model->calculate_age($patient_date_of_birth);


				$diagnosis_rs = $this->nurse_model->get_visit_diagnosis($visit_id);
				$diagnosis = '';
				if($diagnosis_rs->num_rows() > 0)
				{
					foreach ($diagnosis_rs->result() as $key_other) {
						# code...
						$diseases_name = $key_other->diseases_name;

						$diseases_code = $key_other->diseases_code;

						$diagnosis .= $diseases_name.'  '.$diseases_code.' ';
					}
				}


				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $patient_number;
				$report[$row_count][2] = $patient_surname.' '.$patient_othernames;
				$report[$row_count][3] = $gender;
				$report[$row_count][4] = $age;
				$report[$row_count][5] = $visit_date;
				$report[$row_count][6] = $close_card_status;
				$report[$row_count][7] = $diagnosis;
				$report[$row_count][8] = $rip_status;
				$report[$row_count][9] = $visit_time;
				$report[$row_count][10] = $visit_time_out;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	
	}

	public function get_all_lab_visit($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,service_charge.service_charge_id,service.service_name,service_charge.lab_test_id');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		$this->db->order_by('total_count','DESC');
		$this->db->group_by('service_charge.lab_test_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_procedures_visit($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,service_charge.service_charge_id,service.service_name');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		$this->db->order_by('total_count','DESC');
		$this->db->group_by('service_charge.service_charge_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function export_procedures_report($service_charge_id)
	{
		$this->load->library('excel');
		$branch_id = $this->session->userdata('branch_id');

		// var_dump($branch_id);die();
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Procedures" OR service.service_name = "Ultrasound") AND visit.branch_id ='.$branch_id;
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$inpatient_report_search = $this->session->userdata('procedure_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		if(!empty($service_charge_id))
		{
			$where .= ' AND visit_charge.service_charge_id = '.$service_charge_id;
		}
		
		$this->db->where($where);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount');
		$this->db->order_by('total_count','DESC');
		$this->db->group_by('service_charge.service_charge_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Procedure Report ';

		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Procedure Name';
			$report[$row_count][2] = 'Procedure Count';
			$report[$row_count][3] = 'Rate';
			$report[$row_count][4] = 'Revenue';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$service_charge_name = $row->service_charge_name;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$service_charge_amount = $row->service_charge_amount;

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $service_charge_name;
				$report[$row_count][2] = $total_count;
				$report[$row_count][3] = $service_charge_amount;
				$report[$row_count][4] = $total_revenue;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function export_visit_procedures_report($service_charge_id)
	{
		$this->load->library('excel');
		
		//get all transactions

		$branch_id = $this->session->userdata('branch_id');

		// var_dump($branch_id);die();
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND service.service_id = service_charge.service_id AND (service.service_name = "Procedures" OR service.service_name = "Ultrasound") AND visit.patient_id = patients.patient_id AND visit.visit_id = visit_charge.visit_id AND visit.branch_id ='.$branch_id;


		$table = 'visit_charge,service_charge,visit,patients,visit_invoice,service';
		$inpatient_report_search = $this->session->userdata('procedure_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		if(!empty($service_charge_id))
		{
			$where .= ' AND visit_charge.service_charge_id = '.$service_charge_id;
		}
		
		$this->db->where($where);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,patients.patient_othernames,patients.patient_surname,visit.visit_date');
		$this->db->order_by('visit.visit_date','ASC');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Procedure Report ';

		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient Name';
			$report[$row_count][3] = 'Procedure Count';
			$report[$row_count][4] = 'Rate';
			$report[$row_count][5] = 'Revenue';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$service_charge_name = $row->service_charge_name;
				$patient_surname = $row->patient_surname;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$visit_date = $row->visit_date;
				$service_charge_amount = $row->service_charge_amount;

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_surname;
				$report[$row_count][3] = $total_count;
				$report[$row_count][4] = $service_charge_amount;
				$report[$row_count][5] = $total_revenue;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	/*
	*	Retrieve total revenue
	*
	*/
	public function get_total_cash_collection($where, $table, $page = NULL)
	{
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		
		// if($page != 'cash')
		// {
		// 	$where .= ' AND visit.visit_id = payments.visit_id AND payments.cancel = 0';
		// }
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table);
		}
		$this->db->select('SUM(v_transactions_by_date.cr_amount) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		
		$cash = $query->row();
		$total_paid = $cash->total_paid;
		if($total_paid > 0)
		{
		}
		
		else
		{
			$total_paid = 0;
		}
		
		return $total_paid;
	}

	/*
	*	Retrieve total revenue
	*
	*/
	public function get_normal_payments($where, $table, $page = NULL)
	{
		// if($page != 'cash')
		// {
		// 	$where .= ' AND visit.visit_id = payments.visit_id AND payments.cancel = 0';
		// }
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}
		
		else
		{
			$this->db->from($table);
		}
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');
		
		return $query;
	}

	public function get_debt_payment_totals($type_payment=NULL,$first_day,$last_day,$branch_id)
	{
		// $visit_payments = $this->session->userdata('visit_payments');
		$visit_invoices = $this->session->userdata('visit_invoices');
		
		$visit_type_id = $this->session->userdata('visit_type_id');
		$visit_type = $this->session->userdata('visit_type');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';
		$add_debt = '';



		
		$add .= ' AND v_transactions_by_date.transaction_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" ';
		

		if($type_payment == 0)
		{
			$add .= ' AND v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date ';
		}
		else
		{
			$add .= ' AND v_transactions_by_date.transaction_date <> v_transactions_by_date.invoice_date ';
		}
		$branch_session = $branch_id;

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}
		
		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.reference_id  IN (SELECT v_transactions_by_date.transaction_id FROM v_transactions_by_date WHERE  v_transactions_by_date.transactionCategory = "Revenue" '.$add_debt.' ) '.$add);
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}

	public function get_patients_visits($patient_visit)
	{
		$visit_invoices = $this->session->userdata('visit_invoices');
		$visit_type_id = $this->session->userdata('visit_type_id');
		$patient_number = $this->session->userdata('patient_number');
		$add ='';
		$table_add = '';

		

		$debtor_query = $this->session->userdata('debtors_search_query');

		// var_dump($visit_type_id); die();
		if(!empty($debtor_query))
		{
			$debtor_query = str_replace('v_transactions_by_date.transaction_date','visit.visit_date', $debtor_query);
			$debtor_query = str_replace('v_transactions_by_date.branch_id','visit.branch_id', $debtor_query);
			$debtor_query = str_replace('v_transactions_by_date.payment_type','visit.visit_type', $debtor_query);

			$add .= $debtor_query;

			$branch_session = $this->session->userdata('branch_id');

			if($branch_session > 0)
			{
				$add .= ' AND visit.branch_id = '.$branch_session;
			
			}
		}
		else
		{
			$add .= ' AND visit.visit_date = \''.date('Y-m-d').'\'';

			$branch_session = $this->session->userdata('branch_id');

			if($branch_session > 0)
			{
				$add .= ' AND visit.branch_id = '.$branch_session;
			
			}
		}

		if($patient_visit == 1)
		{
			$add .= ' AND patients.patient_id = visit.patient_id';
		}
		else if($patient_visit == 0)
		{
			$add .= ' AND patients.patient_id = visit.patient_id';
		}
		$this->db->where('visit.visit_delete = 0 AND visit.close_card <> 2 '.$add);
		$this->db->select('visit.patient_id');
		$query = $this->db->get('visit,patients');
		$total_new = 0;
		$total_old = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$patient_id = $value->patient_id;

				$this->db->where('visit.visit_delete = 0 AND visit.close_card <> 2  AND visit.patient_id ='.$patient_id);
				$this->db->select('*');
				$query_numbers = $this->db->get('visit');
				if($query_numbers->num_rows() == 1)
				{
					$total_new +=1;
				}
				else
				{
					$total_old +=1;
				}
			}
		}
		$response['total_new'] = $total_new;
		$response['total_old'] = $total_old;

		return $response;
	}
	

	public function get_total_collected($doctor_id=NULL, $date_from = NULL, $date_to = NULL,$visit_type_id = NULL,$branch_id)
	{
		if($visit_type_id == 1)
		{
			$add = ' AND v_transactions.payment_type = 1';
		}
		else
		{
			$add = ' AND v_transactions.payment_type > 1';
		}

		$branch_session = $branch_id;
		
		if($branch_session > 0)
		{
			$add .= ' AND branch_id = '.$branch_session.' ';
		
		}
		$table = 'v_transactions';
		if($doctor_id > 0)
		{
			$where = 'v_transactions.personnel_id = '.$doctor_id.$add;
		}
		else
		{
			$where = 'v_transactions.personnel_id = 0 '.$add;
		}
		
		
		
		$where .= ' AND (v_transactions.invoice_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\') AND (v_transactions.transactionCategory = "Revenue"  OR v_transactions.transactionCategory = "Credit Note")  ';

		
		$this->db->select('SUM(dr_amount) - SUM(cr_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}

	public function get_total_collected_invoice($doctor_id, $date_from = NULL, $date_to = NULL)
	{
		$table = 'visit_charge, visit';
		
		$where = 'visit_charge.visit_id = visit.visit_id AND visit.visit_delete = 0 AND visit.visit_type >= 2 AND visit_charge.visit_charge_delete = 0 AND visit.personnel_id = '.$doctor_id;
		
		$visit_search = $this->session->userdata('all_doctors_search');
		if(!empty($visit_search))
		{
			$where = 'visit_charge.visit_id = visit.visit_id AND visit.visit_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit.personnel_id = '.$doctor_id.' '. $visit_search;
		}
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (visit.visit_date >= \''.$date_from.'\' AND visit.visit_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND visit_date LIKE \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND visit_date LIKE \''.$date_from.'\'';
		}
		
		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}

	public function get_total_collected_invoice_total($doctor_id, $date_from = NULL, $date_to = NULL)
	{
		$table = 'visit_charge, visit';
		
		$where = 'visit_charge.visit_id = visit.visit_id AND visit.visit_delete = 0 AND visit.visit_type <> 2 AND visit_charge.visit_charge_delete = 0 AND visit.personnel_id = '.$doctor_id;
		
		$visit_search = $this->session->userdata('all_doctors_search');
		if(!empty($visit_search))
		{
			$where = 'visit_charge.visit_id = visit.visit_id AND visit.visit_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit.personnel_id = '.$doctor_id.' '. $visit_search;
		}
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (visit.visit_date >= \''.$date_from.'\' AND visit.visit_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND visit_date LIKE \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND visit_date LIKE \''.$date_from.'\'';
		}
		
		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}


	public function get_total_waivers($doctor_id, $date_from = NULL, $date_to = NULL)
	{
		$table = 'payments, visit';
		
		$where = 'payments.visit_id = visit.visit_id AND visit.visit_delete = 0 AND payments.cancel = 0 and payments.payment_type = 2 AND visit.personnel_id = '.$doctor_id;
		
		
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (visit.visit_date >= \''.$date_from.'\' AND visit.visit_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND visit.visit_date LIKE \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND visit.visit_date LIKE \''.$date_from.'\'';
		}
		
		$this->db->select('SUM(amount_paid) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}

	public function get_total_payments_made($doctor_id=NULL, $date_from = NULL, $date_to = NULL,$branch_id)
	{
		// $table = 'payments, visit';
		
		// $where = 'payments.visit_id = visit.visit_id AND visit.visit_delete = 0 AND payments.cancel = 0 and payments.payment_type = 1 AND visit.personnel_id = '.$doctor_id;
		
		if($doctor_id > 0)
		{
			$add = ' AND v_transactions_by_date.invoice_date BETWEEN "'.$date_from.'" AND "'.$date_to.'" AND personnel_id = '.$doctor_id;
		}
		else
		{
			$add = ' AND v_transactions_by_date.invoice_date BETWEEN "'.$date_from.'" AND "'.$date_to.'" AND personnel_id = 0 ';
		}
		
		
		

		// if($type_payment == 0)
		// {
		// 	$add .= ' AND v_transactions_by_date.transaction_date = v_transactions_by_date.invoice_date ';
		// }
		// else
		// {
		// 	$add .= ' AND v_transactions_by_date.transaction_date <> v_transactions_by_date.invoice_date ';
		// }
		$branch_session = $branch_id;

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}
		
		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0 AND v_transactions_by_date.reference_id '.$add);
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
		
	}

	public function get_total_patients($doctor_id, $date_from = NULL, $date_to = NULL,$revisit_status = NULL)
	{
		$branch_session = $this->session->userdata('branch_id');
		$add ='';
		if($branch_session > 0)
		{
			$add .= ' AND branch_id = '.$branch_session.' ';
		
		}
		$table = 'visit';
		$where = 'visit.visit_delete = 0 AND visit.personnel_id = '.$doctor_id.$add;
		
		if(!empty($date_from) && !empty($date_to))
		{
			$where .= ' AND (visit_date >= \''.$date_from.'\' AND visit_date <= \''.$date_to.'\') ';
		}
		
		else if(empty($date_from) && !empty($date_to))
		{
			$where .= ' AND visit_date = \''.$date_to.'\'';
		}
		
		else if(!empty($date_from) && empty($date_to))
		{
			$where .= ' AND visit_date = \''.$date_from.'\'';
		}

		
		$this->db->where($where);
		$total = $this->db->count_all_results('visit');
		
		return $total;
	}

	public function get_visit_types($first_day,$last_day,$branch_id)
	{
		$branch_session = $branch_id;
		$add ='';
		if($branch_session > 0)
		{
			$add .= ' AND visit.branch_id = '.$branch_session.' ';
		
		}
		$where = ' AND visit.visit_date BETWEEN "'.$first_day.'"  AND "'.$last_day.'" '.$add;
		$this->db->from('visit_type,visit');
		$this->db->select('visit_type.*,COUNT(visit.visit_id) AS total_patients');
		$this->db->where('visit.visit_type = visit_type.visit_type_id AND visit.visit_delete = 0'.$where);
		$this->db->group_by('visit.visit_type');
		$query = $this->db->get();
		
		
		
		return $query;
	}

	public function get_visit_type_invoice_todays($visit_type_id,$visit_date = NULL)
	{
		if(!empty($visit_date))
		{
			$date  = ' AND visit_date = "'.$visit_date.'" ';
		}
		else
		{
			$date  = ' AND visit_date = "'.date('Y-m-d').'" ';
		}
		//retrieve all users
		$this->db->from('visit');
		$this->db->select('*');
		$this->db->where('visit.visit_delete = 0 '.$date.' AND visit_type = '.$visit_type_id);
		$query = $this->db->get('');
		$invoice_amount = 0;
		$payment_amount = 0;
		$balance_amount = 0;
		// if($query->num_rows() > 0)
		// {
		// 	foreach ($query->result() as $key => $value) {

				// $visit_id = $value->visit_id;


				$payments_value = $this->accounts_model->total_payments_today($visit_date,$visit_type_id);

				$invoice_total = $this->accounts_model->total_invoice_today($visit_date,$visit_type_id);

				$balance = $this->accounts_model->balance($payments_value,$invoice_total);

				$invoice_amount = $invoice_amount + $invoice_total;
				$payment_amount = $payment_amount + $payments_value;
				$balance_amount = $balance_amount + $balance;

		// 	}
		// }

		$response['invoice_total'] = $invoice_amount;
		$response['payments_value']= $payment_amount;
		$response['balance'] = $balance_amount;
		return $response;
	}

	function get_months()
	{

		return $this->db->get('month');
	}

	public function get_all_doctors($first_day,$last_day,$branch_id)
	{
		$branch_session = $branch_id;
		$add ='';
		if($branch_session > 0)
		{
			$add .= ' AND visit.branch_id = '.$branch_session.' ';
		
		}

		$add .= ' AND close_card <> 2 AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" ';
		$this->db->select('personnel.personnel_id,personnel.personnel_fname,personnel.personnel_onames,COUNT(visit.visit_id) AS total_patients');
		$this->db->where('personnel.personnel_id = personnel_job.personnel_id AND personnel_job.job_title_id = job_title.job_title_id  AND visit.personnel_id = personnel.personnel_id AND visit.visit_delete = 0 '.$add);
		$this->db->order_by('personnel_fname');
		$this->db->group_by('visit.personnel_id');


		$query = $this->db->get('personnel,personnel_job,job_title,visit');
		
		return $query;
	}

	public function get_total_revenue($date_from = NULL, $date_to = NULL,$branch_id)
	{
		

		$branch_session = $branch_id;
		$add = '';
		if($branch_session > 0)
		{
			$add .= ' AND branch_id = '.$branch_session.' ';
		
		}
		$table = 'v_transactions';
		
		
		$where = '(v_transactions.transaction_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\') AND transactionClassification = "Invoice Patients" '.$add;

		
		$this->db->select('SUM(dr_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					return 0;
				}
				else
				{
					return $total;
				}
			endforeach;
		}
		else
		{
			return 0;
		}
		
	}



	public function get_visit_type_amounts($date_from = NULL, $date_to = NULL,$visit_type_id = NULL,$branch_id)
	{
		

		$branch_session = $branch_id;
		$add = '';
		if($branch_session > 0)
		{
			$add .= ' AND branch_id = '.$branch_session.' ';
		
		}
		$table = 'v_transactions';
		
		$where = '(v_transactions.invoice_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\') AND v_transactions.payment_type = '.$visit_type_id.' AND (v_transactions.transactionCategory = "Revenue"  OR v_transactions.transactionCategory = "Credit Note") '.$add;

		
		$this->db->select('SUM(dr_amount) - SUM(cr_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		// $result = $query->row();
		// $total = $result[0]->service_total;
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key):
				# code...
				$total = $key->service_total;

				if(!is_numeric($total))
				{
					$total_invoice = 0;
				}
				else
				{
				 	$total_invoice = $total;
				}
			endforeach;
		}
		
		
		$add = ' AND v_transactions.invoice_date BETWEEN "'.$date_from.'" AND "'.$date_to.'"  ';
	
		$branch_session = $branch_id;

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions.branch_id = '.$branch_session;
		
		}
		
		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('v_transactions.transactionCategory = "Revenue Payment" AND v_transactions.reference_id > 0 AND v_transactions.reference_id AND v_transactions.payment_type = '.$visit_type_id.' '.$add);
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}

		$response['total_payments'] = $total_payments;
		$response['total_invoices'] = $total_invoice;
		return $response;
		
	}

	public function get_all_expenses($date_from,$date_to,$branch_id)
	{

		$add = ' AND v_expenses_ledger.transactionDate BETWEEN "'.$date_from.'" AND "'.$date_to.'"  ';
	
		$branch_session = $branch_id;

		if($branch_session > 0)
		{
			$add .= ' AND v_expenses_ledger.branch_id = '.$branch_session;
		
		}
		
		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('(v_expenses_ledger.transactionClassification = "Purchases" OR v_expenses_ledger.transactionClassification = "Direct Payment") '.$add);
		$this->db->select('SUM(dr_amount) AS total_payments');
		$query = $this->db->get('v_expenses_ledger');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}

		return $total_payments;

	}


	public function get_all_expenses_list($date_from,$date_to,$branch_id)
	{

		$add = ' AND v_expenses_ledger.transactionDate BETWEEN "'.$date_from.'" AND "'.$date_to.'"  ';
	
		$branch_session = $branch_id;

		if($branch_session > 0)
		{
			$add .= ' AND v_expenses_ledger.branch_id = '.$branch_session;
		
		}
		
		$visit_type = $this->session->userdata('visit_type');
		
		$this->db->where('(v_expenses_ledger.transactionClassification = "Purchases" OR v_expenses_ledger.transactionClassification = "Expense" OR v_expenses_ledger.transactionClassification = "Direct Payment") '.$add);
		$this->db->select('v_expenses_ledger.accountName,SUM(dr_amount) AS total_expense,COUNT(v_expenses_ledger.transactionId) AS total_transactions');
		$this->db->group_by('v_expenses_ledger.accountId');
		$query = $this->db->get('v_expenses_ledger');

		return $query;

	}

	public function get_all_payments_transactions($table,$where)
	{
		

			//retrieve all users
		$this->db->from($table);
		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit_invoice.created AS invoice_date');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('personnel', 'payments.payment_created_by = personnel.personnel_id', 'left');
		$this->db->join('visit_invoice', 'visit_invoice.visit_invoice_id = v_transactions_by_date.reference_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');
		$query = $this->db->get('');
		
		
		return $query;
	}
	public function get_all_invoices_transactions($table,$where)
	{
		$this->db->from($table);

		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id,visit.visit_time_in,visit.visit_time_out');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');


		$query = $this->db->get('');
		
		return $query;
	}
	public function get_visit_invoice_payments($visit_invoice_id)
	{
		$this->db->select('SUM(payment_item_amount) AS total_amount');
		$this->db->where('payments.cancel = 0 AND payment_item.payment_item_deleted = 0 AND payments.payment_id = payment_item.payment_id AND payment_item.visit_invoice_id ='.$visit_invoice_id);
		$query = $this->db->get('payments,payment_item');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		if(empty($total_amount))
		{
			$total_amount = 0;
		}

		return $total_amount;
	}

	public function balance($payments, $invoice_total)
	{
		
		$value = $payments - $invoice_total;
		if($value > 0){
			$value= '(-'.$value.')';
		}
		else{
			$value= -(1) * ($value);
		}
	
		return $value;
	}

	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_expense_report($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');

		// $this->db->join('staff', 'staff.payroll_no = patients.strath_no', 'left');
		// $this->db->join('staff_dependant', 'staff_dependant.staff_dependant_id = patients.dependant_id', 'left');
		$this->db->where($where);
		// $this->db->order_by('visit.visit_date, visit.visit_time','DESC');
		// $this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_expenses_summary()
	{
		$where = '(v_expenses_ledger_by_date.transactionClassification = "Purchases" OR v_expenses_ledger_by_date.transactionClassification = "Direct Payments" OR v_expenses_ledger_by_date.transactionClassification = "Creditors Invoices Payments") ';
		$table = 'v_expenses_ledger_by_date';
		$visit_report_search = $this->session->userdata('export_payment_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND MONTH(v_expenses_ledger_by_date.transactionDate) = "'.date('m').'" AND YEAR(v_expenses_ledger_by_date.transactionDate) = "'.date('Y').'"';
		}

		$branch_session = $this->session->userdata('branch_id');

	    if($branch_session > 0)
	    {
	      $where .= ' AND v_expenses_ledger_by_date.branch_id = '.$branch_session;
	    }

		//retrieve all users
		$this->db->from($table);
		$this->db->select('SUM(dr_amount) AS total_amount,SUM(cr_amount) AS total_cr_amount,accountName,accountsclassfication');
		$this->db->where($where);
		$this->db->group_by('accountName');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_expenses_detail()
	{
		$where = '(v_expenses_ledger_by_date.transactionClassification = "Purchases" OR v_expenses_ledger_by_date.transactionClassification = "Direct Payments" OR v_expenses_ledger_by_date.transactionClassification = "Creditors Invoices Payments")';
		$table = 'v_expenses_ledger_by_date';
		$visit_report_search = $this->session->userdata('export_payment_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND MONTH(v_expenses_ledger_by_date.transactionDate) = "'.date('m').'" AND YEAR(v_expenses_ledger_by_date.transactionDate) = "'.date('Y').'"';
		}

		$branch_session = $this->session->userdata('branch_id');

	    if($branch_session > 0)
	    {
	      $where .= ' AND v_expenses_ledger_by_date.branch_id = '.$branch_session;
	    }

		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function export_expense_transactions()
	{

		$this->load->library('excel');
	
		$where = '(v_expenses_ledger_by_date.transactionClassification = "Purchases" OR v_expenses_ledger_by_date.transactionClassification = "Direct Payments" OR v_expenses_ledger_by_date.transactionClassification = "Creditors Invoices Payments")';
		$table = 'v_expenses_ledger_by_date';
		$visit_report_search = $this->session->userdata('export_payment_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND MONTH(v_expenses_ledger_by_date.transactionDate) = "'.date('m').'" AND YEAR(v_expenses_ledger_by_date.transactionDate) = "'.date('Y').'"';
		}
		$branch_session = $this->session->userdata('branch_id');

	    if($branch_session > 0)
	    {
	      $where .= ' AND v_expenses_ledger_by_date.branch_id = '.$branch_session;
	    }

		$this->db->where($where);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get($table);

		$search_title = $this->session->userdata('expense_search_title');
	
		if(!empty($search_title))
		{
			$title = $search_title;
		}
		else
		{
			$title = 'Expense for the month of '.date('M').' '.date('Y').'';
		}

		// var_dump($query); die();
		
		$title = $title;
		$col_count = 0;
		
		if($query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
				 
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Account Name';
			$col_count++;
			$report[$row_count][$col_count] = 'Transaction Category';
			$col_count++;
			$report[$row_count][$col_count] = 'Description';
			$col_count++;
			$report[$row_count][$col_count] = 'Reference Code';
			$col_count++;
			$report[$row_count][$col_count] = 'Amount';
			$col_count++;
			//display all patient data in the leftmost columns
			foreach($query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;

				$dr_amount = $row->dr_amount;
				$accountName = $row->accountName;
				$referenceCode = $row->referenceCode;
				$account_id = $row->accountId;
			    $transactionDescription = $row->transactionDescription;
			    $transactionClassification = $row->transactionClassification;
			    $transactionDate = $row->transactionDate;
			    $transactionCode = $row->transactionCode;

				if(empty($referenceCode))
				{
					$referenceCode = $transactionCode;
				}
               
				if($transactionClassification == "Creditors Invoices Payments")
				{
					$dr_amount = $row->cr_amount;
					$accountName = 'Creditor Payments';
				}

				$total_operational_amount += $total_amount;
				
				$count++;
				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDate;
				$col_count++;
				$report[$row_count][$col_count] = strtoupper($accountName);
				$col_count++;
				$report[$row_count][$col_count] = strtoupper($transactionClassification);
				$col_count++;
				$report[$row_count][$col_count] = strtoupper($transactionDescription);
				$col_count++;
				$report[$row_count][$col_count] = strtoupper($referenceCode);
				$col_count++;
				$report[$row_count][$col_count] = number_format($dr_amount,2);
				$col_count++;
		
				
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}



	public function export_tests_report()
	{
		$this->load->library('excel');
		$branch_id = $this->session->userdata('branch_id');

		// var_dump($branch_id);die();
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Laboratory") AND visit.branch_id ='.$branch_id;
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$inpatient_report_search = $this->session->userdata('tests_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		
		
		$this->db->where($where);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount');
		$this->db->order_by('total_count','DESC');
		$this->db->group_by('service_charge.lab_test_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Procedure Report ';

		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Procedure Name';
			$report[$row_count][2] = 'Procedure Count';
			$report[$row_count][3] = 'Revenue';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$service_charge_name = $row->service_charge_name;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$service_charge_amount = $row->service_charge_amount;

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $service_charge_name;
				$report[$row_count][2] = $total_count;
				$report[$row_count][3] = $total_revenue;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function export_visit_tests_report($lab_test_id)
	{
		$this->load->library('excel');
		
		//get all transactions

		$branch_id = $this->session->userdata('branch_id');

		// var_dump($branch_id);die();
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND service.service_id = service_charge.service_id AND (service.service_name = "Laboratory")  AND visit.patient_id = patients.patient_id AND visit.visit_id = visit_charge.visit_id';


		$table = 'visit_charge,service_charge,visit,patients,visit_invoice,service';
		$inpatient_report_search = $this->session->userdata('tests_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		if(!empty($lab_test_id))
		{
			$where .= ' AND service_charge.lab_test_id = '.$lab_test_id;
		}
		
		$this->db->where($where);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,patients.patient_othernames,patients.patient_surname,visit.visit_date');
		$this->db->order_by('visit.visit_date','ASC');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);
		
		$title = 'Procedure Report ';

		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient Name';
			$report[$row_count][3] = 'Procedure Count';
			$report[$row_count][4] = 'Rate';
			$report[$row_count][5] = 'Revenue';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$service_charge_name = $row->service_charge_name;
				$patient_surname = $row->patient_surname;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$visit_date = $row->visit_date;
				$service_charge_amount = $row->service_charge_amount;

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_surname;
				$report[$row_count][3] = $total_count;
				$report[$row_count][4] = $service_charge_amount;
				$report[$row_count][5] = $total_revenue;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function get_all_services()
	{

		 $this->db->from('service');
		  $this->db->select('*');
		  $this->db->where('service_status = 1 AND service_delete  = 0 ');
		  $this->db->order_by('service_id','ASC');

		  $query = $this->db->get();

		   return $query;

	}


	public function get_receivables_transactions_per_service($service_id = NULL,$type=NULL,$date_from=null,$date_to =null)
	{


		

		if(!empty($date_from) AND !empty($date_to))
		{
			$search_invoice_add =  '  AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$search_invoice_add = '  AND transaction_date = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
		}
		


		$add = '';
		if(!empty($service_id))
		{
			$add = ' AND child_service = '.$service_id;
		}
		$this->db->where('v_transactions_by_date.child_service > 0 AND (v_transactions_by_date.transactionCategory = "Revenue"  OR v_transactions_by_date.transactionCategory = "Credit Note")  '.$add.$search_invoice_add);
		$this->db->from('v_transactions_by_date');
		$this->db->select('SUM(dr_amount) AS dr_amount, SUM(cr_amount) AS cr_amount');
		$this->db->group_by('v_transactions_by_date.child_service');
		$query = $this->db->get();

		return $query;
		
		

	}










	public function get_visit_invoice_totals($date_from,$date_to)
	{
		if(!empty($date_from) AND !empty($date_to))
		{
			$add =  '  AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$add = '  AND transaction_date = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$add = ' AND transaction_date = \''.$date_to.'\'';
		}
		

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}
	
		
		$this->db->where('(v_transactions_by_date.transactionCategory = "Revenue") '.$add);
		$this->db->select('(SUM(dr_amount)) AS total_invoice');
		$query = $this->db->get('v_transactions_by_date');
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}
		if(empty($total_invoice))
		{
			$total_invoice = 0;
		}
		return $total_invoice;
	}

	public function get_visit_credits_totals($date_from,$date_to)
	{
		if(!empty($date_from) AND !empty($date_to))
		{
			$add =  '  AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$add = '  AND transaction_date = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$add = ' AND transaction_date = \''.$date_to.'\'';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}

		$this->db->where('(v_transactions_by_date.transactionCategory = "Credit Note") '.$add);
		$this->db->select('SUM(cr_amount) AS total_invoice');
		$query = $this->db->get('v_transactions_by_date');
		$total_invoice = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice = $value->total_invoice;
			}
		}
		if(empty($total_invoice))
		{
			$total_invoice = 0;
		}
		return $total_invoice;
	}
	public function get_visit_payment_totals($date_from,$date_to)
	{
		if(!empty($date_from) AND !empty($date_to))
		{
			$add =  '  AND (invoice_date >= \''.$date_from.'\' AND invoice_date <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$add = '  AND invoice_date = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$add = ' AND invoice_date = \''.$date_to.'\'';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}

		$visit_type = $this->session->userdata('visit_type');

		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0  '.$add.'');
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}

	public function get_period_payment_totals($date_from,$date_to)
	{
		if(!empty($date_from) AND !empty($date_to))
		{
			$add =  '  AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$add = '  AND transaction_date = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$add = ' AND transaction_date = \''.$date_to.'\'';
		}

		$branch_session = $this->session->userdata('branch_id');

		if($branch_session > 0)
		{
			$add .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		
		}

		$visit_type = $this->session->userdata('visit_type');

		$this->db->where('v_transactions_by_date.transactionCategory = "Revenue Payment" AND v_transactions_by_date.reference_id > 0  '.$add.'');
		$this->db->select('SUM(cr_amount) AS total_payments');
		$query = $this->db->get('v_transactions_by_date');
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}


	public function all_payments_period($date_from,$date_to)
	{
		if(!empty($date_from) AND !empty($date_to))
		{
			$add =  '  AND (payment_date >= \''.$date_from.'\' AND payment_date <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$add = '  AND payment_date = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$add = ' AND payment_date = \''.$date_to.'\'';
		}


		if(!empty($visit_type_id))
		{
			$add .= $visit_type_id;
		}
		if(!empty($patient_number))
		{
			$add .= $patient_number.' AND patients.patient_id = visit.patient_id';
			$table_add .= ',patients';
		}
		
		$this->db->where('cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_delete = 0 AND payments.payment_type = 1 '.$add);
		$this->db->select('SUM(amount_paid) AS total_payments');
		$query = $this->db->get('payments,payment,visit'.$table_add);
		$total_payments = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_payments = $value->total_payments;
			}
		}
		return $total_payments;
	}


	public function get_parent_account_id($account_name)
	{
		$account_id = 0;
		
		$this->db->select('account_id');
		$this->db->where('account_name = "'.$account_name.'"');
		$query = $this->db->get('account');
		
		$bal = $query->row();
		$account_id = $bal->account_id;
		// var_dump($account_id); die();
		return $account_id;
		
	}
	public function get_all_child_accounts($account_id = NULL)
	{
	 
	  $this->db->from('account');
	  $this->db->select('*');
	  $this->db->where('parent_account = '.$account_id);

	  $query = $this->db->get();

	   return $query;

	}



	public function get_expense_account_transactions($accountId,$type=NULL,$date_from=null,$date_to=null)
	{

		
		if(!empty($date_from) AND !empty($date_to))
		{
			$search_invoice_add =  ' AND (v_expenses_ledger.transactionDate >= \''.$date_from.'\' AND v_expenses_ledger.transactionDate <= \''.$date_to.'\') ';
		}
		else if(!empty($date_from))
		{
			$search_invoice_add = ' AND v_expenses_ledger.transactionDate = \''.$date_from.'\'';
		}
		else if(!empty($date_to))
		{
			$search_invoice_add = ' AND v_expenses_ledger.transactionDate = \''.$date_to.'\'';
		}
		




		
		$this->db->from('v_expenses_ledger');
		$this->db->select('SUM(v_expenses_ledger.dr_amount) AS dr_amount,SUM(v_expenses_ledger.cr_amount) AS cr_amount,v_expenses_ledger.accountName');
		$this->db->where('accountId = '.$accountId.$search_invoice_add);
		$this->db->order_by('accountId','ASC');
			$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
		

	}


	public function get_all_out_of_stock_items($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('product.stock_amount,product.whole_sale_stock','ASC');
		// $this->db->group_by('pos_order_item.product_id','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_product_expiries_items($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$store_id = $this->session->userdata('store_id');

		if($store_id == 5)
		{
			$this->db->order_by('product.last_expiry_date_retail','ASC');
		}
		else
		{
			$this->db->order_by('product.last_expiry_date_wholesale','ASC');
		}
		
		
	
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	public function get_all_product_stocks($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		//$this->db->join('st', 'staff.payroll_no = patients.strath_no', 'left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}



	public function get_all_product_stocks_p($table, $where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		//$this->db->join('st', 'staff.payroll_no = patients.strath_no', 'left');
		$query = $this->db->get('');
		
		return $query;
	}




	public function get_product_purchases($store_id,$product_id)
	{


		$where = 'v_creditor_ledger_aging.transactionId = orders.order_id AND (v_creditor_ledger_aging.transactionClassification = "Supplies Credit Note" OR v_creditor_ledger_aging.transactionClassification = "Supplies Invoices") AND v_creditor_ledger_aging.patient_id = '.$product_id;
		// $dashboard_date_from = $this->session->userdata('dashboard_date_from'); 
		// $dashboard_date_to = $this->session->userdata('dashboard_date_to'); 

		// if(!empty($dashboard_date_from) && !empty($dashboard_date_to))
		// {
		// 	$visit_date = ' AND v_creditor_ledger_aging.transactionDate BETWEEN \''.$dashboard_date_from.'\' AND \''.$dashboard_date_to.'\'';
	
		// }

		// else if(!empty($dashboard_date_from))
		// {
		// 	$visit_date = ' AND v_creditor_ledger_aging.transactionDate = \''.$dashboard_date_from.'\'';
			
		// }

		// else if(empty($dashboard_date_from) AND !empty($dashboard_date_to))
		// {
		// 	$visit_date = ' AND v_creditor_ledger_aging.transactionDate = \''.$dashboard_date_to.'\'';
		
		// }

		// else
		// {
		// 	$visit_date = ' AND v_creditor_ledger_aging.transactionDate = \''.date('Y-m-d').'\'';
			
		// }

		$where .= ' AND orders.store_id = '.$store_id;
		


		$this->db->where($where.$visit_date);
		$this->db->select('(SUM(v_creditor_ledger_aging.dr_amount) - SUM(v_creditor_ledger_aging.cr_amount)) AS total_amount');
		$query = $this->db->get('v_creditor_ledger_aging,orders');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;
	}



	public function get_drug_amounts_trail($product_id,$store_id)
	{

		$search_items = $this->session->userdata('stock_search');
		if(!empty($search_items))
		{
			
			$date_from = $this->session->userdata('date_from_stock');
			$date_to = $this->session->userdata('date_to_stock');

			if(!empty($date_from) && !empty($date_to))
			{
				$add = ' AND data.transactionDate >= "'.$date_from.'" AND data.transactionDate <= "'.$date_to.'"';
			}
			else if(!empty($date_from) AND empty($date_to))
			{
				
				$add = ' AND data.transactionDate = "'.$date_from.'"  ';
			}

			else if(!empty($date_to) AND empty($date_from))
			{
			
				$add = ' AND data.transactionDate = "'.$date_to.'"';
			}

			else
			{

				$start_date  = date('Y-m-d', strtotime('-7 days'));
				$end_date = date('Y-m-d');

				$add = ' AND data.transactionDate < "'.$start_date.'"';
			}
		}
		else
		{
			$start_date  = date('Y-m-d', strtotime('-7 days'));

			$add = ' AND data.transactionDate > "'.$start_date.'" AND data.transactionDate <= "'.date('Y-m-d').'"';
		}


		$select_statement  = "
							SELECT 
								data.transactionDate,
								SUM(data.dr_quantity) AS dr_quantity,
								SUM(data.dr_purchases) AS dr_purchases,
								SUM(data.cr_quantity) AS cr_quantity
							FROM
							 (SELECT
							    `store_product`.`store_product_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								store_product.owning_store_id AS `store_id`,
								'' AS `invoice_id`,
								'' AS `invoice_number`,
							    '' AS `receiving_store`,
								product.product_name AS `product_name`,
							    store.store_name AS `store_name`,
								CONCAT('Opening Balance of',' ',`product`.`product_name`) AS `transactionDescription`,
							    `store_product`.`store_quantity` AS `dr_quantity`,
							    '0' AS `cr_quantity`,
								(`product`.`product_unitprice` * `store_product`.`store_quantity` ) AS `dr_amount`,
								'0' AS `cr_amount`,
								0 AS `dr_purchases`,
								`store_product`.`created` AS `transactionDate`,
								`store_product`.`created` AS `referenceDate`,
								`product`.`product_status` AS `status`,
							    `product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Product Opening Stock' AS `transactionClassification`,
								'store_product' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM
							store_product,product,store
							WHERE  product.product_id = store_product.product_id AND product.product_deleted = 0
							AND store.store_id = store_product.owning_store_id AND store_product.product_id = ".$product_id."

							UNION ALL

							SELECT
							  `order_supplier`.`order_supplier_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								orders.store_id AS `store_id`,
								orders.order_id AS `invoice_id`,
								orders.supplier_invoice_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Purchase of',' ',`product`.`product_name`) AS `transactionDescription`,
								(quantity_received*pack_size) AS `dr_quantity`,
							    '0' AS `cr_quantity`,
								(order_supplier.total_amount) AS `dr_amount`,
								'0' AS `cr_amount`,
								(order_supplier.total_amount) AS `dr_purchases`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`order_supplier`.`created` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Supplier Purchases' AS `transactionClassification`,
								'order_item' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM (`order_item`, `order_supplier`, `product`, `orders`,store)
							WHERE
							`order_item`.`order_item_id` = order_supplier.order_item_id
							AND order_item.product_id = product.product_id
							AND orders.order_id = order_item.order_id
							AND product.product_deleted = 0
							AND orders.supplier_id > 0
							AND orders.is_store < 2
							AND orders.order_approval_status = 7
							AND product.product_id
							AND store.store_id = orders.store_id
							AND order_item.product_id = ".$product_id."


							UNION ALL

							SELECT
								`product_purchase`.`purchase_id` AS transactionId,
							    `product_purchase`.`product_id` AS `product_id`,
							    `product`.`category_id` AS `category_id`,
								 product_purchase.store_id AS `store_id`,
								 '' AS `invoice_id`,
								 '' AS `invoice_number`,
								 '' AS `receiving_store`,
								 product.product_name AS `product_name`,
							  	store.store_name AS `store_name`,
								product_purchase.purchase_description AS `transactionDescription`,
							    (purchase_quantity * purchase_pack_size) AS `dr_quantity`,
							    '0' AS `cr_quantity`,
								(`product`.`product_unitprice` * (purchase_quantity * purchase_pack_size) ) AS `dr_amount`,
								'0' AS `cr_amount`,
								(`product`.`product_unitprice` * (purchase_quantity * purchase_pack_size) )  AS `dr_purchases`,
								`product_purchase`.`purchase_date` AS `transactionDate`,
								`product_purchase`.`purchase_date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
							  `product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Product Addition' AS `transactionClassification`,
								'product_purchase' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`product_purchase`,product,store)
							WHERE product.product_id = product_purchase.product_id AND product.product_deleted = 0
							AND store.store_id = product_purchase.store_id
							AND product_purchase.product_id = ".$product_id."


							UNION ALL


							SELECT
							`product_deductions_stock`.`product_deductions_stock_id` AS transactionId,
							`product_deductions_stock`.`product_id` AS `product_id`,
							`product`.`category_id` AS `category_id`,
							 product_deductions_stock.store_id AS `store_id`,
							 '' AS `invoice_id`,
							 '' AS `invoice_number`,
							 '' AS `receiving_store`,
							 product.product_name AS `product_name`,
							store.store_name AS `store_name`,
							 product_deductions_stock.deduction_description AS `transactionDescription`,
							 '0' AS `dr_quantity`,
							 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS  `cr_quantity`,
							'0' AS `dr_amount`,
							 (`product`.`product_unitprice` * (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
							 0 AS `dr_purchases`,
							`product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`,
							`product_deductions_stock`.`product_deductions_stock_date` AS `referenceDate`,
							`product`.`product_status` AS `status`,
							`product`.`product_deleted` AS `product_deleted`,
							'Expense' AS `transactionCategory`,
							'Product Deductions' AS `transactionClassification`,
							'product_deductions_stock' AS `transactionTable`,
							'product' AS `referenceTable`
							FROM (`product_deductions_stock`,product,store)
							WHERE product.product_id = product_deductions_stock.product_id AND product.product_deleted = 0
							AND store.store_id = product_deductions_stock.store_id
							AND product_deductions_stock.product_id = ".$product_id."


							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								orders.store_id AS `store_id`,
								orders.order_id AS `invoice_id`,
								orders.supplier_invoice_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Credit note of',' ',`product`.`product_name`) AS `transactionDescription`,
								 '0' AS `dr_quantity`,
							   (quantity_received*pack_size) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(order_supplier.total_amount) AS `cr_amount`,
								 -(order_supplier.total_amount) AS `dr_purchases`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								order_supplier.created AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Supplier Credit Note' AS `transactionClassification`,
								'order_item' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM (`order_item`, `order_supplier`, `product`, `orders`,store)
							WHERE `order_item`.`order_item_id` = order_supplier.order_item_id
							AND order_item.product_id = product.product_id
							AND orders.order_id = order_item.order_id
							AND product.product_deleted = 0
							AND orders.supplier_id > 0
							AND orders.order_approval_status = 7
							AND orders.is_store = 3
							AND store.store_id = orders.store_id
							AND order_item.product_id = ".$product_id."


							UNION ALL


							SELECT
								`product_return_stock`.`product_deductions_stock_id` AS transactionId,
								`product_return_stock`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								 product_return_stock.from_store_id AS `store_id`,
								 '' AS `invoice_id`,
								 '' AS `invoice_number`,
								 product_return_stock.to_store_id AS `receiving_store`,
								 product.product_name AS `product_name`,
								 store.store_name AS `store_name`,
								 CONCAT('Store Transfer') AS `transactionDescription`,
								 '0' AS `dr_quantity`,
								 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
								0 AS `dr_purchases`,
								`product_return_stock`.`product_deductions_stock_date` AS `transactionDate`,
								`product_return_stock`.`product_deductions_stock_date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Product Addition' AS `transactionClassification`,
								'product_return_stock' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`product_return_stock`,product,store)
							WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
							AND store.store_id = product_return_stock.from_store_id
							AND  product_return_stock.product_id = ".$product_id."

							UNION ALL

							SELECT
								`product_return_stock`.`product_deductions_stock_id` AS transactionId,
								`product_return_stock`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								 product_return_stock.to_store_id AS `store_id`,
								 '' AS `invoice_id`,
								 '' AS `invoice_number`,
								 product_return_stock.from_store_id AS `receiving_store`,
								 product.product_name AS `product_name`,
								 store.store_name AS `store_name`,
								 CONCAT('Store Transfer') AS `transactionDescription`,
								 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS `dr_quantity`,
								 '0' AS `cr_quantity`,
								 (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `dr_amount`,
								 '0' AS `cr_amount`,
								 0 AS `dr_purchases`,
								`product_return_stock`.`product_deductions_stock_date` AS `transactionDate`,
								`product_return_stock`.`product_deductions_stock_date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Store Deduction' AS `transactionClassification`,
								'product_return_stock' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`product_return_stock`,product,store)
							WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
							AND store.store_id = product_return_stock.to_store_id
							AND product_return_stock.product_id = ".$product_id."


							UNION ALL


							SELECT
								`visit_charge`.`visit_charge_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								visit_charge.store_id AS `store_id`,
								'' AS `invoice_id`,
								'' AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Product Sale',' ',`product`.`product_name`) AS `transactionDescription`,
								'0' AS `dr_quantity`,
							    (visit_charge.visit_charge_units) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(visit_charge.visit_charge_units * visit_charge.buying_price) AS `cr_amount`,
								0 AS `dr_purchases`,
								`visit_charge`.`date` AS `transactionDate`,
								`visit_charge`.`date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Drug Sales' AS `transactionClassification`,
								'visit_charge' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`visit_charge`,product,store)
							WHERE `visit_charge`.`charged` = 1
							AND visit_charge.visit_charge_delete = 0
							AND product.product_id = visit_charge.product_id AND product.product_deleted = 0 
							AND store.store_id = visit_charge.store_id
							AND visit_charge.product_id = ".$product_id."


							UNION ALL

							SELECT
								`pos_order_item`.`pos_order_item_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								pos_order_item.store_id AS `store_id`,
								pos_order.pos_order_id AS `invoice_id`,
								pos_order.pos_order_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Product Sale',' ',`product`.`product_name`) AS `transactionDescription`,
								'0' AS `dr_quantity`,
							    (pos_order_item.pos_order_item_quantity * pos_order_item.pack_size) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(pos_order_item.pos_order_item_quantity * pos_order_item.pos_order_item_amount) AS `cr_amount`,
								0 AS `dr_purchases`,
								DATE(`pos_order_item`.`created`) AS `transactionDate`,
								DATE(`pos_order_item`.`created`) AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Drug Sales' AS `transactionClassification`,
								'pos_order_item' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`pos_order_item`,product,store,pos_order)
							WHERE `pos_order_item`.`charged` = 1
							AND pos_order_item.pos_order_item_deleted = 0
							AND product.product_id = pos_order_item.product_id 
							AND product.product_deleted = 0 
							AND store.store_id = pos_order_item.store_id
							AND pos_order_item.order_invoice_id > 0
							AND pos_order.pos_order_id = pos_order_item.pos_order_id
							AND pos_order.pos_order_deleted = 0
							AND pos_order.sale_type < 3 
						
							AND pos_order_item.product_id = ".$product_id."


							UNION ALL


							SELECT
								`pos_order_item_return`.`pos_order_item_return_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								pos_order_item_return.store_id AS `store_id`,
								'' AS `invoice_id`,
								pos_order_return.pos_order_return_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Product Return',' ',`product`.`product_name`) AS `transactionDescription`,
								(pos_order_item_return.units * pos_order_item_return.pack_size) AS `dr_quantity`,
							    0 AS `cr_quantity`,
								(pos_order_item_return.units * pos_order_item_return.product_order_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								0 AS `dr_purchases`,
								DATE(`pos_order_item_return`.`created`) AS `transactionDate`,
								DATE(`pos_order_item_return`.`created`) AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Sale Return' AS `transactionClassification`,
								'pos_order_item_return' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`pos_order_item_return`,pos_order_return,product,store)
							WHERE pos_order_item_return.pos_order_item_return_delete = 0
							AND product.product_id = pos_order_item_return.product_id 
							AND product.product_deleted = 0 
							AND store.store_id = pos_order_item_return.store_id
							AND pos_order_item_return.order_invoice_id > 0
							AND pos_order_return.pos_order_return_id = pos_order_item_return.pos_order_return_id
							AND pos_order_return.pos_order_return_deleted = 0
							
						
							AND pos_order_item_return.product_id = ".$product_id."




							UNION ALL 

							SELECT
							`product_deductions`.`product_deductions_id` AS `transactionId`,
							`product`.`product_id` AS `product_id`,
							`product`.`category_id` AS `category_id`,
							`store`.`store_id` AS `store_id`,
							'' AS `invoice_id`,
							'' AS `invoice_number`,
							'' AS `receiving_store`,
							product.product_name AS `product_name`,
							store.store_name AS `store_name`,
							CONCAT('Product Added',' ',`product`.`product_name`) AS `transactionDescription`,
							  '0'  AS `dr_quantity`,
							 (quantity_given*pack_size) AS `cr_quantity`,
							 '0' AS `dr_amount`,
							 (product.product_unitprice * (quantity_given*pack_size) ) AS `cr_amount`,
							 0 AS `dr_purchases`,
							`product_deductions`.`search_date` AS `transactionDate`,
							`product_deductions`.`search_date` AS `referenceDate`,
							`product`.`product_status` AS `status`,
							`product`.`product_deleted` AS `product_deleted`,
							'Expense' AS `transactionCategory`,
							'Drug Transfer' AS `transactionClassification`,
							'product_deductions' AS `transactionTable`,
							'product' AS `referenceTable`
							FROM (`product_deductions`, `product`, `orders`,store)
							WHERE `product_deductions`.`order_id` = orders.order_id
							AND product_deductions.product_id = product.product_id
							AND product.product_deleted = 0
							AND orders.supplier_id > 0
							AND orders.is_store = 2
							AND orders.order_approval_status = 7
							AND `orders`.`store_id` = store.store_id
							AND product_deductions.product_id = ".$product_id."




						) AS data WHERE data.store_id = ".$store_id." ".$add." GROUP BY data.transactionDate ORDER BY data.transactionDate ASC  ";

				$query = $this->db->query($select_statement);
				$result = '';
				$total_cr_amount = 0;
				$total_dr_amount = 0;
				$total_arrears = 0;
				$total_dr_purchases = 0;
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...

						$dr_quantity = $value->dr_quantity;
						$dr_purchases = $value->dr_purchases;
						$cr_quantity = $value->cr_quantity;

						

						$total_arrears += $dr_quantity - $cr_quantity;

						$total_dr_amount += $dr_quantity;
						$total_cr_amount += $cr_quantity;
						$total_dr_purchases += $dr_purchases;

						
					}
				}
						
		$response['total_cr_amount'] = $total_cr_amount;
		$response['total_dr_amount'] = $total_dr_amount;
		$response['total_dr_purchases'] = $total_dr_purchases;


		return $response;
	}


	public function get_drug_amounts_trail_opening($product_id,$store_id)
	{

		$search_items = $this->session->userdata('stock_search');
		if(!empty($search_items))
		{

			$date_from = $this->session->userdata('date_from_stock');
			$date_to = $this->session->userdata('date_to_stock');

			if(!empty($date_from) && !empty($date_to))
			{
				$add = ' AND data.transactionDate < "'.$date_from.'"';
			}
			else if(!empty($date_from) AND empty($date_to))
			{
				$add = ' AND data.transactionDate < "'.$date_from.'"';
			}

			else if(!empty($date_to) AND empty($date_from))
			{
			
				$add = ' AND data.transactionDate < "'.$date_to.'"';
			}

			else
			{

				$start_date  = date('Y-m-d', strtotime('-7 days'));
				$end_date = date('Y-m-d');

				$add = ' AND data.transactionDate < "'.$start_date.'"';
			}
	
		}
		else
		{
			$start_date  = date('Y-m-d', strtotime('-7 days'));

			$add = ' AND data.transactionDate < "'.$start_date.'"';
		}
		// if($store_id == 6)
		// {
		// 		var_dump($add);die();
		// }
	
		$select_statement  = "
							SELECT 
								data.transactionDate,
								SUM(data.dr_quantity) AS dr_quantity,
								SUM(data.cr_quantity) AS cr_quantity
							FROM
							 (SELECT
							    `store_product`.`store_product_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								store_product.owning_store_id AS `store_id`,
								'' AS `invoice_id`,
								'' AS `invoice_number`,
							    '' AS `receiving_store`,
								product.product_name AS `product_name`,
							    store.store_name AS `store_name`,
								CONCAT('Opening Balance of',' ',`product`.`product_name`) AS `transactionDescription`,
							    `store_product`.`store_quantity` AS `dr_quantity`,
							    '0' AS `cr_quantity`,
								(`product`.`product_unitprice` * `store_product`.`store_quantity` ) AS `dr_amount`,
								'0' AS `cr_amount`,
								`store_product`.`created` AS `transactionDate`,
								`store_product`.`created` AS `referenceDate`,
								`product`.`product_status` AS `status`,
							    `product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Product Opening Stock' AS `transactionClassification`,
								'store_product' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM
							store_product,product,store
							WHERE  product.product_id = store_product.product_id AND product.product_deleted = 0
							AND store.store_id = store_product.owning_store_id AND store_product.product_id = ".$product_id."

							UNION ALL

							SELECT
							  `order_supplier`.`order_supplier_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								orders.store_id AS `store_id`,
								orders.order_id AS `invoice_id`,
								orders.supplier_invoice_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Purchase of',' ',`product`.`product_name`) AS `transactionDescription`,
								(quantity_received*pack_size) AS `dr_quantity`,
							    '0' AS `cr_quantity`,
								(order_supplier.total_amount) AS `dr_amount`,
								'0' AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								`order_supplier`.`created` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Supplier Purchases' AS `transactionClassification`,
								'order_item' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM (`order_item`, `order_supplier`, `product`, `orders`,store)
							WHERE
							`order_item`.`order_item_id` = order_supplier.order_item_id
							AND order_item.product_id = product.product_id
							AND orders.order_id = order_item.order_id
							AND product.product_deleted = 0
							AND orders.supplier_id > 0
							AND orders.is_store < 2
							AND orders.order_approval_status = 7
							AND product.product_id
							AND store.store_id = orders.store_id
							AND order_item.product_id = ".$product_id."


							UNION ALL

							SELECT
								`product_purchase`.`purchase_id` AS transactionId,
							    `product_purchase`.`product_id` AS `product_id`,
							    `product`.`category_id` AS `category_id`,
								 product_purchase.store_id AS `store_id`,
								 '' AS `invoice_id`,
								 '' AS `invoice_number`,
								 '' AS `receiving_store`,
								 product.product_name AS `product_name`,
							  	store.store_name AS `store_name`,
								product_purchase.purchase_description AS `transactionDescription`,
							    (purchase_quantity * purchase_pack_size) AS `dr_quantity`,
							    '0' AS `cr_quantity`,
								(`product`.`product_unitprice` * (purchase_quantity * purchase_pack_size) ) AS `dr_amount`,
								'0' AS `cr_amount`,
								`product_purchase`.`purchase_date` AS `transactionDate`,
								`product_purchase`.`purchase_date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
							  `product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Product Addition' AS `transactionClassification`,
								'product_purchase' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`product_purchase`,product,store)
							WHERE product.product_id = product_purchase.product_id AND product.product_deleted = 0
							AND store.store_id = product_purchase.store_id
							AND product_purchase.product_id = ".$product_id."


							UNION ALL


							SELECT
							`product_deductions_stock`.`product_deductions_stock_id` AS transactionId,
							`product_deductions_stock`.`product_id` AS `product_id`,
							`product`.`category_id` AS `category_id`,
							 product_deductions_stock.store_id AS `store_id`,
							 '' AS `invoice_id`,
							 '' AS `invoice_number`,
							 '' AS `receiving_store`,
							 product.product_name AS `product_name`,
							store.store_name AS `store_name`,
							 product_deductions_stock.deduction_description AS `transactionDescription`,
							 '0' AS `dr_quantity`,
							 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS  `cr_quantity`,
							'0' AS `dr_amount`,
							 (`product`.`product_unitprice` * (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
							`product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`,
							`product_deductions_stock`.`product_deductions_stock_date` AS `referenceDate`,
							`product`.`product_status` AS `status`,
							`product`.`product_deleted` AS `product_deleted`,
							'Expense' AS `transactionCategory`,
							'Product Deductions' AS `transactionClassification`,
							'product_deductions_stock' AS `transactionTable`,
							'product' AS `referenceTable`
							FROM (`product_deductions_stock`,product,store)
							WHERE product.product_id = product_deductions_stock.product_id AND product.product_deleted = 0
							AND store.store_id = product_deductions_stock.store_id
							AND product_deductions_stock.product_id = ".$product_id."


							UNION ALL

							SELECT
								`order_supplier`.`order_supplier_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								orders.store_id AS `store_id`,
								orders.order_id AS `invoice_id`,
								orders.supplier_invoice_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Credit note of',' ',`product`.`product_name`) AS `transactionDescription`,
								 '0' AS `dr_quantity`,
							   (quantity_received*pack_size) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(order_supplier.total_amount) AS `cr_amount`,
								`orders`.`supplier_invoice_date` AS `transactionDate`,
								order_supplier.created AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Supplier Credit Note' AS `transactionClassification`,
								'order_item' AS `transactionTable`,
								'orders' AS `referenceTable`
							FROM (`order_item`, `order_supplier`, `product`, `orders`,store)
							WHERE `order_item`.`order_item_id` = order_supplier.order_item_id
							AND order_item.product_id = product.product_id
							AND orders.order_id = order_item.order_id
							AND product.product_deleted = 0
							AND orders.supplier_id > 0
							AND orders.order_approval_status = 7
							AND orders.is_store = 3
							AND store.store_id = orders.store_id
							AND order_item.product_id = ".$product_id."


							UNION ALL


							SELECT
								`product_return_stock`.`product_deductions_stock_id` AS transactionId,
								`product_return_stock`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								 product_return_stock.from_store_id AS `store_id`,
								 '' AS `invoice_id`,
								 '' AS `invoice_number`,
								 product_return_stock.to_store_id AS `receiving_store`,
								 product.product_name AS `product_name`,
								 store.store_name AS `store_name`,
								 CONCAT('Store Transfer') AS `transactionDescription`,
								 '0' AS `dr_quantity`,
								 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
								`product_return_stock`.`product_deductions_stock_date` AS `transactionDate`,
								`product_return_stock`.`product_deductions_stock_date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Product Addition' AS `transactionClassification`,
								'product_return_stock' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`product_return_stock`,product,store)
							WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
							AND store.store_id = product_return_stock.from_store_id
							AND  product_return_stock.product_id = ".$product_id."

							UNION ALL

							SELECT
								`product_return_stock`.`product_deductions_stock_id` AS transactionId,
								`product_return_stock`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								 product_return_stock.to_store_id AS `store_id`,
								 '' AS `invoice_id`,
								 '' AS `invoice_number`,
								 product_return_stock.from_store_id AS `receiving_store`,
								 product.product_name AS `product_name`,
								 store.store_name AS `store_name`,
								 CONCAT('Store Transfer') AS `transactionDescription`,
								 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS `dr_quantity`,
								 '0' AS `cr_quantity`,
								 (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `dr_amount`,
								 '0' AS `cr_amount`,
								`product_return_stock`.`product_deductions_stock_date` AS `transactionDate`,
								`product_return_stock`.`product_deductions_stock_date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Income' AS `transactionCategory`,
								'Store Deduction' AS `transactionClassification`,
								'product_return_stock' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`product_return_stock`,product,store)
							WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
							AND store.store_id = product_return_stock.to_store_id
							AND product_return_stock.product_id = ".$product_id."


							UNION ALL


							SELECT
								`visit_charge`.`visit_charge_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								visit_charge.store_id AS `store_id`,
								'' AS `invoice_id`,
								'' AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Product Sale',' ',`product`.`product_name`) AS `transactionDescription`,
								'0' AS `dr_quantity`,
							    (visit_charge.visit_charge_units) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(visit_charge.visit_charge_units * visit_charge.buying_price) AS `cr_amount`,
								`visit_charge`.`date` AS `transactionDate`,
								`visit_charge`.`date` AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Drug Sales' AS `transactionClassification`,
								'visit_charge' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`visit_charge`,product,store)
							WHERE `visit_charge`.`charged` = 1
							AND visit_charge.visit_charge_delete = 0
							AND product.product_id = visit_charge.product_id AND product.product_deleted = 0 
							AND store.store_id = visit_charge.store_id
							AND visit_charge.product_id = ".$product_id."


							UNION ALL

							SELECT
								`pos_order_item`.`pos_order_item_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								pos_order_item.store_id AS `store_id`,
								pos_order.pos_order_id AS `invoice_id`,
								pos_order.pos_order_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Product Sale',' ',`product`.`product_name`) AS `transactionDescription`,
								'0' AS `dr_quantity`,
							    (pos_order_item.pos_order_item_quantity * pos_order_item.pack_size) AS `cr_quantity`,
								 '0' AS `dr_amount`,
								(pos_order_item.pos_order_item_quantity * pos_order_item.pos_order_item_amount) AS `cr_amount`,
								DATE(`pos_order_item`.`created`) AS `transactionDate`,
								DATE(`pos_order_item`.`created`) AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Drug Sales' AS `transactionClassification`,
								'pos_order_item' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`pos_order_item`,product,store,pos_order)
							WHERE `pos_order_item`.`charged` = 1
							AND pos_order_item.pos_order_item_deleted = 0
							AND product.product_id = pos_order_item.product_id 
							AND product.product_deleted = 0 
							AND store.store_id = pos_order_item.store_id
							AND pos_order_item.order_invoice_id > 0
							AND pos_order.pos_order_id = pos_order_item.pos_order_id
							AND pos_order.pos_order_deleted = 0
							AND pos_order.sale_type < 3 
						
							AND pos_order_item.product_id = ".$product_id."


							UNION ALL


							SELECT
								`pos_order_item_return`.`pos_order_item_return_id` AS `transactionId`,
								`product`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								pos_order_item_return.store_id AS `store_id`,
								'' AS `invoice_id`,
								pos_order_return.pos_order_return_number AS `invoice_number`,
								'' AS `receiving_store`,
								product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								CONCAT('Product Return',' ',`product`.`product_name`) AS `transactionDescription`,
								(pos_order_item_return.units * pos_order_item_return.pack_size) AS `dr_quantity`,
							    0 AS `cr_quantity`,
								(pos_order_item_return.units * pos_order_item_return.product_order_item_amount) AS `dr_amount`,
								0 AS `cr_amount`,
								DATE(`pos_order_item_return`.`created`) AS `transactionDate`,
								DATE(`pos_order_item_return`.`created`) AS `referenceDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								'Expense' AS `transactionCategory`,
								'Sale Return' AS `transactionClassification`,
								'pos_order_item_return' AS `transactionTable`,
								'product' AS `referenceTable`
							FROM (`pos_order_item_return`,pos_order_return,product,store)
							WHERE pos_order_item_return.pos_order_item_return_delete = 0
							AND product.product_id = pos_order_item_return.product_id 
							AND product.product_deleted = 0 
							AND store.store_id = pos_order_item_return.store_id
							AND pos_order_item_return.order_invoice_id > 0
							AND pos_order_return.pos_order_return_id = pos_order_item_return.pos_order_return_id
							AND pos_order_return.pos_order_return_deleted = 0
							
						
							AND pos_order_item_return.product_id = ".$product_id."




							UNION ALL 

							SELECT
							`product_deductions`.`product_deductions_id` AS `transactionId`,
							`product`.`product_id` AS `product_id`,
							`product`.`category_id` AS `category_id`,
							`store`.`store_id` AS `store_id`,
							'' AS `invoice_id`,
							'' AS `invoice_number`,
							'' AS `receiving_store`,
							product.product_name AS `product_name`,
							store.store_name AS `store_name`,
							CONCAT('Product Added',' ',`product`.`product_name`) AS `transactionDescription`,
							  '0'  AS `dr_quantity`,
							 (quantity_given*pack_size) AS `cr_quantity`,
							 '0' AS `dr_amount`,
							 (product.product_unitprice * (quantity_given*pack_size) ) AS `cr_amount`,
							`product_deductions`.`search_date` AS `transactionDate`,
							`product_deductions`.`search_date` AS `referenceDate`,
							`product`.`product_status` AS `status`,
							`product`.`product_deleted` AS `product_deleted`,
							'Expense' AS `transactionCategory`,
							'Drug Transfer' AS `transactionClassification`,
							'product_deductions' AS `transactionTable`,
							'product' AS `referenceTable`
							FROM (`product_deductions`, `product`, `orders`,store)
							WHERE `product_deductions`.`order_id` = orders.order_id
							AND product_deductions.product_id = product.product_id
							AND product.product_deleted = 0
							AND orders.supplier_id > 0
							AND orders.is_store = 2
							AND orders.order_approval_status = 7
							AND `orders`.`store_id` = store.store_id
							AND product_deductions.product_id = ".$product_id."




						) AS data WHERE data.store_id = ".$store_id." ".$add." GROUP BY data.transactionDate ORDER BY data.transactionDate ASC  ";

				$query = $this->db->query($select_statement);
				$result = '';
				$total_cr_amount = 0;
				$total_dr_amount = 0;
				$total_arrears = 0;
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...

						$dr_quantity = $value->dr_quantity;
						$cr_quantity = $value->cr_quantity;

						

						$total_arrears += $dr_quantity - $cr_quantity;

						$total_dr_amount += $dr_quantity;
						$total_cr_amount += $cr_quantity;

						
					}
				}
						
		$response['total_cr_amount'] = $total_cr_amount;
		$response['total_dr_amount'] = $total_dr_amount;


		return $response;
	}

	public function export_product_valuations()
	{


		$this->load->library('excel');
		
		//get all transactions
		$where = 'product.product_deleted = 0 AND product.stock_take = 1 AND product_status = 1 AND category.category_id = product.category_id';
		$table = 'product,category';
		$product_stock_search = $this->session->userdata('product_stock_search');

		if(!empty($product_stock_search))
		{
			$where .= $product_stock_search;
		}
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$visits_query = $this->db->get('');
		
		$title = 'STOCK VALUATION REPORT';
		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'PRODUCT CODE';
			$report[$row_count][2] = 'PRODUCT NAME';
			$report[$row_count][3] = 'PRODUCT CODE';
			$report[$row_count][4] = 'RETAIL OPENING STOCK';
			$report[$row_count][5] = 'RETAIL PURCHASES';
			$report[$row_count][6] = 'RETAIL SALES';
			$report[$row_count][7] = 'RETAIL CLOSING STOCK';
			$report[$row_count][8] = 'WHOLESALE OPENING STOCK';
			$report[$row_count][9] = 'WHOLESALE PURCHASES';
			$report[$row_count][10] = 'WHOLESALE SALES';
			$report[$row_count][11] = 'WHOLESALE CLOSING STOCK';
			$report[$row_count][12] = 'TOTAL STOCK';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row => $value)
			{
				$row_count++;
				

				$product_code = $value->product_code;
				$product_name = $value->product_name;
				$stock_amount = $value->stock_amount;
				$product_code = $value->product_code;
				$whole_sale_stock = $value->whole_sale_stock;
				$reorder_level = $value->reorder_level;
				$product_id = $value->product_id;


				$retail_opening_view = $this->get_drug_amounts_trail_opening($product_id,5);


				$retail_opening_purchases = $retail_opening_view['total_dr_amount'];
				$retail_opening_sales = $retail_opening_view['total_cr_amount'];
				$retail_opening = $retail_opening_purchases - $retail_opening_sales;


				$wholesale_opening_view = $this->get_drug_amounts_trail_opening($product_id,6);

				$wholesale_opening_purchases = $wholesale_opening_view['total_dr_amount'];
				$wholesale_opening_sales = $wholesale_opening_view['total_cr_amount'];
				$wholesale_opening = $wholesale_opening_purchases - $wholesale_opening_sales;

				// var_dump($wholesale_opening);die();


				$retail_view = $this->reports_model->get_drug_amounts_trail($product_id,5);
				$wholesale_view = $this->reports_model->get_drug_amounts_trail($product_id,6);

				$retail_purchases = $retail_view['total_dr_amount'];
				$retail_sales = $retail_view['total_cr_amount'];

				$retail_total_purchases = $retail_view['total_dr_purchases'];

				$wholesale_purchases = $wholesale_view['total_dr_amount'];
				$wholesale_sales = $wholesale_view['total_cr_amount'];

				$wholesale_total_purchases = $wholesale_view['total_dr_purchases'];



				$retail_closing = ($retail_opening + $retail_purchases) - $retail_sales;
				$wholesale_closing = ($wholesale_opening + $wholesale_purchases) - $wholesale_sales;
	
				$total_closing = $retail_closing + $wholesale_closing;

				$count++;



				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $product_code;
				$report[$row_count][2] = $product_name;
				$report[$row_count][3] = $product_code;
				$report[$row_count][4] = $retail_opening;
				$report[$row_count][5] = $retail_purchases;
				$report[$row_count][6] = $retail_total_purchases;
				$report[$row_count][7] = $retail_sales;
				$report[$row_count][8] = $retail_closing;
				$report[$row_count][9] = $wholesale_opening;
				$report[$row_count][10] = $wholesale_purchases;
				$report[$row_count][11] = $wholesale_total_purchases;
				$report[$row_count][12] = $wholesale_sales;
				$report[$row_count][13] = $wholesale_closing;
				$report[$row_count][14] = $total_closing;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}

	public function get_report_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,service_charge.service_charge_id,service.service_name');

		$this->db->where($where);
		$this->db->order_by('total_count','DESC');
		$this->db->group_by('service_charge.service_charge_id');
		$query = $this->db->get('');
		
		return $query;
	}


	public function get_lab_report_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,service_charge.service_charge_id,service.service_name');

		$this->db->where($where);
		$this->db->order_by('total_count','DESC');
		$this->db->group_by('service_charge.lab_test_id');
		$query = $this->db->get('');
		
		return $query;
	}

	public function export_ultra_report()
	{

		$this->load->library('excel');
		
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Ultrasound")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('ultra_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->reports_model->get_report_content($table, $where,'service_charge.service_charge_name' ,'ASC');
		
		$ultra_title_search = $this->session->userdata('ultra_title_search');
		$title = 'RADIOLOGY REPORT '.$ultra_title_search;
		
		if($query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'PROCEDURE NAME';
			$report[$row_count][2] = 'PROCEDURE COUNT';
			$report[$row_count][3] = 'RATE PER PROCEDURE';
			$report[$row_count][4] = 'REVENUE';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($query->result() as $row => $row)
			{
				$row_count++;
				

				$service_charge_name = $row->service_charge_name;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$service_charge_amount = $row->service_charge_amount;
				$service_charge_id = $row->service_charge_id;
				$service_name = $row->service_name;
				if(empty($service_charge_amount))
				{
					$service_charge_amount = 0;
				}
				
				
				$count++;
				
			
				$total_amount += $total_revenue;




				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $service_charge_name;
				$report[$row_count][2] = $total_count;
				$report[$row_count][3] = number_format($service_charge_amount,2);
				$report[$row_count][4] = number_format($total_revenue,2);
					
				
				
			}

			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = 'TOTAL';
			$report[$row_count][3] = '';
			$report[$row_count][4] = number_format($total_amount,2);
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}


	public function export_visit_ultrasound_report($service_charge_id)
	{
		$this->load->library('excel');
		
		//get all transactions

		$branch_id = $this->session->userdata('branch_id');

		// var_dump($branch_id);die();
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Ultrasound") AND visit.patient_id = patients.patient_id';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service,patients';
		$visit_report_search = $this->session->userdata('ultra_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}


		if(!empty($service_charge_id))
		{
			$where .= ' AND visit_charge.service_charge_id = '.$service_charge_id;
		}
		
		$this->db->where($where);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,patients.patient_othernames,patients.patient_surname,visit.visit_date');
		$this->db->order_by('visit.visit_date','ASC');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);

		// var_dump($visits_query);die();
		$title = $this->session->userdata('ultra_title_search');
		
		$title = 'RADIOLOGY REPORT '.$title.' PATIENTS';

		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient Name';
			$report[$row_count][3] = 'Procedure Count';
			$report[$row_count][4] = 'Rate';
			$report[$row_count][5] = 'Revenue';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$service_charge_name = $row->service_charge_name;
				$patient_surname = $row->patient_surname;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$visit_date = $row->visit_date;
				$service_charge_amount = $row->service_charge_amount;

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_surname;
				$report[$row_count][3] = $total_count;
				$report[$row_count][4] = $service_charge_amount;
				$report[$row_count][5] = $total_revenue;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function export_dental_report()
	{

		$this->load->library('excel');
		
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Dental")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('dental_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->reports_model->get_report_content($table, $where,'service_charge.service_charge_name' ,'ASC');
		
		$ultra_title_search = $this->session->userdata('dental_title_search');
		$title = 'DENTAL REPORT '.$ultra_title_search;
		
		if($query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'PROCEDURE NAME';
			$report[$row_count][2] = 'PROCEDURE COUNT';
			$report[$row_count][3] = 'RATE PER PROCEDURE';
			$report[$row_count][4] = 'REVENUE';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($query->result() as $row => $row)
			{
				$row_count++;
				

				$service_charge_name = $row->service_charge_name;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$service_charge_amount = $row->service_charge_amount;
				$service_charge_id = $row->service_charge_id;
				$service_name = $row->service_name;
				if(empty($service_charge_amount))
				{
					$service_charge_amount = 0;
				}
				
				
				$count++;
				
			
				$total_amount += $total_revenue;




				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $service_charge_name;
				$report[$row_count][2] = $total_count;
				$report[$row_count][3] = number_format($service_charge_amount,2);
				$report[$row_count][4] = number_format($total_revenue,2);
					
				
				
			}

			$report[$row_count][0] = '';
			$report[$row_count][1] = '';
			$report[$row_count][2] = 'TOTAL';
			$report[$row_count][3] = '';
			$report[$row_count][4] = number_format($total_amount,2);
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}

	public function export_visit_dental_report($service_charge_id)
	{
		$this->load->library('excel');
		
		//get all transactions

		$branch_id = $this->session->userdata('branch_id');

		// var_dump($branch_id);die();
		//get all transactions
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Dental") AND visit.patient_id = patients.patient_id';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service,patients';
		$visit_report_search = $this->session->userdata('ultra_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}


		if(!empty($service_charge_id))
		{
			$where .= ' AND visit_charge.service_charge_id = '.$service_charge_id;
		}
		
		$this->db->where($where);
		$this->db->select('service_charge_name,sum(visit_charge.visit_charge_units) AS total_count,sum(visit_charge.visit_charge_units*visit_charge.visit_charge_amount) AS total_revenue,service_charge.service_charge_amount,patients.patient_othernames,patients.patient_surname,visit.visit_date');
		$this->db->order_by('visit.visit_date','ASC');
		$this->db->group_by('visit.visit_id');
		$visits_query = $this->db->get($table);

		// var_dump($visits_query);die();
		$title = $this->session->userdata('dental_title_search');
		
		$title = 'DENTAL REPORT '.$title.' PATIENTS';

		
		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Visit Date';
			$report[$row_count][2] = 'Patient Name';
			$report[$row_count][3] = 'Procedure Count';
			$report[$row_count][4] = 'Rate';
			$report[$row_count][5] = 'Revenue';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;
				$service_charge_name = $row->service_charge_name;
				$patient_surname = $row->patient_surname;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$visit_date = $row->visit_date;
				$service_charge_amount = $row->service_charge_amount;

				$count++;
				
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $visit_date;
				$report[$row_count][2] = $patient_surname;
				$report[$row_count][3] = $total_count;
				$report[$row_count][4] = $service_charge_amount;
				$report[$row_count][5] = $total_revenue;
					
				
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


}


?>