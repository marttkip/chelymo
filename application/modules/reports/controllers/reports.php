<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once "./application/modules/administration/controllers/administration.php";
error_reporting(0);
class Reports extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reception/reception_model');
		$this->load->model('reports/reports_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/dashboard_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/email_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('reception/database');
		$this->load->model('inventory/categories_model');
		$this->load->model('inventory/stores_model');
		$this->load->model('inventory_management/products_model');
		$this->load->model('administration/reports_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('reception/reception_model');

		$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}

	public function summaries_dashboard() 
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		// if($page == NULL)
		// {
		// 	$page = 0;
		// }
		$page = 0;
		
		$table= 'visit,patients';
		$where='visit.close_card = 2 AND visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_date = "'.date('Y-m-d').'" AND preauth = 0';
		$config["per_page"] = $v_data['per_page'] = $per_page = 30;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}
		$v_data['page'] = $page;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->admin_model->get_all_visits_parent($table, $where, $config["per_page"], $page);

		$v_data['appointment_list'] = $query;


		$table= 'visit,patients';
		$where='(visit.close_card = 0 OR visit.close_card = 1 OR visit.close_card = 4 OR visit.close_card = 3 OR visit.close_card = 5) AND visit.patient_id = patients.patient_id AND visit.visit_delete = 0  AND visit.visit_date = "'.date('Y-m-d').'" AND preauth = 0';
		$config["per_page"] = $v_data['per_page'] = $per_page = 30;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}
		$v_data['page'] = $page;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->admin_model->get_all_visits_parent($table, $where, $config["per_page"], $page);

		$v_data['todays_visit'] = $query;


		$date_tomorrow = date("Y-m-d",strtotime("tomorrow"));

		$dt= $date_tomorrow;
        $dt1 = strtotime($dt);
        $dt2 = date("l", $dt1);
        $dt3 = strtolower($dt2);
  //   	if(($dt3 == "sunday"))
		// {
  //           // echo $dt3.' is weekend'."\n";

  //           $date_tomorrow = strtotime('+1 day', strtotime($dt));
  //           $date_tomorrow = date("Y-m-d",$date_tomorrow);
  //           $date_to_send = 'Monday';
  //       } 
  //   	else
		// {
            // echo $dt3.' is not weekend'."\n";
             $date_tomorrow = $dt;
             $date_to_send = 'Tomorrow';
        // }


		$table= 'visit,patients';
		$where='visit.close_card = 2 AND visit.patient_id = patients.patient_id AND visit.visit_delete = 0  AND visit.visit_date = "'.$date_tomorrow.'" AND preauth = 0';
		$config["per_page"] = $v_data['per_page'] = $per_page = 30;
		if($page==0)
		{

			$counted = 0;
		}
		else if($page > 0)
		{

			$counted = $per_page*$page;
		}
		$v_data['page'] = $page;
		$v_data['date_to_send'] = $date_to_send;
		$page = $counted;
		$v_data['total_rows'] = $this->reception_model->count_items($table, $where);
		$query = $this->admin_model->get_all_visits_parent($table, $where, $config["per_page"], $page);

		$v_data['tomorrows_appointments'] = $query;





		
		$data['content'] = $this->load->view('summaries_dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}

	public function moh_717_report() 
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$moh717search = $this->session->userdata('moh_717_report_search');

		if(!empty($moh717search))
		{
			// $where .= $moh717search;
		}

		$data['content'] = $this->load->view('moh_717_report', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}
	
	public function visit_report()
	{
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card >= 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/outpatient-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		
		$query = $this->reports_model->get_all_visits($table, $where, $config["per_page"], $page, 'ASC');
		$v_data['doctors'] = $this->reception_model->get_doctor();

		
		$page_title = 'Visit Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('visit_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function moh_204_report()
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card >= 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'moh-report/moh-204';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 100;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		
		$query = $this->reports_model->get_all_visits($table, $where, $config["per_page"], $page, 'ASC');		
		$v_data['doctors'] = $this->reception_model->get_doctor();
		// var_dump($query);die();
		
		$page_title = 'MOH 204 Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('moh_204_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}


	public function inpatient_report()
	{
		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 1 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/inpatient-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_visits($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Visit Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('inpatient_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}


	public function birth_report()
	{
		$where = 'patients.height > 0';
		$table = 'patients';
		$birth_report_search = $this->session->userdata('birth_report_search');
		
		if(!empty($birth_report_search))
		{
			$where .= $birth_report_search;
		}
		else
		{
		 $where .= ' AND patients.patient_date_of_birth = "'.date('Y-m-d').'"';

		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/birth-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_patient_birth($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = "Patient's Birth Report"; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('birth_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}





	public function sick_off_report()
	{
		$where = 'patient_leave.visit_id = visit.visit_id AND patients.patient_id = visit.patient_id AND patient_leave.leave_type_id = leave_type.leave_type_id ';
		$table = 'patients,patient_leave,visit, leave_type';
		$sick_off_report_search = $this->session->userdata('sick_off_report_search');
		
		if(!empty($sick_off_report_search))
		{
			$where .= $sick_off_report_search;
		}
		else
		{
			$where .= ' AND patient_leave.start_date = "'.date('Y-m-d').'"';
		}
		//echo $where; die();
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/sick-off-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_visits_sick_offs($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Sick Off Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$department = $this->reports_model->get_all_departments();
		$departments = '';
		if($department->num_rows() > 0)
		{
			foreach ($department->result() as $department_test_rs):
				//var_dump($department_test_rs); die();
			  $department_name = $department_test_rs->department_name;
	
			  $departments .="<option value='".$department_name."'>".$department_name."</option>";
	
			endforeach;
		}
		
		$this->db->order_by('leave_type_name');
		$leave_types = $this->db->get('leave_type');
		$l_types = '';
		if($leave_types->num_rows() > 0)
		{
			foreach ($leave_types->result() as $rs):
				//var_dump($department_test_rs); die();
			  $leave_type_name = $rs->leave_type_name;
			  $leave_type_id = $rs->leave_type_id;
	
			  $l_types .="<option value='".$leave_type_id."'>".$leave_type_name."</option>";
	
			endforeach;
		}

		$v_data['l_types'] = $l_types;
		$v_data['departments'] = $departments;
		$data['content'] = $this->load->view('sick_off_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_visit_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}

			//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' Name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		$search = $visit_date.$surname;
		
		$this->session->set_userdata('visit_report_search', $search);
		$this->session->set_userdata('visit_title_search', $visit_search_title);
		
		redirect('records/outpatient-report');
	}

		public function search_visit()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}

			//search surname
		if(!empty($_POST['surname']))
		{
			$search_title .= ' Name <strong>'.$_POST['surname'].'</strong>';
			$surnames = explode(" ",$_POST['surname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\')';
				}
				
				else
				{
					$surname .= ' (patients.patient_surname LIKE \'%'.addslashes($surnames[$r]).'%\' OR patients.patient_othernames LIKE \'%'.addslashes($surnames[$r]).'%\') AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		$search = $visit_date.$surname;
		
		$this->session->set_userdata('visit_report_search', $search);
		$this->session->set_userdata('visit_title_search', $visit_search_title);
		
		redirect('moh-report/moh-204');
	}


	public function search_summary_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		
		$search = $visit_date;
		
		$this->session->set_userdata('summary_report_search', $search);
		$this->session->set_userdata('summary_title_search', $visit_search_title);
		
		redirect('records/summaries');
	}


	public function search_inpatient_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		
		$search = $visit_date;
		
		$this->session->set_userdata('inpatient_report_search', $search);
		$this->session->set_userdata('inpatient_title_search', $visit_search_title);
		
		redirect('records/inpatient-report');
	}


	public function search_rip_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND patients.rip_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
	 if(!empty($visit_date_from))
		{
			$visit_date = ' AND patients.rip_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND patients.rip_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}

	    //search surname
		$surnames = explode(" ",$_POST['surname']);
		$total = count($surnames);
		
		$count = 1;
		$surname = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
			}
			
			else
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
			}
			$count++;
		}
		$surname .= ') ';
		
		
		
		$search = $visit_date.$surname;
		
		$this->session->set_userdata('rip_report_search', $search);
		$this->session->set_userdata('inpatient_rip_search', $visit_search_title);
		
		redirect('records/rip-patients');
	}


	// public function search_birth_reports()
	// {
	// 	$visit_date_from = $this->input->post('visit_date_from');
	// 	$visit_date_to = $this->input->post('visit_date_to');
	// 	$visit_search_title ='';
	// 	if(!empty($visit_date_from) && !empty($visit_date_to))
	// 	{
	// 		$visit_date = ' AND patients.patient_date_of_birth BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

	// 		$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
	// 	}
		
	//  if(!empty($visit_date_from))
	// 	{
	// 		$visit_date = ' AND patients.patient_date_of_birth = \''.$visit_date_from.'\'';
	// 		$visit_search_title = 'Visit From '.$visit_date_from.' ';
	// 	}
		
	// 	else if(!empty($visit_date_to))
	// 	{
	// 		$visit_date = ' AND patients.patient_date_of_birth = \''.$visit_date_to.'\'';
	// 		$visit_search_title = 'Visit To '.$visit_date_to.'';
	// 	}
		
	// 	else
	// 	{
	// 		$visit_date = '';

	// 	}

	//     //search surname
	// 	$surnames = explode(" ",$_POST['surname']);
	// 	$total = count($surnames);
		
	// 	$count = 1;
	// 	$surname = ' AND (';
	// 	for($r = 0; $r < $total; $r++)
	// 	{
	// 		if($count == $total)
	// 		{
	// 			$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
	// 		}
			
	// 		else
	// 		{
	// 			$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
	// 		}
	// 		$count++;
	// 	}
	// 	$surname .= ') ';
		
		
		
	// 	$search = $visit_date.$surname;
		
	// 	$this->session->set_userdata('birth_report_search', $search);
	// 	$this->session->set_userdata('inpatient_birth_search', $visit_search_title);
		
	// 	redirect('records/birth-report');
	// }



	public function search_sick_off_reports()
	{
		$payroll_number = $this->input->post('payroll_number');
		$leave_type_id = $this->input->post('leave_type_id');
		$department_name = $this->input->post('department_name');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title = '';
		
		if(!empty($payroll_number))
		{
			$visit_search_title .= ' Payroll number '.$payroll_number;
			$payroll_number = ' AND patients.strath_no = \''.$payroll_number.'\'';
		}
		
		if(!empty($leave_type_id))
		{
			$this->db->where('leave_type_id', $leave_type_id);
			$query = $this->db->get('leave_type');
			$leave_type_name = '';
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$leave_type_name = $row->leave_type_name;
			}
			$visit_search_title .= ' Leave type '.$leave_type_name;
			$leave_type_id = ' AND patient_leave.leave_type_id = \''.$leave_type_id.'\'';
		}
		
		if(!empty($department_name))
		{
			$visit_search_title .= ' Department '.$department_name;
			$department_name = ' AND visit.department_name = \''.$department_name.'\'';
		}
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.start_date >= \''.$visit_date_from.'\' AND patient_leave.start_date <= \''.$visit_date_to.'\'';

			$visit_search_title = 'Start Date From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND patient_leave.start_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Start From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.start_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Start To '.$visit_date_to.'';
		}

		$search = $visit_date.$payroll_number.$department_name.$leave_type_id;

		$this->session->set_userdata('sick_off_report_search', $search);
		$this->session->set_userdata('sick_off_title_search', $visit_search_title);
		
		redirect('records/sick-off-report');
	}

	public function close_visit_search()
	{
		# code...
		$this->session->unset_userdata('visit_report_search');
		$this->session->unset_userdata('visit_title_search');

		redirect('records/outpatient-report');
	}

	public function close_rip_search()
	{
		# code...
		$this->session->unset_userdata('rip_report_search');
		$this->session->unset_userdata('rip_title_search');

		redirect('records/rip-report');
	}

	public function close_inpatient_search()
	{
		# code...
		$this->session->unset_userdata('inpatient_report_search');
		$this->session->unset_userdata('inpatient_title_search');

		redirect('records/inpatient-report');
	}

	public function close_sick_off_search()
	{
		# code...
		$this->session->unset_userdata('sick_off_report_search');
		$this->session->unset_userdata('sick_off_title_search');

		redirect('records/sick-off-report');
	}

	public function print_visit_report()
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card >= 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$query = $this->reports_model->get_all_visits_content($table, $where,'visit.visit_time' ,'ASC');


		$page_title = 'Visit Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('visit_report_print', $v_data);


	}

	// 	public function print_visit_report()
	// {

	// 	$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
	// 	$table = 'visit, patients, visit_type';
	// 	$visit_report_search = $this->session->userdata('visit_report_search');
		
	// 	if(!empty($visit_report_search))
	// 	{
	// 		$where .= $visit_report_search;
		
	// 		if(!empty($table_search))
	// 		{
	// 			$table .= $table_search;
	// 		}
			
	// 	}
	// 	else
	// 	{
	// 		// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
	// 	}

	// 	$query = $this->reports_model->get_all_visits_content($table, $where,'visit.visit_time' ,'ASC');


	// 	$page_title = 'Visit Report'; 
	// 	$data['title'] = $v_data['title'] = $page_title;
	// 	$v_data['query'] = $query;

	// 	$v_data['contacts'] = $this->site_model->get_contacts();
		
	// 	$this->load->view('visit_report_print', $v_data);


	// }

	public function print_moh_204_report()
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 0 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$visit_report_search = $this->session->userdata('visit_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}

		$query = $this->reports_model->get_all_visits_content($table, $where,'visit.visit_time' ,'ASC');


		$page_title = 'MOH 204 Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('moh_204_report_print', $v_data);


	}

	public function print_inpatient_report()
	{

		$where = 'visit.patient_id = patients.patient_id AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0 AND visit.inpatient = 1 AND patients.rip_status =0 AND (visit.close_card = 0 OR visit.close_card = 2)';
		$table = 'visit, patients, visit_type';
		$inpatient_report_search = $this->session->userdata('inpatient_report_search');
		
		if(!empty($inpatient_report_search))
		{
			$where .= $inpatient_report_search;
		
			if(!empty($table_search))
			{
				$table .= $table_search;
			}
			
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$query = $this->reports_model->get_all_visits_content($table, $where,'visit.visit_time' ,'ASC');


		$page_title = 'Inpatient Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('inpatient_report_print', $v_data);


	}

	public function print_sick_off_report()
	{
		$where = 'patient_leave.visit_id = visit.visit_id AND patients.patient_id = visit.patient_id   ';
		$table = 'patients, patient_leave,visit';
		$sick_off_report_search = $this->session->userdata('sick_off_report_search');
		
		if(!empty($sick_off_report_search))
		{
			$where .= $sick_off_report_search;
		}
		else
		{
			$where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}



		$query = $this->reports_model->get_all_sick_off_content($table, $where,'patient_leave.from_date' ,'ASC');


		$page_title = 'Sick Off Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('sick_off_report_print', $v_data);
	}
	
	public function leave_reports($order = 'patient_leave.start_date',$order_method = 'DESC')
	{
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_id = patient_leave.visit_id AND patient_leave.leave_type_id = leave_type.leave_type_id';
		$table = 'visit, patients, patient_leave, leave_type';
		
		$leave_search = $this->session->userdata('leave_report_search');
		if(!empty($leave_search))
		{
			$where .= $leave_search;
		}
		else
		{
			$where .='  AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 5;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/leave-reports/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_patient_leave($table, $where, $config["per_page"], $page, $order, $order_method);
		
		$page_title = 'Patient Leave Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('patient_leave_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}
	public function search_leave_reports()
	{
		$payroll_number = $this->input->post('payroll_number');
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		
		if(!empty($payroll_number))
		{
			$payroll_number = ' AND patients.strath_no = \''.$payroll_number.'\'';
		}
		
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.start_date >= \''.$visit_date_from.'\' AND patient_leave.end_date <= \''.$visit_date_to.'\'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND patient_leave.start_date >= \''.$visit_date_from.'\'';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND patient_leave.end_date <= \''.$visit_date_to.'\'';
		}

		$search = $visit_date.$payroll_number;

		$this->session->set_userdata('leave_report_search', $search);
		redirect('records/leave-reports');
	}
	public function close_leave_search()
	{
		$this->session->unset_userdata('leave_report_search');
		redirect('records/leave-reports');
	}
	public function patient_statistics()
	{
	}


	public function rip_patients()
	{
		$where = 'rip_status = 1';
		$table = 'patients';
		$rip_report_search = $this->session->userdata('rip_report_search');
		
		if(!empty($rip_report_search))
		{
			$where .= $rip_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'records/rip-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_patient_rip($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = "Patient's RIP Report"; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('patients_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function export_outpatient_report()
	{
		$this->reports_model->export_outpatient_report();
	}
	public function export_inpatient_report()
	{
		$this->reports_model->export_inpatient_report();
	}

	public function procedures_report()
	{
		$branch_id = $this->session->userdata('branch_id');

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Procedures" OR service.service_name = "Ultrasound")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('procedure_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/procedures-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 100;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_procedures_visit($table, $where, $config["per_page"], $page, 'ASC');
		
		$//page_title = 'Procedures Report'; 



		$page_title = $this->session->userdata('procedure_title_search');
		
		if(empty($page_title))
		{
			$page_title = 'Procedure Report for '.date('Y-m-d');
		}

		$data['title'] = $v_data['title'] = $page_title;

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('procedures_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_procedures_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$procedure_name = $this->input->post('procedure_name');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		

		if(!empty($procedure_name))
		{
			$procedure_name = ' AND service_charge.service_charge_name LIKE \'%'.$procedure_name.'%\'';
			$visit_search_title .= ' Procedure '.$procedure_name.'';
		}
		
		else
		{
			$procedure_name = '';

		}

		
		$search = $visit_date.$procedure_name;
		
		$this->session->set_userdata('procedure_report_search', $search);
		$this->session->set_userdata('procedure_title_search', $visit_search_title);
		
		redirect('management-reports/procedures-report');
	}

	public function close_procedures_search()
	{
		# code...
		$this->session->unset_userdata('procedure_report_search');
		$this->session->unset_userdata('procedure_title_search');

		redirect('management-reports/procedures-report');
	}

	public function export_procedures_report($service_charge_id = NULL)
	{
		$this->reports_model->export_procedures_report($service_charge_id);
	}
	public function export_visit_procedures_report($service_charge_id = NULL)
	{
		$this->reports_model->export_visit_procedures_report($service_charge_id);
	}
	public function general_report()
	{

		 
		
		$v_data['general_report'] = '';
		$data['content'] = $this->load->view('general_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function branch_report()
	{

		$branch_report_search = $this->session->userdata('branch_report_search');
		if(!empty($branch_report_search))
		{
			$combination = $this->session->userdata('year').'-'.$this->session->userdata('month').'-01';

			$first_day = date("Y-m-01", strtotime($combination));
			$last_day = date("Y-m-t", strtotime($combination));
		}
		else
		{

			$dt = date('Y-m-d');

			$first_day = date("Y-m-01", strtotime($dt));
			$last_day = date("Y-m-t", strtotime($dt));

		}
		
		$v_data['title'] = 'Branch Report for '.date('M Y', strtotime($first_day));
		$data['title'] = 'Branch Report '.date('M Y', strtotime($first_day));
		$branch_id = $this->session->userdata('branch_id');

		$v_data['first_day'] = $first_day;
		$v_data['last_day'] = $last_day;
		$v_data['branch_id'] = $branch_id;
		$data['content'] = $this->load->view('branch_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_branch_report()
	{
		$month_id = $this->input->post('month');
		$year = $this->input->post('year');

		$this->session->set_userdata('branch_report_search',TRUE);
		$this->session->set_userdata('year',$year);
		$this->session->set_userdata('month',$month_id);

		redirect('management-reports/branch-report');
	}
	public function close_branch_report()
	{
		$this->session->unset_userdata('branch_report_search');
		$this->session->unset_userdata('year');
		$this->session->unset_userdata('month');

		redirect('management-reports/branch-report');
	}
	public function print_dl_report2()
	{
		$branch_report_search = $this->session->userdata('branch_report_search');
		if(!empty($branch_report_search))
		{
			$combination = $this->session->userdata('year').'-'.$this->session->userdata('month').'-01';

			$first_day = date("Y-m-01", strtotime($combination));
			$last_day = date("Y-m-t", strtotime($combination));
		}
		else
		{

			$dt = date('Y-m-d');

			$first_day = date("Y-m-01", strtotime($dt));
			$last_day = date("Y-m-t", strtotime($dt));

		}
		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$v_data['contacts'] = $this->site_model->get_contacts($branch_id);
		
		$v_data['title'] = 'Visit Report for '.date('M Y', strtotime($first_day));
		$data['title'] = 'Visit Report '.date('M Y', strtotime($first_day));


		$v_data['first_day'] = $first_day;
		$v_data['last_day'] = $last_day;
		$v_data['branch_id'] = $branch_id;
		$this->load->view('print_visit_report', $v_data);

	}

	public function print_branch_report()
	{
		$branch_report_search = $this->session->userdata('branch_report_search');
		if(!empty($branch_report_search))
		{
			$combination = $this->session->userdata('year').'-'.$this->session->userdata('month').'-01';

			$first_day = date("Y-m-01", strtotime($combination));
			$last_day = date("Y-m-t", strtotime($combination));
		}
		else
		{

			$dt = date('Y-m-d');

			$first_day = date("Y-m-01", strtotime($dt));
			$last_day = date("Y-m-t", strtotime($dt));

		}
		$branch_id = $this->session->userdata('branch_id');

		if($branch_id == 0)
		{
			$branch_id = NULL;
		}
		$v_data['contacts'] = $this->site_model->get_contacts($branch_id);
		
		$v_data['title'] = 'Branch Report for '.date('M Y', strtotime($first_day));
		$data['title'] = 'Branch Report '.date('M Y', strtotime($first_day));


		$v_data['first_day'] = $first_day;
		$v_data['last_day'] = $last_day;
		$v_data['branch_id'] = $branch_id;
		$this->load->view('print_visit_report', $v_data);

	}

	public function send_branch_report()
	{

		
		$branches = $this->site_model->get_all_branches();
		// var_dump($branches);die();
		if($branches->num_rows() > 0)
		{
			foreach ($branches->result() as $key => $value) {
				# code...
				$branch_id = $value->branch_id;
				$branch_name = $value->branch_name;
				$doctor_email = $value->doctor_email;

				$dt = date('2021-01-d');
				$first_day = date('Y-m-d', strtotime('first day of previous month'));//date("2021-01-01", strtotime($dt));
				$last_day = date('Y-m-d', strtotime('last day of previous month'));//date("2021-01-t", strtotime($dt));

				$v_data['contacts'] = $contacts = $this->site_model->get_contacts($branch_id);
				
				$v_data['title'] = $branch_name.' Report for '.date('M 2021', strtotime($first_day));
				$data['title'] =  $branch_name.' Report for '.date('M 2021', strtotime($first_day));


				$v_data['first_day'] = $first_day;
				$v_data['last_day'] = $last_day;
				$v_data['branch_id'] = $branch_id;

				// var_dump($v_data);die();
				$html = $this->load->view('print_branch_report', $v_data,true);
				// var_dump($html);die();

				$this->load->library('mpdf');
				
				$document_number = date("Ymdhis");
				$receipt_month_date = date('Y-m-d');
				$receipt_month = date('M',strtotime($receipt_month_date));
				$receipt_year = date('Y',strtotime($receipt_month_date));
				$title = $document_number.'-'.$data['title'].'.pdf';
				$invoice = $title;
				// echo $first_day;die();
				
				$mpdf=new mPDF();
				$mpdf->WriteHTML($html);
				$mpdf->Output($title, 'F');

				while(!file_exists($title))
				{

				}
				$message['text'] =$text;


				$sender_email = 'info@1809ltd.co.ke';//$this->config->item('sender_email');//$contacts['email'];
				$shopping = "";
				$from = $sender_email; 
				
				$button = '';
				$sender['email']= $sender_email; 
				$sender['name'] = $contacts['company_name'];
				$receiver['name'] = $subject;
				$payslip = $title;

				$sender_email = $sender_email;
				$tenant_email = 'marttkip@gmail.com/'.$doctor_email;//$this->config->item('recepients_email');
				$email_array = explode('/', $tenant_email);
				$total_rows_email = count($email_array);

				for($x = 0; $x < $total_rows_email; $x++)
				{
					$receiver['email'] = $email_tenant = $email_array[$x];


					$message['subject'] = $data['title'];
					$message['text'] = ' <p>Good evening <br>
											Please find attached report for the month 
											</p>
										';
					$shopping = ""; 
					
					$button = '';
					
					$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
					

				}
				
				unlink($payslip);

				// var_dump($payslip);die();

			}
		}
		
		$response['status'] = 'success';

		echo json_encode($response);
	}

	public function expenses_report()
	{


		$where = '(v_expenses_ledger.transactionClassification = "Purchases" OR v_expenses_ledger.transactionClassification = "Direct Payments" OR v_expenses_ledger.transactionClassification = "Creditors Invoices Payments")';
		$table = 'v_expenses_ledger';
		$visit_report_search = $this->session->userdata('export_payment_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' AND MONTH(v_expenses_ledger.transactionDate) = "'.date('m').'" AND YEAR(v_expenses_ledger.transactionDate) = "'.date('Y').'"';
		}

		$branch_session = $this->session->userdata('branch_id');

	    if($branch_session > 0)
	    {
	      $where .= ' AND v_expenses_ledger.branch_id = '.$branch_session;
	    }
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/expenses-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_expense_report($table, $where, $config["per_page"], $page, 'ASC');
		// var_dump($query);die();
		$page_title = 'Expense Report'; 

		$search_title = $this->session->userdata('expense_search_title');
		
		if(!empty($search_title))
		{
			$page_title = $search_title;
		}
		else
		{
			$page_title = 'Expenses for the month of '.date('M').' '.date('Y').'';
		}
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('expenses_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_expenses_report()
	{
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		

		$search_title = 'Expense report for: ';
		if(!empty($date_from) && !empty($date_to))
		{
			$date = ' AND v_expenses_ledger.transactionDate >= \''.$date_from.'\' AND v_expenses_ledger.transactionDate <= \''.$date_to.'\'';

			$search_title .= ' Date '.$date_from.' to '.$date_to.' ';
		}
		
		else if(!empty($date_from))
		{
			$date = ' AND v_expenses_ledger.transactionDate >= \''.$date_from.'\'';
			$search_title .= ' Date '.$date_from.' ';
		}
		
		else if(!empty($date_to))
		{
			$date = ' AND v_expenses_ledger.transactionDate <= \''.$date_to.'\'';
			$search_title .= ' Date '.$date_to.' ';
		}

		$search = $date;

		$this->session->set_userdata('export_payment_search', $search);
		$this->session->set_userdata('expense_search_title', $search_title);
		redirect('management-reports/expenses-report');
	}
	public function close_expense_search_report()
	{
		$this->session->unset_userdata('export_payment_search');
		$this->session->unset_userdata('expense_search_title');
		redirect('management-reports/expenses-report');
	}
	public function print_expenses_report()
	{
		$search = $this->session->userdata('export_payment_search');
		$search_title = $this->session->userdata('expense_search_title');
		
		if(!empty($search_title))
		{
			$title = $search_title;
		}
		else
		{
			$title = 'Expenses for the month of '.date('M').' '.date('Y').'';
		}
		$branch_id = $this->session->userdata('branch_id');
		$v_data['contacts'] = $this->site_model->get_contacts($branch_id);
		$v_data['title'] = $search_title;
		$this->load->view('print_expenses_report', $v_data);
	}
	public function export_expenses_report()
	{
		$this->reports_model->export_expense_transactions();
	}









	// lab tests


	public function lab_test_report()
	{
		$branch_id = $this->session->userdata('branch_id');

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Laboratory") ';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('tests_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/tests-report';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 100;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_lab_visit($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Laboratory Tests Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('lab_test_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}
	// lab tests


	public function ultra_test_report()
	{
		$branch_id = $this->session->userdata('branch_id');

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Ultrasound") AND visit.branch_id ='.$branch_id;
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('ultra_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/radiology-reports';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 200;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_procedures_visit($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Utrasound Tests Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('ultra_test_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}


	public function search_tests_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$procedure_name = $this->input->post('procedure_name');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		

		if(!empty($procedure_name))
		{
			$procedure_name = ' AND service_charge.service_charge_name LIKE \'%'.$procedure_name.'%\'';
			$visit_search_title .= ' Procedure '.$procedure_name.'';
		}
		
		else
		{
			$procedure_name = '';

		}

		
		$search = $visit_date.$procedure_name;
		
		$this->session->set_userdata('tests_report_search', $search);
		$this->session->set_userdata('tests_title_search', $visit_search_title);
		
		redirect('management-reports/tests-report');
	}


	public function search_ultra_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$procedure_name = $this->input->post('procedure_name');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		

		if(!empty($procedure_name))
		{
			$procedure_name = ' AND service_charge.service_charge_name LIKE \'%'.$procedure_name.'%\'';
			$visit_search_title .= ' Procedure '.$procedure_name.'';
		}
		
		else
		{
			$procedure_name = '';

		}

		
		$search = $visit_date.$procedure_name;
		
		$this->session->set_userdata('ultra_report_search', $search);
		$this->session->set_userdata('ultra_title_search', $visit_search_title);
		
		redirect('management-reports/radiology-reports');
	}


	public function close_tests_search()
	{
		# code...
		$this->session->unset_userdata('tests_report_search');
		$this->session->unset_userdata('tests_title_search');

		redirect('management-reports/tests-report');
	}

	public function close_ultra_search()
	{
		# code...
		$this->session->unset_userdata('ultra_report_search');
		$this->session->unset_userdata('tests_title_search');

		redirect('management-reports/radiology-reports');
	}

	public function export_tests_report()
	{
		$this->reports_model->export_tests_report();
	}

	public function export_ultra_report($service_charge_id = NULL)
	{
		$this->reports_model->export_ultra_report($service_charge_id);
	}

	public function export_visit_tests_report($lab_test_id = NULL)
	{
		$this->reports_model->export_visit_tests_report($lab_test_id);
	}

	public function search_moh_717_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		
		$search = $visit_date;
		
		$this->session->set_userdata('moh_717_report_search', $search);
		$this->session->set_userdata('moh_717_title_search', $visit_search_title);
		
		redirect('moh-report/moh-717');
	}

	public function close_moh_717_search()
	{
		# code...
		$this->session->unset_userdata('moh_717_report_search');
		$this->session->unset_userdata('moh_717_title_search');

		redirect('moh-report/moh-717');
	}

	public function medical_reports_dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];








		
		$data['content'] = $this->load->view('medical_reports_dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}


	public function out_of_stock()
	{

		$session_main = $this->session->userdata('main_store_search');

		$add_new = '';

		if($session_main == 5)
		{
			$add_new = ' AND pos_order.sale_type = 0 ';
		}
		else
		{
			$add_new = ' AND (pos_order.sale_type = 1 OR pos_order.sale_type = 2) ';
		}

		$where = 'product.product_id = store_product.product_id  AND product.product_id IN (SELECT pos_order_item.product_id FROM pos_order_item,pos_order WHERE pos_order.pos_order_id = pos_order_item.pos_order_id AND order_invoice_id > 0 '.$add_new.') AND store_product.owning_store_id = store.store_id AND product.product_deleted = 0 AND product.stock_take = 1';
		$table = 'product,store_product,store';
		$visit_report_search = $this->session->userdata('out_of_stock_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;




			$segment = 3;
			//pagination
			$this->load->library('pagination');
			$config['base_url'] = site_url().'administrative-reports/out-of-stock';
			$config['total_rows'] = $this->reception_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 20;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
	        $v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_out_of_stock_items($table, $where, $config["per_page"], $page, 'ASC');
			
			$page_title = "Product Out of Stock"; 
			$data['title'] = $v_data['title'] = $page_title;
			$v_data['query'] = $query;
			$v_data['page'] = $page;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

			$page_title = "Product Out of Stock"; 
			$data['title'] = $v_data['title'] = $page_title;
			$v_data['query'] = null;
			$v_data['page'] = 0;
		}


		
		
		
		$data['content'] = $this->load->view('out_of_stock', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function search_out_of_stock()
	{
		$product_name = $this->input->post('product_name');
		$store_id = $main_store = $this->input->post('store_id');
		
		if(!empty($product_name))
		{
			$visit_search_title .= ' Product Name '.$product_name;
			$product_name = ' AND product.product_name = \''.$product_name.'\'';
		}
		
		if(!empty($store_id))
		{
			$this->db->where('store_id', $store_id);
			$query = $this->db->get('store');
			$leave_type_name = '';
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$store_name = $row->store_name;
			}
			$visit_search_title .= ' Store '.$store_name;
			$store_id = ' AND store.store_id = \''.$store_id.'\'';
		}
		

		$search = $store_id.$product_name;

		$this->session->set_userdata('out_of_stock_report_search', $search);

		$this->session->set_userdata('main_store_search', $main_store);

		$this->session->set_userdata('out_of_stock_title_search', $visit_search_title);
		
		redirect('administrative-reports/out-of-stock');
	}
	public function close_out_of_stock_items()
	{
		$this->session->unset_userdata('out_of_stock_report_search');
		$this->session->unset_userdata('out_of_stock_title_search');
		$this->session->unset_userdata('main_store_search');
		
		redirect('administrative-reports/out-of-stock');
	}
	public function drug_expiries()
	{

	
		$visit_report_search = $this->session->userdata('expiries_report_search');
		
		if(!empty($visit_report_search))
		{
			$where = 'product.product_id = store_product.product_id  AND store_product.owning_store_id = store.store_id AND product.product_deleted = 0 AND product.stock_take = 1 AND product_status = 1';
			$table = 'product,store_product,store';
			$where .= $visit_report_search;

			$segment = 3;
			//pagination
			$this->load->library('pagination');
			$config['base_url'] = site_url().'medical-reports/product-expiries';
			$config['total_rows'] = $this->reception_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 1000;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
	        $v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_product_expiries_items($table, $where, $config["per_page"], $page, 'ASC');
			
			$page_title = "Product Expiries"; 
			$data['title'] = $v_data['title'] = $page_title;
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			
			
			$data['content'] = $this->load->view('drug_expiries', $v_data, true);
			
			$this->load->view('admin/templates/general_page', $data);
		}
		else
		{	
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';

				$page_title = "Product Expiries"; 
				$data['title'] = $v_data['title'] = $page_title;
				$v_data['query'] = null;
				$v_data['page'] = 0;
				
				
				$data['content'] = $this->load->view('drug_expiries', $v_data, true);
				
				$this->load->view('admin/templates/general_page', $data);
		}


		

	}


	public function search_expiries()
	{
		$product_name = $this->input->post('product_name');
		$store_id = $store = $this->input->post('store_id');
		$visit_search_title = '';
		if(!empty($store_id))
		{
			$this->db->where('store_id', $store_id);
			$query = $this->db->get('store');
			$store_name = '';
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$store_name = $row->store_name;
			}


			if($store_id == 5)
			{
				$add = ' AND store_product.store_balance > 0';
			}
			else
			{
				$add = ' AND store_product.store_balance > 0';
			}
			$visit_search_title .= ' STORE '.$store_name;
			$store_id = ' AND store.store_id = \''.$store_id.'\''.$add;
		}
		
		if(!empty($product_name))
		{
			$visit_search_title .= ' PRODUCT '.$product_name;
			$product_name = ' AND product.product_name = \''.$product_name.'\'';
		}
		
		
		

		$search = $store_id.$product_name;
		$this->session->set_userdata('store_id', $store);
		$this->session->set_userdata('expiries_report_search', $search);
		$this->session->set_userdata('expiries_title_search', $visit_search_title);
		
		redirect('medical-reports/product-expiries');
	}
	public function close_expiries_items()
	{
		$this->session->unset_userdata('expiries_report_search');
		$this->session->unset_userdata('expiries_title_search');
		$this->session->unset_userdata('store_id');
		
		redirect('medical-reports/product-expiries');
	}

	public function inventory_valuation()
	{	



			$where = 'product.product_deleted = 0 AND product.stock_take = 1 AND product_status = 1 AND category.category_id = product.category_id AND  product.product_id = store_product.product_id AND store.store_id = product.store_id';
			$table = 'product,category,store_product,store';

			$product_stock_search = $this->session->userdata('product_stock_search');

			if(!empty($product_stock_search))
			{
				$where .= $product_stock_search;
			}
			

			$segment = 3;
			//pagination
			$this->load->library('pagination');
			$config['base_url'] = site_url().'inventory/stock-movements';
			$config['total_rows'] = $this->reception_model->count_items($table, $where);
			$config['uri_segment'] = $segment;
			$config['per_page'] = 10;
			$config['num_links'] = 5;
			
			$config['full_tag_open'] = '<ul class="pagination pull-right">';
			$config['full_tag_close'] = '</ul>';
			
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			$config['next_tag_open'] = '<li>';
			$config['next_link'] = 'Next';
			$config['next_tag_close'] = '</span>';
			
			$config['prev_tag_open'] = '<li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			
			$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
	        $v_data["links"] = $this->pagination->create_links();
			$query = $this->reports_model->get_all_product_stocks($table, $where, $config["per_page"], $page, 'ASC');
			
			$stock_search = $this->session->userdata('stock_search');

			if(!empty($stock_search))
			{
				$search_title = $this->session->set_userdata('stock_title_search');

			}
			else
			{
				$start_date  = date('Y-m-d', strtotime('-7 days'));

				$search_title = 'Stock of '.$start_date.' to '.date('Y-m-d').' ';
			}

			$page_title = $search_title; 
			$data['title'] = $v_data['title'] = $page_title;
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			
			
			$data['content'] = $this->load->view('product_valuations', $v_data, true);
			
			$this->load->view('admin/templates/general_page', $data);

	}

		public function print_movements()
	{

		 $where = 'product.product_deleted = 0 AND product.stock_take = 1 AND product_status = 1 AND category.category_id = product.category_id AND  product.product_id = store_product.product_id AND store.store_id = product.store_id';
			$table = 'product,category,store_product,store';


		
	
		 $product_stock_search = $this->session->userdata('product_stock_search');

			if(!empty($product_stock_search))
			{
				$where .= $product_stock_search;
			}
			
		// else
		// {
		// 	$where .= ' AND product_deductions.search_date = "'.$search_date.'"';
		// }

		$query = $this->reports_model->get_all_product_stocks_p($table, $where);
		// $v_data['store_id'] = $store_id;

		$data['title'] = 'Stickers';
		$v_data['title'] = $data['title'];
		$v_data['contacts'] = $this->site_model->get_contacts();

		$v_data['query'] = $query;
		// $v_data['branches'] = $branches;

		$this->load->view('print_valuation', $v_data);

	}


	public function search_product_valuation()
	{
		$date_from = $year_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		$product_id = $this->input->post('product_id');
		$category_id = $this->input->post('category_id');


		if(!empty($date_from) && !empty($date_to))
		{

			$search_title = 'REPORT FOR PERIOD '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}


		else if(!empty($date_from))
		{
			
			$search_title = 'REPORT FOR PERIOD '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else if(!empty($date_to))
		{
			$date_to = $date_to;
			$date_to = $year_from;
			$search_title = 'REPORT FOR PERIOD '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}

		else
		{

			$start_date  = date('Y-m-d', strtotime('-7 days'));



			$search_title = 'Stock of '.$start_date.' to '.date('Y-m-d').' ';
		}


		if(!empty($product_id))
		{
			$product_add = ' AND product.product_id = '.$product_id;
		}
		else
		{
			$product_add = '';
		}

		if(!empty($category_id))
		{
			$category_add = ' AND product.category_id = '.$category_id;
		}
		else
		{
			$category_add = '';
		}

		$search_add = $product_add.$category_add;

		$this->session->set_userdata('date_from_stock',$date_from);
		$this->session->set_userdata('date_to_stock',$date_to);
		$this->session->set_userdata('product_stock_search',$search_add);
		$this->session->set_userdata('stock_title_search',$search_title);
		$this->session->set_userdata('stock_search',1);

		redirect('inventory/stock-movements');
	}

	function close_stock_search()
	{
		$this->session->unset_userdata('date_from_stock');
		$this->session->unset_userdata('date_to_stock');
		$this->session->unset_userdata('product_stock_search');
		$this->session->unset_userdata('stock_title_search');
		$this->session->unset_userdata('stock_search');

		redirect('inventory/stock-movements');
	}
	public function export_product_valuations()
	{
		$this->reports_model->export_product_valuations();
	}
	public function print_radiology_report()
	{

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Ultrasound")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('ultra_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->reports_model->get_report_content($table, $where,'service_charge.service_charge_name' ,'ASC');


		$page_title = 'Radiology Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('radiology_report_print', $v_data);
	}

	public function export_visit_ultrasound_report($service_charge_id = NULL)
	{
		$this->reports_model->export_visit_ultrasound_report($service_charge_id);
	}


	// dental 


	public function dental_procedures_report()
	{
		$branch_id = $this->session->userdata('branch_id');

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Dental")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('dental_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'management-reports/dental-reports';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 200;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_procedures_visit($table, $where, $config["per_page"], $page, 'ASC');
		
		$page_title = 'Dental Procedures Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		
		$data['content'] = $this->load->view('dental_test_report', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}
	public function search_dental_report()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$procedure_name = $this->input->post('procedure_name');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Visit From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
		else if(!empty($visit_date_from))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_from.'\'';
			$visit_search_title = 'Visit From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date_to.'\'';
			$visit_search_title = 'Visit To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}
		

		if(!empty($procedure_name))
		{
			$procedure_name = ' AND service_charge.service_charge_name LIKE \'%'.$procedure_name.'%\'';
			$visit_search_title .= ' Procedure '.$procedure_name.'';
		}
		
		else
		{
			$procedure_name = '';

		}

		
		$search = $visit_date.$procedure_name;
		
		$this->session->set_userdata('dental_report_search', $search);
		$this->session->set_userdata('dental_title_search', $visit_search_title);
		
		redirect('management-reports/dental-reports');
	}



	public function close_dental_search()
	{
		# code...
		$this->session->unset_userdata('dental_report_search');
		$this->session->unset_userdata('dental_title_search');

		redirect('management-reports/dental-reports');
	}

	public function export_dental_report()
	{
		$this->reports_model->export_dental_report();
	}

	public function export_visit_dental_report($service_charge_id)
	{
		$this->reports_model->export_visit_dental_report($service_charge_id);
	}
	public function print_dental_report()
	{

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Dental")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('dental_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->reports_model->get_report_content($table, $where,'service_charge.service_charge_name' ,'ASC');


		$page_title = 'Dental Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('dental_report_print', $v_data);

	}

	public function print_lab_tests_report()
	{

		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id = visit_invoice.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_charge_delete = 0 AND visit.visit_delete = 0 AND visit.visit_id = visit_charge.visit_id AND service.service_id = service_charge.service_id AND (service.service_name = "Laboratory")';
		$table = 'visit_charge,service_charge,visit,visit_invoice,service';
		$visit_report_search = $this->session->userdata('tests_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			// $where .= ' AND visit.visit_date = "'.date('Y-m-d').'"';
		}




		$query = $this->reports_model->get_lab_report_content($table, $where,'service_charge.service_charge_name' ,'ASC');


		$page_title = 'Laboratory Report'; 
		$data['title'] = $v_data['title'] = $page_title;
		$v_data['query'] = $query;

		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$this->load->view('laboratory_report_print', $v_data);


	}

	public function search_birth_reports()
	{
		$visit_date_from = $this->input->post('visit_date_from');
		$visit_date_to = $this->input->post('visit_date_to');
		$visit_search_title ='';
		if(!empty($visit_date_from) && !empty($visit_date_to))
		{
			$visit_date = ' AND patients.patient_date_of_birth BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';

			$visit_search_title = 'Birth Date From '.$visit_date_from.' To '.$visit_date_to.'';
		}
		
	 if(!empty($visit_date_from))
		{
			$visit_date = ' AND patients.patient_date_of_birth = \''.$visit_date_from.'\'';
			$visit_search_title = 'Birth Date From '.$visit_date_from.' ';
		}
		
		else if(!empty($visit_date_to))
		{
			$visit_date = ' AND patients.patient_date_of_birth = \''.$visit_date_to.'\'';
			$visit_search_title = 'Birth Date To '.$visit_date_to.'';
		}
		
		else
		{
			$visit_date = '';

		}

	    //search surname
		$surnames = explode(" ",$_POST['surname']);
		$total = count($surnames);
		
		$count = 1;
		$surname = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\'';
			}
			
			else
			{
				$surname .= ' patients.patient_surname LIKE \'%'.$surnames[$r].'%\' AND ';
			}
			$count++;
		}
		$surname .= ') ';
		
		
		
		$search = $visit_date.$surname;
		
		$this->session->set_userdata('birth_report_search', $search);
		$this->session->set_userdata('inpatient_birth_search', $visit_search_title);
		
		redirect('records/birth-report');
	}

	public function close_birth_search()
	{
		# code...
		$this->session->unset_userdata('birth_report_search');
		$this->session->unset_userdata('inpatient_birth_search');

		redirect('records/birth-report');
	}



	
}
?>