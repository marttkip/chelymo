<div class="col-md-12">
	

	<!-- dates  -->
	<?php

	$where = 'v_transactions_by_date.transactionCategory = "Revenue Payment"';
			
	$table = 'v_transactions_by_date';
	$visit_search = $this->session->userdata('cash_report_search');

	// if(!empty($visit_search))
	// {
	// 	$where .= $visit_search;
	// }
	// else
	// {
	$where .=' AND v_transactions_by_date.transaction_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" ';
	// }

	$branch_session = $branch_id;

	if($branch_session > 0)
	{
		$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
		

	}
	$where2 = $where;

	$normal_payments = $this->reports_model->get_normal_payments($where2, $table, 'cash');
	$payment_methods = $this->reports_model->get_payment_methods($where2, $table, 'cash');


	$branch_session = $this->session->userdata('branch_id');

	$branch_add = '';
	$visit_branch_add = '';
	if($branch_session > 0)
	{
		$branch_add = ' AND branch_id = '.$branch_session;
		$visit_branch_add = ' AND visit.branch_id = '.$branch_session;
	}

	$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND appointments.appointment_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
	$table = 'patients,visit,appointments';
	$total_appointments = $this->reception_model->count_items($table, $where);



	$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 ) AND appointments.appointment_date BETWEEN "'.$first_day.'" AND "'.$last_day.'"'.$visit_branch_add;
	$table = 'patients,visit,appointments';
	$honoured_appointments = $this->reception_model->count_items($table, $where);








	$where = 'patients.patient_delete = 0 AND visit.appointment_id = 0 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
	$table = 'patients,visit';
	$coming_appointments = $this->reception_model->count_items($table, $where);



	$where = 'patients.patient_delete = 0 AND (visit.revisit = 1 OR visit.revisit = 0) AND close_card <> 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0   AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
	$table = 'patients,visit';
	$new_visits = $this->reception_model->count_items($table, $where);


	$where = 'patients.patient_delete = 0 AND visit.revisit = 2 AND close_card <> 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
	$table = 'patients,visit';
	$revisit = $this->reception_model->count_items($table, $where);





	// $patients = $this->reports_model->get_patients_visits(1);



	
 
	?>

	<div class="row">
	<?php echo $this->load->view('search/branch_report_search', '', TRUE);?>

	<div class="row">
		<div class="col-md-12 center-align">
		<?php
			$search = $this->session->userdata('branch_report_search');
			echo $title;
			if(!empty($search))
			{
				echo ' <a href="'.site_url().'reports/close_branch_report" class="btn btn-warning btn-xs ">Close Search</a>
				';
			}
			
		?>
	</div>
	</div>

	<section class="panel panel-featured panel-featured-info">
	    <header class="panel-heading">
	    	<h2 class="panel-title">Report Summary <?php echo $title;?></h2>
	    </header>             
	    <div class="panel-body">

	    	<div class="row">
		        <!-- End Transaction Breakdown -->
		        <div class="col-md-2">
		            <h4 class="center-align">Visit's Summary</h4>
		            <table class="table table-striped table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>Type</th>
	                            <th>Visits</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <th>BOOKINGS</th>
	                            <td><?php echo $honoured_appointments;?></td>
	                        </tr>
	                        <tr>
	                            <th>WALKIN'S</th>
	                            <td><?php echo $coming_appointments;?></td>
	                        </tr>
	                    </tbody>
	                </table>
	                <h4 class="center-align">New Visit vs Re-visits</h4>
		            <table class="table table-striped table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>Type</th>
	                            <th>Visits</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <th>NEW PATIENTS</th>
	                            <td><?php echo $new_visits;?></td>
	                        </tr>
	                        <tr>
	                            <th>RETURNING PATIENTS</th>
	                            <td><?php echo $revisit;?></td>
	                        </tr>
	                         <tr>
	                            <th>TOTAL PATIENTS</th>
	                            <td><?php echo $new_visits + $revisit;?></td>
	                        </tr>
	                    </tbody>
	                </table>
		        </div>
		         
		        <div class="col-md-2">
		            <h4 class="center-align">Collection Report</h4>
		            <table class="table table-striped table-hover table-condensed">
		                <tbody>
		                    <?php
		                    $total_cash_breakdown = 0;
		                  
		                    if($payment_methods->num_rows() > 0)
		                    {
		                        foreach($payment_methods->result() as $res)
		                        {
		                            $method_name = $res->payment_method;
		                            $payment_method_id = $res->payment_method_id;
		                            $total = 0;
		                            
		                            if($normal_payments->num_rows() > 0)
		                            {
		                                foreach($normal_payments->result() as $res2)
		                                {
		                                    $payment_method_id2 = $res2->payment_method_id;
		                                   
		                                
		                                    if($payment_method_id == $payment_method_id2)
		                                    {
		                                       
		                                        $total += $res2->cr_amount;
		                                    }
		                                }
		                            }
		                            
		                            $total_cash_breakdown += $total;
		                        
		                            echo 
		                            '
		                            <tr>
		                                <th>'.strtoupper($method_name).'</th>
		                                <td>'.number_format($total, 2).'</td>
		                            </tr>
		                            ';
		                        }
		                        
		                        echo 
		                        '
		                        <tr>
		                            <th>TOTAL</th>
		                            <th>'.number_format($total_cash_breakdown, 2).'</th>
		                        </tr>
		                        ';
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		       
		        <?php
		        $total_invoices_revenue =$month_revenue = $this->reports_model->get_visit_invoice_totals($first_day,$last_day);
			    $total_invoices_revenue -=  $total_credit_notes = $this->reports_model->get_visit_credits_totals($first_day,$last_day);
			    $total_payments_revenue = $this->reports_model->get_visit_payment_totals($first_day,$last_day);

			    $all_payments_period =  $this->reports_model->get_period_payment_totals($first_day,$last_day);
			    $receivable = $all_payments_period - $total_payments_revenue;

		        ?>
		        <div class="col-md-8">
			        <div class="col-md-12">
			        	<div class="col-md-4 center-align">
				            <h3> INVOICES</h3>   

				            <h4>Ksh <?php echo number_format($month_revenue, 2);?></h4>
				        </div>
				         <div class="col-md-4 center-align">
				            <h3> CREDIT NOTES</h3>   
				            <h4>Ksh <?php echo number_format($total_credit_notes, 2);?></h4>
				        </div>
				        <div class="col-md-4 center-align">
				            <h3> REVENUE</h3>   
				            <h4>Ksh <?php echo number_format($month_revenue-$total_credit_notes, 2);?></h4>
				        </div>
			        	
			        </div>
			        <div class="col-md-12">
			        
				        <div class="col-md-4 center-align">
				            <h3> PERIOD COLLECTION</h3>   
				            <h4>Ksh <?php echo number_format($total_payments_revenue, 2);?></h4>
				        </div>
				        <div class="col-md-4 center-align">
				            <h3>DEBT COLLECTED</h3>   
				            <h4>Ksh <?php echo number_format($receivable, 2);?></h4>
				        </div>
				        <div class="col-md-4 center-align">
				            <h3>COLLECTIONS</h3>   
				            <h4>Ksh <?php echo number_format($all_payments_period, 2);?></h4>
				        </div>
				        
				     </div>
			    </div>
		    </div>
	    </div>
	</section>


	<?php
	$date_tomorrow = date('Y-m-d');
	$doctor_results = $this->reports_model->get_all_doctors($first_day,$last_day,$branch_session);
	$counting =0;
	$date_from =$date_tomorrow;
	$date_to =$date_tomorrow;
	$doctors_results ='';
	if($doctor_results->num_rows() > 0)
	{
	$count = $full = $percentage = $daily = $hourly = 0;

		$doctors_results .=  
			'
				<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
				  <thead>
					<tr>
					  <th>#</th>
					  <th style="padding:5px;">DOCTOR</th>
					  <th style="padding:5px;">PATIENTS</th>
					  <th style="padding:5px;">CASH INVOICES</th>
					  <th style="padding:5px;">INSURANCE INVOICES</th>
					  <th style="padding:5px;">TOTAL REVENUE </th>
					  <th style="padding:5px;">PAYMENTS </th>
					  <th style="padding:5px;">BALANCES </th>
					</tr>
				</thead>
				<tbody>
			';
		$result = $doctor_results->result();
		$grand_total = 0;
		$patients_total = 0;
		$total_charge_waivers = 0;
		$total_revenue = 0;
		$total_payments_made = 0;
		$total_balances = 0;
		$total_cash = 0;
		$insurance_total = 0;
		$cash_total = 0;
		$grand_insurance =0;
		foreach($result as $res)
		{
			$personnel_id = $res->personnel_id;
			$personnel_onames = $res->personnel_onames;
			$personnel_fname = $res->personnel_fname;
			$patients = $res->total_patients;
			
			
			//get service total
			$cash_total = $this->reports_model->get_total_collected($personnel_id, $first_day, $last_day,1,$branch_session);
			$insurance_total = $this->reports_model->get_total_collected($personnel_id, $first_day, $last_day,2,$branch_session);
			// $patients = $this->reports_model->get_total_patients($personnel_id, $first_day, $last_day);
			$waivers = 0;//$this->reports_model->get_total_waivers($personnel_id, $first_day, $last_day);
			$payments_made = $this->reports_model->get_total_payments_made($personnel_id, $first_day, $last_day,$branch_session);



			$revenue = $cash_total + $insurance_total;
			$grand_total += $cash_total;
			$patients_total += $patients;
			$total_revenue += $revenue;
			$total_charge_waivers += $waivers;
			$total_payments_made += $payments_made;

			$balance_charged = $revenue - $payments_made;
			$grand_insurance += $insurance_total;

			$total_balances += $balance_charged;
			$total_cash += $cash_total;
			// if($patients > 0)
			// {
				$count++;
				$doctors_results.= '
					<tr>
						<td style="padding:5px;">'.$count.'</td>
						<td >'.strtoupper($personnel_fname).'</td>
						<td >'.$patients.'</td>
						<td >'.number_format($cash_total, 2).'</td>
						<td >'.number_format($insurance_total, 2).'</td>
						<td >'.number_format($revenue, 2).'</td>
						<td >('.number_format($payments_made, 2).')</td>
						<td >'.number_format($balance_charged, 2).'</td>
						
					</tr>
				';
			// }
		}
		$cash_total = $this->reports_model->get_total_collected(0, $first_day, $last_day,1,$branch_session);
		$payments_made = $this->reports_model->get_total_payments_made(0, $first_day, $last_day,$branch_session);
		// var_dump($cash_total);die();
		$revenue = $cash_total;
		$grand_total += $revenue;
		$total_revenue += $revenue;
		$balance_charged = $revenue - $payments_made;
		$total_payments_made += $payments_made;
		$total_cash += $cash_total;

		$total_balances += $balance_charged;
		$doctors_results.= '
					<tr>
						<td style="padding:5px;">'.$count.'</td>
						<td >WALKINS</td>
						<td ></td>
						<td >'.number_format($cash_total, 2).'</td>
						<td >'.number_format(0, 2).'</td>
						<td >'.number_format($revenue, 2).'</td>
						<td >('.number_format($payments_made, 2).')</td>
						<td >'.number_format($balance_charged, 2).'</td>
						
					</tr>
				';
		
		$doctors_results.= 
		'
			
				<tr>
					<td colspan="2">TOTAL</td>
					<td ><span class="bold" >'.$patients_total.'</span></td>
					<td ><span class="bold">'.number_format($grand_total, 2).'</span></td>
					<td ><span class="bold">'.number_format($grand_insurance, 2).'</span></td>
					<td ><span class="bold">'.number_format($total_revenue, 2).'</span></td>
					<td ><span class="bold">('.number_format($total_payments_made, 2).')</span></td>
					<td ><span class="bold">'.number_format($total_balances, 2).'</span></td>
				</tr>
			</tbody>
		</table>
		';
	}


	$visit_types_rs = $this->reports_model->get_visit_types($first_day,$last_day,$branch_session);
	$visit_results = '<table  class="table table-hover table-bordered table-condensed ">
						<thead>
							<tr>
								<th style="padding:5px;text-align:center;">PATIENT TYPE</th>
								<th style="padding:5px;text-align:center;">NO</th>
								<th style="padding:5px;text-align:center;">AMOUNT INVOICED</th>
								<th style="padding:5px;text-align:center;">AMOUNT COLLECTED</th>
								<th style="padding:5px;text-align:center;">BALANCE</th>
							</tr>
						</thead>
						</tbody> ';
	$total_balance = 0;
	$total_invoices = 0;
	$total_payments = 0;
	$total_patients = 0;
	$total_cash_invoices = 0;
	$total_insurance_invoices = 0;

	$total_cash_payments = 0;
	$total_insurance_payments = 0;

	$total_cash_balance = 0;
	$total_insurance_balance = 0;

	if($visit_types_rs->num_rows() > 0)
	{
		foreach ($visit_types_rs->result() as $key => $value) {
			# code...

			$visit_type_name = $value->visit_type_name;
			$visit_type_id = $value->visit_type_id;
			$total_visit_type_patients = $value->total_patients;



			// calculate invoiced amounts
			$report_response = $this->reports_model->get_visit_type_amounts($first_day,$last_day,$visit_type_id,$branch_session);

			$invoice_amount = $report_response['total_invoices'];
			$payments_value = $report_response['total_payments'];
			$balance = $invoice_amount - $payments_value;

			if($visit_type_id == 1)
			{
				$total_cash_invoices += $invoice_amount;
				$total_cash_payments += $payments_value;
				$total_cash_balance += $balance;
			}
			else
			{
				$total_insurance_invoices += $invoice_amount;
				$total_insurance_payments += $payments_value;
				$total_insurance_balance += $balance;
			}	

			// calculate amounts paid
			if($total_visit_type_patients > 0)
			{
				$visit_results .='<tr>
							  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
							  		<td style="text-align:center;"> '.$total_visit_type_patients.'</td>
							  		<td style="text-align:center;"> '.number_format($invoice_amount,2).'</td>
							  		<td style="text-align:center;"> '.number_format($payments_value,2).'</td>
							  		<td style="text-align:center;"> '.number_format($balance,2).'</td>
							  	</tr>';
			}
			$total_patients = $total_patients + $total_visit_type_patients;
			$total_invoices = $total_invoices + $invoice_amount;
			$total_payments = $total_payments + $payments_value;
			$total_balance = $total_balance + $balance;


		}

		$visit_results .='<tr>
						  		<td style="text-align:left;" colspan="1"> TOTAL </td>
						  		<td style="text-align:center;border-top:2px solid #000;" > '.$total_patients.' </td>
						  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_invoices,2).'</td>
						  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_payments,2).'</td>
						  		<td style="text-align:center;border-top:2px solid #000;">Ksh.'.number_format($total_balance,2).'</td>
						  	</tr>';
	}
	$visit_results .='</tbody>
					</table>';




	$expenses_rs = $this->reports_model->get_all_expenses_list($first_day,$last_day,$branch_session);
	$expense_results = '<table  class="table table-hover table-bordered table-condensed ">
						<thead>
							<tr>
								<th style="padding:5px;text-align:center;">EXPENSE ACCOUNT</th>
								<th style="padding:5px;text-align:center;">TRANSACTIONS</th>
								<th style="padding:5px;text-align:center;">AMOUNT</th>
							</tr>
						</thead>
						</tbody> ';


	$grand_expense = 0;
	$grand_transactions = 0;

	if($expenses_rs->num_rows() > 0)
	{
		foreach ($expenses_rs->result() as $key => $value) {
			# code...

			$accountName = $value->accountName;
			$total_expense = $value->total_expense;
			$total_transactions = $value->total_transactions;

			$grand_transactions += $total_transactions;
			$grand_expense += $total_expense;

			$expense_results .='<tr>
						  		<td style="text-align:left;"> '.strtoupper($accountName).'  </td>
						  		<td style="text-align:center;"> '.$total_transactions.'</td>
						  		<td style="text-align:center;"> '.number_format($total_expense,2).'</td>
						  	</tr>';
		}
			

		

		$expense_results .='<tr>
						  		<td style="text-align:left;" colspan="1"> TOTAL </td>
						  		<td style="text-align:center;border-top:2px solid #000;">'.$grand_transactions.'</td>
						  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($grand_expense,2).'</td>
						  	</tr>';
	}
	$expense_results .='</tbody>
					</table>';




	$grand_income = 0;
	$income_result = '';



	$revenue_rs = $this->reports_model->get_all_services();


	if($revenue_rs->num_rows() > 0)
	{
		$balance = 0;
		$total_amount = 0;
		// var_dump($revenue_rs->result());die();
		foreach ($revenue_rs->result() as $key => $value5) {
			# code...
			// get all transactions
			$service_id = $value5->service_id;
			$department_id = $value5->department_id;
			$service_name = $value5->service_name;



			// update the service Details
			$revenue_items_rs = $this->reports_model->get_receivables_transactions_per_service($service_id,1,$first_day,$last_day);


			if($revenue_items_rs->num_rows() > 0)
			{
				$balance = 0;
				$total_amount = 0;
				// var_dump($revenue_items_rs->result());die();
				foreach ($revenue_items_rs->result() as $key => $value6) 
				{

					// $dr_amount = $value6->dr_amount;
					$dr_amount = $value6->dr_amount;
					$cr_amount = $value6->cr_amount;
					
					$total_income = $dr_amount-$cr_amount;
					$grand_income += $total_income;

					$income_result .='<tr>
										<td class="text-left">'.strtoupper($service_name).'</td>
										<td class="text-right">'.number_format($total_income,2).'</td>
										</tr>';

				

				}
				
				
			}
		}

	}
	$income_result .='<tr>
							<td class="text-left"><b>TOTAL INCOME</b></td>
							<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
						</tr>';




	// expense


	$parent_account_id = $this->reports_model->get_parent_account_id('Cost of Goods');

	$account_rs = $this->reports_model->get_all_child_accounts($parent_account_id);

	// var_dump($account_rs->result());die();
	$grand_expense = 0;
	$total_operational_amount = 0;
	
	$operation_result ='';
	$grand_goods = 0;
	if($account_rs->num_rows() > 0)
	{
	 	foreach ($account_rs->result() as $key => $value) 
	 	{
	 		# code...

	 		$account_name = $value->account_name;
	 		$account_id = $value->account_id;


			$expense_of_account_rs = $this->reports_model->get_expense_account_transactions($account_id,1,$first_day,$last_day);

				// var_dump($parent_account_id);die();
			if($expense_of_account_rs->num_rows() > 0)
			{
				$balance = 0;
				$total_amount = 0;
				// var_dump($expense_of_account_rs->result());die();
				foreach ($expense_of_account_rs->result() as $key => $value4) {
					# code...
					// get all transactions

					$dr_amount = $value4->dr_amount;
					$cr_amount = $value4->cr_amount;
				

					

					$total_goods = $dr_amount-$cr_amount;
					$grand_expense += $total_goods;
					// $grand_balance += $balance;
					$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right">'.number_format($total_goods,2).'</td>
										</tr>';
				

				}
				

			}
		}
	}
	


	$parent_account_id2 = $this->reports_model->get_parent_account_id('Expense Accounts');
	$account_rs2 = $this->reports_model->get_all_child_accounts($parent_account_id2);

	
	if($account_rs2->num_rows() > 0)
	{
	 	foreach ($account_rs2->result() as $key => $value) 
	 	{
	 		# code...

	 		$account_name = $value->account_name;
	 		$account_id = $value->account_id;
	 		 
			$expense_of_account_rs =$this->reports_model->get_expense_account_transactions($account_id,1,$first_day,$last_day);
			// var_dump($account_rs2);die();

			if($expense_of_account_rs->num_rows() > 0)
			{

				
				$balance = 0;
				$total_amount = 0;
				// var_dump($expense_of_account_rs->result());die();
				foreach ($expense_of_account_rs->result() as $key => $value4) {
					# code...
					// get all transactions

					$dr_amount = $value4->dr_amount;
					$cr_amount = $value4->cr_amount;
					$accountName = $value4->accountName;

					$total_expense = $dr_amount-$cr_amount;
					$grand_expense += $total_expense;

					
					$operation_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).'</td>
											<td class="text-right">'.number_format($total_expense,2).'</td>
										</tr>';

				}
				

			}
		}
	}


	$parent_account_id2 = $this->reports_model->get_parent_account_id('Payroll');
	$account_rs2 = $this->reports_model->get_all_child_accounts($parent_account_id2);

	// $grand_expense = 0;
	if($account_rs2->num_rows() > 0)
	{
	 	foreach ($account_rs2->result() as $key => $value) 
	 	{
	 		# code...

	 		$account_name = $value->account_name;
	 		$account_id = $value->account_id;
	 		 
			$expense_of_account_rs = $this->reports_model->get_expense_account_transactions($account_id,1,$first_day,$last_day);

			// var_dump($account_rs2);die();

			if($expense_of_account_rs->num_rows() > 0)
			{

				
				$balance = 0;
				$total_amount = 0;
				// var_dump($expense_of_account_rs->result());die();
				foreach ($expense_of_account_rs->result() as $key => $value4) {
					# code...
					// get all transactions

					$dr_amount = $value4->dr_amount;
					$cr_amount = $value4->cr_amount;
					$accountName = $value4->accountName;

					$total_expense = $dr_amount-$cr_amount;
					$grand_expense += $total_expense;

					
					$operation_result .='<tr>
											<td class="text-left">'.strtoupper($account_name).'</td>
											<td class="text-right">'.number_format($total_expense,2).'</td>
										</tr>';

				}
				

			}
		}
	}

	$operation_result .='<tr>
							<td class="text-left"><b>TOTAL OPERATING EXPENSE</b></td>
							<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_expense,2).'</b></td>
						</tr>';
	$operation_result .='<tr>
							<td class="text-left"><b>PERIOD PROFIT</b></td>
							<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income - $grand_expense,2).'</b></td>
						</tr>';

	?>

	<section class="panel panel-featured panel-featured-info">
	    <header class="panel-heading">
	    	<h2 class="panel-title">Doctors Summaries</h2>
	    </header>             
	    <div class="panel-body">
	    	<?php echo $doctors_results;?>


	    </div>
	</section>

	<section class="panel panel-featured panel-featured-info">
	    <header class="panel-heading">
	    	<h2 class="panel-title">Visit Types Summaries</h2>
	    </header>             
	    <div class="panel-body">
	    	<?php echo $visit_results;?>
	    </div>
	</section>

	<div class="row">
		<div class="col-md-2">
			
			
		</div>
		<div class="col-md-8">
			<section class="panel panel-featured panel-featured-info">
			    <header class="panel-heading">
			    	<h2 class="panel-title">INCOME STATEMENT OF THE MONTH</h2>
			    	<a href="<?php echo site_url().'company-financials/profit-and-loss'?>" class="btn btn-sm btn-success pull-right" style="margin-top:-25px;margin-left:5px"> GO TO P&L</a>
			    </header>             
			    <div class="panel-body">
			    	<table  class="table table  table-striped table-condensed ">
						<thead>
							<tr>
								<th class="text-left"> INCOME ACCOUNTS</th>
								<th class="text-right" >REVENUES</th>
							</tr>
						</thead>
						</tbody>
			    			<?php echo $income_result;?>
			    		</tbody>
					</table>
			    	<table class="table  table-striped table-condensed">
					<thead>
						<tr>
		        			<th class="text-left"> EXPENSE ACCOUNTS</th>
							<th class="text-right">EXPENSE AMOUNT</th>
						</tr>
					</thead>
					<tbody>
						
						<?php echo $operation_result;?>

					</tbody>
				</table>
			    </div>
			</section>
			
		</div>
		<div class="col-md-2">
			
			
		</div>
	</div>
	


	

	</div>
</div>