<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Payments</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 9px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:9px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
				.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}


		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        <?php
        ?>
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong><?php echo $title?></strong>
            </div>
        </div>
        
    	<div class="row">
        	<div class="col-md-12">
        		<?php

        		$summary_report_search = $this->session->userdata('summary_report_search');
					if(isset($summary_report_search))
					{
						$patient_add = str_replace('visit.visit_date','DATE(patients.patient_date)',$summary_report_search);
						//AND DATE(visit.visit_time_out) = "'.$date_today.'"
						$visit_add2 = str_replace('visit.visit_date', 'DATE(visit.visit_time_out)',$summary_report_search);
						$visit_add = $summary_report_search;
					}
					else{
						$date_today = date('Y-m-d');
						$visit_add = ' AND visit.visit_date = "'.$date_today.'"';
						$patient_add = ' AND DATE(patients.patient_date) = "'.$date_today.'"';
						$visit_add = ' AND DATE(visit.visit_time_out) = "'.$date_today.'"';

					}
						$date_today = date('Y-m-d');
						$visit_types_rs = $this->reception_model->get_visit_types();
						$visit_results = '';
					
						if($visit_types_rs->num_rows() > 0)
						{
							foreach ($visit_types_rs->result() as $key => $value) {
								# code...

								$visit_type_name = $value->visit_type_name;
								$visit_type_id = $value->visit_type_id;


								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 0 AND patients.patient_delete = 0'.$visit_add;	
								$total_outpatient_patients = $this->reception_model->count_items($table,$where);
							




								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 1 AND patients.patient_delete = 0'.$visit_add;
								$total_inpatient_patients = $this->reception_model->count_items($table,$where);



								$total_visits = $total_outpatient_patients + $total_inpatient_patients;
								// calculate amounts paid
							
								$visit_results .='<tr>
												  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
												  		<td style="text-align:center;"> '.$total_outpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_inpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_visits.'</td>
												  	</tr>';
								
					


							}

							
						}


					?>
			
            <div class="col-print-6">
            	<div style="padding:5px;">
            		<h5>NEW VISITS AND REVISITS</h5>	

	        		<table  class="table table-hover table-bordered ">
						<thead>
							<tr>
								<th style="padding:5px;">VISIT TYPE</th>
								<th style="padding:5px;">OUTPATIENTS</th>
								<th style="padding:5px;">INPATIENTS</th>
								<th style="padding:5px;">TOTAL PATIENTS</th>
								
							</tr>
						</thead>
						</tbody> 
						  	<?php echo $visit_results?>
					  	</tbody>
					</table>
            		
            	</div>
            	
            </div>
            <div class="col-print-1">
            	&nbsp;
			</div>

            <div class="col-print-5">
            	<div style="padding:5px;">

		        	<h5>NEW VISITS AND REVISITS PER DEPARTMENT</h5>	
					<table class="table table-hover table-bordered ">
	                  <thead>
	                    <tr>
	                      <th>#</th>
	                      <th>DEPARTMENT</th>
	                      <th>NEW VISITS</th>
	                      <th>REVISITS</th>
	                      <th>TOTAL</th>
	                    </tr>
	                  </thead>
	                  <tbody>
					
		            <?php
		               
		                 $this->db->where('report_item = 1');
		                $query = $this->db->get('departments');
		                $result = '';
		                $grand_number_new = 0;
		                $grand_number_old = 0;
		                $grand_number = 0;
		                //if users exist display them
		                if ($query->num_rows() > 0)
		                {
		                    $count = $page;
		                    
		                    $result .= 
		                        '
		                            
		                        ';

		                    
		                    foreach ($query->result() as $row)
		                    {
		                        
		                        $department_name = $row->department_name;
		                        $department_id = $row->department_id;
		                        
		                        $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.revisit <= 1 AND patients.patient_type = 0'.$visit_add;
		                        $community_table = 'visit,patients';
		                        $total_number_new = $this->reception_model->count_items($community_table, $community_where);

		                        $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.revisit = 2 AND patients.patient_type = 0'.$visit_add;
		                        $community_table = 'visit,patients';
		                        $total_number_old = $this->reception_model->count_items($community_table, $community_where);
		                        $total = $total_number_new + $total_number_old;

		                        $grand_number_new += $total_number_new ;
		                    	$grand_number_old += $total_number_old;
		                    	$grand_number += $total;
		                        $count++;
		                        $result .= 
		                            '
		                                <tr>
		                                    <td>'.$count.'</td>
		                                    <td>'.strtoupper($department_name).'</td>
		                                    <td>'.$total_number_new.'</td>
		                                    <td>'.$total_number_old.'</td>
		                                    <td>'.$total.'</td>
		                                    
		                                </tr> 
		                            ';
		                    }

		                    $result .= 
		                            '
		                                <tr>
		                                    <th></th>
		                                    <th>TOTAL</th>
		                                    <th>'.$grand_number_new.'</th>
		                                    <th>'.$grand_number_old.'</th>
		                                    <th>'.$grand_number.'</th>
		                                    
		                                </tr> 
		                            ';
		                    
		                   
		                }
		                
		                else
		                {
		                    $result .= "There are no departments";
		                }
		                
		                echo $result;



		                ?>
							</tbody>
		               	</table>
		                <?php

		                $date_today = date('Y-m-d');
						$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 1 AND visit.visit_delete = 0  AND patients.patient_delete = 0'.$visit_add;
						$community_table = 'patients,visit';
						$total_men = $this->reception_model->count_items($community_table, $community_where);


						  $date_today = date('Y-m-d');
						$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 2 AND visit.visit_delete = 0  AND patients.patient_delete = 0'.$visit_add;
						$community_table = 'patients,visit';
						$total_female = $this->reception_model->count_items($community_table, $community_where);


						$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 0 AND visit.visit_delete = 0  AND patients.patient_delete = 0'.$visit_add;
						$community_table = 'patients,visit';
						$not_indicated = $this->reception_model->count_items($community_table, $community_where);
		                ?>
		                <h5>VISITS PER GENDER</h5>	
		    			
		            	<table class="table table-hover table-bordered ">
		              		<tbody>
		              			<tr>
			                      <th>MALE</th>
			                      <td><?php echo $total_men?></td>
			                      <th>FEMALE</th>
			                      <td><?php echo $total_female?></td>
			                      <th>NOT INDICATED</th>
			                      <td><?php echo $not_indicated?></td>
			                    </tr>
		              			
		              		</tbody>
		                    
		                 
		             
		                 </table>
		            </div>

            </div>
            
        </div>
    </body>
    
</html>