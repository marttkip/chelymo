<!-- dates  -->
<?php

$where = 'v_transactions_by_date.transactionCategory = "Revenue Payment"';
		
$table = 'v_transactions_by_date';
$visit_search = $this->session->userdata('cash_report_search');

// if(!empty($visit_search))
// {
// 	$where .= $visit_search;
// }
// else
// {
$where .=' AND v_transactions_by_date.transaction_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" ';
// }

$branch_session = $branch_id;
// var_dump($branch_session);die();
if($branch_session > 0)
{
	$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
	

}
$where2 = $where;

$normal_payments = $this->reports_model->get_normal_payments($where2, $table, 'cash');
$payment_methods = $this->reports_model->get_payment_methods($where2, $table, 'cash');



$branch_add = '';
$visit_branch_add = '';
if($branch_session > 0)
{
	$branch_add = ' AND branch_id = '.$branch_session;
	$visit_branch_add = ' AND visit.branch_id = '.$branch_session;
}

$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND appointments.appointment_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$total_appointments = $this->reception_model->count_items($table, $where);



$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 ) AND appointments.appointment_date BETWEEN "'.$first_day.'" AND "'.$last_day.'"'.$visit_branch_add;
$table = 'patients,visit,appointments';
$honoured_appointments = $this->reception_model->count_items($table, $where);








$where = 'patients.patient_delete = 0 AND visit.appointment_id = 0 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit';
$coming_appointments = $this->reception_model->count_items($table, $where);




$where = 'patients.patient_delete = 0 AND (visit.revisit = 1 OR visit.revisit = 0) AND close_card <> 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0   AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit';
$new_visits = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.revisit = 2 AND close_card <> 2 AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND visit.visit_date BETWEEN "'.$first_day.'" AND "'.$last_day.'" '.$visit_branch_add;
$table = 'patients,visit';
$revisit = $this->reception_model->count_items($table, $where);





// $patients = $this->reports_model->get_patients_visits(1);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $title?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        		font-size: 8px;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 10px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:8px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			.table td
			{
				padding: 3px !important;
			}
			/*th, td {
			    border: 1px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			thead tr:first-child th:first-child {
			    border-radius: 10px 0 0 0 !important;
			}
			thead tr:first-child th:last-child {
			    border-radius: 0 10px 0 0 !important;
			}
			tbody tr:last-child td:first-child {
			    border-radius: 0 0 0 10px !important;
			}
			tbody tr:last-child td:last-child {
			    border-radius: 0 0 10px 10px !important;
			}
			tbody tr:first-child td:first-child {
			    border-radius: 10px 10px 0 0 !important;
			    border-bottom: 0px solid #000 !important;
			}*/
			.padd
			{
				padding:10px;
			}
			
		</style>
    </head>
    <body onLoad="window.print();return false;">
    	
 		

    	<div class="padd " >
    		<div class="col-print-12">
	    		<div class="col-print-4">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" style="margin-bottom: 10px;" />
	            </div>
	            <div class="col-print-3" >
	            	<h4 ><strong>REPORT</strong></h4>
	            </div>
	        	<div class="col-print-5">
	            	<p style="font-size: 11px;">
	                	<strong><?php echo $contacts['company_name'];?></strong> <br/>
	                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
	                    Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
	                    Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
	              		E-mail: <strong><?php echo $contacts['email'];?>.</strong><br/>
	                </p>
	            </div>
	        </div>
    
        
	      	<div class="row" style="margin-top: 15px;" >
	      		<div class="col-md-12">
	      			<div class="col-print-6 left-align">
	      				<p style="font-size: 15px;"><?php echo $title;?> </p>
		            	<p style="font-size: 15px;">Date printed: <?php echo date('jS M Y'); ?> </p>
		            </div>
		            <div class="col-print-3">
		            	&nbsp;
		            </div>
		            <div class="col-print-3">
		            	
		            	&nbsp;
		            </div>
	      			
	      		</div>
	        	
	        </div>
	       
	      	<div class="row"  style="border: 1px solid #000;">
	      		<div class="col-md-12">
	      			<div class="col-print-12">
	      				<div class="col-print-6">
					        <div class="col-print-6">
					            <h5 class="center-align">Visit's Summary</h5>
					            <table class="table table-striped table-hover table-condensed">
				                	<thead>
				                    	<tr>
				                        	<th>Type</th>
				                            <th>Visits</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <tr>
				                            <th>BOOKINGS</th>
				                            <td><?php echo $honoured_appointments;?></td>
				                        </tr>
				                        <tr>
				                            <th>WALKIN'S</th>
				                            <td><?php echo $coming_appointments;?></td>
				                        </tr>
				                    </tbody>
				                </table>
				                <h5 class="center-align">New Visit vs Re-visits</h5>
					            <table class="table table-striped table-hover table-condensed">
				                	<thead>
				                    	<tr>
				                        	<th>Type</th>
				                            <th>Visits</th>
				                        </tr>
				                    </thead>
				                    <tbody>
				                        <tr>
				                            <th>NEW PATIENTS</th>
				                            <td><?php echo $new_visits;?></td>
				                        </tr>
				                        <tr>
				                            <th>RETURNING PATIENTS</th>
				                            <td><?php echo $revisit;?></td>
				                        </tr>
				                         <tr>
				                            <th>TOTAL PATIENTS</th>
				                            <td><?php echo $new_visits + $revisit;?></td>
				                        </tr>
				                    </tbody>
				                </table>
					        </div>
					         
					        <div class="col-print-6">
					            <h5 class="center-align">Collection Report</h5>
					            <table class="table table-striped table-hover table-condensed">
					            	<thead>
				                    	<tr>
				                        	<th>Method</th>
				                            <th>Amount</th>
				                        </tr>
				                    </thead>
					                <tbody>
					                    <?php
					                    $total_cash_breakdown = 0;
					                  
					                    if($payment_methods->num_rows() > 0)
					                    {
					                        foreach($payment_methods->result() as $res)
					                        {
					                            $method_name = $res->payment_method;
					                            $payment_method_id = $res->payment_method_id;
					                            $total = 0;
					                            
					                            if($normal_payments->num_rows() > 0)
					                            {
					                                foreach($normal_payments->result() as $res2)
					                                {
					                                    $payment_method_id2 = $res2->payment_method_id;
					                                   
					                                
					                                    if($payment_method_id == $payment_method_id2)
					                                    {
					                                       
					                                        $total += $res2->cr_amount;
					                                    }
					                                }
					                            }
					                            
					                            $total_cash_breakdown += $total;
					                        
					                            echo 
					                            '
					                            <tr>
					                                <th>'.strtoupper($method_name).'</th>
					                                <td>'.number_format($total, 2).'</td>
					                            </tr>
					                            ';
					                        }
					                        
					                        echo 
					                        '
					                        <tr>
					                            <th>TOTAL</th>
					                            <th>'.number_format($total_cash_breakdown, 2).'</th>
					                        </tr>
					                        ';
					                    }
					                    ?>
					                </tbody>
					            </table>
					        </div>
				       </div>
				        <?php
				        $revenue = $this->reports_model->get_total_revenue($first_day,$last_day,$branch_session);
				        $period_collection =  $this->reports_model->get_debt_payment_totals(0,$first_day,$last_day,$branch_session); 
				        $debt_collection = $this->reports_model->get_debt_payment_totals(1,$first_day,$last_day,$branch_session); 
				        $expenses = $this->reports_model->get_all_expenses($first_day,$last_day,$branch_session); 

				        ?>
				        <div class="col-print-6">
				        	<div class="col-md-12">
				        		<div class="col-print-6 ">
						            <h6> Invoices</h6>   
						            <p>Ksh <?php echo number_format($revenue, 2);?></p>
						        </div>
						        <div class="col-print-6 ">
						            <h6> Period Collection</h6>   
						            <p>Ksh <?php echo number_format($period_collection, 2);?></p>
						        </div>
						        
				        	</div>
				        	<div class="col-md-12">
				        		<div class="col-print-6 ">
						            <h6>Debt Collected</h6>   
						            <p>Ksh <?php echo number_format($debt_collection, 2);?></p>
						        </div>
						        <div class="col-print-6 ">
						            <h6> Expenses</h6>   
						            <p>Ksh <?php echo number_format($expenses, 2);?></p>
						        </div>
				        	</div>
				        </div>
				       
	      			</div>
	      		</div>

	      		<div class="col-md-12">
	      			 <h4>Doctors Summaries</h4>
	      		</div>


				<?php
				$date_tomorrow = date('Y-m-d');
				$doctor_results = $this->reports_model->get_all_doctors($first_day,$last_day,$branch_session);
				$counting =0;
				$date_from =$date_tomorrow;
				$date_to =$date_tomorrow;
				$doctors_results ='';
				if($doctor_results->num_rows() > 0)
				{
				$count = $full = $percentage = $daily = $hourly = 0;

					$doctors_results .=  
						'
							<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
							  <thead>
								<tr>
								  <th>#</th>
								  <th style="padding:5px;">DOCTOR</th>
								  <th style="padding:5px;">PATIENTS</th>
								  <th style="padding:5px;">CASH INVOICES</th>
				  					<th style="padding:5px;">INSURANCE INVOICES</th>
								  <th style="padding:5px;">TOTAL REVENUE </th>
								  <th style="padding:5px;">PAYMENTS </th>
								  <th style="padding:5px;">BALANCES </th>
								</tr>
							</thead>
							<tbody>
						';
					$result = $doctor_results->result();
					$grand_total = 0;
					$patients_total = 0;
					$total_charge_waivers = 0;
					$total_revenue = 0;
					$total_payments_made = 0;
					$total_balances = 0;
					$total_cash = 0;
					$insurance_total = 0;
					$cash_total = 0;
					$grand_insurance =0;
					foreach($result as $res)
					{
						$personnel_id = $res->personnel_id;
						$personnel_onames = $res->personnel_onames;
						$personnel_fname = $res->personnel_fname;
						$patients = $res->total_patients;
						
						
						//get service total
						$cash_total = $this->reports_model->get_total_collected($personnel_id, $first_day, $last_day,1,$branch_session);
						$insurance_total = $this->reports_model->get_total_collected($personnel_id, $first_day, $last_day,2,$branch_session);
						// $patients = $this->reports_model->get_total_patients($personnel_id, $first_day, $last_day);
						$waivers = 0;//$this->reports_model->get_total_waivers($personnel_id, $first_day, $last_day);
						$payments_made = $this->reports_model->get_total_payments_made($personnel_id, $first_day, $last_day,$branch_session);



						$revenue = $cash_total + $insurance_total;
						$grand_total += $cash_total;
						$patients_total += $patients;
						$total_revenue += $revenue;
						$total_charge_waivers += $waivers;
						$total_payments_made += $payments_made;

						$balance_charged = $revenue - $payments_made;
						$grand_insurance += $insurance_total;

						$total_balances += $balance_charged;
						$total_cash += $cash_total;
						// if($patients > 0)
						// {
							$count++;
							$doctors_results.= '
								<tr>
									<td style="padding:5px;">'.$count.'</td>
									<td >'.strtoupper($personnel_fname).'</td>
									<td >'.$patients.'</td>
									<td >'.number_format($cash_total, 2).'</td>
									<td >'.number_format($insurance_total, 2).'</td>
									<td >'.number_format($revenue, 2).'</td>
									<td >('.number_format($payments_made, 2).')</td>
									<td >'.number_format($balance_charged, 2).'</td>
									
								</tr>
							';
						// }
					}
					$cash_total = $this->reports_model->get_total_collected(0, $first_day, $last_day,1,$branch_session);
					$payments_made = $this->reports_model->get_total_payments_made(0, $first_day, $last_day,$branch_session);
					// var_dump($cash_total);die();
					$revenue = $cash_total;
					$grand_total += $revenue;
					$total_revenue += $revenue;
					$balance_charged = $revenue - $payments_made;
					$total_payments_made += $payments_made;
					$total_cash += $cash_total;

					$total_balances += $balance_charged;
					$doctors_results.= '
								<tr>
									<td style="padding:5px;">'.$count.'</td>
									<td >WALKINS</td>
									<td ></td>
									<td >'.number_format($cash_total, 2).'</td>
									<td >'.number_format(0, 2).'</td>
									<td >'.number_format($revenue, 2).'</td>
									<td >('.number_format($payments_made, 2).')</td>
									<td >'.number_format($balance_charged, 2).'</td>
									
								</tr>
							';
								
					$doctors_results.= 
					'
						
							<tr>
								<td colspan="2">TOTAL</td>
								<td ><span class="bold" >'.$patients_total.'</span></td>
								<td ><span class="bold">'.number_format($grand_total, 2).'</span></td>
								<td ><span class="bold">'.number_format($grand_insurance, 2).'</span></td>
								<td ><span class="bold">'.number_format($total_revenue, 2).'</span></td>
								<td ><span class="bold">('.number_format($total_payments_made, 2).')</span></td>
								<td ><span class="bold">'.number_format($total_balances, 2).'</span></td>
							</tr>
						</tbody>
					</table>
					';
				}


				$visit_types_rs = $this->reports_model->get_visit_types($first_day,$last_day,$branch_session);
				$visit_results = '<table  class="table table-hover table-bordered table-condensed ">
									<thead>
										<tr>
											<th style="padding:5px;text-align:center;">PATIENT TYPE</th>
											<th style="padding:5px;text-align:center;">NO</th>
											<th style="padding:5px;text-align:center;">AMOUNT INVOICED</th>
											<th style="padding:5px;text-align:center;">AMOUNT COLLECTED</th>
											<th style="padding:5px;text-align:center;">BALANCE</th>
										</tr>
									</thead>
									</tbody> ';
				$total_balance = 0;
				$total_invoices = 0;
				$total_payments = 0;
				$total_patients = 0;
				$total_cash_invoices = 0;
				$total_insurance_invoices = 0;

				$total_cash_payments = 0;
				$total_insurance_payments = 0;

				$total_cash_balance = 0;
				$total_insurance_balance = 0;

				if($visit_types_rs->num_rows() > 0)
				{
					foreach ($visit_types_rs->result() as $key => $value) {
						# code...

						$visit_type_name = $value->visit_type_name;
						$visit_type_id = $value->visit_type_id;
						$total_visit_type_patients = $value->total_patients;



						// calculate invoiced amounts
						$report_response = $this->reports_model->get_visit_type_amounts($first_day,$last_day,$visit_type_id,$branch_session);

						$invoice_amount = $report_response['total_invoices'];
						$payments_value = $report_response['total_payments'];
						$balance = $invoice_amount - $payments_value;

						if($visit_type_id == 1)
						{
							$total_cash_invoices += $invoice_amount;
							$total_cash_payments += $payments_value;
							$total_cash_balance += $balance;
						}
						else
						{
							$total_insurance_invoices += $invoice_amount;
							$total_insurance_payments += $payments_value;
							$total_insurance_balance += $balance;
						}	

						// calculate amounts paid
						if($total_visit_type_patients > 0)
						{
							$visit_results .='<tr>
										  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
										  		<td style="text-align:center;"> '.$total_visit_type_patients.'</td>
										  		<td style="text-align:center;"> '.number_format($invoice_amount,2).'</td>
										  		<td style="text-align:center;"> '.number_format($payments_value,2).'</td>
										  		<td style="text-align:center;"> '.number_format($balance,2).'</td>
										  	</tr>';
						}
						$total_patients = $total_patients + $total_visit_type_patients;
						$total_invoices = $total_invoices + $invoice_amount;
						$total_payments = $total_payments + $payments_value;
						$total_balance = $total_balance + $balance;


					}

					$visit_results .='<tr>
									  		<td style="text-align:left;" colspan="1"> TOTAL </td>
									  		<td style="text-align:center;border-top:2px solid #000;" > '.$total_patients.' </td>
									  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_invoices,2).'</td>
									  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($total_payments,2).'</td>
									  		<td style="text-align:center;border-top:2px solid #000;">Ksh.'.number_format($total_balance,2).'</td>
									  	</tr>';
				}
				$visit_results .='</tbody>
								</table>';




				$expenses_rs = $this->reports_model->get_all_expenses_list($first_day,$last_day,$branch_session);
				$expense_results = '<table  class="table table-hover table-bordered table-condensed ">
									<thead>
										<tr>
											<th style="padding:5px;text-align:center;">EXPENSE ACCOUNT</th>
											<th style="padding:5px;text-align:center;">TRANSACTIONS</th>
											<th style="padding:5px;text-align:center;">AMOUNT</th>
										</tr>
									</thead>
									</tbody> ';


				$grand_expense = 0;
				$grand_transactions = 0;

				if($expenses_rs->num_rows() > 0)
				{
					foreach ($expenses_rs->result() as $key => $value) {
						# code...

						$accountName = $value->accountName;
						$total_expense = $value->total_expense;
						$total_transactions = $value->total_transactions;

						$grand_transactions += $total_transactions;
						$grand_expense += $total_expense;

						$expense_results .='<tr>
									  		<td style="text-align:left;"> '.strtoupper($accountName).'  </td>
									  		<td style="text-align:center;"> '.$total_transactions.'</td>
									  		<td style="text-align:center;"> '.number_format($total_expense,2).'</td>
									  	</tr>';
					}
						

					

					$expense_results .='<tr>
									  		<td style="text-align:left;" colspan="1"> TOTAL </td>
									  		<td style="text-align:center;border-top:2px solid #000;">'.$grand_transactions.'</td>
									  		<td style="text-align:center;border-top:2px solid #000;">Ksh. '.number_format($grand_expense,2).'</td>
									  	</tr>';
				}
				$expense_results .='</tbody>
								</table>';


				$grand_income = 0;
				$income_result = '<table  class="table table-hover table-bordered table-condensed ">
									<thead>
										<tr>
											<th style="padding:5px;text-align:center;"> SERVICE</th>
											<th style="padding:5px;text-align:center;">REVENUE</th>
										</tr>
									</thead>
									</tbody>';



				$revenue_rs = $this->reports_model->get_all_services();


				if($revenue_rs->num_rows() > 0)
				{
					$balance = 0;
					$total_amount = 0;
					// var_dump($revenue_rs->result());die();
					foreach ($revenue_rs->result() as $key => $value5) {
						# code...
						// get all transactions
						$service_id = $value5->service_id;
						$department_id = $value5->department_id;
						$service_name = $value5->service_name;



						// update the service Details
						$revenue_items_rs = $this->reports_model->get_receivables_transactions_per_service($service_id,1,$first_day,$last_day);


						if($revenue_items_rs->num_rows() > 0)
						{
							$balance = 0;
							$total_amount = 0;
							// var_dump($revenue_items_rs->result());die();
							foreach ($revenue_items_rs->result() as $key => $value6) 
							{

								// $dr_amount = $value6->dr_amount;
								$dr_amount = $value6->dr_amount;
								$cr_amount = $value6->cr_amount;
								
								$total_income = $dr_amount-$cr_amount;
								$grand_income += $total_income;

								$income_result .='<tr>
													<td class="text-left">'.strtoupper($service_name).'</td>
													<td class="text-right">'.number_format($total_income,2).'</td>
													</tr>';

							

							}
							
							
						}
					}

				}
				$income_result .='<tr>
										<td class="text-left"><b>TOTAL SERVICES REVENUE</b></td>
										<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
									</tr>';
				$income_result .='</tbody>
								</table>';


				?>
	      		<div class="col-md-12">
	      			<div class="col-print-12">
	      				<?php echo $doctors_results;?>
	      			</div>
	      		</div>
	      		<div class="col-md-12">
	      			<div class="col-print-6">
	      				<div class="col-md-12">
			      			 <h4>SERVICES SUMMARIES</h4>
			      		</div>
			      		<div class="col-md-12">
			      			<div class="col-print-12">
			      				<?php echo $income_result;?>
			      			</div>
			      		</div>
	      				
	      			</div>
	      			<div class="col-print-6">
	      				
	      			
			      		<div class="col-md-12">
			      			 <h4>Visit Summaries</h4>
			      		</div>
			      		<div class="col-md-12">
			      			<div class="col-print-12">
			      				<?php echo $visit_results;?>
			      			</div>
			      		</div>
			      	</div>
	      		<p style="page-break-before: always">
	      		<div class="col-md-12">
	      			 <h4>Expense Summaries</h4>
	      		</div>
	      		<div class="col-md-12">
	      			<div class="col-print-12">
	      				<?php echo $expense_results;?>
	      			</div>
	      		</div>

	      		

	      		<?php
	      		
				
				$where = 'v_transactions_by_date.transactionCategory = "Revenue Payment"';
				
				$table = 'v_transactions_by_date';
				
				$where .=' AND v_transactions_by_date.transaction_date BETWEEN "'.$first_day.'" AND "'.$last_day.'"';
				


				if($branch_session > 0)
				{
					$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
					// $where .= $visit_search;
				
				}

				$query = $this->reports_model->get_all_payments_transactions($table,$where);
				$payments_result = '';
				if ($query->num_rows() > 0)
				{
					$count = 0;
					
					$payments_result .= 
						'
							<table class="table table-bordered table-striped table-responsive col-md-12">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>Payment Type</th>
								  <th>Payment Date</th>
								  <th>Invoice Date</th>
								  <th>Patient</th>
								  <th>Payment Method</th>
								  <th>Amount</th>
								  <th>Receipt No</th>
								  <th>Paid to</th>
								  <th>Recorded by</th>
								</tr>
							  </thead>
							  <tbody>
					';
					$total_payments = 0;
					$period_payment = 0;
					$debt_payment = 0;
					foreach ($query->result() as $row)
					{
						$count++;
						$total_invoiced = 0;
						$payment_date = $row->transaction_date;
						$payment_created = date('jS M Y',strtotime($row->transaction_date));
						$time = date('H:i a',strtotime($row->created_at));
					
						$patient_id = $row->patient_id;
						$personnel_id = $row->personnel_id;
						$patient_othernames = $row->patient_othernames;
						$patient_surname = $row->patient_surname;
						$payment_method = $row->payment_method;
						$cr_amount = $row->cr_amount;
						$reference_code = $row->reference_code;
						$transaction_description = $row->transaction_description;
						$transactionClassification = $row->transactionClassification;
						$visit_invoice_number = $row->visit_invoice_number;
						$invoice_date = $row->invoice_date;
						$branch_code = $row->branch_code;
						$created_by = $row->personnel_fname.' '.$row->personnel_onames;

						if(!empty($invoice_date))
						{
							$invoice = date('jS M Y',strtotime($row->invoice_date));
						}
						else
						{
							$invoice ='';
						}
						

						if($payment_date == $invoice_date)
						{
							$type = 'Period Payment';
							$color = 'success';
							$period_payment += $cr_amount;
						}
						else
						{
							$type = 'Debt repayment';
							$color = 'info';
							$debt_payment += $cr_amount;
						}
						
						$total_payments += $cr_amount;

							$payments_result .= 
								'
									<tr>
										<td class="'.$color.'">'.$count.'</td>
										<td class="'.$color.'">'.$type.'</td>
										<td>'.$payment_created.'</td>
										<td>'.$invoice.'</td>
										<td>'.ucwords(strtolower($patient_surname)).'</td>
										<td>'.$payment_method.'</td>
										<td>'.number_format($cr_amount, 2).'</td>
										<td>'.$reference_code.'</td>
										<td>'.$visit_invoice_number.'</td>
										<td>'.$created_by.'</td>
									</tr> 
							';

					}
					$payments_result .= 
								'
									<tr>
										<th></th>
										<th></th>
										<th></th>

										<th></th>
										<th></th>
										<th>Total</th>
										<th>'.number_format($total_payments, 2).'</th>
										<th></th>
										<th></th>
										<th></th>
									</tr> 
							';
					
					$payments_result .= 
					'
								  </tbody>
								</table>
					';
				}
				
				else
				{
					$payments_result .= "There are no payments";
				}
				
	      		?>
	      		<p style="page-break-before: always">
	      		<div class="col-md-12">
	      			 <h4>Payments Done</h4>
	      		</div>
	      		<div class="col-md-12">
	      			 <?php echo $payments_result;?>
	      		</div>

	      		<?php
	      		$where = 'v_transactions_by_date.transactionCategory = "Revenue" AND visit_invoice.visit_invoice_id = v_transactions_by_date.transaction_id';
				
				$table = 'v_transactions_by_date,visit_invoice';
				
				$where .=' AND v_transactions_by_date.transaction_date BETWEEN "'.$first_day.'" AND "'.$last_day.'"';
				


				if($branch_session > 0)
				{
					$where .= ' AND v_transactions_by_date.branch_id = '.$branch_session;
					// $where .= $visit_search;
				
				}

				$query = $this->reports_model->get_all_invoices_transactions($table,$where);
				$invoice_result ='';
						//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = 0;
					
					$invoice_result .= 
						'
							<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
							  <thead>
								<tr>
									  <th>#</th>
									  <th>Invoice Date</th>
									  <th>Patient No.</th>
									  <th>Patient</th>
									  <th>Category</th>
									  <th>Doctor</th>
									  <th>Invoice No.</th>
									  <th>Invoice Amount</th>
									  <th>Payments.</th>
									  <th>Balance.</th>
								</tr>
							  </thead>
							  <tbody>
					';
					
					$total_waiver = 0;
					$total_payments = 0;
					$total_invoice = 0;
					$total_balance = 0;
					$total_rejected_amount = 0;
					$total_cash_balance = 0;
					$total_insurance_payments =0;
					$total_insurance_invoice =0;
					$total_payable_by_patient = 0;
					$total_payable_by_insurance = 0;
					$total_debit_notes = 0;
					$total_credit_notes= 0;
					$total_balance =0;
					foreach ($query->result() as $row)
					{
						$total_invoiced = 0;
						$visit_date = date('jS M Y',strtotime($row->transaction_date));
						$visit_time = date('H:i a',strtotime($row->visit_time_in));
						if($row->visit_time_out != '0000-00-00 00:00:00')
						{
							$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
						}
						else
						{
							$visit_time_out = '-';
						}
						
						$visit_id = $row->visit_id;
						$patient_id = $row->patient_id;
						$personnel_id = $row->personnel_id;
						$dependant_id = $row->dependant_id;
						$strath_no = $row->strath_no;
						$patient_number = $row->patient_number;
						$patient_table_visit_type = $visit_type_id;
						$visit_invoice_number = $row->visit_invoice_number;
						$visit_invoice_id = $row->visit_invoice_id;
						$branch_code = $row->branch_code;

						
						$visit_type_name = $row->payment_type_name;
						$patient_othernames = $row->patient_othernames;
						$patient_surname = $row->patient_surname;
						$patient_date_of_birth = $row->patient_date_of_birth;

						$doctor = $row->personnel_fname;
						$count++;
						$invoice_total = $row->dr_amount;
						$payments_value = $this->reports_model->get_visit_invoice_payments($visit_invoice_id);
						$balance  = $this->reports_model->balance($payments_value,$invoice_total);

						$total_payable_by_patient += $invoice_total;
						$total_payments += $payments_value;
						$total_balance += $balance;
						$invoice_result .= 
							'
								<tr>
									<td>'.$count.'</td>
									<td>'.$visit_date.'</td>
									<td>'.$patient_number.'</td>
									<td>'.ucwords(strtolower($patient_surname)).'</td>
									<td>'.$visit_type_name.'</td>
									<td>'.$doctor.'</td>
									<td>'.$visit_invoice_number.'</td>
									<td>'.number_format($invoice_total,2).'</td>
									<td>'.(number_format($payments_value,2)).'</td>
									<td>'.(number_format($balance,2)).'</td>
								</tr> 
						';
						
					}

					$invoice_result .= 
							'
								<tr>
									<td colspan=7> Totals</td>
									<td><strong>'.number_format($total_payable_by_patient,2).'</strong></td>
									<td><strong>'.number_format($total_payments,2).'</strong></td>
									<td><strong>'.number_format($total_balance,2).'</strong></td>
								</tr> 
						';
					
					$invoice_result .= 
					'
								  </tbody>
								</table>
					';
				}
				
				else
				{
					$invoice_result .= "There are no visits";
				}
		
	      		?>
	      		<p style="page-break-before: always">
	      		<div class="col-md-12">
	      			 <h4>Invoices</h4>
	      		</div>
	      		<div class="col-md-12">
	      			 <?php echo $invoice_result;?>
	      		</div>

	      	</div>
	      	
	    </div>


    </body>
    
</html>