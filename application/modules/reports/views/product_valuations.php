<div class="row">



<div class="col-md-12">
<?php echo $this->load->view('search/search_product_valuation', '', TRUE);?>
<section class="panel panel-featured panel-featured-info">
		<header class="panel-heading">
			 <h2 class="panel-title"><?php echo $title;?></h2>
			
		</header>             

        <div class="panel-body">
        		   <div class="pull-right">
	         <a href="<?php echo base_url();?>reports/print_movements" class="btn btn-danger btn-sm fa fa-print"> Print Movemnets</a>
	         <a href="<?php echo site_url()?>reports/export_product_valuations" class="btn btn-sm btn-success">Export to Excel</a>
	    </div>
        
        	
         <br>
		<?php
		$result = '';
		$search = $this->session->userdata('stock_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reports/close_stock_search" class="btn btn-sm btn-warning">Close Search</a>';
		}


		if(!empty($query))
		{
			//if users exist display them
			if ($query->num_rows() > 0)
			{
				$count = $page;
				
				$result .= 
					'
						<table class="table  table-bordered table-striped table-responsive col-md-12">
							<thead>
								<tr>
									<th></th>
									<th>PRODUCT CODE</th>
									<th>PRODUCT NAME</th>
									<th>CATEGORY</th>
									<th colspan="5" class="center-align">STOCK MOVEMENTS</th>
								</tr>

								<tr>
									<th colspan="4"></th>
									<th >OPENING STOCK</th>
								  <th >OPENING STOCK</th>
									<th>PURCHASES</th>
									<th>TOTAL STOCK</th>
									<th>SALES</th>
									<th>REMAINING</th>

								</tr>
							</thead>
							<tbody>
					';
					
			
				
				
				foreach ($query->result() as $row => $value)
				{
					$total_invoiced = 0;
					
					$product_code = $value->product_code;
					$product_name = $value->product_name;
					$stock_amount = $value->stock_amount;
					$store_quantity = $value->store_quantity;
					$store_balance = $value->store_balance;
					$category_name = $value->category_name;				
					$store_name = $value->store_name;
					$whole_sale_stock = $value->whole_sale_stock;
					$reorder_level = $value->reorder_level;
					$product_id = $value->product_id;


					$retail_opening_view = $this->reports_model->get_drug_amounts_trail_opening($product_id,5);


					$retail_opening_purchases = $retail_opening_view['total_dr_amount'];
					$retail_opening_sales = $retail_opening_view['total_cr_amount'];
					$retail_opening = $retail_opening_purchases - $retail_opening_sales;


					$wholesale_opening_view = $this->reports_model->get_drug_amounts_trail_opening($product_id,6);

					$wholesale_opening_purchases = $wholesale_opening_view['total_dr_amount'];
					$wholesale_opening_sales = $wholesale_opening_view['total_cr_amount'];
					$wholesale_opening = $wholesale_opening_purchases - $wholesale_opening_sales;

					// var_dump($wholesale_opening);die();


					$retail_view = $this->reports_model->get_drug_amounts_trail($product_id,5);
					$wholesale_view = $this->reports_model->get_drug_amounts_trail($product_id,6);

					$retail_purchases = $retail_view['total_dr_amount'];
					$retail_sales = $retail_view['total_cr_amount'];

					$retail_total_purchases = $retail_view['total_dr_purchases'];

					$wholesale_purchases = $wholesale_view['total_dr_amount'];
					$wholesale_sales = $wholesale_view['total_cr_amount'];

					$wholesale_total_purchases = $wholesale_view['total_dr_purchases'];


					$retail_closing = ($retail_opening + $retail_purchases) - $retail_sales;
					$wholesale_closing = ($wholesale_opening + $wholesale_purchases) - $wholesale_sales;
		
					$total_closing = $retail_closing + $wholesale_closing;
					$total_op = $retail_purchases + $store_quantity;
					$total_sales  = $total_op - $store_balance;

					$total_balance = $total_op - $total_sales;

					$count++;
					$result .= '<tr >
										<td>'.$count.'</td>
										<td>'.strtoupper($product_code).'</td>
										<td>'.$store_name.'</td>
										<td>'.$product_name.'</td>
										<td>'.$category_name.'</td>
										<td>'.$store_quantity.'</td>
										<td>'.$retail_purchases.'</td>
										<td>'.$total_op.'</td>
										<td>'.$total_sales.'</td>
										<td>'.$total_balance.'</td>
									
									</tr>';
					
					
				}
				
				$result .= 
				'
							  </tbody>
							</table>
				';
			}
			
			else
			{
				$result .= "There are no out of stock items";
			}
		}
		else
		{
			$result = "Please search for a product";
		}
		
		
		echo $result;
		?>
    </div>
      
  	<div class="widget-foot">
                        
		<?php if(isset($links)){echo $links;}?>
    
        <div class="clearfix"></div> 
    
    </div>

</section>


  </div>

  <script type="text/javascript">
  	
  	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

  </script>