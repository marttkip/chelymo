<?php echo $this->load->view('search/search_summaries', '', TRUE);?>
<?php
$summary_report_search = $this->session->userdata('summary_report_search');

// var_dump($summary_report_search);die();
if(!empty($summary_report_search))
{
	$patient_add = str_replace('visit.visit_date','DATE(patients.patient_date)',$summary_report_search);
	//AND DATE(visit.visit_time_out) = "'.$date_today.'"
	$visit_add2 = str_replace('visit.visit_date', 'DATE(visit.visit_time_out)',$summary_report_search);
	$visit_add = $summary_report_search;
}
else{
	$date_today = date('Y-m-d');
	$visit_add = ' AND visit.visit_date = "'.$date_today.'"';
	
	$patient_add = ' AND DATE(patients.patient_date) = "'.$date_today.'"';
	$visit_add2 = ' AND DATE(visit.visit_time_out) = "'.$date_today.'"';

}

// var_dump($patient_add);die();

 $community_where ='patients.patient_type = 0 AND patients.patient_delete = 0'.$patient_add;
$community_table = 'patients';
$total_number_new = $this->reception_model->count_items($community_table, $community_where);




$date_today = date('Y-m-d');
$community_where ='visit.visit_delete = 0 AND visit.revisit >= 0 AND patients.patient_delete = 0 AND visit.close_card >= 0 AND visit.patient_id = patients.patient_id  AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND visit.inpatient = 0 '.$visit_add;
$community_table = 'patients,visit,visit_type';
$total_outpatients = $this->reception_model->count_items($community_table, $community_where);


$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 7)  AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0'.$visit_add;
$community_table = 'patients,visit';
$total_inpatients = $this->reception_model->count_items($community_table, $community_where);


$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 7)  AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0';
$community_table = 'patients,visit';
$new_inpatients = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 2)  AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0'.$visit_add2;
$community_table = 'patients,visit';
$discharges = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.revisit <= 1 AND visit_type.visit_type_id = visit.visit_type AND visit.visit_delete = 0  AND patients.patient_delete = 0' .$visit_add;
$community_table = 'patients,visit,visit_type';
$total_newisits = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient >= 0 AND visit.revisit  = 2 AND visit.visit_delete = 0  AND visit_type.visit_type_id = visit.visit_type AND patients.patient_delete = 0'.$visit_add;
$community_table = 'patients,visit,visit_type';
$total_revisits = $this->reception_model->count_items($community_table, $community_where);




?>
<div class="row">
	<!-- <div class="col-md-12"> -->
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-primary">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">1st TIME CLIENT'S AT <?php echo $this->session->userdata('branch_code')?></h5>
								<div class="info">
									<strong class="amount"><?php echo $total_number_new?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-success">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">OUTPATIENTS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_outpatients?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-info">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">ACTIVE INPATIENTS</h5>
								<div class="info">
									<strong class="amount"><?php echo $new_inpatients?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-warning">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">DISCHARGES</h5>
								<div class="info">
									<strong class="amount"><?php echo $discharges?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-danger">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">NEW VISITS VS REVISITS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_newisits.' vs '.$total_revisits?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">

			<section class="card mb-4">
				<div class="card-body bg-default">
					<div class="widget-summary">
						
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title" style="color: #fff">APPOINTMENTS</h5>
								<div class="info">
									<strong class="amount" style="color: #fff"><?php echo '0';?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
			
		</div>
		


	<!-- </div> -->
</div>

<div class="row" style="margin-top:20px;">
	
			<h2></h2>


			<div class="col-md-4">	

				<h5>PATIENTS SEEN/REGISTERED (PER VISIT TYPE) <a href="#" onclick="javascript:xport.toCSV('Patients Department');">EXPORT TO XLS</a></h5>	
				<br>
					<div class="panel-body" style="height:40vh !important;overflow-y: scroll; ">
					<?php
						$date_today = date('Y-m-d');
						$visit_types_rs = $this->reception_model->get_visit_types();
						$visit_results = '';
					
						$total_p = 0;
						$total_o = 0;
						$total_v = 0;
						if($visit_types_rs->num_rows() > 0)
						{
							foreach ($visit_types_rs->result() as $key => $value) {
								# code...

								$visit_type_name = $value->visit_type_name;
								$visit_type_id = $value->visit_type_id;


								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 0 AND patients.patient_delete = 0'.$visit_add;	
								$total_outpatient_patients = $this->reception_model->count_items($table,$where);
							




								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 1 AND patients.patient_delete = 0'.$visit_add;
								$total_inpatient_patients = $this->reception_model->count_items($table,$where);



								$total_visits = $total_outpatient_patients + $total_inpatient_patients;
								$total_o += $total_outpatient_patients;
								$total_p += $total_inpatient_patients;
								$total_v += $total_visits;


								// calculate amounts paid
							
								$visit_results .='<tr>
												  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
												  		<td style="text-align:center;"> '.$total_outpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_inpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_visits.'</td>
												  	</tr>';
								
					


							}

							
						}


					?>


					<table  class="table table-hover table-bordered " id="Patients Department">
						<thead>
							<tr>
								<th style="padding:5px;">VISIT TYPE</th>
								<th style="padding:5px;">OUTPATIENTS</th>
								<th style="padding:5px;">INPATIENTS</th>
								<th style="padding:5px;">TOTAL PATIENTS</th>
								
							</tr>
						</thead>
						</tbody> 
						  	<?php echo $visit_results?>
					  	</tbody>
					  		<thead>
							<tr>
								<th style="padding:5px;">TOTAL</th>
								<th style="padding:5px;"><?php echo $total_o?></th>
								<th style="padding:5px;"><?php echo $total_p?></th>
								<th style="padding:5px;"><?php echo $total_v?></th>
								
							</tr>
						</thead>

					</table>
				</div>


			</div>

			<div class="col-md-4">

				<h5>NEW VISITS AND REVISITS <a href="#" onclick="javascript:xport.toCSV('WALKINS VS CLINIC VISITS');">EXPORT TO XLS</a></h5>	
				<br>
				<div class="panel-body" style="height:40vh !important;overflow-y: scroll; ">
					<?php


                   
                     $this->db->where('report_item = 1 AND department_report = 1');
                    $query = $this->db->get('departments');
                    $result = '';
                    $grand_walkin_number_new = 0;
                    $grand_walkin_number_old = 0;
                    $grand_number_new = 0;
                    $grand_number_old = 0;
                    $grand_number = 0;
                    //if users exist display them
                    if ($query->num_rows() > 0)
                    {
                        $count = $page;
                        
                        $result .= 
                            '
                                <table class="table table-hover table-bordered " id="WALKINS VS CLINIC VISITS">
                                  <thead>
                                   <tr>
                                      <th>#</th>
                                      <th></th>
                                      <th colspan="2">WALKINS</th>
                                      <th colspan="2">CLINIC VISIT</th>
                                     
                                      <th>TOTAL</th>
                                    </tr>
                                  </thead>
                                    <tr>
                                      <th>#</th>
                                      <th>DEPARTMENT</th>
                                      <th>NEW VISITS</th>
                                      <th>REVISITS</th>

                                      <th>NEW VISITS</th>
                                      <th>REVISITS</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                            ';

                        
                        foreach ($query->result() as $row)
                        {
                            
                            $department_name = $row->department_name;
                            $department_id = $row->department_id;
                            
                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.revisit <= 1 AND patients.patient_type = 0'.$visit_add;
                            $community_table = 'visit,patients';
                            $total_number_new = $this->reception_model->count_items($community_table, $community_where);



                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.revisit = 2 AND patients.patient_type = 0'.$visit_add;
                            $community_table = 'visit,patients';
                            $total_number_old = $this->reception_model->count_items($community_table, $community_where);




                            // walkins visits 
                            $sql = 'SELECT COUNT(*) AS total_count from visit where visit_id IN (SELECT visit_id FROM visit_charge,service_charge,service WHERE visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id > 0 AND service_charge.service_id = service.service_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND service.department_id = '.$department_id.') AND visit.revisit <= 1 AND visit.department_id <> '.$department_id.' '.$visit_add;
                            $total_walkin_number_new = $this->reception_model->count_items_by_query($sql);



                             $sql = 'SELECT COUNT(*) AS total_count from visit where visit_id IN (SELECT visit_id FROM visit_charge,service_charge,service WHERE visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.visit_invoice_id > 0 AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND service_charge.service_id = service.service_id AND service.department_id = '.$department_id.') AND visit.revisit = 2 AND visit.department_id <> '.$department_id.' '.$visit_add;
                            $total_walkin_number_old = $this->reception_model->count_items_by_query($sql);
                            

                            $total_walkin = $total_walkin_number_new + $total_walkin_number_old;

                            $total_clinic = $total_number_new + $total_number_old;

                            $total = $total_walkin + $total_clinic;

                            $grand_walkin_number_new += $total_walkin_number_new;
                             $grand_walkin_number_old += $total_walkin_number_old;
                            $grand_number_new += $total_number_new ;
                        	$grand_number_old += $total_number_old;
                        	$grand_number += $total;
                            $count++;
                            $result .= 
                                '
                                    <tr>
                                        <td>'.$count.'</td>
                                        <td>'.strtoupper($department_name).'</td>
                                        <td>'.$total_walkin_number_new.'</td>
                                        <td>'.$total_walkin_number_old.'</td>

                                        <td>'.$total_number_new.'</td>
                                        <td>'.$total_number_old.'</td>
                                        <td>'.$total.'</td>
                                        
                                    </tr> 
                                ';
                        }

                        $result .= 
                                '
                                    <tr>
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th>'.$grand_walkin_number_new.'</th>
                                        <th>'.$grand_walkin_number_old.'</th>
                                        <th>'.$grand_number_new.'</th>
                                        <th>'.$grand_number_old.'</th>
                                        <th>'.$grand_number.'</th>
                                        
                                    </tr> 
                                ';
                        
                        $result .= 
                        '
                                      </tbody>
                                    </table>
                        ';
                    }
                    
                    else
                    {
                        $result .= "There are no departments";
                    }
                    
                    echo $result;
                    ?>
				
					
	            </div>
			</div>
			
			<div class="col-md-4">
				<h5>NEW VISITS AND REVISITS PER DEPARTMENT <a href="#" onclick="javascript:xport.toCSV('testTable');">EXPORT TO XLS</a></h5>	
				<br>
				<div class="panel-body" style="height:40vh !important;overflow-y: scroll; ">
                <?php
                   
                     $this->db->where('report_item = 1');
                    $query = $this->db->get('departments');
                    $result = '';
                    $grand_number_new = 0;
                    $grand_number_old = 0;
                    $grand_number = 0;
                    //if users exist display them
                    if ($query->num_rows() > 0)
                    {
                        $count = $page;
                        
                        $result .= 
                            '
                                <table class="table table-hover table-bordered " id="testTable">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>DEPARTMENT</th>
                                      <th>NEW VISITS</th>
                                      <th>REVISITS</th>
                                      <th>TOTAL</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                            ';

                        
                        foreach ($query->result() as $row)
                        {
                            
                            $department_name = $row->department_name;
                            $department_id = $row->department_id;
                            
                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.revisit <= 1 AND patients.patient_type = 0'.$visit_add;
                            $community_table = 'visit,patients';
                            $total_number_new = $this->reception_model->count_items($community_table, $community_where);

                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.revisit = 2 AND patients.patient_type = 0'.$visit_add;
                            $community_table = 'visit,patients';
                            $total_number_old = $this->reception_model->count_items($community_table, $community_where);
                            $total = $total_number_new + $total_number_old;

                            $grand_number_new += $total_number_new ;
                        	$grand_number_old += $total_number_old;
                        	$grand_number += $total;
                            $count++;
                            $result .= 
                                '
                                    <tr>
                                        <td>'.$count.'</td>
                                        <td>'.strtoupper($department_name).'</td>
                                        <td>'.$total_number_new.'</td>
                                        <td>'.$total_number_old.'</td>
                                        <td>'.$total.'</td>
                                        
                                    </tr> 
                                ';
                        }

                        $result .= 
                                '
                                    <tr>
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th>'.$grand_number_new.'</th>
                                        <th>'.$grand_number_old.'</th>
                                        <th>'.$grand_number.'</th>
                                        
                                    </tr> 
                                ';
                        
                        $result .= 
                        '
                                      </tbody>
                                    </table>
                        ';
                    }
                    
                    else
                    {
                        $result .= "There are no departments";
                    }
                    
                    echo $result;
                    ?>

                    <?php

                    $date_today = date('Y-m-d');
					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 1 AND visit.visit_delete = 0  AND patients.patient_delete = 0'.$visit_add;
					$community_table = 'patients,visit';
					$total_men = $this->reception_model->count_items($community_table, $community_where);


					  $date_today = date('Y-m-d');
					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 2 AND visit.visit_delete = 0  AND patients.patient_delete = 0'.$visit_add;
					$community_table = 'patients,visit';
					$total_female = $this->reception_model->count_items($community_table, $community_where);


					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 0 AND visit.visit_delete = 0  AND patients.patient_delete = 0'.$visit_add;
					$community_table = 'patients,visit';
					$not_indicated = $this->reception_model->count_items($community_table, $community_where);
                    ?>
                    <h5>VISITS PER GENDER</h5>	
        			<br>
                	<table class="table table-hover table-bordered ">
                  
                        <tr>
                          <th>MALE</th>
                          <td><?php echo $total_men?></td>
                          <th>FEMALE</th>
                          <td><?php echo $total_female?></td>
                          <th>NOT INDICATED</th>
                          <td><?php echo $not_indicated?></td>
                        </tr>
                     
                 
                     </table>


                </div><a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
			</div>

			<div class="col-md-8">
			
				<h5>SPECIALTY CLINICS</h5>	
				<br>
				<div class="panel-body" style="height:40vh !important;overflow-y: scroll; overflow-x: scroll; ">
                <?php


                	$this->db->where('report_item = 1');
                    $query = $this->db->get('departments');
                    $top_bar = '';
                    
                    //if users exist display them
                    if ($query->num_rows() > 0)
                    {
                   
                        foreach ($query->result() as $row)
                        {
                            
                            $department_name = $row->department_name;
                            $department_id = $row->department_id;
                     
                            $top_bar .= '<th>'.strtoupper($department_name).'</th>';
                        }
                        
                    }
                   

                   $date_today = date('Y-m-d');
					$visit_types_rs = $this->reception_model->get_visit_types();
					$visit_results = '';
					$grand_total_list = 0;
					if($visit_types_rs->num_rows() > 0)
					{
						foreach ($visit_types_rs->result() as $key => $value) {
							# code...

							$visit_type_name = $value->visit_type_name;
							$visit_type_id = $value->visit_type_id;

							$total_count_list= 0;
							// calculate amounts paid
							 $count_bar = '';
							if ($query->num_rows() > 0)
		                    {
		                   
		                        foreach ($query->result() as $row)
		                        {
		                            
		                            $department_name = $row->department_name;
		                            $department_id = $row->department_id;

		                     	   	$community_where ='visit.department_id = '.$department_id.' AND visit.visit_type = '.$visit_type_id.' AND patients.patient_id = visit.patient_id  AND patients.patient_type = 0'.$visit_add;
		                            $community_table = 'visit,patients';
		                            $total_number_new = $this->reception_model->count_items($community_table, $community_where);
		                            $grand_total_list += $total_number_new;
		                            $total_count_list += $total_number_new;
		                            $count_bar .= '<td>'.$total_number_new.'</td>';
		                        }
		                        
		                    }
                   
						
							$visit_results .='<tr>
											  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
											  		'.$count_bar.'
											  		<th>'.$total_count_list.'</th>
											  	</tr>';
							

						}
						$total_patients = $query->num_rows()+1;
						$visit_results .='<tr>
											  		<td style="text-align:left;" colspan="'.$total_patients.'"> GRAND TOTAL </td>
											  		
											  		<th>'.$grand_total_list.'</th>
											  	</tr>';

						
					}


					$result = 
                            '
                                <table class="table table-hover table-bordered ">
                                  <thead>
                                    <tr>
                                      <th>ACCOUNT</th>
                                      '.$top_bar.'
                                      <th>TOTAL</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                            	';
                            	$result .= $visit_results;

                    $result .= '</tbody>
                                </table>';

                    //  $this->db->where('report_item = 1');
                    // $query = $this->db->get('departments');
                    // $result = '';
                    
                    // //if users exist display them
                    // if ($query->num_rows() > 0)
                    // {
                    //     $count = $page;
                        
                        
                        
                    //     foreach ($query->result() as $row)
                    //     {
                            
                    //         $department_name = $row->department_name;
                    //         $department_id = $row->department_id;
                            
                    //         $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 0 AND patients.patient_type = 0'.$visit_add;
                    //         $community_table = 'visit,patients';
                    //         $total_number_new = $this->reception_model->count_items($community_table, $community_where);

                    //         $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 1 AND patients.patient_type = 0'.$visit_add;
                    //         $community_table = 'visit,patients';
                    //         $total_number_old = $this->reception_model->count_items($community_table, $community_where);
                    //         $total = $total_number_new + $total_number_old;
                    //         $count++;
                    //         $result .= 
                    //             '
                    //                 <tr>
                    //                     <td>'.$count.'</td>
                    //                     <td>'.strtoupper($department_name).'</td>
                    //                     <td>'.$total_number_new.'</td>
                    //                     <td>'.$total_number_old.'</td>
                    //                     <td>'.$total.'</td>
                                        
                    //                 </tr> 
                    //             ';
                    //     }
                        
                    // }
                    
                    // else
                    // {
                    //     $result .= "There are no departments";
                    // }
                    
                    echo $result;
                    ?>

                  
                   <a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>


                </div>
			

</div>
<div class="col-md-4">

	<h5>NEW VISITS AND REVISITS</h5>	
	<br>
	<div class="panel-body" style="height:40vh !important;overflow-y: scroll; ">
		<?php


		$visits_rs = '{
	                        label: "NEW VISITS ",
	                        data: [
	                            [1, '.$total_newisits.']
	                        ],
	                        color: "#0088cc"
	                    }, {
	                        label: "REVISITS",
	                        data: [
	                            [1, '.$total_revisits.']
	                        ],
	                        color: "#734ba9"
	                    }';


		?>
		<!-- Flot: Pie -->
        <div class="chart chart-md" id="flotPie3"></div>
        <script type="text/javascript">
            var flotPieData3 = [<?php echo $visits_rs?>];

            // See: js/examples/examples.charts.js for more settings.
        </script>
    </div>
</div>


<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
					
					
					