       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Search Products Valuation</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
            <?php
            echo form_open("reports/search_product_valuation", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-2">
                    
                    
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label"> Product: </label>
                        
                        <div class="col-md-10">
                            <select id='product_id' name='product_id' class='form-control custom-select'>
                                <option value="0">SELECT A PRODUCT</option>
                                <?php
                                $products_query = $this->products_model->all_products();
                                if($products_query->num_rows() > 0)
                                {
                                    foreach ($products_query->result() as $key ) {
                                        # code...
                                        $product_id = $key->product_id;
                                        $product_name = $key->product_name;

                                        echo '<option value="'.$product_id.'">'.$product_name.'</option>';
                                    }
                                }
                                ?>

                            
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Date From: </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Start Date" >
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Product Category: </label>
                        
                        <div class="col-md-8">
                             <select name="category_id" id="category_id" class="form-control">
                                <?php
                                echo '<option value="0">No Category</option>';
                                $all_categories = $this->categories_model->all_categories();
                                if($all_categories->num_rows() > 0)
                                {
                                    $result = $all_categories->result();
                                    
                                    foreach($result as $res)
                                    {
                                        if($res->category_id == set_value('category_id'))
                                        {
                                            echo '<option value="'.$res->category_id.'" selected>'.$res->category_name.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$res->category_id.'">'.$res->category_name.'</option>';
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Date To: </label>
                        
                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="End Date" >
                            </div>
                        </div>
                    </div>
                </div>

                                <div class="col-md-2">

                    <div class="form-group">
                                <label class="col-md-5 control-label">Store Name: </label>
                                
                                <div class="col-md-7">
                                     <select name="store_id" id="store_id" class="form-control custom-select">
                                        <?php
                                        $all_stores = $this->stores_model->all_stores_d();
                                        echo '<option value="0">No Store</option>';
                                        if($all_stores->num_rows() > 0)
                                        {
                                            $result = $all_stores->result();
                                            
                                            foreach($result as $res)
                                            {
                                                if($res->store_id == set_value('store_id'))
                                                {
                                                    echo '<option value="'.$res->store_id.'" selected>'.$res->store_name.'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option value="'.$res->store_id.'">'.$res->store_name.'</option>';
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                 <div class="col-md-1">
                    <div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search </button>
                        </div>
                     </div>
                 </div>
            </div>
            
            
            
            <?php
            echo form_close();
            ?>
          </div>
        </section>
<script type="text/javascript">
    $(function() {
        $("#product_id").customselect();
        // $("#leave_type_id").customselect();
      });
</script>