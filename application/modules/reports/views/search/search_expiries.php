       
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<!-- <h2 class="panel-title pull-right">Sick Search:</h2> -->
            	<h2 class="panel-title">Search Products</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("reports/search_expiries", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-4">
                    
                    
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label"> Store: </label>
                        
                        <div class="col-md-8">
                        	<select id='store_id' name='store_id' class='form-control ' required="required">
                                <option value=''>None - Please Select a Store</option>
                                <option value='5'>Main Store</option>
                                <option value='6'>OPD Store</option>
                                <option value='9'>IPD Store</option>
                                <option value='10'>Catering Store</option>
                                <option value='11'>Non Pharmaceutical</option>
                                <option value='12'>Theatre Store</option>
                                <option value='13'>Nurse Store</option>
                                <option value='14'>Laboratory Store</option>
                                <option value='15'>Optical Store</option>
                                <option value='16'>House Keeping Store</option>
                                <option value='17'>Dental Store</option>
                                <option value='18'>Ultrasound Sound</option>
                                <option value='19'>Biomed Store</option>
                                <option value='20'>Front Office</option>
                                <option value='21'>Icu Store</option>

                           	
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label"> Product Name: </label>
                        
                        <div class="col-md-8">
                        	<input type="text" class="form-control" name="product_name" placeholder="Product Name" autocomplete="off">
                        </div>
                    </div>
                    
                </div>
                 <div class="col-md-4">
                 	<div class="form-group">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search Report</button>
                        </div>
              		 </div>
                 </div>
            </div>
            
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>
<script type="text/javascript">
	$(function() {
	    $("#department_name").customselect();
	    $("#leave_type_id").customselect();
	  });
</script>