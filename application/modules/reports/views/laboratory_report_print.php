<?php


		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
	
				
		$search = $this->session->userdata('test_report_search');
	
		
		$result = '';
		
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			
				$result .= 
							'
								<table class="table table-hover table-bordered ">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Procedure Name</th>
									  <th>Procedure Count</th>
									  <th>Revenue</th>
									</tr>
								  </thead>
								  <tbody>
							';
			
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			$total_amount = 0;
			foreach ($query->result() as $row)
			{

				$service_charge_name = $row->service_charge_name;
				$total_count = $row->total_count;
				$total_revenue = $row->total_revenue;
				$service_charge_amount = $row->service_charge_amount;
				$service_charge_id = $row->service_charge_id;
				$service_name = $row->service_name;
				if(empty($service_charge_amount))
				{
					$service_charge_amount = 0;
				}
				
				
				$count++;
				
			
				$total_amount += $total_revenue;
				
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							
							<td>'.$service_charge_name.' </td>
							<td>'.$total_count.' </td>
							<td>'.number_format($total_revenue,2).' </td>

						</tr> 
					';

				
			}
			$result .= 
						'
							<tr>
								<th></th>
								<th></th>
								<th>TOTAL</th>
								<th>'.number_format($total_amount,2).' </th>
						

							</tr> 
						';
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no tests";
		}
		$tests_title_search = $this->session->userdata('tests_title_search');
		
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Dental Report</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        
		<div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong>LABBORATORY REPORT <?php echo strtoupper($tests_title_search)?></strong>
            </div>
        </div>
        <div class="row receipt_bottom_border" style="margin-bottom: 10px;">
        	<div class="col-md-12">
        		<?php echo $result;?>
        	</div>
        </div>
		<!-- <a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">XLS</a> -->
    </body>
    
</html>