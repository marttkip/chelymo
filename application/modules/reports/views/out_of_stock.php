<div class="row">
<?php echo $this->load->view('search/search_out_of_stock', '', TRUE);?>
<?php
$search_title = $this->session->userdata('out_of_stock_title_search');
if(!empty($search_title))
{
	$title_ext = $search_title;
}
else
{
	$title_ext = ' Report for out of stock items ';
}

?>

    <!-- <div class="col-md-12"> -->

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            	 <!-- <a href="<?php echo site_url();?>print-sick-off" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print Sick Off List</a> -->
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $title_ext;?></h5>
          <br>
<?php
		$result = '';
		$search = $this->session->userdata('out_of_stock_report_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reports/close_out_of_stock_items" class="btn btn-sm btn-warning">Close Search</a>';
		}


		if(!empty($query))
		{
			//if users exist display them
			if ($query->num_rows() > 0)
			{
				$count = $page;
				
				$result .= 
					'
						<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
							<thead>
								<tr>
									<th></th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>Store</th>
									<th>Reorder Level</th>
									<th>Current Stock</th>
								</tr>
							</thead>
							<tbody>
					';
					
			
				
				
				foreach ($query->result() as $row => $value)
				{
					$total_invoiced = 0;
					
					$product_code = $value->product_code;
					$product_name = $value->product_name;
					$stock_amount = $value->stock_amount;
					$whole_sale_stock = $value->whole_sale_stock;
					$reorder_level = $value->reorder_level;
					$product_id = $value->product_id;
					$store_id = $value->store_id;
					$store_name = $value->store_name;

					if($store_id == 6)
					{

						$quantity = $whole_sale_stock;
					}
					else
					{
						$quantity = $stock_amount;
					}



					$count++;
					$result .= '<tr onclick="view_drug_trail_options('.$product_id.','.$store_id.')">
										<td>'.$count.'</td>
										<td>'.strtoupper($product_code).'</td>
										<td>'.$product_name.'</td>
										<td>'.$store_name.'</td>
										<td>'.$reorder_level.'</td>
										<td>'.$quantity.'</td>
									</tr>';
					
					
				}
				
				$result .= 
				'
							  </tbody>
							</table>
				';
			}
			
			else
			{
				$result .= "There are no out of stock items";
			}
		}
		else
		{
			$result = "Please search for a product";
		}
		
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    <!-- </div> -->


  </div>

  <script type="text/javascript">
  	
  	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

  </script>