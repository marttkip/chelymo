<?php

class Auth_model extends CI_Model 
{
	/*
	*	Validate a personnel's login request
	*
	*/
	public function validate_personnel()
	{
		//select the personnel by username from the database
		$this->db->select('*');
		$this->db->where(
			array(
				'personnel_username' => $this->input->post('personnel_username'), 
				'personnel_status' => 1, 
				'personnel_password' => md5($this->input->post('personnel_password'))
			)
		);
		$this->db->join('branch', 'branch.branch_id = personnel.branch_id');
		$query = $this->db->get('personnel');
		
		//if personnel exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();

			// get an active branch

			//$branch_details = $this->get_active_branch();
			$personnel_id = $result[0]->personnel_id;
			// $department_id = $this->reception_model->get_personnel_department($personnel_id);
			$this->db->where('job_title.job_title_id = personnel_job.job_title_id AND personnel_job.personnel_id = '.$personnel_id);
			$query=$this->db->get('personnel_job,job_title');

			$department_id = 0;
			$job_title_name = '';
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					$department_id = $value->department_id;
					$job_title_name = $value->job_title_name;
				}
			}
			else
			{
				$department_id = 0;
				$job_title_name = '';
			}


			$this->db->where('personnel_id = '.$personnel_id);
			$this->db->order_by('shift_id','DESC');
			$this->db->limit(1);
			$query_old=$this->db->get('personnel_shift');

			$shift_type = 0;
			if($query_old->num_rows() > 0)
			{

				foreach ($query_old->result() as $key => $value) {
					$shift_type = $value->shift_type;
				}

			}
			else
			{
				$shift_type = 0;
			}
			//create personnel's login session
			$newdata = array(
                   'login_status'     			=> TRUE,
                   'first_name'     			=> $result[0]->personnel_fname,
                   'username'     				=> $result[0]->personnel_username,
                   'personnel_id'  				=> $result[0]->personnel_id,
                   'branch_id'  				=> $result[0]->branch_id,
                   'personnel_department_id'  			=> $department_id,
                   'branch_code'  				=> $result[0]->branch_code,
                   'branch_name'  				=> $result[0]->branch_name,
				   'authorize_invoice_changes'	=> $result[0]->authorize_invoice_changes,
                   'personnel_type_id'     		=> $result[0]->personnel_type_id,
                   'shift_type'     		=> $shift_type,
                   'personnel_role' =>$job_title_name
               );

			// var_dump($newdata); die();

			$this->session->set_userdata($newdata);
			
			//update personnel's last login date time
			$this->update_personnel_login($result[0]->personnel_id);
			return TRUE;
		}
		
		//if personnel doesn't exist
		else
		{
			return FALSE;
		}
	}

	public function get_active_branch()
	{
		$this->db->where('branch_status = 1');
		$this->db->from('branch');
		$query = $this->db->get();
		
		$result = $query->row();

		return $result;
	}
	
	/*
	*	Update personnel's last login date
	*
	*/
	private function update_personnel_login($personnel_id)
	{
		$data['last_login'] = date('Y-m-d H:i:s');
		$this->db->where('personnel_id', $personnel_id);
		if($this->db->update('personnel', $data))
		{
			$session_log_insert = array(
				"personnel_id" => $personnel_id, 
				"session_name_id" => 1
			);
			$table = "session";
			if($this->db->insert($table, $session_log_insert))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
		
		else
		{
			return FALSE;
		}
	}
	
	/*
	*	Reset a personnel's password
	*
	*/
	public function reset_password($personnel_id)
	{
		$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);
		
		$data['personnel_password'] = md5($new_password);
		$this->db->where('personnel_id', $personnel_id);
		$this->db->update('personnel', $data); 
		
		return $new_password;
	}
	
	/*
	*	Check if a has logged in
	*
	*/
	public function check_login()
	{
		$personnel_type_id = $this->session->userdata('personnel_type_id');
		
		if($this->session->userdata('login_status'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function get_personnel_roles($personnel_id)
	{
	}

	public function get_personnel_department($personnel_id)
	{
		$this->db->where('personnel_id = '.$personnel_id);
		$query=$this->db->get('personnel_job');

		$department_id = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$department_id = $value->department_id;
			}
		}
		else
		{
			$department_id = 0;
		}

		return $department_id;

	}
}
?>