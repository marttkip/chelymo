<?php

$rs2 = $this->nurse_model->get_visit_procedure_billed_list_item($visit_id,$visit_charge_id);


                     
$total= 0;  
if($rs2->num_rows() >0){
	foreach ($rs2->result() as $key1):
		$v_procedure_id = $key1->visit_charge_id;
		$procedure_id = $key1->service_charge_id;
		$visit_charge_amount = $key1->visit_charge_amount;
		$units = $key1->visit_charge_units;
		$procedure_name = $key1->service_charge_name;
		$service_id = $key1->service_id;
		$visit_type_id = $key1->visit_type_id;
		$date = $key1->date;
		$plan_status = $key1->plan_status;
		$visit_charge_notes = $key1->visit_charge_notes;
		$surface = $key1->surface_id;
		$teeth = $key1->teeth_id;
		$dentine_id = $key1->dentine_id;
		$visit_invoice_id = $key1->visit_invoice_id;
		$name = $key1->personnel_fname;
		if($visit_invoice_id > 0)
		{
			$marked = 'readonly';
		}
		else
		{
			$marked = '';
		}
		if($visit_type_id == 1)
		{
			$visit = 'SELF';
		}
		else
		{
			$visit = 'INSURANCE';
		}
	
		$total= $total +($units * $visit_charge_amount);
		$checked="";
		$personnel_check = TRUE;
		if($personnel_check)
		{
			$checked = "<td>
						<a class='btn btn-xs btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
						</td>
						<td>
						<a class='btn btn-xs btn-danger'  onclick='change_payer(".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-recycle'></i></a>
						</td>
						<td>
							<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
						</td>";
		}
		
		
		endforeach;

}
$rs_pa = $this->dental_model->get_payment_info($visit_id);

if(count($rs_pa) >0){
	foreach ($rs_pa as $r2):
		# code...
		$payment_info = $r2->payment_info;
		$visit_date = $rs2->visit_date;

		// get the visit charge

	endforeach;
}

$notes = '';
$rs2 = $this->dental_model->get_visit_procedure_notes($visit_id,$visit_charge_id);
if($rs2->num_rows() >0){
	foreach ($rs2->result() as $key2):
		$notes = $key2->notes;
	endforeach;
}
?>

<div class="row" style="margin-top: 20px;">
	<section class="panel">
	    <!-- <header class="panel-heading">
	        <h5 class="pull-left"><i class="icon-reorder"></i>Procedure</h5>         
	    </header> -->
	    <div class="panel-body">
	        <div class="padd">
	        	<?php
	        	///echo form_open("finance/creditors/confirm_invoice_note/".$visit_id."/".$visit_charge_id, array("class" => "form-horizontal","id" => "submit-charges-old"));
	        	?>
	          	<div class="row">
		            <div class="col-md-6">

		            	<div class="form-group">
		                    <label class="col-md-4 control-label">Description: </label>
		                    
		                    <div class="col-md-8">
		                       <?php echo $procedure_name;?>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Units: </label>
		                    
		                    <div class="col-md-8">
		                       <input type='text' name="units<?php echo $v_procedure_id;?>" class="form-control-two" value="<?php echo $units?>" id="units<?php echo $v_procedure_id;?>"  size='3' <?php echo $marked?>/>
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-4 control-label">Units Amount: </label>
		                    
		                    <div class="col-md-8">
		                    	<input type='text' class='form-control-two' name="billed_amount" value='<?php echo $visit_charge_amount?>' id="billed_amount<?php echo $v_procedure_id;?>" <?php echo $marked?> />
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Teeth: </label>
		                    
		                    <div class="col-md-8">
		                    	<?php echo $teeth?>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Surface: </label>
		                    
		                    <div class="col-md-8">
		                    	<?php echo $surface?>
		                    </div>
		                </div>
		            </div>
		            <input type="hidden" name="dentine_id" id="dentine_id<?php echo $v_procedure_id;?>" value="<?php echo $dentine_id;?>">
		            <input type="hidden" name="teeth_id" id="teeth_id<?php echo $v_procedure_id;?>" value="<?php echo $teeth_id;?>">
		            <input type="hidden" name="patient_id" id="patient_id<?php echo $v_procedure_id;?>" value="<?php echo $patient_id;?>">
		            <div class="col-md-6">	
						<h4>Plans</h4>
						<div class="form-group left-align">
			                <div class="radio">

			                	<?php
			                	if($plan_status == 1)
			                	{
			                		?>
			                		<label>
				                        <input id="optionsRadios2" type="radio" name="work_status" checked  id="work_status"  value="1">
				                         Treatment Plan
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"  id="work_status"  value="2">
				                         Completed
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"  id="work_status"  value="3">
				                         Exam
				                    </label>
				                    <br>
			                		<?php

			                	}
			                	else if($plan_status == 2)
			                	{
			                		?>

			                		<label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="1">
				                         Treatment Plan
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status" checked  id="work_status"  value="2">
				                         Completed
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="3">
				                         Exam
				                    </label>
				                    <br>

			                		<?php
			                	}else if($plan_status == 3)
			                	{
			                		?>

			                		<label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="1">
				                         Treatment Plan
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status"   id="work_status"  value="2">
				                         Completed
				                    </label>
				                    <label>
				                        <input id="optionsRadios2" type="radio" name="work_status" checked  id="work_status"  value="3">
				                         Exam
				                    </label>
				                    <br>

			                		<?php
			                	}
			                	?>
			                	
			                	
			                    
			                </div>
			            </div>
			            <div class="form-group">
			            	<h4>Previous procedure notes</h4>
			            	<div class="col-md-12">
			            		<table class="table table-condensed table-hover">
			            			<thead>
			            				<tr>
			            					<td>Date</td>
			            					<td>Notes</td>
			            					<td>Doctor</td>
			            				</tr>
			            			</thead>
			            			
			            		
				            		<?php
				            		if(!empty($visit_charge_notes))
									{
										echo"
												<tr> 
													<td>".date('jS M Y',strtotime($date))."</td>
													<td>".$visit_charge_notes."</td>
													<td >".$name."</td>
												</tr>	
											";

									}

									$procedure_rs = $this->dental_model->get_visit_procedure_notes(NULL,$v_procedure_id,NULL,$visit_date);

									if($procedure_rs->num_rows() > 0)
									{
										foreach ($procedure_rs->result() as $key => $value) {
											# code...
											$notes = $value->notes;
											$visit_date = $value->visit_date;
											$personnel_fname = $value->personnel_fname;

											echo"
												<tr> 
													<td>".date('jS M Y',strtotime($visit_date))."</td>
													<td>".$notes."</td>
													<td >".$personnel_fname."</td>
												</tr>	
											";
										}
									}
				            		?>
				            	</table>
			            	</div>
			            </div>             	
		                <div class="form-group">
		                   <div class="col-md-12">
		                      <textarea  name="notes<?php echo $v_procedure_id;?>" id="notes<?php echo $v_procedure_id;?>"  class="form-control cleditor" required="required"><?php echo $notes?></textarea>
		                    </div>
		                </div>
		            </div>
	          	</div>
	          	<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
		        <input type="hidden" name="visit_charge_id" id="visit_charge_id" value="<?php echo $visit_charge_id?>">
	          	<div class="row" style="margin-top: 10px;">
		            <div class="col-md-6">		           		
		            	<button  class="btn btn-sm btn-success pull-right" onclick="update_visit_charges(<?php echo $visit_id;?>,<?php echo $visit_charge_id;?>)"> 
		            		submit update 
		            	</button>
		            </div>
		            <div class="col-md-6 ">	
		            	<?php
		            	if($plan_status == 1)
		            	{		            	
			            	?>	           		
				            	<button  class="btn btn-sm btn-danger pull-right" onclick="delete_procedure(<?php echo $visit_charge_id;?>,<?php echo $visit_id;?>,<?php echo $dentine_id?>)"> 
				            		 Delete
				            	</button>
			            	<?php
		                 }
		            	?>
		            </div>
		        </div>
		        <?php //echo form_close();?>
	        </div>
	    </div>
	</section>
</div>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>			        
		        </div>
		    </div>
			
		</li>
	</ul>
</div>