<?php

	$patient_health_form = $this->dental_model->get_patient_history_forms(3,0,7);

	$health =  "<table class='table table-condensed table-striped table-bordered table-hover'> ";
	$count_disease = 0;

	if($patient_health_form->num_rows() > 0)
	{
		$patient_health_rs = $patient_health_form->result();
		
		foreach($patient_health_rs as $dis)
		{	
			$fd_id = $dis->patient_history_id;
			$disease = $dis->patient_history_name;
			
			

			$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

			if($query_result->num_rows() > 0)
			{
				$patient_history_result = $query_result->result();
				foreach ($patient_history_result as $key) {
					# code...
					$patient_history_status = $key->patient_history_status;
				}
			}
			else
			{
				$patient_history_status = 0;
			}

			if($patient_history_status == 1)
			{
				$checked = "checked='checked'";
				$checked_value = 0;
			}
			else
			{
				$checked = '';
				$checked_value = 1;
			}
							
			$health =  $health."<tr>
										<td style='width:90%' >".$disease." </td>
										<td style='width:10%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
										</td>  
								</tr>";
			
			$count_disease++;
		}
	}
	$health .= "</table>
				";



$patient_history_form2 = $this->dental_model->get_patient_history_forms(4,0,7);

$history2 =  "<table class='table table-condensed table-striped table-bordered table-hover'> ";

$history2 .= "<tr>
				<td colspan='8'><h5 class='left-align'>Profile</h5></td>
			</tr>";

$patient_history_form3 = $this->dental_model->get_patient_history_forms(12,0,7);


// var_dump($patient_history_form3); die();
$count_disease = 0;

if($patient_history_form3->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form3->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;


				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				else if($patient_history_status == 1 AND $item_id == 4)
				{
					$checked_four = "checked='checked'";
					$checked_value_four =4;
				}
				else if($patient_history_status == 1 AND $item_id == 5)
				{
					$checked_five = "checked='checked'";
					$checked_value_five = 5;
				}
			}
		}
		

		// echo $checked_three;
		
						
		$history2 =  $history2."<tr>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> Convex
									</td>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> Concave
									</td>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> Straight
									</td> 
							</tr>";
		
		$count_disease++;
	}
}
$history2 .= "</table>
			";
$history2 .=  "<table class='table table-condensed table-striped table-bordered table-hover'> ";
$history2 .= "<tr>
				<td colspan='10'><h4 class='center-align'></h4></td>
			</tr>";

// var_dump($patient_history_form2); die();
$count_disease = 0;

if($patient_history_form2->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form2->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				
			}
		}
		
						
		$history2 =  $history2."<tr>
									<td style='width:40%' >".$disease." </td>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> YES
									</td>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> NO
									</td>
							 
							</tr>";
		
		$count_disease++;
	}
}








$patient_history_form5 = $this->dental_model->get_patient_history_forms(7,0,7);


$count_disease = 0;

if($patient_history_form5->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form5->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
			}
		}
				
		$history2 =  $history2."<tr>
									<td style='width:40%' >".$disease." </td>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> YES
									</td>
									<td style='width:20%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> NO
									</td>
									<td style='width:20%'> 
									</td>  
							</tr>";
		
		$count_disease++;
	}
}




$history2 .= "</table>
			";

$patient_health_form7 = $this->dental_model->get_patient_history_forms(9,0,7);

	$health7 =  "<table class='table table-condensed table-striped table-bordered table-hover'> ";
	$count_disease = 0;

	if($patient_health_form7->num_rows() > 0)
	{
		$patient_health_rs = $patient_health_form7->result();
		$checked_one = '';
		$checked_two = '';
		$checked_three = '';
		$checked_four = '';
		$checked_five = '';

		$checked_value_one = 1;
		$checked_value_two = 2;
		$checked_value_three = 3;
		$checked_value_four = 4;
		$checked_value_five = 5;
		foreach($patient_health_rs as $dis)
		{	
			$fd_id = $dis->patient_history_id;
			$disease = $dis->patient_history_name;
			
			

			$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

			if($query_result->num_rows() > 0)
			{
				$patient_history_result = $query_result->result();
				foreach ($patient_history_result as $key) {
					# code...
					$patient_history_status = $key->patient_history_status;
					$item_id = $key->item_id;

					if($patient_history_status == 1 AND $item_id == 1)
					{
						$checked_one = "checked='checked'";
						$checked_value_one = 1;
					}
					else if($patient_history_status == 1 AND $item_id == 2)
					{
						$checked_two = "checked='checked'";
						$checked_value_two = 2;
					}
					else if($patient_history_status == 1 AND $item_id == 3)
					{
						$checked_three = "checked='checked'";
						$checked_value_three = 3;
					}
					else if($patient_history_status == 1 AND $item_id == 4)
					{
						$checked_four = "checked='checked'";
						$checked_value_four =4;
					}
				}
			}
			
							
			$health7 =  $health7."<tr>
										<td style='width:20%' >".$disease." </td>
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> Class I
										</td>  
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'>
											Class II
										</td>  
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'>
											Class III
										</td>  
										<td style='width:20%'> 

										</td>   
								</tr>";
			
			$count_disease++;
		}
	}


	$patient_health_form8 = $this->dental_model->get_patient_history_forms(10,0,7);

	$count_disease = 0;

	if($patient_health_form8->num_rows() > 0)
	{
		$patient_health_rs = $patient_health_form8->result();
		$checked_one = '';
		$checked_two = '';
		$checked_three = '';
		$checked_four = '';
		$checked_five = '';

		$checked_value_one = 1;
		$checked_value_two = 2;
		$checked_value_three = 3;
		$checked_value_four = 4;
		$checked_value_five = 5;
		foreach($patient_health_rs as $dis)
		{	
			$fd_id = $dis->patient_history_id;
			$disease = $dis->patient_history_name;
			
			

			$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

			if($query_result->num_rows() > 0)
			{
				$patient_history_result = $query_result->result();
				foreach ($patient_history_result as $key) {
					# code...
					$patient_history_status = $key->patient_history_status;
					$item_id = $key->item_id;

					if($patient_history_status == 1 AND $item_id == 1)
					{
						$checked_one = "checked='checked'";
						$checked_value_one = 1;
					}
					else if($patient_history_status == 1 AND $item_id == 2)
					{
						$checked_two = "checked='checked'";
						$checked_value_two = 2;
					}
					else if($patient_history_status == 1 AND $item_id == 3)
					{
						$checked_three = "checked='checked'";
						$checked_value_three = 3;
					}
					else if($patient_history_status == 1 AND $item_id == 4)
					{
						$checked_four = "checked='checked'";
						$checked_value_four =4;
					}
				}
			}
			
							
			$health7 =  $health7."<tr>
										<td style='width:20%' >".$disease." </td>
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> Pain
										</td>  
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'>
											Popping
										</td>  
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'>
											Deviation
										</td>  
										<td style='width:20%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'>
											Tooth Wear
										</td>   
								</tr>";
			
			$count_disease++;
		}
	}
	$health7 .= "</table>
				";




$v_data['query'] = $this->dental_model->get_dental_notes(1, $visit_id);
$mobile_personnel_id = $this->session->userdata('personnel_id');
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['visit_id'] = $visit_id;

$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);
$result = '';
$result .= '<div class="row">
      	<div class="col-md-12">
  			<div class="form-group">
   				<label class="col-lg-12 control-label">DETAILS</label>
	    		<div class="col-lg-12">
	      				<textarea id="doctors_oral_health_notes" rows="5" cols="50"  class="form-control col-md-12 cleditor" > </textarea>
	      		</div>
	      	</div>
	    </div>

	  </div>';
$result .=  '
  <br>
	<div class="row">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="center-align">
                      <a hred="#" class="btn btn-sm btn-info" onclick="save_oral_health_notes('.$visit_id.','.$patient_id.',1)">Save Note</a>
                     
                  </div>
            </div>
        </div>
    </div>';


?>
	<div class="row">
	<div class="col-md-5">
		<h3>EXTRA ORAL EXAM</h3>	
			<div class="padd" style="margin-top: 5px;">
				<div class="col-md-12">
					<?php echo $history2;?>

				</div>
				
			</div>
		<
	</div>
	<div class="col-md-7">
	<div class="row">
					<div id="assessment_section"></div>
				</div>
				<div class="row">
					<?php
					 echo $result;
					?>
				</div>
</div>
</div>

<?php
$patient_history_form9 = $this->dental_model->get_patient_history_forms(11,0,6);

$dental_oral =  "<table class='table table-condensed table-striped table-bordered table-hover'> ";
$count_disease = 0;

if($patient_history_form9->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form9->result();
	
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
			}
		}
		else
		{
			$patient_history_status = 0;
		}

		if($patient_history_status == 1)
		{
			$checked = "checked='checked'";
			$checked_value = 0;
		}
		else
		{
			$checked = '';
			$checked_value = 1;
		}
						
		$dental_oral =  $dental_oral."<tr>
											<td style='width:10%'> 
												<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
											</td>  											
											<td style='width:90%' >".$disease." </td>
									</tr>";
		
		$count_disease++;
	}
}
$dental_oral .= "</table>
			";

$patient_history_form10 = $this->dental_model->get_patient_history_forms(11,6,6);

$dental_oral2 =  "<table class='table table-condensed table-striped table-bordered table-hover'> ";
$count_disease = 0;

if($patient_history_form10->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form10->result();
	
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
			}
		}
		else
		{
			$patient_history_status = 0;
		}

		if($patient_history_status == 1)
		{
			$checked = "checked='checked'";
			$checked_value = 0;
		}
		else
		{
			$checked = '';
			$checked_value = 1;
		}
						
		$dental_oral2 =  $dental_oral2."<tr>
											<td style='width:10%'> 
												<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
											</td>  											
											<td style='width:90%' >".$disease." </td>
									</tr>";
		
		$count_disease++;
	}
}
$dental_oral2 .= "</table>
			";
?>


