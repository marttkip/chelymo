<?php
$patient_history_form = $this->dental_model->get_patient_history_forms(1,0,7);

$history =  "<table class='table table-condensed table-striped table-hover'> ";
$count_disease = 0;

if($patient_history_form->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form->result();
	
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
			}
		}
		else
		{
			$patient_history_status = 0;
		}

		if($patient_history_status == 1)
		{
			$checked = "checked='checked'";
			$checked_value = 0;
		}
		else
		{
			$checked = '';
			$checked_value = 1;
		}
						
		$history =  $history."<tr>
									<td style='width:90%' >".$disease." </td>
									<td style='width:10%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
									</td>  
							</tr>";
		
		$count_disease++;
	}
}
$history .= "</table>
			";


// history two 

$patient_history_form2 = $this->dental_model->get_patient_history_forms(1,7,7);

$history2 =  "<table class='table table-condensed table-striped table-hover'> ";
$count_disease = 0;

if($patient_history_form2->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form2->result();
	
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
			}
		}
		else
		{
			$patient_history_status = 0;
		}

		if($patient_history_status == 1)
		{
			$checked = "checked='checked'";
			$checked_value = 0;
		}
		else
		{
			$checked = '';
			$checked_value = 1;
		}
						
		$history2 =  $history2."<tr>
									<td style='width:90%' >".$disease." </td>
									<td style='width:10%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
									</td>  
							</tr>";
		
		$count_disease++;
	}
}
$history2 .= "</table>
			";


$patient_history_form3 = $this->dental_model->get_patient_history_forms(1,14,7);

$history3 =  "<table class='table table-condensed table-striped table-hover'> ";
$count_disease = 0;

if($patient_history_form3->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form3->result();
	
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
			}
		}
		else
		{
			$patient_history_status = 0;
		}

		if($patient_history_status == 1)
		{
			$checked = "checked='checked'";
			$checked_value = 0;
		}
		else
		{
			$checked = '';
			$checked_value = 1;
		}
						
		$history3 =  $history3."<tr>
									<td style='width:90%' >".$disease." </td>
									<td style='width:10%'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
									</td>  
							</tr>";
		
		$count_disease++;
	}
}
$history3 .= "</table>
			";

?>
<div class="row">
	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Dental History (Please check the box for YES)</h2>
			</header>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="col-md-4">
						<?php echo $history;?>
					</div>
					<div class="col-md-4">
						<?php echo $history2;?>
					</div>
					<div class="col-md-4">
						<?php echo $history3;?>
					</div>
				</div>
			</div>
		</section>
		<?php
		$patient_health_form = $this->dental_model->get_patient_history_forms(2,0,9);

		$health =  "<table class='table table-condensed table-striped table-hover'> ";
		$count_disease = 0;

		if($patient_health_form->num_rows() > 0)
		{
			$patient_health_rs = $patient_health_form->result();
			
			foreach($patient_health_rs as $dis)
			{	
				$fd_id = $dis->patient_history_id;
				$disease = $dis->patient_history_name;
				
				

				$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

				if($query_result->num_rows() > 0)
				{
					$patient_history_result = $query_result->result();
					foreach ($patient_history_result as $key) {
						# code...
						$patient_history_status = $key->patient_history_status;
					}
				}
				else
				{
					$patient_history_status = 0;
				}

				if($patient_history_status == 1)
				{
					$checked = "checked='checked'";
					$checked_value = 0;
				}
				else
				{
					$checked = '';
					$checked_value = 1;
				}
								
				$health =  $health."<tr>
											<td style='width:90%' >".$disease." </td>
											<td style='width:10%'> 
												<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
											</td>  
									</tr>";
				
				$count_disease++;
			}
		}
		$health .= "</table>
					";


		// history two 

		$patient_health_form2 = $this->dental_model->get_patient_history_forms(2,9,9);

		$health2 =  "<table class='table table-condensed table-striped table-hover'> ";
		$count_disease = 0;

		if($patient_health_form2->num_rows() > 0)
		{
			$patient_health_rs = $patient_health_form2->result();
			
			foreach($patient_health_rs as $dis)
			{	
				$fd_id = $dis->patient_history_id;
				$disease = $dis->patient_history_name;
				
				

				$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

				if($query_result->num_rows() > 0)
				{
					$patient_history_result = $query_result->result();
					foreach ($patient_history_result as $key) {
						# code...
						$patient_history_status = $key->patient_history_status;
					}
				}
				else
				{
					$patient_history_status = 0;
				}

				if($patient_history_status == 1)
				{
					$checked = "checked='checked'";
					$checked_value = 0;
				}
				else
				{
					$checked = '';
					$checked_value = 1;
				}
								
				$health2 =  $health2."<tr>
											<td style='width:90%' >".$disease." </td>
											<td style='width:10%'> 
												<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
											</td>  
									</tr>";
				
				$count_disease++;
			}
		}
		$health2 .= "</table>
					";


		$patient_health_form3 = $this->dental_model->get_patient_history_forms(2,18,9);

		$health3 =  "<table class='table table-condensed table-striped table-hover'> ";
		$count_disease = 0;

		if($patient_health_form3->num_rows() > 0)
		{
			$patient_health_rs = $patient_health_form3->result();
			
			foreach($patient_health_rs as $dis)
			{	
				$fd_id = $dis->patient_history_id;
				$disease = $dis->patient_history_name;
				
				

				$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

				if($query_result->num_rows() > 0)
				{
					$patient_history_result = $query_result->result();
					foreach ($patient_history_result as $key) {
						# code...
						$patient_history_status = $key->patient_history_status;
					}
				}
				else
				{
					$patient_history_status = 0;
				}

				if($patient_history_status == 1)
				{
					$checked = "checked='checked'";
					$checked_value = 0;
				}
				else
				{
					$checked = '';
					$checked_value = 1;
				}
								
				$health3 =  $health3."<tr>
											<td style='width:90%' >".$disease." </td>
											<td style='width:10%'> 
												<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
											</td>  
									</tr>";
				
				$count_disease++;
			}
		}
		$health3 .= "</table>
					";

		$v_data['query'] = $this->dental_model->get_dental_notes(2, $visit_id);
		$mobile_personnel_id = $this->session->userdata('personnel_id');
		if(!isset($mobile_personnel_id))
		{
			$mobile_personnel_id = NULL;
		}
		$v_data['mobile_personnel_id'] = $mobile_personnel_id;
		$v_data['visit_id'] = $visit_id;

		$result = '';
		$result .= '<div class="row">
			      	<div class="col-md-12">
			  			<div class="form-group">
			   				<label class="col-lg-12 control-label">Doctor\'s Notes :  </label>
				    		<div class="col-lg-12">
				      				<textarea id="doctors_history_notes" rows="5" cols="50"  class="form-control col-md-12 cleditor" > </textarea>
				      		</div>
				      	</div>
				    </div>

				  </div>';
		$result .=  '
		  <br>
			<div class="row">
		        <div class="form-group">
		            <div class="col-lg-12">
		                <div class="center-align">
		                      <a hred="#" class="btn btn-sm btn-info" onclick="save_history_notes('.$visit_id.','.$patient_id.',2)">Save Note</a>
		                     
		                  </div>
		            </div>
		        </div>
		    </div>';


		?>

		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Health History (Please check the box for YES)</h2>
			</header>
			<div class="panel-body">
				<div class="col-md-12">
					<div class="col-md-4">
						<?php echo $health;?>
					</div>
					<div class="col-md-4">
						<?php echo $health2;?>
					</div>
					<div class="col-md-4">
						<?php echo $health3;?>
					</div>
				</div>
				<div class="col-md-12">
					<div id="history_section"></div>
				</div>
				<div class="col-md-12">
					<?php echo $result;?>
				</div>

			</div>
		</section>
	</div>
	
</div>
<script type="text/javascript">
	
	var config_url = $("#config_url").val();
	
	function save_condition1(cond, family, patient_id)
	{
		url = config_url+"nurse/save_family_disease/"+cond+"/"+family+"/"+patient_id;
		$.get( url, function( data ) {
			$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
				$("#new-nav").html(data);
			});
		});
	}
	
	function delete_condition(cond, family, patient_id)
	{
		url = config_url+"nurse/delete_family_disease/"+cond+"/"+family+"/"+patient_id;
		$.get( url, function( data ) {
			$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
				$("#new-nav").html(data);
			});
		});
	}
	
</script>