<?php

$all_services_rs = $this->dental_model->get_all_services();




?>
<!-- <div class="row">
	<div class="col-md-7">
    	<br/> -->
    	<div class="row">
        	<div class="col-md-7 ">
    			<input type="hidden" name="provider_id" value="0">
				<input data-format="yyyy-MM-dd" type="hidden" data-plugin-datepicker class="form-control" name="visit_date_date" id="visit_date_date" placeholder="Admission Date" value="<?php echo date('Y-m-d');?>">
    			<div class="col-md-6" >
    				<h4>Procedures / Services</h4>
    				<hr>
    				<div class="radio" style="position: relative;display: block;margin-top: 9px !important;margin-bottom: 0px !important;font-size: 15px;line-height: 25px;">
    					<?php
    					if($all_services_rs->num_rows() > 0)
    					{
    						foreach ($all_services_rs->result() as $key => $value) {
    							# code...
    							$service_id = $value->service_id;
    							$service_name = $value->service_name;

    							echo '
						
										<label><input type="radio" name="service_id" id="service_id" value="'.$service_id.'" onclick="get_service_charges('.$service_id.','.$visit_id.')"> '.strtoupper($service_name).' </label>
										  <br>
										';
    						}

    					}

    					?>
    				</div>
	                  
	            </div>
	            <div class="col-md-6" >
	            	<div class="col-md-12" id="view-service-items" style="display: none;">
	            	 	<input type="hidden" name="visit_id_checked" id="visit_id_checked">
		            	<div class="form-group">
		                  	<div class="col-md-12">
			                  	<input type="text" name="service_charge_name" id="service_charge_name" class="form-control" placeholder="Service" id="service_charge_name" autocomplete="off" onkeyup="service_list()">

			                    
		                    </div>
		               </div>

		            	
		            	<div id="all-service-charges" style="height: 50vh;overflow-y: scroll;"></div>
	            	
	            	</div>
	            	


	            	
	            </div>
			</div>
			<div class="col-md-5">
				<div id="billing"></div>
			</div>
			
                
		</div>
		<!-- <div class="col-md-5">
			<br/>
	    	<div class="row">
				<div class="form-group">
	                <label class="col-md-4 control-label">Lab Work Description: </label>
	                
	                <div class="col-md-8">
	                	<textarea class="form-control" name="lab_work" id="lab_work_done" ></textarea>
	                </div>
	            </div>
	        </div>
	        <br/>
	        <div class="row">
	        	 <div class="col-md-12" >
	            	<div class="center-align">
						<button class='btn btn-info btn-sm'  onclick="pass_lab_work(<?php echo $visit_id;?>,1);" >Add Lab Charge</button>
					</div>
	            </div>
	        </div>

	        <div id="lab-work-done"></div>
                
		</div> -->
	<!-- </div>	 -->
  <script type="text/javascript">
	  $(document).ready(function(){
	  	// $("#service_id_item").customselect();
	       display_billing(<?php echo $visit_id?>);
	       display_lab_work(<?php echo $visit_id?>);
	  });

  	function open_window_billing(visit_id){
	  var config_url = $('#config_url').val();
	  
	  window.open(config_url+"dental/dental_services/"+visit_id,"Popup","height=1200, width=800, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
	}
	function parse_procedures(visit_id,suck,procedure_id)
    {

    	var res = confirm('Are you sure you want to add this to bill ?');

    	if(res)
    	{
    		 // var procedure_id = document.getElementById("service_id_item_new").value;
		      // alert(procedure_id)
		       procedures(procedure_id, visit_id, suck);
    	}
     
     
    }

	function procedures(id, v_id, suck){
       
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var url = "<?php echo site_url();?>nurse/procedure/"+id+"/"+v_id+"/"+suck;
       
         if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    // document.getElementById("billing").innerHTML=XMLHttpRequestObject.responseText;
                    document.getElementById("service_charge_name").value = '';
                     $('#view-service-items').css('display', 'none');
					 // window.localStorage.setItem('service_id',service_id);
					 // window.localStorage.setItem('visit_id',visit_id);
                    display_billing(v_id);
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

    }

    function pass_lab_work(visit_id,suck)
    {

      var lab_work_done = document.getElementById("lab_work_done").value;
       lab_work_request(lab_work_done, visit_id, suck);
     
    }

    function lab_work_request(lab_work_done, v_id, suck){

    	var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_lab_work/"+v_id;
        //window.alert(data_url);
        var doctor_notes_rx = lab_work_done; 
        //$('#deductions_and_other').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes_rx},
        dataType: 'json',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;

           window.alert(data.message);
           display_lab_work(v_id);  
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        	alert(error);
        	display_lab_work(v_id);  
        }

        });
  

    }

    function display_lab_work(visit_id){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = $('#config_url').val();
	    var url = config_url+"dental/view_lab_work/"+visit_id;
		// alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("lab-work-done").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	
	function display_billing(visit_id){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = $('#config_url').val();
	    var url = config_url+"dental/view_billing/"+visit_id;
		// alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("billing").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  
	   // alert(billed_amount);
	    grand_total(id, units, billed_amount, v_id);

	}
	function grand_total(procedure_id, units, amount, v_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;

	    var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
				{
	    			// display_patient_bill(v_id);
	    			display_billing(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function delete_procedure(id, visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_procedure/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                display_billing(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}


	function delete_lab_work(id, visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"dental/delete_lab_work/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                display_lab_work(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function save_other_deductions(visit_id)
	{
		 // start of saving rx
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_other_deductions/"+visit_id;
        // window.alert(data_url);
         var doctor_notes_rx = $('#deductions_and_other_info').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes_rx},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the payment information");
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}



  </script>