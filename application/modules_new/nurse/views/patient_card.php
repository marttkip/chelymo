<div id="loader" style="display: none;"></div>
<?php if($mike == 1){
	}else{?>
<section class="panel">
	<div class="row">
		<?php if ($module == 0){?>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_accounts/".$visit_id, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-sm btn-primary" value="Send To Accounts" onclick="return confirm('Send to Accounts?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<?php }
			if($doctor_id == 0)
			{
				?>
				<div class="col-md-6">
					<div class="center-align">
						<?php echo form_open("nurse/attend_to_patient/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php

			}
			else
			{
				?>
				<div class="col-md-2">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-success center-align" value="Send To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-warning center-align" value="Send To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php
				

			}
		?>
			

		
		<div class="col-md-4">
			<div class="center-align">
				<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<!-- <div class="row">
	<div class="col-md-12">
	   <div class="alert alert-danger">The process of keying in patient vitals has been changed from auto saving to a manual button saving. Please find a button named  <a hred="#" class="btn btn-sm btn-success" >Save Vitals</a> to save the keyed in vitals. The next row will display the vitals you have keyed in. ~ development team </div>
	</div>
	</div> -->
<div class="well well-sm info">
	<h5 style="margin:0;">
		<div class="row">
			<div class="col-md-4">
				<div class="row">
					<div class="col-lg-4">
						<strong>Name:</strong>
					</div>
					<div class="col-lg-8">
						<?php echo $patient_surname.' '.$patient_othernames;?>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					<div class="col-lg-6">
						<strong>Gender:</strong>
					</div>
					<div class="col-lg-6">
						<?php echo $gender;?>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					<div class="col-lg-6">
						<strong>Age:</strong>
					</div>
					<div class="col-lg-6">
						<?php echo $patient_age;?>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="row">
					<div class="col-lg-6">
						<strong>Balance:</strong>
					</div>
					<div class="col-lg-6">
						Kes <?php echo number_format($account_balance, 2);?>
					</div>
				</div>
			</div>
		</div>
	</h5>
</div>
<div class="center-align">
	<?php
		$error = $this->session->userdata('error_message');
		$validation_error = validation_errors();
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
		echo '<div class="alert alert-danger">'.$error.'</div>';
		$this->session->unset_userdata('error_message');
		}
		
		if(!empty($validation_error))
		{
		echo '<div class="alert alert-danger">'.$validation_error.'</div>';
		}
		
		if(!empty($success))
		{
		echo '<div class="alert alert-success">'.$success.'</div>';
		$this->session->unset_userdata('success_message');
		}
		?>
	<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<?php
if($doctor_id = 1 AND $module == 1)
{

?>
	<div class="tabbable" style="margin-bottom: 18px;">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a href="#patient-history" data-toggle="tab">Past Medical History</a></li>
			<?php if($mike == 1){
				}else{?><li ><a href="#vitals-pane" data-toggle="tab">Vitals</a></li>
			<?php
				}
				?>
			<li><a href="#soap" data-toggle="tab">History</a></li>
			<!-- <li><a href="#medical-checkup" data-toggle="tab">Examination Findings</a></li> -->
			<li><a href="#investigations" data-toggle="tab" onclick="get_patient_investigation(<?php echo $visit_id?>)">Investigations</a></li>
			<li><a href="#plan-visit" data-toggle="tab" onclick="get_patient_plan(<?php echo $visit_id;?>)">Plan</a></li>
			
			<!-- <li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li> -->
		</ul>
		<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
			<div class="tab-pane active" id="patient-history">
				<div id="patient-history-view"></div>
			</div>	
		
			<div class="tab-pane " id="vitals-pane">
				<?php echo $this->load->view("patients/vitals", '', TRUE);?>
			</div>

			<div class="tab-pane" id="soap">
				<?php echo $this->load->view("patients/soap", '', TRUE);?>
				<?php echo $this->load->view("patients/lifestyle", '', TRUE); ?>

			</div>
			<!-- <div class="tab-pane" id="medical-checkup"> -->
				<?php //echo $this->load->view("patients/medical_checkup", '', TRUE);?>
			<!-- </div> -->
			<div class="tab-pane" id="investigations">
				<div id="patient-investigation-view"></div>
			</div>
			<div class="tab-pane" id="plan-visit">
				<div id="patient-plan-view"></div>
				<?php //echo $this->load->view("patients/plan_visit", '', TRUE);?>
			</div>
		
			<div class="tab-pane" id="visit_trail">
				<?php //echo $this->load->view("patients/visit_trail", '', TRUE);?>
			</div>
		</div>
	</div>
<?php
}
else if(($doctor_id == 1 OR $doctor_id > 0) AND $module == 0)
{
	?>
	<div class="tabbable" style="margin-bottom: 18px;">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a href="#patient-history" data-toggle="tab">Past Medical History</a></li>
			<?php if($mike == 1){
				}else{?><li ><a href="#vitals-pane" data-toggle="tab">Vitals</a></li>
			<?php
				}
				?>
			<li><a href="#soap" data-toggle="tab">History</a></li>
			<!-- <li><a href="#medical-checkup" data-toggle="tab">Examination Findings</a></li> -->
			<li><a href="#investigations" data-toggle="tab" onclick="get_patient_investigation(<?php echo $visit_id?>)">Investigations</a></li>
			<li><a href="#plan-visit" data-toggle="tab" onclick="get_patient_plan(<?php echo $visit_id;?>)">Plan</a></li>
			 <li><a href="#previous-vitals" data-toggle="tab" onclick="get_patient_billing(<?php echo $visit_id?>)">Billing</a></li>
			<!-- <li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li> -->
		</ul>
		<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
			<div class="tab-pane active" id="patient-history">
				<div id="patient-history-view"></div>
			</div>	
		
			<div class="tab-pane " id="vitals-pane">
				<?php echo $this->load->view("patients/vitals", '', TRUE);?>
			</div>

			<div class="tab-pane" id="soap">
				<?php echo $this->load->view("patients/soap", '', TRUE);?>
			</div>
			<!-- <div class="tab-pane" id="medical-checkup"> -->
				<?php //echo $this->load->view("patients/medical_checkup", '', TRUE);?>
			<!-- </div> -->
			<div class="tab-pane" id="investigations">
				<div id="patient-investigation-view"></div>
			</div>
			<div class="tab-pane" id="plan-visit">
				<div id="patient-plan-view"></div>
				<?php //echo $this->load->view("patients/plan_visit", '', TRUE);?>
			</div>
			<div class="tab-pane" id="previous-vitals">
                <div id="patient-billing-view"></div>
             </div>
			
			<div class="tab-pane" id="visit_trail">
				<?php //echo $this->load->view("patients/visit_trail", '', TRUE);?>
			</div>
		</div>
	</div>
<?php
}
?>
<?php if($mike == 1){
	}else{?>
<div class="row">
	<?php if ($module == 0){?>
	<div class="col-md-2">
		<div class="center-align">
			<?php echo form_open("nurse/send_to_accounts/".$visit_id, array("class" => "form-horizontal"));?>
			<input type="submit" class="btn btn-large btn-primary" value="Send To Accounts" onclick="return confirm('Send to Accounts?');"/>
			<?php echo form_close();?>
		</div>
	</div>
	<?php }
		if($doctor_id == 0)
		{
			?>
			<div class="col-md-6">
				<div class="center-align">
					<?php echo form_open("nurse/attend_to_patient/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
					<input type="submit" class="btn btn-sm btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
					<?php echo form_close();?>
				</div>
			</div>
			<?php

		}
		else
		{
			?>
			<div class="col-md-2">
				<div class="center-align">
					<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
					<input type="submit" class="btn btn-sm btn-success center-align" value="Send To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
					<?php echo form_close();?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="center-align">
					<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
					<input type="submit" class="btn btn-sm btn-warning center-align" value="Send To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
					<?php echo form_close();?>
				</div>
			</div>
			<?php
			

		}
	?>
</div>
<?php } ?>

<script type="text/javascript">
    
    function get_patient_billing(visit_id)
    {
         // document.getElementById("loader-circle").style.display = "block";
         document.getElementById("loader").style.display = "block";
         var XMLHttpRequestObject = false;       
         if (window.XMLHttpRequest) {
         
           XMLHttpRequestObject = new XMLHttpRequest();
         } 
           
         else if (window.ActiveXObject) {
           XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"accounts/get_patient_bill_view/"+visit_id;

         // alert(url);
         if(XMLHttpRequestObject) {
           var obj = document.getElementById("patient-billing-view");
           XMLHttpRequestObject.open("GET", url);
               
           XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

               obj.innerHTML = XMLHttpRequestObject.responseText;
               //obj2.innerHTML = XMLHttpRequestObject.responseText;
               // $("#procedure_id").customselect();
               // vitals_interface(visit_id);

               $("#service_id_item").customselect();
               $("#provider_id_item").customselect();
               $("#parent_service_id").customselect();
               get_visit_detail(visit_id);
               document.getElementById("loader").style.display = "none";
             }
           }
           
           XMLHttpRequestObject.send(null);
         }
          // document.getElementById("loader").style.display = "none";
    }
</script>
<script type="text/javascript">
    function get_visit_detail(visit_id)
    {
        // alert(visit_id);
        
        document.getElementById("visit_id_checked").value = visit_id;
        document.getElementById("visit_id_payments").value = visit_id;
        document.getElementById("visit_id_visit").value = visit_id;
        document.getElementById("visit_discharge_visit").value = visit_id;

        display_patient_bill(visit_id);
    }
    function get_next_page(page,visit_id)
    {
        // alert()
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_visits_div/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visits_div").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }

    function get_next_invoice_page(page,visit_id)
    {
        // alert(page);
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }

    function get_next_payments_page(page,visit_id)
    {
        // alert(page);
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_receipt/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }


    function get_page_header(visit_id)
    {
        // alert()
         var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_details_bill_header/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("page_header").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

    }
</script>

<script type="text/javascript">
	var config_url = document.getElementById("config_url").value;

	
	$(document).ready(function(){
		var visit_id = <?php echo $visit_id?>;
		var config_url = document.getElementById("config_url").value;
		get_patient_history(visit_id);

		
		// $.get(config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
		// 	$("#new-nav").html(data);
		// 	$("#checkup_history").html(data);
		// });
	});

	function get_patient_history(visit_id)
	{
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_history_view/"+visit_id;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-history-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       // alert("sadhkasjdhakj");
       			
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}
	function get_patient_investigation(visit_id)
	{
		 // document.getElementById("loader-circle").style.display = "block";
		 document.getElementById("loader").style.display = "block";
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_investigation_view/"+visit_id;
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-investigation-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       $("#lab_test_id").customselect();
		       $("#xray_id").customselect();
		       get_lab_table(visit_id);
		       get_xray_table(visit_id);
		       get_xray_scans(visit_id);
		       document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}

	function parse_xray(visit_id)
	{
	  var xray_id = document.getElementById("xray_id").value;
	  xray(xray_id, visit_id);

	}

	function xray(id, visit_id){
	    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id+"/"+id;
	    // window.alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	               document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	               //get_xray_table(visit_id);
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_xray_table(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	                document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_xray_scans(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_scans/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	                document.getElementById("xray_scans").innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_patient_plan(visit_id)
	{
		

  		document.getElementById("loader").style.display = "block";
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_plan_view/"+visit_id;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-plan-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       $("#drug_id").customselect();
		       $("#diseases_id").customselect();
		       $("#theatre_procedure_id").customselect();
		       get_disease(visit_id);
		       display_prescription(visit_id,0);
		       display_inpatient_prescription(visit_id,0);
		       get_theatre_procedures_table(visit_id,0);
		       	tinymce.init({
   					selector: ".cleditor",
   					height : "100"
   				});

		       document.getElementById("loader").style.display = "none";
		       // get_lab_table(visit_id);

		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}
	// start of lab details
	function parse_lab_test(visit_id)
   {
     var lab_test_id = document.getElementById("lab_test_id").value;
      lab(lab_test_id, visit_id);
     
   }
    function get_lab_table(visit_id){
         var XMLHttpRequestObject = false;
             
         if (window.XMLHttpRequest) {
         
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
             
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"laboratory/test_lab/"+visit_id;
     
         if(XMLHttpRequestObject) {
                     
             XMLHttpRequestObject.open("GET", url);
                     
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                 }
             }
             
             XMLHttpRequestObject.send(null);
         }
     }
   
    function lab(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"laboratory/test_lab/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_lab_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
	// end of lab details
</script>
<script type="text/javascript">



     
  function getservices(id){

        var type_payment =  $("input[name='type_payment']:checked").val();

        // var myTarget1 = document.getElementById("service_div");
        var myTarget5 = document.getElementById("normal_div");
        var myTarget6 = document.getElementById("waiver_div");
        // alert(id);
        if(type_payment == 1)
        {
          myTarget6.style.display = 'none';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget6.style.display = 'block';
          myTarget5.style.display = 'none';
        }
        
  }



  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget4 = document.getElementById("debit_card_div");

    var myTarget5 = document.getElementById("bank_deposit_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 7)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'block';
    }
    else if(payment_type_id == 8)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'block';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 6)
    {
       myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';  
    }

  }

   function display_patient_bill(visit_id){

    // alert(visit_id);
      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/patient_bill_view/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;

                 get_services_offered(visit_id);
                 get_patient_receipt(visit_id);
                 get_page_header(visit_id);
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

      
  }
  function get_services_offered(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_services_billed/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("billed_services").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      get_page_header(visit_id);
  }

  function get_patient_receipt(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_patient_receipt/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      get_page_header(visit_id);
  }
  function get_all_visits_div(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/get_visits_div/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visits_div").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

    //Calculate procedure total
    function calculatetotal(amount, id, procedure_id, v_id){
           
        var units = document.getElementById('units'+id).value;  
        var billed_amount = document.getElementById('billed_amount'+id).value;  

        grand_total(id, units, billed_amount, v_id);
    }
    function grand_total(procedure_id, units, amount, v_id){
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
    
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_id: v_id},
        dataType: 'json',
        success:function(data){
            alert(data.message);
            display_patient_bill(v_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(v_id);
        }
        });
        return false;

        
       
    }
    function delete_service(id, visit_id){

        var res = confirm('Are you sure you want to delete this charge?');
        
        if(res)
        {

            var config_url = document.getElementById("config_url").value;
            var url = config_url+"accounts/delete_service_billed/"+id+"/"+visit_id;
        
            $.ajax({
            type:'POST',
            url: url,
            data:{visit_id: visit_id,id: id},
            dataType: 'json',
            success:function(data){
                alert(data.message);
                display_patient_bill(visit_id);
                // get_all_visits_div(patient_id);
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                display_patient_bill(visit_id);
            }
            });
            return false;
            var XMLHttpRequestObject = false;
                
            if (window.XMLHttpRequest) {
            
                XMLHttpRequestObject = new XMLHttpRequest();
            } 
                
            else if (window.ActiveXObject) {
                XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            }
             var config_url = document.getElementById("config_url").value;
            var url = config_url+"accounts/delete_service_billed/"+id;
            
            if(XMLHttpRequestObject) {
                        
                XMLHttpRequestObject.open("GET", url);
                        
                XMLHttpRequestObject.onreadystatechange = function(){
                    
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                        display_patient_bill(visit_id);
                    }
                }
                        
                XMLHttpRequestObject.send(null);
            }
        }
    }
    function save_service_items(visit_id)
    {
        var provider_id = $('#provider_id'+visit_id).val();
        var service_id = $('#service_id'+visit_id).val();
        var visit_date = $('#visit_date_date'+visit_id).val();
        var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
        dataType: 'text',
        success:function(data){
            alert("You have successfully billed");
            display_patient_bill(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    }

    $(document).on("submit","form#add_bill",function(e)
    {
        e.preventDefault(); 

        var service_id = $('#service_id_item').val();
        var patient_id = $('#patient_id_item').val();
        var provider_id = $('#provider_id_item').val();
        var visit_date = $('#visit_date_date').val();
        var visit_id = $('#visit_id_checked').val();
        var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
        dataType: 'json',
        success:function(data){

            // alert(data.message);
            $('#add_to_bill').modal('toggle');

            display_patient_bill(visit_id);
            get_all_visits_div(patient_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });

    $(document).on("submit","form#visit_type_change",function(e)
    {
        e.preventDefault(); 

        var visit_type_id = $('#visit_type_id').val();
        var visit_id = $('#visit_id_visit').val();
        var url = "<?php echo base_url();?>accounts/change_patient_visit/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_type_id: visit_type_id},
        dataType: 'text',
        success:function(data){
            alert("You have successfully changed patient type");
            $('#change_patient_type').modal('toggle');
            display_patient_bill(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });

    $(document).on("submit","form#discharge-patient",function(e)
    {
        e.preventDefault(); 

        var visit_date_charged = $('#visit_date_charged').val();
        var visit_id = $('#visit_discharge_visit').val();
        var url = "<?php echo base_url();?>accounts/discharge_patient/"+visit_id;
        
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_date_charged: visit_date_charged},
        dataType: 'json',
        success:function(data){
            alert(data.message);
            $('#end_visit_date').modal('toggle');
            display_patient_bill(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });

    $(document).on("submit","form#payments-paid-form",function(e)
    {
        // alert("changed");
        e.preventDefault(); 

        var cancel_action_id = $('#cancel_action_id').val();
        var cancel_description = $('#cancel_description').val();
        var visit_id = $('#visit_id').val();
        var payment_id = $('#payment_id').val();
        var url = "<?php echo base_url();?>accounts/cancel_payment/"+payment_id+"/"+visit_id;       
        $.ajax({
        type:'POST',
        url: url,
        data:{cancel_description: cancel_description, cancel_action_id: cancel_action_id},
        dataType: 'text',
        success:function(data){
            alert("You have successfully cancelled a payment");
            $('#refund_payment'+visit_id).modal('toggle');
            get_page_header(visit_id);
            get_patient_receipt(visit_id);
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            get_page_header(visit_id);
            get_patient_receipt(visit_id);
        }
        });
        return false;
    });


    $(document).on("submit","form#add_payment",function(e)
    {
        e.preventDefault(); 

        var payment_method = $('#payment_method').val();
        var amount_paid = $('#amount_paid').val();
        var type_payment =  $("input[name='type_payment']:checked").val(); //$('#type_payment').val();
        // alert(amount_paid); die();
        var service_id = $('#service_id').val();
        var waiver_amount = $('#waiver_amount').val();
        var waiver_service_id = $('#waiver_service_id').val();      
        var cheque_number = $('#cheque_number').val();
        var insuarance_number = $('#insuarance_number').val();
        var mpesa_code = $('#mpesa_code').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var change_payment = $('#change_payment').val();

        var debit_card_detail = $('#debit_card_detail').val();
        var deposit_detail = $('#deposit_detail').val();
        var password = $('#password').val();


        var visit_id = $('#visit_id_payments').val();

        var payment_service_id = $('#payment_service_id').val();

    
        var url = "<?php echo base_url();?>accounts/make_payments/"+visit_id;
        // alert(type_payment);
        $.ajax({
        type:'POST',
        url: url,
        data:{payment_method: payment_method, amount_paid: amount_paid, type_payment: type_payment,service_id: service_id, cheque_number: cheque_number, insuarance_number: insuarance_number, mpesa_code: mpesa_code,username: username,password: password, payment_service_id: payment_service_id,debit_card_detail: debit_card_detail,deposit_detail: deposit_detail,change_payment:change_payment,waiver_amount: waiver_amount, waiver_service_id},
        dataType: 'json',
        success:function(data){

            if(data.result == 'success')
            {
                alert(data.message);

                $('#add_payment_modal').modal('toggle');
                get_page_header(visit_id);
                get_patient_receipt(visit_id,null);
                 
            }
            else
            {
                alert(data.message);
            }
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    });
    function close_visit(visit_id)
    {
        var res = confirm('Are you sure you want send to accounts ?');
     
        if(res)
        {
            var url = "<?php echo base_url();?>accounts/close_visit/"+visit_id;
        
            $.ajax({
            type:'POST',
            url: url,
            data:{visit_id: visit_id},
            dataType: 'json',
            success:function(data){
                alert(data.message);
                // setTimeout(function() {
                //  send_message(visit_id);
                //   }, 2000);
                // display_patient_bill(visit_id);
                window.location.href = '<?php echo base_url();?>queues/outpatient-queue';
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                display_patient_bill(visit_id);
            }
            });
            return false;

        }
    }
    function send_message(visit_id)
    {
        var url = "<?php echo base_url();?>accounts/send_message/"+visit_id;
        
            $.ajax({
            type:'POST',
            url: url,
            data:{visit_id: visit_id},
            dataType: 'json',
            success:function(data){
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                
            }
            });
            return false;
    }
    function get_change()
    {

        var visit_id = $('#visit_id_payments').val();
    
        var amount_paid = $('#amount_paid').val();
        var url = "<?php echo base_url();?>accounts/get_change/"+visit_id;
    
        $.ajax({
        type:'POST',
        url: url,
        data:{visit_id: visit_id, amount_paid: amount_paid},
        dataType: 'json',
        success:function(data){
            var change = data.change;

            document.getElementById("change_payment").value = change;
            $('#change_item').html("Kes."+data.change);

        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            display_patient_bill(visit_id);
        }
        });
        return false;
    }


    function display_treatment_prescription(visit_id,module){

        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"doctor/display_previous_prescription/"+visit_id+"/"+module;
        // alert(url);
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("visit_prescription_two").innerHTML=XMLHttpRequestObject.responseText;
                    
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }
    function display_todays_tsheet(visit_id,module){

        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"doctor/display_tsheet_prescription/"+visit_id+"/"+module;
        // alert(url);
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("todays_tsheet_drugs").innerHTML=XMLHttpRequestObject.responseText;
                    $("#prescription_items").customselect();
                    
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }

    function pass_days_tsheet(visit_id)
   {
   var prescription_items = document.getElementById("prescription_items").value;


   save_drug_tsheet(prescription_items, visit_id);
   
   }
   
   function save_drug_tsheet(prescription_items, visit_id){
   
       var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/save_tsheet/"+prescription_items+"/"+visit_id;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }
   function update_morning_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#morning'+prescription_id).val();
    
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_morning_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }


    function update_midday_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#midday'+prescription_id).val();
    // alert(checkedValue);
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_midday_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }

    function update_evening_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#evening'+prescription_id).val();
    
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_evening_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }

   function update_night_value(prescription_id,visit_id)
   {
  
    var checkedValue = $('#night'+prescription_id).val();
    
    var XMLHttpRequestObject = false;
         
       if (window.XMLHttpRequest) {
       
         XMLHttpRequestObject = new XMLHttpRequest();
       } 
         
       else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
       } 
       var config_url = $('#config_url').val();
       var url = config_url+"doctor/update_night_value/"+prescription_id+"/"+visit_id+"/"+checkedValue;
        // alert(url);
       if(XMLHttpRequestObject) {
             
         XMLHttpRequestObject.open("GET", url);
             
         XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             display_todays_tsheet(visit_id,1);
           }
         }
         
         XMLHttpRequestObject.send(null);
       }
   }


   function delete_tsheet_prescription(prescription_id, visit_id)
   {
   var res = confirm('Are you sure you want to mark this drug as completed?. This drug will not appear tommorow when prescribing');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = config_url+"doctor/delete_tsheet_prescription/"+prescription_id+"/"+visit_id;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
             display_todays_tsheet(visit_id,1);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }

   function get_patient_treatment_sheet(visit_id)
   {
        document.getElementById("loader").style.display = "block";
        display_treatment_prescription(visit_id,1);
        display_todays_tsheet(visit_id,1);
        document.getElementById("loader").style.display = "none";

   }

</script>