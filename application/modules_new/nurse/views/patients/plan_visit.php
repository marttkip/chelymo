
<div class="row">
  <div class="col-md-6">
    <section class="panel panel-featured panel-featured-info">
      <header class="panel-heading">
        <h2 class="panel-title">Diagnosis</h2>
      </header>

      <div class="panel-body">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" name="disease_q" id="disease_q" class="form-control" placeholder=" Search diseaase name, disease code " autocomplete="off" onkeyup="get_diseases_list(<?php echo $visit_id;?>)">
               <!--  <select id='diseases_id' name='diseases_id' class='form-control custom-select ' >
                  <option value=''>None - Please Select a diagnosis</option>
                  <?php echo $diseases;?>
                </select> -->
              </div>
            
            </div>
            <div class="col-md-12">
                <div id="disease-list"></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
 
              <a class='btn btn-sm btn-info ' data-toggle='modal' data-target='#add_diseases' ><i class="fa fa-plus"></i> Add Missing Disease</a>
            </div> 
      </div>
      <!-- visit Procedures from java script -->
     
      <!-- end of visit procedures -->
      <div id="patient_diagnosis"></div>
           <div class="modal fade bs-example-modal-lg" id="add_diseases" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Disease</h4>
                </div>
                 <?php echo form_open("nurse/add_diseases", array("class" => "form-horizontal"));?>
                <div class="modal-body">
                  <div class="row">
                      <div class='col-md-12'>
                            <div class="form-group">
                <label class="col-lg-4 control-label">Disease Name: </label>
                <div class="col-lg-5">
                  <input type="text" class="form-control" name="diseases_name" placeholder="" autocomplete="off">
                  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                </div>
              </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class='btn btn-info btn-sm' type='submit' >Add Disease</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
                <?php echo form_close();?>
            </div>
        </div>
</div>


            
    </section>
    </div>
    <div class="col-md-6">
      <div class="row">
   <div class="col-md-12">
    <!-- Widget -->
    <section class="panel panel-featured panel-featured-info">
          <header class="panel-heading">
              <h2 class="panel-title">Diagnosis</h2>
          </header>
          <div class="panel-body">
            <div class="padd">
                <?php $data['visit_id'] = $visit_id;?>
               <!-- vitals from java script -->
               <?php echo $this->load->view("nurse/soap/view_diagnosis", $data, TRUE); ?>
               
               <!-- end of vitals data -->
            </div>
          </div>
      </section>
    </div>
 </div>
    </div>
</div>
<?php 
if($allow_prescription)
{

?>
<div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Prescription</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='drug_id' name='drug_id' class='form-control custom-select ' onchange="get_drug_to_prescribe(<?php echo $visit_id;?>);">
                      <option value=''>None - Please Select an drug</option>
                      <?php echo $drugs;?>
                    </select>
                  </div>
                
                </div>
                
                <!-- end of visit procedures -->
            </div>
             <div id="prescription_view"></div>
             <div id="visit_prescription"></div>

             <?php // echo $this->load->view("pharmacy/display_prescription", $data, TRUE); ?>
              
         </section>
    </div>
</div>
<?php
}
else
{
  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger">Sorry you are unable to prescribe drugs before you diagnose</div>
    </div>
  </div>
  <?php
}
?>

<div id="theatre_results">
  <div class="row">
  <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Theatre Procedures</h2>
            </header>
            <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='theatre_procedure_id' name='theatre_procedure_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select an Theatre Procedure</option>
                      <?php echo $theatre;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="parse_theatre_procedures(<?php echo $visit_id;?>);"> Add an Theatre Procedure</button>
                  </div>
                </div>
                 <!-- visit Procedures from java script -->
                
                <!-- end of visit procedures -->
            </div>
             <div id="theatre_procedures_table"></div>
             <?php echo $this->load->view("theatre/tests/test2", $data, TRUE); ?>
         </section>
    </div>
</div>
  
</div>



<div class="row">
    <div class="col-md-12">
          <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Plan</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                <div id="visit_plan_detail"></div>

                <table align='center' class='table table-striped table-hover table-condensed'>
                  <tr>
                    <th>Plan Name</th>
                    <th>Description</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td valign='top'>
                      <select id='plan_id' class='form-control'>
                        <?php
                        $plan_rs = $this->nurse_model->get_plan_details();
                        $num_plans = count($plan_rs);
                        if($num_plans > 0){
                          foreach ($plan_rs as $key_item):
                        
                          $plan_name = $key_item->plan_name;
                          $plan_id = $key_item->plan_id;
                          echo "<option value='".$plan_id."'>".$plan_name."</option>";
                          endforeach;
                        }
                        ?>
                    </select>
                    </td>
                        <td><textarea id='plan_description_value' class='cleditor' ></textarea></td>
                        <td><input type='button' class='btn btn-sm btn-success' value='Save' onclick='save_visit_plan_detail(<?php echo $visit_id?>)' /></td>
                    </tr>
                </table>
                <!-- end of vitals data -->
              </div>
            </div>
          </section>
    </div>
</div>