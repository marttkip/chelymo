<?php

class Moh_reports_model extends CI_Model 
{
	public function get_moh_morbidity_report($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('diseases.*');
		$this->db->where($where);
		$this->db->order_by('diseases.diseases_id','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	
	}

	public function get_counters_per_disease($diseases_id,$visit_date)
	{
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_id = diagnosis.visit_id AND diseases.diseases_id = diagnosis.disease_id AND diagnosis.disease_id = '.$diseases_id.' AND visit.visit_date = "'.$visit_date.'"';

		// if($visit_date == "2017-03-10")
		// 	{
		// 			var_dump($where); die();
		// 	}
		// var_dump($where); die();
		$visit_report_search = $this->session->userdata('morbidity_report_search');
		
		if(!empty($visit_report_search))
		{
			// var_dump($visit_report_search); die();
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' ';
		}
		$this->db->from('diseases,diagnosis,visit,patients');
		$this->db->where($where);

		return $this->db->count_all_results();
	}
	public function get_per_disease_total($visit_date, $patient_id)
	{
		$no_of_visit_per_month = 0;
		$where = 'visit.patient_id = patients.patient_id AND visit.visit_delete = 0 AND visit.visit_id = diagnosis.visit_id AND diseases.diseases_id = diagnosis.disease_id AND visit.visit_date < "'.$visit_date.'" AND patients.patient_id = '.$patient_id;
		
		$visit_report_search = $this->session->userdata('morbidity_report_search');
		
		if(!empty($visit_report_search))
		{
			$where .= $visit_report_search;
		}
		else
		{
			$where .= ' ';
		}
		//var_dump($where);die();
		$this->db->select('COUNT(visit.visit_id) as no_of_visit');
		$this->db->where($where);
		$query = $this->db->get('diseases,diagnosis,visit,patients');
		
		if($query->num_rows() > 0)
		{
			$result = $query->row();
			$no_of_visit_per_month = $result->no_of_visit;
		}
		return $no_of_visit_per_month;
	}
	public function get_all_visit_all($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,service.*');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	

	}

	public function get_all_visit_all_procedures($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,service.*');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		$this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_visit_all_content($table, $where, $order_by, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,service.*');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		$query = $this->db->get('');
		
		return $query;
	}

	public function get_test_procedures_done($visit_id)
	{
		$where = 'service_charge.service_id = service.service_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_name ="Laboratory" AND service.service_delete = 0  AND visit.visit_id = visit_charge.visit_id AND patients.patient_id = visit.patient_id AND visit_charge.visit_charge_delete = 0 AND visit_charge.charged = 1 AND visit_charge.visit_id = '.$visit_id;
		$table = 'service_charge,service,visit_charge,visit,patients';

		$this->db->from($table);
		$this->db->select('visit.*,visit_charge.*, patients.*, service_charge.*,service.*');
		$this->db->where($where);
		$this->db->order_by('visit.visit_date','DESC');
		$query = $this->db->get('');
		return $query;

	}

	public function calculate_age($patient_date_of_birth)
	{
		$value = $this->dateDiff(date('y-m-d  h:i'), $patient_date_of_birth." 00:00", 'year');
		
		return $value;
	}
	public function dateDiff($time1, $time2, $interval) 
	{
	    // If not numeric then convert texts to unix timestamps
	    if (!is_int($time1)) {
	      $time1 = strtotime($time1);
	    }
	    if (!is_int($time2)) {
	      $time2 = strtotime($time2);
	    }
	 
	    // If time1 is bigger than time2
	    // Then swap time1 and time2
	    if ($time1 > $time2) {
	      $ttime = $time1;
	      $time1 = $time2;
	      $time2 = $ttime;
	    }
	 
	    // Set up intervals and diffs arrays
	    $intervals = array('year','month','day','hour','minute','second');
	    if (!in_array($interval, $intervals)) {
	      return false;
	    }
	 
	    $diff = 0;
	    // Create temp time from time1 and interval
	    $ttime = strtotime("+1 " . $interval, $time1);
	    // Loop until temp time is smaller than time2
	    while ($time2 >= $ttime) {
	      $time1 = $ttime;
	      $diff++;
	      // Create new temp time from time1 and interval
	      $ttime = strtotime("+1 " . $interval, $time1);
	    }
	 
	    return $diff;
  	}

  	public function get_moh_item_list($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('departments.*');
		$this->db->where($where);
		$this->db->order_by('departments.department_id','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	
	}

	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function get_all_items($table,$where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_all_visits($date, $end)
	{
		$this->db->select('patient_id, visit_date');
		$this->db->where('visit_delete = 0 AND visit.visit_date >= "'.$date.'" AND visit.visit_date <= "'.$end.'"');
		$query = $this->db->get('visit');
		
		return $query;
	}
}

?>