<div class="row statistics">
    
    <div class="col-md-12 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown for <?php echo $total_visits;?> patients</h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <div class="row">
                   
                    <div class="col-md-3">
                        <?php
                        
                            $total_invoices_revenue = $this->accounting_model->get_visit_invoice_totals();
                            $total_invoices_revenue2 = $this->accounting_model->get_visit_invoice_totals2();
                            $total_transfers = $this->accounting_model->get_visit_invoice_children_totals();
                            $total_payments_revenue = $this->accounting_model->get_visit_payment_totals();
                            $total_payments_revenue2 = $this->accounting_model->get_visit_payment_totals2();
                            $total_waiver_revenue = $this->accounting_model->get_visit_waiver_totals();
                            $total_debits_revenue = $this->accounting_model->get_visit_debits_totals();
                      
                            $all_payments_period = $this->accounting_model->all_payments_period();
                            $receivable = $all_payments_period - $total_payments_revenue;
                            $total_rejected_amounts = $this->accounting_model->get_rejected_amounts();
                            


                            $all_properties = $this->accounting_model->get_all_visit_types();
                            $receivables = '';
                            $total_balances = 0;
                           if($all_properties->num_rows() > 0)
                            {

                           foreach ($all_properties->result() as $key => $value) {
        // code...
                          $visit_type_id = $value->visit_type_id;
                          $visit_type_name = $value->visit_type_name;
                          $property_balance = $this->accounting_model->get_receivable_balances($visit_type_id);
                          $receivables .= '   <tr>
                                                    <td class="text-left">'.strtoupper($visit_type_name).'</td>
                                            <td class="text-right"><a href="'.site_url().'customer-invoices/'.$visit_type_id.'" >'.number_format($property_balance,2).'</a></td>
                                        </tr>';
                          $total_balances += $property_balance;
                          }


                         }
                        $receivables .= '    <tr>
                                            <td class="text-left"><b>Total Payments</b></td>
                                    <td class="text-right"><b class="match">'.number_format($total_balances,2).'</b></td>
                                </tr>';

                            // $total_invoices_revenue -= $total_rejected_amounts;


                            // get payments done today and visits not for today
                            // var_dump($total_payments_revenue); die();
                            $cash_debt_repayments = $this->accounting_model->get_all_visit_payments_totals(1,1);
                            $insurance_debt_repayments = $this->accounting_model->get_all_visit_payments_totals(2,1);




                            if($receivable < 0)
                            {
                                $receivable = ($receivable);
                            }

                        ?>
                        <?php
                        $patients = $this->accounting_model->get_patients_visits(1);
                        // $returning_patients = $this->accounting_model->get_patients_visits(0);
                        ?>
                        <h5>VISIT TYPE BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                 <tr>
                                    <th>TOTAL INVOICES</th>
                                    <td><?php echo number_format($total_invoices_revenue + $total_invoices_revenue2, 2);?></td>
                                </tr>
                                
                               <tr>
                                    <th>TOTAL DEBITS</th>
                                    <td><?php echo number_format($total_debits_revenue, 2);?></td>
                                </tr>
                                <tr>
                                    <th>TOTAL COLLECTION</th>
                                    <td>(<?php echo number_format($total_payments_revenue + $total_payments_revenue2, 2);?>)</td>
                                </tr>
                                 <tr>
                                    <th>TOTAL WAIVERS</th>
                                    <td>(<?php echo number_format($total_waiver_revenue, 2);?>)</td>
                                </tr>

                                <tr>
                                    <th>DEBT BALANCE</th>
                                    <td><?php echo number_format(($total_invoices_revenue +$total_debits_revenue + $total_invoices_revenue2 - $total_payments_revenue - $total_payments_revenue2 - $total_waiver_revenue), 2);?></td>
                                </tr>
                            </tbody>
                        </table>
                   
                    

                         <h5>VISIT  BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr>
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients['total_new'] + $patients['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <div class="clearfix"></div>
                    </div>
                    <!-- End Transaction Breakdown -->
                   
                   
                    <div class="col-md-2">
                       
                        <h5>PAYMENT METHODS BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php
                                $total_cash_breakdown = 0;
                                $payment_methods = $this->accounting_model->get_payment_methods();
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = $this->accounting_model->get_amount_collected($payment_method_id);
                                 
                                        
                                        echo 
                                        '
                                        <tr>
                                            <th>'.$method_name.'</th>
                                            <td>'.number_format($total, 2).'</td>
                                        </tr>
                                        ';
                                        $total_cash_breakdown += $total;
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th>Total</th>
                                        <td>'.number_format($total_cash_breakdown, 2).'</td>
                                    </tr>
                                    ';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                   
                   <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-4 center-align">
                                <h4>CASH INVOICED</h4>
                                <h3>Ksh <?php echo number_format($total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                 <h4>INSURANCE INVOICED</h4>
                           <h3>Ksh <?php echo number_format($total_invoices_revenue2 + $total_debits_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                 <h4>TOTAL INVOICE</h4>
                                <h3>Ksh  <?php echo number_format($total_invoices_revenue2 + $total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>
                            <hr>
                         <div class="row">
                            <div class="col-md-4 center-align">
                                   <h4>CASH PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue2, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                             <h4>INSURANCE PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                     <h4>TOTAL PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue + $total_payments_revenue2, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>

                       <hr>
                       
                        <hr>
                         <div class="row">
                            <div class="col-md-4 center-align">
                                   <h4>CASH BALANCE</h4>
                            <h3>Ksh <?php echo number_format($total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue - $total_payments_revenue2 , 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                             <h4>INSURANCE BALANCE</h4>
                            <h3>Ksh <?php echo number_format($total_invoices_revenue2 + $total_debits_revenue - $total_payments_revenue , 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                     <h4>TOTAL BALANCE</h4>
                                 <h3>Ksh  <?php echo number_format($total_invoices_revenue2 + $total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue - $total_payments_revenue -  $total_payments_revenue2, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>
                       <hr>
                        <div class="row" style="margin-top: 20px;">
                            <div class="center-align">
                                
                              <h4>UPDATE PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_balances, 2);?></h3>
                            </div>
                        </div>
                     
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>