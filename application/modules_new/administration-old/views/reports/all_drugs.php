<?php
$drug_result = '';
if($query->num_rows() > 0)
{
	$count = $page;
	$drug_result .='
			<table class="table table-hover table-bordered table-striped table-responsive col-md-12" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Visit Date</th>
						<th>Patient Name</th>
						<th>Patient Type</th>
						<th>Drug</th>
						<th>Prescribed By</th>
						<th>Dispensed By</th>
						<th>Unit Price</th>
						<th>Units</th>
						<th>Selling Price</th>
						<th>Total</th>
						<th>Units</th>
						<th>Initial Stock</th>
						<th>Current Stock</th>
					</tr>
				</thead>
				<tbody>
			';
	foreach($query->result() as $visit_drug_result)
	{
		$visit_id = $visit_drug_result->visit_id;
		$patient_id = $visit_drug_result->patient_id;
		$patient_surname = $visit_drug_result->patient_surname;
		$patient_othernames = $visit_drug_result->patient_othernames;
		$patient_id = $visit_drug_result->patient_id;
		$visit_type_name = $visit_drug_result->visit_type_name;
		$service_charge = $visit_drug_result->service_charge_name;
		$service_amount = $visit_drug_result->service_charge_amount;
		$visit_charge_amount = $visit_drug_result->visit_charge_amount;
		$branch_code = $visit_drug_result->branch_code;
		$patient_number = $visit_drug_result->patient_number;
		$visit_date = date('jS M Y',strtotime($visit_drug_result->visit_date));
		$qty_given = $visit_drug_result->units_given;
		$patient_type = $visit_drug_result->patient_type;
		if($patient_type == 1)
		{
		$patient_name = 'Walkin';

		}
		else
		{
		$patient_name = $patient_surname.' '.$patient_othernames;

		}
		$dispense_by = $visit_drug_result->b_first_name.' '.$visit_drug_result->b_onames;
		$prescribed_by = $visit_drug_result->a_first_name.' '.$visit_drug_result->a_onames;
		$count++;
		

		//find items in stock:
		 //1. create db connection
		//$db_host = "localhost"; $db_name="ambiance"; $db_password = ""; $db_username = "root";

		//$con = new mysqli($db_host, $db_username, $db_password, $db_host);

		//2. find quantity for each drug

		//$productQuery = $con->query("SELECT * FROM product WHERE `product_name` = '{$service_charge}'");

		//$inStock = 0;
		//while($row = $productQuery->fetch_array()){

			//$inStock += $row['quantity'];
		//}





		//branch Code
	
		$drug_result .='
					<tr>
						<td>'.$count.'</td>
						<td>'.$visit_date.'</td>
						<td>'.$patient_name.'</td>
						<td>'.$visit_type_name.'</td>
						<td>'.$service_charge.'</td>
						<td>'.$prescribed_by.'</td>
						<td>'.$dispense_by.'</td>
						<td>'.number_format($service_amount,2).'</td>
						<td>'.$qty_given.'</td>
						<td>'.number_format($visit_charge_amount,2).'</td>
						<td>'.number_format(($visit_charge_amount * $qty_given),2).'</td>
					</tr>';
	}
	$drug_result.='
				</tbody>
			</table>';
}
else
{
	$drug_result.= 'No drugs have been dispensed';
}
// echo $this->load->view('administration/reports/graphs/drug_sales', '', TRUE);
?>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>
            <div class="panel-body">
             <?php echo $this->load->view("administration/reports/search/drugs", '', TRUE);?>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>
            <!-- Widget content -->
            <div class="panel-body">
                <div class="row">
                	<div class="col-md-4 col-md-offset-8">
                    	<!-- <a class="btn btn-sm btn-warning" id="open_search" onclick="open_search_box()" pull-right><i class="fa fa-search"></i> Open Search</a>
                    	<a class="btn btn-sm btn-info" id="close_search" style="display:none;" onclick="close_search()"><i class="fa fa-search-minus"></i> Close Search</a> -->
                        <?php
                        $search = $this->session->userdata('all_drugs_search');
						if(!empty($search))
						{
						?>
                    	<a class="btn btn-sm btn-danger" href="<?php echo site_url().'administration/reports/clear_drugs_search';?>"><i class="fa fa-search"></i> Clear Search</a>
                        <?php }?>
                    	<a class="btn btn-sm btn-success" target="_blank" href="<?php echo site_url().'administration/reports/export_drugs';?>">Download</a>
                    </div>
                </div>
                
                <div class="row">
                	<div class="col-md-12">
            			<?php echo $drug_result;?>
                    </div>
                </div>
            </div>
            <!--<a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">EXCEL DOWNLOADS</a>-->
            <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">

	function open_search_box()
	{
		var myTarget2 = document.getElementById("search_section");
		var button = document.getElementById("open_search");
		var button2 = document.getElementById("close_search");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	
	function close_search()
	{
		var myTarget2 = document.getElementById("search_section");
		var button = document.getElementById("open_search");
		var button2 = document.getElementById("close_search");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
</script>