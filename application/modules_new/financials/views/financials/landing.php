<div class="padd" style="margin-top:20px">   
    <h4 class="page-">  RECEIVABLES REPORTS</h4>
    <div class="row">
        <div class="col-md-6">

                <section class="panel panel-success">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Income by Customer</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/customer-income'?>">
                        <blockquote>
                            <p>See the income you received, broken down by source.</p>
                            <small>All<cite title="Source Title"> Invoices</cite></small>
                        </blockquote>
                      </a>
                    </div>
                </section>

        </div>
        <div class="col-md-6">

                <section class="panel panel-success">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Aged Receivables</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/aged-receivables'?>">
                        <blockquote>
                            <p>See how much money is expected to come in, and how long you've been waiting for it.</p>
                            <small>Pending<cite title="Source Title"> Invoices</cite></small>
                        </blockquote>
                        </a>
                    </div>

                </section>

        </div>
        <!-- ./col -->

    </div>
    <!-- /.row -->

    <h4 class="page-">PAYABLES REPORTS</h4>
    <div class="row">
        <div class="col-md-6">

                <section class="panel panel-warning">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Expense by Vendor</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/vendor-expenses'?>">
                        <blockquote>
                            <p>See what you paid in expenses, broken down by recipient.</p>
                            <small>All<cite title="Source Title"> Bills</cite></small>
                        </blockquote>
                        </a>
                    </div>
                    <!-- /.panel-body -->
                </section>


        </div>

        <!-- ./col -->
        <div class="col-md-6">

                <section class="panel panel-warning">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Aged Payables</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/aged-payables'?>">
                        <blockquote>
                            <p>See the expenses you haven't paid yet, and how long payment has been outstanding.</p>
                            <small>Pending<cite title="Source Title"> Bills</cite></small>
                        </blockquote>
                      </a>
                    </div>
                    <!-- /.panel-body -->
                </section>
                <!-- /.box -->

        </div>
    </div>


  <h4 class="page-">Financial Statements</h4>

   <div class="row">
        <div class="col-md-4">

                <section class="panel panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Business Budget</h5>
                        <div class="widget-icons pull-right">

                        </div>
                        <div class="clearfix"></div>
                    </header>
                    <div class="panel-body">
                        <!-- /.box-header -->
                        <a href="<?php echo site_url().'company-financials/budget'?>">

                            <blockquote>
                                <p>Business budgets based on estimates.</p>
                                <small>budget</small>
                            </blockquote>
                        </a>
                    </div>
                    <!-- /.panel-body -->
                </section>
            <!-- /.box -->

        </div>
        <!-- ./col -->

        <div class="col-md-4">

                <section class="panel  panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Business Budget (Actual)</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/budget-actual'?>">
                        <blockquote>
                            <p>Actual Business budget based on expediture in the clinic</p>
                            <small>Actual<cite title="Source Title"> Budget</cite></small>
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>

        <div class="col-md-4">

                <section class="panel  panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Business Budget VS Actual Analysis</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/budget-comparison'?>">
                        <blockquote>
                            <p>Comparison of estimated budget vs actual expenditure</p>
                            <small>Analysis<cite title="Source Title"> Budget</cite></small>
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>
        

    </div>
    <div class="row">
        <div class="col-md-3">

                <section class="panel panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Income Statement</h5>
                        <div class="widget-icons pull-right">

                        </div>
                        <div class="clearfix"></div>
                    </header>
                    <div class="panel-body">
                        <!-- /.box-header -->
                        <a href="<?php echo site_url().'company-financials/profit-and-loss'?>">

                            <blockquote>
                                <p>Income minus expenses; tells you if you brought in more than you spent this period.</p>
                                <small>Profit<cite title="Source Title"> & Loss</cite></small>
                            </blockquote>
                        </a>
                    </div>
                    <!-- /.panel-body -->
                </section>
            <!-- /.box -->

        </div>
        <!-- ./col -->

        <div class="col-md-3">

                <section class="panel  panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Balance Sheet</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/balance-sheet'?>">
                        <blockquote>
                            <p>Describes assets, liabilities and equity status of the company</p>
                            <small>Pending<cite title="Source Title"> Bills</cite></small>
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>
        <div class="col-md-3">

                <section class="panel  panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>SCHEDULE OF EXPENDITURE</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/schedule-of-expenditure'?>">
                        <blockquote>
                            <p>Describes assets, liabilities and equity status of the company</p>
                            <small>Pending<cite title="Source Title"> Bills</cite></small>
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>

        <div class="col-md-3">

                <section class="panel  panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>TRIAL BALANCE</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/trial-balance'?>">
                        <blockquote>
                            <p>Describes assets, liabilities and equity status of the company</p>
                            <small>Pending<cite title="Source Title"> Bills</cite></small>
                        </blockquote>
                      </a>
                    </div>
                </section>
                <!-- /.panel-body -->

            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">

                <section class="panel  panel-info">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Account Ledgers</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/accounts-ledgers'?>" style="text-decoration: none !important;">
                        <blockquote>
                            <p>General statements of bank accounts and statements from transactions completed.</p>
                            <small>Bank accounts<cite title="Source Title"> statements</cite></small>
                        </blockquote>
                      </a>
                    </div>
                    <!-- /.panel-body -->
                </section>


        </div>
    </div>
    <!-- ./col -->

    <h4 class="page-">Taxes</h4>

    <div class="row">
        <div class="col-md-6">

                <section class="panel">
                    <header class="panel-heading">
                        <h5 class="panel-title"><i class="icon-reorder"></i>Sales taxes</h5>
                        <div class="clearfix"></div>
                    </header>
                    <!-- /.box-header -->
                    <div class="panel-body">
                      <a href="<?php echo site_url().'company-financials/sales-taxes'?>">
                        <blockquote>
                            <p>See how much money is expected to come in, and how long you've been waiting for it.</p>
                            <small>Pending<cite title="Source Title"> Invoices</cite></small>
                        </blockquote>
                      </a>
                    </div>
                    <!-- /.panel-body -->
                </section>


        </div>
    </div>

   
    <!-- <h4 class="page-">Other</h4> -->
    <!-- /.row -->
    <!-- <div class="row">
        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    <h5 class="panel-title"><i class="icon-reorder"></i>General Ledger</h5>
                    <div class="clearfix"></div>
                </header>
                <div class="panel-body">
                  <a href="<?php echo site_url().'company-financials/general-ledger'?>">
                    <blockquote>
                        <p>General Ledger Statement.</p>
                        <small>All<cite title="Source Title"> General Ledger</cite></small>
                    </blockquote>
                  </a>
                </div>
            </section>
        </div>
        <div class="col-md-6">
              <section class="panel">
                  <header class="panel-heading">
                      <h5 class="panel-title"><i class="icon-reorder"></i>Account Transactions</h5>
                      <div class="clearfix"></div>
                  </header>
                  <div class="panel-body">
                    <a href="<?php echo site_url().'company-financials/account-transactions'?>">
                      <blockquote>
                          <p>See the transactions that occurred in each account.</p>
                          <small>summary<cite title="Source Title"> details</cite></small>
                      </blockquote>
                    </a>
                  </div>
              </section>
        </div>
    </div> -->
    <!-- /.row -->
</div>
