<?php 

$recon_id = $recon_id;


$recon_rs = $this->reconcilliation_model->get_recon_details($recon_id);

if($recon_rs->num_rows() > 0)  
{
	foreach ($recon_rs->result() as $key => $value) {
		# code...
		$account_id = $value->account_id;
		$account_name = $value->account_name;
		$recon_date = $value->recon_date;
		$start_date = $value->start_date;
		$opening_balance = $value->opening_balance;
		$interest_earned = $value->interest_earned;
		$service_charged = $value->service_charged;


	}
}

// var_dump($recon_id);die();
?>
<div class="row">
	<input type="hidden" name="recon_id" id="recon_id" value="<?php echo $recon_id;?>">
    <!-- <div class="col-md-12"> -->

        <section class="panel">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo ucfirst(strtolower($account_name)). ' For period starting'.date('jS M Y',strtotime($recon_date));?></h2>
                
            </header>

            <div class="panel-body">
				<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				?>
				<div class="col-md-12" style="height: 58vh">

					<div class="col-md-6" style="height: 100vh;overflow-y: scroll;">
						<h4>Cheques and Payments</h4>

						<div id="money-out-div"></div>
							
						
					</div>
					<div class="col-md-6" style="height: 100vh;overflow-y: scroll;">
						<h4>Deposits and Other Credits</h4>
						
						<div id="money-in-div"></div>
							
					</div>
				</div>
				<hr>
				<input type="hidden" name="ending_balance" id="ending_balance">
				<input type="hidden" name="cleared_balance" id="cleared_balance">
				<input type="hidden" name="difference" id="difference">
				<input type="hidden" name="total_deposits" id="total_deposits">
				<input type="hidden" name="total_cheques" id="total_cheques">
				<div class="col-md-12" style="height: 11vh">
					<div class="col-md-6">
						<div class="col-md-6">
							Begining Balance
						</div>
						<div class="col-md-6 align-right">
							<?php echo number_format($opening_balance,2)?>
						</div>
						<div class="col-md-12">
							Transactions you have marked cleared
						</div>
						<div class="col-md-6">
							Deposits and other credits
						</div>
						<div class="col-md-6 align-right">
							<div id="total-money-in-div"></div>
						</div>
						<div class="col-md-6">
							Cheques and Other Payments
						</div>
						<div class="col-md-6 align-right">
							<div id="total-money-out-div"></div>
						</div>
					</div>
					<div class="col-md-6" style="border: 1px solid grey;">
						<div class="col-md-6">
							Service Charged
						</div>
						<div class="col-md-6 align-right">
							<?php echo number_format($service_charged,2)?>
						</div>
						
						<div class="col-md-6">
							Interest Earned
						</div>
						<div class="col-md-6 align-right">
							<?php echo number_format($interest_earned,2)?>
						</div>
						<div class="col-md-6">
							Ending Balance
						</div>
						<div class="col-md-6 align-right">
							<div id="total-ending-balance"></div>
						</div>
						<div class="col-md-6">
							Cleared Balance
						</div>
						<div class="col-md-6 align-right">
							<div id="total-cleared-balance"></div>
						</div>
						<div class="col-md-6">
							Difference
						</div>
						<div class="col-md-6 align-right">
							<div id="total-difference"></div>
						</div>
					</div>
				</div>
				<hr>
				<div class="col-md-12" style="height: 6vh">
					<div class="" style="margin-top: 20px;">
						<a  onclick="complete_reconcilliation(<?php echo $recon_id;?>)" class="btn btn-sm btn-success pull-right"> RECONCILE NOW </a>
					</div>
				</div>

				
				
          	</div>
		</section>
    <!-- </div> -->
</div>
<script type="text/javascript">
	
$(document).ready(function()
{
	var recon_id = $('#recon_id').val();
	// alert(recon_id);
	get_money_out(recon_id);
	get_money_in(recon_id);
	get_total_money(recon_id);

	
});

function get_money_out(recon_id)
{
	
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/get_money_out/"+recon_id;
    
    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{account_payment_id: 1},
	    dataType: 'text',
	    success:function(data){
	    	// alert(data);
	        $("#money-out-div").html(data);

	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

    });
}

function get_money_in(recon_id)
{
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/get_money_in/"+recon_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        $("#money-in-div").html(data);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}


function get_total_money(recon_id)
{
	
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/get_total_recons/"+recon_id;
    
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){
    	// alert(data);
    	 var data = jQuery.parseJSON(data);
        $("#total-money-out-div").html(data.total_money_out);
        $("#total-money-in-div").html(data.total_money_in);
        
        $("#total-ending-balance").html(data.total_ending_balance);
        $("#total-difference").html(data.total_difference);
        $("#total-cleared-balance").html(data.total_cleared_balance);
        document.getElementById("ending_balance").value = data.ending_balance;
        document.getElementById("cleared_balance").value = data.cleared_balance;
        document.getElementById("difference").value = data.difference;
        document.getElementById("total_deposits").value = data.money_in;
        document.getElementById("total_cheques").value = data.money_out;
        


    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}
function mark_transaction_out(transaction_id,recon_id,type)
{
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/update_transaction/"+transaction_id+"/"+recon_id+"/"+type;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        get_money_out(recon_id);
		get_money_in(recon_id);
		get_total_money(recon_id);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}

function mark_transaction_in(transaction_id,recon_id,type)
{
	var config_url = $('#config_url').val();
    var data_url = config_url+"financials/reconcilliation/update_transaction_in/"+transaction_id+"/"+recon_id+"/"+type;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{account_payment_id: 1},
    dataType: 'text',
    success:function(data){

        get_money_out(recon_id);
		get_money_in(recon_id);
		get_total_money(recon_id);

    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}

function complete_reconcilliation(recon_id)
{
	var res = confirm('Are you sure you want to complate this reconcilliation');

	if(res)
	{

		var config_url = $('#config_url').val();
	    var data_url = config_url+"financials/reconcilliation/complete_reconcilliation/"+recon_id;
	    //window.alert(data_url);

	    var ending_balance = $('#ending_balance').val();
	    var cleared_balance = $('#cleared_balance').val();
	    var difference = $('#difference').val();
	    var total_deposits = $('#total_deposits').val();
	    var total_cheques = $('#total_cheques').val();

	    var difference = $('#difference').val();
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{ending_balance: ending_balance,cleared_balance: cleared_balance, difference: difference,total_deposits: total_deposits, total_cheques: total_cheques},
	    dataType: 'text',
	    success:function(data){
	    	var data = jQuery.parseJSON(data);

	    	if(data.message == 'success')
	    	{

	    		window.location.href = config_url+'accounting/bank-reconcilliation';

	    	}
	    	else
	    	{
	    		alert('Something went wrong. Please try again');
	    	}
	      

	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });
	}
}
</script>
