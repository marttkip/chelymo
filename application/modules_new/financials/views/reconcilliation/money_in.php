<?php 

$money_out_rs = $this->reconcilliation_model->get_money_in($recon_id);


$result = '<table class="table table-hover table-stripped table-condensed table-bordered ">
			 	<thead>
					<tr>
					  <th></th>
					  <th> DATE</th>
					  <th> CHQ #</th>
					  <th> PAYEE </th>
					  <th> AMOUNT </th>
					</tr>
				 </thead>
			  	<tbody>';
if($money_out_rs->num_rows() > 0)
{
	foreach ($money_out_rs->result() as $key => $value) {
		# code...
		$payment_date = $value->payment_date;
		$cheque = $value->cheque;
		$payee = $value->payee;
		$amount = $value->amount;
		$type = $value->type;
		$transactionid = $value->transactionid;
		$recon_idd = $value->recon_id;

		if($recon_idd > 0)
		{
			$marked = 'checked';
		}
		else
		{
			$marked ='';
		}

		 $checkbox_data = array(
                    'name'        => 'money_out_items[]',
                    'id'          => 'checkbox'.$transactionid,
                    'class'          => 'css-checkbox  lrg ',
                    'checked'=>$marked,
                    'value'       => $transactionid,
                    'onclick'=>'mark_transaction_in('.$transactionid.','.$recon_id.','.$type.')'
                  );
		$result .= '<tr>	
						 <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$transactionid.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td>'.$payment_date.'</td>
						<td>'.$cheque.'</td>
						<td>'.$payee.'</td>
						<td>'.number_format($amount,2).'</td>


					</tr>';
		

	}

}

$result .= '</tbody>
			</table>';

echo $result;

?>