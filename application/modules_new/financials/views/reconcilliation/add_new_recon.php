<?php



?>

<div class="col-md-12" style="margin-top:40px !important;">
	<section class="panel">
		<div class="panel-body">
			 <div class="padd">
		         <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form","id" => "edit-direct-payment"));?>
		            <div class="row">
		                <div class="col-md-12">
		                	 <input type="hidden" class="form-control" name="opening_balance" id="opening_balance" placeholder="Ending Balance" value="" required>
		                    <div class="form-group">
		                        <label class="col-lg-2 control-label">Parent Account</label>
		                        <div class="col-lg-4">
		                            <select id="account_id" name="account_id" class="form-control" onchange="get_account_reconcilliation(this.value)">
		                                <option value="0">--- Account ---</option>
		                                <?php
		                                if($accounts->num_rows() > 0)
		                                {   
		                                    foreach($accounts->result() as $row):
		                                        // $company_name = $row->company_name;
		                                        $account_name = $row->account_name;
		                                        $account_id = $row->account_id;
		                                        
		                                       	echo "<option value=".$account_id."> ".$account_name."</option>";
		                                        
		                                        
		                                        
		                                    endforeach; 
		                                } 
		                                ?>
		                            </select>
		                        </div>
		                        <div class="col-lg-6"> 
		                        	Last reconcilled on <span id="last-recon"></span> 
		                        </div>
		                    </div> 
		                    <div class="form-group">
		                        <label class="col-lg-2 control-label">Statement Date: </label>
		                        
		                        <div class="col-lg-4">
		                            <div class="input-group">
		                                <span class="input-group-addon">
		                                    <i class="fa fa-calendar"></i>
		                                </span>
		                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="recon_date" id="recon_date" placeholder="Last Recon Date" >
		                            </div>
		                        </div>
		                         <div class="col-lg-6"> 
		                        	
		                        </div>
		                    </div>  
		                    <div class="form-group">
		                        <label class="col-lg-2 control-label">Beginning Balance *</label>
		                       
		                        <div class="col-lg-4 text-align" > 
		                        	<div id="opening-balance"></div>
		                        </div>
		                        <div class="col-lg-6"> 
		                        	
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-lg-2 control-label">Ending Balance *</label>
		                        <div class="col-lg-4">
		                            <input type="number" class="form-control" name="ending_balance" id="ending_balance" placeholder="Ending Balance" value="" required>
		                        </div>
		                        <div class="col-lg-6"> 
		                        	
		                        </div>
		                    </div>

		                   
		                </div>
		                

		            </div>
		            <hr>
		            <div class="col-md-12">
		                
		                	Enter any service charge or interest earned 
		                
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                	<div class="col-md-12">
		                		<div class="col-md-4">
			                		<label class="col-lg-12">Service Charge *</label>
			                		<div class="col-lg-12"> 
			                        	<input type="text" class="form-control" name="service_charged" id="service_charged" placeholder="Ending Balance" value="" required>
			                        </div>
			                    </div>
			                    <div class="col-md-4">
			                    	<label class="col-lg-12">Date *</label>
			                		<div class="col-lg-12"> 
			                        	<div class="input-group">
			                                <span class="input-group-addon">
			                                    <i class="fa fa-calendar"></i>
			                                </span>
			                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="charged_date" id="charged_date" placeholder="Charged Date" value="">
			                            </div>
			                        </div>
			                    </div>
			                    <div class="col-md-4">
			                    	<label class="col-lg-12">Account *</label>
			                    	<div class="col-lg-12">
			                    		 <?php
			                                $changed = '<option value="">--- Account ---</option>';
			                                // var_dump($all_accounts->num_rows());die();
			                                $current_parent ='';
			                                 if($all_accounts->num_rows() > 0)
			                                 {
			                                     foreach($all_accounts->result() as $row):
			                                         // $company_name = $row->company_name;
			                                         $account_name = $row->account_name;
			                                         $account_id = $row->account_id;
			                                         $parent_account = $row->parent_account;

			                                         if($parent_account != $current_parent)
			                                         {
			                                         	  $account_from_name = $this->reconcilliation_model->get_account_name($parent_account);
			                                         	$changed .= '<optgroup label="'.$account_from_name.'">';
			                                         }

			                                       	 $changed .= "<option value=".$account_id."> ".$account_name."</option>";
			                                       	 $current_parent = $parent_account;
			                                       	 if($parent_account != $current_parent)
			                                         {
			                                         	$changed .= '</optgroup>';
			                                         }

			                                     	 
			                                     	
			                                     endforeach;
			                                 }
			                    		 ?>
			                             <select id="expense_account_id" name="expense_account_id" class="form-control">                                    
			                                <?php
			                                 echo $changed;
			                                 ?>
			                                  
											    
			                            </select>
			                    	</div>
			                    </div>
			                </div>
			                <div class="col-md-12">

			                    <div class="col-md-4">
			                		<label class="col-lg-12 ">Interest Earned *</label>
			                		<div class="col-lg-12"> 
			                        	<input type="text" class="form-control" name="interest_earned" id="interest_earned" placeholder="Interest Earned" value="" required>
			                        </div>
			                    </div>
			                    <div class="col-md-4">
			                    	<label class="col-lg-12">Date *</label>
			                		<div class="col-lg-12"> 
			                        	<div class="input-group">
			                                <span class="input-group-addon">
			                                    <i class="fa fa-calendar"></i>
			                                </span>
			                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="interest_date" id="interest_date" placeholder="Interest Earned Dtae" value="">
			                            </div>
			                        </div>
			                    </div>
			                    <div class="col-md-4">
			                    	<label class="col-lg-12">Account *</label>
			                    	<div class="col-lg-12">
			                    		 <select id="interest_account_id" name="interest_account_id" class="form-control">
			                               <?php
			                                	$changed = '<option value="">--- Account ---</option>';
			                                 if($all_accounts->num_rows() > 0)
			                                 {
			                                 	 $current_parent ='';
			                                     foreach($all_accounts->result() as $row):
			                                         // $company_name = $row->company_name;
			                                         $account_name = $row->account_name;
			                                         $account_id = $row->account_id;
			                                         $parent_account = $row->parent_account;

			                                         if($parent_account != $current_parent)
			                                         {
			                                         	  $account_from_name = $this->reconcilliation_model->get_account_name($parent_account);
			                                         	$changed .= '<optgroup label="'.$account_from_name.'">';
			                                         }

			                                       	 $changed .= "<option value=".$account_id."> ".$account_name."</option>";
			                                       	 $current_parent = $parent_account;
			                                       	 if($parent_account != $current_parent)
			                                         {
			                                         	$changed .= '</optgroup>';
			                                         }

			                                     	 
			                                     	
			                                     endforeach;
			                                 }
			                                 echo $changed;
			                                 ?>
			                            </select>
			                    	</div>
			                    </div>
		                	</div>
		                </div>
		            </div>

		            <hr>
		            <div class="row">
		                
		                <div class="col-md-12">
							<div class="col-md-8">
							</div>
							<div class="col-md-4">
								<button type="submit" class="btn btn-sm btn-info"> Continue to reconcile</button>
								<a onclick="close_side_bar()" class="btn btn-sm btn-default"> Cancel reconcilliation </a>
							</div>
		                </div>
		                
		            </div>
		            <?php echo form_close();?>
		        </div>
		</div>
	</section>
</div>
<br/>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
