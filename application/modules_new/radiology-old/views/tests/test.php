
    <section class="panel panel-featured panel-featured-info">
        <header class="panel-heading">
            <strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Patient Visit: </strong><?php echo $visit_type_name;?>.
            <?php
            if($inpatient == 1)
            {
            	?>
            		<a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top: -5px;"><i class="fa fa-arrow-left"></i> Back to Inpatient's Queue</a>
            	<?php	
            }
            else
            {
            	?>
            		<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top: -5px;"><i class="fa fa-arrow-left"></i> Back to Outpatient's Queue</a>
            	<?php
            }
            ?>
        </header>

        <div class="panel-body">
        	
            <div class="tabbable" style="margin-bottom: 18px;">
              <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#tests-pane" data-toggle="tab">Theatre Charges</a></li>
                <li ><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li>
              </ul>
              <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
                <div class="tab-pane active" id="tests-pane">
                	<div id="xray_results">
						<div class="row">
							<div class="col-md-12">
							    <section class="panel panel-featured panel-featured-info">
							        <header class="panel-heading">
							            <h2 class="panel-title">Theatre Charges</h2>
							        </header>
							        <div class="panel-body">
							            <div class="col-lg-8 col-md-8 col-sm-8">
							              <div class="form-group">
							                <select id='xray_id' name='xray_id' class='form-control custom-select ' >
							                  <option value=''>None - Please Select a Theatre Charge</option>
							                  <?php echo $xrays;?>
							                </select>
							              </div>
							            
							            </div>
							            <div class="col-lg-4 col-md-4 col-sm-4">
							              <div class="form-group">
							                  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_xray(<?php echo $visit_id;?>);"> Add a Theatre Charge</button>
							              </div>
							            </div>
							             <!-- visit Procedures from java script -->
							            
							            <!-- end of visit procedures -->
							        </div>
							        <div id="xray_table"></div>

							        <?php 
							        $data['visit_id'] = $visit_id;
							        // echo $this->load->view("radiology/tests/test1", $data, TRUE); 
							        ?>
							     </section>
							</div>
						</div>
					</div>
                    
                </div>
                 <div class="tab-pane" id="visit_trail">
                  <?php echo $this->load->view("nurse/patients/visit_trail", '', TRUE);?>
                </div>
              </div>
           </div>
			
		</div>
	</div>
</section>
   

  <script type="text/javascript">
	$(function() {
		$("#xray_id").customselect();
	});
	$(document).ready(function(){
	   get_test_results(100, <?php echo $visit_id?>);
	   get_xray_table(<?php echo $visit_id;?>);
	});
	function parse_xray(visit_id)
	{
		var xray_id = document.getElementById("xray_id").value;
		xray(xray_id, visit_id);
	}	   
	function xray(id, visit_id){
	     var XMLHttpRequestObject = false;
	         
	     if (window.XMLHttpRequest) {
	     
	         XMLHttpRequestObject = new XMLHttpRequest();
	     } 
	         
	     else if (window.ActiveXObject) {
	         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	     }
	     var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id+"/"+id;
	     // window.alert(url);
	     if(XMLHttpRequestObject) {
	                 
	         XMLHttpRequestObject.open("GET", url);
	                 
	         XMLHttpRequestObject.onreadystatechange = function(){
	             
	             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                 
	                document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	                //get_xray_table(visit_id);
	             }
	         }
	         
	         XMLHttpRequestObject.send(null);
	     }
	   }
   
	   function get_xray_table(visit_id){
	     var XMLHttpRequestObject = false;
	         
	     if (window.XMLHttpRequest) {
	     
	         XMLHttpRequestObject = new XMLHttpRequest();
	     } 
	         
	     else if (window.ActiveXObject) {
	         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	     }
	     var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
	     
	     if(XMLHttpRequestObject) {
	                 
	         XMLHttpRequestObject.open("GET", url);
	                 
	         XMLHttpRequestObject.onreadystatechange = function(){
	             
	             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                 
	                 document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	             }
	         }
	         
	         XMLHttpRequestObject.send(null);
	     }
	   }
  	function open_window_xray(test, visit_id){
	  var config_url = $('#config_url').val();
	  window.open(config_url+"radiology/xray/xray_list/"+test+"/"+visit_id,"Popup","height=1200, width=800, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
	}
	function get_test_results(page, visit_id){

	  var XMLHttpRequestObject = false;
	    
	  if (window.XMLHttpRequest) {
	  
	    XMLHttpRequestObject = new XMLHttpRequest();
	  } 
	    
	  else if (window.ActiveXObject) {
	    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  var config_url = $('#config_url').val();
	  if((page == 1) || (page == 65) || (page == 85)){
	    
	    url = config_url+"radiology/xray/test/"+visit_id;
	  }
	  
	  else if ((page == 75) || (page == 100)){
	    url = config_url+"radiology/xray/test1/"+visit_id;
	  }
	// alert(url);
	  if(XMLHttpRequestObject) {
	    if((page == 75) || (page == 85)){
	      var obj = window.opener.document.getElementById("test_results");
	    }
	    else{
	      var obj = document.getElementById("test_results");
	    }
	    XMLHttpRequestObject.open("GET", url);
	    
	    XMLHttpRequestObject.onreadystatechange = function(){
	    
	      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	  //window.alert(XMLHttpRequestObject.responseText);
	        obj.innerHTML = XMLHttpRequestObject.responseText;
			/* CL Editor */
			$(".cleditor").cleditor({
				width: "auto",
				height: "100%"
			});
	        if((page == 75) || (page == 85)){
	          window.close(this);
	        }
	        
	      }
	    }
	    XMLHttpRequestObject.send(null);
	  }
	}

	function save_xray_comment(id, visit_id)
	{
		var config_url = $('#config_url').val();
		
		var res = document.getElementById("xray_comment"+id).value;
		
		var data_url = config_url+"radiology/xray/save_xray_comment";
			
		$.ajax({
			type:'POST',
			url: data_url,
			data:{visit_charge_id: id, xray_visit_format_comments: res, visit_id: visit_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
	
		});
	}

	function save_result(visit_charge_id, visit_id){
		var config_url = $('#config_url').val();
		
		var result = document.getElementById("xray_result"+visit_charge_id).value;
		var data_url = config_url+"radiology/xray/save_result";
         	
        $.ajax({
			type:'POST',
			url: data_url,
			data:{visit_charge_id: visit_charge_id, result: result, visit_id: visit_id},
			dataType: 'text',
			success:function(data)
			{
				if(data == 'true')
				{
					alert('Comment saved successfully');
				}
				else
				{
					alert('Unable to save comment');
				}
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
	}
	
	function send_to_doc(visit_id){
	

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();

		var url = config_url+"radiology/xray/send_to_doctor/"+visit_id;
					
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/xray/xray_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}
	function finish_xray_test(visit_id){

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();
		var url = config_url+"radiology/xray/finish_xray_test/"+visit_id;
				
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/xray/xray_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}

	function save_comment(visit_charge_id){
		var config_url = $('#config_url').val();
		var comment = document.getElementById("test_comment").value;
        var data_url = config_url+"radiology/xray/save_comment/"+comment+"/"+visit_charge_id;
     
        // var comment_tab = $('#comment').val();//document.getElementById("vital"+vital_id).value;
         	
        $.ajax({
        type:'POST',
        url: data_url,
       // data:{comment: comment_tab},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       // alert(error);
        }

        });
	

		
	}
	function print_previous_test(visit_id, patient_id){
		var config_url = $('#config_url').val();
    	window.open(config_url+"radiology/xray/print_test/"+visit_id+"/"+patient_id,"Popup","height=900,width=1200,,scrollbars=yes,"+
                        "directories=yes,location=yes,menubar=yes," +
                         "resizable=no status=no,history=no top = 50 left = 100");
	}

	function delete_xray_cost(visit_charge_id, visit_id)
    {
     var res = confirm('Are you sure you want to delete this charge?');
     
     if(res)
     {
         var XMLHttpRequestObject = false;
         
         if (window.XMLHttpRequest) {
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
         
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var url = config_url+"radiology/xray/delete_cost/"+visit_charge_id+"/"+visit_id;
         
         if(XMLHttpRequestObject) {
             var obj = document.getElementById("xray_table");
             
             XMLHttpRequestObject.open("GET", url);
             
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     obj.innerHTML = XMLHttpRequestObject.responseText;
                     get_xray_table(visit_id);
                 }
             }
             XMLHttpRequestObject.send(null);
         }
     }
   }

  </script>