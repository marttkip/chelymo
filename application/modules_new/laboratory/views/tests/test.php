
 <section class="panel ">
	<header class="panel-heading">
        <div class="panel-title"><strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> GENDER: </strong> <?php echo $gender;?><strong> AGE: </strong>  <?php echo $patient_age;?>  <strong> Account Bal: </strong> Kes <?php echo number_format($account_balance, 2);?>
		</div>
		<div class="pull-right">
			<?php
			if($inpatient > 0)
			{
				?>
				<a href="<?php echo site_url();?>queues/inpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Inpatient Queue</a>
		
				<?php
			}
			else
			{
				?>
				<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
		
				<?php
			}
			?>
		</div>
    </header>

	<div class="panel-body">
	
		
		<div class="tabbable" style="margin-bottom: 18px;">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a href="#tests-pane" data-toggle="tab">Tests</a></li>
				<li><a href="#visits-test-pane" data-toggle="tab">Visit's Tests Done</a></li>
				<li ><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li>
			</ul>
			<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
			<div class="tab-pane active" id="tests-pane">
			  <div class="row">
				  <div class="col-md-12">
					  	<div class="col-lg-8 col-md-8 col-sm-8">
						  <div class="form-group">
							<select id='lab_test_id' name='lab_test_id' class='form-control custom-select '>
							  <option value=''>None - Please Select a Lab test</option>
							  <?php echo $lab_tests;?>
							</select>
						  </div>
						
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4">
						  <div class="form-group">
							  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_lab_test(<?php echo $visit_id;?>);"> Add Lab Test</button>
						  </div>
						</div>
					</div>
				</div>
				<div id="lab_table"></div>
				<div id="test_results"></div>
				
			</div>

			<div class="tab-pane" id="visits-test-pane">				 
				<div id="visit_results"></div>				
			</div>
			 <div class="tab-pane" id="visit_trail">
			  <?php echo $this->load->view("nurse/patients/visit_trail", '', TRUE);?>
			</div>
		  </div>
	   </div>
		
	</div>

</section>
<section class="panel panel-featured panel-featured-info">
	<header class="panel-heading">
		<h2 class="panel-title">Consumables</h2>
	</header>
	<div class="panel-body">
		<div class="col-lg-8 col-md-8 col-sm-8">
		  <div class="form-group">
			<select id='consumable_id' name='consumable_id' class='form-control custom-select '>
			<!-- <select class="form-control custom-select" id='consumable_id' name='consumable_id'> -->
			  <option value=''>None - Please Select a consumable</option>
			  <?php echo $consumables;?>
			</select>
		  </div>
		
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4">
		  <div class="form-group">
			  <button class="btn btn-sm btn-success"  onclick="parse_consumable_charge(<?php echo $visit_id;?>,1);"> Add Consumable</button>
		  </div>
		</div>
	   
	</div>
	 <!-- visit Procedures from java script -->
		<div id="consumables_to_patients"></div>
		<!-- end of visit procedures -->
 </section>
  <script type="text/javascript">
  	$(function() {
	    $("#lab_test_id").customselect();
	    $("#consumable_id").customselect();
	});
	  $(document).ready(function(){
	       get_test_results(100, <?php echo $visit_id?>);
		   get_lab_table(<?php echo $visit_id;?>);
		   get_visit_results(<?php echo $visit_id?>);
			display_visit_consumables(<?php echo $visit_id;?>);
			$(function() {
				$("#lab_test_id").customselect();
			});
	  });
	   function parse_lab_test(visit_id)
	  {
	    var lab_test_id = document.getElementById("lab_test_id").value;
	     lab(lab_test_id, visit_id);
	    
	  }
	  function lab(id, visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id+"/"+id;
	    // window.alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	               document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
	               get_lab_table(visit_id);
	               get_test_results(100, visit_id);
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}
	function get_lab_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>laboratory/confirm_lab_test_charge/"+visit_id;
		
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                      get_test_results(100, visit_id);

                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
  	function open_window_lab(test, visit_id){
	  var config_url = $('#config_url').val();
	  window.open(config_url+"laboratory/laboratory_list/"+test+"/"+visit_id,"Popup","height=1200, width=800, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
	}
	function get_test_results(page, visit_id){

	  var XMLHttpRequestObject = false;
	    
	  if (window.XMLHttpRequest) {
	  
	    XMLHttpRequestObject = new XMLHttpRequest();
	  } 
	    
	  else if (window.ActiveXObject) {
	    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  var config_url = $('#config_url').val();
	  if((page == 1) || (page == 65) || (page == 85)){
	    
	    url = config_url+"laboratory/test/"+visit_id;
	  }
	  
	  else if ((page == 75) || (page == 100)){
	    url = config_url+"laboratory/test1/"+visit_id;
	  }
		// alert(url);
	  if(XMLHttpRequestObject) {
	    if((page == 75) || (page == 85)){
	      var obj = window.opener.document.getElementById("test_results");
	    }
	    else{
	      var obj = document.getElementById("test_results");
	    }
	    XMLHttpRequestObject.open("GET", url);
	    
	    XMLHttpRequestObject.onreadystatechange = function(){
	    
	      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	  		//window.alert(XMLHttpRequestObject.responseText);
	        obj.innerHTML = XMLHttpRequestObject.responseText;
	        if((page == 75) || (page == 85)){
	          window.close(this);
	        }
	        
	      }
	    }
	    XMLHttpRequestObject.send(null);
	  }
	}


	function get_visit_results(visit_id){

	  var XMLHttpRequestObject = false;
	    
	  if (window.XMLHttpRequest) {
	  
	    XMLHttpRequestObject = new XMLHttpRequest();
	  } 
	    
	  else if (window.ActiveXObject) {
	    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  var config_url = $('#config_url').val();
	  
	   url = config_url+"laboratory/visit_results/"+visit_id;
		// alert(url);
	  if(XMLHttpRequestObject) {
	    
	    var obj = document.getElementById("visit_results");
	    
	    XMLHttpRequestObject.open("GET", url);
	    
	    XMLHttpRequestObject.onreadystatechange = function(){
	    
	      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	  		//window.alert(XMLHttpRequestObject.responseText);
	        obj.innerHTML = XMLHttpRequestObject.responseText;
	        
	      }
	    }
	    XMLHttpRequestObject.send(null);
	  }
	}


	function save_result_format(visit_lab_test_id, lab_test_format_id, visit_id,lab_visit_results_id)
	{
		var config_url = $('#config_url').val();
		
		var res = document.getElementById("laboratory_result2"+lab_visit_results_id).value;
		var data_url = config_url+"laboratory/save_result_lab";
         	// alert(data_url);
        $.ajax({
			type:'POST',
			url: data_url,
			data:{res: res, format: lab_test_format_id, visit_id: visit_id, visit_lab_test_id: visit_lab_test_id,lab_visit_results_id: lab_visit_results_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
        });
	}

	function save_lab_comment(id, visit_id)
	{
		var config_url = $('#config_url').val();
		
		var res = document.getElementById("laboratory_comment"+id).value;
		
		var data_url = config_url+"laboratory/save_lab_comment";
			
		$.ajax({
			type:'POST',
			url: data_url,
			data:{visit_charge_id: id, lab_visit_format_comments: res, visit_id: visit_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
	
		});
	}

	function save_result(id, visit_id){
		
		var config_url = $('#config_url').val();
		var res = document.getElementById("laboratory_result"+id).value;
        var data_url = config_url+"laboratory/save_result/"+id+"/"+res+"/"+visit_id;
   	 // window.alert(data_url);
         var result_space = $('#result_space'+id).val();//document.getElementById("vital"+vital_id).value;
         	
        $.ajax({
			type:'POST',
			url: data_url,
			data:{result: result_space},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+id).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
	
		
	}
	function send_to_doc(visit_id){
	

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();

		var url = config_url+"laboratory/send_to_doctor/"+visit_id;
					
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/laboratory/lab_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}
	function finish_lab_test(visit_id){

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();
		var url = config_url+"laboratory/finish_lab_test/"+visit_id;
				
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/laboratory/lab_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}

	function save_comment(visit_charge_id){
		var config_url = $('#config_url').val();
		var comment = document.getElementById("test_comment").value;
        var data_url = config_url+"laboratory/save_comment/"+comment+"/"+visit_charge_id;
     
        // var comment_tab = $('#comment').val();//document.getElementById("vital"+vital_id).value;
         	
        $.ajax({
        type:'POST',
        url: data_url,
       // data:{comment: comment_tab},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       // alert(error);
        }

        });
	

		
	}
	function update_lab_test_charge(visit_lab_test_id,visit_id){
		var config_url = $('#config_url').val();
		var lab_test_amount = document.getElementById("lab_test_price"+visit_lab_test_id).value;
		var charge_date = document.getElementById("charge_date"+visit_lab_test_id).value;
        var data_url = "<?php echo site_url();?>laboratory/update_lab_charge_amount/"+visit_lab_test_id+"/"+visit_id;

		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{amount: lab_test_amount,charge_date: charge_date},
		  dataType: 'text',
		  success:function(data){
		    window.alert("You have successfully updated the lab test amount");
		    
		   get_lab_table(visit_id);
		  //obj.innerHTML = XMLHttpRequestObject.responseText;
		  },
		  error: function(xhr, status, error) {
		  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		  alert(error);
		  }

		  });
		   get_lab_table(visit_id);
		
	}

	function print_previous_test(visit_id, patient_id){
		var config_url = $('#config_url').val();
    	window.open(config_url+"laboratory/print_test/"+visit_id+"/"+patient_id,"Popup","height=900,width=1200,,scrollbars=yes,"+
                        "directories=yes,location=yes,menubar=yes," +
                         "resizable=no status=no,history=no top = 50 left = 100");
	}
	function parse_consumable_charge(visit_id,suck)
    {
      var consumable_id = document.getElementById("consumable_id").value;
       // alert(consumable_id);
      consumable(consumable_id, visit_id,suck);

    }

    function consumable(id, visit_id,suck){
        
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>nurse/inpatient_consumables/"+id+"/"+visit_id+"/"+suck;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                   document.getElementById("consumables_to_patients").innerHTML = XMLHttpRequestObject.responseText;
                   //get_surgery_table(visit_id);
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
	function delete_consumable(id, visit_id)
	{
		var confirmation = confirm('Delete consumable?');
	
		if(confirmation)
		{
			var XMLHttpRequestObject = false;
				
			if (window.XMLHttpRequest) {
			
				XMLHttpRequestObject = new XMLHttpRequest();
			} 
				
			else if (window.ActiveXObject) {
				XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var config_url = document.getElementById("config_url").value;
			var url = config_url+"nurse/delete_consumable/"+id+"/"+visit_id;;
			
			if(XMLHttpRequestObject) {
						
				XMLHttpRequestObject.open("GET", url);
						
				XMLHttpRequestObject.onreadystatechange = function(){
					
					if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
					{
	            		document.getElementById("consumables_to_patients").innerHTML = XMLHttpRequestObject.responseText;
					}
				}
						
				XMLHttpRequestObject.send(null);
			}
		}
	}
	function display_visit_consumables(visit_id)
	{
		var config_url = document.getElementById("config_url").value;
		var url = config_url+"nurse/visit_consumables/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#consumables_to_patients").html(data);
		});
	}
	function calculateconsumabletotal(amount, id, procedure_id, v_id)
	{
		var units = document.getElementById('units'+id).value;  
		//alert(units);
		grand_consumable_total(id, units, amount, v_id);
	}
	function grand_consumable_total(vaccine_id, units, amount, v_id)
	{
		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = document.getElementById("config_url").value;
		
		var url = config_url+"nurse/consuamble_total/"+vaccine_id+"/"+units+"/"+amount;
		//alert(vaccine_id);
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					display_visit_consumables(v_id);
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}

  </script>