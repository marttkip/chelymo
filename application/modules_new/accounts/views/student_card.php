
    <section class="panel panel-featured panel-featured-info">
        <header class="panel-heading">
            <strong>Name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?>. <strong> Patient Visit: </strong><?php echo $visit_type_name;?>
            <a href="<?php echo site_url();?>accounts/print_invoice_new/<?php echo $visit_id?>" target="_blank" class="btn btn-warning btn-sm pull-right " style="margin-top: -5px;margin-left:5px;"><i class="fa fa-print"></i> Print Invoice</a>
           
            <a href="<?php echo site_url();?>queues/students-queue" class="btn btn-info btn-sm pull-right " style="margin-top: -5px;"><i class="fa fa-arrow-left"></i> Back to Students Queue</a>
            
            
        </header>

        <div class="panel-body">

    		<div id="ultrasound_results">
				<div class="row">
					<div class="col-md-12">
					    <section class="panel panel-featured panel-featured-info">
					        <header class="panel-heading">
					            <h2 class="panel-title">Charges</h2>
					        </header>
					        <div class="panel-body">
					            <div class="col-lg-6 col-md-6 col-sm-6">
					              <div class="form-group">
					              	<label class="col-md-2 control-label">Charge: </label>
                
                						<div class="col-md-10">
							                <select id='ultrasound_id' name='ultrasound_id' class='form-control custom-select ' >
							                  <option value=''>None - Please Select a charges Services</option>
							                  <?php echo $charge_list;?>
							                </select>
							         </div>
					              </div>
					            
					            </div>
					            <div class="col-lg-4 col-md-4 col-sm-4">
					              <div class="form-group">
					              	 <label class="col-md-2 control-label">Date: </label>
                
                						<div class="col-md-10">
					                	 <div class="input-group">
			                                <span class="input-group-addon">
			                                    <i class="fa fa-calendar"></i>
			                                </span>
			                                <input data-format="yyyy-MM-dd" type="text"  data-plugin-datepicker class="form-control" name="charge_date" id="charge_date" placeholder="charge date" value="<?php echo $visit_date?>">
			                            </div>
			                           </div>
					              </div>
					            </div>
					            <div class="col-lg-2 col-md-2 col-sm-2">
					              <div class="form-group">
					                  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_ultrasound(<?php echo $visit_id;?>);"> Add to Charge Sheet</button>
					              </div>
					            </div>
					             <!-- visit Procedures from java script -->
					            
					            <!-- end of visit procedures -->
					        </div>
					         <div id="ultrasound_table"></div>
					         <?php $data['visit_id'] = $visit_id?>
					     </section>
					</div>
				</div>
			</div>	
			<div class="center-align">
				<?php 
					// var_dump($module); die();
				echo '<a href="'.site_url().'accounts/end_student_visit/'.$visit_id.'" onclick="return confirm(\' Do you want to end this visit ?\');" class="btn btn-sm btn-success"> End Visit</a>'; ?>
				
			</div>
            
			
		</div>
	</div>
</section>
   

  <script type="text/javascript">
	  	$(function() {
	       $("#ultrasound_id").customselect();
	       $("#drug_id").customselect();
	   });
	  $(document).ready(function(){
	       get_test_results(100, <?php echo $visit_id?>);
		   get_ultrasound_table(<?php echo $visit_id;?>);
		   display_inpatient_prescription(<?php echo $visit_id;?>,0);
	  });
	 function get_ultrasound_table(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>accounts/student_bill_table/"+visit_id;
     // alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("ultrasound_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }

    function parse_ultrasound(visit_id)
   {

   var ultrasound_id = document.getElementById("ultrasound_id").value;
  var charge_date = document.getElementById("charge_date").value; //$('#charge_date').val();
   // alert(charge_date);
   ultrasound(ultrasound_id, visit_id,charge_date);
   
   }
   function ultrasound(id, visit_id,charge_date){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var url = "<?php echo site_url();?>accounts/student_bill_table/"+visit_id+"/"+id+"/"+charge_date;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("ultrasound_table").innerHTML = XMLHttpRequestObject.responseText;
                // get_ultrasound_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
  	function open_window_ultrasound(test, visit_id)
	{
		  var config_url = $('#config_url').val();
		  var win = window.open(config_url+"radiology/ultrasound/ultrasound_list/"+test+"/"+visit_id,"Popup","height=1200, width=800, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
		  win.focus();
	}
	function get_test_results(page, visit_id){

	  var XMLHttpRequestObject = false;
	    
	  if (window.XMLHttpRequest) {
	  
	    XMLHttpRequestObject = new XMLHttpRequest();
	  } 
	    
	  else if (window.ActiveXObject) {
	    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  var config_url = $('#config_url').val();
	  if((page == 1) || (page == 65) || (page == 85)){
	    
	    url = config_url+"radiology/ultrasound/test/"+visit_id;
	  }
	  
	  else if ((page == 75) || (page == 100)){
	    url = config_url+"radiology/ultrasound/test1/"+visit_id;
	  }
	// alert(url);
	  if(XMLHttpRequestObject) {
	    if((page == 75) || (page == 85)){
	      var obj = window.opener.document.getElementById("test_results");
	    }
	    else{
	      var obj = document.getElementById("test_results");
	    }
	    XMLHttpRequestObject.open("GET", url);
	    
	    XMLHttpRequestObject.onreadystatechange = function(){
	    
	      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	  //window.alert(XMLHttpRequestObject.responseText);
	        obj.innerHTML = XMLHttpRequestObject.responseText;
			/* CL Editor */
			$(".cleditor").cleditor({
				width: "auto",
				height: "100%"
			});
	        if((page == 75) || (page == 85)){
	          window.close(this);
	        }
	        
	      }
	    }
	    XMLHttpRequestObject.send(null);
	  }
	}

	function save_ultrasound_comment(id, visit_id)
	{
		var config_url = $('#config_url').val();
		
		var res = document.getElementById("ultrasound_comment"+id).value;
		
		var data_url = config_url+"radiology/ultrasound/save_ultrasound_comment";
			
		$.ajax({
			type:'POST',
			url: data_url,
			data:{visit_charge_id: id, ultrasound_visit_format_comments: res, visit_id: visit_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
	
		});
	}

	function save_result(visit_charge_id, visit_id){
		var config_url = $('#config_url').val();
		
		var result = document.getElementById("ultrasound_result"+visit_charge_id).value;
		var data_url = config_url+"radiology/ultrasound/save_result";
         	
        $.ajax({
			type:'POST',
			url: data_url,
			data:{visit_charge_id: visit_charge_id, result: result, visit_id: visit_id},
			dataType: 'text',
			success:function(data)
			{
				if(data == 'true')
				{
					alert('Comment saved successfully');
				}
				else
				{
					alert('Unable to save comment');
				}
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
	}
	
	function send_to_doc(visit_id){
	

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();

		var url = config_url+"radiology/ultrasound/send_to_doctor/"+visit_id;
					
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/ultrasound/ultrasound_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}
	function finish_ultrasound_test(visit_id){

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();
		var url = config_url+"radiology/ultrasound/finish_ultrasound_test/"+visit_id;
				
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/ultrasound/ultrasound_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}

	function save_comment(visit_charge_id){
		var config_url = $('#config_url').val();
		var comment = document.getElementById("test_comment").value;
        var data_url = config_url+"radiology/ultrasound/save_comment/"+comment+"/"+visit_charge_id;
     
        // var comment_tab = $('#comment').val();//document.getElementById("vital"+vital_id).value;
         	
        $.ajax({
        type:'POST',
        url: data_url,
       // data:{comment: comment_tab},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       // alert(error);
        }

        });
	

		
	}
	function print_previous_test(visit_id, patient_id){
		var config_url = $('#config_url').val();
    	window.open(config_url+"radiology/ultrasound/print_test/"+visit_id+"/"+patient_id,"Popup","height=900,width=1200,,scrollbars=yes,"+
                        "directories=yes,location=yes,menubar=yes," +
                         "resizable=no status=no,history=no top = 50 left = 100");
	}
	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       // alert("sdklahdhak");
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('unit_price'+id).value;  
     
	    grand_total(id, units, billed_amount, v_id);
	}
	function grand_total(procedure_id, units, amount, v_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
				{					
	   				get_ultrasound_table(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	    get_ultrasound_table(v_id);
	}

   function delete_ultrasound_cost(visit_charge_id, visit_id)
   {
     var res = confirm('Are you sure you want to delete this charge?');
     
     if(res)
     {
         var XMLHttpRequestObject = false;
         
         if (window.XMLHttpRequest) {
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
         
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = document.getElementById("config_url").value;
         var url = config_url+"radiology/ultrasound/delete_cost/"+visit_charge_id+"/"+visit_id;
         // alert(url);
         if(XMLHttpRequestObject) {
             var obj = document.getElementById("ultrasound_table");
             
             XMLHttpRequestObject.open("GET", url);
             
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     obj.innerHTML = XMLHttpRequestObject.responseText;
                     get_ultrasound_table(visit_id);
                 }
             }
             XMLHttpRequestObject.send(null);
         }
     }
   }



  </script>
  <!-- prescription items  -->


  <script type="text/javascript">
   function get_drug_to_prescribe(visit_id)
   {
   var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var drug_id = document.getElementById("drug_id").value;
   
     var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
   
      if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
               var prescription_view = document.getElementById("prescription_view");
              
               document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
                prescription_view.style.display = 'block';
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   
   
   }

    function display_inpatient_prescription(visit_id,module){
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }
    function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     // alert(url);
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,0);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }



   function button_update_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var url = "<?php echo base_url();?>pharmacy/update_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption},
   dataType: 'text',
   success:function(data){
   
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }
   
   function dispense_prescription(visit_id,visit_charge_id,prescription_id,module)
   {
   var quantity = $('#quantity'+prescription_id).val();
   var x = $('#x'+prescription_id).val();
   var duration = $('#duration'+prescription_id).val();
   var consumption = $('#consumption'+prescription_id).val();
   var charge = $('#charge'+prescription_id).val();
   var units_given = $('#units_given'+prescription_id).val();
   
   var url = "<?php echo base_url();?>pharmacy/dispense_inpatient_prescription/"+visit_id+"/"+visit_charge_id+"/"+prescription_id+"/"+module;
   
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption,charge: charge, units_given: units_given},
   dataType: 'text',
   success:function(data){
     window.alert(data.result);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   display_inpatient_prescription(visit_id,0);
   return false;
   }

    function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }


  function check_frequency_type()
   {
   
     var x = document.getElementById("x_value").value;
     var type_of_drug = document.getElementById("type_of_drug").value;
       
     var number_of_days = document.getElementById("number_of_days_value").value;
     var quantity = document.getElementById("quantity_value").value;
     var dose_value = document.getElementById("dose_value").value;

    var service_charge_id = document.getElementById("drug_id").value;
    var visit_id = document.getElementById("visit_id").value;
    var consumption = document.getElementById("consumption_value").value;

     if(x == "" || x == 0)
     {

       // alert("Please select the frequency of the medicine");
       x = 1;
     }
     
     if(number_of_days == "" || number_of_days == 0)
     {
        number_of_days = 1;
     }
     
         
      var url = "<?php echo base_url();?>pharmacy/get_values";
       $.ajax({
       type:'POST',
       url: url,
       data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
       dataType: 'text',
       success:function(data){
          var data = jQuery.parseJSON(data);

          var amount = data.amount;
          var frequency = data.frequency;
           var item = data.item;

          // {"message":"success","amount":"35","frequency":"5"}

          if(type_of_drug == 3)
          {
           
            var total_units = number_of_days * frequency * quantity;
            var total_amount =  number_of_days * frequency * amount * quantity;
          }
          else
          {

             var total_units =   quantity;
             var total_amount =   amount * quantity;
          }
        

        // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
         $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
         $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
         $( "#item_description" ).html("<p> "+ item +"</p>");

         document.getElementById("input-total-value").value = total_units;
         // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
   
        
     
  }
   
  </script>