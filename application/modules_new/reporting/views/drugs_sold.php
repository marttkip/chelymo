<?php
$drug_result = '';

$total_buyin_p = 0;
$total_service_amount = 0;
$total_qty_given = 0;
$total_visit_charge_amount =0;
$total_profit_margin = 0;
$grand_total = 0;

if($query->num_rows() > 0)
{
	$count = $page;
	$drug_result .='
			<table class="table table-hover table-bordered table-condensed table-striped table-responsive col-md-12" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Visit Date</th>
						<th>Patient Name</th>
						<th>Patient Type</th>
						<th>Drug</th>
						<th>Prescribed By</th>
						<th>Dispensed By</th>
						<th>Buying Price</th>
						<th>Units Given</th>
						<th>Selling Price</th>
						<th>Profit Margin</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
			';
	foreach($query->result() as $visit_drug_result)
	{
		$visit_id = $visit_drug_result->visit_id;
		$patient_id = $visit_drug_result->patient_id;
		$patient_surname = $visit_drug_result->patient_surname;
		$patient_othernames = $visit_drug_result->patient_othernames;
		$patient_id = $visit_drug_result->patient_id;
		$visit_type_name = $visit_drug_result->visit_type_name;
		$service_charge = $visit_drug_result->service_charge_name;
		$service_amount = $visit_drug_result->service_charge_amount;
		$visit_charge_amount = $visit_drug_result->visit_charge_amount;
		$branch_code = $visit_drug_result->branch_code;
		$patient_number = $visit_drug_result->patient_number;
		$visit_date = date('jS M Y',strtotime($visit_drug_result->visit_date));
		$qty_given = $visit_drug_result->units_given;
		$patient_type = $visit_drug_result->patient_type;
		if($patient_type == 1)
		{
		$patient_name = 'Walkin';

		}
		else
		{
		$patient_name = $patient_surname.' '.$patient_othernames;

		}
		$dispense_by = $visit_drug_result->b_first_name.' '.$visit_drug_result->b_onames;
		$prescribed_by = $visit_drug_result->a_first_name.' '.$visit_drug_result->a_onames;
		$count++;

		$buyin_p = $visit_charge_amount / 1.33 ;
		$profit_margin = $visit_charge_amount - $buyin_p;
		
	
		$total_amount_p= $buyin_p * $qty_given;
		$total_amount= $visit_charge_amount * $qty_given;

		//branch Code

		$total_buyin_p += $buyin_p;
		$total_service_amount += $service_amount;
		$total_qty_given +=  $qty_given;
		$total_visit_charge_amount += $visit_charge_amount;
		$total_profit_margin += $profit_margin;
		$grand_total += $total_amount;

		$total_amount_t = $total_amount - $total_amount_p;
	    $color = 'default';
	
		$drug_result .='
					<tr>
						<td>'.$count.'</td>
						<td>'.$visit_date.'</td>
						<td>'.$patient_name.'</td>
						<td>'.$visit_type_name.'</td>
						<td>'.$service_charge.'</td>
						<td>'.$prescribed_by.'</td>
						<td>'.$dispense_by.'</td>
						<td>'.number_format($buyin_p,2).'</td>
						<td>'.$qty_given.'</td>
						<td>'.number_format($visit_charge_amount,2).'</td>
						<td>'.number_format(($total_amount_t),2).'</td>
						<td>'.number_format(($total_amount),2).'</td>
					</tr>';
	}
	$drug_result .='
					<tr>
						<th colspan="7"> Totals</th>
						<th>'.number_format($total_buyin_p,2).'</th>
						<th>'.$total_qty_given.'</th>
						<th>'.number_format($total_visit_charge_amount,2).'</th>
						<th>'.number_format($total_profit_margin,2).'</th>
						<th>'.number_format($grand_total,2).'</th>
					</tr>';
	$drug_result.='
				</tbody>
			</table>';
}
else
{
	$drug_result.= 'No drugs have been dispensed';
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Creditors</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:11px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 10px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">    	       
      <div class="row" >
      		
        	<div class="col-md-12 center-align">
            	<h4><strong>DRUGS SALES FOR  <?php echo date('Y-m-d');?></strong></h4>
            </div>
					
        </div>
        
    	<div class="row">
        	<div class="col-md-12">
            	<?php echo $drug_result;?>
            </div>

        </div>
       
    </body>
    
</html>