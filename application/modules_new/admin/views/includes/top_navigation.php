<?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="http://preview.oklerthemes.com/porto-admin/" class="logo">
						<img src="<?php echo base_url().'assets/logo/'.$logo;?>" height="35" alt="<?php echo $company_name;?>" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>

				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
					<?php
					$department_id = $this->session->userdata('personnel_department_id');
					// $department_id = $this->reception_model->get_personnel_department($personnel_id);
					// var_dump($department_id); die();

					if($department_id == 7)
					{
						$shift_type = $this->session->userdata('shift_type');
						if($shift_type == 1)
						{
							?>
							
							<a class='btn btn-danger ' data-toggle='modal' data-target='#close_shift'  > <i class="fa fa-plus"></i> Close Shift </a>
							<?php
						}
						else
						{
							?>
							<a class='btn btn-success ' data-toggle='modal' data-target='#open_shift'  > <i class="fa fa-plus"></i> Open Shift </a>
							<?php
						}
					}
					?>
					


					<div class="modal fade bs-example-modal-lg" id="open_shift" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					                <h4 class="modal-title" id="myModalLabel">Starting Shift AT (<?php echo date('Y-m-d H:i A')?>)</h4>
					            </div>
					              <div class="modal-body">
					            <?php echo form_open("open-shift", array("class" => "form-horizontal"));?>
					          
					            	<div class="row">
					                	<div class='col-md-12'>
					                      	<div class="form-group">
												<label class="col-lg-4 control-label">MPESA COLLECTION : </label>
												<div class="col-lg-6">
													<input type="text" class="form-control" name="mpesa_amount" placeholder="" autocomplete="off">
												</div>											  
											</div>
											<div class="form-group">
					                      		<div class="form-group">
													<label class="col-lg-4 control-label">CASH COLLECTED : </label>
													<div class="col-lg-6">
														<input type="text" class="form-control" name="amount" placeholder="" autocomplete="off">
													</div>											  
												</div>
											</div>
											 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
											 <input type="hidden" class="form-control" name="personnel_id" placeholder="" autocomplete="off" value="<?php echo $this->session->userdata('personnel_id')?>">
											 <input type="hidden" class="form-control" name="shift_type" placeholder="" autocomplete="off" value="1">
					                   
					            <div class="modal-footer">
					            	<button type="submit" class='btn btn-info btn-sm' type='submit' onclick="return confirm('You are about to start the shift ')">Start Shift</button>
					                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					                
					            </div>
					              </div>
					    </div>
					            <?php echo form_close();?>
					      
					</div>
					</div>
				</div>
			</div>
					<div class="modal fade bs-example-modal-lg" id="close_shift" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					                <h4 class="modal-title" id="myModalLabel">Close Shift AT (<?php echo date('Y-m-d H:i A')?>)</h4>
					            </div>
					            <?php echo form_open("close-shift", array("class" => "form-horizontal"));?>
					            <div class="modal-body">
					            	<div class="row">
					                	<div class='col-md-12'>
					                		<div class="form-group">
												<label class="col-lg-4 control-label">EXPENSE AMOUNT : </label>
												<div class="col-lg-6">
													<input type="text" class="form-control" name="expense_amount" placeholder="" autocomplete="off">
												</div>											  
											</div>
					                		<div class="form-group">
												<label class="col-lg-4 control-label">MPESA COLLECTION : </label>
												<div class="col-lg-6">
													<input type="text" class="form-control" name="mpesa_amount" placeholder="" autocomplete="off">
												</div>											  
											</div>
					                      	<div class="form-group">
												<label class="col-lg-4 control-label">CASH COLLECTED : </label>
												<div class="col-lg-6">
													<input type="text" class="form-control" name="cash_amount" placeholder="" autocomplete="off">
												</div>											  
											</div>
											 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
											  <input type="hidden" class="form-control" name="personnel_id" placeholder="" autocomplete="off" value="<?php echo $this->session->userdata('personnel_id')?>">
											  <input type="hidden" class="form-control" name="shift_type" placeholder="" autocomplete="off" value="0">
					                    
					                    </div>
					                </div>
					            </div>
					            <div class="modal-footer">
					            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Close Shift</button>
					                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					                
					            </div>
					            <?php echo form_close();?>
					        </div>
					    </div>
					</div>
					<a  href="<?php echo site_url().$this->uri->uri_string();?>" class="btn btn-info" ><i class="fa fa-recycle"></i> Refresh</a>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo base_url().'assets/img/avatar.jpg';?>" alt="<?php echo $this->session->userdata('first_name');?>" class="img-circle" data-lock-picture="<?php echo base_url().'assets/img/avatar.jpg';?>" />
							</figure>
							<div class="profile-info" data-lock-name="<?php echo $this->session->userdata('first_name');?>" data-lock-email="<?php echo $this->session->userdata('email');?>">
								<span class="name">
									<?php 
									//salutation
									if(date('a') == 'am')
									{
										echo 'Good morning, ';
									}
									
									else if((date('H') >= 12) && (date('H') < 17))
									{
										echo 'Good afternoon, ';
									}
									
									else
									{
										echo 'Good evening, ';
									}
									echo $this->session->userdata('first_name');
									
									?>
                                </span>
								<span class="role"><?php echo $this->session->userdata('branch_code');?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo site_url()."dashboard";?>"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo site_url()."logout-admin";?>"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->