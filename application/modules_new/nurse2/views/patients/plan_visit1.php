
<div class="row">
    <div class="col-md-12">
          <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Plan</h2>
            </header>
            <div class="panel-body">
              <div class="padd">
                <div id="visit_plan_detail"></div>

                <table align='center' class='table table-striped table-hover table-condensed'>
                  <tr>
                    <th>Plan Name</th>
                    <th>Description</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td valign='top'>
                      <select id='plan_id' class='form-control'>
                        <?php
                        $plan_rs = $this->nurse_model->get_plan_details();
                        $num_plans = count($plan_rs);
                        if($num_plans > 0){
                          foreach ($plan_rs as $key_item):
                        
                          $plan_name = $key_item->plan_name;
                          $plan_id = $key_item->plan_id;
                          echo "<option value='".$plan_id."'>".$plan_name."</option>";
                          endforeach;
                        }
                        ?>
                    </select>
                    </td>
                        <td><textarea id='plan_description_value' class='cleditor' ></textarea></td>
                        <td><input type='button' class='btn btn-sm btn-success' value='Save' onclick='save_visit_plan_detail(<?php echo $visit_id?>)' /></td>
                    </tr>
                </table>
                <!-- end of vitals data -->
              </div>
            </div>
          </section>
    </div>
</div>