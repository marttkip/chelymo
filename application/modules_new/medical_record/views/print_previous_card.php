<?php

//patient details
//$visit_type = $patient['visit_type'];
// $patient_type = $patient['patient_type'];
// $patient_othernames = $patient['patient_othernames'];
// $patient_surname = $patient['patient_surname'];
// $patient_surname = $patient['patient_surname'];
// $patient_number = $patient['patient_number'];
// $gender = $patient['gender'];
// $patient_insurance_number = $patient['patient_insurance_number'];
// $account_balance = $patient['account_balance'];
// $inpatient = $patient['inpatient'];
// $visit_type_name = $patient['visit_type_name'];
// $visit_status = 0;

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
$data['visit_id'] = $v_data['visit_id'] = $visit_id;
$data['lab_test'] = 100;

if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['visit_history'] = 1;
//Symptoms
$rs_symptoms = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows_symptoms = count($rs_symptoms);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] = $this->nurse_model->get_notes(3, $visit_id);

$symptoms = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Objective findings
//$rs_objective_findings = $this->nurse_model->get_visit_objective_findings($visit_id);
//$num_rows_objective_findings = count($rs_objective_findings);

//$objective_findings = $this->load->view('medical_record/notes_history', $v_data, TRUE);
$v_data['query'] = $this->nurse_model->get_notes(4, $visit_id);

$objective_findings = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Assessment
$v_data['query'] = $this->nurse_model->get_notes(5, $visit_id);

$assessment = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Assessment
//Plan
$v_data['query'] = $this->nurse_model->get_visit_pan_detail($visit_id);
$plan = $this->load->view('medical_record/plan', $v_data, TRUE);





$v_data['query'] = $this->nurse_model->get_notes(7, $visit_id);

$presenting_complain = $this->load->view('medical_record/notes_history', $v_data, TRUE);

$v_data['query'] = $this->nurse_model->get_notes(8, $visit_id);

$review = $this->load->view('medical_record/notes_history', $v_data, TRUE);

//Plan
$v_data['query'] = $this->nurse_model->get_visit_pan_detail($visit_id);

$plan = $this->load->view('medical_record/plan', $v_data, TRUE);

// $v_data['query'] = $this->nurse_model->get_visit_pres_detail($visit_id);

// $pres = $this->load->view('nurse/inpatient/display_prescription', $v_data, TRUE);

$times_rs = $this->pharmacy_model->get_drug_times();
$num_times = count($times_rs);
$time_list = "<select name = 'x' class='form-control'>";

    foreach ($times_rs as $key_items):

        $time = $key_items->drug_times_name;
        $drug_times_id = $key_items->drug_times_id;
        
        if($drug_times_id == set_value('x'))
        {
            $time_list = $time_list."<option value='".$drug_times_id."' selected='selected'>".$time."</option>";
        }
        
        else
        {
            $time_list = $time_list."<option value='".$drug_times_id."'>".$time."</option>";
        }
    endforeach;
$time_list = $time_list."</select>";

//get consumption
$rs_cons = $this->pharmacy_model->get_consumption();
$num_cons = count($rs_cons);
$cons_list = "<select name = 'consumption' class='form-control'>";
    foreach ($rs_cons as $key_cons):

    $con = $key_cons->drug_consumption_name;
    $drug_consumption_id = $key_cons->drug_consumption_id;
        
    if($drug_times_id == set_value('consumption'))
    {
        $cons_list = $cons_list."<option value='".$drug_consumption_id."' selected='selected'>".$con."</option>";
    }
    
    else
    {
        $cons_list = $cons_list."<option value='".$drug_consumption_id."'>".$con."</option>";
    }
    endforeach;
$cons_list = $cons_list."</select>";

//get durations
$duration_rs = $this->pharmacy_model->get_drug_duration();
$num_duration = count($duration_rs);
$duration_list = "<select name = 'duration' class='form-control'>";
    foreach ($duration_rs as $key_duration):
    $durations = $key_duration->drug_duration_name;
    $drug_duration_id = $key_duration->drug_duration_id;
        
    if($drug_times_id == set_value('duration'))
    {
        $duration_list = $duration_list."<option value='".$drug_duration_id."' selected='selected'>".$durations."</option>";
    }
    
    else
    {
        $duration_list = $duration_list."<option value='".$drug_duration_id."'>".$durations."</option>";
    }
    endforeach;
$duration_list = $duration_list."</select>";


$rs = $this->pharmacy_model->select_prescription($visit_id);

?>

 <head>
        <title><?php echo $contacts['company_name'];?> | Print Patient Card</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        body{font-family:"Courier New", Courier, monospace}
        .receipt_spacing{letter-spacing:0px; font-size: 12px;}
        .center-align{margin:0 auto; text-align:center;}
        
        .receipt_bottom_border{border-bottom: #888888 medium solid;}
        .row .col-md-12 table {
            border:solid #000 !important;
            border-width:1px 0 0 1px !important;
            font-size:10px;
        }
        .row .col-md-12 th, .row .col-md-12 td {
            border:solid #000 !important;
            border-width:0 1px 1px 0 !important;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
        {
             padding: 2px;
             font-size: 12px;
        }
        
        .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 10px;}
        .title-img{float:left; padding-left:10px;}
        img.logo{max-height:70px; margin:0 auto;}
    </style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 center-align receipt_bottom_border">
                <strong>
                    <?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?><br/>
                </strong>
            </div>
        </div>
        </div>
        
        <!-- Patient Details -->
        <div class="row receipt_bottom_border" style="margin-bottom: 10px; font-weight: bold;">
            <div class="col-md-6">
                <div class="row"  style="margin-bottom: 10px; font-weight: bold;">
                    <div class="col-md-12">
                        
                        <div class="title-item" style="width: 100; height: 100;" =>Patient Name:</div>
                        
                        <?php echo $patient_surname; ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-item">Patient Number:</div> 
                        
                        <?php echo $patient_number; ?>

                    </div>
                </div>
                
                
            </div>
            
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-item">Att. Doctor:</div> 
                        
                        <?php echo $doctor; ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row receipt_bottom_border" style="margin-bottom: 20px; font-weight: bold;">
            <div class="col-md-12 center-align">
                <strong>PATIENTS NOTES</strong>
            </div>
            <td>
               <tr style="height:5px;"><?php echo $this->load->view("nurse/show_previous_vitals", $data, TRUE); ?></tr>
                <tr style="height:5px;" width="100"><?php echo $symptoms; ?></tr>
                <tr style="height:5px;"><?php echo $plan; ?></tr>
                <tr style="height:5px;"><?php echo $objective_findings; ?></tr>
                <tr style="height:5px;"><?php echo $review; ?></tr>
                <tr style="height:5px;"><?php echo $this->load->view("medical_record/diagnosis_history", $data, TRUE); ?></tr>
                <tr style="height:5px;"><?php echo $this->load->view("laboratory/tests/test1", $data, TRUE); ?></tr>
                  
                 <tr style="height:5px;"><?php echo $presenting_complain; ?></tr>
                </tr>
            </td>

        </div>

            <div class="row receipt_bottom_border">
            <div class="col-md-12 center-align">
                <strong>DRUGS PRESCRIBED</strong>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped col-md-12">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Drug Name</th>
                      <th>Prescription</th>
                      <th>Days</th>
                   <!--    <th>Units</th>
                      <th>Unit Cost</th>
                      <th>Total</th> -->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $temp_visit_charge_amount =0;
                   // $total_selected_drugs = count($selected_drugs);
                    $num_rows =count($rs);
                    $s=0;
                    if($num_rows > 0){
                    foreach ($rs as $key_rs):
                        //var_dump($key_rs->prescription_substitution);
                        $service_charge_id =$key_rs->product_id;
                        $checker_id = $key_rs->checker_id;
                        if(empty($checker_id))
                        {
                            $checker_id = $service_charge_id;
                        }
                        $frequncy = $key_rs->drug_times_name;
                        $id = $key_rs->prescription_id;
                        $date1 = $key_rs->prescription_startdate;
                        $date2 = $key_rs->prescription_finishdate;
                        $sub = $key_rs->prescription_substitution;
                        $duration = $key_rs->drug_duration_name;
                        $sub = $key_rs->prescription_substitution;
                        $duration = $key_rs->drug_duration_name;
                        $consumption = $key_rs->drug_consumption_name;
                        $quantity = $key_rs->prescription_quantity;
                        $medicine = $key_rs->product_name;
                        $charge = $key_rs->product_charge;
                        $visit_charge_id = $key_rs->visit_charge_id;
                        $number_of_days = $key_rs->number_of_days;
                        $units_given = $key_rs->units_given;


                        $rs2 = $this->pharmacy_model->get_drug($service_charge_id);
                        $doseunit = '';
                        foreach ($rs2 as $key_rs2 ):
                            $drug_type_id = $key_rs2->drug_type_id;
                            $admin_route_id = $key_rs2->drug_administration_route_id;
                            $doseunit = $key_rs2->unit_of_measure;
                            $drug_type_name = $key_rs2->drug_type_name;
                        endforeach;

                        if(!empty($admin_route_id)){
                            $rs3 = $this->pharmacy_model->get_admin_route2($admin_route_id);
                            $num_rows3 = count($rs3);
                            if($num_rows3 > 0){
                                foreach ($rs3 as $key_rs3):
                                    $admin_route = $key_rs3->drug_administration_route_name;
                                endforeach;
                            }
                        }
                        

                        $rsf = $this->pharmacy_model->select_invoice_drugs($visit_id,$service_charge_id);
                        $num_rowsf = count($rsf);
                        $sum_units = 0;
                        $visit_charge_amount = 0;
                        
                        foreach ($rsf as $key_price):
                            $sum_units = $key_price->visit_charge_units;
                            $visit_charge_amount = $key_price->visit_charge_amount;
                        endforeach;
                        
                        //display selected drugs
                      
                        
                               
                        $amoun=$visit_charge_amount* $sum_units ;
                        $total_visit_charge_amount=$amoun+$temp_visit_charge_amount;
                        $temp_visit_charge_amount=$total_visit_charge_amount; 
                        $s++;
                         ?>
                          
                                    <?php
                                
                            
                        
                        
                    
                        
                            $amoun=$visit_charge_amount* $sum_units ;
                            $total_visit_charge_amount=$amoun+$temp_visit_charge_amount;
                            $temp_visit_charge_amount=$total_visit_charge_amount; 
                            $s++;
                            ?>
                                <tr>
                                    <td><?php echo $s;?></td>
                                    <td><?php echo $medicine;?></td>
                                    <td><?php echo $consumption;?> - <?php echo $duration;?> - <?php echo $frequncy;?></td>
                                    <td><?php echo $number_of_days;?></td>
                                  
                            <?php
                        
                        endforeach;

                        ?>

                        
                    </tbody>
               </table>
            </div>
        </div>
        <?php
        $this->session->unset_userdata('selected_drugs');
        }
        
        if($doctor == '-')
        {

        }
        else
        {
        ?>
            <div class="row" style="font-style:bold; font-size:11px;">
                <div class="col-md-10 pull-left">
                    <div class="col-md-6 pull-left">
                        <p>Prescribed By: </p>
                        <p><strong>Dr. <?php echo $doctor;?></strong></p>
                        <br>

                        <p>Sign : ................................ </p>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

       
        
       
        
        <div class="" style="font-style:italic; font-size:20px;font-weight: bold;">
            <div style="float:left; margin:0 10px 0 10px;">
                Served by: <?php echo $served_by; ?>
            </div>
            <div style="float:right; margin:0 10px 0 10px;">
                <?php echo $today; ?> Thank you
            </div>
        </div>
    </body>
    
</html>
<