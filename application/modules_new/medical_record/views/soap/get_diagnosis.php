<?php 

$rs = $this->nurse_model->get_diagnosis($visit_id);
$rs2 = $this->nurse_model->get_visit_icd10($visit_id);
$num_rows = count($rs);
$num_rows2 = count($rs2);
//echo $num_rows;

echo "<div class='row'>";
if($num_rows > 0)
{
	echo
	"
	<div class='col-md-6'>
		<table class='table table-striped table-hover table-condensed'>
			<tr>
				<th>Code</th>
				<th>Disease</th>
				<th></th>
			</tr>";

			foreach ($rs as $key):
				$diagnosis_id = $key->diagnosis_id;
				$name = $key->diseases_name;
				$code = $key->diseases_code;
				
				echo "
				<tr>
					<td>".$code."</td>
					<td>".$name."</td>
					<td>
						<div class='btn-toolbar'>
							<div class='btn-group'>
								<a class='btn btn-danger btn-sm delete_diagnosis' href='".$diagnosis_id."' id='".$visit_id."'><i class='fa fa-trash'></i></a>
							</div>
						</div>
					</td>
				</tr>";
			endforeach;
		echo"</table>
	</div>
 	";
}

if($num_rows2 > 0)
{
	echo
	"
	<div class='col-md-6'>
		<table class='table table-striped table-hover table-condensed'>
			<tr>
				<th>Code</th>
				<th>Disease</th>
				<th></th>
			</tr>";

			foreach ($rs2 as $key):
				$visit_icd10_id = $key->visit_icd10_id;
				$name = $key->icd10_name;
				$code = $key->icd10_code;
				
				echo "
				<tr>
					<td>".$code."</td>
					<td>".$name."</td>
					<td>
						<div class='btn-toolbar'>
							<div class='btn-group'>
								<a class='btn btn-danger btn-sm delete_visit_icd10' href='".$visit_icd10_id."' id='".$visit_id."'><i class='fa fa-trash'></i></a>
							</div>
						</div>
					</td>
				</tr>";
			endforeach;
		echo"</table>
	</div>
 	";
}
echo "</div>";
?>