<?php

if($query->num_rows() > 0)
{
	$notes_type_name = '';
	$result = '';
	
	foreach($query->result() as $row)
	{
		$personnel_fname = $row->personnel_fname;
		$notes_id = $row->notes_id;
		$notes_name = $row->notes_name;
		$notes_type_id = $row->notes_type_id;
		$notes_type_name = $row->notes_type_name;
		$notes_signature = $row->notes_signature;
		$created_by = $row->created_by;
		$notes_date = date('jS M Y',strtotime($row->notes_date));
		$notes_time = date('H:i a',strtotime($row->notes_time));
		
		$created_check = $mobile_personnel_id;
		$actions = '<td></td><td></td>';
		
		$result .= '
		<tr>
			<td>'.$notes_date.'</td>
			<td>'.$notes_time.'</td>
			<td>'.$notes_name.'</td>
			<td>'.$personnel_fname.'</td>
		</tr>
		';
	}
	
	if($notes_type_id < 3)
	{
		echo '<h4>'.$notes_type_name.'</h4>';
	}
	echo '
	<table class="table table-responsive table-striped table-condensed table-bordered">
		<tr>
			<th>Date</th>
			<th>Time</th>
			<th>Notes</th>
			<th>By</th>
		</tr>
		'.$result.'
	</table>
	';
}
?>