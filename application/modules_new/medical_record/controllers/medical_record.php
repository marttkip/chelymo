<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Medical_record  extends MX_Controller
{	
	var $signature_path;
	var $signature_location;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('medical_record_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('reception/reception_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('reception/database');
		$this->load->model('medical_admin/medical_admin_model');
		$this->load->model('pharmacy/pharmacy_model');
		$this->load->model('administration/administration_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('inventory_management/inventory_management_model');
		$this->load->model('laboratory/lab_model');
		$this->load->model('radiology/xray_model');
		$this->load->model('radiology/ultrasound_model');
		$this->load->model('radiology/ultrasound_model');
		// $this->load->model('doctor/leave_model');
		$this->load->model('doctor/doctor_model');
		$this->load->model('theatre/theatre_model');
		$this->signature_path = realpath(APPPATH . '../assets/signatures');
		$this->signature_location = base_url().'assets/signatures/';
		
		$this->signature_path = realpath(APPPATH . '../assets/signatures');
		$this->signature_location = base_url().'assets/signatures/';
		
		//removed because doctors loose notes
		/*$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}*/
	}
	
	public function patient_card($visit_id,$page_name)
	{
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$v_data['visit_ward_id'] = $patient['ward_id'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$visit_date = $this->reception_model->get_visit_date($visit_id);
		$gender = $patient['gender'];
		$visit_date = date('jS M Y',strtotime($visit_date));
		
		$v_data['age'] = $age;
		$v_data['visit_date'] = $visit_date;
		$v_data['gender'] = $gender;
		$v_data['visit_id'] = $visit_id;
		$v_data['module'] = $page_name;
		$v_data['wards'] = $this->reception_model->get_wards();
		$v_data['ward_rooms'] = $this->nurse_model->get_ward_rooms($v_data['visit_ward_id']);
		$v_data['visit_bed'] = $this->nurse_model->get_visit_bed($visit_id);
		
		$data['title'] = 'Patient Card';
		$data['content'] = $this->load->view('patient_card', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function vitals($visit_id,$patient_id)
	{
		//get visit type
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}
		
		//procedures for that visit type
		$order = 'service_charge.service_charge_name';
		$where = 'service_charge.service_id = service.service_id AND (service.service_name = "Procedure" OR service.service_name = "procedure" OR service.service_name = "procedures" OR service.service_name = "Procedures" ) AND service.department_id = departments.department_id AND departments.department_name = "General practice" AND service.branch_code = "OSH" AND service_charge.visit_type_id = visit_type.visit_type_id  AND service_charge.visit_type_id = '.$visit_t;

		$table = 'service_charge,visit_type,service, departments';
		$config["per_page"] = 0;
		$procedure_query = $this->nurse_model->get_other_procedures($table, $where, $order);

		$rs9 = $procedure_query->result();
		$procedures = '';
		
		foreach ($rs9 as $rs10) :
		$procedure_id = $rs10->service_charge_id;
		$proced = $rs10->service_charge_name;
		$visit_type = $rs10->visit_type_id;
		$visit_type_name = $rs10->visit_type_name;
		$stud = $rs10->service_charge_amount;
		$procedures .="<option value='".$procedure_id."'>".$proced." KES.".$stud."</option>";
		endforeach;
		
		//vaccines for that visit type
		$vaccine_order = 'service_charge.service_charge_name';
		$vaccine_where = 'service_charge.service_id = service.service_id AND (service.service_name = "vaccine" OR service.service_name = "Vaccination") AND service_charge.visit_type_id = visit_type.visit_type_id  AND service_charge.visit_type_id = '.$visit_t;
		$vaccine_table = 'service_charge,visit_type,service';
		$vaccine_query = $this->nurse_model->get_inpatient_vaccines_list($vaccine_table, $vaccine_where, $vaccine_order);
		$rs10 = $vaccine_query->result();
		$vaccines = '';
		foreach ($rs10 as $vaccine_rs) :
		  $vaccine_id = $vaccine_rs->service_charge_id;
		  $vaccine_name = $vaccine_rs->service_charge_name;
		  $visit_type_name = $vaccine_rs->visit_type_name;
		  $vaccine_price = $vaccine_rs->service_charge_amount;
		  $vaccines .="<option value='".$vaccine_id."'>".$vaccine_name." KES.".$vaccine_price."</option>";
		endforeach;
		
		//consumables
		$consumble_where = 'service_charge.product_id = product.product_id AND category.category_id = product.category_id AND category.category_name = "Consumable" AND service_charge.visit_type_id = '.$visit_t;
		$consumable_table = 'service_charge,product,category';
		
		$store_priviledges = $this->inventory_management_model->get_assigned_stores();
		if($store_priviledges->num_rows() > 0)
		{
			$consumble_where .= ' AND store_product.store_id = store.store_id AND product.product_id = store_product.product_id 
AND store.store_parent > 0 AND store_product.store_id IN (';
			$consumable_table .=',store_product, store';
			$count = 0;
			foreach ($store_priviledges->result() as $key) 
			{
				# code...
				$store_parent = $key->store_parent;
				$store_id = $key->store_id;
				$count ++;
				if($count == $store_priviledges->num_rows())
				{
					$consumble_where .= ' '.$store_id;
				}
				else
				{
					$consumble_where .= ' '.$store_id.',';
				}
				
			}
			$consumble_where .=')';
		}
		$consumable_order = 'service_charge.service_charge_name';
		$consumable_query = $this->nurse_model->get_inpatient_consumable_list($consumable_table, $consumble_where, $consumable_order);
		$rs8 = $consumable_query->result();
		$consumables = '';
		foreach ($rs8 as $consumable_rs) :
		$consumable_id = $consumable_rs->service_charge_id;
		$consumable_name = $consumable_rs->service_charge_name;
		$consumable_name_stud = $consumable_rs->service_charge_amount;
		    $consumables .="<option value='".$consumable_id."'>".$consumable_name." KES.".$consumable_name_stud."</option>";
		endforeach;
		
		
		$v_data['visit_id'] = $visit_id;
		$v_data['procedures'] = $procedures;
		$v_data['vaccines'] = $vaccines;
		$v_data['consumables'] = $consumables;
		$v_data['title'] = $data['title'] = 'Vitals';
		$this->load->view('vitals', $v_data);
	}
	public function lifestyle($visit_id,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['title'] = $data['title'] = 'LifeStyle';
		$this->load->view('lifestyle', $v_data);
	}
	public function previous_vitals($visit_id,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['title'] = $data['title'] = 'Previous Vitals';
		$data['content'] = $this->load->view('previous_vitals', $v_data);
	}
	public function patient_history($visit_id,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['query'] = $this->nurse_model->get_patient_history($patient_id);
		$v_data['title'] = $data['title'] = 'Patient History';
		$data['content'] = $this->load->view('patient_history', $v_data);
	}
	
	public function icd10_diagnosis($visit_id)
	{
		//ICD10 Diseases
		$icd10_order = 'icd10_id';
		$icd10_where = 'icd10_id > 0 AND icd10_id NOT IN (SELECT icd10_id FROM visit_icd10 WHERE visit_id = '.$visit_id.')';
		$icd10_table = 'icd10';
		
		$query = $this->nurse_model->get_inpatient_diseases($icd10_table, $icd10_where,$icd10_order);

		$rs9 = $query->result();
		$icd10 = '';
		
		foreach ($rs9 as $rs10) :
			$icd10_name = $rs10->icd10_name;
			$icd10_id = $rs10->icd10_id;
			$icd10_code = $rs10->icd10_code;

			$icd10 .="<option value='".$icd10_id."'>".$icd10_name." Code: ".$icd10_code." </option>";
		endforeach;
		$v_data['icd10'] = $icd10;
		$v_data['title'] = $data['title'] = 'ICD10 Diagnosis';
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('medical_record/soap/icd10_diagnosis', $v_data, true);
		$this->load->view('admin/templates/no_sidebar', $data);
	}
	public function moh_diagnosis($visit_id)
	{
		//MOH Diseases
		$moh_order = 'diseases_id';
		$moh_where = 'diseases_id > 0 AND diseases_id NOT IN (SELECT disease_id FROM diagnosis WHERE visit_id = '.$visit_id.')';
		$moh_table = 'diseases';
		
		$query = $this->nurse_model->get_inpatient_diseases($moh_table, $moh_where,$moh_order);

		$rs9 = $query->result();
		$moh = '';
		
		foreach ($rs9 as $rs10) :
			$moh_name = $rs10->diseases_name;
			$moh_id = $rs10->diseases_id;
			$moh_code = $rs10->diseases_code;

			$moh .="<option value='".$moh_id."'>".$moh_name." Code: ".$moh_code." </option>";
		endforeach;
		$v_data['moh'] = $moh;
		$v_data['visit_id'] = $visit_id;
		$v_data['title'] = $data['title'] = 'MoH Diagnosis';
		
		$data['content'] = $this->load->view('medical_record/soap/moh_diagnosis', $v_data, true);
		
		$this->load->view('admin/templates/no_sidebar', $data);
	}
	
	public function soap($visit_id, $patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['dental'] = 0;
		$v_data['patient_id'] = $patient_id;
		$v_data['title'] = $data['title'] = 'SOAP';
		$this->load->view('soap', $v_data);
	}
	public function leave($visit_id,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['title'] = $data['title'] = 'Leave';
		$this->load->view('leave', $v_data);
	}
	public function medical_checkup($visit_id,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['title'] = $data['title'] = 'Medical Checkup';
		$this->load->view('medical_checkup', $v_data);
	}
	public function visit_trail($visit_id,$patient_id)
	{
		$v_data['visit_id'] = $visit_id;
		$v_data['patient_id'] = $patient_id;
		$v_data['title'] = $data['title'] = 'Visit Trail';
		$data['content'] = $this->load->view('visit_trail', $v_data);
	}
	function get_diagnosis($visit_id)
	{
		$data = array('visit_id'=>$visit_id);
		$this->load->view('soap/get_diagnosis',$data);
	}
	public function open_lab_tests($visit_id)
	{
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}
		//Lab Tests
		$lab_test_order = 'service_charge_name';
		$lab_test_where = 'service_charge.service_charge_name = lab_test.lab_test_name AND lab_test_class.lab_test_class_id = lab_test.lab_test_class_id  AND service_charge.service_id = service.service_id AND (service.service_name = "Lab" OR service.service_name = "lab" OR service.service_name = "Laboratory" OR service.service_name = "laboratory" OR service.service_name = "Laboratory test")  AND  service_charge.visit_type_id = '.$visit_t.' AND service_charge.service_charge_id NOT IN (SELECT service_charge_id FROM visit_lab_test WHERE visit_id = '.$visit_id.')';
		$lab_test_table = '`service_charge`, lab_test_class, lab_test, service';
		
		$query = $this->nurse_model->get_inpatient_diseases($lab_test_table, $lab_test_where,$lab_test_order);

		$rs9 = $query->result();
		$lab = '';
		
		foreach ($rs9 as $rs10) :
			$lab_test_name = $rs10->service_charge_name;
			$lab_test_id = $rs10->service_charge_id;
			$service_charge_amount = $rs10->service_charge_amount;

			$lab .="<option value='".$lab_test_id."'>".$lab_test_name." Kes: ".$service_charge_amount." </option>";
		endforeach;
		$v_data['lab'] = $lab;
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('medical_record/soap/lab_tests', $v_data, true);
		
		$data['title'] = 'Lab Tests';
		$this->load->view('admin/templates/no_sidebar', $data);
	}
	public function open_xray($visit_id)
	{
		//XRAY
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}
		$xray_order = 'service_charge_name';
		$xray_where = 'service_charge.service_id = service.service_id AND (service.service_name = "X Ray" OR service.service_name = "xray" OR service.service_name = "XRay" OR service.service_name = "xray" OR service.service_name = "Xray")  AND  service_charge.visit_type_id = '.$visit_t;
		$xray_table = '`service_charge`, service';
		$xray_query = $this->xray_model->get_inpatient_xrays($xray_table, $xray_where, $xray_order);

		$rs12 = $xray_query->result();
		$xrays = '';
		foreach ($rs12 as $xray_rs) :
		  $xray_id = $xray_rs->service_charge_id;
		  $xray_name = $xray_rs->service_charge_name;
		  $xray_price = $xray_rs->service_charge_amount;
		  $xrays .="<option value='".$xray_id."'>".$xray_name." KES.".$xray_price."</option>";
		endforeach;
		
		$v_data['xrays'] = $xrays;
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('medical_record/soap/xrays', $v_data, true);
		
		$data['title'] = 'XRays';
		$this->load->view('admin/templates/no_sidebar', $data);
	}
	public function open_ultrasound($visit_id)
	{
		//XRAY
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}
		$ultra_sound_order = 'service_charge_name';
		$ultra_sound_where = 'service_charge.service_id = service.service_id AND (service.service_name = "Ultrasound" OR service.service_name = "ultrasound") AND  service_charge.visit_type_id = '.$visit_t;
		$ultra_sound_table = '`service_charge`, service';

		$ultra_sound_query = $this->ultrasound_model->get_inpatient_ultrasounds($ultra_sound_table, $ultra_sound_where, $ultra_sound_order);
		$rs13 = $ultra_sound_query->result();
		$ultrasound = '';
		foreach ($rs13 as $ultra_sound_rs) :
			$ultra_sound_id = $ultra_sound_rs->service_charge_id;
			$ultra_sound_name = $ultra_sound_rs->service_charge_name;
			$ultra_sound_price = $ultra_sound_rs->service_charge_amount;
			$ultrasound .="<option value='".$ultra_sound_id."'>".$ultra_sound_name." KES.".$ultra_sound_price."</option>";
		endforeach;
		
		$v_data['ultrasound'] = $ultrasound;
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('medical_record/soap/ultra_sound', $v_data, true);
		
		$data['title'] = 'Ultra Sound';
		$this->load->view('admin/templates/no_sidebar', $data);
	}
	public function open_drugs($visit_id)
	{
		$rs = $this->nurse_model->check_visit_type($visit_id);
		if(count($rs)>0){
		  foreach ($rs as $rs1) {
		    # code...
		      $visit_t = $rs1->visit_type;
		  }
		}
		//Drugs
		$drugs_order = 'product.product_name'; 
		$drugs_where = 'product.product_id = service_charge.product_id  AND service_charge.service_id = service.service_id AND (service.service_name = "Pharmacy" OR service.service_name = "pharmacy") AND service_charge.visit_type_id = '.$visit_t.' AND product.product_id NOT IN (SELECT service_charge.product_id FROM service_charge, pres WHERE pres.visit_id = '.$visit_id.' AND service_charge.service_charge_id = pres.service_charge_id)';
		$drugs_table = 'product, service_charge, service';
		$drug_query = $this->pharmacy_model->get_inpatient_drugs($drugs_table, $drugs_where, $drugs_order);

		$rs15 = $drug_query->result();
		$drugs = '';
		foreach ($rs15 as $drug_rs) :
			$drug_id = $drug_rs->service_charge_id;
			$drug_name = $drug_rs->service_charge_name;
			$brand_name = $drug_rs->brand_name;
			$drug_price = $drug_rs->service_charge_amount;
			$product_balance = $drug_rs->product_balance;
			if(empty($product_balance))
			{
				$product_balance = 0;
			}
			//$stock_level = $this->nurse_model->get_stock_level($drug_id);
			
			$drugs .="<option value='".$drug_id."'>".$drug_name." Stock::".$product_balance."</option>";
		endforeach;
		$v_data['drugs'] = $drugs;
		$v_data['visit_id'] = $visit_id;
		$data['content'] = $this->load->view('medical_record/soap/drugs', $v_data, true);
		
		$data['title'] = 'Add Prescription';
		$this->load->view('admin/templates/no_sidebar', $data);
	}
	
	public function get_lab_test_results($visit_id)
	{
		$data['visit_id'] = $visit_id;
		echo $this->load->view("laboratory/tests/test2", $data, TRUE);
	}
	
	public function previous_card($visit_id)
	{
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_age'] = $patient['patient_age'];
		$v_data['patient_id'] = $patient['patient_id'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$personnel_id = $this->session->userdata('personnel_id');
		$v_data['mobile_personnel_id'] = $personnel_id;
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$visit_date = $this->reception_model->get_visit_date($visit_id);
		$gender = $patient['gender'];
		$visit_date = date('jS M Y',strtotime($visit_date));
		$v_data['age'] = $age;
		$v_data['visit_date'] = $visit_date;
		$v_data['gender'] = $gender;
		
		$v_data['visit_id'] = $visit_id;
		
		$data['content'] = $this->load->view('previous_card', $v_data, true);
		
		$data['title'] = 'Patient Card';
		$this->load->view('admin/templates/no_sidebar', $data);	
	}

	public function print_selected_drugs()
	{
		$prescription_id = $_POST['prescription_id'];
		
		if(count($prescription_id > 0))
		{
			$this->session->set_userdata('selected_drugs', $prescription_id);
		}
		echo 'true';
	}


	public function print_previous_card($visit_id)
	  {
	  	//$data = array('visit_id'=>$visit_id);
		//$data['contacts'] = $this->site_model->get_contacts();
		
		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$v_data['patient_type'] = $patient['patient_type'];
		$v_data['patient_othernames'] = $patient['patient_othernames'];
		$v_data['patient_surname'] = $patient['patient_surname'];
		$v_data['patient_type_id'] = $patient['visit_type_id'];
		$v_data['patient_number'] = $patient['patient_number'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['visit_type_name'] = $patient['visit_type_name'];
		$v_data['patient_id'] = $patient['patient_id'];
		$patient_date_of_birth = $patient['patient_date_of_birth'];
		$personnel_id = $this->session->userdata('personnel_id');
		$v_data['mobile_personnel_id'] = $personnel_id;
		$age = $this->reception_model->calculate_age($patient_date_of_birth);
		$visit_date = $this->reception_model->get_visit_date($visit_id);
		$gender = $patient['gender'];
		$visit_date = date('jS M Y',strtotime($visit_date));
		$v_data['age'] = $age;
		$v_data['visit_date'] = $visit_date;
		$v_data['gender'] = $gender;
		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$v_data['visit_id'] = $visit_id;
		$prescription_id = $this->session->userdata('selected_drugs');
		$data = array('visit_id'=>$visit_id, 'selected_drugs'=>$prescription_id);

		$data['contacts'] = $this->site_model->get_contacts();

		$patient = $this->reception_model->patient_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		
		$data['content'] = $this->load->view('print_previous_card', $v_data, true);
		
		$data['title'] = 'Patient Card';
		$this->load->view('admin/templates/no_sidebar', $data);	
	}


}
	
	