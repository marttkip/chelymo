<!-- search -->
<?php echo $this->load->view('search/search_patients', '', TRUE);?>
<!-- end search -->
 
 <section class="panel panel-primary">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> for <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></h2>

        <div class="pull-right">
	          <a href="<?php echo site_url();?>patients" class="btn btn-info btn-sm pull-right " style="margin-top:-25px"><i class="fa fa-arrow-right"></i> Patients List</a>
	    </div>
    </header>
      <div class="panel-body">
          <div class="padd">
          
<?php
		$search = $this->session->userdata('general_queue_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reception/close_general_queue_search/'.$page_name.'" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		$dependant_id =0;
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
				
				
			$result .= 
				'
					<table class="table  table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Date</th>
						  <th>Patient Number</th>
						  <th>Patient Names</th>
						  <th>Visit Type</th>
						  <th>Waiting Time</th>
						  <th>Coming From</th>
						  <th>Going To</th>
						   <th>Sent At</th>
						  <th>Doctor</th>
						  <th colspan="6">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			// $personnel_query = $this->personnel_model->get_all_personnel();

			// var_dump($department_id); die();
			
			foreach ($query->result() as $row)
			{
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				$visit_created = date('H:i a',strtotime($row->visit_date));
				$visit_id = $row->visit_id;
			    $waiting_time = $this->nurse_model->waiting_time($visit_id);
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$patient_number = $row->patient_number;
				$visit_type_id = $row->visit_type;
				$visit_type = $row->visit_type;
				$accounts = "";//$row->accounts;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type;
				$coming_from = $this->reception_model->coming_from($visit_id);
				$sent_to = $this->reception_model->going_to($visit_id);

				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$doctor = $row->personnel_onames;

				$patient_date_of_birth = $row->patient_date_of_birth;
				$hold_card = $row->hold_card;
				$ward_id = 1;//$row->ward_id;
				$response = 0;//$this->accounts_model->get_visit_balance($visit_id);
				$invoice_total = 0;//$this->accounts_model->total_invoice($visit_id);
				$waiver = 0;//$this->accounts_model->total_waivers($visit_id);
				//$balance = 0;//$this->accounts_model->balance($payments_value,$invoice_total);				
				// $balance = $balance - $waiver;
				// $balance = $response['balance'];


				


				//cash paying patient sent to department but has to pass through the accounts
				if($sent_to == "Accounts")
				{
					$balanced = 'danger';
				}
				elseif($sent_to == "Doctor")
				{
					$balanced = 'success';
				}
				elseif($sent_to == "Laboratory")
				{
					$balanced = 'success';
				}
				elseif($sent_to == "Pharmacy")
				{
					$balanced = 'success';
				}


				
				$v_data = array('visit_id'=>$visit_id);
				$count++;			


				if($department_id == 2 OR $department_id == 8)
				{
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
				

					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Card</a></td>
						<td><a href="'.site_url().'reception/edit_visit/'.$visit_id.'" class="btn btn-sm btn-success fa fa-pencil"> </a></td>
				
					
					

					';
				}else if($department_id == 3)
				{
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
							<td><a href="'.site_url().'dental/'.$visit_id.'" class="btn btn-sm btn-primary">Dental</a></td>
						<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Card</a></td>
				
					
					

					';
				}else if($department_id == 4)
				{
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
							<td><a href="'.site_url().'optical/'.$visit_id.'" class="btn btn-sm btn-primary">Optical</a></td>
					

					';
				}else if($department_id == 6)
				{
					// $department_id = 30;
					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
				

					<td><a href="'.site_url().'laboratory/test/'.$visit_id.'" class="btn btn-sm btn-info">Tests</a></td>
					';
				}else if($department_id == 5)
				{

					$close_page = 0;
					// $department_id = 5;
					if($hold_card == 0)
					{
						$current_time = strtotime(date('h:i:s a'));
						
						$buttons = '
									<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
									
									<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Prescription</a></td>
									'.$button_accounts.'
									';
					}
					else
					{
						$buttons ='<td colspan="4">This card is held</td>';
					}
					
				}else if($department_id == 7)
				{
					$close_page = 0;
					// $department_id = 6;
					$buttons = '
					<td><a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-success">Invoice </a></td>
					<td><a href="'.site_url().'receipt-payment/'.$visit_id.'/'.$close_page.'" class="btn btn-sm btn-primary" >Payments</a></td>	
					<td><a href="'.site_url().'pharmacy/prescription1/'.$visit_id.'/1" class="btn btn-sm btn-info">Prescription</a></td>

					';
				}else if($department_id == 0 OR $department_id == 1)
				{
					// var_dump($personnel_id); die();
					// $department_id = 2;
					$close_page = 0;

					$buttons = '

					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					<td><a href="'.site_url().'nurse/patient_card/'.$visit_id.'/a/0" class="btn btn-sm btn-info">Card</a></td>
					<td><button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#book-appointment'.$visit_id.'"><i class="fa fa-plus"></i> Appointment </button>
								<div class="modal fade " id="book-appointment'.$visit_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content ">
								            <div class="modal-header">
								            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								            	<h4 class="modal-title" id="myModalLabel">Schedule Appointment for '.$patient_surname.'</h4>
								            </div>
								            '.form_open("reception/save_appointment_accounts/".$patient_id."/".$visit_id, array("class" => "form-horizontal")).'

								            <div class="modal-body">
								            	<div class="row">
								            		<input type="hidden" name="redirect_url" id="redirect_url'.$visit_id.'" value="'.$this->uri->uri_string().'">
								            		<input type="hidden" name="patient_id" id="patient_id'.$visit_id.'" value="'.$patient_id.'">
								            		<input type="hidden" name="current_date" id="current_date'.$visit_id.'" value="'.$past_visit_date.'">
								            		<div class="col-md-12">
								            			<div class="col-md-6">
								            				<div class="form-group">
															<label class="col-lg-4 control-label">Visit date: </label>
															
															<div class="col-lg-8">
						                                        <div class="input-group">
						                                            <span class="input-group-addon">
						                                                <i class="fa fa-calendar"></i>
						                                            </span>
						                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="visit_date" id="scheduledate'.$visit_id.'" placeholder="Visit Date" value="'.date('Y-m-d').'" required>
						                                        </div>
															</div>
														</div>

														<div class="form-group">
														  <label class="col-lg-4 control-label">Room: </label>	
															<div class="col-lg-8">
																	<select name="room_id" id="room_id'.$visit_id.'" class="form-control" >
																		<option value="">----Select Room----</option>
																		 '.$all_wards.'
																	</select>
															</div>
														</div>
														

														 <div class="form-group">
										                        <label class="col-lg-4 control-label">Procedure to be done</label>
										                        <div class="col-lg-8">
										                        	<textarea class="form-control" name="procedure_done" id="procedure_done'.$visit_id.'"></textarea>
										                           
										                       </div>
							                             </div>  
														
						                                	
								            			</div>
								            			<div class="col-md-6">
								            				<div class="form-group">
																<label class="col-lg-4 control-label">Doctor: </label>
																<div class="col-lg-8">
																	 <select name="doctor_id" id="doctor_id'.$visit_id.'" class="form-control">
																		<option value="">----Select a Doctor----</option>
																		 '.$all_doctors.'
																	</select>
																</div>
															</div>
								            				<div id="appointment_details" >
							                                    <div class="form-group">
							                                        <label class="col-lg-4 control-label">Schedule: </label>
							                                        
							                                        <div class="col-lg-8">
							                                            <a onclick="check_date('.$visit_id.')" style="cursor:pointer;">[Show Doctors Schedule]</a><br>
							                                            <div id="show_doctor'.$visit_id.'" style="display:none;"> 
							                                                
							                                            </div>
							                                            <div  id="doctors_schedule'.$visit_id.'" style="margin-left: -94px;font-size: 10px;"> </div>
							                                        </div>
							                                    </div>
							                                    
							                                    <div class="form-group">
							                                        <label class="col-lg-4 control-label">Start time : </label>
							                                    
							                                        <div class="col-lg-8">
							                                            <div class="input-group">
							                                                <span class="input-group-addon">
							                                                    <i class="fa fa-clock-o"></i>
							                                                </span>
							                                                <input type="text" class="form-control" data-plugin-timepicker="" name="timepicker_start" id="timepicker_start'.$visit_id.'">
							                                            </div>
							                                        </div>
							                                    </div>
							                                        
							                                    <div class="form-group" >
							                                        <label class="col-lg-4 control-label">End time : </label>
							                                        
							                                        <div class="col-lg-8">		
							                                            <div class="input-group">
							                                                <span class="input-group-addon">
							                                                    <i class="fa fa-clock-o"></i>
							                                                </span>
							                                                <input type="text" class="form-control" data-plugin-timepicker="" name="timepicker_end" id="timepicker_end'.$visit_id.'">
							                                            </div>
							                                        </div>
							                                    </div>
							                                </div>
								            			</div>
								            		</div>
								            	</div>
								            	
														
								              	
								            </div>
								            <div class="modal-footer">
								            	<a  class="btn btn-sm btn-success" onclick="submit_appointment('.$visit_id.','.$patient_id.')">Schedule Appointment</a>
								                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
								            </div>

								               '.form_close().'
								        </div>
								    </div>
								</div>

							</td>
				
					
					
					';
					
				}else if($department_id == 9)
				{
					// var_dump($personnel_id); die();
					// $department_id = 0;

					$buttons = '
					<input type="hidden" name="department_id" id="department_id" value="'.$department_id.'" >
					
					<td></td>';
					
				}
				
								
					
				$result .= 
					'
						<tr class="'.$balanced.'">
							<td>'.$count.'</td>
							<td>'.$visit_date.' '.$time_start.'</td>
							<td>'.$patient_number.'</td>
							<td>'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$visit_type_name.'</td>
							<td>'.$waiting_time.'</td>
							<td>'.$coming_from.'</td>
							<td>'.$sent_to.'</td>
							<td>'.$visit_time.'</td>
						    <td>'.$doctor.'</td>
							'.$buttons.'
						</tr> 
					';
					
						$pink = 15;
					
					$v_data['patient_type'] = $visit_type_id;
					
					
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
?>
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		echo $result;
		?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>

  <audio id="sound1" src="<?php echo base_url();?>sound/beep.mp3"></audio>
  <script type="text/javascript">
  	$(document).ready(function(){
  	   $("#personnel_id").customselect();
       $("#bed_id").customselect();
       $("#room_id").customselect();
       var department_id = document.getElementById("department_id").value;
       // alert(department_id);
		// setInterval(function(){check_new_patients(department_id)},10000);

	 });

  
   	function check_new_patients(module)
		{	
		 var XMLHttpRequestObject = false;
        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		    
		    var config_url = $('#config_url').val();
		    var url = config_url+"nurse/check_queues/"+module;
		    // alert(url);
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	
	         			var one = XMLHttpRequestObject.responseText;
	         			if(one == 1)
	         			{
	         				 var audio1 = document.getElementById("sound1");
						 	 if (audio1.paused !== true){
							    audio1.pause();
							 }
							 else
							 {
								audio1.play();
							 }
	         			}
	         			else
	         			{

	         			}
			         	
	         
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
  </script>

  <script type="text/javascript">
	
	$(document).on("change","select#ward_id",function(e)
	{
		var ward_id = $(this).val();
		
		var url = "<?php echo site_url();?>nurse/get_ward_rooms/"+ward_id;
		// alert(url);
		//get rooms
		$.get( url , function( data ) 
		{
			$( "#room_id" ).html( data );
			
			$.get( "<?php echo site_url();?>nurse/get_room_beds/0", function( data ) 
			{
				$( "#bed_id" ).html( data );
			});
		});
	});
	
	$(document).on("change","select#room_id",function(e)
	{
		var room_id = $(this).val();
		
		//get beds
		$.get( "<?php echo site_url();?>nurse/get_room_beds/"+room_id, function( data ) 
		{
			$( "#bed_id" ).html( data );
		});
	});
</script>

 