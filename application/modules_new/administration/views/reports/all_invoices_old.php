<!-- search -->
<?php echo $this->load->view('search/invoices', '', TRUE);?>
<!-- end search -->
<?php //echo $this->load->view('transaction_statistics', '', TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
<?php
		$result = '';
		if(!empty($search))
		{
			echo '<a href="'.site_url().'administration/reports/close_invoice_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Visit Date</th>
						  <th>Invoice</th>
						  <th>Patient</th>
						  <th>Category</th>
						  <th>Doctor</th>
						  <th>Invoice Total</th>
						  <th>Payments</th>
						  <th>Balance</th>
						  <th colspan="4"></th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			// $personnel_query = $this->personnel_model->get_all_personnel();
			// $result.= form_open("administration/reports/update_checked_payments", array("class" => "form-horizontal"));
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
			
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$invoice_number = $row->invoice_number;
				$patient_table_visit_type = $visit_type_id;
				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$close_card = $row->close_card;
				$hold_card = $row->hold_card;
				$invoice_total = $row->total_invoice;
				$total_waiver = $row->total_waiver;
				$payments_value = $row->amount_paid;
				$balance = $invoice_total - $total_waiver - $payments_value;
				$doctor = $row->personnel_onames.' '.$row->personnel_fname;

				// this is to check for any credit note or debit notes
				// $response = $this->accounts_model->get_visit_balance($visit_id);
				// $balance = $response['balance'];
				// $invoice_total = $response['invoiced_amount'];
				// $payments_value = $response['paid_amount'];
				// end of the debit and credit notes
				$balance = round($balance);
				// $checkbox_data = array(
				// 				  'name'        => 'visit[]',
				// 				  'id'          => 'checkbox'.$visit_id,
				// 				  'class'          => 'css-checkbox lrg',
				// 				  'onclick'          => 'get_visit_balance('.$visit_id.','.$balance.')',
				// 				  'value'       => $visit_id
				// 				);

			
				
				$count++;
				
				
				if($hold_card == 1)
				{
					$button ='<td><a href="'.site_url().'reception/unhold_card/'.$visit_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to unhold this card?\');">Unhold Card</a></td>';
				}
				else
				{
					if($close_card == 1)
					{
						$button ='<td><a href="'.site_url().'accounts/print_invoice_new/'.$visit_id.'" class="btn btn-sm btn-success" target="_blank">Invoice</a></td>
								 <td><a href="'.site_url().'administration/reports/open_visit_current/'.$visit_id.'"  onclick="return confirm(\'Do you want to open card ?\');" class="btn btn-sm btn-info" >Open Card</a></td>';
					}
					else
					{
						$button ='<td><a href="'.site_url().'administration/reports/end_visit_current/'.$visit_id.'"  onclick="return confirm(\'Do you want to close visit ?\');" class="btn btn-sm btn-danger" >Close Card</a></td>';
					}
				}
				// payment value ///
			
				
					$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$visit_date.'</td>
								<td>'.$invoice_number.'</td>
								<td>'.$patient_surname.' '.$patient_othernames.'</td>
								<td>'.$visit_type_name.'</td>
								<td>'.$doctor.'</td>
								<td>'.number_format($invoice_total,2).'</td>
								<td>'.number_format($payments_value,2).'</td>
								<td>'.number_format($balance,2).'</td>
								
								
								'.$button.'
								
							</tr> 
					';
					// <td><input type="text" name="amount'.$visit_id.'" class="form-control" value=""/></td>
					// 			<td><button type="submit" class="btn btn-sm btn-warning" >Update Payment</button></td>
			
			}
		
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visits";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>
  <script type="text/javascript">
  	function get_visit_balance(visit_id,balance)
  	{
  		// alert(visit_id);
  		document.getElementById("amount"+visit_id).value = balance;
  	}
  </script>