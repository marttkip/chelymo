<?php

class Hospital_administration_model extends CI_Model 
{	
	/*
	*	Retrieve all departments
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_current_payments($table, $where, $per_page, $page, $order = 'receipt_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('receipt_number');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	function import_invoice_template()
	{
		$this->load->library('Excel');
		
		$title = 'Invoices Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Visit ID';
		$report[$row_count][1] = 'Invoice Number';
		$report[$row_count][2] = 'Patient No';
		$report[$row_count][3] = 'Invoice Date';
		$report[$row_count][4] = 'Invoiced amount';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_invoices($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_csv_invoices_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_invoices_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($total_columns);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 8))
		{
			$count = 0;
			$comment = '';
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Charge name</th>
						  <th>Amount</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$visit_id = $array[$r][0];
					$invoice_number = $array[$r][1];
					$customer_id = $array[$r][2];
					$visit_date = $array[$r][3];
					$amount_invoiced = $array[$r][4];
					$item_name = $array[$r][5];
					$customer_name = $array[$r][6];
					$quantity = $array[$r][7];

					// get the patient id from the list of patients

					// $this->db->where('customer_id',$customer_id);
					// $query = $this->db->get('')

					// var_dump($customer_id); die();

					$visit_data = array(
										
										"invoice_number" => $invoice_number,
										"customer_id" => $customer_id,
										"invoice_date" => $visit_date,
										"item_price" => $amount_invoiced,
										"item_name" => $item_name,
										"customer_name" => $customer_name,
										"quantity" => $quantity
									);

					$this->db->where($visit_data);
					$query_check = $this->db->get('customer_invoices');
					if($query_check->num_rows() == 0)
					{
						$this->db->insert('customer_invoices', $visit_data);
					}




					$count++;
					$response .= '
									<tr class="">
										<td>'.$count.'</td>
										<td>'.$invoice_number.'</td>
										<td>'.$customer_id.'</td>
										<td>'.$visit_date.'</td>
										<td>'.$amount_invoiced.'</td>
									</tr> 
							';
				
				$response .= '</table>';
				
				$return['response'] = $response;
				$return['check'] = TRUE;
			}
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Charges data not found';
			$return['check'] = FALSE;
		}
		
		return $return;
	}



	// payments
	function import_payment_template()
	{
		$this->load->library('Excel');
		
		$title = 'payments Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'Receipt No';
		$report[$row_count][1] = 'Invoice number';
		$report[$row_count][2] = 'Payment Date';
		$report[$row_count][3] = 'Payed By';
		$report[$row_count][4] = 'payment type';
		$report[$row_count][5] = 'payment amount';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_payments($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_payments_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_payments_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 6))
		{
			$count = 0;
			$comment = '';
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Charge name</th>
						  <th>Amount</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					
					$receipt_number = $array[$r][0];
					$invoice_number = $array[$r][1];
					$payment_date = $array[$r][2];
					$payed_by = $array[$r][3];
					$payment_method_id = $array[$r][4];
					$amount_paid = $array[$r][5];



					
					$data = array(
									'receipt_number' => $receipt_number,
									'invoice_number'=>$invoice_number,
									'payment_date'=>$payment_date,
									'payment_method_id'=>$payment_method_id,
									'amount'=>$amount_paid,
									'bank_id'=>$this->input->post('bank_id')
								);

					// var_dump($data); die();
					$this->db->insert('current_payments', $data);
					
					

					
					$count++;
					$response .= '
									<tr class="">
										<td>'.$count.'</td>
										<td>'.$receipt_number.'</td>
										<td>'.$amount_paid.'</td>
										<td>'.$payment_date.'</td>
									</tr> 
							';
				
				
				
				
				
			}
			$response .= '</tbody></table>';

			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Charges data not found';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	public function get_receipt_amount($receipt_number)
	{

		$this->db->where('receipt_number = "'.$receipt_number.'"');
		$this->db->select('SUM(amount) AS total_amount');
		$query = $this->db->get('current_payments');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;


	}

	public function get_receipt_amount_paid($receipt_number)
	{

		$this->db->where('receipt_number = "'.$receipt_number.'" AND current_payment_status = 2');
		$this->db->select('SUM(amount) AS total_amount');
		$query = $this->db->get('current_payments');

		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}
		return $total_amount;


	}
	public function get_visit_amount($visit_id)
	{
		$this->db->from('v_patients_visits');
		$this->db->select('*');
		$this->db->where('v_patients_visits.visit_id = '.$visit_id);
		$this->db->order_by('v_patients_visits.visit_id','DESC');
		$this->db->join('v_visit_payments','v_visit_payments.visit_id = v_patients_visits.visit_id','LEFT');
		$this->db->join('v_waivers','v_waivers.visit_id = v_patients_visits.visit_id','LEFT');
		$this->db->join('v_visit_invoices','v_visit_invoices.visit_id = v_patients_visits.visit_id','LEFT');
		$this->db->join('personnel','v_patients_visits.personnel_id = personnel.personnel_id','LEFT');
		// $this->db->cache_on();
		$query = $this->db->get('');
		$balance = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $row) {
				# code...
				$invoice_total = $row->total_invoice;
				$total_waiver = $row->total_waiver;
				$payments_value = $row->amount_paid;
				$balance = $invoice_total - $total_waiver - $payments_value;
			}
		}
		return $balance;
	}
}
?>