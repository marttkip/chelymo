<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hospital_administration extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/database');
		$this->load->model('hr/personnel_model');
		$this->load->model('administration/administration_model');
		$this->load->model('administration/reports_model');
		$this->load->model('companies_model');
		$this->load->model('visit_types_model');
		$this->load->model('departments_model');
		$this->load->model('wards_model');
		$this->load->model('rooms_model');
		$this->load->model('beds_model');
		$this->load->model('services_model');
		$this->load->model('hospital_administration_model');
		// $this->load->model('insurance_scheme_model');
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		$this->session->unset_userdata('all_transactions_search');
		
		$data['content'] = $this->load->view('administration/dashboard', '', TRUE);
		
		$data['title'] = 'Dashboard';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function update_service_charges($service_id)
	{
		// get
		// get
		$this->db->where('service_id ='.$service_id.' AND service_charge_status = 1 AND visit_type_id <> 1');
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$service_charge_id = $key->service_charge_id;
				$service_id = $key->service_id;
				$service_charge_name = $key->service_charge_name;
				$service_charge_amount = $key->service_charge_amount;

				$this->db->where('visit_type_id <> 1');
				$query_type = $this->db->get('visit_type');

				if($query_type->num_rows() > 0)
				{
					foreach ($query_type->result() as $key_query) {
						$visit_type_id = $key_query->visit_type_id;

						$this->db->where('service_charge_name = "'.$service_charge_name.'" AND service_id = '.$service_id.' AND service_charge_status = 1 AND visit_type_id = '.$visit_type_id);
						$query_update = $this->db->get('service_charge');

						if($query_update->num_rows() == 0)
						{

							$insert_query = array(
										'service_charge_amount'=>$service_charge_amount,
										'service_id'=>$service_id,
										'service_charge_name'=> $service_charge_name,
										'visit_type_id'=> $visit_type_id,
										'service_charge_status'=>1,
										'created'=>date('Y-m-d H:i:s'),
										'created_by'=>$this->session->userdata('personnel_id'),
										'modified_by'=>$this->session->userdata('personnel_id')
									);
							// var_dump($insert_query); die();
							$this->db->insert('service_charge',$insert_query);

						}


					}
				}
			}
		}
		$this->session->set_userdata("success_message", "Successfully updated charges ");
		redirect('hospital-administration/service-charges/'.$service_idd);
	}
	public function update_service_charges_schools($service_id)
	{
		// get
		// get
		$this->db->where('service_id ='.$service_id.' AND service_charge_status = 1 AND service_charge_delete = 0  AND visit_type_id = 10');
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				unset($key->visit_type_id);
				unset($key->service_charge_id);
				// $service_charge_id = $key->service_charge_id;

				$service_charge_name = $key->service_charge_name;
				$key = (array)$key;				
				// $service_id = $key->service_id;
				// $service_charge_amount = $key->service_charge_amount;

				$this->db->where('visit_type_id > 20');
				$query_type = $this->db->get('visit_type');

				if($query_type->num_rows() > 0)
				{
					foreach ($query_type->result() as $key_query) {
						$visit_type_id = $key_query->visit_type_id;
						$key['visit_type_id'] = $visit_type_id;
						$this->db->where('service_charge_name = "'.$service_charge_name.'" AND service_id = '.$service_id.' AND service_charge_status = 1 AND service_charge_delete = 0 AND visit_type_id = '.$visit_type_id);
						$query_update = $this->db->get('service_charge');

						if($query_update->num_rows() == 0)
						{
							// $insert_query = array(
							// 			'service_charge_amount'=>$service_charge_amount,
							// 			'service_id'=>$service_id,
							// 			'service_charge_name'=> $service_charge_name,
							// 			'visit_type_id'=> $visit_type_id,
							// 			'service_charge_status'=>1,
							// 			'created'=>date('Y-m-d H:i:s'),
							// 			'created_by'=>$this->session->userdata('personnel_id'),
							// 			'modified_by'=>$this->session->userdata('personnel_id')
							// 		);
							// var_dump($insert_query); die();
							// var_dump($key); die();
							$this->db->insert('service_charge',$key);

						}


					}
				}
			}
		}
		$this->session->set_userdata("success_message", "Successfully updated charges ");
		redirect('hospital-administration/services/');
	}
	function import_invoices_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_invoice_template();
	}

	function import_invoices()
	{
		//open the add new product
		$v_data['title'] = 'Import invoices';
		$data['title'] = 'Import invoices';
		$data['content'] = $this->load->view('invoices', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_invoice_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_invoices($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Charges';
		$data['title'] = 'Import Charges';
		$data['content'] = $this->load->view('invoices', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	function import_payments_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_payment_template();
	}

	function import_payments()
	{


		$where = 'current_payment_id > 0 AND account.account_id = current_payments.bank_id';
		$table = 'current_payments,account';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital-administration/import-payments';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->hospital_administration_model->get_current_payments($table, $where, $config["per_page"], $page);
		
		//change of order method 
		
		
		$data['title'] = 'Payments';		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_payment_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_payments($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		redirect('hospital-administration/import-payments');
		//open the add new product
		//$v_data['title'] = 'Import Charges';
		//$data['title'] = 'Import Charges';
		//$data['content'] = $this->load->view('payments', $v_data, true);
		//$this->load->view('admin/templates/general_page', $data);
	}

	public function update_payments()
	{
		$this->db->where('current_payment_status = 0');
		$query = $this->db->get('current_payments');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$invoice_number = $value->invoice_number;
				$amount_paid = $value->amount;
				$payment_date = $value->payment_date;
				$receipt_number = $value->receipt_number;
				$payment_method_id = $value->payment_method_id;
				$current_payment_id = $value->current_payment_id;

				$paid_by = $value->paid_by;
				$bank_id = $value->bank_id;

				$type_payment = 1;
				$transaction_code = $receipt_number;
				$payment_service_id = 16;
				$change = 0;


				$this->db->where('invoice_number = "'.$invoice_number.'"');
				$query_amount = $this->db->get('visit');
				if($query_amount->num_rows() == 1)
				{
					foreach ($query_amount->result() as $key => $value) {
						# code...
						$visit_id = $value->visit_id;
					}
					$balance = $this->hospital_administration_model->get_visit_amount($visit_id);

					//var_dump($balance); die();
					$data = array(
						'visit_id' => $visit_id,
						'payment_method_id'=>$payment_method_id,
						'amount_paid'=>$amount_paid,
						'personnel_id'=>$this->session->userdata("personnel_id"),
						'payment_type'=>$type_payment,
						'transaction_code'=>$transaction_code,
						'payment_service_id'=>$payment_service_id,
						'change'=>$change,
						'payment_created'=>$payment_date,
						'payment_created_by'=>$this->session->userdata("personnel_id"),
						'approved_by'=>$this->session->userdata("personnel_id"),
						'date_approved'=>$payment_date,
						'bank_id'=>$bank_id
					);
					// var_dump($data); die();
					if($this->db->insert('payments', $data))
					{
						$payment_id = $this->db->insert_id();
						$update_array['payment_id'] = $payment_id;
						$update_array['current_payment_status'] = 2;
						$this->db->where('current_payment_id',$current_payment_id);
						$this->db->update('current_payments',$update_array);


						if($balance < $amount_paid)
						{
							// has a deni close card 5
							$visit_update['close_card'] = 5;
						}
						else if($balance == $amount_paid)
						{
							$visit_update['close_card'] = 6;
						}
						else
						{
							$visit_update['close_card'] = 7;
						}

						$this->db->where('visit_id',$visit_id);
						$this->db->update('visit',$visit_update);


					}
				}
				
			}
		}

		redirect('hospital-administration/import-payments');
	}
	public function update_lab_test()
	{
		$this->db->where('lab_test_id > 0 and visit_type_id = 1 AND drug_id = 0');
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$service_charge_id = $value->service_charge_id;
				$lab_test_id = $value->lab_test_id;

				$update['lab_test_id'] = $lab_test_id;

				$this->db->where('service_charge_id',$service_charge_id);
				$this->db->update('visit_charge',$update);

				$update_query['drug_id'] = 1;
				$this->db->where('service_charge_id',$service_charge_id);
				$this->db->update('service_charge',$update_query);
			}
		}
	}


}
?>