SELECT
	visit.patient_id AS receivables_id,
	'Individual' AS receivables_type,
	CONCAT( "Patient ", patients.patient_surname, " Patient Number : ", patients.patient_number ) AS receivable_Name,
	(
		CASE
		WHEN
		( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) = 0 ), visit_charge.visit_charge_amount,0 ) ) = 0 )
		THEN
		0
		WHEN (
			sum(
			IF
				(
					( DATEDIFF( CURDATE( ), visit_charge.date ) = 0 ),
					visit_charge.visit_charge_amount,
					0
				)
			) > 0
			) THEN
			(
				(
					(
						sum(
						IF
							(
								( DATEDIFF( CURDATE( ), visit_charge.date ) = 0 ),
								visit_charge.visit_charge_amount,
								0
							)
							) - (
							SELECT COALESCE( sum( payments.amount_paid ), 0 ) FROM
							payments
							WHERE
							( payments.visit_id = visit.visit_id )
							)
					)

				)
			)
		END
	) AS `Coming Due`,
	(
		CASE
			WHEN (
				sum(
					IF
						(
							(
								DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 1 
								AND 30
							),
							visit_charge.visit_charge_amount,
							0
						)
					) = 0
					) THEN
					0
					WHEN (
						sum(
						IF
							(
								(
									DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 1 
									AND 30
								),
								visit_charge.visit_charge_amount,
								0
							)
						) > 0
						) THEN
						(
							(
								(
									(
										sum(
										IF
											(
												(
													DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 1 
													AND 30
												),
												visit_charge.visit_charge_amount,
												0
											)
											) - (SELECT COALESCE ( sum( payments.amount_paid ), 0 ) FROM payments WHERE  ( payments.visit_id = visit.visit_id ) )
										)

									)

								)

						)
						END
	) AS `1-30 Days`,
	(
		CASE
		WHEN
		 ( sum(IF( ( DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 31 AND 60),visit_charge.visit_charge_amount,0)) = 0 ) 
		THEN
		 0
		 WHEN ( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 31 AND 60 ), visit_charge.visit_charge_amount, 0) ) > 0)
		 THEN
		 (
		 	( ( ( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 31 AND 60 ), visit_charge.visit_charge_amount, 0 ) ) - 
		 		( SELECT COALESCE ( sum( payments.amount_paid ), 0 ) FROM payments WHERE ( payments.visit_id = visit.visit_id ) ) )
		 	  )
		 	)
		 )
		 END
	) AS `31-60 Days`,
	(
		CASE
		WHEN
		( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 61 AND 90 ), visit_charge.visit_charge_amount, 0 )
									) = 0 )
		THEN
		0
		WHEN ( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 61 AND 90 ), visit_charge.visit_charge_amount, 0 ) ) > 0 )
		THEN
		(
			( ( ( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) BETWEEN 61 AND 90 ), visit_charge.visit_charge_amount, 0 ) )
			 - ( SELECT COALESCE ( sum( payments.amount_paid ), 0 ) FROM payments  WHERE ( payments.visit_id = visit.visit_id ) ) ) ) )
			)
		END
	) AS `61-90 Days`,
	(
		CASE
			WHEN
				( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) > 90 ), visit_charge.visit_charge_amount, 0 ) ) = 0 ) 
			THEN
				0
			WHEN ( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) > 90 ), visit_charge.visit_charge_amount, 0 ) ) > 0 )
			THEN
			 	( ( ( ( sum( IF ( ( DATEDIFF( CURDATE( ), visit_charge.date ) > 90 ), visit_charge.visit_charge_amount, 0 ) )
			 	 - ( SELECT COALESCE ( sum( payments.amount_paid ), 0 ) FROM payments WHERE ( payments.visit_id = visit.visit_id ) ) ) )
			 	  )

			 	)
			 
			END
	) AS `>90 Days`,
	(

	) AS `Total`

	FROM
	visit_charge
	JOIN visit ON visit.visit_id = visit_charge.visit_id
	LEFT JOIN patients ON patients.patient_id = visit.patient_id 
WHERE
	visit.visit_type = "1" 
GROUP BY
	visit.patient_id